<!doctype html>
<html>
<?php include_once 'head.php' ?>
<?php include_once 'connection.php' ?>
<body>
  <?php include_once 'header.php'?>
	<div class="banner">
		<img src="assets/images/about.jpg" alt="img">
		<div class="container">
			<h1 class="inner-heading"><span>Our</span> Products</h1>
		</div>
	</div>
	
	
	<!---------DIsplay------->
	<section class="first-block">
		<div class="container">
			<ul class="row products-list">
				    <?php 
							
							  $num_rec_per_page=8;
							  if (isset($_GET["page"])) 
							  { $page  = $_GET["page"]; 
								} 
								else { $page=1; }; 
							$start_from = ($page-1) * $num_rec_per_page; 
							$sql = "SELECT * FROM data LIMIT $start_from, $num_rec_per_page"; 
							$rs_result = mysqli_query ($link,$sql);
							 
						
						 while ($row=mysqli_fetch_row($rs_result))
					  {
						  $id=$row['0'];
						  $name=$row['1'];
						  $sku=$row['2'];
						  $image=$row['3'];
						  $description=$row['4'];
						  $price=$row['5'];
						  $quantity=$row['6'];
						  $weight=$row['7'];
				?>
		
	
	
	
				<li class="col-xs-6 col-sm-3">
					<figure>
						<img src="admin/<?php echo $image ?>">
						<figcaption>
							<h4> <?php echo $name ?></h4>
							<div class="row">
								
								<div class="col-xs-12 col-sm-12">
									<span><?php echo $price ?> Rs</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<a href="contact.php?sku=<?php echo $sku; ?>">Send Quote</a>
								</div>
							</div>
							
						</figcaption>
					</figure>
				</li>
				<?php } ?>
			</ul>
			    <?php 
						$sql = "SELECT * FROM data"; 
						$rs_result = mysqli_query($link,$sql); 
						$total_records = mysqli_num_rows($rs_result); 
						$total_pages = ceil($total_records / $num_rec_per_page);
                ?>						
			    <nav class="pagination" aria-label="Page navigation example">
				  <ul class="pagination">
					<li class="page-item">
					  <a class="page-link" href="product.php?page=1" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
						<span class="sr-only">Previous</span>
					  </a>
					</li>
					    <?php for ($i=1; $i<=$total_pages; $i++) { ?>
									<li class="page-item"><a class="page-link" href="product.php?page=<?php echo $i ?>"><?php echo $i ?></a></li>
						<?php }; ?>
					      
					
					<li class="page-item">
					  <a class="page-link" href="product.php?page=<?php echo $total_pages ?>" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
						<span class="sr-only">Next</span>
					  </a>
					</li>
				  </ul>
			    </nav>
		</div>
</section>
<footer>
    <div class="footer_top">
        <div class="container">
            <div class="col-md-3 col-sm-3 col-xs-12"> <a href="#"><img src="assets/images/logo-footer.png" width="182" height="46" alt=""/></a>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="social_icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> </a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>INFORMATION</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>SERVICES</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>KEEP IN TOUCH</h4>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="mailling">
                    <input type="text" placeholder="your email address">
                    <button><i class="fa fa-paper-plane" aria-hidden="true"></i> </button>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom text-center">
    <div class="container">
    Copy right@2017!All rights are reserved
    </div>
    </div>
</footer>
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
