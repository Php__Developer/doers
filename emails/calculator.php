<!DOCTYPE html>
<html>
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta content="utf-8" http-equiv="encoding">
	<title>Milk Calculator</title>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" ></script>

	<link rel="stylesheet" type="text/css" href="http://getbootstrap.com/dist/css/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="#">Milk Calculator</a>
      <!--button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button-->

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <!--ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
        </ul-->
      </div>
    </nav>

    <main role="main" class="container">

      <div class="starter-template">
        <h1>Milk Calculator</h1>
        
        	<form>
        		<div class="row">
	        		<div class="col-md-6 m-b-lg">
						<div class="form-group">
						    <label for="per_kg_fat_price">Per Kg. Fat Price</label>
						    <input type="number" class="form-control" id="per_kg_fat_price" placeholder="Per Kg. Fat Price" min="1" value="530" step="0.01">
						    <small id="emailHelp" class="form-text text-muted">Please enter Per Kg. Fat Price</small>
						</div>

						<h2>Milk Price</h2>
						<table class="table table-bordered">
							<tr>
								<th></th>
								<th>Milk Price</th>
								<th>Ratio</th>
								<th></th>
							</tr>
							<tr>
								<td>Fat</td>
								<td><span id="fat_milk_price"></span></td>
								<td><input type="number" name="fat_vol_qty" min="1" value="4" step="0.01" ></td>
								<td><span id="fat_sale_price"></span></td>
							</tr>
							<tr>
								<td>SNF</td>
								<td><span id="snf_milk_price"></span></td>
								<td><input type="number" name="snf_vol_qty" min="1" value="8.5" step="0.01" ></td>
								<td><span id="snf_sale_price"></span></td>
							</tr>
							<tr>
								<td colspan="3" align="right">Total</td>
								<td><span id="tot_milk_price"></span></td>
							</tr>
							<tr>
								<td colspan="3" align="right">Milk Volume</td>
								<td><input type="number" name="txt_milk_volume" id="txt_milk_volume" value="1" min="1" ></td>
							</tr>
							<tr>
								<td colspan="3" align="right">Total Milk Price(per LTr)</td>
								<td><span id="tot_milk_volume"></span></td>
							</tr>
						</table>
					</div>
					<div class="col-md-3 m-b-lg">
						<h2>Fat Kg. Price</h2>
						<div class="form-group">
					    	<label for="fat_incentive_1">Incentive</label>
					    	<input type="number" class="form-control" id="fat_incentive_1" value="6.6" step="0.01" >
						</div>
						<div class="form-group">
					    	<label for="fat_incentive_2">Incentive</label>
					    	<input type="number" class="form-control" id="fat_incentive_2" value="0" step="0.01" >
						</div>
						<div class="form-group" style="display:none;">
					    	<label for="fat_kg_price">Total</label>
					    	<input type="text" class="form-control" id="fat_kg_price" placeholder="Fat Kg. Price" readonly="" >
						</div>
						

						<!--h2>Fat with Quantity</h2>
					  	<table class="table table-bordered">
					  		<tr>
					  			<td>Quantity</td>
					  			<td>Amount</td>
					  		</tr>
					  		<tr>
					  			<td><input type="number" name="fat_qty" value="4" min="1" step="0.01" ></td>
					  			<td><span class="fat_qty_value"></span></td>
					  		</tr>
					  	</table>
						<h2>SNF with Quantity</h2>
					  	<table class="table table-bordered">
					  		<tr>
					  			<td>Quantity</td>
					  			<td>Amount</td>
					  		</tr>
					  		<tr>
					  			<td><input type="number" name="snf_qty" value="8.5" min="1" step="0.01" ></td>
					  			<td><span class="snf_qty_value"></span></td>
					  		</tr>
					  	</table-->
					</div>
					<div class="col-md-3 m-b-lg">
						<h2>SNF Kg. Price</h2>
						<div class="form-group">
					    	<label for="snf_incentive_1">Incentive</label>
					    	<input type="number" class="form-control" id="snf_incentive_1" value="10" step="0.01" >
						</div>
						<div class="form-group">
					    	<label for="snf_incentive_2">Incentive</label>
					    	<input type="number" class="form-control" id="snf_incentive_2" value="4.7" step="0.01" >
						</div>
					  	<div class="form-group" style="display:none;">
					    	<label for="snf_kg_price">Total</label>
					    	<input type="text" class="form-control" id="snf_kg_price" placeholder="SNF Kg. Price" readonly="" >
					  	</div>
						
					  	<!--table class="table table-bordered">
					  		<tr>
					  			<td>Milk Price</td>
					  			<td>Sale Price</td>
					  		</tr>
					  		<tr>
					  			<td><input type="number" name="milk_price" value="2.71" min="1" step="0.01" ></td>
					  			<td><span class="sale_price"></span></td>
					  		</tr>
					  		<tr>
					  			<td><input type="number" name="milk_price_1" value="1.91" min="1" step="0.01" ></td>
					  			<td><span class="sale_price_1"></span></td>
					  		</tr>
					  		<tr>
					  			<td>Total</td>
					  			<td><span class="total_amt"></span></td>
					  		</tr>
					  	</table-->
					</div>
				</div>
			  	<div class="col-lg-12">
			  		<table class="table table-bordered">
			  			<tr class="milk_inc_tr">
			  				<th>Milk Incentive </th>
			  				<th ><input type="hidden" name="incentive_quantity[]" class="incentive_quantity" value="50" /></th>
			  				<th  class="quantity_th" data-quantity="50">50 Lt per day
			  						
			  				</th>
			  				<td><input type="number" name="incentive_amount[]" min="0" step="0.01" value="2" class="incentive_amount" /> </td>
			  				<td> <a href="#" class="add_more">Add More</a> </td>
			  			</tr>


			  			<tr class="bmc_inc">
			  				<th>BMC  Incentive</th>
			  				<th></th>
			  				<th>Management Charges</th>
			  				<td><input type="number" name="bmc_management_charges" min="0" step="0.01" value="0" /> </td>
			  			</tr>
			  			<tr>
			  				<th></th>
			  				<th></th>
			  				<th>Maintenances Charges</th>
			  				<td><input type="number" name="bmc_maintenance_charges" min="0" step="0.01" value="0" /> </td>
			  			</tr>
			  			<tr>
			  				<td colspan="4"></td>
			  			</tr>
			  			<tr>
			  				<th></th>
			  				<th></th>
			  				<th>Total</th>
			  				<td><span class="incentive_total" ></span></td>
			  			</tr>
			  			<tr>
			  				<th></th>
			  				<th></th>
			  				<th>Incentive + Total Milk Price</th>
			  				<td><span class="incentive_total_milk_volume" ></span></td>
			  			</tr>
			  		</table>
			  	</div>
			  
			</form>

      </div>

    </main><!-- /.container -->

    <script type="text/javascript">
    	$(function(){
    		var trhtml = $('.milk_inc_tr').html();
    		$('.table-bordered').on('click', '.add_more', function (event) {
    			event.preventDefault();
    			$(this).hide();    			
    			//alert(trhtml);
    			$('<tr class="milk_inc_tr">'+trhtml + '</tr>').insertBefore('.bmc_inc');
    			var current_quantity = $(this).closest('tr').find('.quantity_th').data('quantity');    			
				$(this).closest('tr').next('tr').find('.quantity_th').attr('data-quantity',parseInt(current_quantity)+ 50);
				//alert($(this).closest('tr').next('tr').find('.incentive_quantity').attr('name'));
				$(this).closest('tr').next('tr').find('.incentive_quantity').val(parseInt(current_quantity)+ 50);				
				$(this).closest('tr').next('tr').find('.quantity_th').text(parseInt(current_quantity)+ 50 + ' Lt per day');
				cal_fat_snf();
    		})
    		//alert('here');
    		$('form').on('keyup blur change', 'input', function(){
    			cal_fat_snf();
    		})
    		
    	})

    	function cal_fat_snf(){
    		var per_kg_fat_price = $("#per_kg_fat_price").val();
    		var fat_incentive_1 = $("#fat_incentive_1").val();
    		fat_incentive_1 = (parseFloat(fat_incentive_1)>0)?parseFloat(fat_incentive_1):0;
    		var fat_incentive_2 = $("#fat_incentive_2").val();
    		fat_incentive_2 = (parseFloat(fat_incentive_2)>0)?parseFloat(fat_incentive_2):0;
    		var snf_incentive_1 = $("#snf_incentive_1").val();
    		snf_incentive_1 = (parseFloat(snf_incentive_1)>0)?parseFloat(snf_incentive_1):0;
    		var snf_incentive_2 = $("#snf_incentive_2").val();
    		snf_incentive_2 = (parseFloat(snf_incentive_2)>0)?parseFloat(snf_incentive_2):0;

			var fat_qty = $("#fat_qty").val();
    		fat_qty = (parseFloat(fat_qty)>0)?parseFloat(fat_qty):0;

			var snf_qty = $("#snf_qty").val();
    		snf_qty = (parseFloat(snf_qty)>0)?parseFloat(snf_qty):0;
    		

    		//var fat_kg_price = $("#fat_kg_price").val();
    		//var snf_kg_price = $("#snf_kg_price").val();

    		var fat_kg_price = (per_kg_fat_price/2).toFixed(1);
    		var final_fat_kg_price = parseFloat(fat_kg_price)+parseFloat(fat_incentive_1)+parseFloat(fat_incentive_2);
    		//console.log(final_fat_kg_price);

    		var snf_kg_price = (per_kg_fat_price/3).toFixed(1);
    		
    		var final_snf_kg_price = parseFloat(snf_kg_price)+parseFloat(snf_incentive_1)+parseFloat(snf_incentive_2);
    		//var fat_total = (final_fat_kg_price*fat_qty).toFixed(1);
    		//var snf_total = Math.round(final_snf_kg_price*snf_qty);
    		//console.log(final_fat_kg_price.toFixed(1));
    		$("#fat_kg_price").val(final_fat_kg_price.toFixed(1));
    		$("#snf_kg_price").val(final_snf_kg_price.toFixed(1));
    		//$("#fat_total").val(fat_total);
    		//$("#snf_total").val(snf_total);

    		/* FAT AND SNF with quantity */
    		/* var fat_qty = $('input[name="fat_qty"]').val();
    		var snf_qty = $('input[name="snf_qty"]').val();
    		var fat_qty_value = fat_kg_price*fat_qty;
    		var snf_qty_value = Math.round(snf_kg_price*snf_qty);
    		$(".fat_qty_value").text(fat_qty_value.toFixed(1));
    		$(".snf_qty_value").text(snf_qty_value); */

    		/* calculate milk price */
    		var fat_milk_price = Math.trunc(final_fat_kg_price)/100;
    		var snf_milk_price = Math.trunc(final_snf_kg_price)/100;
    		//console.log(fat_milk_price);
    		$("#fat_milk_price").text(fat_milk_price);
    		$("#snf_milk_price").text(snf_milk_price);

    		/* var final_fat_sale_amt = fat_milk_price*fat_qty;
    		var final_snf_sale_amt = snf_milk_price*snf_qty;

    		$("#fat_sale_price").text(final_fat_sale_amt.toFixed(2));
    		$("#snf_sale_price").text(final_snf_sale_amt.toFixed(2)); */

			var fat_vol_qty = $('input[name="fat_vol_qty"]').val();
    		fat_vol_qty = (parseFloat(fat_vol_qty)>0)?parseFloat(fat_vol_qty):0;
			var snf_vol_qty = $('input[name="snf_vol_qty"]').val();
    		snf_vol_qty = (parseFloat(snf_vol_qty)>0)?parseFloat(snf_vol_qty):0;
			//console.log(fat_milk_price+' - '+fat_vol_qty);
			var final_fat_sale_amt = fat_milk_price*fat_vol_qty;
    		var final_snf_sale_amt = snf_milk_price*snf_vol_qty;

    		$("#fat_sale_price").text(final_fat_sale_amt.toFixed(2));
    		$("#snf_sale_price").text(final_snf_sale_amt.toFixed(2));

    		var tot_milk_vol = parseFloat(final_fat_sale_amt)+parseFloat(final_snf_sale_amt);
    		$("#tot_milk_price").text(tot_milk_vol.toFixed(2));

    		var txt_milk_volume = $('input[name="txt_milk_volume"]').val();
    		txt_milk_volume = (parseFloat(txt_milk_volume)>0)?parseFloat(txt_milk_volume):0;
    		var final_milk_volumne = tot_milk_vol*txt_milk_volume;
    		$("#tot_milk_volume").text(final_milk_volumne.toFixed(2));
    		//console.log(final_milk_volumne.toFixed(2));
    		var incentive_amount = 0
    		$('.milk_inc_tr').each(function(idx, li) {
				if(parseInt(txt_milk_volume) >= parseInt($(this).find('.incentive_quantity').val()) /*&& parseInt(txt_milk_volume) < (parseInt($(this).find('.incentive_quantity').val()) + 50 )*/ ) {
					incentive_amount = $(this).find('.incentive_amount').val();					
					//with_incentive_price += incentive
				}
			});
    		/* Calculate Milk and BMC incentive */
			/*var milk_inc_50 = $('input[name="milk_inc_50"]').val();
    		milk_inc_50 = (parseFloat(milk_inc_50)>0)?parseFloat(milk_inc_50):0;
			var milk_inc_100 = $('input[name="milk_inc_100"]').val();
    		milk_inc_100 = (parseFloat(milk_inc_100)>0)?parseFloat(milk_inc_100):0;
			var milk_inc_200 = $('input[name="milk_inc_200"]').val();
    		milk_inc_200 = (parseFloat(milk_inc_200)>0)?parseFloat(milk_inc_200):0;
			var milk_inc_500 = $('input[name="milk_inc_500"]').val();
    		milk_inc_500 = (parseFloat(milk_inc_500)>0)?parseFloat(milk_inc_500):0;
			var bmc_management_charges = $('input[name="bmc_management_charges"]').val();
    		bmc_management_charges = (parseFloat(bmc_management_charges)>0)?parseFloat(bmc_management_charges):0;
			var bmc_maintenance_charges = $('input[name="bmc_maintenance_charges"]').val();
    		bmc_maintenance_charges = (parseFloat(bmc_maintenance_charges)>0)?parseFloat(bmc_maintenance_charges):0;
    		var incentive_tot = milk_inc_50+milk_inc_100+milk_inc_200+milk_inc_500+bmc_management_charges+bmc_maintenance_charges + incentive_amount;
    		$(".incentive_total").text(incentive_tot);*/
    		var milk_inc_50 = $('input[name="milk_inc_50"]').val();
    		milk_inc_50 = (parseFloat(milk_inc_50)>0)?parseFloat(milk_inc_50):0;
			var milk_inc_100 = $('input[name="milk_inc_100"]').val();
    		milk_inc_100 = (parseFloat(milk_inc_100)>0)?parseFloat(milk_inc_100):0;
			var milk_inc_200 = $('input[name="milk_inc_200"]').val();
    		milk_inc_200 = (parseFloat(milk_inc_200)>0)?parseFloat(milk_inc_200):0;
			var milk_inc_500 = $('input[name="milk_inc_500"]').val();
    		milk_inc_500 = (parseFloat(milk_inc_500)>0)?parseFloat(milk_inc_500):0;
			var bmc_management_charges = $('input[name="bmc_management_charges"]').val();
    		bmc_management_charges = (parseFloat(bmc_management_charges)>0)?parseFloat(bmc_management_charges):0;
			var bmc_maintenance_charges = $('input[name="bmc_maintenance_charges"]').val();
    		bmc_maintenance_charges = (parseFloat(bmc_maintenance_charges)>0)?parseFloat(bmc_maintenance_charges):0;
    		var incentive_tot = bmc_management_charges+bmc_maintenance_charges + parseFloat(incentive_amount);
    		//console.log(incentive_tot);
    		var final_incentive_amount = incentive_tot * parseInt(txt_milk_volume);
    		$(".incentive_total").text(final_incentive_amount.toFixed(2) );
    		var incentive_total_milk_volume = (final_incentive_amount + final_milk_volumne).toFixed(2);
    		$(".incentive_total_milk_volume").text(incentive_total_milk_volume);

    		/* var milk_price_1 = $('input[name="milk_price_1"]').val();
    		var sale_price = (milk_price*fat_qty).toFixed(1);
    		var sale_price_1 = (milk_price_1*snf_qty).toFixed(1);
    		var tot_amt = (parseFloat(sale_price)+parseFloat(sale_price_1)).toFixed(1);
    		$(".sale_price").text(sale_price);
    		$(".sale_price_1").text(sale_price_1);
    		$(".total_amt").text(tot_amt); */
    	}
    	cal_fat_snf();
    </script>

</body>
</html>