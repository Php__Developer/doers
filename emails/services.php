<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>DAWO</title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/fonts.css">
</head>

<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button>
            <a class="navbar-brand visible-xs" href="#"><img src="assets/images/logo.png" alt=""/></a>
          </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li > <a href="index.html">HOME</a> </li>
                            <li> <a href="product.php">PRODUCTS</a> </li>
                            <li class="logo hidden-xs"><a href="index.html"><img src="assets/images/logo.png" alt=""/></a></li>
                            <li> <a href="about.html">ABOUT US</a> </li>
                            <li> <a href="contact.html">CONTACT US</a> </li>
                            <li class="active"> <a href="services.php">Services</a> </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>


<div class="container">
<div class="row">
<div class="col-md-4 col-sm-4 col-xs-12 text-center">
<div class="thumbnail" data-toggle="modal" data-target="#myModal">
    <img src="assets/images/blog_03.png" alt=""/>
<h3>Milking Machine Service</h3>
    </div>
     </div>
     
     <div class="col-md-4 col-sm-4 col-xs-12 text-center">
<div class="thumbnail" data-toggle="modal" data-target="#myModal2">
    <img src="assets/images/blog_03.png" alt=""/>
<h3>Miliking Parlours Service</h3>
    </div>
     </div>
     
          <div class="col-md-4 col-sm-4 col-xs-12 text-center">
<div class="thumbnail" data-toggle="modal" data-target="#myModal3">
    <img src="assets/images/blog_03.png" alt=""/>
<h3>Scrapper</h3>
    </div>
     </div>


</div>
</div>





<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Milk Machines Service Request</h4>
      </div>
      <div class="modal-body" data-type="machine_service">
      <center><div class="success-msg"></div></center>
       <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Name*</label>
    <input type="text" class="form-control name" id="exampleInputEmail1" placeholder="Email" name="name">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control email" id="exampleInputEmail1" placeholder="Email" name="email">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone*</label>
    <input type="text" class="form-control phone" id="exampleInputEmail1" placeholder="Email" name="phone">
    <div class="errors"></div>
  </div>
  <button type="submit" class="btn btn-default btn btn-primary submit-btn">Submit</button>
</form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>


<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Milking Parlour Service Request</h4>
      </div>
      <div class="modal-body" data-type="parlour_service">
      <center><div class="success-msg"></div></center>
       <form action="saverequest.php">
    <div class="form-group">
    <label for="exampleInputEmail1">Name*</label>
    <input type="text" class="form-control name" id="exampleInputEmail1" placeholder="Email" name="name">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control email" id="exampleInputEmail1" placeholder="Email" name="email">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone*</label>
    <input type="text" class="form-control phone" id="exampleInputEmail1" placeholder="Email" name="phone">
    <div class="errors"></div>
  </div>

  <!-- <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div> -->
  <!-- <div class="form-group">
    <label for="exampleInputFile">File input</label>
    <input type="file" id="exampleInputFile">
    <p class="help-block">Example block-level help text here.</p>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox"> Check me out
    </label>
  </div>-->
  <button type="submit" class="btn btn-default btn-primary submit-btn">Submit</button>
</form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>


<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Scrapper Request</h4>
      </div>
      <div class="modal-body" data-type="scrapper">
      <center><div class="success-msg"></div></center>
       <form>
   <div class="form-group">
    <label for="exampleInputEmail1">Name*</label>
    <input type="text" class="form-control name" id="exampleInputEmail1" placeholder="Email" name="name">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" class="form-control email" id="exampleInputEmail1" placeholder="Email" name="email">
    <div class="errors"></div>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone*</label>
    <input type="text" class="form-control phone" id="exampleInputEmail1" placeholder="Email" name="phone">
    <div class="errors"></div>
  </div>
  <button type="submit" class="btn btn-default btn-primary submit-btn">Submit</button>
</form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>





<footer>
    <div class="footer_top">
        <div class="container">
            <div class="col-md-3 col-sm-3 col-xs-12"> <a href="#"><img src="assets/images/logo-footer.png" width="182" height="46" alt=""/></a>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="social_icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> </a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>INFORMATION</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>SERVICES</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>KEEP IN TOUCH</h4>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="mailling">
                    <input type="text" placeholder="your email address">
                    <button><i class="fa fa-paper-plane" aria-hidden="true"></i> </button>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom text-center">
    <div class="container">
    Copy right@2017!All rights are reserved
    </div>
    </div>
</footer>
<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
      $('.submit-btn').on('click',function(event){
       //var base = 'http://localhost/codei/dawo/admin1/'
        var base = 'http://demo.digizap.in/dawo/admin/'
        //alert("asdf");  
          event.preventDefault();
          /*console.log($(this).closest('.modal-body').attr('data-type'));
          console.log($(this).closest('.modal-body').find('.name').attr('class'));*/
          var name = $(this).closest('.modal-body').find('.name').val();
          //console.log(name);
          var email = $(this).closest('.modal-body').find('.email').val();
          //console.log(email);
          var phone = $(this).closest('.modal-body').find('.phone').val();
          var type = $(this).closest('.modal-body').attr('data-type');
          //console.log(phone);
          if(name == ''){
            //console.log('asdfasdf');
            $(this).closest('.modal-body').find('.name').closest('.form-group').find('.errors').html('Name is Required Field');
            return false;
          }


          if(phone == ''){

            $(this).closest('.modal-body').find('.phone').closest('.form-group').find('.errors').html('Phone is Required Field');
            return false;
          }
          var datatopost = { type : type , phone : phone, email :email ,name : name};
          serviceajax(datatopost,base+'Service/add_request' ,'save_request',$(this));

      });

      function serviceajax(datatopost,url,type,$this){
        $.ajax({
                  url: url,
                  type: "post",
                  data: datatopost,
                  beforeSend: function() {
                    console.log("Beforsend")
                  },
                  complete: function(){
                    console.log("complete")
                  },
                  success: function(data){
                    console.log(data);

                    if(data.status == '1'){
                      if(type =='save_request'){
                        $this.closest('.modal-body').find('.success-msg').html('Request Sent! you will be contacted soon.');
                        setTimeout(function(){
                          $(document).find('.close').click();    
                        },2000)
                        
                      }
                    } else if(data.status == '0'){
                      if(type == 'delel'){
                        alert(data.reason);
                      } else if (type == 'save_request'){
                        if(data.reason == 'validation'){
                          $.each(data.errors, function( index, value ) {
                            console.log(value);
                            $(document).find('.'+index).closest('.form-group').find('.errors').html(value);
                          //alert( index + ": " + value );
                        });
                        }
                      }
                    }
                  }
      });

  }


  });
</script>
</body>
</html>
