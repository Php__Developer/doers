<footer>
    <div class="footer_top">
        <div class="container">
            <div class="col-md-3 col-sm-3 col-xs-12"> <a href="#"><img src="assets/images/logo-footer.png" width="182" height="46" alt=""/></a>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="social_icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> </a></li>
                        <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i> </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>INFORMATION</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>SERVICES</h4>
                <ul>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                    <li><a href="#">Lorem Ipsum</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <h4>KEEP IN TOUCH</h4>
                <p class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy tex</p>
                <div class="mailling">
                    <input type="text" placeholder="your email address">
                    <button><i class="fa fa-paper-plane" aria-hidden="true"></i> </button>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom text-center">
    <div class="container">
    Copy right@2017!All rights are reserved
    </div>
    </div>
</footer>