<header>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </button>
						<a class="navbar-brand visible-xs" href="#"><img src="assets/images/logo.png" alt=""/></a>
					</div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li> <a href="index.html">HOME</a> </li>
                            <li class="active"> <a href="product.html">PRODUCTS</a> </li>
                            <li class="logo hidden-xs"><a href="index.html"><img src="assets/images/logo.png" alt=""/></a></li>
                            <li> <a href="about.html">ABOUT US</a> </li>
                            <li> <a href="contact.html">CONTACT US</a> </li>
                            <li     > <a href="services.php">Services</a> </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>