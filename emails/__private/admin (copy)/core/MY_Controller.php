<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
           
           
            //echo $current_route; die;


        }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                echo 'Hello World!';        
                //$this->load->view('welcome_message');
        }

        public function checklogin($data){
           // print_r($data); die;
             $current_route = $this->router->fetch_method();
            $protected_actions = [
                'addexpense',
                'get_expenses',
                'addincome','get_income'
            ];
            //print_r(in_array($current_route, $protected_actions)); die;
            if(in_array($current_route, $protected_actions)){
                if(isset($data['key'])) {
                    $record = $this->User_model->get_one($data['key']);
                    if(count($record) == 0){
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];    
                    } else {
                        $result = ['status' => '1','reason' => 'authorized' , 'userdata' => $record ];    
                    }   
                } else {
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];
                }    
            } else{
                $result = ['status' => '1'];
            }
            return $result;
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

        public function checkadmin($data){
           // print_r($data); die;
             $current_route = $this->router->fetch_method();
            /*$protected_actions = [
                'requests',
            ];*/
            //print_r(in_array($current_route, $protected_actions)); die;
           // if(in_array($current_route, $protected_actions)){
                if(isset($data['key'])) {
                    $record = $this->User_model->get_one($data['key']);
                    if(count($record) == 0){
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];    
                    } else {
                        //print_r($record); die;
                        //echo $record['role_id']; die;
                        if($record['role_id'] == 1 ) {
                            $result = ['status' => '1','reason' => 'authorized' , 'userdata' => $record ];        
                        } else {
                            $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Admin Protected Page' ];    
                        }
                        
                    }   
                } else {
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];
                }    
           /* } else{
                $result = ['status' => '1'];
            }*/
            return $result;
        }




         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            //$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');

            $this->form_validation->set_rules('equipment', 'Equipment', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|min_length[5]|max_length[200]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]|integer',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            //$this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    //echo json_encode(validation_errors());

                /*$arr = array(
                    'field_name_one' => form_error('field_name_one'),
                    'field_name_two' => form_error('field_name_two')
                );*/
                // errors_array()
                //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
                $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'name' => $this->input->post('name'),
                'last_name' => 'None',
                'email' => $this->input->post('email'),
                'equipment' => $this->input->post('equipment'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
               // 'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!','key' => $ref];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
       

        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                 $data = array(
                'email' => $this->input->post('email'),
                'password' =>$this->input->post('password'),
                );
               $result = $this->User_model->login($data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));
        }

          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function addexpense(){
             $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            $this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));    

        }

}
