 <div class="content-wrapper">
        <section class="content-header">
            <h1> Page Header <small>Optional description</small> </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <section class="content container-fluid">
        
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Hover Data Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <?php echo form_open_multipart('product/addproduct',['class' => '' , 'method' => 'post']); ?>
                        <div class="box-body">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Name</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'name' ,'value' => set_value('name')]);?>
                                  <?php echo form_error('name'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Sku</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'sku','value' => set_value('sku')]);?>
                                  <?php echo form_error('sku'); ?>
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Description</label>
                                  <?php echo form_textarea(['class' => 'form-control' ,'name' => 'description','value' => set_value('description')]);?>
                                  <?php echo form_error('description'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Price</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'price','value' => set_value('price')]);?>
                                  <?php echo form_error('price'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Quanitity</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'quantity','value' => set_value('quantity')]);?>
                                  <?php echo form_error('quantity'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Weight</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'weight','value' => set_value('weight')]);?>
                                  <?php echo form_error('weight'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Image</label>
                                  <input type="file" name="image" size="20" class="" />
                                  <?php echo  (isset($image_error)) ? $image_error : ''; ;?>
                                </div>                                                                
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                         <?php echo "</form>";?>   
                    </div>
                </div>
            </div>
        </section>
        
         </section>
    </div>