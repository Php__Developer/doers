 <div class="content-wrapper">
        <section class="content-header">
            <h1> Page Header <small>Optional description</small> </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <section class="content container-fluid">
        
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Hover Data Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Equipments</th>
                                        <th>Address</th>    
                                        <th>Edit</th>
                                        <th>Subscription Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $counter = 1;
                                    if(count($result) > 0){
                                        foreach($result as $data){    
                                 ;?>
                                    <tr>
                                        <td><?php echo $counter++;?></td>
                                        <td><?php echo $data->name ;?></td>
                                        <td><?php echo $data->email ;?></td>
                                        <td><?php echo $data->gender ;?></td>
                                        <td><?php echo $data->equipment ;?></td>
                                        <td><?php echo $data->address ;?></td>

                                        <td>
                                            <?php echo anchor('product/edit/'.$data->id, 'Edit');
                                              ?>
                                        </td>
                                        <td><?php
                                                if($data->has_subscribed == 'No'){
                                                    $date = new DateTime($data->created_at);
                                                    $date->modify("+30 day");
                                                    $validity =  $date->format("Y-m-d H:i:s");
                                                    $today = date("Y-m-d H:i:s");
                                                    if(strtotime($today) > strtotime($validity)){  ?>
                                                        <a href="<?php echo base_url();?>user/activate" class="togglestatus" data-origin='<?php echo $data->id ;?>' data-current='<?php echo $data->has_subscribed ;?>' style=" {{$data->has_subscribed == 'Yes' ? '': 'color:red'}}" data-confirmation="Are you sure to Make Changes?" data-type="user-activation">Expired</a>
                                                    <?php } else { ;?>
                                                         <a href="<?php echo base_url();?>user/activate" class="togglestatus" data-origin='<?php echo $data->id ;?>' data-current='<?php echo $data->has_subscribed ;?>' style=" {{$data->has_subscribed == 'Yes' ? '': 'color:red'}}"  data-confirmation="Are you sure to Make Changes?" data-type="user-activation"> Not Subscribed</a>   
                                                    <?php } 
                                                } else {
                                                    echo "Subscribed";
                                                }
                                         ;?></td>
                                    </tr>
                                <?php }
                                    }
                                ;?>
                                </tbody>
                                    
                            </table>
                            <?php if (isset($links)) { ?>
                                <?php echo $links ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
         </section>
    </div>