<?php

class User_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function get_last_ten_entries()
        {
                $query = $this->db->get('users', 10);
                return $query->result();
        }

        public function insert_entry($data)
        {
                 $this->db->insert('users', $data);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();
                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

         public function get_all()
        {       
                
                $this->db->order_by("id", "desc");
                $query = $this->db->get('users');
                return $query->result();
        }

         public function get_current_page_records($limit, $start)
        {   
            $this->db->limit($limit, $start);
            $query = $this->db->get("users");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {   
                    $users[] = $row;
                }
                return $users;
            }
          
            return false;
        }

         public function get_total()
        {
            return $this->db->count_all("users");
        }

        public function login($data){
            $details = $this->db->get_where('users', array('email' => $data['email']));
            $record_arr = $details->first_row('array');
            if(count($details) > 0){
                $date = new DateTime($record_arr['created_at']);
                $date->modify("+30 day");
                $validity =  $date->format("Y-m-d H:i:s");
                $today = date("Y-m-d H:i:s");
               /* if(strtotime($today) > strtotime($validity)){

                    $result = ['status' => '0'  ,'message' => 'Free Subscription Expired' ];
                } else {
                   if (password_verify($data['password'],$record_arr['password'])) {
                       $result = ['status' => '1' , 'key' => $record_arr['api_key'] ,'message' => 'Logged In Successfully!' ];
                    } else {
                        $result = ['status' => '0'  ,'message' => 'Invalid Username or Password', 'reason' => 'Password' ];
                    } 
                }*/
                if(strtotime($today) > strtotime($validity)){
                    // Subscription Expired 
                    //$result = ['status' => '0'  ,'errors' => ['message' => 'Subscription Expired!'] ];
                        if($record_arr['has_subscribed'] == 'Yes'){
                            // IF USER HAS SUBSCRIPTION
                            //$result = ['status' => '1' , 'key' => $record_arr['api_key'] ,'message' => 'Logged In Successfully!' ];
                            if (password_verify($data['password'],$record_arr['password'])) {
                                unset($record_arr['password']);
                                unset($record_arr['created_at']);
                                unset($record_arr['updated_at']);
                                //unset($details['updated_at']);
                               $result = ['status' => '1' , 'key' => $record_arr['api_key'] ,'message' => 'Logged In Successfully!',
                                    'profile' =>$record_arr
                                ];
                            } else {
                                $result = ['status' => '0'  ,'errors' => ['message' => 'Invalid Username or Password'], 'reason' => 'Password' ];
                            } 
                        } else {
                            // IF USER DOES NOT HAVE SUBSCRIPTION
                            $result = ['status' => '0'  ,'message' => 'Subscription Expired', 'reason' => 'Subscription' ];    
                        }

                } else {
                   if (password_verify($data['password'],$record_arr['password'])) {
                        unset($record_arr['password']);
                        unset($record_arr['created_at']);
                        unset($record_arr['updated_at']);
                        //unset($details['updated_at']);
                       $result = ['status' => '1' , 'key' => $record_arr['api_key'] ,'message' => 'Logged In Successfully!',
                            'profile' =>$record_arr
                        ];
                    } else {
                        $result = ['status' => '0'  ,'errors' => ['message' => 'Invalid Username or Password'], 'reason' => 'Password' ];
                    } 
                }
                
            } else {
                $result = ['status' => '0' ,  'message' => 'Invalid Username or Password', 'reason' => 'Record not found'];
            }
            return $result;

        }


        public function get_one($key){
            $details = $this->db->get_where('users', array('api_key' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function activate($data){
            $details = $this->db->get_where('users', array('id' => $data['origin']));
            $record_arr = $details->first_row('array');
            $date = new DateTime($record_arr['created_at']);
            $date->modify("+365 day");
            $validity =  $date->format("Y-m-d H:i:s");
            $this->db->update('users',['validity' => $validity, 'has_subscribed' => 'Yes'], array('id' => $data['origin']));
            return '1';
        }
}


 ;?>