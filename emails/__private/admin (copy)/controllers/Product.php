<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
            $this->load->library('session');
        }
        
        public function _remap($method, $params = array())
        {   
                $protected_routes = [
                    'requests',
                    'addproduct',
                    'editproduct',
                    'delete',
                    'list',
                    'add',
                    'edit'
                ];
                if(in_array($method, $protected_routes)){
                    if(isset($this->session->api_key)){
                        $result = $this->checkadmin(['key' => $this->session->api_key]);
                        if($result['status'] == '0'){
                            redirect('/');
                        }
                    } else {
                        redirect('/'); 
                    }   
                }
                $this->$method();
        }


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                $this->load->library('session');
                if($this->session->has_userdata('email')){
                    //$this->load->view('home');
                    $data['middle'] = 'home';
                    $this->load->view('template',$data);
                } else {
                    $this->load->helper('form');
                    $this->load->view('login');    
                }
        }

        public function list(){
                $data = $this->Data_model->get_all();
                //echo count($data); die;
                $this->load->library('pagination');

                  $params = array();
                    $limit_per_page = 10;
                    $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $total_records = $this->Data_model->get_total();
             
                    if ($total_records > 0) 
                    {
                        // get current page records
                        $params["result"] = $this->Data_model->get_current_page_records($limit_per_page, $start_index);
                        $config['base_url'] = base_url() . 'index.php/product/list';
                        $config['total_rows'] = $total_records;
                        $config['per_page'] = $limit_per_page;
                        $config["uri_segment"] = 3;
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_link'] = false;
                        $config['last_link'] = false;
                        $config['first_tag_open'] = '<li>';
                        $config['first_tag_close'] = '</li>';
                        $config['prev_link'] = '&laquo';
                        $config['prev_tag_open'] = '<li class="prev">';
                        $config['prev_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo';
                        $config['next_tag_open'] = '<li>';
                        $config['next_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li>';
                        $config['last_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $this->pagination->initialize($config);
                        // build paging links
                        $params["links"] = $this->pagination->create_links();
                    }

                $params['middle'] = 'products/list';
                $this->load->view('template',$params);
        }


        public function edit($id){
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $this->load->helper('form');
            $this->load->library('upload', $config);
            $product = $this->Data_model->get_one($id);
            //print_r($product); die;
            $data['middle'] = 'products/edit';
            $data['result'] = $product;
            //$data['image_error'] = $this->upload->display_errors();
            $this->load->view('template',$data);
            //print_r($product);

        }

        public function editproduct(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('sku', 'Name', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('description', 'Description', 'required|min_length[5]|max_length[200]');
            $this->form_validation->set_rules('price', 'Price', 'required|integer');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|integer');
            $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
            $product = $this->Data_model->get_one($this->input->post('id'));
            $name = date('Ymdhis').md5(rand(999,9999));
            $config['upload_path']          = './images/';
            $config['file_name']          = $name;
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('image')) {   
                //print_r($this->upload->display_errors()); die;
                $image = $product['image'];
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
            } else {
               $img = $this->upload->data(); 
               $image = 'images/'.$img['file_name'];
            }    


           if ($this->form_validation->run() == FALSE) {
                    //$this->load->helper('form');
                   // $this->load->view('login'); 
                    $data['middle'] = 'products/add';
                    $data['result'] = [];
                    $this->load->view('template',$data);
                    //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors() ];
            } /*else if ( ! $this->upload->do_upload('image')) {   
                //print_r($this->upload->display_errors()); die;
                $data['middle'] = 'products/add';
                $data['result'] = [];
                $data['image_error'] = $this->upload->display_errors();
                $this->load->view('template',$data);
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
            }*/  else {
              // $img = $this->upload->data();
               //echo "<pre>";
               //print_r($img); die;
            $data = array(
                'name' => $this->input->post('name'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                'price' => $this->input->post('price'),
                'quantity' => $this->input->post('quantity'),
                'weight' => $this->input->post('weight'),
                'image' => $image
                );
                //Transfering data to Model
                $this->Data_model->update_product($product['id'],$data);
                redirect('product/list');
                    //$data = array('upload_data' => $this->upload->data());

                    //$this->load->view('upload_success', $data);
            } 


        }

        public function add(){
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $this->load->helper('form');
            $this->load->library('upload', $config);
            
            $data['middle'] = 'products/add';
            $data['result'] = [];
            $this->load->view('template',$data);
            //print_r($product);

        }

         public function addproduct(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('sku', 'Name', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('description', 'Description', 'required|min_length[5]|max_length[200]');
            $this->form_validation->set_rules('price', 'Price', 'required|integer');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|integer');
            $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
            $name = date('Ymdhis').md5(rand(999,9999));
            $config['upload_path']          = './images/';
            $config['file_name']          = $name;
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;
            $this->load->library('upload', $config);
             
           if ($this->form_validation->run() == FALSE) {
                    //$this->load->helper('form');
                   // $this->load->view('login'); 
                    $data['middle'] = 'products/add';
                    $data['result'] = [];
                    $this->load->view('template',$data);
                    //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors() ];
            } else if ( ! $this->upload->do_upload('image')) {   
                //print_r($this->upload->display_errors()); die;
                $data['middle'] = 'products/add';
                $data['result'] = [];
                $data['image_error'] = $this->upload->display_errors();
                $this->load->view('template',$data);
                //$error = array('error' => $this->upload->display_errors());
                //$this->load->view('upload_form', $error);
            }  else {
               $img = $this->upload->data();
               //echo "<pre>";
               //print_r($img); die;
            $data = array(
                'name' => $this->input->post('name'),
                'sku' => $this->input->post('sku'),
                'description' => $this->input->post('description'),
                'price' => $this->input->post('price'),
                'quantity' => $this->input->post('quantity'),
                'weight' => $this->input->post('weight'),
                'image' => 'images/'.$img['file_name']
                );
                //Transfering data to Model
                $this->Data_model->insert_entry($data);
                redirect('product/list');
                    //$data = array('upload_data' => $this->upload->data());

                    //$this->load->view('upload_success', $data);
            }
        }

         public function requests(){
                //echo count($data); die;
                $this->load->library('pagination');
                 $this->load->model(array('Request_model'));
                 $formdata = $this->input->post();
                  $params = array();
                    $limit_per_page = 10;
                    $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $total_records = $this->Request_model->get_total();
             
                    if ($total_records > 0) 
                    {
                        // get current page records
                        $params["result"] = $this->Request_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                        $config['base_url'] = base_url() . 'index.php/product/requests';
                        $config['total_rows'] = $total_records;
                        $config['per_page'] = $limit_per_page;
                        $config["uri_segment"] = 3;
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_link'] = false;
                        $config['last_link'] = false;
                        $config['first_tag_open'] = '<li>';
                        $config['first_tag_close'] = '</li>';
                        $config['prev_link'] = '&laquo';
                        $config['prev_tag_open'] = '<li class="prev">';
                        $config['prev_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo';
                        $config['next_tag_open'] = '<li>';
                        $config['next_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li>';
                        $config['last_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $this->pagination->initialize($config);
                        // build paging links
                        $params["links"] = $this->pagination->create_links();
                    }

                $params['middle'] = 'products/requests';
                $this->load->view('template',$params);
        }


        public function delete($id){
               $this->Data_model->delete($id);  
               redirect('product/list');
        }
        


}
