<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends My_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model','Income_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->model(array('User_model'));
                       
            $this->load->helper('url');
            $this->load->library('common');
            //require_once APPPATH.'third_party/PHPExcel.php';
            //$this->excel = new PHPExcel(); 
            //$this->LogData->
        }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                echo 'Hello World!';        
                //$this->load->view('welcome_message');
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            //$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');

            $this->form_validation->set_rules('equipment', 'Equipment', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|min_length[5]|max_length[200]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]|integer',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            //$this->form_validation->set_rules('gender', 'Gender', 'required');
            if ($this->form_validation->run() == FALSE) {
                    //echo json_encode(validation_errors());

                /*$arr = array(
                    'field_name_one' => form_error('field_name_one'),
                    'field_name_two' => form_error('field_name_two')
                );*/
                // errors_array()
                //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
                $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
              //echo $this->input->post('password');
              $p =  password_hash($this->input->post('password'), PASSWORD_DEFAULT); 
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $date = new DateTime();
                $date->modify("+30 day");
                $validity =  $date->format("Y-m-d H:i:s");

                $data = array(
                'name' => $this->input->post('name'),
                'last_name' => 'None',
                'email' => $this->input->post('email'),
                'equipment' => $this->input->post('equipment'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
               // 'gender' => $this->input->post('gender'),
                'password' => $p,
                'role_id' => 2,
                'api_key' => $ref,
                'validity' => $validity

                );
                
                
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                unset($data['password']);
                unset($data['last_name']);
                unset($data['gender']);
                $result = ['status' => '1','message' => 'Registered Successfully!','key' => $ref,
                            'profile' => $data
                ];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
       

        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                 $data = array(
                'email' => $this->input->post('email'),
                'password' =>$this->input->post('password'),
                );
               $result = $this->User_model->login($data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function addexpense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $user_id =  $checklogin['userdata']['id'];

                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    $this->form_validation->set_rules('machines_maintenance', 'Machines Maintenance', 'required|integer');
                    $this->form_validation->set_rules('diesel', 'Diesel', 'required|integer');
                    $this->form_validation->set_rules('farm_milk_consumption', 'Milk Consumption In Farm', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('machines_maintenance'),$this->input->post('diesel'),
                                $this->input->post('farm_milk_consumption'),$this->input->post('others')
                            ));
                        $data = array(
                            'user_id' => $user_id,
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total,
                            'machines_maintenance' => $this->input->post('machines_maintenance'),
                            'diesel' => $this->input->post('diesel'),
                            'farm_milk_consumption' => $this->input->post('farm_milk_consumption'),
                        );
                        $this->Expense_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Expense Added Successfully!'];
                }
             }
            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function get_expenses(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Expense_model->get_total($checklogin['userdata']);
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Expense_model->get_all($checklogin['userdata']);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

         /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_expense(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Expense_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


         /*
        |--------------------------------------------------------------------------
        | Function : update Expense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

         public function update_expense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             //print_r($checklogin); die;
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    
                    $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('others')
                            ));
                        //echo $user_id; die;
                        $data = array(
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total
                        );
                        $this->Expense_model->update_entry($data);
                        $result = ['status' => '1','message' => 'Expense Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }





        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 




          public function addincome(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  // print_r( $checklogin['userdata']) ; die;
                   $user_id =  $checklogin['userdata']['id'];
                    //$this->form_validation->set_rules('user_id', 'User Name', 'required');
                    $this->form_validation->set_rules('income_date', 'Income Date', 'required');
                    $this->form_validation->set_rules('milk_sale_rate', 'Milk Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('manure_sale_rate', 'Manuare Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('milk_sale', 'Milk Sale', 'required|integer');
                    $this->form_validation->set_rules('manure_sale', 'Manuare Sale', 'required|integer');
                    $this->form_validation->set_rules('others', 'others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $total = array_sum(array(
                                $this->input->post('milk_sale_rate') * $this->input->post('milk_sale'),
                                $this->input->post('manure_sale_rate') * $this->input->post('manure_sale'),
                                $this->input->post('others')
                            ));

                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        $data = array(
                        'user_id' => $user_id,
                        'milk_sale_rate' => $this->input->post('milk_sale_rate'),
                        'manure_sale_rate' => $this->input->post('manure_sale_rate'),
                        'milk_sale' => $this->input->post('milk_sale'),
                        'manure_sale' => $this->input->post('manure_sale'),
                        'others' => $this->input->post('others'),
                        'total' => $total,
                        'income_date' => $date,
                        );
                        //Transfering data to Model
                        $this->Income_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Income Added Successfully!'];
                }
             }
          

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        public function get_income(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                            $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Income_model->get_total($formdata, $checklogin['userdata']);
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    //$params["result"] = $this->Income_model->get_current_page_records($limit_per_page, $start_index,$formdata,$checklogin['userdata']);
                                    $params["result"] = $this->Income_model->get_all($checklogin['userdata']);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }

                    }
             }

             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }


           /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 
        
        public function get_one_income(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Income_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


         /*
        |--------------------------------------------------------------------------
        | Function : update Expense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

         public function update_income(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  // print_r( $checklogin['userdata']) ; die;
                   $user_id =  $checklogin['userdata']['id'];
                    //$this->form_validation->set_rules('user_id', 'User Name', 'required');
                   $this->form_validation->set_rules('id', 'ID', 'required');
                    $this->form_validation->set_rules('income_date', 'Income Date', 'required');
                    $this->form_validation->set_rules('milk_sale_rate', 'Milk Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('manure_sale_rate', 'Manuare Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('milk_sale', 'Milk Sale', 'required|integer');
                    $this->form_validation->set_rules('manure_sale', 'Manuare Sale', 'required|integer');
                    $this->form_validation->set_rules('others', 'others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $total = array_sum(array(
                                $this->input->post('milk_sale_rate') * $this->input->post('milk_sale'),
                                $this->input->post('manure_sale_rate') * $this->input->post('manure_sale'),
                                $this->input->post('others')
                            ));

                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        $data = array(
                        'user_id' => $user_id,
                        'milk_sale_rate' => $this->input->post('milk_sale_rate'),
                        'manure_sale_rate' => $this->input->post('manure_sale_rate'),
                        'milk_sale' => $this->input->post('milk_sale'),
                        'manure_sale' => $this->input->post('manure_sale'),
                        'others' => $this->input->post('others'),
                        'total' => $total,
                        'income_date' => $date,
                        );
                        //Transfering data to Model
                        $this->Income_model->update_entry($data);
                        $result = ['status' => '1','message' => 'Updated Successfully!'];
                }
             }
          

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }

        public function add_request(){
             $this->load->library('form_validation');
            $this->load->model(array('Request_model'));
                $formdata = $this->input->post();
               // print_r($formdata); die;
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('phone', 'Phone', 'required|integer');
                    $this->form_validation->set_rules('type', 'Type', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $data = array(
                        'name' =>$this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'type' => $this->input->post('type'),
                        );
                        //Transfering data to Model
                        $this->Request_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Request Added Successfully!'];
                }

                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    


        }


             /*
        |--------------------------------------------------------------------------
        | Function : add_cattle
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function add_cattle(){
             $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $this->load->model(array('History_model'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('BreedingProcess'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|max_length[250]');
                    $this->form_validation->set_rules('dam_id', 'Dam ID', 'required|max_length[250]');
                    $this->form_validation->set_rules('sire_id', 'Sire ID', 'required|max_length[250]');
                    $this->form_validation->set_rules('breed', 'Breed', 'required|max_length[250]');
                    $this->form_validation->set_rules('type', 'Cattle Type', 'required|max_length[250]');
                    $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
                    $this->form_validation->set_rules('dop', 'Date Of Purchase', 'required');
                    $this->form_validation->set_rules('purchase_price', 'Purchase Price', 'required|integer');            
                    $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
                    $this->form_validation->set_rules('is_pregnant','Is Cow Pregnant', 'required');                    
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        // FINDING RECORD FROM DB ON THE BASIS OF CURRENT TAG
                        //$tagidcheck = $this->Cattle_model->get_one_from_tag($this->input->post('tag_id'));
                        $tagidcheck = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('tag_id'),$checklogin['userdata']);
                        //print_r($user_id); die;
                        if(count($tagidcheck) > 0 ){
                          //WE NEED TO MAKE TAG ID UNIQUE USERWISE
                          //IF WE FIND THAT DB TAGID RECORD CATTLE OWNER == CURRENT USER
                          //WHICH MEANS CURRENT USER HAS CATTLE WITH THE SAME TAG ALREADY
                          // WE WILL GENERATE AN ERROR
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => ['tag_id' => 'You have already alloted that tag to one of your cattles'] ];
                        } else {
                            //IN CASE OF CALF
                            if($this->input->post('type') == 'Calf'){
                                $damidcheck = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('dam_id'),$checklogin['userdata']);
                                if(count($damidcheck) == 0){
                                    $result = ['status' => '0','reason' => 'validation' , 'errors' => ['dam_id' => 'Please Enter Correct Dam ID (Mother tag ID)'] ];    
                                     return $this->output
                                    ->set_content_type('application/json')
                                    ->set_status_header(200)
                                    ->set_output(json_encode($result) );
                                }
                            }

                            //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $ai_date = !empty($this->input->post('ai_date')) ?  date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s') : '';

                        $data = array(
                            'tag_id' => $this->input->post('tag_id'),
                            'dam_id' => $this->input->post('dam_id'),
                            'dob' => $this->input->post('dob'),
                            'dop' => $this->input->post('dop'),
                            'purchase_price' => $this->input->post('purchase_price'),
                            'weight' => $this->input->post('weight'),
                            'ai_date' => $ai_date,
                            'is_pregnant' => $this->input->post('is_pregnant'),
                            'calving_date' => empty($this->input->post('calving_date')) ? '' : $this->input->post('calving_date'),
                            'sale_date' => empty($this->input->post('sale_date')) ? '' : $this->input->post('sale_date'),
                            'death_date' => empty($this->input->post('death_date')) ? '' : $this->input->post('death_date'),
                            'sale_price' => empty($this->input->post('sale_price')) ? 0 : $this->input->post('sale_price'),
                            'owner_id ' => $user_id,
                            'sire_id' => $this->input->post('sire_id'),
                            'lactation' => empty($this->input->post('lactation')) ? 0 : $this->input->post('lactation'),
                            'per_day_milk' => empty($this->input->post('per_day_milk')) ? 0 : $this->input->post('per_day_milk'),
                            'type' => $this->input->post('type'),
                            'breed' => $this->input->post('breed'),
                        );
                       $insert_id =  $this->Cattle_model->insert_entry($data);
                        if(!empty($ai_date) && $this->input->post('type') !== 'Calf'){
                             // AI PROCESS WILL TAKE EFFECT ONLY IF NEWLY ADDED RECORD CATTLE IS NOT CALF                              
                             $default = $this->BreedingProcess->get_one(1);
                              $data1 = $default;
                              $data1['ai_on'] = 0;
                              $data1['ai_date'] = date_create($ai_date)->format('Y-m-d H:i:s');
                              $data1['cattle_id'] = $insert_id;
                              $data1['current_state'] = 'ai';
                              $event_date = new DateTime($data['ai_date']);
                              $event_type = 'ai';
                              $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                              $this->UpcomingEvent_model->delete_by_type($insert_id,'ai');  
                              $alertdata = [
                                'cattle_id' => $insert_id,
                                'event_type' => $event_type,
                                'message' => $event_message,
                                'event_date' => $event_date->format('Y-m-d 00:00:00')
                               ];
                                //print_r($event_date); die;
                              $this->UpcomingEvent_model->insert_entry($alertdata); 
                              if(!empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                              } else {
                                  $first_pd_type = 'UltraSound';
                              }

                              if(empty($this->input->post('first_pd_on'))){
                                  $first_pd_on = $default['first_pd_on'];
                                  if($first_pd_type == 'UltraSound'){
                                    $first_pd_on = $default['first_pd_on'];
                                  }                                
                              } else {
                                  $first_pd_on = $this->input->post('first_pd_on');
                              }

                              $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                              //print_r($event_date); die;
                              $event_date->modify("+".$first_pd_on." day");
                              $event_type = 'first_pd';
                              $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');

                              //print_r($event_message); die;

                              $data1['first_pd_type'] = $first_pd_type;
                              $data1['first_pd_on'] = $first_pd_on;
                              $data1['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                            

                            $result =['status' => '1','result' => $data ];   

                            $alertdata = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                              ];

                            $this->UpcomingEvent_model->insert_entry($alertdata);    
                            unset($data1['id']);

                            $this->BreedingProcess->insert_entry($data1);
                            $default['ai_date'] = $ai_date;
                            $default['cattle_id'] = $insert_id;
                            $this->update_event($default,$event_type, $event_date, $insert_id,[]);                             
                         } else if($this->input->post('type') == 'Calf'){
                           //IF NEW RECORD IS A CALF
                           //CREATING AN EVENT THAT CALF WILL BE ON HEAT ON 15 MONTHS FROM BIRTH OF CALF FOR HEAT
                          $event_date = new DateTime(date_create($this->input->post('dob'))->format('Y-m-d H:i:s'));
                          $event_date->modify("+450 day");
                          $event_message = 'Heat is going to take place on '.$event_date->format('Y-m-d');
                             $alertdata = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => 'heat',
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                              ];
                            $this->UpcomingEvent_model->insert_entry($alertdata);   
                         }

                          $result = ['status' => '1','message' => 'Cattle Data Added Successfully!'];
                        }
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }



               /*
        |--------------------------------------------------------------------------
        | Function : update_cattle
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function update_cattle(){
             $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                   $cattle = $this->Cattle_model->get_one($this->input->post('id'));

                   if($cattle['tag_id'] !== $this->input->post('tag_id')){
                    // IF TAG ID MATCHES OTHER ID THAN THE CURRENT RECORD IMPLEMENT UNIQUE
                    $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|max_length[250]');
                   } else {
                     $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|max_length[250]');
                   }
                    $this->form_validation->set_rules('dam_id', 'Dam ID', 'required|max_length[250]');
                    $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
                    $this->form_validation->set_rules('dop', 'Date Of Purchase', 'required');
                    $this->form_validation->set_rules('purchase_price', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
                    $this->form_validation->set_rules('id', 'ID', 'required|integer');
                    $this->form_validation->set_rules('sire_id', 'Sire ID', 'required|max_length[250]');
                    $this->form_validation->set_rules('breed', 'Breed', 'required|max_length[250]');
                    $this->form_validation->set_rules('type', 'Cattle Type', 'required|max_length[250]');
                    $this->form_validation->set_rules('is_pregnant','Is Cow Pregnant', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                         //Setting values for tabel columns
                        $tagidcheck = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('tag_id'),$checklogin['userdata']);
                        //print_r($tagidcheck); die;
                        if(count($tagidcheck) > 0 && $tagidcheck['id'] !== $this->input->post('id') ){
                          //WE NEED TO MAKE TAG ID UNIQUE USERWISE
                          //IF WE FIND THAT DB TAGID RECORD CATTLE OWNER == CURRENT USER
                          //WHICH MEANS CURRENT USER HAS CATTLE WITH THE SAME TAG ALREADY
                          // WE WILL GENERATE AN ERROR
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => ['tag_id' => 'You have already alloted that tag to one of your cattles'] ];
                        } else {
                           $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $data = array(
                            'tag_id' => $this->input->post('tag_id'),
                            'dam_id' => $this->input->post('dam_id'),
                            'dob' => $this->input->post('dob'),
                            'dop' => $this->input->post('dop'),
                            'purchase_price' => $this->input->post('purchase_price'),
                            'weight' => $this->input->post('weight'),
                            'ai_date' => $this->input->post('ai_date'),
                            'is_pregnant' => $this->input->post('is_pregnant'),
                            'calving_date' => $this->input->post('calving_date'),
                            'sale_date' => $this->input->post('sale_date'),
                            'death_date' => $this->input->post('death_date'),
                            'sale_price' => empty($this->input->post('sale_price')) ? 0 : $this->input->post('sale_price'),
                            'owner_id ' => $user_id,
                            'sire_id' => $this->input->post('sire_id'),
                            'lactation' => (empty($this->input->post('lactation')) || $this->input->post('type') == 'Calf')  ? 0 : $this->input->post('lactation'),
                            'per_day_milk' => (empty($this->input->post('per_day_milk')) || $this->input->post('type') == 'Calf') ? 0 : $this->input->post('per_day_milk'),
                            'type' => $this->input->post('type'),
                            'breed' => $this->input->post('breed'),
                        );
                       // $this->Cattle_model->insert_entry($data);
                        
                        $ai_date = !empty($this->input->post('ai_date')) ?  date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s') : '';
                        $insert_id = $this->input->post('id');
                        if(!empty($ai_date) && $this->input->post('type') !== 'Calf'){                              
                             $default = $this->BreedingProcess->get_one(1);
                              $exists = $this->BreedingProcess->get_one_by_cattle_id($this->input->post('id'));
                              if(count($exists) >0){                                    
                                    $data1 = $exists;
                                    $already = $exists; // Being Lazy
                                    if((date_create($formdata['ai_date'])->format('Y-m-d') !== date_create($already['ai_date'])->format('Y-m-d') ) || $cattle['type'] == 'Calf'){                                  
                                        //WE WILL MAKE CHANGES ONLY IF AI DATE IS DIFFERENT FROM PREVIOUSE ONE
                                        $data1['ai_on'] = 0;
                                        $data1['ai_date'] = date_create($ai_date)->format('Y-m-d H:i:s');
                                        $data1['cattle_id'] = $insert_id;
                                        $data1['current_state'] = 'ai';
                                        $event_date = new DateTime($data['ai_date']);
                                        $event_type = 'ai';
                                        $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                                        $this->UpcomingEvent_model->delete_by_type($insert_id,'ai');  
                                        $alertdata = [
                                          'cattle_id' => $insert_id,
                                          'event_type' => $event_type,
                                          'message' => $event_message,
                                          'event_date' => $event_date->format('Y-m-d 00:00:00')
                                         ];
                                        $this->UpcomingEvent_model->insert_entry($alertdata); 
                                        if(!empty($this->input->post('first_pd_type'))){
                                          $first_pd_type = $this->input->post('first_pd_type');
                                        } else {
                                            $first_pd_type = 'UltraSound';
                                        }
                                        if(empty($this->input->post('first_pd_on'))){
                                            $first_pd_on = $default['first_pd_on'];
                                            if($first_pd_type == 'UltraSound'){
                                              $first_pd_on = $default['first_pd_on'];
                                            }                                
                                        } else {
                                            $first_pd_on = $this->input->post('first_pd_on');
                                        }
                                        $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                                        $event_date->modify("+".$first_pd_on." day");
                                        $event_type = 'first_pd';
                                        $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');
                                        $data1['first_pd_type'] = $first_pd_type;
                                        $data1['first_pd_on'] = $first_pd_on;
                                        $data1['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                                        $result =['status' => '1','result' => $data ];
                                        $alertdata = [
                                              'cattle_id' => $insert_id,
                                              'event_type' => $event_type,
                                              'message' => $event_message,
                                              'event_date' => $event_date->format('Y-m-d')
                                           ];
                                         $this->UpcomingEvent_model->delete_by_type($insert_id,'first_pd');  
                                         $this->UpcomingEvent_model->insert_entry($alertdata);   
                                         $this->update_event($data1,$event_type, $event_date, $insert_id,[]);                             
                                          } //ENDIF DATE AI DATE COMPARISON
                                   } else{ // ENDIF BREEDING PROCESS RECORD CHECKING
                                      //IF THERE IS NO RECORD FOUND IN THE BREEDING PROCESS TABLE
                                      // WE WILL BE MAKING ENTERIES IN THE BREEDING PROCESS TABLE 
                                      // WE WILL BE MAKING ENTERIES IN THE UPCOMING EVENTS TABLE
                                        $data1 = $default;
                                        $data1['ai_on'] = 0;
                                        $data1['ai_date'] = date_create($ai_date)->format('Y-m-d H:i:s');
                                        $data1['cattle_id'] = $insert_id;
                                        $data1['current_state'] = 'ai';
                                        $event_date = new DateTime($data['ai_date']);
                                        $event_type = 'ai';
                                        $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                                        $this->UpcomingEvent_model->delete_by_type($insert_id,'ai');  
                                        $alertdata = [
                                          'cattle_id' => $insert_id,
                                          'event_type' => $event_type,
                                          'message' => $event_message,
                                          'event_date' => $event_date->format('Y-m-d 00:00:00')
                                         ];
                                        $this->UpcomingEvent_model->insert_entry($alertdata); 
                                        if(!empty($this->input->post('first_pd_type'))){
                                          $first_pd_type = $this->input->post('first_pd_type');
                                        } else {
                                            $first_pd_type = 'UltraSound';
                                        }
                                        if(empty($this->input->post('first_pd_on'))){
                                            $first_pd_on = $default['first_pd_on'];
                                            if($first_pd_type == 'UltraSound'){
                                              $first_pd_on = $default['first_pd_on'];
                                            }                                
                                        } else {
                                            $first_pd_on = $this->input->post('first_pd_on');
                                        }
                                        $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                                        $event_date->modify("+".$first_pd_on." day");
                                        $event_type = 'first_pd';
                                        $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');
                                        $data1['first_pd_type'] = $first_pd_type;
                                        $data1['first_pd_on'] = $first_pd_on;
                                        $data1['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                                        $result =['status' => '1','result' => $data ];
                                        $alertdata = [
                                              'cattle_id' => $insert_id,
                                              'event_type' => $event_type,
                                              'message' => $event_message,
                                              'event_date' => $event_date->format('Y-m-d')
                                           ];
                                         $this->UpcomingEvent_model->delete_by_type($insert_id,'first_pd');  
                                         $this->UpcomingEvent_model->insert_entry($alertdata);   
                                         $this->update_event($data1,$event_type, $event_date, $insert_id,[]); 
                                        
                                    }
                                        //$current_state = $already['current_state'];
                                        $result =['status' => '1' ,'message' => 'Updated Successfully!' ]; //IF NOTHING CHANGED SENDING STATUS AS IT IS
                                        $data1['ai_date'] = $ai_date;
                                        $data1['cattle_id'] = $insert_id;
                            if(count($exists) >0){
                                $this->BreedingProcess->update_entry_by_cattle_id($insert_id,$data1);
                              } else{
                                unset($data1['id']);
                                $this->BreedingProcess->insert_entry($data1);                            
                              }
                         } // IF AI DATE IS NOT EMPTY
                        $this->Cattle_model->update_entry( $this->input->post('id'), $data);
                        $result = ['status' => '1','message' => 'Cattle Updated Successfully!']; 
                      } // IF TAG ID UNIQUE USERWISE VALIDATION PASSES
                } // ENDIF FORM VALIDATION PASSES
             } //ENDIF LOGIN CHECK 
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }


        /*
        |--------------------------------------------------------------------------
        | Function : get_cattle_from_tag
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


          public function get_cattle_from_tag(){
            $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        //print_r($checklogin['userdata']); die;
                        //$expense = $this->Cattle_model->get_one_from_tag($this->input->post('tag_id'));
                         $cattle = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('tag_id'),$checklogin['userdata']);

                        if(count($cattle) > 0){
                            $result =['status' => '1','result' => $cattle];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


           /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function get_cattles(){
            $this->load->library('form_validation');
            $this->load->model(array('Cattle_model'));
            $formdata = $this->input->post();
            $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Cattle_model->get_total($formdata,$checklogin['userdata']);
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Cattle_model->get_all($checklogin['userdata']);
                                    $config['base_url'] = base_url() . '/get_cattles';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => [],'total' => 0 ];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

        /*
        |--------------------------------------------------------------------------
        | Function : get_one_income
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_cattle(){
            $this->load->library('form_validation');
            $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Cattle_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '0','result' => 'Cattle record does not exist' ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function products(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 2;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Data_model->get_total();
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Data_model->get_all();
                                  /*  $params["result"] = $this->Data_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);*/
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }


          /*
        |--------------------------------------------------------------------------
        | Function : get_one_product
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_product(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Data_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


        public function get_breeding_process(){
           $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('History_model'));
             $this->load->model(array('Cattle_model'));
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        //$expense = $this->BreedingProcess->get_one($this->input->post('id'));
                        $cattle = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('cattle_id'),$checklogin['userdata']);
                        //print_r($cattle); die;
                        if(count($cattle) > 0){
                            $expense = $this->BreedingProcess->get_one_by_cattle_id($cattle['id']);
                            $all_history = $this->History_model->get_all($cattle['id']);
                            if(count($expense) > 0){
                                 $expense['history'] = count($all_history) ? $all_history : [];
                                $result =['status' => '1','result' => $expense];            
                            } else {
                                $expense = $this->BreedingProcess->get_one(1);

                                $expense['id'] = 0;
                                $expense['cattle_id'] = $cattle['id'];
                                $expense['ai_date'] = date('Y-m-d');
                                $expense['history'] = [];
                                $result =['status' => '1','result' => $expense ];            
                            }
                        } else {
                            $result = ['status' => '0','reason' => 'validation' , 'errors' => ['cattle_id' => 'Record Does not Exist' ] ];
                        }
                                                
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

        public function set_breeding_process(){
            $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                   //$cattle = $this->BreedingProcess->get_one($this->input->post('id'));
                   
                    $this->form_validation->set_rules('ai_on', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('first_us_pd_on', 'First Ultra Sound PD Date', 'required');
                    $this->form_validation->set_rules('first_manual_pd_on', 'First Manual PD Date', 'required');
                    $this->form_validation->set_rules('second_us_pd_on', 'Second Ultra Sound PD Date', 'required');
                    $this->form_validation->set_rules('second_manual_pd_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('dry_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('steam_up_on', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('delivery_on', 'Weight', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                       // $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $data = array(
                            'ai_on' => $this->input->post('ai_on'),
                            'first_us_pd_on' => $this->input->post('first_us_pd_on'),
                            'first_manual_pd_on' => $this->input->post('first_manual_pd_on'),
                            'second_us_pd_on' => $this->input->post('second_us_pd_on'),
                            'second_manual_pd_on' => $this->input->post('second_manual_pd_on'),
                            'cattle_id' => $this->input->post('cattle_id'),
                            'dry_on' => $this->input->post('dry_on'),
                            'steam_up_on' => $this->input->post('steam_up_on'),
                            'delivery_on' => $this->input->post('delivery_on'),
                        );
                       // $this->Cattle_model->insert_entry($data);
                        if($this->input->post('id') == 0){
                          $this->BreedingProcess->insert_entry($data);
                        } else {
                          $this->BreedingProcess->update_entry($this->input->post('id'), $data);
                        }
                        
                        $result = ['status' => '1','message' => 'Breeding Process Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }


        public function update_breeding_process(){
            //'ai','first_pd','second_pd','pregnancy_confirmation','dry','steam_up','delivery',
              $this->load->library('form_validation');
             $post = $this->input->post();
             $checklogin = $this->checklogin($post);             
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('History_model'));
             $this->load->model(array('Cattle_model'));
             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                //print_r($post); die;
                    //$this->form_validation->set_rules('ai_on', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('ai_date', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('first_pd_on', 'First  PD Days', 'required');
                    $this->form_validation->set_rules('first_pd_type', 'First PD Type', 'required');
                    $this->form_validation->set_rules('second_pd_on', 'Second  PD Date', 'required');
                    $this->form_validation->set_rules('second_pd_type', 'Second  PD Type', 'required');
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('dry_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('steam_up_on', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('delivery_on', 'Weight', 'required|integer');

                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {

                       $cattle_id = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('cattle_id'),$checklogin['userdata'])['id'];
                       //echo $cattle_id; die;
                        $data = array(
                                      'ai_on' => 0,
                                      'first_pd_on' => $this->input->post('first_pd_on'),
                                      'first_pd_type' => $this->input->post('first_pd_type'),
                                      'second_pd_on' => $this->input->post('second_pd_on'),
                                      'second_pd_type' => $this->input->post('second_pd_type'),
                                      'cattle_id' => $cattle_id,
                                      'dry_on' => $this->input->post('dry_on'),
                                      'steam_up_on' => $this->input->post('steam_up_on'),
                                      'delivery_on' => $this->input->post('delivery_on'),
                                  );
                       
                        $formdata = $this->input->post();
                        $already = $this->BreedingProcess->get_one_by_cattle_id($cattle_id);
                        $default = $this->BreedingProcess->get_one(1);
                        if(count($already) == 0){
                            $already = $default;
                        }
                        //echo "if"; die;
                        if(count($already) > 0){
                            //echo "already"; die;
                            // IF BREEDING PROCESS IS ALREADY THERE
                            $current_state = $already['current_state'];
                            $result =['status' => '1' ,'message' => 'Updated Successfully!' ]; //IF NOTHING CHANGED SENDING STATUS AS IT IS
                            if(date_create($formdata['ai_date'])->format('Y-m-d') !== date_create($already['ai_date'])->format('Y-m-d')){
                              // IF POSTED AI DATE DOES NOT MATCHED THE DATE IN DB
                              // DELETED OLD BREEDING PROCESS RECORD AND WILL CREATE NEW
                              $this->BreedingProcess->delete($already['id']);
                              $data['ai_on'] = 0;
                              $data['ai_date'] = date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s');
                              $data['cattle_id'] = $cattle_id;
                              $data['current_state'] = 'ai';
                              $event_date = new DateTime($data['ai_date']);
                              $event_type = 'ai';
                              $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d');                               


                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'ai');  
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'first_pd');  
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'second_pd'); 
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'dry');  
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'steam_up');  
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'delivery');  

                              $alertdata = [
                                'cattle_id' => $cattle_id,
                                'event_type' => $event_type,
                                'message' => $event_message,
                                'event_date' => $event_date->format('Y-m-d 00:00:00')
                               ];
                                //print_r($event_date); die;
                              $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('ai',$cattle_id);
                              $this->UpcomingEvent_model->delete($u['id']); 
                              $this->UpcomingEvent_model->insert_entry($alertdata); 

                              //INSERTING RECORD FOR THE HEAT EVENT
                              $event_date1 = new DateTime($data['ai_date']);
                                $event_date1->modify("-1 day");
                                $event_message1 = 'Heat is going to take place on '.$event_date1->format('Y-m-d');
                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => 'heat',
                                  'message' => $event_message1,
                                  'event_date' => $event_date1->format('Y-m-d')
                                ];
                                //DELETING OLD HEAT EVENT RECORD
                                $heat = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('heat',$cattle_id);
                                $this->UpcomingEvent_model->delete($heat['id']); 
                                //INSERTING HEAT EVENT IN DB
                                $this->UpcomingEvent_model->insert_entry($alertdata);

                              if(!empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                              } else {
                                  $first_pd_type = 'UltraSound';
                              }

                              if(empty($this->input->post('first_pd_on'))){
                                  $first_pd_on = $default['first_pd_on'];
                                  if($first_pd_type == 'UltraSound'){
                                    $first_pd_on = $default['second_pd_on'];
                                  }                                
                              } else {
                                  $first_pd_on = $this->input->post('first_pd_on');
                              }
                              
                              $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                              $event_date->modify("+".$first_pd_on." day");
                              $event_type = 'first_pd';
                              $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');

                              $data['first_pd_type'] = $first_pd_type;
                              $data['first_pd_on'] = $first_pd_on;
                              $data['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                              $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('first_pd',$cattle_id);
                              $this->UpcomingEvent_model->delete($u['id']);
                               $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                  ]; 
                              

                              $this->UpcomingEvent_model->insert_entry($alertdata);
                              $this->BreedingProcess->insert_entry($data);
                              $this->update_event($post,$event_type, $event_date, $cattle_id,[]);
                              $result =['status' => '1','result' => $data ]; 
                            } else {
                              // IF POSTED AI DATE MATCHED THE DATE IN DB
                              // MEANS BREEDING PROCESSING IS MOVING FORWARD
                              if( $post['first_pd_on'] !== $already['first_pd_on']){
                                // IF AI IS DONE BUT FIRST PD CHANGED BEFORE GETTING DONE
                                $first_pd_type = $post['first_pd_type'];
                                if(empty($this->input->post('first_pd_on'))){
                                    $first_pd_on = $default['first_manual_pd_on'];
                                    if($first_pd_type == 'UltraSound'){
                                      $first_pd_on = $default['first_pd_on'];
                                    }                                
                                } else {
                                    $first_pd_on = $this->input->post('first_pd_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('first_pd',$cattle_id);
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$first_pd_on." day");
                                $event_type = 'first_pd';
                                $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');
                                //SAVING DATA TO BREEDING TABLE
                                $data['first_pd_on'] = $first_pd_on;
                                $data['first_pd_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $cattle_id;
                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                  ];
                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                //$this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 

                              if($post['second_pd_on'] !== $already['second_pd_on']) {
                                // IF AI IS DONE BUT Second PD CHANGED BEFORE GETTING DONE
                                $second_pd_type = $post['second_pd_type'];

                                if(empty($this->input->post('second_pd_on'))){
                                    $second_pd_on = $default['second_manual_pd_on'];
                                    if($second_pd_type == 'UltraSound'){
                                      $second_pd_on = $default['second_pd_on'];
                                    }                                
                                } else {
                                    $second_pd_on = $this->input->post('second_pd_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('second_pd',$cattle_id);
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$second_pd_on." day");
                                $event_type = 'second_pd';
                                $event_message = 'Second PD is going to take place on '.$event_date->format('Y-m-d');

                                $data['second_pd_on'] = $second_pd_on;
                                $data['second_pd_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $cattle_id;


                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                            ];
                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                //$this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 

                              if( $post['dry_on'] !== $already['dry_on']) {
                                  $dry_on = $post['dry_on'];
                                if(empty($this->input->post('dry_on'))){
                                    $dry_on = $default['dry_on'];
                                } else {
                                    $dry_on = $this->input->post('dry_on');
                                }
                               // print_r($dry_on); die;
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('dry',$cattle_id);
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$dry_on." day");
                                $event_type = 'dry';
                                $event_message = 'Putting Cattle on Dry is going to take place on '.$event_date->format('Y-m-d'); 

                                $data['dry_on'] = $dry_on;
                                $data['dry_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $cattle_id;

                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                ];

                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                               // $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 
                               if( $post['steam_up_on'] !== $already['steam_up_on']) {
                                  $steam_up_on = $post['steam_up_on'];
                                if(empty($this->input->post('steam_up_on'))){
                                    $steam_up_on = $default['steam_up_on'];
                                } else {
                                    $steam_up_on = $this->input->post('steam_up_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('steam_up',$cattle_id);
                                if(count($u) > 0){
                                  $this->UpcomingEvent_model->delete_by_type($cattle_id,'steam_up');  
                                }
                                
                                $event_date = new DateTime($already['ai_date']);                                
                                $event_date->modify("+".$steam_up_on." day");
                                //print_r($event_date); die;
                                $event_type = 'steam_up';
                                $event_message = 'Putting Cattle on Steam  is going to take place on  '.$event_date->format('Y-m-d'); 

                                $data['steam_up_on'] = $steam_up_on;
                                $data['steam_up_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $cattle_id;

                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                ];                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                //$this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
    
                              } 

                               if( $post['delivery_on'] !== $already['delivery_on']) {
                                  $delivery_on = $post['delivery_on'];
                                if(empty($this->input->post('delivery_on'))){
                                    $delivery_on = $default['delivery_on'];
                                } else {
                                    $delivery_on = $this->input->post('delivery_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$cattle_id);
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$delivery_on." day");
                                $event_type = 'delivery';
                                $event_message = 'Putting Calf Delivery is going to take place on '.$event_date->format('Y-m-d'); 
                                //$this->UpcomingEvent_model->delete_by_type($this->input->post('cattle_id'),'steam_up');  
                                $data['delivery_on'] = $delivery_on;
                                $data['delivery_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $cattle_id;
                                $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                 ];
                                
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                               // $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 
                              
                              
                            }
                            
                        } else {
                              //IF THERE IS NO BREEDING PROCESS RECORD IN DB
                              // WE WILL BE INSERTING NEW RECORD TO DB
                              //echo "new"; die;
                              $data['ai_on'] = 0;
                              $data['ai_date'] = date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s');
                              $data['cattle_id'] = $cattle_id;
                              $data['current_state'] = 'ai';

                              $event_date = new DateTime($data['ai_date']);
                              //$event_date->modify("+".$on." day");
                              $event_type = 'ai';
                              $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                              $this->UpcomingEvent_model->delete_by_type($cattle_id,'ai');  
                              $alertdata = [
                                'cattle_id' => $cattle_id,
                                'event_type' => $event_type,
                                'message' => $event_message,
                                'event_date' => $event_date->format('Y-m-d 00:00:00')
                               ];
                                //print_r($event_date); die;
                              $this->UpcomingEvent_model->insert_entry($alertdata); 
                              if(!empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                              } else {
                                  $first_pd_type = 'UltraSound';
                              }

                              if(empty($this->input->post('first_pd_on'))){
                                  $first_pd_on = $default['first_manual_pd_on'];
                                  if($first_pd_type == 'UltraSound'){
                                    $first_pd_on = $default['second_us_pd_on'];
                                  }                                
                              } else {
                                  $first_pd_on = $this->input->post('first_pd_on');
                              }

                              $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                              //print_r($event_date); die;
                              $event_date->modify("+".$first_pd_on." day");
                              $event_type = 'first_pd';
                              $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');

                              //print_r($event_message); die;

                              $data['first_pd_type'] = $first_pd_type;
                              $data['first_pd_on'] = $first_pd_on;
                              $data['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                            

                            $result =['status' => '1','result' => $data ];   

                            $alertdata = [
                                  'cattle_id' => $cattle_id,
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                              ];
                            //print_r($event_date); die;                           

                            $this->UpcomingEvent_model->insert_entry($alertdata);    
                            print_r($data); die;
                            $this->BreedingProcess->insert_entry($data);
                            $this->update_event($post,$event_type, $event_date, $cattle_id,[]);     
                        }
                             // $this->form_validation->set_rules('first_pd_type', 'First PD ty', 'required|integer|is_unique[cattles.tag_id]');
                            
                        
                    }
             }
                 return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));


        }


        public function update_state(){
            $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('History_model','DeliveryData','Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                   $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('type', 'Breeding Process Type', 'required');
                    $this->form_validation->set_rules('cattle_id[]', 'Cattle ID(s)', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        $cattles = $this->input->post('cattle_id');
                        if(count($cattles) > 0){
                          foreach ($cattles as $cattle) {
                              $already = $this->BreedingProcess->get_one_by_cattle_id($cattle);
                              $default = $this->BreedingProcess->get_one(1);
                              $save_data = [];
                              if($formdata['type'] == 'heat'){
                                 $history_event_type = 'heat';
                                 $history_event_date = date('Y-m-d');
                                 $history_ai_date = date('Y-m-d');
                                 $history_message = 'Cattle was on Heat on '. date('Y-m-d');                                

                              } else if($formdata['type'] == 'ai'){
                                 $save_data['is_ai_done'] = 'Yes';
                                 $save_data['current_state'] = 'ai';

                                 $history_event_type = 'ai';
                                 $history_event_date = date('Y-m-d 00:00:00');
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'AI was Done'. date('Y-m-d');
                                 $on = $already['first_pd_on'];                                
                                // $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$cattle);
                               
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");                                
                                $event_type = 'first_pd';
                                $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d'); 


                              } else if($formdata['type'] == 'first_pd'){
                                 $save_data['is_first_pd_done'] = 'Yes';
                                 $save_data['first_pd_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'first_pd';
                                 
                                 $history_event_type = 'first_pd';
                                 $history_event_date = $save_data['first_pd_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'First PD Done on'. date('Y-m-d');
                                 $on = $already['second_pd_on'];
                               //echo $already['ai_date']; die;
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'second_pd';
                                $event_message = 'Second PD is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'second_pd'){
                                 $save_data['is_second_pd_done'] = 'Yes';
                                 $save_data['second_pd_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'second_pd';
                                 
                                 $history_event_type = 'second_pd';
                                 $history_event_date = $save_data['second_pd_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Second PD Done on'. date('Y-m-d');
                                 $on = $already['dry_on'];
                                
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'dry';
                                $event_message = 'Putting Cattle on Dry is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'dry'){
                                 $save_data['is_dry_done'] = 'Yes';
                                 $save_data['dry_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'dry';
                                 
                                 $history_event_type = 'dry';
                                 $history_event_date = $save_data['dry_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Putting On Dry Done on'. date('Y-m-d');
                                 $on = $already['steam_up_on'];
                               
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'steam_up';
                                $event_message = 'Putting Cattle on Steam Up is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'steam_up'){
                                 $save_data['is_steam_up_done'] = 'Yes';
                                 $save_data['steam_up_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'steam_up';
                                 
                                 $history_event_type = 'steam_up';
                                 $history_event_date = $save_data['steam_up_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Steam Up Done on'. date('Y-m-d');
                                 $on = $already['delivery_on'];
                               
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'delivery';
                                $event_message = 'Delivery is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'delivery'){
                                 //$delivery_event = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$cattle);
                                $delivery_event = $this->DeliveryData->find_upcomming_by_date_and_cattle_id($already['delivery_date'],$cattle);
                                
                                 $save_data['is_delivery_done'] = 'Yes';
                                 //$save_data['delivery_date'] = date('Y-m-d 00:00:00');
                                 $save_data['delivery_date'] =  (count($delivery_event) > 0) ?  $delivery_event['delivery_date'] :  date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'delivery';


                                 $history_event_type = 'delivery';
                                 $history_event_date = $save_data['delivery_date'];
                                 $history_ai_date = $already['ai_date'];
                                 
                                if(count($delivery_event) > 0){

                                    //$delivery_data = $this->DeliveryData->find_upcomming_by_date_and_cattle_id($delivery_event['event_date'],$cattle);
                                    //print_r($delivery_data); die;
                                    $history_message = 'Delivery Done on '.date_create( $delivery_event['delivery_date'])->format('Y-m-d').'.<br>Male: '. $delivery_event['male_count'] .'  <br>Female: '. $delivery_event['female_count'] .' <br>Died: '. $delivery_event['dead_count'] .'';
                                } else {
                                    $history_message = 'Delivery Done on'. date('Y-m-d');   
                                }
                                $cattle_record = $this->Cattle_model->get_one($cattle);

                                //print_r($cattle_record); die;
                                if(count($cattle_record) > 0){
                                    $update_lactation = [
                                        'lactation' => $cattle_record['lactation'] + 1,
                                        'per_day_milk' => 0,
                                    ];
                                  $this->Cattle_model->update_entry( $cattle, $update_lactation);    
                                }
                                $on = 40;
                              
                                $event_date1 = new DateTime($already['delivery_date']);
                                $event_date1->modify("+39 day");
                                $event_message1 = 'Heat is going to take place on '.$event_date1->format('Y-m-d');
                                $alertdata = [
                                  'cattle_id' => $cattle,
                                  'event_type' => 'heat',
                                  'message' => $event_message1,
                                  'event_date' => $event_date1->format('Y-m-d')
                                ];
                                $this->UpcomingEvent_model->delete_by_type($cattle,'heat');
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                //$save_data['current_state'] = 'delivery';
                                //IF DELIVER IS DONE CREATING NEW EVENT FOR AI AGAIN
                                //WHOLE PROCESS WILL BE REGENERATED AFTER AI IS MARKED DONE.
                                // NEED DAT TO FIND DATA FROM DELIVERY DATA
                                $event_date = new DateTime($already['delivery_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'ai';
                                $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d');   
                                $save_data['ai_date'] = $event_date->format('Y-m-d H:i:s');
                                /*if($cattle == 123){
                                        print_r((count($already) > 0) ? $already : $default); die;
                                 }
*/                                //echo $cattle; die;
                                $this->BreedingProcess->update_entry_by_cattle_id($cattle, $save_data);
                                //AS DELIVERY IS DONE SO IS ALL THE BREEDING PROCESS
                                //DELETING ALL THE DATA FROM THE UPCOMMING EVENTS TABLE TO THE RELATED CATTLE
                                $this->UpcomingEvent_model->delete_by_type($cattle,'ai');  
                                $this->UpcomingEvent_model->delete_by_type($cattle,'first_pd');  
                                $this->UpcomingEvent_model->delete_by_type($cattle,'second_pd'); 
                                $this->UpcomingEvent_model->delete_by_type($cattle,'dry');  
                                $this->UpcomingEvent_model->delete_by_type($cattle,'steam_up');  
                                $this->UpcomingEvent_model->delete_by_type($cattle,'delivery');  
                                $this->update_event((count($already) > 0) ? $already : $default ,$event_type, $event_date, $cattle,[]);     
                                //$this->BreedingProcess->delete($cattle);
                              }

                              $history = [
                                    'cattle_id' => $cattle,
                                    'event_type' => $history_event_type,
                                    'event_date' => $history_event_date,
                                    'ai_date' => $history_ai_date,
                                    'message' => $history_message
                                ];
                                  if($formdata['type'] == 'heat'){
                                      $this->History_model->insert_entry($history);
                                      $this->UpcomingEvent_model->delete_by_type($cattle, $formdata['type']);  
                                  } else {
                                    $alertdata = [
                                        'cattle_id' => $cattle,
                                        'event_type' => $event_type,
                                        'message' => $event_message,
                                        'event_date' => $event_date->format('Y-m-d')
                                     ];
                                     $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id($event_type,$cattle);
                                     if(count($u) > 0){
                                          $this->UpcomingEvent_model->delete($u['id']);
                                     }
                                      $this->UpcomingEvent_model->insert_entry($alertdata);                                    
                                      $this->History_model->insert_entry($history);
                                      /*if($cattle == 53){
                                        echo $formdata['type'];  die;
                                      }*/
                                      $this->BreedingProcess->update_entry_by_cattle_id($cattle, $save_data);
                                      $this->UpcomingEvent_model->delete_by_type($cattle, $formdata['type']);  
                                  }
                                  
                                 /*if($formdata['type'] == 'delivery'){
                                    $this->BreedingProcess->delete($already['id']);
                                 }*/ 
                          }

                        }
                        $result = ['status' => '1','message' => 'Breeding Process Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }


        public function get_events_count(){
             $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model','Cattle_model','DailyMilkData'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                   $heat = $this->UpcomingEvent_model->get_by_type('heat',$checklogin['userdata']);
                   $ai = $this->UpcomingEvent_model->get_by_type('ai',$checklogin['userdata']);
                   $first_pd = $this->UpcomingEvent_model->get_by_type('first_pd',$checklogin['userdata']);
                   $second_pd = $this->UpcomingEvent_model->get_by_type('second_pd',$checklogin['userdata']);
                   $dry = $this->UpcomingEvent_model->get_by_type('dry',$checklogin['userdata']);
                   $steam_up = $this->UpcomingEvent_model->get_by_type('steam_up',$checklogin['userdata']);
                   $delivery = $this->UpcomingEvent_model->get_by_type('delivery',$checklogin['userdata']);
                   $total_cattles = $this->Cattle_model->get_total([], $checklogin['userdata']);
                   $mycattles = $this->Cattle_model->get_all_cattle_ids($checklogin['userdata']);
                   $final_milk_production= 0;
                   if(count($mycattles) > 0){
                      foreach ($mycattles as  $cattle) {
                         $today_milk = $this->DailyMilkData->get_by_cattle_and_date($cattle['id']);
                         if(count($today_milk) > 0){
                            $final_milk_production += $today_milk['total'];
                         }
                      }
                   } else {
                      //NO CATTLES NO MILK PRODUCTION
                      $final_milk_production += 0;
                   }
                   //$total_milk_production = 
                   $result['heat'] = ['total' => count($heat),'IDs' => count($heat) ? $heat : [] ];
                   $result['ai'] = ['total' => count($ai),'IDs' => count($ai) ? $ai : [] ];
                   $result['first_pd'] = ['total' => count($first_pd),'IDs' => count($first_pd) ? $first_pd : [] ];
                   $result['second_pd'] = ['total' => count($second_pd),'IDs' => count($second_pd) ? $second_pd : [] ];
                   $result['dry'] = ['total' => count($dry),'IDs' => count($dry) ? $dry : [] ];
                   $result['steam_up'] = ['total' => count($steam_up),'IDs' => count($steam_up) ? $steam_up : [] ];
                   $result['delivery'] = ['total' => count($delivery),'IDs' => count($delivery) ? $delivery : [] ];
                   /*$result['total_cattles'] = ['total' => $total_cattles ,'IDs' => [] ];
                   $result['total_cattles'] = ['total' => $total_cattles ,'IDs' => [] ];*/
                   $result['total_cattles'] = $total_cattles;
                   $result['today_milk_production'] = $final_milk_production;
                   $result = ['status' => '1','result' => $result];
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

        public function cattle_history(){
             $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('History_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                   $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('page', 'Page', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                       $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                                $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->History_model->get_total($formdata);                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->History_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    //build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                        $result = $params;
                    }          
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

        }

         public function delete_cattle(){
             $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $this->Cattle_model->soft_delete($this->input->post('cattle_id'));
                        $result = ['status' => '1','message' => 'Deleted Successfully!'];
                    }
                   //$result['delivery'] = date_create();
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

         }

         public function delete_expense(){
             $this->load->library('form_validation');
             $this->load->model(array('Expense_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('id', 'ID', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $this->Expense_model->delete($this->input->post('id'));
                        $result = ['status' => '1','message' => 'Deleted Successfully!'];
                    }
                   //$result['delivery'] = date_create();
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

         }

             public function delete_income(){
             $this->load->library('form_validation');
             $this->load->model(array('Income_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('id', 'ID', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $this->Income_model->delete($this->input->post('id'));
                        $result = ['status' => '1','message' => 'Deleted Successfully!'];
                    }
                   //$result['delivery'] = date_create();
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

         }


         public function product_request(){
            $this->load->library('form_validation');
             $this->load->model(array('ProductRequest_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('phone', 'Phone', 'required');
                    $this->form_validation->set_rules('sku', 'Sku', 'required');
                    $this->form_validation->set_rules('quantity', 'Quantity', 'required');
                    $this->form_validation->set_rules('quote_Message', 'Quote Message', 'required');
                    $this->form_validation->set_rules('email', 'Email', 'required');

                    $savedata = [
                        'name' => $this->input->post('name'),
                        'contact' => $this->input->post('phone'),
                        'sku' => $this->input->post('sku'),
                        'quantity' => $this->input->post('quantity'),
                        'message' => $this->input->post('quote_Message'),
                        'email' => $this->input->post('email'),
                    ];
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $this->ProductRequest_model->insert_entry($savedata);
                        $result = ['status' => '1','message' => 'Quote Sent Successfully!'];
                    }
                   //$result['delivery'] = date_create();
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));


         }

  public function get_fat_price(){
             $this->load->library('form_validation');
             $this->load->model(array('FatPrice','Cattle_model'));
             //$this->load->model(array('ProductRequest_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('tag_id', 'Tag ID', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('tag_id'),$checklogin['userdata']);                         
                        if(count($expense) > 0){
                            $fatprice = $this->FatPrice->get_one_by_cattle_id($expense['id']);
                               $result = ['status' => '1','result' => [] ];
                            if(count($fatprice) > 0){
                               $result = ['status' => '1','result' => $fatprice];
                            }                          
                        } else {
                              $result = ['status' => '0','result' => ['tag_id' => 'Cattle Record Not Found'] ];
                        }
                    }
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

  }

  public function add_milk_income(){
             $this->load->library('form_validation');
             $this->load->model(array('FatPrice','Income_model','MilkIncentive','MilkData'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('fat_price', 'Fat Price', 'required');                    
                    $this->form_validation->set_rules('fat_quantity', 'Fat Quantity', 'required');                    
                    $this->form_validation->set_rules('snf_quantity', 'SNF Quantity', 'required');                    
                    $this->form_validation->set_rules('milk_volume', 'Milk Volume', 'required');
                    $this->form_validation->set_rules('fat_incentive_1', 'Fat Incentive 1', 'required');
                    $this->form_validation->set_rules('fat_incentive_2', 'Fat Incentive 2', 'required');
                    $this->form_validation->set_rules('snf_incentive_1', 'SNF Incentive 1', 'required');
                    $this->form_validation->set_rules('snf_incentive_2', 'SNF Incentive 2', 'required');
                    //$this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    
                    //$this->form_validation->set_rules('cattle_id[]', 'Cattle ID(s)', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        /*$fat_price = bcdiv( ($formdata['fat_price']/2 + ($formdata['fat_incentive_1'] + $formdata['fat_incentive_2'])) / 100, 1, 2);
                        $snf_price = bcdiv( ($formdata['fat_price']/3 + ($formdata['snf_incentive_1'] + $formdata['snf_incentive_2'])) / 100, 1, 2);
                        $final_fat_price = bcdiv( $fat_price * $formdata['fat_quantity'], 1, 2);
                        $final_snf_price =bcdiv(  $snf_price * $formdata['snf_quantity'], 1, 2);
                        $final_milk_price = bcdiv( $final_fat_price + $final_snf_price, 1, 2);*/
                        $fat_price = ($this->truncate_float(($formdata['fat_price']/2 + ($formdata['fat_incentive_1'] + $formdata['fat_incentive_2'])) / 100, 2));                        
                        //echo $fat_price; die;
                        $snf_price = $this->truncate_float(($formdata['fat_price']/3 + ($formdata['snf_incentive_1'] + $formdata['snf_incentive_2'])) / 100, 2);
                        $final_fat_price = $this->truncate_float($fat_price * $formdata['fat_quantity'], 2);
                        $final_snf_price = $this->truncate_float($snf_price * $formdata['snf_quantity'], 2);
                        $final_milk_price = $this->truncate_float($final_fat_price + $final_snf_price, 2);
                        ///                        truncate_float($final_fat_price + $final_snf_price, 2);
                        //$fat_price = bcdiv( ($formdata['fat_price']/2 + ($formdata['fat_incentive_1'] + $formdata['fat_incentive_2'])) / 100, 1, 2);
                        //$got_incentive = 'No';
                        $incentive_amount = 0;
                        //if(isset($formdata['incentive_quantity']) &&  count($formdata['incentive_quantity']) > 0){
                            //IF THERE ARE INCENTIVE PRESENT
                          //  for($i=0; $i < count($formdata['incentive_quantity']); $i++) { 
                                //echo $formdata['incentive_quantity'][$i]; die;
                                //if($formdata['milk_volume'] >= $formdata['incentive_quantity'][$i]){
                                  //IF MILK VOLUME IS GREATER THAN INCENTIVE QUANTITY
                                  //HE/SHE IS ELIGIBLE FOR THE INCENTIVE
                                  $incentive_amount = $formdata['incentive_amount'][0];
                                //}
                          //  }  
                      //  }

                        if(isset($formdata['management_charges'])){
                          $incentive_amount+=$formdata['management_charges'];
                        }
                        if(isset($formdata['maintenance_charges'])){
                          $incentive_amount+=$formdata['maintenance_charges'];
                        }

                        //echo $final_milk_price;
                        $incentive =  $formdata['milk_volume'] * $incentive_amount;
                        $user_id =  $checklogin['userdata']['id'];
                        $milk_amount = $final_milk_price  * $formdata['milk_volume'];
                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        //echo $formdata['milk_volume']; die;
                        $data = array(
                          'user_id' => $user_id,
                          'milk_sale_rate' => $final_milk_price,
                          'manure_sale_rate' => 0,
                          'milk_sale' => $formdata['milk_volume'],
                          'manure_sale' => 0,
                          'others' => $incentive,
                          'total' => $milk_amount + $incentive,
                          'income_date' => $date,                          
                        );
                        //Transfering data to Model
                        $this->Income_model->insert_entry($data);

                        //$fatprice = $this->FatPrice->get_one_by_cattle_id($formdata['cattle_id']);                          
                        $fatdata = [
                              'fat_price' =>  $formdata['fat_price'],
                              'fat_incentive_1'  => $formdata['fat_incentive_1'],
                              'fat_incentive_1'  => $formdata['fat_incentive_1'],
                              'snf_incentive_1'  => $formdata['snf_incentive_1'],
                              'snf_incentive_2'  => $formdata['snf_incentive_2'],
                              'milk_volume' => $formdata['milk_volume'],
                              //'cattle_id' => $formdata['cattle_id'],
                          ];
                        /*if(count($fatprice) > 0){
                          $this->FatPrice->update_entry($fatdata);  
                          $result = ['status' => '1','message' =>'Updated Successfully' ];
                        } else {
                          $this->FatPrice->insert_entry($fatdata);
                          $result = ['status' => '1','message' =>'Updated Successfully' ]; 
                        } */   

                        unset($formdata['incentive_quantity']);
                        unset($formdata['incentive_amount']);
                        unset($formdata['key']);
                        unset($formdata['fat_price']);
                        unset($formdata['maintenance_charges']);
                        unset($formdata['management_charges']);
                        $formdata['incentive'] = $incentive_amount;
                        $this->MilkData->insert_entry($formdata);
                        $result = ['status' => '1','message' =>'Updated Successfully' ]; 

                    }
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));


  }

     function daily_report(){
              $fileType = 'Excel5';              
              $this->load->model(array('Income_model','User_model'));
              //load our new PHPExcel library
              $this->load->library('excel');
              $users = $this->User_model->get_all();
              // read data to active sheet
              foreach($users as $user){
                    $fileName = APPPATH.'uploads/daily_report'.$user['id'].'.xls';
                    $this->excel->setActiveSheetIndex(0);
                    //name the worksheet
                    $rowCount = 1;
                    $this->excel->getActiveSheet()->setTitle('Daily Report');
                    $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, 'Date');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Milk Sale Price');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Milk Volume');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Manuare Sale Price');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, 'Manuare Volume');
                    $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, 'Others');
                    $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, 'Total');      
                    $income = $this->Income_model->get_all_today($user);
                    $grand_total = 0;
                    if(!empty($income)){
                        foreach($income as $i){
                           $rowCount++;
                           $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, $i['income_date'] );        
                           $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, $i['milk_sale_rate'] );        
                           $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, $i['milk_sale'] );        
                           $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, $i['manure_sale_rate'] );        
                           $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, $i['manure_sale'] );        
                           $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, $i['others'] );
                           $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, $i['total'] );
                           $grand_total+=$i['total'];
                        }
                    }
                    $rowCount++;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, 'Grand Total:');
                    $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, $grand_total);
                    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
                    $objWriter->save($fileName); 
                    chmod($fileName, 0777); 
                    $response['name'] = $user['name'];
                    $response['date'] = date('Y-m-d');
                    $pre_date = date("Y-m-d" , strtotime("-1 day") );
                    $this->sendmail('emails/daily_report',$response,$user,'Daily Income Report('.$pre_date.')',[$fileName]);                    
                    $this->excel->disconnectWorksheets();
                    $this->excel->createSheet();
              }

              $result = ['status' => '1'];
               return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));  
    }


    function monthly_report(){
              $fileType = 'Excel5';              
              $this->load->model(array('Income_model','User_model'));
              //load our new PHPExcel library
              $this->load->library('excel');
              $users = $this->User_model->get_all();
              // read data to active sheet
              foreach($users as $user){
                    $fileName = APPPATH.'uploads/monthly_report'.$user['id'].'.xls';
                    $this->excel->setActiveSheetIndex(0);
                    //name the worksheet
                    $rowCount = 1;
                    $this->excel->getActiveSheet()->setTitle('Monthly Report');
                    $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, 'Date');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Milk Sale Price');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Milk Volume');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Manuare Sale Price');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, 'Manuare Volume');
                    $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, 'Others');
                    $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, 'Total');      
                    $income = $this->Income_model->get_all_current_month($user);
                    
                    $grand_total = 0;
                    if(!empty($income)){

                        foreach($income as $i){
                           $rowCount++;
                           $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, $i['income_date'] );        
                           $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, $i['milk_sale_rate'] );        
                           $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, $i['milk_sale'] );        
                           $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, $i['manure_sale_rate'] );        
                           $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, $i['manure_sale'] );        
                           $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, $i['others'] );
                           $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, $i['total'] );
                           $grand_total+=$i['total'];
                        }
                    }
                    $rowCount++;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$rowCount, '');
                    $this->excel->getActiveSheet()->SetCellValue('F'.$rowCount, 'Grand Total:');
                    $this->excel->getActiveSheet()->SetCellValue('G'.$rowCount, $grand_total);
                    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
                    $objWriter->save($fileName); 
                    chmod($fileName, 0777); 
                    $response['name'] = $user['name'];
                    $response['date'] = date('Y-m-d');
                    $month = date('M', strtotime('-1 months'));
                    $year = (int) date('Y', strtotime('-1 months')); 
                    $this->sendmail('emails/daily_report',$response,$user,'Monthly Income Report('.$month.'  '.$year.')',[$fileName]);                    
                    $this->excel->disconnectWorksheets();
                    $this->excel->createSheet();
              }

              $result = ['status' => '1'];
               return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));  
    }






    public function reset_password(){
        $this->load->model(array('User_model'));
         $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email ID', 'required');
        if ($this->form_validation->run() == FALSE) {
            $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
        } else {
          $user = $this->User_model->get_one_by_email($this->input->post('email'));
          if(count($user) > 0){
            $response = $this->User_model->reset_password($user);
            $response['name'] = $user['name'];
            $this->sendmail('emails/forgot_password',$response,$user,'Change Password',[]);
            $result = ['status' => '1','message' => 'Password has been sent to your email. Kindly check your email.'];
          }
        }

        return $this->output
          ->set_content_type('application/json')
          ->set_status_header(500)
          ->set_output(json_encode($result));  
    }

    public function add_milk(){
             $this->load->library('form_validation');
             $this->load->model(array('FatPrice','Cattle_model','DailyMilkData'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('tag_id[]', 'Tag ID(s)', 'required');
                    $this->form_validation->set_rules('morning[]', 'Morning Milkdata', 'required');
                    $this->form_validation->set_rules('afternoon[]', 'Afternoon Milkdata', 'required');
                    $this->form_validation->set_rules('evening[]', 'Evening Milkdata', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $today = date('Y-m-d');
                       if(count($formdata['tag_id']) > 0){
                           for($i=0; $i < count($formdata['tag_id']); $i++) { 
                            //IF WE FOUND CATTLE RECORD AS PER TAG ID AND CURRENT LOGGED IN USER
                            $cattle = $this->Cattle_model->get_one_from_tag_and_user($formdata['tag_id'][$i],$checklogin['userdata']);
                            if(count($cattle) > 0){
                              // IF WE FOUND RECORD FOR THE CATTLE
                              $today_milk_record = $this->DailyMilkData->get_one_by_cattle_and_date($cattle['id'],$today);
                              //print_r($today_milk_record); die;
                              $data = [
                                    'milk_date' => $today,
                                    'cattle_id' => $cattle['id'],
                                    'morning' => (isset($formdata['morning'][$i])) ? $formdata['morning'][$i] : 0 ,
                                    'afternoon' => (isset($formdata['afternoon'][$i])) ? $formdata['afternoon'][$i] : 0,
                                    'evening' => (isset($formdata['evening'][$i])) ? $formdata['evening'][$i] : 0,
                                    'total' => $formdata['morning'][$i] + $formdata['afternoon'][$i] + $formdata['evening'][$i],
                                ];
                              if(count($today_milk_record) == 0){
                                // IF THERE IS NO RECORD FOR THE MILK RECORD FOR THE CURRENT CATTLE                             
                                // WE WILL BE INSERTING RECORD IN THE DAILY MILK DATA RECORD TABLE   
                                $this->DailyMilkData->insert_entry($data);
                                $cattle_data =[
                                    'per_day_milk' => $cattle['per_day_milk'] + $data['total']
                                ];
                                $this->Cattle_model->update_entry($cattle['id'],$cattle_data);
                              } else {
                                $this->DailyMilkData->update_entry( $data,$today_milk_record['id'] );
                                // IF THERE IS ALREADY RECORD IN THE TABLE WE WILL UPDATE THE RECORD.
                                $minus_today_milk = $cattle['per_day_milk'] - $today_milk_record['total'];
                                $cattle_data =[
                                    'per_day_milk' => $minus_today_milk + $data['total']
                                ];
                                $this->Cattle_model->update_entry($cattle['id'],$cattle_data);
                              }
                            }
                           }
                       }
                       $result = ['status' => '1' ,'message' => 'Milk Data Updated Successfully!'];                         
                    }
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
    }

    public function get_milk_data(){
            $this->load->library('form_validation');
             $this->load->model(array('FatPrice','Cattle_model','DailyMilkData'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                 $cattles = $this->Cattle_model->get_all_cattles($checklogin['userdata']);
                 //print_r($cattles); die;
                 $final_arr= [];
                 $today = date('Y-m-d');
                 if(count($cattles) > 0){
                   foreach($cattles as $cattle){
                       $today_milk_record = $this->DailyMilkData->get_one_by_cattle_and_date($cattle['id'],$today);
                       $cattle['milk_data'] = (count($today_milk_record) > 0) ? $today_milk_record : [];
                       $final_arr[] = $cattle;
                   }
                 }
                 $result = ['status' => '1' , 'result' => $final_arr ];
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
    }


    public function add_delivery_data(){
             $this->load->library('form_validation');
             $this->load->model(array('DeliveryData','Cattle_model','DailyMilkData','UpcomingEvent_model','BreedingProcess'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('male_count', 'Number of Males', 'required');
                    $this->form_validation->set_rules('female_count', 'Number of Females', 'required');
                    $this->form_validation->set_rules('alive_count', 'Number of Alive Calves', 'required');
                    $this->form_validation->set_rules('dead_count', 'Number of Dead Calves', 'required');
                    $this->form_validation->set_rules('del_date', 'Delivery Date', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array()];
                    } else {
                        //$cattle = $this->Cattle_model->get_one_from_tag_and_user($this->input->post('tag_id'),$checklogin['userdata']);
                        $cattle = $this->Cattle_model->get_one($formdata['cattle_id']);
                        
                        $delivery_event = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$cattle['id']);
                        /*if($formdata['cattle_id']  == 53){
                            print_r($delivery_event); die;
                         }*/
                        if(count($cattle) > 0){
                             $data = [
                                        'cattle_id' => $cattle['id'],
                                        'male_count' => $formdata['male_count'],
                                        'female_count' => $formdata['female_count'],
                                        'alive_count' => $formdata['alive_count'],
                                        'dead_count' => $formdata['dead_count'],
                                        'delivery_date' => (!empty($formdata['del_date'])) ? date_create($formdata['del_date'])->format('Y-m-d H:i:s') : $delivery_event['event_date']
                                     ];

                                    /*if($formdata['cattle_id']  == 53){
                                        print_r($data); die;
                                     }*/
                           $this->DeliveryData->insert_entry($data);
                           //UPDATING CALVING DATE INTO CATTLE TABLE
                           $cattle_save_data = [
                                'calving_date' => (!empty($formdata['del_date'])) ? date_create($formdata['del_date'])->format('Y-m-d H:i:s') : $delivery_event['event_date']
                           ];
                           $this->Cattle_model->update_entry($cattle['id'],$cattle_save_data);
                           //UPGRADING BREEDING PROCESS ALSO.
                           $already = $this->BreedingProcess->get_one_by_cattle_id($cattle['id']);
                           $breedingprocess_save_data = [
                                'delivery_date' =>  (!empty($formdata['del_date'])) ? date_create($formdata['del_date'])->format('Y-m-d H:i:s') : $delivery_event['event_date']
                           ];

                           if(count($already) > 0){
                                $this->BreedingProcess->update_entry($already['id'],$breedingprocess_save_data);
                           }

                        }
                         $result = ['status' => '1','message' => 'Information Saved!'];
                    }
                
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result)); 
    }

     function aztro() {
        //$checklogin = $this->checklogin($formdata);
            $formdata = $this->input->post();          
           /* $arr = [];
            $ch = curl_init('http://stellarastrology.in/api/v1/horoscope');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($formdata));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                             'Content-Type: application/json'                                                                                
                            //'Content-Length: ' . strlen($data_string)
                            )                                                                       
                        );
            $response = curl_exec($ch);
            // close the connection, release resources used
            curl_close($ch);*/
            $response =  $this->post_curl('http://stellarastrology.in/api/v1/horoscope','POST',$formdata);

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($response)); 
            // do anything you want with your response
            //var_dump($response); die;
    }

    function getheroscope(){
       $formdata = $this->input->post();
       $response =  $this->post_curl("http://horoscope-api.herokuapp.com/horoscope/".$formdata['duration']."/".$formdata['current_sign']."",'GET',$formdata);

            return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($response)); 
        
    }

    function save_style(){
        $formdata = $this->input->post();
        $this->load->model(array('Data_model'));
        $this->Data_model->update_product($formdata['id'],$formdata);
    }



     public function confirm_pregnency(){
        $this->load->model(array('Cattle_model','UpcomingEvent_model','BreedingProcess'));
         $this->load->library('form_validation');
        //$this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('is_pregnant', 'Is Cattle Pregnant', 'required');
        $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
        if ($this->form_validation->run() == FALSE) {
            $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
        } else {
           $cattle_record = $this->Cattle_model->get_one($this->input->post('cattle_id')); 
           if(count($cattle_record) > 0){
                $cattle =$cattle_record['id'];
                if($this->input->post('is_pregnant') == 'Yes'){
                    //
                } else {
                    //IF CATTLE IS NOT PREGNANT
                    $this->UpcomingEvent_model->delete_by_type($cattle,'ai');  
                    $this->UpcomingEvent_model->delete_by_type($cattle,'first_pd');  
                    $this->UpcomingEvent_model->delete_by_type($cattle,'second_pd'); 
                    $this->UpcomingEvent_model->delete_by_type($cattle,'dry');  
                    $this->UpcomingEvent_model->delete_by_type($cattle,'steam_up');  
                    $this->UpcomingEvent_model->delete_by_type($cattle,'delivery');  
                    $already = $this->BreedingProcess->get_one_by_cattle_id($cattle);
                    if(count($already) > 0){
                        $this->BreedingProcess->delete($already['id']);    
                    }

                    $event_date1 = new DateTime(date('Y-m-d H:i:s')) ;
                    $event_date1->modify("+1 day");
                    $event_message1 = 'Heat is going to take place on '.$event_date1->format('Y-m-d');
                    $alertdata = [
                      'cattle_id' => $cattle,
                      'event_type' => 'heat',
                      'message' => $event_message1,
                      'event_date' => $event_date1->format('Y-m-d')
                    ];
                    $this->UpcomingEvent_model->insert_entry($alertdata);
                }
           }
        }
        $result = ['status' => '1','message' => 'Information Saved!'];
        return $this->output
          ->set_content_type('application/json')
          ->set_status_header(200)
          ->set_output(json_encode($result));  
    }







}
