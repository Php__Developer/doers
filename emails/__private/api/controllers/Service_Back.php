<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends My_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model','Income_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
            $this->load->library('common');
        }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                echo 'Hello World!';        
                //$this->load->view('welcome_message');
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            //$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');

            $this->form_validation->set_rules('equipment', 'Equipment', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|min_length[5]|max_length[200]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]|integer',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            //$this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    //echo json_encode(validation_errors());

                /*$arr = array(
                    'field_name_one' => form_error('field_name_one'),
                    'field_name_two' => form_error('field_name_two')
                );*/
                // errors_array()
                //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
                $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
              //echo $this->input->post('password');
              $p =  password_hash($this->input->post('password'), PASSWORD_DEFAULT); 
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'name' => $this->input->post('name'),
                'last_name' => 'None',
                'email' => $this->input->post('email'),
                'equipment' => $this->input->post('equipment'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
               // 'gender' => $this->input->post('gender'),
                'password' => $p,
                'role_id' => 2,
                'api_key' => $ref
                );
                
                
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                unset($data['password']);
                unset($data['last_name']);
                unset($data['gender']);
                $result = ['status' => '1','message' => 'Registered Successfully!','key' => $ref,
                            'profile' => $data
                ];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
       

        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                 $data = array(
                'email' => $this->input->post('email'),
                'password' =>$this->input->post('password'),
                );
               $result = $this->User_model->login($data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function addexpense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    $user_id =  $checklogin['userdata']['id'];

                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('others')
                            ));
                        $data = array(
                            'user_id' => $user_id,
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total
                        );
                        $this->Expense_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Expense Added Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function get_expenses(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Expense_model->get_total($checklogin['userdata']);
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Expense_model->get_current_page_records($limit_per_page, $start_index,$formdata,$checklogin['userdata']);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

         /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_expense(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Expense_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


         /*
        |--------------------------------------------------------------------------
        | Function : update Expense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

         public function update_expense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             //print_r($checklogin); die;
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    
                    $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('others')
                            ));
                        //echo $user_id; die;
                        $data = array(
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total
                        );
                        $this->Expense_model->update_entry($data);
                        $result = ['status' => '1','message' => 'Expense Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }





        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 




          public function addincome(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  // print_r( $checklogin['userdata']) ; die;
                   $user_id =  $checklogin['userdata']['id'];
                    //$this->form_validation->set_rules('user_id', 'User Name', 'required');
                    $this->form_validation->set_rules('income_date', 'Income Date', 'required');
                    $this->form_validation->set_rules('milk_sale_rate', 'Milk Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('manure_sale_rate', 'Manuare Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('milk_sale', 'Milk Sale', 'required|integer');
                    $this->form_validation->set_rules('manure_sale', 'Manuare Sale', 'required|integer');
                    $this->form_validation->set_rules('others', 'others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $total = array_sum(array(
                                $this->input->post('milk_sale_rate') * $this->input->post('milk_sale'),
                                $this->input->post('manure_sale_rate') * $this->input->post('manure_sale'),
                                $this->input->post('others')
                            ));

                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        $data = array(
                        'user_id' => $user_id,
                        'milk_sale_rate' => $this->input->post('milk_sale_rate'),
                        'manure_sale_rate' => $this->input->post('manure_sale_rate'),
                        'milk_sale' => $this->input->post('milk_sale'),
                        'manure_sale' => $this->input->post('manure_sale'),
                        'others' => $this->input->post('others'),
                        'total' => $total,
                        'income_date' => $date,
                        );
                        //Transfering data to Model
                        $this->Income_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Income Added Successfully!'];
                }
             }
          

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        public function get_income(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Income_model->get_total($formdata, $checklogin['userdata']);
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Income_model->get_current_page_records($limit_per_page, $start_index,$formdata,$checklogin['userdata']);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }

                    }

              


             }

             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }


           /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 
        
        public function get_one_income(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Income_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


         /*
        |--------------------------------------------------------------------------
        | Function : update Expense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

         public function update_income(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  // print_r( $checklogin['userdata']) ; die;
                   $user_id =  $checklogin['userdata']['id'];
                    //$this->form_validation->set_rules('user_id', 'User Name', 'required');
                   $this->form_validation->set_rules('id', 'ID', 'required');
                    $this->form_validation->set_rules('income_date', 'Income Date', 'required');
                    $this->form_validation->set_rules('milk_sale_rate', 'Milk Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('manure_sale_rate', 'Manuare Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('milk_sale', 'Milk Sale', 'required|integer');
                    $this->form_validation->set_rules('manure_sale', 'Manuare Sale', 'required|integer');
                    $this->form_validation->set_rules('others', 'others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $total = array_sum(array(
                                $this->input->post('milk_sale_rate') * $this->input->post('milk_sale'),
                                $this->input->post('manure_sale_rate') * $this->input->post('manure_sale'),
                                $this->input->post('others')
                            ));

                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        $data = array(
                        'user_id' => $user_id,
                        'milk_sale_rate' => $this->input->post('milk_sale_rate'),
                        'manure_sale_rate' => $this->input->post('manure_sale_rate'),
                        'milk_sale' => $this->input->post('milk_sale'),
                        'manure_sale' => $this->input->post('manure_sale'),
                        'others' => $this->input->post('others'),
                        'total' => $total,
                        'income_date' => $date,
                        );
                        //Transfering data to Model
                        $this->Income_model->update_entry($data);
                        $result = ['status' => '1','message' => 'Updated Successfully!'];
                }
             }
          

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }

        public function add_request(){
             $this->load->library('form_validation');
            $this->load->model(array('Request_model'));
                $formdata = $this->input->post();
               // print_r($formdata); die;
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('phone', 'Phone', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $data = array(
                        'name' =>$this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'type' => $this->input->post('type'),
                        );
                        //Transfering data to Model
                        $this->Request_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Request Added Successfully!'];
                }

                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    


        }


             /*
        |--------------------------------------------------------------------------
        | Function : add_cattle
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function add_cattle(){
             $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $this->load->model(array('History_model'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('BreedingProcess'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|integer|is_unique[cattles.tag_id]');
                    $this->form_validation->set_rules('dam_id', 'Dam ID', 'required|integer');
                    $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
                    $this->form_validation->set_rules('dop', 'Date Of Purchase', 'required');
                    $this->form_validation->set_rules('purchase_price', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
                    $this->form_validation->set_rules('is_pregnant','Is Cow Pregnant', 'required');                    
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $ai_date = !empty($this->input->post('ai_date')) ?  date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s') : '';

                        $data = array(
                            'tag_id' => $this->input->post('tag_id'),
                            'dam_id' => $this->input->post('dam_id'),
                            'dob' => $this->input->post('dob'),
                            'dop' => $this->input->post('dop'),
                            'purchase_price' => $this->input->post('purchase_price'),
                            'weight' => $this->input->post('weight'),
                            'ai_date' => $ai_date,
                            'is_pregnant' => $this->input->post('is_pregnant'),
                            'calving_date' => empty($this->input->post('calving_date')) ? '' : $this->input->post('calving_date'),
                            'sale_date' => empty($this->input->post('sale_date')) ? '' : $this->input->post('sale_date'),
                            'death_date' => empty($this->input->post('death_date')) ? '' : $this->input->post('death_date'),
                            'sale_price' => empty($this->input->post('sale_price')) ? 0 : $this->input->post('sale_price'),
                            'owner_id ' => $user_id,
                        );
                       $insert_id =  $this->Cattle_model->insert_entry($data);
                        if(!empty($ai_date)){
                         $id =  $b = $this->BreedingProcess->get_one(1);                          
                            // $this->form_validation->set_rules('first_pd_type', 'First PD ty', 'required|integer|is_unique[cattles.tag_id]');
                            if(empty(!$this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                            } else {
                                $first_pd_type = 'UltraSound';
                            }

                            if(empty($this->input->post('first_pd_on'))){
                                $first_pd_on = $b['first_manual_pd_on'];
                                if($first_pd_type == 'UltraSound'){
                                  $first_pd_on = $b['second_us_pd_on'];
                                }                                
                            } else {
                                $first_pd_on = $this->input->post('first_pd_on');
                            }
                            
                            $first_pd_date = new DateTime($ai_date);
                            $first_pd_date->modify("+".$first_pd_on." day");
                            $alertdata = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => 'first_pd',
                                  'message' => 'First PD is going to take place on '.$first_pd_date->format('Y-m-d'),
                                  'event_date' => $first_pd_date->format('Y-m-d')
                            ];
                            $this->UpcomingEvent_model->insert_entry($alertdata);
                            $updatedata = [
                                'first_pd_type' => $first_pd_type,
                                'first_pd_date' => $first_pd_date->format('Y-m-d'),
                                'first_pd_on' => $first_pd_on
                            ];
                            $this->Cattle_model->update_entry($insert_id,$updatedata);
                            $history = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => 'ai',
                                  'event_date' => date_create($ai_date)->format('Y-m-d')
                            ];
                            $this->History_model->insert_entry($history);
                            // Inserting Data to Breeding Table
                          
                            $default_process = $this->BreedingProcess->get_one(1);
                            $default_process['ai_on'] = 0;
                            $default_process['ai_date'] =$ai_date;
                            $default_process['is_ai_done'] = 'Yes';
                            $default_process['cattle_id'] = $insert_id;
                            $default_process['current_state'] = 'ai';
                            unset($default_process['id']);
                            $this->BreedingProcess->insert_entry($default_process);
                            //$this->Cattle_model->insert_entry($data);
                            
                        }

                        $result = ['status' => '1','message' => 'Cattle Data Added Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }



               /*
        |--------------------------------------------------------------------------
        | Function : update_cattle
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function update_cattle(){
             $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                   $cattle = $this->Cattle_model->get_one($this->input->post('id'));

                   if($cattle['tag_id'] !== $this->input->post('tag_id')){
                     /*echo $this->input->post('tag_id');
                   print_r($cattle); die;*/
                    // IF TAG ID MATCHES OTHER ID THAN THE CURRENT RECORD IMPLEMENT UNIQUE
                     $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|integer|is_unique[cattles.tag_id]'); 
                   } else {
                     $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|integer');
                   }
                    $this->form_validation->set_rules('dam_id', 'Dam ID', 'required|integer');
                    $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
                    $this->form_validation->set_rules('dop', 'Date Of Purchase', 'required');
                    $this->form_validation->set_rules('purchase_price', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('weight', 'Weight', 'required|integer');
                    $this->form_validation->set_rules('id', 'ID', 'required|integer');
                    //$this->form_validation->set_rules('ai_date', 'Atrificial Insemination Date', 'required|integer');
                    //$this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('is_pregnant','Is Cow Pregnant', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $data = array(
                            'tag_id' => $this->input->post('tag_id'),
                            'dam_id' => $this->input->post('dam_id'),
                            'dob' => $this->input->post('dob'),
                            'dop' => $this->input->post('dop'),
                            'purchase_price' => $this->input->post('purchase_price'),
                            'weight' => $this->input->post('weight'),
                            'ai_date' => $this->input->post('ai_date'),
                            'is_pregnant' => $this->input->post('is_pregnant'),
                            'calving_date' => $this->input->post('calving_date'),
                            'sale_date' => $this->input->post('sale_date'),
                            'death_date' => $this->input->post('death_date'),
                            'sale_price' => empty($this->input->post('sale_price')) ? 0 : $this->input->post('sale_price'),
                            'owner_id ' => $user_id
                        );
                       // $this->Cattle_model->insert_entry($data);
                        $this->Cattle_model->update_entry($data);
                        $ai_date = !empty($this->input->post('ai_date')) ?  date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s') : '';
                         if(!empty($ai_date)){
                         $b = $this->BreedingProcess->get_one(1);                          
                          $bp = $this->BreedingProcess->get_one_by_cattle_id($this->input->post('id'));
                       
                          if(count($bp) == 0){
                              //$result =['status' => '1','result' => $expense]; 
                              // $this->form_validation->set_rules('first_pd_type', 'First PD ty', 'required|integer|is_unique[cattles.tag_id]');
                            if(empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                            } else {
                                $first_pd_type = 'UltraSound';
                            }

                            if(empty($this->input->post('first_pd_on'))){
                                $first_pd_on = $b['first_manual_pd_on'];
                                if($first_pd_type == 'UltraSound'){
                                  $first_pd_on = $b['second_us_pd_on'];
                                }                                
                            } else {
                                $first_pd_on = $this->input->post('first_pd_on');
                            }
                            
                            $first_pd_date = new DateTime($ai_date);
                            $first_pd_date->modify("+".$first_pd_on." day");
                            $alertdata = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => 'first_pd',
                                  'message' => 'First PD is going to take place on '.$first_pd_date->format('Y-m-d'),
                                  'event_date' => $first_pd_date->format('Y-m-d')
                            ];
                            $this->UpcomingEvent_model->insert_entry($alertdata);
                            $updatedata = [
                                'first_pd_type' => $first_pd_type,
                                'first_pd_date' => $first_pd_date->format('Y-m-d'),
                                'first_pd_on' => $first_pd_on
                            ];
                            $this->Cattle_model->update_entry($insert_id,$updatedata);
                            $history = [
                                  'cattle_id' => $insert_id,
                                  'event_type' => 'ai',
                                  'event_date' => date_create($ai_date)->format('Y-m-d')
                            ];
                            $this->History_model->insert_entry($history);

                            // Inserting Data to Breeding Table
                            $bp_data = array(
                                'ai_on' => 0,
                                'ai_date' => $ai_date,
                                'is_ai_done' => 'Yes'
                            );
                            $default_process = $this->BreedingProcess->get_one(1);
                            $default_process['ai_on'] = 0;
                            $default_process['ai_date'] =$ai_date;
                            $default_process['is_ai_done'] = 'Yes';
                            $default_process['cattle_id'] = $this->input->post('id');
                            $default_process['current_state'] = 'ai';
                            unset($default_process['id']);
                            $this->BreedingProcess->insert_entry($default_process);
                            //$this->Cattle_model->insert_entry($data);
                          }
                        }

                        $result = ['status' => '1','message' => 'Cattle Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }


        /*
        |--------------------------------------------------------------------------
        | Function : get_cattle_from_tag
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


          public function get_cattle_from_tag(){
            $this->load->library('form_validation');
             $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('tag_id', 'Tag ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Cattle_model->get_one_from_tag($this->input->post('tag_id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


           /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function get_cattles(){
            $this->load->library('form_validation');
            $this->load->model(array('Cattle_model'));
            $formdata = $this->input->post();
            $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Cattle_model->get_total($formdata,$checklogin['userdata']);
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Cattle_model->get_current_page_records($limit_per_page, $start_index,$formdata,$checklogin['userdata']);
                                    $config['base_url'] = base_url() . '/get_cattles';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => [],'total' => 0 ];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

        /*
        |--------------------------------------------------------------------------
        | Function : get_one_income
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_cattle(){
            $this->load->library('form_validation');
            $this->load->model(array('Cattle_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Cattle_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function products(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 2;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Data_model->get_total();
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Data_model->get_all();
                                  /*  $params["result"] = $this->Data_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);*/
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }


          /*
        |--------------------------------------------------------------------------
        | Function : get_one_product
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_product(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Data_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


        public function get_breeding_process(){
           $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('History_model'));
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        //$expense = $this->BreedingProcess->get_one($this->input->post('id'));
                        $expense = $this->BreedingProcess->get_one_by_cattle_id($this->input->post('cattle_id'));
                        $all_history = $this->History_model->get_all($this->input->post('cattle_id'));
                        if(count($expense) > 0){
                             $expense['history'] = count($all_history) ? $all_history : [];
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            //echo 'else'; die;
                            $expense = $this->BreedingProcess->get_one(1);

                            $expense['id'] = 0;
                            $expense['cattle_id'] = $this->input->post('cattle_id');
                            $expense['ai_date'] = date('Y-m-d');
                            $expense['history'] = [];
                            $result =['status' => '1','result' => $expense ];            
                        }                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

        public function set_breeding_process(){
            $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  //print_r($checklogin); die;
                   $user_id =  $checklogin['userdata']['id'];
                   //$cattle = $this->BreedingProcess->get_one($this->input->post('id'));
                   
                    $this->form_validation->set_rules('ai_on', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('first_us_pd_on', 'First Ultra Sound PD Date', 'required');
                    $this->form_validation->set_rules('first_manual_pd_on', 'First Manual PD Date', 'required');
                    $this->form_validation->set_rules('second_us_pd_on', 'Second Ultra Sound PD Date', 'required');
                    $this->form_validation->set_rules('second_manual_pd_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('dry_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('steam_up_on', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('delivery_on', 'Weight', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                       // $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        $data = array(
                            'ai_on' => $this->input->post('ai_on'),
                            'first_us_pd_on' => $this->input->post('first_us_pd_on'),
                            'first_manual_pd_on' => $this->input->post('first_manual_pd_on'),
                            'second_us_pd_on' => $this->input->post('second_us_pd_on'),
                            'second_manual_pd_on' => $this->input->post('second_manual_pd_on'),
                            'cattle_id' => $this->input->post('cattle_id'),
                            'dry_on' => $this->input->post('dry_on'),
                            'steam_up_on' => $this->input->post('steam_up_on'),
                            'delivery_on' => $this->input->post('delivery_on'),
                        );
                       // $this->Cattle_model->insert_entry($data);
                        if($this->input->post('id') == 0){
                          $this->BreedingProcess->insert_entry($data);
                        } else {
                          $this->BreedingProcess->update_entry($this->input->post('id'), $data);
                        }
                        
                        $result = ['status' => '1','message' => 'Breeding Process Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }


        public function update_breeding_process(){
            //'ai','first_pd','second_pd','pregnancy_confirmation','dry','steam_up','delivery',
              $this->load->library('form_validation');
             $post = $this->input->post();
             $checklogin = $this->checklogin($post);             
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('History_model'));
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                    //$this->form_validation->set_rules('ai_on', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('ai_date', 'Arificial Insemination', 'required');
                    $this->form_validation->set_rules('first_pd_on', 'First  PD Days', 'required');
                    $this->form_validation->set_rules('first_pd_type', 'First PD Type', 'required');
                    $this->form_validation->set_rules('second_pd_on', 'Second  PD Date', 'required');
                    $this->form_validation->set_rules('second_pd_type', 'Second  PD Type', 'required');
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('dry_on', 'Second Manual PD Date', 'required');
                    $this->form_validation->set_rules('steam_up_on', 'Purchase Price', 'required|integer');
                    $this->form_validation->set_rules('delivery_on', 'Weight', 'required|integer');

                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $data = array(
                                      'ai_on' => 0,
                                      'first_pd_on' => $this->input->post('first_pd_on'),
                                      'first_pd_type' => $this->input->post('first_pd_type'),
                                      'second_pd_on' => $this->input->post('second_pd_on'),
                                      'second_pd_type' => $this->input->post('second_pd_type'),
                                      'cattle_id' => $this->input->post('cattle_id'),
                                      'dry_on' => $this->input->post('dry_on'),
                                      'steam_up_on' => $this->input->post('steam_up_on'),
                                      'delivery_on' => $this->input->post('delivery_on'),
                                  );


                        $formdata = $this->input->post();
                        $already = $this->BreedingProcess->get_one_by_cattle_id($this->input->post('cattle_id'));
                        $default = $this->BreedingProcess->get_one(1);
                        //echo "if"; die;
                        if(count($already) > 0){
                            // IF BREEDING PROCESS IS ALREADY THERE
                            $current_state = $already['current_state'];
                            if(date_create($formdata['ai_date'])->format('Y-m-d') !== date_create($already['ai_date'])->format('Y-m-d')){
                              // IF POSTED AI DATE DOES NOT MATCHED THE DATE IN DB
                              // DELETED OLD BREEDING PROCESS RECORD AND WILL CREATE NEW
                              $this->BreedingProcess->delete($already['id']);
                              $data['ai_on'] = 0;
                              $data['ai_date'] = date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s');
                              $data['cattle_id'] = $this->input->post('cattle_id');
                              $data['current_state'] = 'ai';
                              $event_date = new DateTime($data['ai_date']);
                              $event_type = 'ai';
                              $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 

                              $alertdata = [
                                'cattle_id' => $this->input->post('cattle_id'),
                                'event_type' => $event_type,
                                'message' => $event_message,
                                'event_date' => $event_date->format('Y-m-d 00:00:00')
                               ];
                                //print_r($event_date); die;
                              $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('ai',$this->input->post('cattle_id'));
                              $this->UpcomingEvent_model->delete($u['id']); 
                              $this->UpcomingEvent_model->insert_entry($alertdata); 



                              if(!empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                              } else {
                                  $first_pd_type = 'UltraSound';
                              }

                              if(empty($this->input->post('first_pd_on'))){
                                  $first_pd_on = $default['first_manual_pd_on'];
                                  if($first_pd_type == 'UltraSound'){
                                    $first_pd_on = $default['second_us_pd_on'];
                                  }                                
                              } else {
                                  $first_pd_on = $this->input->post('first_pd_on');
                              }
                              
                              $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                              $event_date->modify("+".$first_pd_on." day");
                              $event_type = 'first_pd';
                              $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');

                              $data['first_pd_type'] = $first_pd_type;
                              $data['first_pd_on'] = $first_pd_on;
                              $data['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                              $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('first_pd',$this->input->post('cattle_id'));
                              $this->UpcomingEvent_model->delete($u['id']);
                               $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                  ]; 
                              

                              $this->UpcomingEvent_model->insert_entry($alertdata);
                              $this->BreedingProcess->insert_entry($data);
                              $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),[]);
                              $result =['status' => '1','result' => $data ]; 
                            } else {

                              // IF POSTED AI DATE MATCHED THE DATE IN DB
                              // MEANS BREEDING PROCESSING IS MOVING FORWARD
                              if($current_state == 'ai' &&  $post['first_pd_on'] !== $already['first_pd_on']){
                                // IF AI IS DONE BUT FIRST PD CHANGED BEFORE GETTING DONE

                                $first_pd_type = $post['first_pd_type'];
                                if(empty($this->input->post('first_pd_on'))){
                                    $first_pd_on = $default['first_manual_pd_on'];
                                    if($first_pd_type == 'UltraSound'){
                                      $first_pd_on = $default['second_us_pd_on'];
                                    }                                
                                } else {
                                    $first_pd_on = $this->input->post('first_pd_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('first_pd',$this->input->post('cattle_id'));
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$first_pd_on." day");
                                $event_type = 'first_pd';
                                $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');
                                //SAVING DATA TO BREEDING TABLE
                                $data['first_pd_on'] = $first_pd_on;
                                $data['first_pd_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');
                                $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                  ];
                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 

                              if($current_state == 'first_pd' &&  $post['second_pd_on'] !== $already['second_pd_on']) {
                                // IF AI IS DONE BUT Second PD CHANGED BEFORE GETTING DONE
                                $second_pd_type = $post['second_pd_type'];

                                if(empty($this->input->post('second_pd_on'))){
                                    $second_pd_on = $default['second_manual_pd_on'];
                                    if($second_pd_type == 'UltraSound'){
                                      $second_pd_on = $default['second_us_pd_on'];
                                    }                                
                                } else {
                                    $second_pd_on = $this->input->post('second_us_pd_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('second_pd',$this->input->post('cattle_id'));
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['first_pd_date']);
                                $event_date->modify("+".$second_pd_on." day");
                                $event_type = 'second_pd';
                                $event_message = 'Second PD is going to take place on '.$event_date->format('Y-m-d');

                                $data['second_pd_on'] = $second_pd_on;
                                $data['second_pd_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');


                                $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                            ];
                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 

                              if($current_state == 'second_pd' &&  $post['dry_on'] !== $already['dry_on']) {
                                  $dry_on = $post['dry_on'];
                                if(empty($this->input->post('dry_on'))){
                                    $dry_on = $default['dry_on'];
                                } else {
                                    $dry_on = $this->input->post('dry_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('dry',$this->input->post('cattle_id'));
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['second_pd_date']);
                                $event_date->modify("+".$dry_on." day");
                                $event_type = 'dry';
                                $event_message = 'Putting Cattle on Dry is going to take place on '.$event_date->format('Y-m-d'); 

                                $data['dry_on'] = $dry_on;
                                $data['dry_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');

                                $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                            ];
                            
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } 
                               if($current_state == 'dry' &&  $post['steam_up'] !== $already['steam_up']) {
                                  $steam_up_on = $post['steam_up_on'];
                                if(empty($this->input->post('steam_up_on'))){
                                    $steam_up_on = $default['steam_up_on'];
                                } else {
                                    $steam_up_on = $this->input->post('steam_up_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('steam_up',$this->input->post('cattle_id'));
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['second_pd_date']);
                                $event_date->modify("+".$steam_up_on." day");
                                $event_type = 'steam_up';
                                $event_message = 'Putting Cattle on Steam Up is going to take place on '.$event_date->format('Y-m-d'); 

                                $data['steam_up_on'] = $steam_up_on;
                                $data['steam_up_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');

                                $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                ];
                            
                            $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
    
                              } 

                               if($current_state == 'steam_up' &&  $post['delivery'] !== $already['delivery']) {
                                  $delivery_on = $post['delivery_on'];
                                if(empty($this->input->post('delivery_on'))){
                                    $delivery_on = $default['delivery_on'];
                                } else {
                                    $delivery_on = $this->input->post('delivery_on');
                                }
                                $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$this->input->post('cattle_id'));
                                $this->UpcomingEvent_model->delete($u['id']);
                                $event_date = new DateTime($already['second_pd_date']);
                                $event_date->modify("+".$delivery_on." day");
                                $event_type = 'steam_up';
                                $event_message = 'Putting Cattle on Steam Up is going to take place on '.$event_date->format('Y-m-d'); 

                                $data['delivery_on'] = $delivery_on;
                                $data['delivery_date'] = $event_date->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');
                                $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                                 ];
                                
                                $this->UpcomingEvent_model->insert_entry($alertdata);
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              } else {
                                // IF NOTHING CHANGED WE WILL SAVE FORMDATA AS IT IS 
                                //$this->BreedingProcess->delete($already['id']);
                                //$data['ai_on'] = 0;
                                $data['ai_date'] = date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s');
                                $data['cattle_id'] = $this->input->post('cattle_id');
                                $event_type = $already['current_state'];
                                $this->BreedingProcess->update_entry($already['id'], $data);
                                //$this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),$already);
                                $result =['status' => '1','result' => $data ];
                              }
                              
                              
                            }
                            
                        } else {
                              //IF THERE IS NO BREEDING PROCESS RECORD IN DB
                              // WE WILL BE INSERTING NEW RECORD TO DB
                              $data['ai_on'] = 0;
                              $data['ai_date'] = date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s');
                              $data['cattle_id'] = $this->input->post('cattle_id');
                              $data['current_state'] = 'ai';

                              $event_date = new DateTime($data['ai_date']);
                              $event_date->modify("+".$on." day");
                              $event_type = 'ai';
                              $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 

                              $alertdata = [
                                'cattle_id' => $this->input->post('cattle_id'),
                                'event_type' => $event_type,
                                'message' => $event_message,
                                'event_date' => $event_date->format('Y-m-d 00:00:00')
                               ];
                                //print_r($event_date); die;
                              $this->UpcomingEvent_model->insert_entry($alertdata); 
                              if(!empty($this->input->post('first_pd_type'))){
                                $first_pd_type = $this->input->post('first_pd_type');
                              } else {
                                  $first_pd_type = 'UltraSound';
                              }

                              if(empty($this->input->post('first_pd_on'))){
                                  $first_pd_on = $default['first_manual_pd_on'];
                                  if($first_pd_type == 'UltraSound'){
                                    $first_pd_on = $default['second_us_pd_on'];
                                  }                                
                              } else {
                                  $first_pd_on = $this->input->post('first_pd_on');
                              }

                              $event_date = new DateTime(date_create($this->input->post('ai_date'))->format('Y-m-d H:i:s'));
                              //print_r($event_date); die;
                              $event_date->modify("+".$first_pd_on." day");
                              $event_type = 'first_pd';
                              $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');

                              //print_r($event_message); die;

                              $data['first_pd_type'] = $first_pd_type;
                              $data['first_pd_on'] = $first_pd_on;
                              $data['first_pd_date'] = $event_date->format('Y-m-d 00:00:00');
                            

                            $result =['status' => '1','result' => $data ];   

                            $alertdata = [
                                  'cattle_id' => $this->input->post('cattle_id'),
                                  'event_type' => $event_type,
                                  'message' => $event_message,
                                  'event_date' => $event_date->format('Y-m-d')
                              ];
                            //print_r($event_date); die;
                            $this->UpcomingEvent_model->insert_entry($alertdata);    
                            $this->BreedingProcess->insert_entry($data);
                            $this->update_event($post,$event_type, $event_date, $this->input->post('cattle_id'),[]);     
                        }
                             // $this->form_validation->set_rules('first_pd_type', 'First PD ty', 'required|integer|is_unique[cattles.tag_id]');
                            
                        
                    }
             }
                 return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));


        }


        public function update_state(){
            $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('History_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                   $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('type', 'Breeding Process Type', 'required');
                    $this->form_validation->set_rules('cattle_id[]', 'Cattle ID(s)', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        $cattles = $this->input->post('cattle_id');
                        if(count($cattles) > 0){
                          foreach ($cattles as $cattle) {
                             $already = $this->BreedingProcess->get_one_by_cattle_id($cattle);
                              $default = $this->BreedingProcess->get_one(1);
                              $save_data = [];
                              
                              if($formdata['type'] == 'ai'){
                                 $save_data['is_ai_done'] = 'Yes';
                                 $save_data['ai_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'ai';

                                 $history_event_type = 'ai';
                                 $history_event_date = $save_data['ai_date'];
                                 $history_ai_date = $save_data['ai_date'];
                                 $history_message = 'AI was Done'. date('Y-m-d');
                                 $on = $already['first_pd_on'];                                
                                // $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$cattle);
                               
                                $event_date = new DateTime($save_data['ai_date']);
                                $event_date->modify("+".$on." day");                                
                                $event_type = 'first_pd';
                                $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d'); 


                              } else if($formdata['type'] == 'first_pd'){
                                 $save_data['is_first_pd_done'] = 'Yes';
                                 $save_data['first_pd_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'first_pd';
                                 
                                 $history_event_type = 'first_pd';
                                 $history_event_date = $save_data['first_pd_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'First PD Done on'. date('Y-m-d');
                                 $on = $already['second_pd_on'];
                               //echo $already['ai_date']; die;
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'second_pd';
                                $event_message = 'Second PD is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'second_pd'){
                                 $save_data['is_second_pd_done'] = 'Yes';
                                 $save_data['second_pd_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'second_pd';
                                 
                                 $history_event_type = 'second_pd';
                                 $history_event_date = $save_data['second_pd_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Second PD Done on'. date('Y-m-d');
                                 $on = $already['dry_on'];
                                
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'dry';
                                $event_message = 'Putting Cattle on Dry is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'dry'){
                                 $save_data['is_dry_done'] = 'Yes';
                                 $save_data['dry_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'dry';
                                 
                                 $history_event_type = 'dry';
                                 $history_event_date = $save_data['dry_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Putting On Dry Done on'. date('Y-m-d');
                                 $on = $already['steam_up_on'];
                               
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'steam_up';
                                $event_message = 'Putting Cattle on Steam Up is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'steam_up'){
                                 $save_data['is_steam_up_done'] = 'Yes';
                                 $save_data['steam_up_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'steam_up';
                                 
                                 $history_event_type = 'steam_up';
                                 $history_event_date = $save_data['steam_up_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Steam Up Done on'. date('Y-m-d');
                                 $on = $already['delivery_on'];
                               
                                $event_date = new DateTime($already['ai_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'delivery';
                                $event_message = 'Delivery is going to take place on '.$event_date->format('Y-m-d'); 
                              } else if($formdata['type'] == 'delivery'){
                                 $save_data['is_delivery_done'] = 'Yes';
                                 $save_data['delivery_date'] = date('Y-m-d 00:00:00');
                                 $save_data['current_state'] = 'delivery';
                                 
                                 $history_event_type = 'delivery';
                                 $history_event_date = $save_data['delivery_date'];
                                 $history_ai_date = $already['ai_date'];
                                 $history_message = 'Delivery Done on'. date('Y-m-d');
                                 $on = 45;

                                $event_date = new DateTime($already['delivery_date']);
                                $event_date->modify("+".$on." day");
                                $event_type = 'ai';
                                $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                              }

                             

                              $history = [
                                    'cattle_id' => $cattle,
                                    'event_type' => $history_event_type,
                                    'event_date' => $history_event_date,
                                    'ai_date' => $history_ai_date,
                                    'message' => $history_message
                                ];

                                $alertdata = [
                                        'cattle_id' => $cattle,
                                        'event_type' => $event_type,
                                        'message' => $event_message,
                                        'event_date' => $event_date->format('Y-m-d')
                                 ];
                                 $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id($event_type,$cattle);
                                 if(count($u) > 0){
                                       $this->UpcomingEvent_model->delete($u['id']);
                                 }
                                  $this->UpcomingEvent_model->insert_entry($alertdata);                                    
                                  $this->History_model->insert_entry($history);
                                  $this->BreedingProcess->update_entry_by_cattle_id($cattle, $save_data);
                                 if($formdata['type'] == 'delivery'){
                                    $this->BreedingProcess->delete($already['id']);
                                 } 
                          }

                        }
                        $result = ['status' => '1','message' => 'Breeding Process Updated Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));  
        }


        public function get_events_count(){
             $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                   $ai = $this->UpcomingEvent_model->get_by_type('ai');
                   $first_pd = $this->UpcomingEvent_model->get_by_type('first_pd');
                   $second_pd = $this->UpcomingEvent_model->get_by_type('second_pd');
                   $dry = $this->UpcomingEvent_model->get_by_type('dry');
                   $steam_up = $this->UpcomingEvent_model->get_by_type('steam_up');
                   $delivery = $this->UpcomingEvent_model->get_by_type('delivery');
                   $result['ai'] = ['total' => count($ai),'IDs' => count($ai) ? $ai : [] ];
                   $result['first_pd'] = ['total' => count($first_pd),'IDs' => count($first_pd) ? $first_pd : [] ];
                   $result['second_pd'] = ['total' => count($second_pd),'IDs' => count($second_pd) ? $second_pd : [] ];
                   $result['dry'] = ['total' => count($dry),'IDs' => count($dry) ? $dry : [] ];
                   $result['steam_up'] = ['total' => count($steam_up),'IDs' => count($steam_up) ? $steam_up : [] ];
                   $result['delivery'] = ['total' => count($delivery),'IDs' => count($delivery) ? $delivery : [] ];
                   $result = ['status' => '1','result' => $result];
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

        public function cattle_history(){
             $this->load->library('form_validation');
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('History_model'));
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                   $result = $checklogin;
             } else{
                    $this->form_validation->set_rules('cattle_id', 'Cattle ID', 'required');
                    $this->form_validation->set_rules('page', 'Page', 'required');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                        $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                       $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                                $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->History_model->get_total($formdata);                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->History_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                        $result = $params;
                    }          
                   
             }
                   return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

        }

        //public function savereference


        


}
