<?php

class Agency_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('agencies', 10);
                return $query->result();
        }

        public function get_all($userdata)
        {       
                $this->db->select('id,tag_id,dam_id,dob, dop, purchase_price, weight,ai_date,is_pregnant,calving_date,sale_date,death_date,sale_price,sire_id,per_day_milk,breed,type,lactation');
                $this->db->where('owner_id',$userdata['id']);
                $this->db->where('is_deleted','No');
                $this->db->order_by("id", "desc");
                $query = $this->db->get('agencies');
                if ($query->num_rows() > 0)
                {
                    foreach ($query->result_array() as $row)
                    {   
                       //echo $row['calving_date']; die;
                        $row['ai_date'] = ($row['ai_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['ai_date'])->format('Y-m-d');
                        $row['dop'] =($row['dop'] == '0000-00-00 00:00:00') ? '' : date_create($row['dop'])->format('Y-m-d');
                        $row['dob'] = ($row['dob'] == '0000-00-00 00:00:00') ? '' : date_create($row['dob'])->format('Y-m-d');
                        $row['sale_date'] = ($row['sale_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['sale_date'])->format('Y-m-d');
                        $row['calving_date'] =($row['calving_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['calving_date'])->format('Y-m-d');
                        $row['death_date'] =($row['death_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['death_date'])->format('Y-m-d');
                        $data[] = $row;
                    }
                    return $data;
                }
              
            return false;
        }

         public function get_all_agencies($userdata)
        {       
                $this->db->select('id,tag_id,dam_id,dob, dop, purchase_price, weight,ai_date,is_pregnant,calving_date,sale_date,death_date,sale_price,sire_id,per_day_milk,breed,type,lactation');
                $this->db->where('owner_id',$userdata['id']);
                $this->db->where('is_deleted','No');
                $this->db->where('type <>','Calf');
                $this->db->order_by("id", "desc");
                $query = $this->db->get('agencies');
                if ($query->num_rows() > 0)
                {
                    foreach ($query->result_array() as $row)
                    {   
                       //echo $row['calving_date']; die;
                        $row['ai_date'] = ($row['ai_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['ai_date'])->format('Y-m-d');
                        $row['dop'] =($row['dop'] == '0000-00-00 00:00:00') ? '' : date_create($row['dop'])->format('Y-m-d');
                        $row['dob'] = ($row['dob'] == '0000-00-00 00:00:00') ? '' : date_create($row['dob'])->format('Y-m-d');
                        $row['sale_date'] = ($row['sale_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['sale_date'])->format('Y-m-d');
                        $row['calving_date'] =($row['calving_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['calving_date'])->format('Y-m-d');
                        $row['death_date'] =($row['death_date'] == '0000-00-00 00:00:00') ? '' : date_create($row['death_date'])->format('Y-m-d');
                        $data[] = $row;
                    }
                    return $data;
                }
              
            return false;
        }


        public function insert_entry($data)
        {
            $this->db->insert('agencies', $data);
            return $this->db->insert_id();
        }

        public function update_entry($id,$data)
        {
            $this->db->update('agencies', $data, array('id' => $id ));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            $this->db->select('id,tag_id,dam_id,dob, dop, purchase_price, weight,ai_date,is_pregnant,calving_date,sale_date,death_date,sale_price,sire_id,per_day_milk,breed,type,lactation');
            //$this->db->limit($limit, $start);
            $this->db->where('owner_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("agencies");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result_array() as $row)
                {   
                    $row['ai_date'] = date_create($row['ai_date'])->format('Y-m-d');          
                    $row['ai_date'] = date_create($row['ai_date'])->format('Y-m-d');
                    $row['dop'] = date_create($row['dop'])->format('Y-m-d');
                    $row['dob'] = date_create($row['dob'])->format('Y-m-d');
                    $row['sale_date'] = date_create($row['sale_date'])->format('Y-m-d');
                    $row['calving_date'] = date_create($row['calving_date'])->format('Y-m-d');
                    $row['death_date'] = date_create($row['death_date'])->format('Y-m-d');
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons,$userdata)
        {       
                //echo $conditons['id']; die;
                $this->db->where('owner_id',$userdata['id']);
                $this->db->where('is_deleted','No');
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("agencies");
            $total = $this->db->count_all_results();
            return $total;

            //return $this->db->count_all("agencies");
        }

         public function get_one($id){
            $details = $this->db->get_where('agencies', array('id' => $id, 'is_deleted' => 'No'));
            $record_arr = $details->first_row('array');  
            if(count($record_arr) > 0){
                $record_arr['ai_date'] = date_create($record_arr['ai_date'])->format('Y-m-d');          
                $record_arr['ai_date'] = date_create($record_arr['ai_date'])->format('Y-m-d');
                $record_arr['dop'] = date_create($record_arr['dop'])->format('Y-m-d');
                $record_arr['dob'] = date_create($record_arr['dob'])->format('Y-m-d');
                $record_arr['sale_date'] = date_create($record_arr['sale_date'])->format('Y-m-d');
                $record_arr['calving_date'] = date_create($record_arr['calving_date'])->format('Y-m-d');
                $record_arr['death_date'] = date_create($record_arr['death_date'])->format('Y-m-d');    
            }
            return $record_arr;
        }


         public function get_one_from_tag($id){
            $details = $this->db->get_where('agencies', array('tag_id' => $id , 'is_deleted' => 'No'));
            $record_arr = $details->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['ai_date'] = date_create($record_arr['ai_date'])->format('Y-m-d');
                $record_arr['dop'] = date_create($record_arr['dop'])->format('Y-m-d');
                $record_arr['dob'] = date_create($record_arr['dob'])->format('Y-m-d');
                $record_arr['sale_date'] = date_create($record_arr['sale_date'])->format('Y-m-d');
                $record_arr['calving_date'] = date_create($record_arr['calving_date'])->format('Y-m-d');
                $record_arr['death_date'] = date_create($record_arr['death_date'])->format('Y-m-d');    
            }
            
            return $record_arr;
        }

        public function get_one_from_tag_and_user($id,$userdata){
            //echo $id; die;
            $details = $this->db->get_where('agencies', array('tag_id' => $id ,'owner_id'=> $userdata['id'] , 'is_deleted' => 'No'));
            $record_arr = $details->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['ai_date'] = date_create($record_arr['ai_date'])->format('Y-m-d');
                $record_arr['dop'] = date_create($record_arr['dop'])->format('Y-m-d');
                $record_arr['dob'] = date_create($record_arr['dob'])->format('Y-m-d');
                $record_arr['sale_date'] = date_create($record_arr['sale_date'])->format('Y-m-d');
                $record_arr['calving_date'] = date_create($record_arr['calving_date'])->format('Y-m-d');
                $record_arr['death_date'] = date_create($record_arr['death_date'])->format('Y-m-d');    
            }
            //print_r($record_arr); die;
            return $record_arr;
        }

         public function delete($id){
             //echo $id; die;
             $this->db->where('id', $id);
             $this->db->delete('agencies'); 
            return true;
        }

        public function soft_delete($id){
             //echo $id; die;
            //$data = ['is_deleted' => 'Yes'];
             $this->db->update('agencies', ['is_deleted' => 'Yes'] , array('id' => $id ));
            return true;
        }

         public function get_all_cattle_ids($userdata) {       
            $this->db->select('id');
            $this->db->where('owner_id',$userdata['id']);
            $this->db->where('is_deleted','No');
            $query = $this->db->get('agencies');
            return $query->result_array();
        }

        //
        public function find_by_name($name){
            $details = $this->db->get_where('agencies', array('agency_name' => $name ));
            $record_arr = $details->first_row('array');  
            return $record_arr;  
        }
        


}


 ;?>