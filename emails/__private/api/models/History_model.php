<?php

class History_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('cattle_history', 10);
                return $query->result();
        }

        public function get_all($id)
        {       
                $this->db->where('cattle_id',$id);
                $this->db->order_by("id", "desc");
                $query = $this->db->get('cattle_history');
                $data = [];
                foreach ($query->result_array() as $row)
                {   
                    $row['event_date'] = date_create($row['event_date'])->format('Y-m-d');
                    $row['ai_date'] = date_create($row['ai_date'])->format('Y-m-d');
                    $row['created_at'] = date_create($row['created_at'])->format('Y-m-d');
                    $row['updated_at'] = date_create($row['updated_at'])->format('Y-m-d');
                    $data[] = $row;
                }
                return $data;
               // return $query->result();
        }

        

        public function insert_entry($data)
        {
              $this->db->insert('cattle_history', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('cattle_history', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            $this->db->limit($limit, $start);
            $this->db->where('cattle_id',$conditons['cattle_id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            

            $query = $this->db->get("cattle_history");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                   
                }
                 //print_r($data); die;
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons)
        {   
            //print_r($userdata); die;
            $this->db->where('cattle_id',$conditons['cattle_id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("cattle_history");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $this->db->get_where('cattle_history', array('id' => $id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }


}


 ;?>