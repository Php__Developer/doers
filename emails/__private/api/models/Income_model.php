<?php

class Income_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('income', 10);


                //return $query->result();
        }

        public function get_all($userdata)
        {
                $this->db->order_by("id", "desc");
                $this->db->select('id,income_date, milk_sale_rate, manure_sale_rate, milk_sale, manure_sale,milk_incentive,others,total');
                $this->db->where('user_id',$userdata['id']);
                $query = $this->db->get('income');
                if ($query->num_rows() > 0)
                {
                    foreach ($query->result_array() as $row)
                    {
                        $row['income_date'] = date_create($row['income_date'])->format('Y-m-d');        
                        $data[] = $row;
                    }
                     
                    return $data;
                }
              
                return false;
                return $query->result();
        }

        public function get_all_today($userdata)
        {
                   //echo date('Y-m-d'); die;
                $this->db->order_by("id", "desc");                 
                $this->db->where('user_id',$userdata['id']);
                $this->db->where('income_date' , date('Y-m-d', strtotime('-1 day')) );
                $query = $this->db->get('income');
                if ($query->num_rows() > 0)
                {

                    
                    foreach ($query->result_array() as $row)
                    {
                        $row['income_date'] = date_create($row['income_date'])->format('Y-m-d');        
                        $data[] = $row;
                    }
                     
                    return $data;
                }
              
                return false;
                return $query->result_array();
        }

        public function get_all_current_month($userdata)
        {
                $month = date('m');
                $year = date('Y');
                $month = (int) date('n', strtotime('-1 months'));
                $year = (int) date('Y', strtotime('-1 months'));
                $this->db->where('user_id',$userdata['id']);
                $this->db->where('MONTH(income_date)', $month);
                $this->db->where('YEAR(income_date)', $year);
                $query = $this->db->get('income');
                if ($query->num_rows() > 0)
                {
                    //print_r($query->result_array()); die;
                    foreach ($query->result_array() as $row)
                    {
                        $row['income_date'] = date_create($row['income_date'])->format('Y-m-d');        
                        $data[] = $row;
                    }
                     
                    return $data;
                }
              
                return false;
                return $query->result_array();
        }

        
        

        public function insert_entry($data)
        {
              
                $this->db->insert('income', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('income', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;
            $this->db->select('id,income_date, milk_sale_rate, manure_sale_rate, milk_sale, manure_sale, others,total');
            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('income_date >=',$conditons['start_date']);
                $this->db->where('income_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("income");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons,$userdata)
        {
            $this->db->where('user_id',$userdata['id']);    
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('income_date >=',$conditons['start_date']);
                $this->db->where('income_date <=', $conditons['end_date']);
            }
            $this->db->from("income");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $this->db->get_where('income', array('id' => $id));
            $record_arr = $details->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['income_date'] = date_create($record_arr['income_date'])->format('Y-m-d');    
            }
            return $record_arr;
        }

         public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('income'); 
            return true;
        }

        public function get_by_date($date)
        { 
                //$query = $this->db->get('income');
                $this->db->where('created_at', $date);
                $this->db->select_sum('milk_sale');
                $query = $this->db->get('income');
                //print_r($query->result_array()); die;
                return $query->result_array();
        }

}


 ;?>