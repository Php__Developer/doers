<?php

class ProductRequest_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('product_request', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('product_request');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
                /*$this->first_name    = $_POST['title']; // please read the below note
                $this->_name  = $_POST['content'];
                $this->date     = time();
*/
                $this->db->insert('product_request', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('product_request', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;

            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("product_request");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($userdata)
        {   
            //print_r($userdata); die;
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("product_request");
            $total = $this->db->count_all_results();
            return $total;

            //return $this->db->count_all("product_request");
        }

         public function get_one($id){
            $details = $this->db->get_where('product_request', array('id' => $id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

         public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('product_request'); 
            return true;
        }


}


 ;?>