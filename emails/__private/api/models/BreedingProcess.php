<?php

class BreedingProcess extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('breeding_process', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('breeding_process');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
               
                $this->db->insert('breeding_process', $data);
        }

        public function update_entry($id,$data)
        {

            $this->db->update('breeding_process', $data, array('id' => $id ));
            //echo $id; die;
            
        }

        public function update_entry_by_cattle_id($id,$data)
        {
               
            $this->db->update('breeding_process', $data, array('cattle_id' => $id ));
            /*if($id == 53){
                echo "done"; die;
            }*/
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;

            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("breeding_process");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($userdata)
        { 
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("breeding_process");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $query = $this->db->get('breeding_process',1);
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_one_by_cattle_id($id){
            $details = $this->db->where('cattle_id',$id);
            $query = $this->db->get("breeding_process");
            $record_arr = $query->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['ai_date'] = date_create($record_arr['ai_date'])->format('Y-m-d');    
            }
            
            //$record_arr['first_pd_date'] = date_create($record_arr['first_pd_date'])->format('d/m/Y');
            //$record_arr['second_pd_date'] = date_create($record_arr['second_pd_date'])->format('d/m/Y');
            
            
            return $record_arr;
        }

        public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('breeding_process'); 
            return true;
        }
        


}


 ;?>