<?php

class DeliveryData extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('delivery_data', 10);
                return $query->result();
        }

        public function get_all($id)
        {       
                $this->db->where('cattle_id',$id);
                $this->db->order_by("id", "desc");
                $query = $this->db->get('delivery_data');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
               $this->db->insert('delivery_data', $data);
               //$this->db->insert('posts', $post_data);
               $insert_id = $this->db->insert_id();
               return  $insert_id;
        }

        public function update_entry($data)
        {
            $this->db->update('delivery_data', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            $this->db->limit($limit, $start);
            $this->db->where('cattle_id',$conditons['cattle_id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            

            $query = $this->db->get("delivery_data");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                   
                }
                 //print_r($data); die;
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons)
        {   
            //print_r($userdata); die;
            $this->db->where('cattle_id',$conditons['cattle_id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("delivery_data");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $this->db->get_where('delivery_data', array('id' => $id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function find_upcomming_by_date_and_cattle_id($delivery_date, $id){

            $this->db->order_by("id", "desc");
            $details = $this->db->get_where('delivery_data', array('delivery_date' => $delivery_date ,'cattle_id'=> $id));
            $record_arr = $details->first_row('array');
            if($id == 53){
               // print_r($delivery_date); die;
             }
            //print_r($record_arr); die;
            return $record_arr;
        }



}


 ;?>