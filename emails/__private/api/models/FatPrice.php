<?php

class FatPrice extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('fat_price', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('fat_price');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
               
                $this->db->insert('fat_price', $data);
        }

        public function update_entry($data)
        {

            $this->db->update('fat_price', $data, array('cattle_id' => $data['cattle_id']));
        }

        public function update_entry_by_cattle_id($id,$data)
        {
               
            $this->db->update('fat_price', $data, array('cattle_id' => $id ));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;

            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("fat_price");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($userdata)
        { 
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("fat_price");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $query = $this->db->get('fat_price',1);
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_one_by_cattle_id($id){
            $details = $this->db->where('cattle_id',$id);
            $query = $this->db->get("fat_price");
            $record_arr = $query->first_row('array');
            return $record_arr;
        }

        public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('fat_price'); 
            return true;
        }
        


}


 ;?>