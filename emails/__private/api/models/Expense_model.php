<?php

class Expense_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('expenses', 10);
                return $query->result();
        }

        public function get_all($userdata)
        {       
                $this->db->order_by("id", "desc");
                $this->db->where('user_id',$userdata['id']);
                $query = $this->db->get('expenses');                
                if ($query->num_rows() > 0)
                {
                    foreach ($query->result_array() as $row)
                    {
                        $row['expense_date'] = date_create($row['expense_date'])->format('Y-m-d');        
                        $data[] = $row;
                    }
                     
                    return $data;
                }
              
                return false;
                return $query->result();
                //return $query->result();
        }

        

        public function insert_entry($data)
        {
                $this->db->insert('expenses', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('expenses', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        { 
            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("expenses");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($userdata)
        {   
            //print_r($userdata); die;
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("expenses");
            $total = $this->db->count_all_results();
            return $total;

            //return $this->db->count_all("expenses");
        }

         public function get_one($id){
            $details = $this->db->get_where('expenses', array('id' => $id));
            $record_arr = $details->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['expense_date'] = date_create($record_arr['expense_date'])->format('Y-m-d');    
            }            
            return $record_arr;
        }

         public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('expenses'); 
            return true;
        }


}


 ;?>