<?php

class UpcomingEvent_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('upcoming_events', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('upcoming_events');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
                $this->db->insert('upcoming_events', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('upcoming_events', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {  
            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("upcoming_events");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
            return false;
        }

        public function get_total($userdata)
        {   
            //print_r($userdata); die;
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("upcoming_events");
            $total = $this->db->count_all_results();
            return $total;

            //return $this->db->count_all("upcoming_events");
        }

         public function get_one($id){
            $details = $this->db->get_where('upcoming_events', array('id' => $id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('upcoming_events'); 
            return true;
        }

        public function delete_by_type($cattle_id,$type){
            //echo $type; die;
             $this->db->where('cattle_id', $cattle_id);
             $this->db->where('event_type', $type);
             $this->db->delete('upcoming_events'); 
            return true;
        }

        public function find_upcomming_by_type_and_cattle_id($type,$cattle_id){
            $details = $this->db->get_where('upcoming_events', array('event_type' => $type,'cattle_id' => $cattle_id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_by_type($type,$userdata){
            $this->load->model(array('Cattle_model'));
            //$mycattles = $this->Cattle_model->get_all_cattle_ids($userdata);
            $query = $this->db->get_where('upcoming_events', array('event_type' => $type,'event_date <=' => date('Y-m-d 00:00:00')));
            if ($query->num_rows() > 0)
            {   
                $data = [];
                foreach ($query->result() as $row)
                {   
                    $cattle = $this->Cattle_model->get_one($row->cattle_id);
                    //echo $userdata['id']; die;

                    //echo $cattle['owner_id']; die;
                    if(count($cattle) > 0){
                       if(($cattle['owner_id']  == $userdata['id'])){
                        //if(($userdata['id']  == $userdata['id'])){
                            $data[] = ['id' => $row->id , 'cattle_id' => $row->cattle_id,'tag_id' => $cattle['tag_id'] , 'date' => date_create($row->event_date)->format('d/m/Y') ];        
                        }
                    }
                }
                return $data;
            }
            return [];
        }
}


 ;?>