<?php

class User_model extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function get_last_ten_entries()
        {
                $query = $this->db->get('users', 10);
                return $query->result_array();
        }

        public function get_all()
        {       
                $this->db->order_by("id", "desc");
                $query = $this->db->get('users');
                return $query->result_array();
        }

        public function insert_entry($data)
        {       
                 //print_r($data)   ; die;
                 $this->db->insert('users', $data);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();
                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

        public function login($data){
            $details = $this->db->get_where('users', array('username' => $data['username']));
            $record_arr = $details->first_row('array');
            if(count($details) > 0){
               
                if (password_verify($data['password'],$record_arr['password'])) {
                    unset($record_arr['password']);
                    unset($record_arr['created_at']);
                    unset($record_arr['updated_at']);
                     $userdata = ['name' => $record_arr['first_name'].' '.$record_arr['last_name'] , 'username' => $record_arr['username'] , 'token' => $record_arr['employee_code']    ];
                     //print_r($userdata); die;
                     $result = ['status' => 'success' , 'message' => 'Logged In Successfully!', 'userdata' => $userdata];
                  
                } else {
                    $result = ['status' => 'failed' , 'reason' => 'common' , 'message' => 'Invalid UserName or Password'];
                } 
                   
            } else {
                //$result = ['status' => '0' ,'errors' => ['message' => 'Invalid Username or Password'], 'reason' => 'Record not found'];
                $result = ['status' => 'failed' , 'reason' => 'common' , 'message' => 'Invalid UserName or Password'];
            }
            return $result;

        }


        public function get_one($key){
            $details = $this->db->get_where('users', array('api_key' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_one_by_empcode($key){
            $details = $this->db->get_where('users', array('employee_code' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_one_by_email($key){
            $details = $this->db->get_where('users', array('employee_code' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function find_by_agency_and_role($agency_id,$role_id){
            $this->db->select('id');
            $this->db->from('users');
             $this->db->where('find_in_set("'.$role_id.'", role_id) <> 0');
            $this->db->where('agency_id', $agency_id); // Produces: WHERE name = 'Joe'
            $this->db->where('status', '1'); // Produces: WHERE name = 'Joe'
            $query = $this->db->get();
            return $query->result_array();
        }

        public function find_by_agency_and_role_with_leads($agency_id,$role_id){
            $cur=date("Y-m-d");
            $current=date("Y-m-d");
            $current = strtotime($current);
            $current = strtotime("-700 day", $current);
            $setdate= date('Y-m-d', $current);

            $this->db->select('users.id,users.first_name,users.last_name ,leads.salesfront_id , COUNT(leads.id) as leads_count');
            $this->db->from('users');
            $this->db->where('find_in_set("'.$role_id.'", role_id) <> 0');
            $this->db->where('bidding_date >=', $setdate);
            $this->db->where('bidding_date <=', $cur);
            $this->db->join('leads', 'leads.salesfront_id = users.id');
            $this->db->where('users.agency_id', $agency_id); // Produces: WHERE name = 'Joe'
            $this->db->group_by("leads.salesfront_id");
            $this->db->where('users.status', '1'); // Produces: WHERE name = 'Joe'
            $query = $this->db->get();
            return $query->result_array();
        }

        public function reset_password($user){
            $data = [];
            $r = rand(999,99999);
            $data['password'] = password_hash($r, PASSWORD_DEFAULT);
            $this->db->update('users', $data , array('id' => $user['id']));
            return ['password' =>  $r];
        }

        

        

        




}


 ;?>