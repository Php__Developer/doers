<?php

class DailyMilkData extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('per_day_milk_record', 10);


                //return $query->result();
        }

        public function get_all($userdata)
        {
                 $this->db->order_by("id", "desc");
                $this->db->where('user_id',$userdata['id']);
                $query = $this->db->get('per_day_milk_record');
                if ($query->num_rows() > 0)
                {
                    foreach ($query->result_array() as $row)
                    {
                        $row['per_day_milk_record_date'] = date_create($row['per_day_milk_record_date'])->format('Y-m-d');        
                        $data[] = $row;
                    }
                     
                    return $data;
                }
              
                return false;
                return $query->result();
        }

        

        public function insert_entry($data)
        {
              
                $this->db->insert('per_day_milk_record', $data);
        }

        public function update_entry($data,$id)
        {
             $this->db->update('per_day_milk_record', $data, array('id' => $id));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;
            $this->db->select('id,per_day_milk_record_date, milk_sale_rate, manure_sale_rate, milk_sale, manure_sale, others,total');
            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('per_day_milk_record_date >=',$conditons['start_date']);
                $this->db->where('per_day_milk_record_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("per_day_milk_record");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons,$userdata)
        {
            $this->db->where('user_id',$userdata['id']);    
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('per_day_milk_record_date >=',$conditons['start_date']);
                $this->db->where('per_day_milk_record_date <=', $conditons['end_date']);
            }
            $this->db->from("per_day_milk_record");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $this->db->get_where('per_day_milk_record', array('id' => $id));
            $record_arr = $details->first_row('array');
            if(count($record_arr) > 0){
                $record_arr['per_day_milk_record_date'] = date_create($record_arr['per_day_milk_record_date'])->format('Y-m-d');    
            }
            return $record_arr;
        }

        public function get_one_by_cattle_and_date($id,$date){
            $details = $this->db->get_where('per_day_milk_record', array('cattle_id' => $id,'milk_date' => $date));
            $record_arr = $details->first_row('array');            
            return $record_arr;
        }

         public function delete($id){
             $this->db->where('id', $id);
             $this->db->delete('per_day_milk_record'); 
            return true;
        }

        public function get_by_date($date)
        { 
                //$query = $this->db->get('per_day_milk_record');
                $this->db->where('created_at', $date);
                $this->db->select_sum('milk_sale');
                $query = $this->db->get('per_day_milk_record');
                //print_r($query->result_array()); die;
                return $query->result_array();
        }

         public function get_by_cattle_and_date($cattle_id){
            $details = $this->db->where('cattle_id',$cattle_id);
            $this->db->where('milk_date', date('Y-m-d'));
            $query = $this->db->get("per_day_milk_record");
            $record_arr = $query->first_row('array');
            return $record_arr;
        }

}


 ;?>