<?php

class Data_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('data', 10);
                return $query->result();
        }

        public function get_all()
        {
                //echo $this->config->base_url(); die;
            
                $this->db->order_by("id", "desc");
                $query = $this->db->get('data');
                $data = [];
                foreach ($query->result_array() as $row)
                {   
                    $row['image'] = 'http://demo.digizap.in/dawo/admin/'.$row['image'];
                    $data[] = $row;
                }

                return $data;
        }
        public function get_total()
        {
            return $this->db->count_all("data");
        }

        

        public function insert_entry($data)
        {
            $this->db->insert('data', $data);
            return true;
        }

        function update_product($id,$data){
            $this->db->where('id', $id);
            $this->db->update('data', $data);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('data', $this, array('id' => $_POST['id']));
        }

        public function get_one($key){
            $details = $this->db->get_where('data', array('id' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }

        public function get_current_page_records($limit, $start)
        { 
            $this->db->limit($limit, $start);
            $query = $this->db->get("data");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {   
                    //$row['image'] = ''
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

    public function delete($id){
       //$this->load->database();
       $this->db->where('id', $id);
       $this->db->delete('data');
       return true;

    }

}


 ;?>