<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
            $this->load->model(array('LogData')); 
            /*$logdata = [
                 'formdata' => json_encode($this->input->post()),
                 'ip' =>  $this->get_client_ip(),
                 'method_name'  =>  $this->router->fetch_method()
            ];
            $this->LogData->insert_entry($logdata);*/
        }

        function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                echo 'Hello World!';        
                //$this->load->view('welcome_message');
        }

        public function checklogin($data){
           // print_r($data); die;
             $current_route = $this->router->fetch_method();
            $protected_actions = [
                'addexpense',
                'get_expenses',
                'addincome','get_income','get_one_expense','update_expense',
                'get_one_income','update_income','add_cattle','get_cattles','update_cattle','update_state'
                ,'update_state','get_events_count','cattle_history','products','delete_cattle','delete_income','delete_expense',
                'get_fat_price','add_milk_income','update_breeding_process','get_breeding_process','get_fat_price','add_milk',
                'get_milk_data','get_cattle_from_tag','add_delivery_data','confirm_pregnency'
            ];
            //print_r(in_array($current_route, $protected_actions)); die;
            if(in_array($current_route, $protected_actions)){
                if(isset($data['key'])) {
                    $record = $this->User_model->get_one($data['key']);
                    if(count($record) == 0){
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];    
                    } else {
                        $result = ['status' => '1','reason' => 'authorized' , 'userdata' => $record ];    
                    }   
                } else {
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];
                }    
            } else{
                $result = ['status' => '1'];
            }
            return $result;
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

        public function checkadmin($data){
           // print_r($data); die;
             $current_route = $this->router->fetch_method();
            /*$protected_actions = [
                'requests',
            ];*/
            //print_r(in_array($current_route, $protected_actions)); die;
           // if(in_array($current_route, $protected_actions)){
                if(isset($data['key'])) {
                    $record = $this->User_model->get_one($data['key']);
                    if(count($record) == 0){
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];    
                    } else {
                        //print_r($record); die;
                        //echo $record['role_id']; die;
                        if($record['role_id'] == 1 ) {
                            $result = ['status' => '1','reason' => 'authorized' , 'userdata' => $record ];        
                        } else {
                            $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Admin Protected Page' ];    
                        }
                        
                    }   
                } else {
                        $result = ['status' => '0','reason' => 'unauthorized' , 'message' => 'Please Login First' ];
                }    
           /* } else{
                $result = ['status' => '1'];
            }*/
            return $result;
        }




         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            //$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');

            $this->form_validation->set_rules('equipment', 'Equipment', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|min_length[5]|max_length[200]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]|integer',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            //$this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    //echo json_encode(validation_errors());

                /*$arr = array(
                    'field_name_one' => form_error('field_name_one'),
                    'field_name_two' => form_error('field_name_two')
                );*/
                // errors_array()
                //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
                $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'name' => $this->input->post('name'),
                'last_name' => 'None',
                'email' => $this->input->post('email'),
                'equipment' => $this->input->post('equipment'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
               // 'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!','key' => $ref];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
       

        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                 $data = array(
                'email' => $this->input->post('email'),
                'password' =>$this->input->post('password'),
                );
               $result = $this->User_model->login($data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));
        }

          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */

        public function update_event($post,$type,$event_date,$cattle_id,$already){
                //echo $type; die;
             $post['cattle_id'] = $cattle_id;
             $this->load->model(array('BreedingProcess'));
             $this->load->model(array('UpcomingEvent_model'));
             $this->load->model(array('History_model'));
             $default = $this->BreedingProcess->get_one(1);
             $already = $this->BreedingProcess->get_one_by_cattle_id($cattle_id);
             $save_data = [];
             if($type == 'ai'){
                 $save_data['current_state'] = 'ai';
                 $on = $post['first_pd_on'];                                
                 $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('first_pd',$post['cattle_id']);
                 if(count($u)){
                    $this->UpcomingEvent_model->delete($u['id']);
                 }
                $event_date = new DateTime($post['ai_date']);
                $event_date->modify("+".$on." day");                                
                $event_type = 'first_pd';


                $event_message = 'First PD is going to take place on '.$event_date->format('Y-m-d');
                $alertdata = [
                      'cattle_id' => $post['cattle_id'],
                      'event_type' => $event_type,
                      'message' => $event_message,
                      'event_date' => $event_date->format('Y-m-d 00:00:00')
                     ];
                      //print_r($alertdata); die;
               $this->UpcomingEvent_model->insert_entry($alertdata);
               $this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
               $this->BreedingProcess->update_entry($already['id'],['first_pd_date' => $event_date->format('Y-m-d 00:00:00')] );
              } else if($type == 'first_pd'){
                 $save_data['current_state'] = 'first_pd';
                 $on = $post['second_pd_on'];
                 //echo $on; die;
                 $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('second_pd',$post['cattle_id']);
                 if(count($u)){
                    $this->UpcomingEvent_model->delete($u['id']);
                 }
                $event_date = new DateTime($post['ai_date']);
                $event_date->modify("+".$on." day");
                $event_type = 'second_pd';
                $event_message = 'Second PD is going to take place on '.$event_date->format('Y-m-d'); 
                $alertdata = [
                  'cattle_id' => $post['cattle_id'],
                  'event_type' => $event_type,
                  'message' => $event_message,
                  'event_date' => $event_date->format('Y-m-d 00:00:00')
                 ];               
                // print_r($alertdata); die;
                $this->UpcomingEvent_model->insert_entry($alertdata);
                $this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
                $this->BreedingProcess->update_entry($already['id'],['second_pd_date' => $event_date->format('Y-m-d 00:00:00')] );
              } else if($type == 'second_pd'){
                 //$save_data['second_pd_date'] = isset($post['first_pd_date']) ? $post['first_pd_date'] : $default['first_pd_date'];
                 $save_data['current_state'] = 'second_pd';
                 $on = $post['dry_on'];
                 $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('dry',$post['cattle_id']);
                 if(count($u)){
                    $this->UpcomingEvent_model->delete($u['id']);
                 }
                $event_date = new DateTime($post['ai_date']);
                $event_date->modify("+".$on." day");
                $event_type = 'dry';
                $event_message = 'Putting Cattle on Dry is going to take place on '.$event_date->format('Y-m-d'); 
                $alertdata = [
              'cattle_id' => $post['cattle_id'],
              'event_type' => $event_type,
              'message' => $event_message,
              'event_date' => $event_date->format('Y-m-d 00:00:00')
             ];
            
            $this->UpcomingEvent_model->insert_entry($alertdata);
            $this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
            $this->BreedingProcess->update_entry($already['id'],['dry_date' => $event_date->format('Y-m-d 00:00:00')] );
              } else if($type == 'dry'){
                    $save_data['current_state'] = 'dry';
                    $on = $post['steam_up_on'];
                    $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('steam_up',$post['cattle_id']);
                     if(count($u)){
                        //$this->UpcomingEvent_model->delete($u['id']);
                        $this->UpcomingEvent_model->delete_by_type($post['cattle_id'],'steam_up');  
                     }
                    $event_date = new DateTime($post['ai_date']);
                    $event_date->modify("+".$on." day");
                    $event_type = 'steam_up';
                    $event_message = 'Putting Cattle on Steam Up is going to take place on '.$event_date->format('Y-m-d'); 
                    $alertdata = [
                      'cattle_id' => $post['cattle_id'],
                      'event_type' => $event_type,
                      'message' => $event_message,
                      'event_date' => $event_date->format('Y-m-d 00:00:00')
                     ];

                     
                    $this->UpcomingEvent_model->insert_entry($alertdata);
                    $this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
                    $this->BreedingProcess->update_entry($already['id'],['steam_up_date' => $event_date->format('Y-m-d 00:00:00')] );


              } else if($type == 'steam_up'){
                 $save_data['current_state'] = 'steam_up';
                 $on = $post['delivery_on'];
                 $u = $this->UpcomingEvent_model->find_upcomming_by_type_and_cattle_id('delivery',$post['cattle_id']);
                 if(count($u)){
                    $this->UpcomingEvent_model->delete($u['id']);
                 }               
                $event_date = new DateTime($post['ai_date']);
                $event_date->modify("+".$on." day");
                $event_type = 'delivery';
                $event_message = 'Delivery is going to take place on '.$event_date->format('Y-m-d'); 
                $alertdata = [
              'cattle_id' => $post['cattle_id'],
              'event_type' => $event_type,
              'message' => $event_message,
              'event_date' => $event_date->format('Y-m-d 00:00:00')
             ];
             //print_r($alertdata); die;
            $this->UpcomingEvent_model->insert_entry($alertdata);
            $this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
            $this->BreedingProcess->update_entry($already['id'],['delivery_date' => $event_date->format('Y-m-d 00:00:00')] );
              } else if($type == 'delivery'){
                 $save_data['current_state'] = 'delivery';
                 $on = 45;
                $event_date = new DateTime($already['delivery_date']);
                $event_date->modify("+".$on." day");
                $event_type = 'ai';
                $event_message = 'Artificial Insemination is going to take place on '.$event_date->format('Y-m-d'); 
                $alertdata = [
                      'cattle_id' => $post['cattle_id'],
                      'event_type' => $event_type,
                      'message' => $event_message,
                      'event_date' => $event_date->format('Y-m-d 00:00:00')
                     ];
            //$this->UpcomingEvent_model->insert_entry($alertdata);
            //$this->BreedingProcess->update_entry($already['id'],['ai_date' => $event_date->format('Y-m-d 00:00:00')] );
            //$this->update_event($post,$event_type, $event_date, $post['cattle_id'],[]);
              }

             

        }

        public function addexpense(){
             $this->load->library('form_validation');
            //Validating Name Field
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            $this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'gender' => $this->input->post('gender'),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'role_id' => 2,
                    'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));    

        }

        function truncate_float($number, $places) {
        $power = pow(10, $places); 
        if($number > 0){
            return floor($number * $power) / $power; 
        } else {
            return ceil($number * $power) / $power; 
        }
    }

      function sendmail($view,$data,$userdata,$subject,$attachments)
      { 
         /* $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'tls://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'maninder2singh@gmail.com', // change it to yours
            'smtp_pass' => 'g@gmail.com', // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
            80.95.186.235
            mail.demo.digizap.in
          );*/

           $config = Array(
            'protocol' => 'mail',
           /* 'smtp_host' => '80.95.186.235',
            'smtp_port' => 25,
            'smtp_user' => 'noreply@demo.digizap.in', // change it to yours
            'smtp_pass' => 'xJ5GUyD_xYit', // change it to yours*/
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
          );

            $message = $this->load->view($view,$data,true);
            //$this->load->library('email', $config); 
            $this->load->library('email'); 
            $this->email->set_newline("\r\n");
            //$this->email->from($config['smtp_user']); // change it to yours
            $this->email->from('noreply@demo.digizap.in'); // change it to yours
            $this->email->to($userdata['email']);// change it to yours
            $this->email->subject($subject);
            $this->email->message($message);
            if(count($attachments) > 0){
                foreach ($attachments as $a) {
                    $this->email->attach($a);
                }
            }
            if($this->email->send()) {
                $this->email->clear(true);
                return '1';
           } else {
               //show_error($this->email->print_debugger());
               $this->email->clear(true);
                return '1';
          }

      }


      function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

            if ( is_object( $arrays ) ) {
                $arrays = get_object_vars( $arrays );
            }

            foreach ( $arrays AS $key => $value ) {
                $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
                if ( is_array( $value ) OR is_object( $value )  ) {
                    $this->http_build_query_for_curl( $value, $new, $k );
                } else {
                    $new[$k] = $value;
                }
            }
            return $new;
        }

          function post_curl($url,$type,$params) {
        //$checklogin = $this->checklogin($formdata);
        $formdata = $this->input->post();
           /* $post = [
                'username' => 'user1',
                'password' => 'passuser1',
                'gender'   => 1,
            ];*/
            $arr = [];
            //$arr = $this->http_build_query_for_curl( $formdata, $arr );
            //print_r($arr); die;
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if($type == 'POST'){
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($formdata));    
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                             'Content-Type: application/json'                                                                                
                            //'Content-Length: ' . strlen($data_string)
                            )                                                                       
                        );
            $response = curl_exec($ch);
            // close the connection, release resources used
            curl_close($ch);
           // response
            return $response;
            // do anything you want with your response
            //var_dump($response); die;
    }



   

}
