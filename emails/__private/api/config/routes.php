<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Service';
$route['login'] = 'service/login';
$route['register'] = 'service/register';
// Add Expenses ::
$route['addexpense'] = 'service/addexpense';
$route['get_expenses'] = 'service/get_expenses';
$route['get_one_expense'] = 'service/get_one_expense';
$route['update_expense'] = 'service/update_expense';
//Income
$route['addincome'] = 'service/addincome';
$route['get_income'] = 'service/get_income';
$route['get_one_income'] = 'service/get_one_income';
$route['update_income'] = 'service/update_income';
//add cattle
$route['add_cattle'] = 'service/add_cattle';
$route['get_cattle_from_tag'] = 'service/get_cattle_from_tag';
$route['get_cattles'] = 'service/get_cattles';
$route['get_one_cattle'] = 'service/get_one_cattle';
$route['update_cattle'] = 'service/update_cattle';
$route['get_breeding_process'] = 'service/get_breeding_process';
$route['set_breeding_process'] = 'service/set_breeding_process';
//Products 
$route['products'] = 'service/products';
$route['get_one_product'] = 'service/get_one_product';
$route['add_delivery_data'] = 'service/add_delivery_data';
$route['save_style'] = 'service/save_style';
$route['confirm_pregnency'] = 'service/confirm_pregnency';




//Breeding Process
$route['update_state'] = 'service/update_state';
$route['update_breeding_process'] = 'service/update_breeding_process';
$route['get_events_count'] = 'service/get_events_count';
$route['cattle_history'] = 'service/cattle_history';
$route['add_request'] = 'service/add_request';
$route['delete_cattle'] = 'service/delete_cattle';
$route['delete_expense'] = 'service/delete_expense';
$route['delete_income'] = 'service/delete_income';
$route['product_request'] = 'service/product_request';
$route['get_fat_price'] = 'service/get_fat_price';
$route['add_milk_income'] = 'service/add_milk_income';
$route['daily_report'] = 'service/daily_report';
$route['monthly_report'] = 'service/monthly_report';
$route['reset_password'] = 'service/reset_password';
$route['add_milk'] = 'service/add_milk';
$route['get_data'] = 'service/get_data';
$route['get_milk_data'] = 'service/get_milk_data';
$route['product/(:num)'] = 'catalog/product_lookup_by_id/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


$route['aztro'] = 'service/aztro';
$route['getheroscope'] = 'service/getheroscope';
