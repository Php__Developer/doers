<aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"> <img src="assests/images/user.png" class="img-circle" alt=""/></div>
                <div class="pull-left info">
                    <p><?php echo isset($this->session->name) ? $this->session->name : '' ;?></p>
                    <!-- Status --> 
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
            </div>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="active">
                    <a href="<?php echo base_url() ;?>index.php/product/list"><i class="fa fa-link"></i> 
                    <span>Products</span></a></li>
                <li>
                    <a href="<?php echo base_url() ;?>index.php/product/add"><i class="fa fa-link"></i>
                     <span>Product Add</span></a>
                </li>

                <li>
                    <a href="<?php echo base_url() ;?>index.php/product/requests"><i class="fa fa-link"></i>
                     <span>Service Requests</span></a>
                </li>
                <!-- <li class="treeview">
                 <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>

                    <ul class="treeview-menu">
                        <li><a href="#">Link in level 2</a></li>
                        <li><a href="#">Link in level 2</a></li>
                    </ul>
                </li> -->
            </ul>
        </section>
    </aside>
