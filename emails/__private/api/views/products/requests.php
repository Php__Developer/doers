 <div class="content-wrapper">
        <section class="content-header">
            <h1> Page Header <small>Optional description</small> </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <section class="content container-fluid">
        
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Hover Data Table</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $counter = 1;
                                    if(count($result) > 0){
                                        foreach($result as $data){    
                                 ;?>
                                    <tr>
                                        <td><?php echo $counter++;?></td>
                                        <td><?php echo $data->name ;?></td>
                                        <td><?php echo $data->phone ;?></td>
                                        <td><?php echo $data->email ;?></td>
                                    </tr>
                                <?php }
                                    }
                                ;?>
                                </tbody>
                                    
                            </table>
                            <?php if (isset($links)) { ?>
                                <?php echo $links ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
         </section>
    </div>