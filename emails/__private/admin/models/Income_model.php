<?php

class Income_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('income', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('income');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
                /*$this->first_name    = $_POST['title']; // please read the below note
                $this->_name  = $_POST['content'];
                $this->date     = time();
*/
                $this->db->insert('income', $data);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('income', $this, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons)
        {   
            //echo $limit;
            $this->db->select('id,income_date, milk_sale_rate, manure_sale_rate, milk_sale, manure_sale, others,total');
            $this->db->limit($limit, $start);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('income_date >=',$conditons['start_date']);
                $this->db->where('income_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("income");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($conditons)
        {
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('income_date >=',$conditons['start_date']);
                $this->db->where('income_date <=', $conditons['end_date']);
            }
            $this->db->from("income");
            $total = $this->db->count_all_results();
            return $total;
        }

         public function get_one($id){
            $details = $this->db->get_where('income', array('id' => $key));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }


}


 ;?>