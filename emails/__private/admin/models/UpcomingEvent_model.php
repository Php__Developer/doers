<?php

class UpcomingEvent_model extends CI_Model {

        public $title;
        public $content;
        public $date;
          public function __construct()
        {
                parent::__construct();
                 $this->load->database();
                // Your own constructor code
        }


        public function get_last_ten_entries()
        {
                $query = $this->db->get('upcoming_events', 10);
                return $query->result();
        }

        public function get_all()
        {
                $query = $this->db->get('upcoming_events');
                return $query->result();
        }

        

        public function insert_entry($data)
        {
                /*$this->first_name    = $_POST['title']; // please read the below note
                $this->_name  = $_POST['content'];
                $this->date     = time();
*/
                $this->db->insert('upcoming_events', $data);
        }

        public function update_entry($data)
        {
            $this->db->update('upcoming_events', $data, array('id' => $_POST['id']));
        }

        public function get_current_page_records($limit, $start,$conditons,$userdata)
        {   
            //echo $limit;

            $this->db->limit($limit, $start);
            $this->db->where('user_id',$userdata['id']);
            if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $query = $this->db->get("upcoming_events");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    $data[] = $row;
                }
                 
                return $data;
            }
          
            return false;
        }

        public function get_total($userdata)
        {   
            //print_r($userdata); die;
            $this->db->where('user_id',$userdata['id']);
             if(isset($conditons['start_date']) && isset($conditons['end_date'])){
                $this->db->where('expense_date >=',$conditons['start_date']);
                $this->db->where('expense_date <=', $conditons['end_date']);
            }
            $this->db->from("upcoming_events");
            $total = $this->db->count_all_results();
            return $total;

            //return $this->db->count_all("upcoming_events");
        }

         public function get_one($id){
            $details = $this->db->get_where('upcoming_events', array('id' => $id));
            $record_arr = $details->first_row('array');
            return $record_arr;
        }


}


 ;?>