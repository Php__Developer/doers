<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
            $this->load->library('session');
        }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                $this->load->library('session');
                if($this->session->has_userdata('email')){
                    //$this->load->view('home');
                    $data['middle'] = 'home';
                    $data['result'] = [];
                    $this->load->view('template',$data);
                } else {
                    $this->load->helper('form');
                    $this->load->view('login');    
                }
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //echo "Test"; die;
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            $this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $this->load->helper('form');
                    $this->load->view('login'); 
                    //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors() ];
            }  else {
                //Setting values for tabel columns

                $data = array(
                        'email' => $this->input->post('email'),
                        'password' =>$this->input->post('password'),
                    );
                
               $result = $this->User_model->login($data);
               if($result['status'] == '1'){
                    $userdata = $this->User_model->get_one($result['key']);
                    //print_r($userdata); die;
                    $newdata = array(
                            'email'   => $this->input->post('email'),
                            'name' =>  $userdata['name'],
                            'id' =>  $userdata['id'],
                            'logged_in' => true,
                            'api_key' => $result['key']
                    );
                    //print_r( $newdata ); die;
                    $this->session->set_userdata($newdata);
                    redirect('/');
               }
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

       /* return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));*/
        }


         public function logout(){
            $newdata = array(
                            'email',
                            'logged_in' ,
                            'api_key'
                    );
                $this->session->unset_userdata($newdata);
                redirect('/');
        }



          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function addexpense(){
             $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            $this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($result));    

        }

         public function list(){
                $data = $this->User_model->get_all();
                $this->load->library('pagination');

                  $params = array();
                    $limit_per_page = 10;
                    $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                    $total_records = $this->User_model->get_total();
             
                    if ($total_records > 0) 
                    {
                        // get current page records
                        $params["result"] = $this->User_model->get_current_page_records($limit_per_page, $start_index);
                        $config['base_url'] = base_url() . 'index.php/user/list';
                        $config['total_rows'] = $total_records;
                        $config['per_page'] = $limit_per_page;
                        $config["uri_segment"] = 3;
                        $config['full_tag_open'] = '<ul class="pagination">';
                        $config['full_tag_close'] = '</ul>';
                        $config['first_link'] = false;
                        $config['last_link'] = false;
                        $config['first_tag_open'] = '<li>';
                        $config['first_tag_close'] = '</li>';
                        $config['prev_link'] = '&laquo';
                        $config['prev_tag_open'] = '<li class="prev">';
                        $config['prev_tag_close'] = '</li>';
                        $config['next_link'] = '&raquo';
                        $config['next_tag_open'] = '<li>';
                        $config['next_tag_close'] = '</li>';
                        $config['last_tag_open'] = '<li>';
                        $config['last_tag_close'] = '</li>';
                        $config['cur_tag_open'] = '<li class="active"><a href="#">';
                        $config['cur_tag_close'] = '</a></li>';
                        $config['num_tag_open'] = '<li>';
                        $config['num_tag_close'] = '</li>';
                        $this->pagination->initialize($config);
                        // build paging links
                        $params["links"] = $this->pagination->create_links();
                    }

                $params['middle'] = 'users/list';
                $this->load->view('template',$params);
        }

        public function get_one_by_email($email){

        }

        public function activate(){
            $formdata = $this->input->post();
            if(count($formdata) > 0){
              $this->User_model->activate($formdata);     
              $result = ['status' => 'success'];
            }

            return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($result)); 

        }

}
