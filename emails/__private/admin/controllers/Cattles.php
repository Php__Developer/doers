<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cattles extends CI_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
            $this->load->library('session');
        }

        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                $this->load->library('session');
                if($this->session->has_userdata('email')){
                    //$this->load->view('home');
                    $data['middle'] = 'home';
                    $data['result'] = [];
                    $this->load->view('template',$data);
                } else {
                    $this->load->helper('form');
                    $this->load->view('login');    
                }
        }

        public function breeding_process(){
            $this->load->helper('form');
            $this->load->model(array('BreedingProcess'));
            $process = $this->BreedingProcess->get_one(1);
            //$this->load->view('breeding_process');    
            $data['middle'] = 'breeding_process';
            $data['result'] = (count($process) > 0 ) ? $process : [];
            //$data['image_error'] = $this->upload->display_errors();
            $this->load->view('template',$data);
        }

        public function saveprocess(){
       //       public function login(){
            $this->load->model(array('BreedingProcess'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('ai_on', 'Artificial Insemination', 'required|integer');
            $this->form_validation->set_rules('first_us_pd_on', 'First PD days from AI Date(Ultra Sound)', 'required|integer');
            $this->form_validation->set_rules('first_manual_pd_on', 'First PD days from AI Date(Manual)', 'required|integer');
            $this->form_validation->set_rules('second_us_pd_on', 'Second PD days from AI Date(Ultra Sound)', 'required|integer');
            $this->form_validation->set_rules('second_manual_pd_on', 'Second PD days from AI Date(Manual)', 'required|integer');
            $this->form_validation->set_rules('dry_on', 'Putting on Dry Days from AI date', 'required|integer');
            $this->form_validation->set_rules('steam_up_on', 'Steam Up Days from AI date', 'required|integer');
            $this->form_validation->set_rules('delivery_on', 'Delivery Days from AI date', 'required|integer');

            if ($this->form_validation->run() == FALSE) {
                    //$result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                $process = $this->BreedingProcess->get_one(1);
                $data['middle'] = 'breeding_process';
                $data['result'] = (count($process) > 0 ) ? $process : [];
                //$data['image_error'] = $this->upload->display_errors();
                $this->load->view('template',$data);
            }  else {
                //Setting values for tabel columns

                $process = $this->BreedingProcess->get_one(1);
                $data = array(
                    'ai_on' => $this->input->post('ai_on'),
                    'first_us_pd_on' =>$this->input->post('first_us_pd_on'),
                    'first_manual_pd_on' =>$this->input->post('first_manual_pd_on'),
                    'second_us_pd_on' =>$this->input->post('second_us_pd_on'),
                    'second_manual_pd_on' =>$this->input->post('second_manual_pd_on'),
                    'dry_on' =>$this->input->post('dry_on'),
                    'steam_up_on' =>$this->input->post('steam_up_on'),
                    'delivery_on' =>$this->input->post('delivery_on'),
                );
                if(count($process) > 0){
                    $this->BreedingProcess->update_entry(1,$data);
                } else {
                    $this->BreedingProcess->insert_entry($data);
                }
               //$result = $this->User_model->login($data);
                $process = $this->BreedingProcess->get_one(1);
                $data['middle'] = 'breeding_process';
                $data['result'] = (count($process) > 0 ) ? $process : [];
                //$data['image_error'] = $this->upload->display_errors();
                $this->load->view('template',$data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        /*return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));*/
       // }

        }

}
