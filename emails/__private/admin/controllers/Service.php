<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends My_Controller {

        //$this->load->model(array('data'));

        function __construct() {
            parent::__construct();
            $this->load->model(array('Data_model','Income_model'));
            $this->load->model(array('Expense_model'));
            $this->load->model(array('User_model'));
            $this->load->model(array('User_model'));
            $this->load->helper('url');
        }
/*
       public function _remap($method)
                {
                        if ($method === 'index')
                        {
                                 echo "HIIII"       ;
                                //$this->$method();
                        }
                        else
                        {
                                $this->default_method();
                        }
                }*/


        /**
         * Index Page for this controller.
         *
         * Maps to the following URL
         *              http://example.com/index.php/welcome
         *      - or -
         *              http://example.com/index.php/welcome/index
         *      - or -
         * Since this controller is set as the default controller in
         * config/routes.php, it's displayed at http://example.com/
         *
         * So any other public methods not prefixed with an underscore will
         * map to /index.php/welcome/<method_name>
         * @see https://codeigniter.com/user_guide/general/urls.html
         */
        public function index()
        {
                echo 'Hello World!';        
                //$this->load->view('welcome_message');
        }

        public function data(){
                $data = $this->Data_model->get_all();
                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(500)
                    ->set_output(json_encode($data));
        }

         /*
        |--------------------------------------------------------------------------
        | Function : register
        |--------------------------------------------------------------------------
        | This will be used to display the list of all admins to the super admin
        */

        public function register(){
            //Including validation library
            $this->load->library('form_validation');
            //$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            //Validating Name Field
            $this->form_validation->set_rules('name', 'Name', 'required|min_length[5]|max_length[50]');
            //$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');

            $this->form_validation->set_rules('equipment', 'Equipment', 'required|min_length[5]|max_length[50]');
            $this->form_validation->set_rules('address', 'Address', 'required|min_length[5]|max_length[200]');
            //Validating Email Field
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            //Validating Mobile no. Field
            $this->form_validation->set_rules('phone', 'Mobile No.', 'required|is_unique[users.phone]|integer',
                array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
         ));

            //Validating Address Field
            //$this->form_validation->set_rules('gender', 'Gender', 'required');

            if ($this->form_validation->run() == FALSE) {
                    //echo json_encode(validation_errors());

                /*$arr = array(
                    'field_name_one' => form_error('field_name_one'),
                    'field_name_two' => form_error('field_name_two')
                );*/
                // errors_array()
                //$result = ['status' => '0','reason' => 'validation' , 'errors' => validation_errors()];
                $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                $ref = md5(rand(999,9999)).'-'.date('YmdHis');
                $data = array(
                'name' => $this->input->post('name'),
                'last_name' => 'None',
                'email' => $this->input->post('email'),
                'equipment' => $this->input->post('equipment'),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
               // 'gender' => $this->input->post('gender'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'role_id' => 2,
                'api_key' => $ref
                );
                //Transfering data to Model
                $this->User_model->insert_entry($data);
                $result = ['status' => '1','message' => 'Registered Successfully!','key' => $ref];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));

        }

          /*
        |--------------------------------------------------------------------------
        | Function : login
        |--------------------------------------------------------------------------
        | Login Through mobile apps.
        */
       

        public function login(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                    $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
            }  else {
                //Setting values for tabel columns
                 $data = array(
                'email' => $this->input->post('email'),
                'password' =>$this->input->post('password'),
                );
               $result = $this->User_model->login($data);
                //$data['message'] = 'Data Inserted Successfully';
               // $result = ['status' => '1','message' => 'Registered Successfully!'];
        }

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));
        }

          /*
        |--------------------------------------------------------------------------
        | Function : addexpense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */


        public function addexpense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                   $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('others')
                            ));
                        $data = array(
                            'user_id' => $user_id,
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total
                        );
                        $this->Expense_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Expense Added Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

        public function get_expenses(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;
                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Expense_model->get_total();
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Expense_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }
                    }

             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

         /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 


        public function get_one_expense(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $result = $checklogin;
             } else{
                 $this->form_validation->set_rules('id', 'ID', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    } else {
                        $expense = $this->Expense_model->get_one($this->input->post('id'));
                        if(count($expense) > 0){
                            $result =['status' => '1','result' => $expense];            
                        } else {
                            $result =['status' => '1','result' => [] ];            
                        }
                        
                    }
             }
             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    
            
        }


         /*
        |--------------------------------------------------------------------------
        | Function : update Expense
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 

         public function update_expense(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                    
                    $user_id =  $checklogin['userdata']['id'];
                    $this->form_validation->set_rules('expense_date', 'Expense Date', 'required');
                    $this->form_validation->set_rules('salary', 'Salary', 'required|integer');
                    $this->form_validation->set_rules('green_fodder', 'Green_fodder', 'required|integer');
                    $this->form_validation->set_rules('dry_fodder', 'Dry fodder', 'required|integer');
                    $this->form_validation->set_rules('concentrate', 'Concentrate', 'required|integer');
                    $this->form_validation->set_rules('electricity', 'Electricity', 'required|integer');
                    $this->form_validation->set_rules('medicine', 'Medicine', 'required|integer');
                    $this->form_validation->set_rules('atrificial_insemination', 'Atrificial Insemination', 'required|integer');
                    $this->form_validation->set_rules('others', 'Others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $date = date_create($this->input->post('expense_date'))->format('Y-m-d');
                        
                        $total = array_sum(array(
                                $this->input->post('salary'),$this->input->post('green_fodder'),
                                $this->input->post('dry_fodder'),$this->input->post('concentrate'),
                                $this->input->post('medicine'),$this->input->post('atrificial_insemination'),
                                $this->input->post('others')
                            ));
                        $data = array(
                            'user_id' => $user_id,
                            'salary' => $this->input->post('salary'),
                            'green_fodder' => $this->input->post('green_fodder'),
                            'dry_fodder' => $this->input->post('dry_fodder'),
                            'concentrate' => $this->input->post('concentrate'),
                            'electricity' => $this->input->post('electricity'),
                            'medicine' => $this->input->post('medicine'),
                            'atrificial_insemination' => $this->input->post('atrificial_insemination'),
                            'others' => $this->input->post('others'),
                            'expense_date' => $date,
                            'total' => $total
                        );
                        $this->Expense_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Expense Added Successfully!'];
                }
             }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }





        /*
        |--------------------------------------------------------------------------
        | Function : update Expenses
        |--------------------------------------------------------------------------
        | This will be used to add expenses.
        */ 




          public function addincome(){
             $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);
             if ($checklogin['status'] == '0') {
                    $result = $checklogin;
             } else{
                  // print_r( $checklogin['userdata']) ; die;
                   $user_id =  $checklogin['userdata']['id'];
                    //$this->form_validation->set_rules('user_id', 'User Name', 'required');
                    $this->form_validation->set_rules('income_date', 'Income Date', 'required');
                    $this->form_validation->set_rules('milk_sale_rate', 'Milk Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('manure_sale_rate', 'Manuare Sale Rate', 'required|integer');
                    $this->form_validation->set_rules('milk_sale', 'Milk Sale', 'required|integer');
                    $this->form_validation->set_rules('manure_sale', 'Manuare Sale', 'required|integer');
                    $this->form_validation->set_rules('others', 'others', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $total = array_sum(array(
                                $this->input->post('milk_sale_rate') * $this->input->post('milk_sale'),
                                $this->input->post('manure_sale_rate') * $this->input->post('manure_sale'),
                                $this->input->post('others')
                            ));

                        $date = date_create($this->input->post('income_date'))->format('Y-m-d');
                        $data = array(
                        'user_id' => $user_id,
                        'milk_sale_rate' => $this->input->post('milk_sale_rate'),
                        'manure_sale_rate' => $this->input->post('manure_sale_rate'),
                        'milk_sale' => $this->input->post('milk_sale'),
                        'manure_sale' => $this->input->post('manure_sale'),
                        'others' => $this->input->post('others'),
                        'total' => $total,
                        'income_date' => $date,
                        );
                        //Transfering data to Model
                        $this->Income_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Income Added Successfully!'];
                }
             }
          

        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));   
        }

        public function get_income(){
            $this->load->library('form_validation');
             $formdata = $this->input->post();
             $checklogin = $this->checklogin($formdata);             
             if ($checklogin['status'] == '0') {
                $params = $checklogin;
             } else{
                $this->form_validation->set_rules('page', 'Page', 'required|integer');
                 if ($this->form_validation->run() == FALSE) {
                         $params = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                          $user_id =  $checklogin['userdata']['id'];
                            $this->load->library('pagination');
                              $params = array();
                                $limit_per_page = 10;

                                $start_index = ($this->input->post('page') ==1 ) ? 0 : $this->input->post('page') * $limit_per_page - $limit_per_page ;
                                $total_records = $this->Income_model->get_total($formdata);
                         
                                if ($total_records > 0) 
                                {
                                    // get current page records
                                    $params["status"] = '1';
                                    $params["total"] = $total_records;
                                    $params["per_page"] = $limit_per_page;
                                    $params["result"] = $this->Income_model->get_current_page_records($limit_per_page, $start_index,$formdata);
                                    $config['base_url'] = base_url() . 'index.php/product/list';
                                    $config['total_rows'] = $total_records;
                                    $config['per_page'] = $limit_per_page;
                                    $this->pagination->initialize($config);
                                    // build paging links
                                    $params["links"] = $this->pagination->create_links();
                                } else {
                                    $params = ['status'=> '1','result' => []];
                                }

                    }

              


             }

             return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($params));    
            
        }

        public function add_request(){
             $this->load->library('form_validation');
            $this->load->model(array('Request_model'));
                $formdata = $this->input->post();
               // print_r($formdata); die;
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('phone', 'Phone', 'required|integer');
                    //Validating Address Field
                    if ($this->form_validation->run() == FALSE) {
                          $result = ['status' => '0','reason' => 'validation' , 'errors' => $this->form_validation->error_array() ];
                    }  else {
                        //Setting values for tabel columns
                        $data = array(
                        'name' =>$this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone'),
                        'type' => $this->input->post('type'),
                        );
                        //Transfering data to Model
                        $this->Request_model->insert_entry($data);
                        $result = ['status' => '1','message' => 'Request Added Successfully!'];
                }

                return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($result));    


        }

}
