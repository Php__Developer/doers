<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Admin</title>
<link rel="stylesheet" href="<?php echo base_url() ;?>assests/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ;?>assests/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url() ;?>assests/css/main.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo"> <a href="#">Logo</a> </div>
    <div class="login-box-body">
    	<?php echo form_open('user/login',['class' => '' , 'method' => 'post']); ?>
    		<div class="form-group has-feedback">
    			<?php echo form_input(['class' => 'form-control' ,'name' => 'email']);?>
                <!-- <input type="email" class="form-control" placeholder="Email"> -->
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span> 
                <?php echo form_error('email'); ?>
             </div>		
             <div class="form-group has-feedback">
                <!-- <input type="password" class="form-control" placeholder="Password"> -->
                <?php echo form_password(['class' => 'form-control','name' => 'password']);?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
                <?php echo form_error('password'); ?>
              </div>
              <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
            </form>

        <!-- <form method="post">
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span> </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span> </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form> -->
        <a href="#">I forgot my password</a> </div>
</div>
<script src="<?php echo base_url() ;?>assests/js/jquery.min.js"></script> 
<script src="<?php echo base_url() ;?>assests/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url() ;?>assests/js/main.js"></script>
</body>
</html>