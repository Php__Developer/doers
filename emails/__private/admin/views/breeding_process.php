 <div class="content-wrapper">
       <!--  <section class="content-header">
            <h1> Page Header <small>Optional description</small> </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section> -->
        <section class="content container-fluid">
        
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Default Values For the Breeding Process</h3>
                        </div>
                        <!-- /.box-header -->
                        <?php echo form_open_multipart('cattles/saveprocess',['class' => '' , 'method' => 'post']); ?>
                        <div class="box-body">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">AI Days from Last Delivery</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'ai_on' ,'value' => isset($result['ai_on']) ? $result['ai_on'] : '' ]);?>
                                  <?php echo form_error('ai_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">First PD days from AI Date(Ultra Sound)</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'first_us_pd_on','value' => isset($result['first_us_pd_on']) ? $result['first_us_pd_on'] : '']);?>
                                  <?php echo form_error('first_us_pd_on'); ?>
                                </div>
                                
                                <div class="form-group">
                                  <label for="exampleInputEmail1">First PD days from AI Date(Manual)</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'first_manual_pd_on','value' => isset($result['first_manual_pd_on']) ? $result['first_manual_pd_on'] : '']);?>
                                  <?php echo form_error('first_manual_pd_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Second PD days from AI date(Ulta Sound)</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'second_us_pd_on','value' => isset($result['second_us_pd_on']) ? $result['second_us_pd_on'] : '']);?>
                                  <?php echo form_error('second_us_pd_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Second PD days from AI date(Manual)</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'second_manual_pd_on','value' => isset($result['second_manual_pd_on']) ? $result['second_manual_pd_on'] : '']);?>
                                  <?php echo form_error('second_manual_pd_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Putting on Dry Days from AI date</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'dry_on','value' => isset($result['dry_on']) ? $result['dry_on'] : '']);?>
                                  <?php echo form_error('dry_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Steam Up Days from AI date</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'steam_up_on','value' => isset($result['steam_up_on']) ? $result['steam_up_on'] : '']);?>
                                  <?php echo form_error('steam_up_on'); ?>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Delivery Days from AI date</label>
                                  <?php echo form_input(['class' => 'form-control' ,'name' => 'delivery_on','value' => isset($result['delivery_on']) ? $result['delivery_on'] : '' ]);?>
                                  <?php echo form_error('delivery_on'); ?>
                              </div>
                                
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                         <?php echo "</form>";?>   
                    </div>
                </div>
            </div>
        </section>
        
         </section>
    </div>