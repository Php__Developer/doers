<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);



Router::scope('/', function (RouteBuilder $routes) {
Router::extensions(['csv']);
Router::extensions(['pdf']);
  $routes->resources('Webservicies');
  $routes->connect('/', ['controller' => 'Users', 'action' => 'index', 'home','prefix'=>'admin']);
  $routes->connect('/goto', ['controller' => 'Users', 'action' => 'goto' ,'prefix'=>'admin']);
  $routes->connect('/register', ['controller' => 'Users', 'action' => 'register','prefix'=>'admin']);
  $routes->connect('/register', ['controller' => 'Agencies', 'action' => 'register','prefix'=>'agencies']);
  $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'admin']);
  $routes->connect('/adminlogin', ['controller' => 'Users', 'action' => 'adminlogin' ,'prefix'=>'admin']);
  $routes->connect('/login/:agency', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'admin'],['pass' => ['agency']]);
  $routes->connect('/detailspage', ['controller' => 'Users', 'action' => 'detailspage' ,'prefix'=>'admin']);
  $routes->connect('/web-development', ['controller' => 'Users', 'action' => 'webdevelopment' ,'prefix'=>'admin']);
  $routes->connect('/mobile-development', ['controller' => 'Users', 'action' => 'mobiledevelopment' ,'prefix'=>'admin']);

  $routes->connect('/cloud-handling', ['controller' => 'Users', 'action' => 'cloudhandling' ,'prefix'=>'admin']);
  $routes->connect('/digital-marketing', ['controller' => 'Users', 'action' => 'digitalmarketing' ,'prefix'=>'admin']);
  $routes->connect('/content-writing', ['controller' => 'Users', 'action' => 'contentwriting' ,'prefix'=>'admin']);
  $routes->connect('/financial-services', ['controller' => 'Users', 'action' => 'financialservices' ,'prefix'=>'admin']);
  
  $routes->connect('/erp', ['controller' => 'Users', 'action' => 'erp' ,'prefix'=>'admin']);
  $routes->connect('/placement', ['controller' => 'Users', 'action' => 'placement' ,'prefix'=>'admin']);
  $routes->connect('/training', ['controller' => 'Users', 'action' => 'training' ,'prefix'=>'admin']);


  $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard' ,'prefix'=>'admin']);
  $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard' ,'prefix'=>'projects']);
  $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard' ,'prefix'=>'sales']);
  $routes->connect('/dashboard', ['controller' => 'Users', 'action' => 'dashboard' ,'prefix'=>'reports']);
  $routes->connect('/details/:id', ['controller' => 'appraisals', 'action' => 'details','prefix'=>'appraisals'],['pass' => ['id'],'pass' => ['agreebox']]);
  $routes->connect('/agree/:id', ['controller' => 'appraisals', 'action' => 'agree','prefix'=>'appraisals'],['pass' => ['id']]);
  $routes->connect('/closereport/:id/:date', ['controller' => 'tickets', 'action' => 'closereport','prefix'=>'attendances'],['pass' => ['id','date']]); // maninder 20-09
  $routes->connect('/forgot_password', ['controller' => 'Users', 'action' => 'forgot_password' ,'prefix'=>'admin']);
  $routes->connect('/get_code', ['controller' => 'Users', 'action' => 'get_code' ,'prefix'=>'admin']);
  $routes->connect('/checkuserstatus/:id', ['controller' => 'Users', 'action' => 'checkuserstatus' ,'prefix'=>'admin'],['pass' => ['id']]);
  $routes->connect('/adddoc', ['controller' => 'webservices', 'action' => 'adddoc']);
  $routes->connect('/dcrids', ['controller' => 'webservices', 'action' => 'dcrids']);
  $routes->connect('/get_data', ['controller' => 'Webservicies', 'action' => 'get_data']);
  
  

  Router::prefix('roles', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'roles']);
    $routes->connect('/list', ['controller' => 'Roles', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'Roles', 'action' => 'add']);
    $routes->connect('/delete/:id', ['controller' => 'Roles', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'Roles', 'action' => 'edit'],['pass' => ['id']]);
  });

  Router::prefix('admin', function ($routes) {
    $routes->connect('/index', ['controller' => 'Users', 'action' => 'index', 'home']);
    $routes->connect('/activities', ['controller' => 'Users', 'action' => 'activities']);
    $routes->connect('/getOrigin', ['controller' => 'Users', 'action' => 'getOrigin']);
    $routes->connect('/credentialscrone', ['controller' => 'Users', 'action' => 'credentialscrone']);
    $routes->connect('/setusercrone', ['controller' => 'Users', 'action' => 'setusercrone']);
    $routes->connect('/reporttomerge/:id', ['controller' => 'Users', 'action' => 'reporttomerge'],['pass' => ['id']]);
    $routes->connect('/projectsmerge', ['controller' => 'Users', 'action' => 'projectsmerge']);
    $routes->connect('/contactssmerge', ['controller' => 'Users', 'action' => 'contactssmerge']);
    $routes->connect('/financessmerge', ['controller' => 'Users', 'action' => 'financessmerge']);
    $routes->connect('/pmidmerge', ['controller' => 'Users', 'action' => 'pmidmerge']);
    $routes->connect('/engineeringusermerge', ['controller' => 'Users', 'action' => 'engineeringusermerge']);
    $routes->connect('/salesfrontusermerge', ['controller' => 'Users', 'action' => 'salesfrontusermerge']);
    $routes->connect('/salesbackendusermerge', ['controller' => 'Users', 'action' => 'salesbackendusermerge']);
    $routes->connect('/teamidmerge', ['controller' => 'Users', 'action' => 'teamidmerge']);
    $routes->connect('/lastidmerge', ['controller' => 'Users', 'action' => 'lastidmerge']);
    $routes->connect('/saleprofilesmerge', ['controller' => 'Users', 'action' => 'saleprofilesmerge']);
    $routes->connect('/profileidmerge', ['controller' => 'Users', 'action' => 'profileidmerge']);
    $routes->connect('/ticketsmerge', ['controller' => 'Users', 'action' => 'ticketsmerge']);
    $routes->connect('/toidmerge', ['controller' => 'Users', 'action' => 'toidmerge']);
    $routes->connect('/fromidmerge', ['controller' => 'Users', 'action' => 'fromidmerge']);
    $routes->connect('/lastrepliermerge', ['controller' => 'Users', 'action' => 'lastrepliermerge']);
    $routes->connect('/projectofticket', ['controller' => 'Users', 'action' => 'projectofticket']);
    $routes->connect('/appraisalsmerge', ['controller' => 'Users', 'action' => 'appraisalsmerge']);
    $routes->connect('/attendancemerge', ['controller' => 'Users', 'action' => 'attendancemerge']);
    $routes->connect('/billingssmerge', ['controller' => 'Users', 'action' => 'billingssmerge']);
    $routes->connect('/evaluationsmerge', ['controller' => 'Users', 'action' => 'evaluationsmerge']);
    $routes->connect('/hardwaresmerge', ['controller' => 'Users', 'action' => 'hardwaresmerge']);
    $routes->connect('/leadsmerge', ['controller' => 'Users', 'action' => 'leadsmerge']);
    $routes->connect('/milestonesmerge', ['controller' => 'Users', 'action' => 'milestonesmerge']);
    $routes->connect('/paymentsmerge', ['controller' => 'Users', 'action' => 'paymentsmerge']);
    $routes->connect('/credentialsauditsmerge', ['controller' => 'Users', 'action' => 'credentialsauditsmerge']);
    $routes->connect('/credentiallogsmerge', ['controller' => 'Users', 'action' => 'credentiallogsmerge']);
    $routes->connect('/profilesauditsmerge', ['controller' => 'Users', 'action' => 'profilesauditsmerge']);
    $routes->connect('/resumesmerge', ['controller' => 'Users', 'action' => 'resumesmerge']);
    $routes->connect('/resumesdatacorrect', ['controller' => 'Users', 'action' => 'resumesdatacorrect']);
    $routes->connect('/settingstablemerge', ['controller' => 'Users', 'action' => 'settingstablemerge']);
    $routes->connect('/staticpagesmerge', ['controller' => 'Users', 'action' => 'staticpagesmerge']);
    $routes->connect('/testimonialsmerge', ['controller' => 'Users', 'action' => 'testimonialsmerge']);
    $routes->connect('/permissionsmerge', ['controller' => 'Users', 'action' => 'permissionsmerge']);
    $routes->connect('/updateagencyid', ['controller' => 'Users', 'action' => 'updateagencyid']);
    $routes->connect('/credentialsuseridmerge/:id', ['controller' => 'Users', 'action' => 'credentialsuseridmerge'],['pass' => ['id']]);
    $routes->connect('/dashbaorddata', ['controller' => 'Users', 'action' => 'dashbaorddata']);
    $routes->connect('/salesdatacorrect/:id', ['controller' => 'Users', 'action' => 'salesdatacorrect'],['pass' => ['id']]); 
    $routes->connect('/oldprojectid', ['controller' => 'Users', 'action' => 'oldprojectid']); 
    $routes->connect('/oldalloteduserid', ['controller' => 'Users', 'action' => 'oldalloteduserid']); 
    $routes->connect('/newprofileid/:id', ['controller' => 'Users', 'action' => 'newprofileid'],['pass' => ['id']]); 
    $routes->connect('/deleteifneeded/:table', ['controller' => 'Users', 'action' => 'deleteifneeded'],['pass' => ['table']]);
    $routes->connect('/profile', ['controller' => 'Users', 'action' => 'profile']);
    $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);
    $routes->connect('/report/:id', ['controller' => 'Users', 'action' => 'report'],['pass' => ['id']]);
    $routes->connect('/report', ['controller' => 'Users', 'action' => 'report']);
    $routes->connect('/employee_salary', ['controller' => 'Users', 'action' => 'employee_salary']); 
    $routes->connect('/employee_leave_other', ['controller' => 'Users', 'action' => 'employee_leave_other']);
    $routes->connect('/userappraisallist', ['controller' => 'appraisals', 'action' => 'userappraisallist']);
    $routes->connect('/addCurrentStatus', ['controller' => 'Users', 'action' => 'addCurrentStatus']);
    $routes->connect('/userdetails/:id', ['controller' => 'Users', 'action' => 'userdetails'],['pass' => ['id']]);
    $routes->connect('/changepass', ['controller' => 'Users', 'action' => 'changepassforall']);
     $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);
  });

    Router::prefix('agencies', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ]);
    $routes->connect('/register', ['controller' => 'agencies', 'action' => 'register']);
    $routes->connect('/list', ['controller' => 'agencies', 'action' => 'list']);
    $routes->connect('/clear-cache', ['controller' => 'agencies', 'action' => 'clearcache']);
    $routes->connect('/add', ['controller' => 'agencies', 'action' => 'add']);
    $routes->connect('/delete/:id', ['controller' => 'agencies', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'agencies', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/aslogin/:id', ['controller' => 'agencies', 'action' => 'aslogin'],['pass' => ['id']]);
    $routes->connect('/loginid/:id', ['controller' => 'agencies', 'action' => 'loginid'],['pass' => ['id']]);
    $routes->connect('/userlogout/:id', ['controller' => 'agencies', 'action' => 'userlogout'],['pass' => ['id']]);
    $routes->connect('/switchlogin/:id', ['controller' => 'agencies', 'action' => 'switchlogin'],['pass' => ['id']]);
    $routes->connect('/destroyallsessions/:id', ['controller' => 'agencies', 'action' => 'destroyallsessions'],['pass' => ['id']]);
    
  });

   Router::prefix('tags', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/list', ['controller' => 'tags', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'tags', 'action' => 'add']);
    $routes->connect('/delete/:id', ['controller' => 'tags', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'tags', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/manage/:id', ['controller' => 'tags', 'action' => 'manage'],['pass' => ['id']]);
    $routes->connect('/edit', ['controller' => 'tags', 'action' => 'edit']);
    $routes->connect('/manage', ['controller' => 'tags', 'action' => 'manage']);
    $routes->connect('/tagcontent/:id', ['controller' => 'tags', 'action' => 'tagcontent'],['pass' => ['id']]);
    $routes->connect('/tagcontent', ['controller' => 'tags', 'action' => 'tagcontent']);
    
  });

  Router::prefix('entities', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/list', ['controller' => 'entities', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'entities', 'action' => 'add']);
    $routes->connect('/delete/:id', ['controller' => 'entities', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'entities', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/manage/:id', ['controller' => 'entities', 'action' => 'manage'],['pass' => ['id']]);
    $routes->connect('/edit', ['controller' => 'entities', 'action' => 'edit']);
    $routes->connect('/manage', ['controller' => 'entities', 'action' => 'manage']);
  });
   
  Router::prefix('employees', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'employees']);
    $routes->connect('/credentialDetail', ['controller' => 'employees', 'action' => 'credentialDetail']);
    $routes->connect('/credentialself/:id', ['controller' => 'employees', 'action' => 'credentialself'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'employees', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'employees', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/credential_detail/:id', ['controller' => 'employees', 'action' => 'credential_detail'],['pass' => ['id']]);
    $routes->connect('/add', ['controller' => 'employees', 'action' => 'add']);
    $routes->connect('/birthday', ['controller' => 'employees', 'action' => 'birthday']);
    $routes->connect('/financereport', ['controller' => 'employees', 'action' => 'financereport']);
    $routes->connect('/employeelist', ['controller' => 'employees', 'action' => 'employeelist']);
    $routes->connect('/settings', ['controller' => 'employees', 'action' => 'settings']); 
    $routes->connect('/changeStatus', ['controller' => 'employees', 'action' => 'changeStatus']);
    $routes->connect('/employeefinancexal', ['controller' => 'employees', 'action' => 'employeefinancexal']);
    $routes->connect('/employeefinancpdf', ['controller' => 'employees', 'action' => 'employeefinancpdf']);
    $routes->connect('/seatreport', ['controller' => 'employees', 'action' => 'seatreport']);
    $routes->connect('/seatreport/:id', ['controller' => 'employees', 'action' => 'seatreport'],['pass' => ['id']]);
    $routes->connect('/startup', ['controller' => 'employees', 'action' => 'startup']);
    
  });
  Router::prefix('skills', function ($routes) {
    $routes->connect('/list', ['controller' => 'skills', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'skills', 'action' => 'add']);
    $routes->connect('/edit/:id', ['controller' => 'skills', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'skills', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/', ['controller' => 'skills', 'action' => 'list']);
  });
  Router::prefix('resumes', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'resumes']);
    $routes->connect('/list', ['controller' => 'resumes', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'resumes', 'action' => 'add']);
    $routes->connect('/edit/:id', ['controller' => 'resumes', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'resumes', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changeStatus', ['controller' => 'resumes', 'action' => 'changeStatus']);
    $routes->connect('/', ['controller' => 'resumes', 'action' => 'list']);
  });
  Router::prefix('attendances', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'attendances']);
    $routes->connect('/userview', ['controller' => 'attendances', 'action' => 'userview']); // Maninder
    $routes->connect('/add', ['controller' => 'attendances', 'action' => 'add']);
    $routes->connect('/report', ['controller' => 'attendances', 'action' => 'report']);
    $routes->connect('/userattendance', ['controller' => 'attendances', 'action' => 'userattendance']); //maninder 20-09
    $routes->connect('/addall/:id/:date/:mode', ['controller' => 'attendances', 'action' => 'addall'],['pass' => ['id','date','mode']]); //maninder 20-09
    $routes->connect('/addall', ['controller' => 'attendances', 'action' => 'addall']); //maninder 20-09
    $routes->connect('/leavereport', ['controller' => 'attendances', 'action' => 'leavereport']);
    $routes->connect('/monthlyattendance/:id/:month/:year', ['controller' => 'attendances', 'action' => 'monthlyattendance'],['pass' => ['id','month','year']]); 
  });

  Router::prefix('appraisals', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'appraisals']);
    $routes->connect('/appstatus'  , ['controller' => 'appraisals', 'action' => 'appstatus']);
    $routes->connect('/appraisalslist', ['controller' => 'appraisals', 'action' => 'appraisalslist']);
    $routes->connect('/userappraisallist', ['controller' => 'appraisals', 'action' => 'userappraisallist']);
    $routes->connect('/appraisaldetails/:id', ['controller' => 'appraisals', 'action' => 'appraisaldetails'],['pass' => 'id']);
    $routes->connect('/salaryreport', ['controller' => 'appraisals', 'action' => 'salaryreport']);
    $routes->connect('/add', ['controller' => 'appraisals', 'action' => 'add']);
    $routes->connect('/details/:id', ['controller' => 'appraisals', 'action' => 'details'],['pass' => ['id']]);
    $routes->connect('/edit/:id', ['controller' => 'appraisals', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'appraisals', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/pending', ['controller' => 'appraisals', 'action' => 'pending']);
    $routes->connect('/incoming', ['controller' => 'appraisals', 'action' => 'incoming']);
    $routes->connect('/comment', ['controller' => 'appraisals', 'action' => 'comment']);
    });

  Router::prefix('static_pages', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'static_pages']);
    $routes->connect('/dashboardeditor', ['controller' => 'static_pages', 'action' => 'dashboardeditor']);
    $routes->connect('/list', ['controller' => 'static_pages', 'action' => 'list']);
    $routes->connect('/sampledocument', ['controller' => 'static_pages', 'action' => 'sampledocument']);
    $routes->connect('/static_view/:key', ['controller' => 'static_pages', 'action' => 'static_view'],['pass' => ['key']]);
    $routes->connect('/view', ['controller' => 'static_pages', 'action' => 'view']);
    $routes->connect('/dashboardeditor/:id', ['controller' => 'static_pages', 'action' => 'dashboardeditor']);
    $routes->connect('/view/:id', ['controller' => 'static_pages', 'action' => 'view'],['pass' => ['id']]);         
  });

  Router::prefix('sales', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'sales']);
    $routes->connect('/csdased', ['controller' => 'sales', 'action' => 'csdased']);
    $routes->connect('/quickedit/:id', ['controller' => 'sales', 'action' => 'quickedit'],['pass' => ['id']]);
    $routes->connect('/quickedit', ['controller' => 'sales', 'action' => 'quickedit']);
    $routes->connect('/profiledetail', ['controller' => 'sales', 'action' => 'profiledetail']);
    $routes->connect('/suggestions', ['controller' => 'sales', 'action' => 'suggestions']);
    $routes->connect('/salesedit', ['controller' => 'sales', 'action' => 'salesedit']);
    $routes->connect('/runningprojects/:id', ['controller' => 'sales', 'action' => 'runningprojects'],['pass' => ['id']]);
    $routes->connect('/suggestions/:id', ['controller' => 'sales', 'action' => 'suggestions'],['pass' => ['id']]);
    $routes->connect('/salesedit/:id', ['controller' => 'sales', 'action' => 'salesedit'],['pass' => ['id']]);
    $routes->connect('/saleslist', ['controller' => 'sales', 'action' => 'saleslist']);
    $routes->connect('/view/:id', ['controller' => 'sales', 'action' => 'view'],['pass' => ['id']]);
    $routes->connect('/allotedto/:id', ['controller' => 'sales', 'action' => 'allotedto'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'sales', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/delete:id', ['controller' => 'sales', 'action' => 'delete']);
    $routes->connect('/add', ['controller' => 'sales', 'action' => 'add']);
    $routes->connect('/share:id', ['controller' => 'sales', 'action' => 'share'],['pass' => ['id']]);
    $routes->connect('/unshare:id', ['controller' => 'sales', 'action' => 'unshare'],['pass' => ['id']]);
    $routes->connect('/changestatus', ['controller' => 'sales', 'action' => 'changestatus']);
    $routes->connect('/share_unshare', ['controller' => 'sales', 'action' => 'shareunshare']);
    $routes->connect('/profilereportbyrunnningproject', ['controller' => 'sales', 'action' => 'profilereportbyrunnningproject']);
    $routes->connect('/salesprofileauditreport', ['controller' => 'sales', 'action' => 'salesprofileauditreport']);
    $routes->connect('/profilereportbyallottedto', ['controller' => 'sales', 'action' => 'profilereportbyallottedto']);
    $routes->connect('/verifiedUnverifiedreport', ['controller' => 'sales', 'action' => 'verifiedUnverifiedreport']);
    $routes->connect('/technologyreport', ['controller' => 'sales', 'action' => 'technologyreport']);
    $routes->connect('/Paymentreport', ['controller' => 'sales', 'action' => 'Paymentreport']);
    $routes->connect('/report', ['controller' => 'sales', 'action' => 'report']);
    $routes->connect('/view/:133', ['controller' => 'static_pages', 'action' => 'view'],['pass' => ['133']]);
  });

  Router::prefix('profileaudits', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'profileaudits']);
    $routes->connect('/auditdetails', ['controller' => 'profileaudits', 'action' => 'auditdetails']);
    $routes->connect('/auditdetails/:id', ['controller' => 'profileaudits', 'action' => 'auditdetails'],['pass' => ['id']]);
    $routes->connect('/delete', ['controller' => 'profileaudits', 'action' => 'delete']);
    $routes->connect('/delete/:id', ['controller' => 'profileaudits', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changestatus', ['controller' => 'profileaudits', 'action' => 'changestatus']);
  });

  Router::prefix('evaluations', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'evaluations']);
    $routes->connect('/evaluationslist', ['controller' => 'evaluations', 'action' => 'evaluationslist']); 
    $routes->connect('/myperformance', ['controller' => 'evaluations', 'action' => 'myperformance']); 
    $routes->connect('/monthlyperformance', ['controller' => 'evaluations', 'action' => 'monthlyperformance']); 
    $routes->connect('/performancereport', ['controller' => 'evaluations', 'action' => 'performancereport']); 
    $routes->connect('/performancereportmonth', ['controller' => 'evaluations', 'action' => 'performancereportmonth']);
    $routes->connect('/add', ['controller' => 'evaluations', 'action' => 'add']);
    $routes->connect('/edit/:id', ['controller' => 'evaluations', 'action' => 'edit'],['pass' => ['id']]); 
    $routes->connect('/delete/:id', ['controller' => 'evaluations', 'action' => 'delete'],['pass' => ['id']]); 
    $routes->connect('/changeStatus', ['controller' => 'evaluations', 'action' => 'changeStatus']);
    $routes->connect('/giveratings/:id', ['controller' => 'evaluations', 'action' => 'giveratings'],['pass' => ['id']]); 
    $routes->connect('/userresponse/:id/:response', ['controller' => 'evaluations', 'action' => 'userresponse'],['pass' => ['id','response']]); 
    $routes->connect('/userresponse/:id/:detail', ['controller' => 'evaluations', 'action' => 'userresponse'],['pass' => ['id','detail']]); 
    $routes->connect('/userresponse', ['controller' => 'evaluations', 'action' => 'userresponse']);
    $routes->connect('/performancereportdatewise', ['controller' => 'evaluations', 'action' => 'performancereportdatewise']); 
  });

  Router::prefix('learning_centers', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'learning_centers']);
    $routes->connect('/list', ['controller' => 'learning_centers', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'learning_centers', 'action' => 'add']);
    $routes->connect('/learningcenterreportuser', ['controller' => 'learning_centers', 'action' => 'learningcenterreportuser']);
  });
  Router::prefix('tickets', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'tickets']);
    $routes->connect('/add/:tag/:project_id', ['controller' => 'tickets', 'action' => 'add'],['pass' => ['tag','project_id']]);
    $routes->connect('/add', ['controller' => 'tickets', 'action' => 'add']);
    $routes->connect('/add/:params', ['controller' => 'tickets', 'action' => 'add'],['pass' => ['params']]);
    
    $routes->connect('/instantsave', ['controller' => 'tickets', 'action' => 'instantsave']);
    $routes->connect('/getOrigin', ['controller' => 'tickets', 'action' => 'getOrigin']);
    $routes->connect('/reportdata', ['controller' => 'tickets', 'action' => 'reportdata']);
    $routes->connect('/add2', ['controller' => 'tickets', 'action' => 'add2']);
    $routes->connect('/index', ['controller' => 'tickets', 'action' => 'index']);
    $routes->connect('/index2', ['controller' => 'tickets', 'action' => 'index2']);
    $routes->connect('/index/:ticket_id', ['controller' => 'tickets', 'action' => 'index'],['pass' => ['ticket_id']]);
    $routes->connect('/index2/:ticket_id', ['controller' => 'tickets', 'action' => 'index2'],['pass' => ['ticket_id']]);
    $routes->connect('/report/:key', ['controller' => 'tickets', 'action' => 'report'],['pass' => ['key']]);
    $routes->connect('/report', ['controller' => 'tickets', 'action' => 'report']);
    $routes->connect('/reordertickets', ['controller' => 'tickets', 'action' => 'reordertickets']);
    $routes->connect('/getneededdata', ['controller' => 'tickets', 'action' => 'getneededdata']);
    $routes->connect('/ticketdata', ['controller' => 'tickets', 'action' => 'ticketdata']);
    $routes->connect('/ticketresponse', ['controller' => 'tickets', 'action' => 'ticketresponse']);
    $routes->connect('/ticketresponse/:id', ['controller' => 'tickets', 'action' => 'ticketresponse'],['pass' => ['id']]);
    $routes->connect('/schedule', ['controller' => 'tickets', 'action' => 'schedule']);
    $routes->connect('/response', ['controller' => 'tickets', 'action' => 'response']);
    $routes->connect('/response/:id', ['controller' => 'tickets', 'action' => 'response'],['pass' => ['id']]);
    //$routes->connect('/response/:ticket_id/:act', ['controller' => 'tickets', 'action' => 'response'],['pass' => ['ticket_id','act']]);
    $routes->connect('/closereport/:id/:date', ['controller' => 'tickets', 'action' => 'closereport'],['pass' => ['id','date']]);
    $routes->connect('/ticketreportdate', ['controller' => 'tickets', 'action' => 'ticketreportdate']);
    $routes->connect('/report/:id/:key/:usr', ['controller' => 'tickets', 'action' => 'report'],['pass' => ['id','key','usr']]);
    $routes->connect('/response/:id/:last_page', ['controller' => 'tickets', 'action' => 'response'],['pass' => ['id','last_page']]);
    $routes->connect('/ticketdetail/:id', ['controller' => 'tickets', 'action' => 'ticketdetail'],['pass' => ['id']]); 
    $routes->connect('/ticketdetail', ['controller' => 'tickets', 'action' => 'ticketdetail']);
    $routes->connect('/getValueofEntity', ['controller' => 'tickets', 'action' => 'getValueofEntity']);
    });

  Router::prefix('billings', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'billings']);
    $routes->connect('/billingslist', ['controller' => 'billings', 'action' => 'billingslist']);
    $routes->connect('/quickadd/:id', ['controller' => 'billings', 'action' => 'quickadd'],['pass' => ['id']]);
    $routes->connect('/quickadd', ['controller' => 'billings', 'action' => 'quickadd']);
    $routes->connect('/quickview/:project_id', ['controller' => 'billings', 'action' => 'quickview'],['pass' => ['project_id']]);
    $routes->connect('/quickview/:id', ['controller' => 'billings', 'action' => 'quickview'],['pass' => ['id']]);
    $routes->connect('/quickview', ['controller' => 'billings', 'action' => 'quickview']);
    $routes->connect('/verifybilling', ['controller' => 'billings', 'action' => 'verifybilling']);
    $routes->connect('/billingreportweek', ['controller' => 'billings', 'action' => 'billingreportweek']);
    $routes->connect('/quickview/:id', ['controller' => 'billings', 'action' => 'quickview'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'billings', 'action' => 'quickview'],['pass' => ['id']]);
  });

  Router::prefix('projects', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'projects']);
    $routes->connect('/projectslist1', ['controller' => 'projects', 'action' => 'projectslist1']);
    $routes->connect('/projectslist', ['controller' => 'projects', 'action' => 'projectslist']);
    $routes->connect('/processaudit', ['controller' => 'projects', 'action' => 'processaudit']);
    $routes->connect('/processreport', ['controller' => 'projects', 'action' => 'processreport']); 
    $routes->connect('/closedproject', ['controller' => 'projects', 'action' => 'closedproject']);
    $routes->connect('/projectreport', ['controller' => 'projects', 'action' => 'projectreport']);
    $routes->connect('/projectStatusChangebyAjax', ['controller' => 'projects', 'action' => 'projectStatusChangebyAjax']);
    $routes->connect('/report/:key', ['controller' => 'projects', 'action' => 'report'],['pass' => ['key']]);
    $routes->connect('/report', ['controller' => 'projects', 'action' => 'report']);
    $routes->connect('/quickedit/:project_id', ['controller' => 'projects', 'action' => 'quickedit'],['pass' => ['project_id']]);
    $routes->connect('/projectdetails/:id', ['controller' => 'projects', 'action' => 'projectdetails'],['pass' => ['id']]);
    $routes->connect('/projectdetails/:project_id', ['controller' => 'projects', 'action' => 'projectdetails'],['pass' => ['project_id']]);
    $routes->connect('/editprocessaudit/:id', ['controller' => 'projects', 'action' => 'editprocessaudit'],['pass' => ['id']]);
    $routes->connect('/description/:id', ['controller' => 'projects', 'action' => 'description'],['pass' => ['id']]);
    $routes->connect('/editprocessaudit/:id', ['controller' => 'projects', 'action' => 'editprocessaudit'],['pass' => ['id']]);
    $routes->connect('/editprocessaudit', ['controller' => 'projects', 'action' => 'editprocessaudit']);
    $routes->connect('/editprocessaudit/:id', ['controller' => 'projects', 'action' => 'editprocessaudit'],['pass' => ['id']]);
    $routes->connect('/quickadd/:project_id', ['controller' => 'projects', 'action' => 'quickadd'],['pass' => ['project_id']]);
    $routes->connect('/quickedit/:id', ['controller' => 'projects', 'action' => 'quickedit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'projects', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/add', ['controller' => 'projects', 'action' => 'add']);
    $routes->connect('/quickedit', ['controller' => 'projects', 'action' => 'quickedit']);
    $routes->connect('/projectdetails', ['controller' => 'projects', 'action' => 'projectdetails']);
    $routes->connect('/export/:id', ['controller' => 'projects', 'action' => 'export'],['pass' => ['id']]);
    $routes->connect('/holdprojects', ['controller' => 'projects', 'action' => 'holdprojects']);
    $routes->connect('/hold_projects', ['controller' => 'projects', 'action' => 'holdprojects']);
    $routes->connect('/addjob', ['controller' => 'projects', 'action' => 'addjob']);
    $routes->connect('/getherdata', ['controller' => 'projects', 'action' => 'getherdata']);
    $routes->connect('/getdocs', ['controller' => 'projects', 'action' => 'getdocs']);
    $routes->connect('/editcname', ['controller' => 'projects', 'action' => 'editcname']);
    $routes->connect('/putonalert', ['controller' => 'projects', 'action' => 'putonalert']);
    $routes->connect('/togglestatus/:id', ['controller' => 'projects', 'action' => 'togglestatus'],['pass' => ['id']]);
    $routes->connect('/projectlead/:id', ['controller' => 'projects', 'action' => 'projectlead'],['pass' => ['id']]);
    $routes->connect('/togglestatus', ['controller' => 'projects', 'action' => 'togglestatus']);
    $routes->connect('/projectlead', ['controller' => 'projects', 'action' => 'projectlead']);
    $routes->connect('/projectcredentials/:id', ['controller' => 'projects', 'action' => 'projectcredentials'],['pass' => ['id']]);
    $routes->connect('/projectcredentials', ['controller' => 'projects', 'action' => 'projectcredentials']);
    $routes->connect('/editprojecteam/:id', ['controller' => 'projects', 'action' => 'editprojecteam'],['pass' => ['id']]);
    $routes->connect('/editprojecteam', ['controller' => 'projects', 'action' => 'editprojecteam']);
    $routes->connect('/cngengagement', ['controller' => 'projects', 'action' => 'cngengagement']);
  });

  Router::prefix('testimonials', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'testimonials']);
    $routes->connect('/status'  , ['controller' => 'testimonials', 'action' => 'status']);
    $routes->connect('/testimonialslist', ['controller' => 'testimonials', 'action' => 'testimonialslist']);
    $routes->connect('/add', ['controller' => 'testimonials', 'action' => 'add']);
    $routes->connect('/edit/:id', ['controller' => 'testimonials', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/exportciodesk', ['controller' => 'testimonials', 'action' => 'exportciodesk']);
    $routes->connect('/exportcielance', ['controller' => 'testimonials', 'action' => 'exportcielance']);
    $routes->connect('/exportciother', ['controller' => 'testimonials', 'action' => 'exportciother']);
    $routes->connect('/download', ['controller' => 'testimonials', 'action' => 'download']);
    $routes->connect('/delete/:id', ['controller' => 'testimonials', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changeStatus', ['controller' => 'testimonials', 'action' => 'changeStatus']);
    $routes->connect('/elance', ['controller' => 'testimonials', 'action' => 'elance']);
    $routes->connect('/other', ['controller' => 'testimonials', 'action' => 'other']);
  });
    
  Router::prefix('leads', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'leads']);
    $routes->connect('/quickedit/:id', ['controller' => 'leads', 'action' => 'quickedit'],['pass' => ['id'] ]);
    $routes->connect('/leadslist', ['controller' => 'leads', 'action' => 'leadslist']);
    $routes->connect('/getOrigin', ['controller' => 'leads', 'action' => 'getOrigin']);
    $routes->connect('/new_list', ['controller' => 'leads', 'action' => 'new_list']);
    $routes->connect('/adminLeadreport', ['controller' => 'leads', 'action' => 'adminLeadreport']);
    $routes->connect('/edit/:id', ['controller' => 'leads', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/quickedit', ['controller' => 'leads', 'action' => 'quickedit']);
    $routes->connect('/edit', ['controller' => 'leads', 'action' => 'edit']);
    $routes->connect('/add', ['controller' => 'leads', 'action' => 'add']);
    $routes->connect('/activelead', ['controller' => 'leads', 'action' => 'activelead']);
    $routes->connect('/delete/:id', ['controller' => 'leads', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/delete/:id/:param2', ['controller' => 'leads', 'action' => 'delete'],['pass' => ['id','param2']]);
    $routes->connect('/leadreport1', ['controller' => 'leads', 'action' => 'leadreport1']);
    $routes->connect('/leadreport', ['controller' => 'leads', 'action' => 'leadreport']);
    $routes->connect('/bidspiechartreport', ['controller' => 'leads', 'action' => 'bidspiechartreport']);
    $routes->connect('/activeleadreport', ['controller' => 'leads', 'action' => 'activeleadreport']);
    $routes->connect('/winclosereport', ['controller' => 'leads', 'action' => 'winclosereport']);
    $routes->connect('/overleadcountuserreport', ['controller' => 'leads', 'action' => 'overleadcountuserreport']);
    $routes->connect('/companywideleadreport', ['controller' => 'leads', 'action' => 'companywideleadreport']);
    $routes->connect('/report', ['controller' => 'leads', 'action' => 'admin_leadreport']);
    $routes->connect('/quickadd', ['controller' => 'leads', 'action' => 'quickadd']);
    
  });

  Router::prefix('generics', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'generics']);
    $routes->connect('/admin_setting', ['controller' => 'generics', 'action' => 'admin_setting']);
    $routes->connect('/tagcloud', ['controller' => 'generics', 'action' => 'tagcloud']);
  });

  Router::prefix('sale_profiles', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'sale_profiles']);
    $routes->connect('/detail', ['controller' => 'sale_profiles', 'action' => 'profiledetail']);
    $routes->connect('/list', ['controller' => 'sale_profiles', 'action' => 'list']);
  }); 

  Router::prefix('hardwares', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'hardwares']);
    $routes->connect('/hardwareslist', ['controller' => 'hardwares', 'action' => 'hardwareslist']);
    $routes->connect('/add', ['controller' => 'hardwares', 'action' => 'add']);
    $routes->connect('/edit/:id', ['controller' => 'hardwares', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'hardwares', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/edit', ['controller' => 'hardwares', 'action' => 'edit']);
  });

  Router::prefix('credentials', function ($routes) {
    $routes->connect('/index', ['controller' => 'credentials', 'action' => 'index' ,'prefix'=>'credentials']);
    $routes->connect('/credentialscrone', ['controller' => 'credentials', 'action' => 'credentialscrone' ,'prefix'=>'credentials']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'credentials']);
    $routes->connect('/credentialslist', ['controller' => 'credentials', 'action' => 'credentialslist']);
    $routes->connect('/freecredits/:key', ['controller' => 'credentials', 'action' => 'freecredits'],['pass' => ['key']]);
    $routes->connect('/add', ['controller' => 'credentials', 'action' => 'add']);
    $routes->connect('/credentialsauditreport', ['controller' => 'credentials', 'action' => 'credentialsauditreport']);
    $routes->connect('/exportci', ['controller' => 'credentials', 'action' => 'exportci']);
    $routes->connect('/download', ['controller' => 'credentials', 'action' => 'download']);
    $routes->connect('/exportcigmail', ['controller' => 'credentials', 'action' => 'exportcigmail']);
    $routes->connect('/cyberoam', ['controller' => 'credentials', 'action' => 'cyberoam']);
    $routes->connect('/exportcidropbox', ['controller' => 'credentials', 'action' => 'exportcidropbox']);
    $routes->connect('/dropbox', ['controller' => 'credentials', 'action' => 'dropbox']);
    $routes->connect('/skype', ['controller' => 'credentials', 'action' => 'skype']);
    $routes->connect('/exportciskype', ['controller' => 'credentials', 'action' => 'exportciskype']);
    $routes->connect('/exportciwebmail', ['controller' => 'credentials', 'action' => 'exportciwebmail']);
    $routes->connect('/webmail', ['controller' => 'credentials', 'action' => 'webmail']);
    $routes->connect('/edit/:id', ['controller' => 'credentials', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'credentials', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changeStatus', ['controller' => 'credentials', 'action' => 'changeStatus']);
    $routes->connect('/allotedto/:id', ['controller' => 'credentials', 'action' => 'allotedto'],['pass' => ['id']]);
    $routes->connect('/allotedto', ['controller' => 'credentials', 'action' => 'allotedto']);
    $routes->connect('/sendemail/:id', ['controller' => 'credentials', 'action' => 'sendemail'],['pass' => ['id']]);
    $routes->connect('/sendemail', ['controller' => 'credentials', 'action' => 'sendemail']);
  });

  Router::prefix('contacts', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'contacts']);
    $routes->connect('/add', ['controller' => 'contacts', 'action' => 'add']);
    $routes->connect('/quickadd', ['controller' => 'contacts', 'action' => 'quickadd']);
    $routes->connect('/contactslist', ['controller' => 'contacts', 'action' => 'contactslist']);
    $routes->connect('/edit/:id', ['controller' => 'contacts', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'contacts', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changeStatus', ['controller' => 'contacts', 'action' => 'changeStatus']);
    $routes->connect('/getresults', ['controller' => 'contacts', 'action' => 'getresults']);    
  });
  Router::prefix('finance', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'finance']);
    $routes->connect('/add', ['controller' => 'finance', 'action' => 'add']);
    $routes->connect('/quickadd', ['controller' => 'finance', 'action' => 'quickadd']);
    $routes->connect('/list', ['controller' => 'finance', 'action' => 'list']);
    $routes->connect('/edit/:id', ['controller' => 'finance', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'finance', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/changeStatus', ['controller' => 'finance', 'action' => 'changeStatus']);
  });
  Router::prefix('faq', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'faq']);
    
    $routes->connect('/faqlist', ['controller' => 'faq', 'action' => 'faqlist']);
    $routes->connect('/faqdetails/:id', ['controller' => 'faq', 'action' => 'faqdetails'],['pass' => ['id']]);
    $routes->connect('/faqadd', ['controller' => 'faq', 'action' => 'faqadd']);
    $routes->connect('/faqpage', ['controller' => 'faq', 'action' => 'faqpage']);
    $routes->connect('/faqquickadd', ['controller' => 'faq', 'action' => 'faqquickadd']);
    $routes->connect('/faqedit/:id', ['controller' => 'faq', 'action' => 'faqedit'],['pass' => ['id']]);
    $routes->connect('/faqdelete/:id',['controller' => 'faq', 'action' => 'faqdelete'],['pass' => ['id']]);
    $routes->connect('/faqchangeStatus', ['controller' => 'faq', 'action' => 'faqchangeStatus']);
    
  });

  Router::prefix('permissions', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'permissions']);
    $routes->connect('/setting', ['controller' => 'permissions', 'action' => 'setting']);
    $routes->connect('/setPermissions', ['controller' => 'permissions', 'action' => 'setPermissions']);
    $routes->connect('/set_permissions', ['controller' => 'permissions', 'action' => 'set_permissions']);
    $routes->connect('/admin_list', ['controller' => 'permissions', 'action' => 'admin_list']);
    $routes->connect('/list', ['controller' => 'permissions', 'action' => 'list']);
    $routes->connect('/add', ['controller' => 'permissions', 'action' => 'add']);
    $routes->connect('/permissionsdetails', ['controller' => 'permissions', 'action' => 'permissionsdetails']);
    $routes->connect('/fetchUserBasedOnAction', ['controller' => 'permissions', 'action' => 'fetchUserBasedOnAction']);
    $routes->connect('/fetchUserBasedOnRole', ['controller' => 'permissions', 'action' => 'fetchUserBasedOnRole']);
    $routes->connect('/subactionscrone', ['controller' => 'permissions', 'action' => 'subactionscrone']);
    $routes->connect('/including/:id'  , ['controller' => 'permissions', 'action' => 'including'],['pass' => ['id']]);
    $routes->connect('/including'  , ['controller' => 'permissions', 'action' => 'including']);
    $routes->connect('/except/:id'  , ['controller' => 'permissions', 'action' => 'except'],['pass' => ['id']]);
    $routes->connect('/except'  , ['controller' => 'permissions', 'action' => 'except']);
    $routes->connect('/edit/:id', ['controller' => 'permissions', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'permissions', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/billingscrone', ['controller' => 'permissions', 'action' => 'billingscrone']);
  });
  Router::prefix('payment_methods', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'payment_methods']);
    $routes->connect('/paymentMethod', ['controller' => 'payment_methods', 'action' => 'payment_method']);
    $routes->connect('/paymentMethodlist', ['controller' => 'payment_methods', 'action' => 'paymentMethodlist']);
    $routes->connect('/edit/:id', ['controller' => 'payment_methods', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/delete/:id', ['controller' => 'payment_methods', 'action' => 'delete'],['pass' => ['id']]);
  });
  Router::prefix('payments', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'payments']);
    $routes->connect('/paymentslist', ['controller' => 'payments', 'action' => 'paymentslist']);
    $routes->connect('/quickadd/:id', ['controller' => 'payments', 'action' => 'quickadd'],['pass' => ['id']]);
    $routes->connect('/quickadd', ['controller' => 'payments', 'action' => 'quickadd']);
    $routes->connect('/quickview/:project_id', ['controller' => 'payments', 'action' => 'quickview'],['pass' => ['project_id']]);
    $routes->connect('/edit/:id', ['controller' => 'payments', 'action' => 'edit'],['pass' => ['id']]);
    $routes->connect('/edit', ['controller' => 'payments', 'action' => 'edit']);
    $routes->connect('/delete/:id', ['controller' => 'payments', 'action' => 'delete'],['pass' => ['id']]);
    $routes->connect('/add', ['controller' => 'payments', 'action' => 'add']);
    $routes->connect('/admin_paymentdetails/:id/:last', ['controller' => 'payments', 'action' => 'admin_paymentdetails'],['pass' => ['id','last']]);
    $routes->connect('/admin_paymentdetails/:id/:cur', ['controller' => 'payments', 'action' => 'admin_paymentdetails'],['pass' => ['id','cur']]);
    $routes->connect('/quickview/:id', ['controller' => 'payments', 'action' => 'quickview'],['pass' => ['id']]);
    $routes->connect('/quickview', ['controller' => 'payments', 'action' => 'quickview']);
    $routes->connect('/paymentreportweek', ['controller' => 'payments', 'action' => 'paymentreportweek']);
   });

  Router::prefix('reports', function ($routes) {
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'reports']);
    $routes->connect('/index', ['controller' => 'reports', 'action' => 'index']);
  });

  Router::prefix('modules', function ($routes) {
      $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'modules']);
      $routes->connect('/moduleslist'  , ['controller' => 'modules', 'action' => 'moduleslist']);
      $routes->connect('/modify'  , ['controller' => 'modules', 'action' => 'modify']);
      $routes->connect('/modifysub'  , ['controller' => 'modules', 'action' => 'modifysub']);
      $routes->connect('/addmodule'  , ['controller' => 'modules', 'action' => 'addmodule']);
      $routes->connect('/addsubmodule'  , ['controller' => 'modules', 'action' => 'addsubmodule']);
      $routes->connect('/editmodules/:origin'  , ['controller' => 'modules', 'action' => 'editmodules'],['pass' => ['origin']]);
      $routes->connect('/editmodules'  , ['controller' => 'modules', 'action' => 'editmodules']);
      $routes->connect('/deletemodule/:origin'  , ['controller' => 'modules', 'action' => 'deletemodule'],['pass' => ['origin']]);
      $routes->connect('/editsubmodules/:origin'  , ['controller' => 'modules', 'action' => 'editsubmodules'],['pass' => ['origin']]);
      $routes->connect('/deletesubmodule/:origin'  , ['controller' => 'modules', 'action' => 'deletesubmodule'],['pass' => ['origin']]);
      $routes->connect('/getactions'  , ['controller' => 'modules', 'action' => 'getactions']);
      $routes->connect('/getaliases'  , ['controller' => 'modules', 'action' => 'getaliases']);
      $routes->connect('/getmodulesdata'  , ['controller' => 'modules', 'action' => 'getmodulesdata']);
      $routes->connect('/extrafields', ['controller' => 'modules', 'action' => 'extrafields']);
      $routes->connect('/editsubmodules'  , ['controller' => 'modules', 'action' => 'editsubmodules']);
      $routes->connect('/getallclasses', ['controller' => 'modules', 'action' => 'getallclasses']);
  });
  Router::prefix('milestones', function ($routes) {
      $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'milestones']);
      $routes->connect('/milestoneslist/:id', ['controller' => 'milestones', 'action' => 'milestoneslist'],['pass' => ['id']]);
      $routes->connect('/milestoneslist', ['controller' => 'milestones', 'action' => 'milestoneslist']);
      $routes->connect('/fixedcontract', ['controller' => 'milestones', 'action' => 'fixedcontract']);
      $routes->connect('/verifyallmilestone', ['controller' => 'milestones', 'action' => 'verifyallmilestone']);
      $routes->connect('/edit', ['controller' => 'milestones', 'action' => 'edit']);
      $routes->connect('/edit/:id', ['controller' => 'milestones', 'action' => 'edit'],['pass' => ['id']]);
      $routes->connect('/delete', ['controller' => 'milestones', 'action' => 'delete']);
      $routes->connect('/delete/:id', ['controller' => 'milestones', 'action' => 'delete'],['pass' => ['id']]);
      $routes->connect('/verifymilestone', ['controller' => 'milestones', 'action' => 'verifymilestone']);
      $routes->connect('/unverifymilestone', ['controller' => 'milestones', 'action' => 'unverifymilestone']);
      $routes->connect('/add', ['controller' => 'milestones', 'action' => 'add']);
      $routes->connect('/add/:project_id', ['controller' => 'milestones', 'action' => 'add'],['pass' => ['project_id']]);

  });

  Router::prefix('credential_audits', function ($routes) {
      $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'credential_audits']);
      $routes->connect('/auditdetails/:id', ['controller' => 'credentialAudits', 'action' => 'auditdetails'],['pass' => ['id']]);
      $routes->connect('/auditdetails', ['controller' => 'credentialAudits', 'action' => 'auditdetails']);
      $routes->connect('/audit_report', ['controller' => 'credentialAudits', 'action' => 'audit_report']);
      $routes->connect('/delete', ['controller' => 'credentialAudits', 'action' => 'delete']);
      $routes->connect('/delete/:id', ['controller' => 'credentialAudits', 'action' => 'delete'],['pass' => ['id']]);
      $routes->connect('/changeStatus', ['controller' => 'credentialAudits', 'action' => 'changeStatus']);
  });

  Router::prefix('documents', function ($routes) {
      $routes->connect('/login', ['controller' => 'Users', 'action' => 'login' ,'prefix'=>'documents']);
      $routes->connect('/add', ['controller' => 'documents', 'action' => 'add']);
      $routes->connect('/addescription', ['controller' => 'documents', 'action' => 'addescription']);
      $routes->connect('/adddoc', ['controller' => 'documents', 'action' => 'adddoc']);
      $routes->connect('/addnote', ['controller' => 'documents', 'action' => 'addnote']);
      $routes->connect('/addnotedes', ['controller' => 'documents', 'action' => 'addnotedes']);
      $routes->connect('/getnote', ['controller' => 'documents', 'action' => 'getnote']);
      $routes->connect('/editnote', ['controller' => 'documents', 'action' => 'editnote']);
      $routes->connect('/delnote', ['controller' => 'documents', 'action' => 'delnote']);
      
      
  });
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
