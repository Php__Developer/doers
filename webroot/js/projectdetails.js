		$(document).ready(function(){
			attached_to = $('.attached_to').val();
			var socketurl = 'http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000';
			currentpage = $(document).find('.currentpage').val();
			var base = $('.base_loc').val();
			function tiggermsg(type,msg){
				if(type=='info'){
					$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
					$(document).find('.succesfontwrapper').find('.succesfont').append(msg);
					$(document).find('.succesfontwrapper').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.succesfontwrapper').removeClass('default_visible');
					}, 3000);
				} else if(type == 'success'){
					$(document).find('.successwrapperdv').find('.successmsg').html(' ');
					$(document).find('.successwrapperdv').find('.successmsg').append(msg);
					$(document).find('.successwrapperdv').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.successwrapperdv').removeClass('default_visible');
					}, 3000);
				} else if(type == 'error') {
					$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
					$(document).find('.succesfontwrapper').find('.succesfont').append(msg);
					$(document).find('.succesfontwrapper').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.succesfontwrapper').removeClass('default_visible');
					}, 3000);
				}
				
			}

			var base = $('.base_loc').val();
			$('.addfiles').on('click',function(){
				$('.uploadwrapper').removeClass('default_hidden');
				$(this).addClass('default_hidden');
			});

			$(document).on('keyup',function(event){
					if(event.keyCode == '27') { // 
					$('.uploadwrapper').addClass('default_hidden');
					$('.addfiles').removeClass('default_hidden');
					$('.noteswrapper').addClass('default_hidden');
					$('.addnotesbtn').removeClass('default_hidden');
					 }
			});
						(function() {

								[].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
									new CBPFWTabs( el );
								});

								})();
			
			
			$('.editbox').on('keyup','.deseditbox',function(event){
					event.preventDefault();
					var thisis = $(this);
					//console.log(event.keyCode);
				 if (event.keyCode == '13') {
				 	//console.log("Enter PRessed");
				 	var docid =  $(this).closest('.singledoc').find('.doc_id').val();
				 	var des = $(this).val();
				 	datatopost = {doc_id : docid , description : des};
				 	 $.ajax({
					    url: base +'documents/addescription',
					    type: "post",
					    data: datatopost,
					    beforeSend: function() {
					    },
					    complete: function(){
					    },
					    success: function(data){
						   	tiggermsg('success', 'Description Added!');
						   	thisis.closest().remove();
						   	var description = $(this).closest('.singledoc').find('li').find('.description').attr('title',des);
						},
					    error: function(result){
					        console.log(result);
					    }
  					});
				 } else if(event.keyCode == '27') { // 
				 	//console.log("escape PRessed");
				 	thisis.remove();
				 }
			});
		$('.adddes').editable({
		     validate: function(value) {
		       if($.trim(value) == '') return 'This field is required';
		     },
		     //mode: 'inline',
		     url: base+ 'documents/addescription', 
	           ajaxOptions: {
	               dataType: 'json' //assuming json response
	           },           
	           success: function(data, config) {
	               if(data && data.id) {  //record created, response like {"id": 2}
	                   //set pk
	                   $(this).editable('option', 'pk', data.id);
	                   //remove unsaved class
	                   $(this).removeClass('editable-unsaved');
	                   //show messages
	                   var msg = 'New user created! Now editables submit individually.';
	                   $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
	                   $('#save-btn').hide(); 
	                   $(this).off('save.newuser');                     
	               } else if(data && data.errors){ 
	                   //server-side validation error, response like {"errors": {"username": "username already exist"} }
	                   config.error.call(this, data.errors);
	               }               
	           },
	           error: function(errors) {
	               var msg = '';
	               if(errors && errors.responseText) { //ajax error, errors = xhr object
	                   msg = errors.responseText;
	               } else { //validation error (client-side or server-side)
	                   $.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	               } 
	               $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	           }
		   });

		$(document).on('click','.deldoc',function(event){
			event.preventDefault();
			 var id = $(this).attr('href');
			 var datatopost = {docid : id};
				 swal({   
	            title: "Are you sure to delete?",   
	            text: "File Will be Deleted Permanently!",   
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Yes, delete it!",   
	            closeOnConfirm: false 
	        }, function(){ 
	        		  $.ajax({
						    url: socketurl+'/deldoc',
						    data:datatopost,
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	//var data  = JSON.parse( data ); 
							    if(data.response == 'success' ){
						    		swal("Deleted!", "Doc Deleted!", "success"); 
						    		$(document).find('.reloaddocs').trigger('click');
							    }
							    if( data.response == 'failed' ){
						    		swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});
	        	//console.log("idk");
	            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 

	        });
		});
		 $('.textarea_editor').wysihtml5({
		 	"font-styles": false,
		 	"image": false, //Button to insert an image.
		 	"link": false, //Button to insert a link.
		 	});
		$(document).on('click','.addnote',function(event){ 
				attached_to = $('.attached_to').val();
				var content = $('.textarea_editor').val();
				var originagency = $('.emporiginagency').val();
				var base = $('.base').val();
				var entity_id = $('.entity_id').val();
				var notetitle =  $('.notetitle').val();
				var datatopost = {attached_to : attached_to, content : content ,originagency : originagency , entity_id : entity_id, title :notetitle }
				$.ajax({
						    url: base+'documents/addnote',
						    data:datatopost,
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	var data  = JSON.parse( data ); 
							    if(data.status == 'success' ){
							    	tiggermsg('success', 'Note Added!');
						    		//swal("Deleted!", "Doc Deleted!", "success"); 
						    		$(document).find('.reloadnotes').trigger('click');
							    }
							    if( data.response == 'failed' ){
						    		//swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});

		});
			$('.addnotesbtn').on('click',function(){
					$('.noteswrapper').removeClass('default_hidden');
					$('iframe').contents().find('.wysihtml5-editor').html(' ');
					$(document).find('.notetitle').val('Untitled');
					$('.originwrapper').find('input').remove();
					$(this).addClass('default_hidden');
					$('.addnote').removeClass('default_hidden');
					$('.editnotebtn').addClass('default_hidden');
					
			});	

			$('.addnotcmnt').editable({
		     validate: function(value) {
		       if($.trim(value) == '') return 'This field is required';
		     },
		     //mode: 'inline',
		     url: base+ 'documents/addnotedes', 
	           ajaxOptions: {
	               dataType: 'json' //assuming json response
	           },           
	           success: function(data, config) {
	               if(data && data.id) {  //record created, response like {"id": 2}
	                   //set pk
	                   $(this).editable('option', 'pk', data.id);
	                   //remove unsaved class
	                   $(this).removeClass('editable-unsaved');
	                   //show messages
	                   var msg = 'New user created! Now editables submit individually.';
	                   $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
	                   $('#save-btn').hide(); 
	                   $(this).off('save.newuser');                     
	               } else if(data && data.errors){ 
	                   //server-side validation error, response like {"errors": {"username": "username already exist"} }
	                   config.error.call(this, data.errors);
	               }               
	           },
	           error: function(errors) {
	               var msg = '';
	               if(errors && errors.responseText) { //ajax error, errors = xhr object
	                   msg = errors.responseText;
	               } else { //validation error (client-side or server-side)
	                   $.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	               } 
	               $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	           }
		   });

			$('.notemainwrapper').slimScroll({
	            color: '#00f',
	            size: '10px',
	            height: '400px',
	            alwaysVisible: true
	        });
			$('.noteresulssets').on('click','.editnote',function(event){
				event.preventDefault();
				var id = $(this).attr('href');
					$.ajax({
						    url: base+'documents/getnote',
						    data:{origin : id},
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	///console.log(data.status);
						    	var data  = JSON.parse( data ); 
							    if(data.status == 'success' ){
							    	$('iframe').contents().find('.wysihtml5-editor').html(data.note.content);
							    	$('.addnote').addClass('default_hidden');
									//$('.editnote').removeClass('default_hidden');
									$('.editnotebtn').removeClass('default_hidden');
							    	$('.noteswrapper').removeClass('default_hidden');
									$('.addnotesbtn').addClass('default_hidden');
									setTimeout(function(){
										$(document).find('.notetitle').val(data.note.title);
										$('.originwrapper').append('<input class="form-control noteorigin"  type="hidden"  value="'+ data.note.id +'">');	
									},200)
									
							    }
							    if( data.response == 'failed' ){
						    		//swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});	
			});




			$('.editnotebtn').on('click',function(event){
				attached_to = $('.attached_to').val();
				var content = $('.textarea_editor').val();
				var originagency = $('.emporiginagency').val();
				var origin = $('.noteorigin').val();
				var base = $('.base').val();
				var entity_id = $('.entity_id').val();
				var notetitle =  $('.notetitle').val();
				var datatopost = {attached_to : attached_to, content : content ,originagency : originagency , entity_id : entity_id, title :notetitle ,origin : origin}
				$.ajax({
						    url: base+'documents/editnote',
						    data:datatopost,
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	//console.log(data.status);
						    	var data = JSON.parse(data);
							    if(data.status == 'success' ){
							    	tiggermsg('success', 'Note Updated!');
						    		//swal("Deleted!", "Doc Deleted!", "success"); 
						    		$(document).find('.reloadnotes').trigger('click');
							    }
							    if( data.response == 'failed' ){
						    		//swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});
			});

		$(document).on('click', '.reloadnotes' , function (event) {
	    	event.preventDefault();
	    	datatopost = {task : 'reloadnotes'};
	    	var url = (currentpage == 'projectdetails') ? base+'projects/projectdetails/'+attached_to: currentpage;
	    	 		$.ajax({
					    url: url,
					    type: "post",
						contentType: "application/json; charset=utf-8",
					    data: datatopost,
					    beforeSend: function() {
					    	//console.log("Beforsend")
					    },
					    complete: function(){
					    	//console.log("complete")
					    },
					    success: function(data){
					    	//console.log(data);
					    	var result = JSON.parse(data);
						   	if(result.status == 'success'){
						   		 var out = '';
					                  $.each(result.docs, function( index, v ) {
					                  	//console.log(v);
					                   // var lebeltext = v.type;
					                   
					                   // out+='<div class="col-md-4 singledoc"><div class="white-box"><div class="col-md-12"><div class="col-md-12 m-t-5"><i class="fa '+ lebelclass +' bigfa" title="'+ lebeltext +'"></i></div><div class="col-md-12 m-t-5"><div class="row"><div class="font-bold orgnalname"><a href="https://doersdocs.s3.amazonaws.com/projects/'+ v.name +'" target ="_blank">' + v.original_name + '</a></div></div><div class="row"><div class="addedby">Added By:'+ v.Creater.first_name +' on '+ v.Creater.last_name +'</div></div>  <div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+ v.id +'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+ (v.description != '') ? v.description : "Add Comment" +'</a></div></div></div></div><div class="col-md-6"></div></div><div class="editbox"></div></div>';
					                   var des = (v.description != null) ? v.description : "Add Comment";
					                   var x = '<div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+ v.id +'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+ des +'</a></div></div></div>';
					                   out+='<div class="col-md-4 singlenote "><a href="'+v.id+'" class="delnote pull-right  m-l-5"><i class="fa fa-times"></i></a><a href="'+v.id+'" class="editnote pull-right"><i class="fa fa-pencil"></i></a><div class="white-box"><div class="" style=""><div class="notemainwrapper p-10" style="height:400px;"><div class="row"><div class="col-md-12"><h4 class="font-bold">'+v.title+'</h4></div><div class="col-md-12 maincontent">'+v.content+'</div></div></div><div class="slimScrollBar" style=""></div><div class="slimScrollRail" style=""></div></div><div class="row text-center"><div class="addedby">Added By:'+ v.Creater.first_name +' on '+ v.Creater.last_name +'</div></div><div class="row text-center">'+ x +'</div></div></div>';
					                    //console.log(out);
					                   // out+='<div class="row"><div class="addedby">Added By:'+v.Creater.first_name+'  '+v.Creater.last_name+' on '+v.created+'</div></div>';
					                   // out+='<div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+v.id+'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+(v.description != '') ? v.description: "Add Comment"+'</a></div></div></div></div><div class="col-md-6"></div></div><div class="editbox"></div></div>';
					                });
									
					               $('.noteresulssets').html(' ');
					               $('.noteresulssets').append(out);
					               editable();
					               restoreeveryting();
					               setTimeout(function(){ 
							                $('.notemainwrapper').slimScroll({
									            color: '#00f',
									            size: '10px',
									            height: '400px',
									            alwaysVisible: true
								       	 });
									}, 200);
					              
					               
						   	}
						},
					    error: function(result){
					        console.log(result);
					    }
  					});
	   		 });

		
		$(document).on('click','.ticcond',function(event){
				classname = $(this).attr('data-classname');
				 if ($(this).is(':checked')){ 
				 		console.log("CHecked");
					    var orderval = $('.'+ classname).val('yes');
						var url = document.URL;
						$('#adminreport').attr('action',url);
				}else{ 
						var orderval = $('.'+ classname).val('no');
						var url = document.URL;
						$('#adminreport').attr('action',url);
				}
				$('#adminreport').submit();
		});

		var paymentsloaded='no';
		$('.pdtabs').on('click','.paymentstab',function(event){
			var type= $(this).attr('data-type');
			var attached_to = $('.attached_to').val();
			if(type == 'payments' && paymentsloaded == 'no'){
				datatopost = {task : 'payments',  pid : attached_to , renderview : 'yes' };
    			var url = (currentpage == 'projectdetails') ? base+'projects/getdocs': currentpage;
				detailajax(datatopost,url,'payments');
			}
		});
		$(document).on('click','.reloadpayments',function(event){
			event.preventDefault();
			paymentsloaded='no';
			console.log($(document).find('.pdtabs').find('.paymentstab').attr('class'));
			$(document).find('.pdtabs').find('.paymentstab')[0].click();
		});
		milestonesloaded = 'no';
		$('.pdtabs').on('click','.milestonestab',function(event){
			var type= $(this).attr('data-type');
			var attached_to = $('.attached_to').val();
			if(type == 'milestones' && milestonesloaded == 'no'){
				datatopost = {task : 'milestones',  pid : attached_to , renderview : 'yes' };
    			var url = (currentpage == 'projectdetails') ? base+'projects/getdocs': currentpage;
				detailajax(datatopost,url,'milestones');
			}
		});
		$(document).on('click','.reloadmilestones',function(event){
			event.preventDefault();
			milestonesloaded='no';
			//console.log($(document).find('.pdtabs').find('.paymentstab').attr('class'));
			$(document).find('.pdtabs').find('.milestonestab')[0].click();
		});

		$(document).on('click','.delnote',function(event){
			event.preventDefault();
			 var id = $(this).attr('href');
			 var datatopost = {docid : id};
				 swal({   
	            title: "Are you sure to delete?",   
	            text: "Note Will be Deleted Permanently!",   
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Yes, delete it!",   
	            closeOnConfirm: false 
	        }, function(){ 
	        		  $.ajax({
						    url: base+'documents/delnote',
						    data:datatopost,
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	var data  = JSON.parse( data ); 
							    if(data.status == 'success' ){
							    	$(document).find('.reloadnotes').trigger('click');
						    		swal("Deleted!", "Doc Deleted!", "success"); 
						    		//$(document).find('.reloadnotes').trigger('click');
							    }
							    if( data.response == 'failed' ){
						    		swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});
	        	//console.log("idk");
	            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 

	        });
		});

	var tabtype = window.location.hash.substr(1);
	if(tabtype !== ''){
		setTimeout(function () {
			//$('.sttabs').find('li').removeClass('tab-current');
			//$('.'+ tabtype).closest('li').addClass('tab-current');
			$('.'+ tabtype).closest('li').click();
			//$('.content-wrap ').find('section').removeClass('content-current');
			//$('#'+ tabtype).addClass('content-current');
		},200)
		
	}
	//console.log(type);	

	$(document).on('click','.addtask',function(event){
			console.log("CLicked Add Task");
			event.preventDefault();
			var type = $(this).attr('data-type');
			var task_project = $(this).attr('data-id');
			var attached_to = $('.attached_to').val();
			//var sc = setCookie(cname, cvalue, exdays)
			var entity_type = $('.entity_type').val();
			var entity_id =  $('.entity_id_original').val();
			var task_type = 'To-Do';
			var arr = { entity_type : entity_type,  entity_id : entity_id , task_type : task_type };
			var counter = 0;
			$.each(arr,function(k,v){
				setCookie(k, v, 1);
				counter++;
				if(counter == 3){
					$('.task_type').val('To-Do');
					$('#adminreport').attr('action',base + 'tickets/add');
					$('#adminreport').submit();
				}
			});

			//$('.task_type').val('To-do');
			//$('#adminreport').attr('action',base + 'tickets/add');
			//$('#adminreport').submit();
		});

	 $(document).on('click','.logstab',function (event) {
          console.log("asdfas");
              var projectid =  $('.attached_to').val();
              datatopost = {originidentity : projectid, task : 'log',key : 'close', check_it : 0 ,ids : projectid ,base_url : base , pageno : 1};
              var url = socketurl + '/projectactivities';
              var type="logs";
              //triggerajax(datatopost,url,'log',$(this) );
              detailajax(datatopost,url,type)
      });	



	function restoreeveryting(){
					$('.noteswrapper').addClass('default_hidden');
					$('iframe').contents().find('.wysihtml5-editor').html(' ');
					$(document).find('.notetitle').val('Untitled');
					$('.originwrapper').find('input').remove();
					$('.addnotesbtn').removeClass('default_hidden');
					$('.addnote').addClass('default_hidden');
					$('.editnotebtn').addClass('default_hidden');
	}

	function editable(){
				

				$('.adddes').editable({
		     validate: function(value) {
		       if($.trim(value) == '') return 'This field is required';
		     },
		     //mode: 'inline',
		     url: base+ 'documents/addescription', 
	           ajaxOptions: {
	               dataType: 'json' //assuming json response
	           },           
	           success: function(data, config) {
	               if(data && data.id) {  //record created, response like {"id": 2}
	                   //set pk
	                   $(this).editable('option', 'pk', data.id);
	                   //remove unsaved class
	                   $(this).removeClass('editable-unsaved');
	                   //show messages
	                   var msg = 'New user created! Now editables submit individually.';
	                   $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
	                   $('#save-btn').hide(); 
	                   $(this).off('save.newuser');                     
	               } else if(data && data.errors){ 
	                   //server-side validation error, response like {"errors": {"username": "username already exist"} }
	                   config.error.call(this, data.errors);
	               }               
	           },
	           error: function(errors) {
	               var msg = '';
	               if(errors && errors.responseText) { //ajax error, errors = xhr object
	                   msg = errors.responseText;
	               } else { //validation error (client-side or server-side)
	                   $.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	               } 
	               $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	           }
		   });
	}
	function detailajax(datatopost,url,type){
						$.ajax({
							    url: url,
							    data:datatopost,
							    type: "post",
							    beforeSend: function() {
							    },
							    complete: function(){
							    },
							    success: function(data){
							    	if(type == 'payments'){
							    			$('.paymentswrapper').html('');
								    		$('.paymentswrapper').append(data);
								    		paymentsloaded ='yes';
								    	} else if(type == 'milestones'){
								    		$('.milestoneswrapper').html('');
								    		$('.milestoneswrapper').append(data);
								    		milestonesloaded ='yes';
								    	}else if(typeof data =='object'){ 
								    		console.log(data.resultData);
								    		 if(data.response == 'success' ){
									    	if (type == 'logs'){
									    		
                       						 var out ='<div class="white-box p-t-0"><h3 class="box-title">logs</h3><ul class="feeds">';
						                         $.each(data.resultData ,function(key,value ) {
						                            if(value.type == 'information'){
						                                bgclass = 'bg-info';
						                                faicon =  'fa-bell-o';
						                            } else if(value.type == 'information'){
						                                bgclass = 'bg-danger';
						                                faicon =  'fa-bell-o';
						                            } else {
						                                bgclass = 'bg-success';
						                                faicon =  'fa-bell-o';
						                            }
						                            if(value.role == 'r'){
						                              roletxt = 'Responsible';
						                            } else if (value.role == 'a'){
						                              roletxt = 'Accountable';
						                            } else if(value.role = 'c'){
						                              roletxt = 'Consultant';
						                            }else if(value.role = 'i'){
						                              roletxt = 'Informer';
						                            }
						                            var logtxt = value.to_name + ' was been assigned '+ roletxt
						                            var shorttext = ((logtxt).length > 120) ? (logtxt).substr(0,120)+ '...' : logtxt; 
						                            out += '<li title ="'+ logtxt +'"><div class="'+bgclass+'"><i class="fa '+faicon +' text-white"></i></div>'+shorttext+'. <span class="text-muted"></span></li>';
						                          }); 
						                         out +='</ul></div>';
						                              $('.logswrapper').html('');
						                              $('.logswrapper').append(out);

                  							}
									    	//tiggermsg('success', 'Note Updated!');
										    }
								    	} else  {
								    		data = JSON.parse(data);
									    if(data.status == 'success' ){
									    	if(type == 'payments'){
									    		console.log(data);
									    	} 
									    	//tiggermsg('success', 'Note Updated!');
										    }
										    if( data.response == 'failed' ){
									    		//swal("Sorry!",JSON.parse( data ).reason , "error"); 
										    }	
								        }


							    	
							    }
		  					});
	}

	function setCookie(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	    return "yes";
	}

	function getCookie(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) == ' ') {
	            c = c.substring(1);
	        }
	        if (c.indexOf(name) == 0) {
	            return c.substring(name.length, c.length);
	        }
	    }
	    return "";
	}

	function checkCookie() {
	    var user = getCookie("username");
	    if (user != "") {
	        alert("Welcome again " + user);
	    } else {
	        user = prompt("Please enter your name:", "");
	        if (user != "" && user != null) {
	            setCookie("username", user, 365);
	        }
	    }
	}

		});
		