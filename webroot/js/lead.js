$(document).ready(function(){

var base = $(document).find('.base').val();
var page_name = $(document).find('.pe').val();

         function convert(str) {
                var date = new Date(str),
                 mnth = ("0" + (date.getMonth()+1)).slice(-2),
               day  = date.getDate() - 1;
              return [  mnth,day,date.getFullYear() ].join("/");
          }

       /*
      @WHY => Common function for check input box validation  in jquery 
      @WHEN  => 21-08-2016
      @WHO => Gurpreet kaur
      */

      function check_html (str) {   
        var regEx = /<|>/g;
        var testString = ''+str+'';
        var search_input =(testString.replace(regEx,""));
        return search_input;
      }

//alert(checkNumericValue(2));
// check negative value 
function checkNumericValue(num)
{
var objRegExp  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
return objRegExp.test(num);
}


function dating(str) {
                    var date = new Date(str),
                     mnth = ("0" + (date.getMonth()+1)).slice(-2),
                       day  = date.getDate() - 1;
                      
                      return [  date.getFullYear(),mnth,day].join("");
                    }

  var base=$(document).find('.base').val();
      //console.log(base);

      /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

$(document).on('click', '.firstLISt li', function(event) {
//$('.alldata').on('click', '.firstLISt li', function(event) {
 event.preventDefault();
  var active_class= $(this).find("a span i").attr('class');
  if(active_class=="ti-home"){
          $(this).closest(".nav-tabs").find(".profile").removeClass('active');
          $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
          $(this).closest(".nav-tabs").find(".home").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").addClass('active');
   }else if(active_class=="ti-user"){
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
      $(this).closest('.alldata').find('.tab-content').find(".iprofile").addClass('active');
   }else if(active_class=="ti-email"){
     $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").addClass('active');
         $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".imessages").addClass('active');
   }else if(active_class=="ti-settings"){
      $(this).closest(".nav-tabs").find(".settings").addClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".isettings").addClass('active');
   }


   });

  /* @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */


   $(document).on('click', '.drp_list li', function(e) {
  e.preventDefault();
   var clicked_icon=$(this).find("a").attr("class");
  if(clicked_icon==1){
     if(page_name=="leadlist"){
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".bidd").val();
            var b= $(b).find(".bidd").val();
            return a > b;
            }) ); 
     }

   }else if(clicked_icon==2){
    if(page_name=="leadlist"){
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".url").val();
            var b= $(b).find(".url").val();
            return a > b;
            }) ); 
     }     
   }else if(clicked_icon==3){
            $(document).find(".parent_data").html(
        $(".alldata").sort(function (a, b) {
        var a= $(a).find(".3").text();
        var b= $(b).find(".3").text();
        return a > b;
        }) // End Sort
       ); 
     }else if(clicked_icon==4){
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".4").val();
            var b= $(b).find(".4").val();
            return a > b;
            }) ); 
       
        }else if(clicked_icon==5){
           if(page_name=="leadlist"){
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".follow").val();
            var b= $(b).find(".follow").val();
            return a > b;
            }) ); 
     }    
      }  
  });


  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop2').hide();
  });

  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop1').hide();
  });

    $('.addlead').on('click',function(event){
    event.preventDefault();
    var bidding_date = $('#bidding_date').val();
    var url_title = $('.url_title').val();
    var technology = $('.technology').val();
    var source = $('.source').val();
    var category = $('.category').val();
    var agency_id = $('.agency_id').val();
    var profile_id = $('.profile_id').val();
    var perspective_amount = $('.perspective_amount').val();
    var status = $('.status').val();
    var nextfollow_date = $('#nextfollow_date').val();
    var time_spent = $('.time_spent').val();
    var notes = $('.notes').val();
    var href = base + 'leads/quickadd';
    data_to_post = {
        bidding_date : bidding_date,
        url_title :url_title,
        technology : technology,
        source : source,
        agency_id :agency_id,
        profile_id :profile_id,
        perspective_amount :perspective_amount,
        status :status,
        nextfollow_date:nextfollow_date,
        time_spent :time_spent,
        notes:notes
    }
    $.post(href,data_to_post,function(data){
        var data = JSON.parse( data )
        console.log(data);
        if(data.status == 'success' ){
          console.log(window.parent.$(".newleadname").attr('name'));
          window.parent.$(".newleadname").val(data.name);
          window.parent.$(".newleadid").val(data.id);
          window.parent.$(".newleadname").trigger('change');
          //window.parent.$(".leadtagsinputwrapper").removeClass('default_hidden');
          
        } else if (data.status == 'Validationerrors'){
          $.each(data.errors, function(key,value) {
             // console.log(key);
              $(document).find('.errors').html(' ');
              if(typeof value._empty !== "undefined") {
                $(document).find('.'+ key ).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ value._empty +' </div>');  
              } else if(typeof value.unique !== "undefined"){
                $(document).find('.'+ key ).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ value.unique +' </div>');  
              } else if(typeof value.notBlank !== "undefined"){
                $(document).find('.'+ key ).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ value.notBlank   +' </div>');  
              }  
          });
        }
    });
  });



}); //end function

