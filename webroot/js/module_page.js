	var firsttime = true;
	var controllers = [];
	var allactions =[];
	var originaldata = [];
	var selecthtml ='';
 $(document).ready(function() {
 		var base_url = $('.base').val();
    if($(document).find('div').hasClass('c0ntr0ll__')){
		 		
 			 $('.c0ntr0ll__').hide()
 			 
		}
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
       var defer = $.Deferred();
       // for slimscroll 
       var hitdb = 'yes';
       var through = '';
       //var totallength =0;
      
          $('.modulesscroll').slimScroll({
            color: '#00f',
            size: '10px',
            height: '250px',
            alwaysVisible: true
        });
    /**********************************************************************/
	/* @page : modules
	/* @who : Maninder
	/* @why : for enabling disbaling modules & submodules
	/* @date: 12-01-2017
	**********************************************************************/
          	
          $('.moduleChekb0x').on('change',function(){
          	var base_url = $('.base').val();
          	$('checkbox').removeClass('clicked');
          	$(this).addClass('clicked');
          	$(this).closest('.module_wrapper0_').find('.submoduleChekb0x').removeClass('.currentsubmodule');
          	$(this).closest('.module_wrapper0_').find('.submoduleChekb0x').addClass('currentsubmodule');
          	var menuOrigin = $(this).closest('.moduleChekb0x_wrapper').find('.m0e0n0u__').val();
          	var eORd = ($(this).is(":checked")) ? 'Yes':'No';
          	href = base_url+'modules/modify';
          	data_to_post = {"menuOrigin":menuOrigin,'eORd': eORd};
			$.post(href,data_to_post,function(data){
				if(JSON.parse( data ).status = 'success' ){
					if(eORd == 'No'){
						var totallength = $( ".currentsubmodule" ).length;
						 var indexed = 1;
						through = 'module';
						hitdb = 'no';
						$( ".currentsubmodule" ).each(function( index ) {
								if($(this).is(":checked")){
									var menuOrigin = $(this).closest('tr').find('.subm0e0n0u__').val();
									var eORd = ($(this).is(":checked")) ? 'Yes':'No';
									href = base_url+'modules/modifysub';
									data_to_post = {"menuOrigin":menuOrigin,'eORd': eORd};
									$(this).click();
								}
								if(indexed == totallength){
									hitdb = 'yes';
								}
							indexed++;
						});
						
					}
					$('.successmessage').html(' ');
			   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Changes Made Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
			   		$('.successmessage').addClass('default_visible');
			   		setTimeout(function(){
			   			 $('.successmessage').removeClass('default_visible');
			   		},2000);
				} else if (JSON.parse( data ).status == 'failed' ){
							$(document).find('.errormsg').html(' ');
							$('.errormsg').addClass('default_visible');
							$(document).find('.errormsg').append('<button type="button" class="close closeerror" data-dismiss="alert" aria-hidden="true">&times;</button>'+ JSON.parse( data ).reason +'');	
				}
			});
          });

    /**********************************************************************/
	/* @page :Modules
	/* @who : Maninder
	/* @why : For enabling disabling modules & submodules
	/* @date: 12-01-2017
	**********************************************************************/

            $('.submoduleChekb0x').on('change',function(){
				$('checkbox').removeClass('clicked');
				$('.errormsg').removeClass('default_visible');
				$(this).addClass('clicked');
				var menuOrigin = $(this).closest('tr').find('.subm0e0n0u__').val();
				var eORd = ($(this).is(":checked")) ? 'Yes':'No';
				href = base_url+'modules/modifysub';
				data_to_post = {"menuOrigin":menuOrigin,'eORd': eORd};
          		if(hitdb == 'yes'){
          			$.post(href,data_to_post,function(data){
						if(JSON.parse( data ).status == 'success' ){
								$('.successmessage').html(' ');
						   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Changes Made Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
						   		$('.successmessage').addClass('default_visible');
						   		setTimeout(function(){
						   			 //window.location.href = base_url + 'modules/moduleslist';
						   			 $('.successmessage').removeClass('default_visible');
						   		},2000);
							//dfd.resolve(JSON.parse( data ).status); // Resolve the promise, give back the response (data)
						} else if (JSON.parse( data ).status == 'failed' ){
							$(document).find('.errormsg').html(' ');
							$('.errormsg').addClass('default_visible');
							$(document).find('.errormsg').append('<button type="button" class="close closeerror" data-dismiss="alert" aria-hidden="true">&times;</button>'+ JSON.parse( data ).reason +'');	
						}
					});
          		}
          });
           $('body').on('click','.closeerror',function(){
           		$('.errormsg').removeClass('default_visible');
           });
    /**********************************************************************/
	/* @page :Modules
	/* @who : Maninder
	/* @why :Adding suggestions while typing
	/* @date: 12-01-2017
	**********************************************************************/

	if($('.c0ntrol3r').val() != '' ) {
		setTimeout(function(){
			$('.input-group').find('.c0ntrol3r').change();
		},200);
		
		}

			allactions =[];
			var name= 'none';
			href = base_url+'modules/getaliases';
				data_to_post = {"cname":name};
          			$.post(href,data_to_post,function(data){
						if(JSON.parse( data ).status = 'success' ){
							var out = "";
							//var allactions = 
							if($('.mode').val() == 'add'){
								$('.tag51nput').tagsinput('removeAll');
							}
							//$('.acti0ns').html('');
							selectedaction = $('.selectedaction').val();
							originaldata = JSON.parse( data ).aliases;
							$.each(JSON.parse( data ).aliases,function(k,v){
								allactions.push(v);
							});
							//console.log(allactions);

								setTimeout(function(){
									 if($('.subm0dulefields').find('input').hasClass('tag51nput')){
									$('.bootstrap-tagsinput').find('input').hide();
									$('.tag51nput').tagsinput({
									confirmKeys: [13, 188],
									freeInput: false
									});

									
										}
									 	if($(document).find('div').hasClass('c0ntr0ll__')){
							 			 controllers = $('.c0ntr0ll__').text().split(',');
							 			 $('.c0ntr0ll__').text(' ');
									} else{
										 controllers = [];	
									}
									if($('.subm0dulefields').find('input').hasClass('typeaheadcontro__')){
									$('.typeaheadcontro__').typeahead('destroy');
									$('.typeaheadcontro__').typeahead({
										hint: true,
										highlight: true,
										minLength: 1
									},
									{
										name: 'states',
										source: substringMatcher(allactions),
									});
									$('.typeaheadcontro__').on('typeahead:select', function(ev, suggestion) {
											$('.tag51nput').tagsinput('add', suggestion);
									});
									}
									},500);


							//$('.acti0ns').append(out);
						}
					});
          			/*$('.tag51nput').on('itemAdded', function(event) {
											console.log("addded");
											//$(document).find('.typeaheadcontro__').val('');
										// event.item: contains the item
					});*/
			//});
					$('.tag51nput').on('beforeItemRemove', function(event) {
							var default_alias = $('.default_alias').val();
							$(document).find('.other_aliases_error').html(' ');
							$.each(originaldata,function(k,v){
								if(k == default_alias){
									if(v == event.item){
										$(document).find('.other_aliases_error').html(' ');
							   			$(document).find('.other_aliases_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Can not Remove Default Alias</div>');
						   				event.cancel = true;
									}
								}
							});
						//event.cancel = true;
					// event.item: contains the item
					// event.cancel: set to true to prevent the item getting removed
					});

			if($('.c0ntroller').val() != '' ) {
					setTimeout(function(){
						$('.input-group').find('.c0ntroller').change();
						
					},200);
			}		


			$('.form-group').on('change','.c0ntroller',function(event){
			event.stopImmediatePropagation();
			allactions =[];
			var name= $(this).val();
			href = base_url+'modules/getactions';
				data_to_post = {"cname":name};
          			$('.acti0n5').html('<option value="emmpty">Loading...</option>');
          			$.post(href,data_to_post,function(data){
						if(JSON.parse( data ).status = 'success' ){
							var out = "";
							//var allactions = 
							if($('.mode').val() == 'add'){
								$('.tag51nput').tagsinput('removeAll');
							}
							$('.acti0n5').html('');
							selectedaction = $('.selectedaction').val();
							$.each(JSON.parse( data ).data,function(k,v){
								if(v == selectedaction){
									out += '<option value='+v+' selected>'+v+'</option>';	
								} else{
									out += '<option value='+v+'>'+v+'</option>';
								}
								if($('.mode').val() == 'add'){

									$('.tag51nput').tagsinput('add', v);	
								}
								allactions.push(v);
							});
							//console.log(allactions);
							$('.acti0n5').append(out);
							setTimeout(function(){
								if($('.actionval').val() !== ''){
									$('.acti0n5').val($('.actionval').val());
								}
							},200);
						}
					});
          		
			});


		   $('.delete1con').on('click',function(event){
		   	event.preventDefault();
		   	var ref = $(this).attr('href');
        swal({   
            title: "Are you sure to delete?",   
            text: "All Sub-modules will be deleted if you delete a module",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){ 
        		  $.ajax({
					    url: ref,
					    type: "get",
					    beforeSend: function() {
					    },
					    complete: function(){
					    },
					    success: function(data){
						    if(JSON.parse( data ).status == 'success' ){
					    		swal("Deleted!", "Module has been Deleted!", "success"); 
						    }
						    if(JSON.parse( data ).status == 'failed' ){
					    		swal("Sorry!",JSON.parse( data ).reason , "error"); 
						    }
					    }
  					});
        	//console.log("idk");
            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 

        });
    });
 	


 	/**********************************************************************/
	/* @page :Modules
	/* @who : Maninder
	/* @why : For enabling disabling modules & submodules
	/* @date: 12-01-2017
	**********************************************************************/

            $('.parentm0dule').on('change',function(){ 
            	var selected = $(this).val();
            
            	if($(this).val() == 0){
            		$('.subm0dulefields').addClass('default_hidden');
            		value = 'module';
            	} else{
            		value = 'submodule';
            		$('.subm0dulefields').removeClass('default_hidden');
            	}
            	$('.input-group').find('.thetype').val(value);
            });
            $('.default_alias').on('change',function(){ 
            	var selected = $(this).val();            	
            	if($(this).val() == 0){
            		
            		//$('.subm0dulefields').addClass('default_hidden');
            	} else{
            		
					$.each(originaldata,function(k,v){
						if(k == selected){
							$('.tag51nput').tagsinput('add', v);					
							}
					});
            	}
            });
            	
         if(/*!$('.parentm0dule').is(':empty')*/ $('.parentm0dule').val() !== '') {

         
			setTimeout(function(){
				$('.input-group').find('.parentm0dule').change();
			},200);
		}

		$('.m0dulesubmit').on('click',function(event){
			event.preventDefault();
			var module_name = $('.module_name').val();
			var mode = $('.mode').val();
			var display_name = $('.display_name').val();
			var parent_module = $('.parentm0dule').val();
			var is_enabled = $('.is_enabled').val();
			var is_displayed = $('.is_displayed').val();
			var controller = $('.c0ntrol3r').val();
			var default_alias = $('.default_alias').val();
			var other_aliases = $('.tag51nput').val();
			var description = $('.desc').val();
			var type =  $('.thetype').val();
			var agency_id = $('.agency_id').val();
			var role_id = $('.role_id').val();
			var id = $('.theid').val();
			var is_core = $('.is_core').val();
			var url = (mode == 'add') ? 'modules/addsubmodule': 'modules/editsubmodules';
			  $.ajax({
					    url: base_url + url,
					    type: "post",
					    data: {
					    		module_name : module_name,
				    		    mode : mode, 
				    		    display_name: display_name,
				    		    parent_module:parent_module,
				    		    is_enabled:is_enabled,
				    		    is_displayed:is_displayed,
				    		    controller:controller,
				    		    default_alias :default_alias,
				    		    other_aliases :other_aliases,
				    		    description :description,
				    		    type : type,
				    		    agency_id : agency_id,
				    		    role_id :role_id,
				    		    id:id,
				    		    is_core :is_core
				    	},
					    beforeSend: function() {
					    	$('.successmessageloader .succesfont').append('Saving...');
						   	$('.successmessageloader').addClass('default_visible');
					    },
					    complete: function(){
					    },
					    success: function(data){
						   $('.successmessageloader .succesfont').html(' ');
						   	$('.successmessageloader').removeClass('default_visible');
						   if(JSON.parse( data ).status == 'success' ){
						   		$('.successmessage').html(' ');
						   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Record Created Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
						   		$('.successmessage').addClass('default_visible');
						   		setTimeout(function(){
						   		 window.location.href = base_url + 'modules/moduleslist';
						   		},2000);
						   } else if(JSON.parse( data ).status == 'failed'){
						   		$.each(JSON.parse( data ).errors, function(key,value) {
						   			$(document).find('.'+ key + '_error').html(' ');
						   			if(typeof value._empty !== "undefined") {
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ value._empty +' </div>');	
						   			} else if(typeof value.unique !== "undefined"){
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ value.unique +' </div>');	
						   			}	
								}); 
						   }
						   
						}
  					});
		});
		$('.untracked_perms').on('click',function(event){
			var type = $('.type').val();
			$('.resultsets').html(' ');
			selecthtml = '';
			var url = 'modules/getallclasses';
				  $.ajax({
					    url: base_url + url,
					    type: "post",
					    data: {
					    		type : type,
				    	},
					    beforeSend: function() {
					    	$('.successmessageloader .succesfont').append('Loading...');
						   	$('.successmessageloader').addClass('default_visible');
					    },
					    complete: function(){
					    },
					    success: function(data){
								$('.successmessageloader .succesfont').html(' ');
								$('.successmessageloader').removeClass('default_visible');
						   if(JSON.parse( data ).status == 'success'  &&  JSON.parse( data ).type == 'permissions'){
						   	$('button').removeClass('default_hidden');
						   	var out ='<table class="tablesaw table-bordered table-hover table tablesaw-swipe permswrapper" data-tablesaw-mode="swipe" id="table-1937" style=""><thead><tr><th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist">Controller</th><th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="">Action</th><th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="">Options</th></tr></thead><tbody>';
						   		$.each(JSON.parse( data ).data , function(key,value) {
						   			if(value.length > 0){
						   				$.each(value , function(k,v) {
						   					/*console.log(k);
						   					console.log(key);*/
						   				out += '<tr><td class="title tablesaw-cell-persist thecontroller">'+key+'</td><td class="theaction">'+v+'</td><td class=""><a href="#" class="addperm">Add</a></td></tr>';
						   				});
						   			}
								});
						   		out +='</tbody></table>';
						   		$('.resultsets').append(out);
						   } else if(JSON.parse( data ).status == 'success'  && JSON.parse( data ).type == 'modules'){
						   		 
								 selecthtml += '<select name="module_id" class="modulesselect form-control">';
								 selecthtml +='<option value="" >--Select--</option>';
								$.each(JSON.parse( data ).modules , function(key,value) {
									selecthtml +='<option value="'+value.id+'" >'+value.display_name +'</option>';
								});
								selecthtml +='</select>';	   	
						   	var out ='<table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch><thead><tr><th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">Controller</th><th scope="col" data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="3">Action</th><th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">Alias</th><th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1"><abbr title="Rotten Tomato Rating">Modules</abbr></th></tr></thead><tbody>';
						   		$.each(JSON.parse( data ).data , function(key,value) {
						   				out += '<tr><td class="title"><a href="javascript:void(0)">'+value.controller+'</a></td><td>'+value.action+'</td><td>'+value.alias+'</td><td><input type="hidden" class="permid" value="'+value.id+'">'+selecthtml+'</td></tr>';
								});
						   		out +='</tbody></table>';
						   		$('.resultsets').append(out);
						   }
						}
  					});
				});
		$('.resultsets').on('change','.modulesselect',function(event){
			var $this = $(this)
			var module_id = $(this).val();
			if(module_id !== ""){
				var permission_id = $(this).closest('td').find('.permid').val();
				var type = 'addpermtomodule';
				var url = 'modules/getallclasses';
				 $.ajax({
					    url: base_url + url,
					    type: "post",
					    data: {
					    		type : type,
					    		module_id: module_id,
					    		permission_id :permission_id
				    	},
					    beforeSend: function() {
					    	$('.successmessageloader .succesfont').append('Loading...');
						   	$('.successmessageloader').addClass('default_visible');
					    },
					    complete: function(){
					    },
					    success: function(data){
								$('.successmessageloader .succesfont').html(' ');
								$('.successmessageloader').removeClass('default_visible');
						   if(JSON.parse( data ).status == 'success' ){
							   	$('.successmessage').html(' ');
						   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Permission Added Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
						   		$('.successmessage').addClass('default_visible');
						   		$this.closest('tr').remove();
						   		setTimeout(function(){
						   			 //window.location.href = base_url + 'modules/moduleslist';
						   			 $('.successmessage').removeClass('default_visible');
						   		},2000);
						   } else if(JSON.parse( data ).status == 'failed' ){
							   $.each(JSON.parse( data ).errors, function(key,value) {
						   			$(document).find('.'+ key + '_error').html(' ');
						   			if(typeof value._empty !== "undefined") {
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value._empty +' </div>');	
						   			} else if(typeof value.unique !== "undefined"){
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value.unique +' </div>');	
						   			}	
								}); 
						   }
						}
  					});
			}
			

		});


		$('.addall').on('click',function(event){
			var type = 'addall';
			var url = 'modules/getallclasses';
				  $.ajax({
					    url: base_url + url,
					    type: "post",
					    data: {
					    		type : type,
				    	},
					    beforeSend: function() {
					    	$('.successmessageloader .succesfont').append('Loading...');
						   	$('.successmessageloader').addClass('default_visible');
					    },
					    complete: function(){
					    },
					    success: function(data){
								$('.successmessageloader .succesfont').html(' ');
								$('.successmessageloader').removeClass('default_visible');
						   if(JSON.parse( data ).status == 'success' ){
							   	$('.successmessage').html(' ');
						   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Record Created Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
						   		$('.successmessage').addClass('default_visible');
						   } else if(JSON.parse( data ).status == 'failed' ){
							   $.each(JSON.parse( data ).errors, function(key,value) {
						   			$(document).find('.'+ key + '_error').html(' ');
						   			if(typeof value._empty !== "undefined") {
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value._empty +' </div>');	
						   			} else if(typeof value.unique !== "undefined"){
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value.unique +' </div>');	
						   			}	
								}); 
						   }
						}
  					});
		});
		$('.resultsets').on('click','.addperm',function(event){
			event.preventDefault();
			var $this = $(this);
		//$('.addperm').on('click',function(event){
			var type = 'addall';
			var controller = $(this).closest('tr').find('.thecontroller').text();
			var action = $(this).closest('tr').find('.theaction').text();
			var url = 'modules/getallclasses';
				  $.ajax({
					    url: base_url + url,
					    type: "post",
					    data: {
					    		type : type,
					    		controller : controller,
					    		action : action

				    	},
					    beforeSend: function() {
					    	$('.successmessageloader .succesfont').append('Loading...');
						   	$('.successmessageloader').addClass('default_visible');
					    },
					    complete: function(){
					    },
					    success: function(data){
								$('.successmessageloader .succesfont').html(' ');
								$('.successmessageloader').removeClass('default_visible');
						   if(JSON.parse( data ).status == 'success' ){
							   	$('.successmessage').html(' ');
						   		$('.successmessage').append('<i class="fa fa-thumbs-up"></i> Record Created Successfully! <a href="#" class="closed" data-dismiss="alert">&times;</a>');
						   		$('.successmessage').addClass('default_visible');
						   		$this.closest('tr').remove();
						   		setTimeout(function(){
						   			 //window.location.href = base_url + 'modules/moduleslist';
						   			 $('.successmessage').removeClass('default_visible');
						   		},2000);
						   } else if(JSON.parse( data ).status == 'failed' ){
							   $.each(JSON.parse( data ).errors, function(key,value) {
						   			$(document).find('.'+ key + '_error').html(' ');
						   			if(typeof value._empty !== "undefined") {
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value._empty +' </div>');	
						   			} else if(typeof value.unique !== "undefined"){
						   				$(document).find('.'+ key + '_error').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error For '+JSON.parse( data ).controller +','+JSON.parse( data ).action+' :'+ value.unique +' </div>');	
						   			}	
								}); 
						   }
						}
  					});
		});


         });
	
/*AJAX SUCCESS FUNCTIONS STRTS FROM BELOW*/

$(document).ajaxSuccess(function() {
  	/* var controllers = [];
		 var allactions =[];*/
		 //if(firsttime == true){
		setTimeout(function(){
		 if($('.subm0dulefields').find('input').hasClass('tag51nput')){
		$('.bootstrap-tagsinput').find('input').hide();
		$('.tag51nput').tagsinput({
		confirmKeys: [13, 188],
		freeInput: false
		});
		$('.tag51nput').on('itemAdded', function(event) {
				//$(document).find('.typeaheadcontro__').val('');
			// event.item: contains the item
			});
		
			}
		 	if($(document).find('div').hasClass('c0ntr0ll__')){
 			 controllers = $('.c0ntr0ll__').text().split(',');
 			 $('.c0ntr0ll__').text(' ');
		} else{
			 controllers = [];	
		}
		if($('.subm0dulefields').find('input').hasClass('typeaheadcontro__')){
		$('.typeaheadcontro__').typeahead('destroy');
		$('.typeaheadcontro__').typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			name: 'states',
			source: substringMatcher(allactions),
		});
		$('.typeaheadcontro__').on('typeahead:select', function(ev, suggestion) {
				$('.tag51nput').tagsinput('add', suggestion);
		});
		}
		},500);


		$('.typeaheadcontro__').on('keypress',function(event){
				event.stopImmediatePropagation();
				var keyCode = event.keyCode || event.which;
				if (keyCode === 13) { 
				event.preventDefault();
				}
		});



});
