$(document).ready(function(){
	var dpass = randomPassword(12);
	var base = $('.base').val();
	function randomPassword(length) {
    var chars = "abcdefghijklmno^pqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
	}
	$('#cpassword').val(dpass);
	$('.genpass').on('click',function(){
		var newpass = randomPassword(12);
		$('#cpassword').val(newpass);
	});
	$('.addcontact').on('click',function(event){
		event.preventDefault();
		var name = $('.name').val();
		var primaryemail = $('.primaryemail').val();
		var primaryphone = $('.primaryphone').val();
		var cpassword = $('#cpassword').val();
		var skype = $('.skype').val();
		var othercontact = $('.othercontact').val();
		var type = $('.type').val();
		var notes = $('.notes').val();
		var iscurrent = $('.iscurrent').val();
		var href = base + 'contacts/quickadd';
		data_to_post = {
				name : name,
				primaryemail :primaryemail,
				primaryphone : primaryphone,
				cpassword : cpassword,
				skype :skype,
				othercontact :othercontact,
				type :type,
				notes :notes,
				iscurrent :iscurrent
		}
		$.post(href,data_to_post,function(data){
				var data = JSON.parse( data )
				if(data.status = 'success' ){
					console.log(window.parent.$(".newclientname").attr('name'));
					window.parent.$(".newclientname").val(data.name);
					window.parent.$(".newclientid").val(data.id);
					window.parent.$(".newclientname").trigger('change');
				}
		});
	});

});