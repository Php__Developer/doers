$(document).ready(function(){
	var base_url = '<?php echo BASE_URL; ?>';
	var base = $('.base').val();
	$(document).on('click', '.editRecord',function(){
		var id = $(this).closest('td').find('.paymentid').val();
		var amount = $(this).closest('tr').find('td').find('.amount_');
		var tdsamount = $(this).closest('tr').find('td').find('.tdsamount_');
		var payDate = $(this).closest('tr').find('td').find('.payDate_');
		spntinp(amount,'amount_');
		spntinp(tdsamount,'tdsamount_');
		spntinp(payDate,'payDate_');
		var note = $(this).closest('tr').find('td').find('.note_').text();
		$(this).closest('tr').find('.payDate_').datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$(this).closest('tr').find('td').find('.note_').replaceWith('<textarea id="" class="note_" rows="2" cols="20">'+note+'</textarea>');
		$(this).closest('tr').find('td').find('.currency_').hide();
		$(this).closest('tr').find('td').find('.resource_').hide();
		$(this).closest('tr').find('td').find('select').show();
		$(this).addClass('default_hidden');
		$(this).closest('td').find('.editData').removeClass('default_hidden');
	});

	$(document).on('click', '.editData',function(){
	var $this = $(this);
	var amount = $(this).closest('tr').find('td').find('.amount_').val();
	var tdsamount = $(this).closest('tr').find('td').find('.tdsamount_').val();
	var payDate = $(this).closest('tr').find('td').find('.payDate_').val();
	var responsible_id =  $(this).closest('tr').find('.responsibleid').val();
	var currency =  $(this).closest('tr').find('.currencyval').val();
	var note = $(this).closest('tr').find('td').find('.note_').text();
	var id =  $(this).closest('td').find('.paymentid').val();
	var postdata = {					'id': id,
										'payment_date':payDate,
										'responsible_id':responsible_id,
										'tds_amount':tdsamount,
										'amount':amount,
										'note':note,
										'currency' : currency,
					};

		if(!amount.match(/^-?\d*(\.\d+)?$/) || amount==""){
					var error_msg = "Enter a valid amount";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if(tdsamount !="" && !tdsamount.match(/^-?\d*(\.\d+)?$/)){
					var error_msg = "Enter a valid tds amount";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if(responsible_id ==""){
					var error_msg = "Select resource";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				$('.ajaxloaderdiv').show();
				$.post(base+'payments/quickview',postdata,function(data){
					var result = JSON.parse(data);
				if(result.status== 'success'){
					$this.closest('tr').find('td').find('.resource_').text(result.currency);
					$this.addClass('default_hidden');
					$this.closest('td').find('.editData').removeClass('default_hidden');
					$this.closest('tr').find('td').find('.resource_').text(result.responsble.first_name+' '+result.responsble.last_name);
					$('.ajaxloaderdiv').hide();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Payment has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					updateRecord($this);
					get_total();
				}
			});
		});	
	
		$(document).on('click', '.deleterec',function(){
			var id = $(this).closest('td').find('.paymentid').val();
			var $this = $(this);
					 swal({   
	            title: "Are you sure to delete?",   
	            text: "Record Will be Deleted Permanently!", 
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Yes, delete it!",   
	            closeOnConfirm: false 
	        }, function(){ 
	        		  $.ajax({
						    url: base+'payments/quickview',
						    data:{'id':id,'key':'delete'},
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	var data = JSON.parse(data);
							    if(data.status == 'success' ){
							    	$this.closest("tr").remove();
						    		swal("Deleted!", "Record Deleted!", "success"); 
						    		$(document).find('.reloaddocs').trigger('click');
							    }
							    if( data.status == 'failed' ){
						    		swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});
	        	//console.log("idk");
	            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 

	        });



			/*$.post(base+'payments/quickview',{'id':id,'key':'delete'},function(data){
				var result = JSON.parse(data);
				if(result.status== 'success'){
						$('.ajaxloaderdiv').hide();
						$(this).closest("tr").remove();
						$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
						$("#success").html("Payment has been deleted successfully!");
						$('#success').show();
						setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
						get_total();
				}
			});*/

		});

	/*function deleteRecord(id){
						$('.ajaxloaderdiv').show();
	if($.active>0){
		}else{
			var base_url = "<?php echo $this->Url->build('/', true); ?>";
			//alert(base_url);
			$.post(base_url+'payments/quickview',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#amount_'+id).closest("tr").remove();
						$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Payment has been deleted successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					get_total();
				}
			});
		}
	}*/


	function spntinp(obj,classname){
		var data_span = obj.text();
		obj.replaceWith('<input type="text" value="'+data_span+'" style="width:100px;height:30px;" class="form-control '+classname+'">');
	}

	function updateRecord($this){
		//alert("asdfas");
		//var id = $this.closest('td').find('.paymentid').val();
		var amount = $this.closest('tr').find('td').find('.amount_');
		var tdsamount = $this.closest('tr').find('td').find('.tdsamount_');
		var payDate = $this.closest('tr').find('td').find('.payDate_');
		inputToSpan(amount,'amount_');
		inputToSpan(tdsamount,'tdsamount_');
		inputToSpan(payDate,'payDate_');
		$this.find('select').hide();
		$this.addClass('default_hidden');
		$this.closest('td').find('.editRecord').removeClass('default_hidden');
		var note = $this.closest('tr').find('td').find('.note_').text();
		$this.closest('tr').find('td').find('.note_').replaceWith('<span class ="note_">'+note+'</span>');
		$this.closest('tr').find('td').find('.currencyval').hide();
		$this.closest('tr').find('td').find('.currency_').show();
		$this.closest('tr').find('td').find('.responsibleid').hide();
		$this.closest('tr').find('td').find('.resource_').show();
	}
	function inputToSpan(inputID,classname){
		var data_input = inputID.val();
		console.log(data_input);
		inputID.replaceWith('<span class ="'+classname+'">'+data_input+'</span>');
	}


});