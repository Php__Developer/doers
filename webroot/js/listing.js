// This file contains all the common javascript functions.
/**
    * @Method : changeCheckboxStatus
    * @Purpose:This method is used to change checkbox status.
    * @Param: formObj
    * @Return: none
**/
 //var baseurl = base_url; // return base url e.g.    http://localhost
var baseurl = $('.base').val();

function changeCheckboxStatus(formObj) {
	selectAll = formObj.elements['selectAll'];
	var len = "";
	if(formObj.elements['IDs[]']){
	len = formObj.elements['IDs[]'].length;
	}
	if (len == undefined) {
		var elementsLen = formObj.elements.length;
		var len = 0;
		for (i = 0; i < elementsLen; i++)	{
			obj = formObj.elements[i];
			if (obj.name == "IDs[]") {
				len++;
			}
		}

		if (len == 1){
			var e = formObj.elements['IDs[]'];
			if (selectAll.checked) {
				e.checked = true;
			}
			else {
				e.checked = false;
			}
		}
	}
	else if (len > 1) {
		for (var i = 0; i < len; i++) {
			var e = formObj.elements['IDs[]'][i];
			if (selectAll.checked) {
				e.checked = true;
			}
			else {
				e.checked = false;
			}
		}
	}
}
/**
    * @Method : toggleCheck
    * @Purpose:This method is used to toggle checkbox.
    * @Param: formObj
    * @Return: none
**/
function toggleCheck(formObj) {
	var selectAll = formObj.elements['selectAll'];
	objCheckBoxes = null;
	if(document.getElementsByName('IDs[]')){
	var objCheckBoxes = document.getElementsByName('IDs[]');
	}
	var count = 0;
    	for (i = 0; i < objCheckBoxes.length; i++) {
		var e = objCheckBoxes[i];
		if(e.checked) {
		  count++;
		}
	}
	if(objCheckBoxes.length == count){
		selectAll.checked = true;
	}else{
		selectAll.checked = false;
	}
}

/**
    * @Method : atleastOneChecked for Template module
    * @Purpose:This method is used to check that atleast one checkbox is checked.
    * @Param: formObj
    * @Return: none
**/
function atleastOneChecked(message,action) {
	if(action != undefined){
		$("#mainform").removeAttr("action");
		$('#mainform').attr('action', base_url+action);
	}
	if(document.getElementsByName('IDs[]')){
	var objCheckBoxes = document.getElementsByName('IDs[]');
	var count = 0;
    	for (i = 0; i < objCheckBoxes.length; i++) {
		var e = objCheckBoxes[i];
		if(e.checked) {
		  count++;
		}
	}
    	if(count <= 0){ 
		alert("Please select atleast one checkbox.");
		return false;
	}else{
		return confirm(message);
	}
	}
	return true;
}
/**
    * @Method : get confirmation 
    * @Purpose:This method is used to get confirmation before proceeding to sone action
    * @Param: message
    * @Return: boolean
**/
function getConfirmation(message)
	{
		var action = confirm(message);
		if(action == true){
		return true;
		}else{
			return false;
		}
	}
/**
    * @Method : sortDropDownListByText 
    * @Purpose:This method is used to sort drop down list alphabetically.
    * @Param: id
	* @Param: neelam thakur
    * @Return: boolean
**/
	function sortDropDownListByText(id) {
		// Loop for each select element on the page.
		$("#"+id).each(function() {
		// Keep track of the selected option.
		var selectedValue = $(this).val();
		// Sort all the options by text. I could easily sort these by val.
		$(this).html($("option", $(this)).sort(function(a, b) {
		return a.text.toUpperCase() == b.text.toUpperCase() ? 0 : a.text.toUpperCase() < b.text.toUpperCase() ? -1 : 1
		}));
		// Select one option.
		$(this).val(selectedValue);
		});
	}
	
	
	function generate_password(id,inputElement){ 
		$('#'+id).pGenerator({
			'bind': 'click',
			'passwordElement': '#'+inputElement,
			'passwordLength': 16,
			'uppercase': true,
			'lowercase': true,
			'numbers':   true,
			'specialChars': true,
		});
	}