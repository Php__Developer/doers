$(document).ready(function(){
var base = $(document).find('.base').val();
var socketurl = 'http://localhost:8080';
var originagency = $('.emporiginagency').val();
var entity_id = "";
var agenciesnamesonly =[];
var finalagencies = [];
var agenciesarr =[];

var rolesnamesonly =[];
var finalroles = [];
var rolesarr =[];

var singleagencydata = [];
var pushedagencies = [];
var pushedroles = [];
var newrole = [];
editable('.ename','name',base + 'entities/edit');
var currentpage = $('.currentpage').val();
if(currentpage == 'manageentities'){
		entity_id = $('.entityid').val();
		var datatopost = {"key": 'init', entity_id :  entity_id};
		var url = base + 'entities/manage';
		var type ='manage';
		postdata(datatopost,url,type);	
}



$('.col-md-12').on("click", ".manage", function(event) {
          event.preventDefault();
          $('#dialog_modal').html('');
          opendialog($(this).attr('href'),700,700,'no',true);
  });

	
	$('.leadtag51nput').on('beforeItemRemove', function(event) {
		 var teaminfo1 = removeelement1(finalagencies, event.item,'name');
		 var rmfrompushed = removeindexel(pushedagencies,event.item);
		 pushedagencies = rmfrompushed;
		 finalagencies = teaminfo1;
		 var out ='<select class="agenciesselect form-control" name="agency_id">';
		  	$.each(finalagencies, function( k, v ) {
				out+='<option value="'+v.id+'">'+v.name +'</option>';
			});
			out+='</select>';
			$('.selectwrapper').html(' ');
			$('.selectwrapper').append(out);

	})

	$('.leadtag51nput').on('beforeItemAdd', function(event) {
		var result = $.inArray(event.item.toLowerCase(), agenciesnamesonly) != -1;
		if(result == false){
			event.cancel = true;
			trigger_error({ status :'failed' , reason : 'outofarray' },'agency','Agency Name');
			var teaminfo1 = removeelement1(finalagencies, event.item,'name');
			finalagencies = teaminfo1;
			 var rmfrompushed = removeindexel(pushedagencies,event.item);
		 	pushedagencies = rmfrompushed;
		} else {
			var out ='<select class="agenciesselect form-control" name="agency_id">';
		  	$.each(finalagencies, function( k, v ) {
				out+='<option value="'+v.id+'">'+v.name +'</option>';
			});
			out+='</select>';
			$('.selectwrapper').html(' ');
			$('.selectwrapper').append(out);
		}

	});

	$('.restag51nput').on('beforeItemRemove', function(event) {
		 var teaminfo1 = removeelement1(finalroles, event.item,'name');
		 var rmfrompushed = removeindexel(pushedroles,event.item);
		 pushedroles = rmfrompushed;
		 finalroles = teaminfo1;
		 var agency_id = $('.agenciesselect').val();
		 entity_id = $('.entityid').val();
		 var url = base + 'entities/manage';
		  var type ='removerole';
		 var x = {name :  event.item , agency_id : agency_id , key : 'removerole' , entity_id : entity_id };
		 postdata(x,url,type);

	});
	$('.restag51nput').on('beforeItemAdd', function(event) {
		var result = $.inArray(event.item.toLowerCase(), rolesnamesonly) != -1;
		if(result == false){
			event.cancel = true;
  		 	trigger_error({ status :'failed' , reason : 'outofarray' },'responsible','Roles Name');
  		 	 var teaminfo1 = removeelement1(finalroles, event.item,'name');
			 var rmfrompushed = removeindexel(pushedroles,event.item);
			 pushedroles = rmfrompushed;
			 finalroles = teaminfo1;
		}
	});






	$('.selectwrapper').on("change", ".agenciesselect", function(event) {
          event.preventDefault();
          event.stopImmediatePropagation();
          var agency_id = $(this).val();
          //console.log(agency_id);
          var datatopost = {"key": 'agencyroles', entity_id :  entity_id , agency_id : agency_id };
		  var url = base + 'entities/manage';
		  var type ='agencyroles';
		  rolesnamesonly =[];
		  finalroles =[];
          postdata(datatopost,url,type);
          //$('#dialog_modal').html('');

          //opendialog($(this).attr('href'),350,600,'yes',true);
   });


	function removeelement1(arr,name,field){
		var _proto = arr;
		for(var i=0; i < arr.length; i++){
	    	if(arr[i][field] == name){
	      		arr.splice(i,1); 
	    	}
			}
		return arr;
	}

	function removeindexel(arr,field){
		
		var index = arr.indexOf(field);
		if (index > -1) {
		    arr.splice(index, 1);
		}
		return arr;
	}

	function editable(classname,type,posturl){
        $(classname).editable({
         validate: function(value) {
           if($.trim(value) == '') return 'This field is required';
         },
         //mode: 'inline',
        	 url: posturl, 
             ajaxOptions: {
                 dataType: 'json' //assuming json response
             },           
             success: function(data, config) {
                 if(data && data.id) {  //record created, response like {"id": 2}
                     //set pk
                     $(this).editable('option', 'pk', data.id);
                     //remove unsaved class
                     $(this).removeClass('editable-unsaved');
                     //show messages
                     var msg = 'New user created! Now editables submit individually.';
                     $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
                     $('#save-btn').hide(); 
                     $(this).off('save.newuser');                     
                 } else if(data && data.errors){ 
                     //server-side validation error, response like {"errors": {"username": "username already exist"} }
                     config.error.call(this, data.errors);
                 }               
             },
             error: function(errors) {
                 var msg = '';
                 if(errors && errors.responseText) { //ajax error, errors = xhr object
                     msg = errors.responseText;
                 } else { //validation error (client-side or server-side)
                     $.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
                 } 
                 $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
             }
       });

     $('.editcname').on('shown', function(e, editable) {
        var inputval = $(this).closest('.text-muted').attr('title');
        editable.input.$input.val(inputval);
    });

    $('.editcname').on('hidden', function(e, reason) {
        if(reason === 'save' /*|| reason === 'cancel'*/) {
          
          var inputval = $(this).text();
          var txt = (inputval.length > 20) ? inputval.substr(0,20)+ '...' : inputval; 
          $(this).closest('.text-muted').attr('title',inputval);
          $(this).attr('title',inputval);
          $(this).text(txt);
            //auto-open next editable
            //$(this).closest('tr').next().find('.editable').editable('show');
        } 
    });

  }

$(document).on('click', '.cls_host', function(e) {
    	window.parent.$('.ui-dialog-buttonpane button' ).trigger( "click" );
});

$(document).on('click', '.deleteprojectdata', function(event) {
             event.preventDefault();
             $(document).find('.alldata').removeClass('clickedone');
              $(this).closest('.alldata').addClass('clickedone');
    var ref = $(this).attr('href');
        swal({   
            title: "Are you sure to delete?",   
            text: "You will not be able to recover this Data!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
          },function(){ 
              $.ajax({
              url: ref,
              type: "get",
              beforeSend: function() {
              },
              complete: function(){
              },
              success: function(data){
               var data= JSON.parse( data );
             if(data.status == 'success' ){
                  $('.clickedone').hide();
                  swal("Deleted!", "Data has been Deleted!", "success"); 
                 }
               }
            });
         
        });
    });

  function opendialog(page,height,width,closebtnneeded,mdbool) {
          
          $('.ui-dialog-buttonset').show();
          $('#dialog_modal').show();
          $("#dialog_modal").css("overflow", "hidden");
          var $dialog = $('#dialog_modal')
          .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
          .dialog({
            show: { effect: "fade", duration: 500 },
            autoOpen: false,
            dialogClass: 'dialog_fixed fixed-dialog cm_responsive',
            modal: mdbool,
            height: height,
            width:width,
            minWidth: 400,
            minHeight: 400,
            draggable:false,
            buttons: { 
                  "Close": function () {   $(this).dialog("close");
                      $(this).dialog('destroy');
                      $('#dialog_modal').html(''); 
                    } 
                  }
                });
          $dialog.dialog('open');
          $(".ui-dialog-titlebar").hide(); /*to hide the title bar on the dialog */
          if(closebtnneeded == 'no'){
            $('.ui-dialog-buttonpane').hide(); /*to hide bottom button bar on the dialog */  
          }
          
        }

  function postdata(datatopost,url,type){
							$.ajax({
							    url: url,
							    type: "post",
							    data: datatopost,
							    beforeSend: function() {
							    	//console.log("Beforsend")
							    },
							    complete: function(){
							    	//console.log("complete")
							    },
							    success: function(data){
							    	if(typeof data =='object'){
										if(data.response =='success'){
											 if (type == 'manage'){
											 		$.each(data.data, function( k, v ) {
											 			var result = $.inArray((v.Agency.agency_name).toLowerCase(), pushedagencies) != -1;
														if(result == false){
															pushedagencies.push((v.Agency.agency_name).toLowerCase());
															finalagencies.push({id : v.Agency.id , name :  (v.Agency.agency_name).toLowerCase() });
														}
										     	    });
							    					$.each(data.agencies, function( k, v ) {
												       agenciesarr.push(jQuery.parseJSON('{ "label": "' + v + '", "Value": "' + k +'"}'));
												       agenciesnamesonly.push(v.toLowerCase());
										     	    });	
							    					
										     	    $.each(data.roles, function( k, v ) {
												       rolesarr.push(jQuery.parseJSON('{ "label": "' + v + '", "Value": "' + k +'"}'));
												       rolesnamesonly.push(v.toLowerCase());
										     	    });

							    					triggertagsinput('.leadtag51nput');
							    					triggerautocomplete(".leadtagsinputwrapper" ,agenciesarr,'.leadtag51nput', 'agencies');
							    					$('.agenciesselect').trigger("change");
							    				} else if (type == 'agencyroles'){
							    						finalroles =[];
							    						pushedroles =[];
							    						$.each(data.data, function( k, v ) {
							    							var result = $.inArray((v.Role.role).toLowerCase(), pushedroles) != -1;
							    							if(result == false){
							    								pushedroles.push((v.Role.role).toLowerCase());
																finalroles.push({id : v.Role.id , name :  (v.Role.role).toLowerCase() , agency_id : v.Agency.id });	
							    							}
										     	    	});
							    					rolesarr =[];
							    					rolesnamesonly = [];
							    					$.each(data.roles, function( k, v ) {
												       rolesarr.push(jQuery.parseJSON('{ "label": "' + v.toLowerCase() + '", "Value": "' + k +'"}'));
												       rolesnamesonly.push(v.toLowerCase());
										     	    });

										     	    $('.restag51nput').tagsinput('destroy');
							    					triggertagsinput('.restag51nput');
							    					triggerautocomplete(".restagsinputwrapper" ,rolesarr,'.restag51nput', 'responsibles');
									    		} else if (type == 'addnewrole'){
									    			tiggermsgc('success','Role Added!');
									    		}  else if (type == 'removerole'){
									    			tiggermsgc('success','Role Removed!');
									    		}
										} 						  
									} else {
											if(JSON.parse(data).status =='success'){
							    		if(type == 'addjob'){
							    			tiggermsgc('success','Project Created Successfully!');	
							    			dp = { project_id : JSON.parse(data).id ,base_url : base};
							    			postdata(dp,socketurl+'/reloadstatsforone','savestatsforproject');
							    			//window.parent.location.href = base + 'projects/projectdetails/'+ JSON.parse(data).id;	
							    		}  
							    		}	
									}
							    
							    	
								},
							    error: function(result){
							        //console.log(result);
							    }
		  				});
			} 

			 function triggerautocomplete(classname ,sourcearr,tagsinputclass, finaldataindex){
			  	   $(classname).find('.bootstrap-tagsinput').find('input').autocomplete({
				        source: function(request, response) {
				          var results = $.ui.autocomplete.filter(sourcearr, request.term);
				          response(results.slice(0, 5));
				        },
				        autoFocus: true,
				        select: function (event, ui) {
				          var selected =ui.item.Value;
				         if(finaldataindex == 'responsibles' || finaldataindex == 'consultants' || finaldataindex == 'informers' || finaldataindex == 'agencies') {
							if(finaldataindex == 'responsibles'){
								var agency_id = $('.agenciesselect').val();
								var result = $.inArray((ui.item.label).toLowerCase(), pushedroles) != -1;
								if(result == false){
									var x = {role_id : selected , name :  ui.item.label , agency_id : agency_id , key : 'addnewrole' , entity_id : entity_id };
									newrole.push(x)
									finalroles.push(x);
									pushedroles.push((ui.item.label).toLowerCase());
									var url = base + 'entities/manage';
	  								var type ='addnewrole';
	  								console.log(x);
									postdata(x,url,type);
								}
								$(tagsinputclass).tagsinput('add', ui.item.label);
					         } else if(finaldataindex == 'consultants'){
					          	consultants.push({id : selected, name : ui.item.label});
					         } else if(finaldataindex == 'accountables'){
					          	accountables.push({id : selected, name : ui.item.label});
					         } else if(finaldataindex == 'informers'){
					          	informers.push({id : selected, name : ui.item.label});
					         } else if(finaldataindex == 'agencies'){
					         	var x = {id : parseInt(selected), name : ui.item.label};
								var result = $.inArray((ui.item.label).toLowerCase(), pushedagencies) != -1;
								if(result == false){
									pushedagencies.push((ui.item.label).toLowerCase());
									finalagencies.push(x);
								}
					          	////console.log(finalagencies);
					         } 
					         $(tagsinputclass).tagsinput('add', ui.item.label);
							//$(classname).find('.bootstrap-tagsinput').find('input').val(' ');
				         } else {
				         	 $(tagsinputclass).tagsinput('add', ui.item.label);
					          if(finaldataindex == 'leadinfo'){
					          	leadinfo.push({id : selected, name : ui.item.label});
					          	var datatopost = {'leadinfo' : leadinfo , projectid :  projectid };	
					          	var url = base +'projects/projectlead';
					          	 postdata(datatopost,url,'editlead');
					          } 
					          //finaldata[finaldataindex] = clientinfo;
					          //$(classname).find('.bootstrap-tagsinput').find('input').hide();	
					          $(classname).find('.bootstrap-tagsinput').find('input').val(' ');
				         }
				          ////console.log(finaldata);
				        }
				      });

			  }

			  function triggertagsinput(classname){
				$(classname).tagsinput({
					confirmKeys: [13, 188],
					freeInput: false
				});
				if(classname =='.restag51nput'){
					$('.restag51nput').tagsinput('removeAll');
					$.each(finalroles, function( k, v ) {
						$('.restag51nput').tagsinput('add', v.name);
							
				    });
					$('.restagsinputwrapper').removeClass('default_hidden');
					//$('.restag51nput').tagsinput('add', admin.label);
				} else if (classname == '.leadtag51nput'){

							$.each(finalagencies, function( k, v ) {
								$('.leadtag51nput').tagsinput('add', v.name);
				     	    });
					       $('.loaderdiv').addClass('default_hidden');
					       $('.leadtagsinputwrapper').removeClass('default_hidden');
					}
			  }

			    function trigger_error(result,field,display_name){
			  		$(document).find('.'+ field).closest('.form-group').find('.errors').html('');
			  		if(result['reason'] == 'empty'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append(display_name + ' Can not be Empty!');
			  			var txt = display_name + ' Can not be Empty!';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		} else if(result['reason'] == 'outofarray'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append('Please Select '+display_name +' from suggestions');
			  			var txt = 'Please Select '+display_name +' from suggestions';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		} else if(result['reason'] == 'notnumberic'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append(display_name +' should be Numeric');
			  			var txt = display_name +' should be Numeric';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ display_name +' should be Numeric</div>');
			  		} else if (result['reason'] == 'cannotremoveadmin'){
			  			var txt = display_name +' can not be removed';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		}
			  		$(document).find('.'+ field).closest('.form-group').find('.bootstrap-tagsinput').find('input').show();
			  }

			      function tiggermsgc(type,msg){
			        if(type=='info'){
			          $(document).find('.succesfontwrapper').find('.succesfont').html(' ');
			          $(document).find('.succesfontwrapper').find('.succesfont').append(msg);
			          $(document).find('.succesfontwrapper').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.succesfontwrapper').removeClass('default_visible');
			          }, 3000);
			        } else if(type == 'success'){
			          $(document).find('.successwrapperdv').find('.successmsg').html(' ');
			          $(document).find('.successwrapperdv').find('.successmsg').append(msg);
			          $(document).find('.successwrapperdv').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.successwrapperdv').removeClass('default_visible');
			          }, 3000);
			        } else if(type == 'error') {
			          $(document).find('.succesfontwrapper').find('.succesfont').html(' ');
			          $(document).find('.succesfontwrapper').find('.succesfont').append(msg);
			          $(document).find('.succesfontwrapper').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.succesfontwrapper').removeClass('default_visible');
			          }, 3000);
			        }
			        
			      }

});