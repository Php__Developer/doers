$(document).ready(function(){

	var base = $('.base').val();

	$(document).on('click', '.editRecord',function(){
		$this = $(this);
		var projectid = $('#project_id').val();
		console.log(projectid);
		var milestoneid = $this.closest('td').find('.milestoneid').val();

		var week = $(this).closest('tr').find('td').find('.week_');
		spntinp(week,'week_');
		var currencyoptions = [
								'USD' ,
								'AUD' ,
								'CAD' ,
								'INR' 
						 ];
		 var out ='';
		  out+='<select class="currency_">';
		 $.each(currencyoptions, function( index, value ) {
  			out+='<option value="'+value +'">'+value+'</option>';
		});
		  out+='</select>';
		 console.log(out);
		 $this.closest('tr').find('td').find('.currency_').replaceWith(out);
		 var options= '';
		 $("#user_id > option").each(function() {
		 if($(this).val()!= $(this).closest('tr').find('td').find(".user_id__").text()){
		       options=options+'<option value="'+$(this).val() +'">'+$(this).text()+'</option>';
            }else{
             options=options+'<option value="'+$(this).val()+'" selected >'+$(this).text()+'</option>';
		 }
		});
		
		$this.closest('tr').find('td').find('.user_edit_').replaceWith('<select class="form-control user_edit_">'+options+'</select>');

		var amount = $(this).closest('tr').find('td').find('.amount_');
		//var tdsamount = $(this).closest('tr').find('td').find('.tdsamount_');
		var payDate = $(this).closest('tr').find('td').find('.milestonedate_');
		spntinp(amount,'amount_');
		//spntinp(tdsamount,'tdsamount_');
		spntinp(payDate,'milestonedate_');
		var note = $(this).closest('tr').find('td').find('.notes_').text();
		$(this).closest('tr').find('.milestonedate_').datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$(this).closest('tr').find('td').find('.notes_').replaceWith('<textarea id="" class="notes_" rows="2" cols="20">'+note+'</textarea>');
		//$(this).closest('tr').find('td').find('.currency_').hide();
		//$(this).closest('tr').find('td').find('.resource_').hide();
		//$(this).closest('tr').find('td').find('select').show();
		$(this).addClass('default_hidden');
		$(this).closest('td').find('.editData').removeClass('default_hidden');
	});

	$(document).on('click', '.editData',function(){
	var $this = $(this);
	var amount = $(this).closest('tr').find('td').find('.amount_').val();
	var milestonedate = $(this).closest('tr').find('td').find('.milestonedate_').val();
	var milestoneid = $this.closest('td').find('.milestoneid').val();
	var user_id =  $(this).closest('tr').find('.user_edit_').val();
	var currency =  $(this).closest('tr').find('.currency_').val();
	var note = $(this).closest('tr').find('td').find('.notes_').val();
	var postdata = {					'id':milestoneid,
										'user_id':user_id,
										'amount':amount,
										'notes':note,
										'currency' : currency,
										'milestonedate' :milestonedate
					};

		if(!amount.match(/^-?\d*(\.\d+)?$/) || amount==""){
					var error_msg = "Enter a valid amount";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if(user_id ==""){
					var error_msg = "Select resource";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				$('.ajaxloaderdiv').show();
				$.post(base+'milestones/edit',postdata,function(data){
					var result = JSON.parse(data);
				if(result.status== 'success'){
					$this.addClass('default_hidden');
					$this.closest('td').find('.editData').removeClass('default_hidden');
					$this.closest('tr').find('td').find('.user_edit_').replaceWith(result.responsble.first_name+' '+result.responsble.last_name);
					$('.ajaxloaderdiv').hide();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Milestone  has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					updateRecord($this);
					//get_total();
				}
			});
		});	

		$(document).on('click', '.addmilestone',function(){
		var $this = $(this);
		/*var amount = $(this).closest('tr').find('td').find('.amount_').val();
		var milestonedate = $(this).closest('tr').find('td').find('.milestonedate_').val();
		var milestoneid = $this.closest('td').find('.milestoneid').val();
		var user_id =  $(this).closest('tr').find('.user_edit_').val();
		var currency =  $(this).closest('tr').find('.currency_').val();
		var note = $(this).closest('tr').find('td').find('.notes_').val();*/

		var milestonedate =$('#milestonedate').val();
		var currency = $('#currency').val();
		var amount = $('#amount').val();
		var notes = $('#notes').val();
		var project_id = $('#project_id').val();
		var user_id = $('#user_id').val();


		var postdata = {'user_id':user_id,'milestonedate':milestonedate,'currency':currency,'amount':amount,'notes':notes,'project_id': project_id} ;
 					if(milestonedate=="")
					{
						document.getElementById("valid_date").style.display = "block";
									
					 }
					 else{
						document.getElementById("valid_date").style.display = "none";
						 }		 
					 // if(amount=="")
					  if(!amount.match(/^-?\d*(\.\d+)?$/) || amount=="")
					 {
						document.getElementById("valid_amount").style.display = "block";
						return true;
						
					 }
					 else		  
					 {
						document.getElementById("valid_amount").style.display = "none";
						
					 }
					 if(notes=="")
					 {
						document.getElementById("valid_notes").style.display = "block";			
					 }else		  
					 {
						document.getElementById("valid_notes").style.display = "none";
						
					 }
					$('.ajaxloaderdiv').show();
					$.post(base+'milestones/add',postdata,function(data){
						var result = JSON.parse(data);
					if(result.status== 'success'){
						console.log("asdf");
						$('.resultset').append('<tr><td class="tablesaw-cell-persist"><span class="week_">'+result.milestonedata.project.project_name+'</span></td><td class="tablesaw-cell-persist"><span class="user_edit_">'+result.milestonedata.user.first_name+'<span class="user_id__" style="display:none;">154</span></span></td><td><span class="currency_">'+result.milestonedata.currency+'</span></td><td><span class="amount_">'+result.milestonedata.amount+'</span></td><td><span class="notes_">'+result.milestonedata.notes+'</span></td><td style="text-align:center"><span class="text-danger" id="show_img_">deactivated</span></td><td><input name="milestoneid" class="milestoneid" value="'+result.milestonedata.id+'" type="hidden"><a href="javascript:void(0)" class="info-tooltip editRecord" title="Edit" id="edit_" onclick=""><i class="fa fa-gear facontrol"></i></a><a href="javascript:void(0)" class="info-tooltip editData default_hidden" title="Edit" id="edit_" onclick=""><i class="fa fa-check-square facontrol"></i></a><a href="javascript:void(0)" class="info-tooltip deleterec" title="Delete" id="del_" onclick=""><i class="fa fa-times facontrol"></i></a><a href="javascript:void(0)" class="info-tooltip verifyrec" title="Click to Verify" id="del_" data-key="verify" onclick=""><i class="fa fa-thumbs-down facontrol"></i></a></td></tr>');
						/*$this.addClass('default_hidden');
						$this.closest('td').find('.editData').removeClass('default_hidden');
						$this.closest('tr').find('td').find('.user_edit_').replaceWith(result.responsble.first_name+' '+result.responsble.last_name);
						$('.ajaxloaderdiv').hide();
						
						updateRecord($this);*/
						$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
						$("#success").html("Milestone  has been Added successfully!");
						$('#success').show();
						setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
						//get_total();
					}
				});
			});

		$(document).on('click', '.verifyrec',function(){
			var $this = $(this);
			var milestoneid = $(this).closest('td').find('.milestoneid').val();
			var action = $(this).attr('data-key');
			$.post(base+'milestones/verifymilestone',{'id':milestoneid,'key': action},function(data){
				var result = JSON.parse(data);
				if(result.status == 'success'){
					$this.attr('data-key',result.key);
					$this.find('i').toggleClass("fa-thumbs-down fa-thumbs-up");
					$('.ajaxloaderdiv').hide();
					var performedaction = (action == 'verify') ? 'Verified' : 'Unverified';
					var statustext = (action == 'verify') ? 'Activated' : 'Deactivated';
					$this.closest('tr').find('td').find('.show_img_').text(statustext);
					$("#success").html("Milestone has been "+ performedaction +" successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					
					}
			});	

		});

		
		
		$(document).on('click', '.deleterec',function(){
			var $this = $(this);
			var milestoneid = $(this).closest('td').find('.milestoneid').val();
			var action = 'delete';
					 swal({   
	            title: "Are you sure to delete?",   
	            text: "Record Will be Deleted Permanently!", 
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Yes, delete it!",   
	            closeOnConfirm: false 
	        }, function(){ 
	        		  $.ajax({
						    url: base+'milestones/delete',
						    data:{'id':milestoneid,'key':'delete'},
						    type: "post",
						    beforeSend: function() {
						    },
						    complete: function(){
						    },
						    success: function(data){
						    	var data = JSON.parse(data);
							    if(data.status == 'success' ){
							    	$this.closest("tr").remove();
						    		swal("Deleted!", "Record Deleted!", "success"); 
						    		//$(document).find('.reloaddocs').trigger('click');
							    }
							    if( data.status == 'failed' ){
						    		swal("Sorry!",JSON.parse( data ).reason , "error"); 
							    }
						    }
	  					});

	        });
		});

	

	function spntinp(obj,classname){
		var data_span = obj.text();
		obj.replaceWith('<input type="text" value="'+data_span+'" style="width:100px;height:30px;" class="form-control '+classname+'">');
	}

	function updateRecord($this){
		var amount = $this.closest('tr').find('td').find('.amount_');
		var milestonedate = $this.closest('tr').find('td').find('.milestonedate_');
		var currency =  $this.closest('tr').find('td').find('.currency_');
		var week = $this.closest('tr').find('td').find('.week_');
		inputToSpan(week,'week_');
		inputToSpan(amount,'amount_');
		inputToSpan(milestonedate,'milestonedate_');
		inputToSpan(currency,'currency_');
		$this.find('select').hide();
		$this.addClass('default_hidden');
		$this.closest('td').find('.editRecord').removeClass('default_hidden');
		var note = $this.closest('tr').find('td').find('.notes_').val();
		$this.closest('tr').find('td').find('.notes_').replaceWith('<span class ="notes_">'+note+'</span>');
	}
	function inputToSpan(inputID,classname){
		var data_input = inputID.val();
		console.log(data_input);
		inputID.replaceWith('<span class ="'+classname+'">'+data_input+'</span>');
	}


});