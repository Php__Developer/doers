
$(document).ready(function(){ 

    /*
   @WHY => on account page send resent mail activation link 
   @WHEN  => 11 jan 2017
   @WHO => Gurpreet kaur
   */

   
$(document).on('click', '.closed', function(event) {
    $(this).closest('.myadmin-alert').hide();
});
  $(document).on('click', '.agency_find', function(event) {
     event.preventDefault();
    var agency_name= $(".agency_name").val();
    var t = $('#token').val();
      if(agency_name.length==0){
      $(".alerttop2").find("h4").html("");
      $(".alerttop2").find("h4").append("Please Enter Your Agency .");
       $(".alerttop2").show();
          return false;
        }
       
    $.ajax({
      url: 'save',
      type: "post",
      dataType:'json',
      data: {'_token': t ,'agency_name' : agency_name },
      beforeSend: function() {
        $(".preloader").show();
      },
      complete: function(){
        $(".preloader").hide();
      },
      success: function(data){
        
        if(data == 'token_missing'){
          $("#openModals").html('');
          $("#openModals").append('<div id ="status">ERROR!</div><p class="model_success">Unexpected Error. Please try again!</p><div class="ok_wrapper"><div class="ok_wrapper"><button class="gotoIndex gotIt">Ok</button></div>');
          $.fancybox({
            href:"#openModals",
            closeClick  : true,
            'autoDimensions':false,
            'autoSize':false,
            modal: true,
            height: 150,
            width: 300,
            helpers   : { 
                            overlay : {closeClick: false}  // prevents closing when clicking OUTSIDE fancybox 
                          },
                          afterClose  : function() {
                          }
                        });
        }else{
           if(data[0]=="success"){
             var name=data[1];
             if(name == "msoftware"){
              window.location.href = "http://doers.online/msoftware/";  
             }else if(name =="vlogic"){
              window.location.href = "http://doers.online/vlogic/";  
             }else if(name =="vlogiclabs"){
              window.location.href = "http://doers.online/vlogiclabs/";  
              }
           }else if(data[0]=="errors"){
               $(".alerttop2").find("h4").html("");
               $(".alerttop2").find("h4").append(''+data[1].agency_name+'');
               $(".alerttop2").show();
           }else if(data[0]=="nomatch"){
               $(".alerttop2").find("h4").html("");
               $(".alerttop2").find("h4").append(data[1]);
               $(".alerttop2").show();
           }
        }
      } 
    });  
  });



 });  //end of main function ----------------------------------------------------------------- braces 

