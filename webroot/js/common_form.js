$(document).ready(function(){
	
	var base = $('.base').val();
	var token = $('._token').val();
	$('.common-form').on('click','.submit-btn',function(event){
		event.preventDefault();
		var url = $('.common-form').attr('action');
		var type = $('.common-form').data('type');

		fd = new FormData();
		//fd.append('thumbnail',  moviethumbnail); 
		$( ".common-form input" ).each(function() {
			var name = $(this).attr('name');
			var input_type = $(this).attr('type');
			console.log(input_type);
			//console.log(name);
			var input_val = $(this).val();
			fd.append(name,  input_val); 
		});

		$( ".common-form textarea" ).each(function() {
			var name = $(this).attr('name');
			var input_type = $(this).attr('type');
			//console.log(input_type);
			//console.log(name);
			var input_val = $(this).val();
			fd.append(name,  input_val); 	
		});

		 formajax(fd,url, type);
	});	

	
	function formajax(datatopost,url,type){
				$.ajax({
							    url: url,
							    type: "post",
							    data: datatopost,
							    processData: false,
				        		contentType: false,
							    beforeSend: function() {
							    	//console.log("Beforsend")
							    	$('.alert-warning').remove();
							    },
							    complete: function(){
							    	console.log("complete")
							    },
							    success: function(data){
							    	if(data.status == 'success'){
							    		if(type == 'contactus'){
							    			window.location.href = base+'contacts/contactslist';
							    		}
							    	} else if(data.status == 'failed'){
							    		if(type == 'delel'){
							    			alert(data.reason);
							    		} else if (type == 'contactus'){
							    			console.log("asdf");
							    			if(data.reason == 'validation'){
							    				$.each(data.errors, function( index, value ) {

							    					console.log(index)	;
							    					//$(document).find('.'+index).closest('.form-group').find('.errors').html(value[0]);

							    					$(document).find("input[name='"+ index +"']").closest('.form-group').find('.errors').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+value._empty+'</div>');
							    					$(document).find("textarea[name='"+ index +"']").closest('.form-group').find('.errors').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+value._empty+'</div>');

												  //alert( index + ": " + value );
												});
							    			}
							    		}
							    	}
							    }
			});

	}	

	/*$('.data-dismiss="alert').on('click',function(){
		$(this)	.closest('.alert').remove();
	});*/

});