$(document).ready(function(){

var base = $(document).find('.base').val();
var page_name = $(document).find('.pe').val();

if(page_name == 'applist'){
   var searchurl='appraisalslist';
   var statusurl= base +'appraisals/appstatus';
   var sort='Appraisals.appraisal_date&direction=desc';
   var linkhref=base+'appraisals/appraisalslist';
}else if(page_name == 'emplist'){
   var searchurl='employeelist';
   var statusurl= base +'employees/changeStatus';
   var sort='';
   var linkhref=base+'employees/employeelist';
}
         function convert(str) {
                var date = new Date(str),
                 mnth = ("0" + (date.getMonth()+1)).slice(-2),
               day  = date.getDate() - 1;
              return [  mnth,day,date.getFullYear() ].join("/");
          }

       /*
      @WHY => Common function for check input box validation  in jquery 
      @WHEN  => 21-08-2016
      @WHO => Gurpreet kaur
      */

      function check_html (str) {   
        var regEx = /<|>/g;
        var testString = ''+str+'';
        var search_input =(testString.replace(regEx,""));
        return search_input;
      }

//alert(checkNumericValue(2));
// check negative value 
function checkNumericValue(num)
{
var objRegExp  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
return objRegExp.test(num);
}


function dating(str) {
                    var date = new Date(str),
                     mnth = ("0" + (date.getMonth()+1)).slice(-2),
                       day  = date.getDate() - 1;
                      
                      return [  date.getFullYear(),mnth,day].join("");
                    }

  var base=$(document).find('.base').val();
      //console.log(base);

      /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

$(document).on('click', '.firstLISt li', function(event) {
//$('.alldata').on('click', '.firstLISt li', function(event) {
 event.preventDefault();
  var active_class= $(this).find("a span i").attr('class');
  if(active_class=="ti-home"){
          $(this).closest(".nav-tabs").find(".profile").removeClass('active');
          $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
          $(this).closest(".nav-tabs").find(".home").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").addClass('active');
   }else if(active_class=="ti-user"){
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
      $(this).closest('.alldata').find('.tab-content').find(".iprofile").addClass('active');
   }else if(active_class=="ti-email"){
     $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").addClass('active');
         $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".imessages").addClass('active');
   }else if(active_class=="ti-settings"){
      $(this).closest(".nav-tabs").find(".settings").addClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".isettings").addClass('active');
   }


   });


// for delete icon
      /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

$(document).on('click', '.deleteprojectdata', function(event) {
             event.preventDefault();
             $(document).find('.alldata').removeClass('clickedone');
              $(this).closest('.alldata').addClass('clickedone');
    var ref = $(this).attr('href');
        swal({   
            title: "Are you sure to delete?",   
            text: "You will not be able to recover this Data!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
          },function(){ 
              $.ajax({
              url: ref,
              type: "get",
              beforeSend: function() {
              },
              complete: function(){
              },
              success: function(data){
               var data= JSON.parse( data );
             if(data.status == 'success' ){
                  $('.clickedone').hide();
                  swal("Deleted!", "Data has been Deleted!", "success"); 
                 }
               }
            });
         
        });
    });


/*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */


   $(document).on('click', '.drp_list li', function(e) {
   e.stopPropagation();
   var clicked_icon=$(this).find("a").attr("class");
  if(clicked_icon==1){
   $(document).find(".parent_data").html(
    $(".alldata").sort(function (a, b) {
    var a= $(a).find(".1").text();
    var b= $(b).find(".1").text();
    return a > b;
    }) // End Sort
   ); 
   }else if(clicked_icon==2){
     $(document).find(".parent_data").html(
        $(".alldata").sort(function (a, b) {
        var a= $(a).find(".2").text();
        var b= $(b).find(".2").text();
        return a > b;
        }) // End Sort
       );         
   }else if(clicked_icon==3){
            $(document).find(".parent_data").html(
        $(".alldata").sort(function (a, b) {
        var a= $(a).find(".3").text();
        var b= $(b).find(".3").text();
        return a > b;
        }) // End Sort
       ); 
     }else if(clicked_icon==4){
        if(page_name=="applist"){
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".appraisal_date").val();
            var b= $(b).find(".appraisal_date").val();
            return a > b;
            }) ); 
         }else if(page_name=="emplist"){
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".phoneicons").val();
            var b= $(b).find(".phoneicons").val();
            return a > b;
            }) ); 

         }else if(page_name=="evalist"){
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".4").text();
            var b= $(b).find(".4").text();
            return a > b;
            }) ); 

         }else{
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".4").val();
            var b= $(b).find(".4").val();
            return a > b;
            }) ); 
         }
        }else if(clicked_icon==5){
          if(page_name=="applist"){
             $(document).find(".parent_data").html(
              $(".alldata").sort(function (a, b) {
              var a= $(a).find(".next_review_date").val();
              var b= $(b).find(".next_review_date").val();
              return a > b;
              }));
           }else if(page_name=="emplist"){
              $(document).find(".parent_data").html(
              $(".alldata").sort(function (a, b) {
              var a= $(a).find(".salaryicons").val();
              var b= $(b).find(".salaryicons").val();
              return a > b;
              }) ); 
          }else if(page_name=="evalist"){
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".monthicons").val();
            var b= $(b).find(".monthicons").val();
            return a > b;
            }) ); 

         }else{
            $(document).find(".parent_data").html(
              $(".alldata").sort(function (a, b) {
              var a= $(a).find(".5").val();
              var b= $(b).find(".5").val();
              //alert(b);
              return a < b;
              }));
          }
      }  
  });




function validation(searchvals){
      if(searchvals.length==0){
          $(".alerttop2").find(".error_text").html("");
          $(".alerttop2").find(".error_text").append('Please Fill Out the  Valid Data');
          $(".alerttop2").show();
          var error="error";
           return error;
         }
          if ($.isNumeric(searchvals)){
             $(".alerttop2").find(".error_text").html("");
          $(".alerttop2").find(".error_text").append('Numeric Data Not Allowed ');
          $(".alerttop2").show();
            var error="error";
           return error;
           }
           if ( $.trim( searchvals ) == '' ){
            $(".alerttop2").find(".error_text").html("");
          $(".alerttop2").find(".error_text").append('Search Field is Required ');
          $(".alerttop2").show();
         var error="error";
           return error;
           }
           if(searchvals.length>50){
            $(".alerttop2").find(".error_text").html("");
          $(".alerttop2").find(".error_text").append('Data Should Not be Greater Than 30 words');
          $(".alerttop2").show();
         var error="error";
           return error;
          }
}
   
     /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

 $(".searchfaicon").on("click", function(event){
      event.preventDefault();
       var lastpage= $(document).find('.paginate').val();
         var searchvals = check_html($('.searchval').val()); //search the input value
          if(page_name=="evalist"){
            var selections = $(document).find('.first').find('.pull-left').text(); //select the column name
          }else{
            var selections = $('.pull-left').text(); //select the column name
          }
            var year=$(document).find('.2nd').find('.pull-left').text();
             var checkvalid= validation(searchvals);
             if(checkvalid=="error"){
                  return false;
                }
                if(page_name=="applist"){
                   if(selections=="Search By Users"){
                      var fieldName='Users.first_name'; 
                     }
                  var postdata= { User:{'fieldName':fieldName,'value1':searchvals} };
                 }else if(page_name=="emplist"){
                       if(selections=="Search By First Name"){
                        var fieldName='Users.first_name'; 
                        }else if(selections=="Search By Employee Code"){
                         var fieldName='Users.employee_code';
                        }else if(selections=="Search By Email"){
                          var fieldName='Users.username';
                        }else if(selections=="Search By Phone"){
                          var fieldName='Users.phone';
                        }else if(selections=="Search By Current Salary"){
                          var fieldName='Appraisals.appraised_amount';
                        }
                  var postdata= { User:{'fieldName':fieldName,'value1':searchvals} };
                  }else if(page_name=="evalist"){
                               if(selections=="January"){
                                  var fieldName="01";
                               }else if(selections=="February"){
                                  var fieldName="02";
                               }else if(selections=="March"){
                                  var fieldName="03";
                               }else if(selections=="April"){
                                  var fieldName="04";
                               }else if(selections=="May"){
                                  var fieldName="05";
                                }else if(selections=="June"){
                                  var fieldName="06";
                               }else if(selections=="July"){
                                  var fieldName="07";
                               }else if(selections=="August"){
                                  var fieldName="08";
                               }else if(selections=="September"){
                                  var fieldName="09";
                                }else if(selections=="October"){
                                  var fieldName="10";
                               }else if(selections=="November"){
                                  var fieldName="11";
                               }else if(selections=="December"){
                                  var fieldName="12";
                               }

                  var postdata= { 'user_id':searchvals,'month':fieldName ,'year': year } ;
                  }

         $.ajax({           
         url : searchurl,
         type: "post",
          dataType: 'json',
         data: postdata,
        // data:{postsearchdata},
         beforeSend: function() {
           //$(".CategoryErrors").html('');
          $('.ajax').hide(); 
          $('.preloader').show();
         },
         complete: function(){
           $('.preloader').hide();
           $('.ajax').show(); 
         }
                }).done(function (data) {
              // var  data = jQuery.parseJSON(data);
                var complete=aftersucess(data);
              if(data.resultData==null){
                $('.ajax').html('');
                $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
              }
        }).fail(function (response) {
          var error=response.responseText;
         if(error){
                $('.ajax').html('');
              $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
           }
     });
 });
  

function aftersucess(data){
                  if(data.resultData != ""){
                    $('.ajax').html('');
                    var out = '';
                    out+='<div class="parent_data">';
                    $.each(data.resultData ,function(key,value ) {
                    if(value.status==0){
                    var status='<input class="js-switch pull-right textChekb0x" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }else{
                      var status='<input class="js-switch pull-right textChekb0x" checked="" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }
                    if(page_name=="applist"){
                       if(value.Supervisor  === null){
                         var a="";
                         }else{
                          var a=value.Supervisor.first_name+" "+value.Supervisor.last_name.substring(0,13);
                         }
                         if(value.Users  === null){
                         var username="";
                         }else{
                         var username=value.Users.first_name+" "+value.Users.last_name.substring(0,15);
                         }
                      var icons='<ul class="list-group"><li class="list-group-item list-group-item-danger 1"><b>Name :</b>'+username+'</li> <li class="list-group-item list-group-item-success 2"><b>Supervisor :</b> '+a+'</li><li class="list-group-item list-group-item-info 3"><b>Appraised amount :</b>'+value.appraised_amount+'</li><li class="list-group-item list-group-item-warning 4"><b>Appraisal date :</b>'+convert(value.appraisal_date)+'</li><li class="list-group-item list-group-item-danger 5"><b>Next review:</b> '+convert(value.next_review_date)+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"> <ul class="nav nav-tabs list-group-item-info " role="tablist"> <li><a id="" class="btn default btn-outline popup_window" href="'+base+'appraisals/details/'+value.id+'" title="Appraisal Detail"><i class="fa fa-list"></i></a></li><li><a href="'+base+'appraisals/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li><a href="'+base+'appraisals/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li>'+status+'</li> </ul><input id="appraisal_date" class="appraisal_date" value="'+dating(value.appraisal_date)+'"><input id="next_review_date" class="next_review_date" value="'+dating(value.next_review_date)+'"> </ul>';
                  }else if(page_name=="emplist"){
                     if(value.salary  === null){
                         var s="";
                         }else{
                          var s=value.salary;
                         }

                      var icons='<ul class="list-group "><li class="list-group-item list-group-item-danger 1"><b>Employee Code :</b> '+value.employee_code.substring(0,15)+' </li><li class="list-group-item list-group-item-success 2"><b>First Name :</b> '+value.first_name.substring(0,15)+'</li><li class="list-group-item list-group-item-info 3"><b>Email :</b>  <a href="mailto:'+value.username+'">'+value.username.substring(0,15)+'</a></li><li class="list-group-item list-group-item-warning 4"><b>Phone :</b> '+value.phone+'</li><li class="list-group-item list-group-item-danger 5"><b>Salary:</b> '+s+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"><input class="phoneicons" value="'+value.phone+'"  type="hidden"><input class="salaryicons" value="'+s+'"  type="hidden"><ul class="nav nav-tabs list-group-item-info " role="tablist"><li> <a href="'+base+'employees/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li> <a href="'+base+'employees/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li> <a href="'+base+'employees/credential_detail/'+value.id+'" class="btn default btn-outline popup_window" title="Credentials detail" target="_blank"><i class="fa icon-key"></i></a> </li> <li>'+status+'</li></ul></ul>';
                    }
                    var icons=icons;
                    out += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata"><div class="white-box"><ul class="nav nav-tabs firstLISt" role="tablist"><li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li><li role="presentation" class=""><a href="#iprofile" style="display:none;" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li><li role="presentation" class=""><a href="#imessages" style="display:none;" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li><li role="presentation" class=""><a href="#isettings" style="display:none;" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li></ul><div class="tab-content"> <div role="tabpanel" class="tab-pane ihome active"><ul class="list-group ">'+icons+'</ul> </div> <div role="tabpanel" class="tab-pane iprofile"> <div class="col-md-6"><h3>Lets check profile</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div> <div class="clearfix"></div> </div><div role="tabpanel" class="tab-pane imessages"> <div class="col-md-6"><h3>Come on you have a lot message</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div><div role="tabpanel" class="tab-pane isettings"><div class="col-md-6"><h3>Just do Settings</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div></div></div></div>';
                    });
                    setTimeout(function(){ 
                      $('.js-switch').each(function() {
                          new Switchery($(this)[0], $(this).data());
                      });
                    }, 200);
                    
                    out +='</div>';
                    $('.ajax').append(out+'<div>')+'';
                    var counter=1;
                    if(data.paginatecount==1){
                    $(document).find('.counteringdata').html('');
                    $(document).find('.counteringdata').append(1+"of"+data.paginatecount);
                    }else{
                    if(data.page==""){
                    data.page=1;
                    }else{
                    data.page=data.page;
                    }
                    $(document).find('.counteringdata').html('');
                    $(document).find('.counteringdata').append(data.page+"of"+data.paginatecount);
                    }
                    $(document).find('.paginate').val('');
                    $(document).find('.paginate').val(data.paginatecount);
                    if(data.paginatecount==1){
                    $(document).find('.paginateproject').html('');
                    $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
                    }else{
                    var nextpage=+counter+1;
                    $(document).find('.paginateproject').html('');
                    $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page='+nextpage+'&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul>');
                    }
               }else{
                  $('.ajax').html('');
                 $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                 $(document).find('.counteringdata').html('');
                 $(document).find('.counteringdata').append(" 0 of 0");
                 $(document).find('.paginateproject').html('');
                 $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
               }



}


     /*
     @WHY => use for the pagination 
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

var counter=2;
   $(document).on('click', '.next a', function (e) {
        e.preventDefault();
          var checkclass=$(this).closest(".paginateproject").find(".next").attr("class");
    if (checkclass=='next disabled') {
         return false;
    }
        var lastpage =$(document).find('.paginate').val();
         var page = $(this).attr('href');
                var searchvals = $('.searchval').val(); //search the input value
               var selections = $('.pull-left').text(); //select the column name
               if(page_name=="applist"){
                   if(selections=="Search By Users"){
                   var seletced='Users.first_name'; 
                  }
               }else if(page_name=="emplist"){
                       if(selections=="Search By First Name"){
                        var seletced='Users.first_name'; 
                        }else if(selections=="Search By Employee Code"){
                         var seletced='Users.employee_code';
                        }else if(selections=="Search By Email"){
                          var seletced='Users.username';
                        }else if(selections=="Search By Phone"){
                          var fieldName='Users.phone';
                        }else if(seletced=="Search By Current Salary"){
                          var seletced='Appraisals.appraised_amount';
                        }
                  }
           $.ajax({ 
                url :page+'&field='+seletced+'&value='+searchvals,
                    dataType: 'json',
                    type: "get",
                   // async : false,
                    beforeSend: function() {
                        $('.ajax').hide(); 
                        $('.preloader').show();
                    },
                    complete: function(){
                        $('.preloader').hide();
                       $('.ajax').show(); 
                    }
                }).done(function (data) {
            if(data.resultData != ""){
              $('.ajax').html('');
              var out = '';
              out+='<div class="parent_data">';
              $.each(data.resultData ,function(key,value ) {
                    if(value.status==0){
                    var status='<input class="js-switch pull-right textChekb0x" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }else{
                      var status='<input class="js-switch pull-right textChekb0x" checked="" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }
                    if(page_name=="applist"){
                       if(value.Supervisor  === null){
                         var a="";
                         }else{
                          var a=value.Supervisor.first_name+" "+value.Supervisor.last_name.substring(0,13);
                         }
                         if(value.Users  === null){
                         var username="";
                         }else{
                         var username=value.Users.first_name+" "+value.Users.last_name.substring(0,15);
                         }
                      var icons='<ul class="list-group"><li class="list-group-item list-group-item-danger 1"><b>Name :</b>'+username+'</li> <li class="list-group-item list-group-item-success 2"><b>Supervisor :</b> '+a+'</li><li class="list-group-item list-group-item-info 3"><b>Appraised amount :</b>'+value.appraised_amount+'</li><li class="list-group-item list-group-item-warning 4"><b>Appraisal date :</b>'+convert(value.appraisal_date)+'</li><li class="list-group-item list-group-item-danger 5"><b>Next review:</b> '+convert(value.next_review_date)+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"> <ul class="nav nav-tabs list-group-item-info " role="tablist"> <li><a id="" class="btn default btn-outline popup_window" href="'+base+'appraisals/details/'+value.id+'" title="Appraisal Detail"><i class="fa fa-list"></i></a></li><li><a href="'+base+'appraisals/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li><a href="'+base+'appraisals/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li>'+status+'</li> </ul><input id="appraisal_date" class="appraisal_date" value="'+dating(value.appraisal_date)+'"><input id="next_review_date" class="next_review_date" value="'+dating(value.next_review_date)+'"> </ul>';
                  }else if(page_name=="emplist"){
                       if(value.salary  === null){
                         var s="";
                         }else{
                          var s=value.salary;
                         }

                      var icons='<ul class="list-group "><li class="list-group-item list-group-item-danger 1"><b>Employee Code :</b> '+value.employee_code.substring(0,15)+' </li><li class="list-group-item list-group-item-success 2"><b>First Name :</b> '+value.first_name.substring(0,15)+'</li><li class="list-group-item list-group-item-info 3"><b>Email :</b>  <a href="mailto:'+value.username+'">'+value.username.substring(0,15)+'</a></li><li class="list-group-item list-group-item-warning 4"><b>Phone :</b> '+value.phone+'</li><li class="list-group-item list-group-item-danger 5"><b>Salary:</b> '+s+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"><input class="phoneicons" value="'+value.phone+'"  type="hidden"><input class="salaryicons" value="'+s+'"  type="hidden"><ul class="nav nav-tabs list-group-item-info " role="tablist"><li> <a href="'+base+'employees/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li> <a href="'+base+'employees/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li> <a href="'+base+'employees/credential_detail/'+value.id+'" class="btn default btn-outline popup_window" title="Credentials detail" target="_blank"><i class="fa icon-key"></i></a> </li> <li>'+status+'</li></ul></ul>';
                    }
              var icons=icons;
                out += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata"><div class="white-box"><ul class="nav nav-tabs firstLISt" role="tablist"><li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li><li role="presentation" class=""><a href="#iprofile" style="display:none;" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li><li role="presentation" class=""><a href="#imessages" style="display:none;" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li><li role="presentation" class=""><a href="#isettings" style="display:none;" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li></ul><div class="tab-content"> <div role="tabpanel" class="tab-pane ihome active"><ul class="list-group ">'+icons+'</ul> </div> <div role="tabpanel" class="tab-pane iprofile"> <div class="col-md-6"><h3>Lets check profile</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div> <div class="clearfix"></div> </div><div role="tabpanel" class="tab-pane imessages"> <div class="col-md-6"><h3>Come on you have a lot message</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div><div role="tabpanel" class="tab-pane isettings"><div class="col-md-6"><h3>Just do Settings</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div></div></div></div>';
              });
                   setTimeout(function(){ 
                      $('.js-switch').each(function() {
                          new Switchery($(this)[0], $(this).data());
                      });
                    }, 200);
              out +='</div>';
              $('.ajax').append(out+'<div>')+'';
              $(document).find('.paginate').val('');
              $(document).find('.paginate').val(data.paginatecount);
              if(data.paginatecount==1){
              $(document).find('.counteringdata').html('');
              $(document).find('.counteringdata').append(1+"of"+data.paginatecount);
              }else{
              if(data.page==""){
              data.page=1;
              }else{
              data.page=data.page;
              }
              $(document).find('.counteringdata').html('');
              $(document).find('.counteringdata').append(data.page+"of"+data.paginatecount);
              }
              if(data.page==lastpage){
              var counter=1;
              var prev= data.page - counter;
              $(document).find('.paginateproject').html('');
              $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev"><a href="'+linkhref+'?page='+prev+'&amp;sort='+sort+'" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
              }else{
              var counter=1;
              var increment=data.page;
              var x= +increment+counter;
              var prev= data.page - counter;
              if(prev==0){
              $(document).find('.paginateproject').html('');
              $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page='+x+'&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul>');
              }else{
              $(document).find('.paginateproject').html('');
              $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev"><a href="'+linkhref+'?page='+prev+'&amp;sort='+sort+'" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page='+x+'&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul>');
              }
              }
          }else{
                  $('.ajax').html('');
                 $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                 $(document).find('.counteringdata').html('');
                 $(document).find('.counteringdata').append(" 0 of 0");
                 $(document).find('.paginateproject').html('');
                 $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
               }

          if(data.resultData==null){
                $('.ajax').html('');
                $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
              }


        }).fail(function (response) {
          var error=response.responseText;
         if(error){
                $('.ajax').html('');
              $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
           }
     });
 });





   $(document).on('click', '.prev a', function (e) {
        e.preventDefault();
          var checkclass=$(this).closest(".paginateproject").find(".prev").attr("class");
    if (checkclass=='prev disabled') {
         return false;
    }
            var lastpage= $(document).find('.paginate').val();
         var page = $(this).attr('href');
                var searchvals = $('.searchval').val(); //search the input value
               var selections = $('.pull-left').text(); //select the column name
               if(page_name=="applist"){
                   if(selections=="Search By Users"){
                   var seletced='Users.first_name'; 
                  }
               }else if(page_name=="emplist"){
                       if(selections=="Search By First Name"){
                        var seletced='Users.first_name'; 
                        }else if(selections=="Search By Employee Code"){
                         var seletced='Users.employee_code';
                        }else if(selections=="Search By Email"){
                          var seletced='Users.username';
                        }else if(selections=="Search By Phone"){
                          var fieldName='Users.phone';
                        }else if(seletced=="Search By Current Salary"){
                          var seletced='Appraisals.appraised_amount';
                        }
                  }
             if(selections != "" && searchvals!= ""){
               var url=page+'&field='+seletced+'&value='+searchvals;
             }else{
              var url=page;
             }
           $.ajax({ 
                url :url,
                    dataType: 'json',
                    type: "get",
                   // async : false,
                    beforeSend: function() {
                        $('.ajax').hide(); 
                        $('.preloader').show();
                    },
                    complete: function(){
                        $('.preloader').hide();
                       $('.ajax').show(); 
                    }
                }).done(function (data) {
                 if(data.resultData != ""){
                $('.ajax').html('');
                var out = '';
                out+='<div class="parent_data">';
                $.each(data.resultData ,function(key,value ) {
                     if(value.status==0){
                    var status='<input class="js-switch pull-right textChekb0x" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }else{
                      var status='<input class="js-switch pull-right textChekb0x" checked="" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }
                  if(page_name=="applist"){
                       if(value.Supervisor  === null){
                         var a="";
                         }else{
                          var a=value.Supervisor.first_name+" "+value.Supervisor.last_name.substring(0,13);
                         }
                         if(value.Users  === null){
                         var username="";
                         }else{
                         var username=value.Users.first_name+" "+value.Users.last_name.substring(0,15);
                         }
                      var icons='<ul class="list-group"><li class="list-group-item list-group-item-danger 1"><b>Name :</b>'+username+'</li> <li class="list-group-item list-group-item-success 2"><b>Supervisor :</b> '+a+'</li><li class="list-group-item list-group-item-info 3"><b>Appraised amount :</b>'+value.appraised_amount+'</li><li class="list-group-item list-group-item-warning 4"><b>Appraisal date :</b>'+convert(value.appraisal_date)+'</li><li class="list-group-item list-group-item-danger 5"><b>Next review:</b> '+convert(value.next_review_date)+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"> <ul class="nav nav-tabs list-group-item-info " role="tablist"> <li><a id="" class="btn default btn-outline popup_window" href="'+base+'appraisals/details/'+value.id+'" title="Appraisal Detail"><i class="fa fa-list"></i></a></li><li><a href="'+base+'appraisals/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li><a href="'+base+'appraisals/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li>'+status+'</li> </ul><input id="appraisal_date" class="appraisal_date" value="'+dating(value.appraisal_date)+'"><input id="next_review_date" class="next_review_date" value="'+dating(value.next_review_date)+'"> </ul>';
                  }else if(page_name=="emplist"){
                       if(value.salary  === null){
                         var s="";
                         }else{
                          var s=value.salary;
                         }

                      var icons='<ul class="list-group "><li class="list-group-item list-group-item-danger 1"><b>Employee Code :</b> '+value.employee_code.substring(0,15)+' </li><li class="list-group-item list-group-item-success 2"><b>First Name :</b> '+value.first_name.substring(0,15)+'</li><li class="list-group-item list-group-item-info 3"><b>Email :</b>  <a href="mailto:'+value.username+'">'+value.username.substring(0,15)+'</a></li><li class="list-group-item list-group-item-warning 4"><b>Phone :</b> '+value.phone+'</li><li class="list-group-item list-group-item-danger 5"><b>Salary:</b> '+s+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"><input class="phoneicons" value="'+value.phone+'"  type="hidden"><input class="salaryicons" value="'+s+'"  type="hidden"><ul class="nav nav-tabs list-group-item-info " role="tablist"><li> <a href="'+base+'employees/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li> <a href="'+base+'employees/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li> <a href="'+base+'employees/credential_detail/'+value.id+'" class="btn default btn-outline popup_window" title="Credentials detail" target="_blank"><i class="fa icon-key"></i></a> </li> <li>'+status+'</li></ul></ul>';
                   }
                 var icons=icons;
                out += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata"><div class="white-box"><ul class="nav nav-tabs firstLISt" role="tablist"><li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li><li role="presentation" class=""><a href="#iprofile" style="display:none;" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li><li role="presentation" class=""><a href="#imessages" style="display:none;" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li><li role="presentation" class=""><a href="#isettings" style="display:none;" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li></ul><div class="tab-content"> <div role="tabpanel" class="tab-pane ihome active"><ul class="list-group ">'+icons+'</ul> </div> <div role="tabpanel" class="tab-pane iprofile"> <div class="col-md-6"><h3>Lets check profile</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div> <div class="clearfix"></div> </div><div role="tabpanel" class="tab-pane imessages"> <div class="col-md-6"><h3>Come on you have a lot message</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div><div role="tabpanel" class="tab-pane isettings"><div class="col-md-6"><h3>Just do Settings</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div></div></div></div>';
           
                });
                    setTimeout(function(){ 
                      $('.js-switch').each(function() {
                          new Switchery($(this)[0], $(this).data());
                      });
                    }, 200);
                out +='</div>';
                $('.ajax').append(out+'<div>')+'';
                if(data.paginatecount==1){
                $(document).find('.counteringdata').html('');
                $(document).find('.counteringdata').append(1 +"of"+data.paginatecount);
                }else{
                if(data.page==""){
                data.page=1;
                }else{
                data.page=data.page;
                }
                $(document).find('.counteringdata').html('');
                $(document).find('.counteringdata').append(data.page+"of"+data.paginatecount);
                }

                $(document).find('.paginate').val('');
                $(document).find('.paginate').val(data.paginatecount);

                if(data.page==lastpage){
                var counter=1;
                var prev= data.page - counter;
                $(document).find('.paginateproject').html('');
                $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev"><a href="'+linkhref+'?page='+prev+'&amp;sort='+sort+'" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
                }else{
                var counter=1;
                var increment=data.page;
                var x= +increment+counter;
                var prev= data.page - counter;
                if(prev==0){
                $(document).find('.paginateproject').html('');
                $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page='+x+'&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul>');
                }else{
                $(document).find('.paginateproject').html('');
                $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev"><a href="'+linkhref+'?page='+prev+'&amp;sort='+sort+'" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page='+x+'&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul>');
                }
                }
          }else{
                  $('.ajax').html('');
                 $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                 $(document).find('.counteringdata').html('');
                 $(document).find('.counteringdata').append(" 0 of 0");
                 $(document).find('.paginateproject').html('');
                 $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
               }
           if(data.resultData==null){
            $('.ajax').html('');
            $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
            return false;
          }
      }).fail(function (response) {
        var error=response.responseText;
         if(error){
                $('.ajax').html('');
              $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
           }
     });
 });




$(document).on('click', '.resetproject', function (e) {
        e.preventDefault();
         var page = $(this).attr('href');
           $.ajax({ 
                url : page,
                    dataType: 'json',
                    type: "get",
                    beforeSend: function() {
                        $('.ajax').hide(); 
                        $('.preloader').show();
                    },
                    complete: function(){
                        $('.preloader').hide();
                       $('.ajax').show(); 
                    }
                }).done(function (data) {

            //console.log(data);



                 if(data.resultData != ""){
              $('.ajax').html('');
              var out = '';
              out+='<div class="parent_data">';
              $.each(data.resultData ,function(key,value ) {
                     var author=value.author;
                    var description=value.description;
                    var type=value.type;
                    var keyword= value.keyword;
                    var modified = convert(value.modified);
                     if(value.status==0){
                    var status='<input class="js-switch pull-right textChekb0x" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }else{
                      var status='<input class="js-switch pull-right textChekb0x" checked="" data-color="#f96262" data-size="small" style="display: none;" data-switchery="true" type="checkbox">';
                    }
                  if(page_name=="applist"){
                       if(value.Supervisor  === null){
                         var a="";
                         }else{
                          var a=value.Supervisor.first_name+" "+value.Supervisor.last_name.substring(0,13);
                         }
                         if(value.Users  === null){
                         var username="";
                         }else{
                         var username=value.Users.first_name+" "+value.Users.last_name.substring(0,15);
                         }
                      var icons='<ul class="list-group"><li class="list-group-item list-group-item-danger 1"><b>Name :</b>'+username+'</li> <li class="list-group-item list-group-item-success 2"><b>Supervisor :</b> '+a+'</li><li class="list-group-item list-group-item-info 3"><b>Appraised amount :</b>'+value.appraised_amount+'</li><li class="list-group-item list-group-item-warning 4"><b>Appraisal date :</b>'+convert(value.appraisal_date)+'</li><li class="list-group-item list-group-item-danger 5"><b>Next review:</b> '+convert(value.next_review_date)+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"> <ul class="nav nav-tabs list-group-item-info " role="tablist"> <li><a id="" class="btn default btn-outline popup_window" href="'+base+'appraisals/details/'+value.id+'" title="Appraisal Detail"><i class="fa fa-list"></i></a></li><li><a href="'+base+'appraisals/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li><a href="'+base+'appraisals/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li>'+status+'</li> </ul><input id="appraisal_date" class="appraisal_date" value="'+dating(value.appraisal_date)+'"><input id="next_review_date" class="next_review_date" value="'+dating(value.next_review_date)+'"> </ul>';
                  }else if(page_name=="emplist"){
                      if(value.salary  === null){
                         var s="";
                         }else{
                          var s=value.salary;
                         }

                      var icons='<ul class="list-group "><li class="list-group-item list-group-item-danger 1"><b>Employee Code :</b> '+value.employee_code.substring(0,15)+' </li><li class="list-group-item list-group-item-success 2"><b>First Name :</b> '+value.first_name.substring(0,15)+'</li><li class="list-group-item list-group-item-info 3"><b>Email :</b>  <a href="mailto:'+value.username+'">'+value.username.substring(0,15)+'</a></li><li class="list-group-item list-group-item-warning 4"><b>Phone :</b> '+value.phone+'</li><li class="list-group-item list-group-item-danger 5"><b>Salary:</b> '+s+'</li><input class="appicons" value="'+value.encryptedid+'" type="hidden"><input class="phoneicons" value="'+value.phone+'"  type="hidden"><input class="salaryicons" value="'+s+'"  type="hidden"><ul class="nav nav-tabs list-group-item-info " role="tablist"><li> <a href="'+base+'employees/edit/'+value.id+'" class="btn default btn-outline" title="Edit" target="_blank"><i class="fa fa-cog"></i></a></li><li> <a href="'+base+'employees/delete/'+value.id+'" class="btn default btn-outline deleteprojectdata" title="Delete"><i class="ti-close"></i></a></li><li> <a href="'+base+'employees/credential_detail/'+value.id+'" class="btn default btn-outline popup_window" title="Credentials detail" target="_blank"><i class="fa icon-key"></i></a> </li> <li>'+status+'</li></ul></ul>';
                  }
                 var icons=icons;  
               out += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata"><div class="white-box"><ul class="nav nav-tabs firstLISt" role="tablist"><li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li><li role="presentation" class=""><a href="#iprofile" style="display:none;" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li><li role="presentation" class=""><a href="#imessages" style="display:none;" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li><li role="presentation" class=""><a href="#isettings" style="display:none;" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li></ul><div class="tab-content"> <div role="tabpanel" class="tab-pane ihome active"><ul class="list-group ">'+icons+'</ul> </div> <div role="tabpanel" class="tab-pane iprofile"> <div class="col-md-6"><h3>Lets check profile</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div> <div class="clearfix"></div> </div><div role="tabpanel" class="tab-pane imessages"> <div class="col-md-6"><h3>Come on you have a lot message</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div><div role="tabpanel" class="tab-pane isettings"><div class="col-md-6"><h3>Just do Settings</h3><h4>you can use it with the small code</h4></div><div class="col-md-5 pull-right"><p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p></div><div class="clearfix"></div></div></div></div></div>';
                });
                  setTimeout(function(){ 
                      $('.js-switch').each(function() {
                          new Switchery($(this)[0], $(this).data());
                      });
                    }, 200);
              out +='</div>';
              $('.ajax').append(out+'<div>')+'';
              if(data.paginatecount==1){
              $(document).find('.counteringdata').html('');
              $(document).find('.counteringdata').append(1+"of"+data.paginatecount);
              }else{
              if(data.page==""){
              data.page=1;
              }else{
              data.page=data.page;
              }
              $(document).find('.counteringdata').html('');
              $(document).find('.counteringdata').append(1 +"of"+data.paginatecount);
              }
              $(document).find('.paginate').val('');
              $(document).find('.paginate').val(data.paginatecount);
              $(document).find('.paginateproject').html('');
              $(document).find('.paginateproject').append('<ul class="pagination paginate-data"><li class="previousdata"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next"><a rel="next" href="'+linkhref+'?page=2&amp;sort='+sort+'"><span class="fa fa-angle-right"></span></a></li></ul> ');
            }else{
                  $('.ajax').html('');
                 $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                 $(document).find('.counteringdata').html('');
                 $(document).find('.counteringdata').append(" 0 of 0");
                 $(document).find('.paginateproject').html('');
                 $(document).find('.paginateproject').append('<ul class="pagination  paginate-data"><li class="disabled"></li><li class="prev disabled"><a href="" onclick="return false;"><span class="fa fa-angle-left"></span></a></li><li></li><li class="next disabled"><a rel="next" href=""><span class="fa fa-angle-right"></span></a></li></ul>');
               }
               if(data.resultData==null){
                $('.ajax').html('');
                $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
              }
       }).fail(function (response) {
        var error=response.responseText;
         if(error){
                $('.ajax').html('');
              $('.ajax').append('<div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30">Click on reset button.</p>  </div>');
                return false;
           }
     });
 });



  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop2').hide();
  });


  $(document).on('change', '.textChekb0x',function(event){
        $('checkbox').removeClass('clicked');
        $(this).addClass('clicked');
        $(this).closest('.alldata').find('.tab-content').find(".textChekb0x").removeClass('currentsubmodule');
        $(this).closest('.alldata').find('.tab-content').find(".textChekb0x").addClass('currentsubmodule');
         var menuOrigin = $(this).closest('.alldata').find('.tab-content').find('.appicons').val();
         var eORd = ($(this).is(":checked"))?'Yes':'No';
        href = statusurl;
        data_to_post = {"menuOrigin":menuOrigin,'eORd': eORd};
      $.post(href,data_to_post,function(data){
        //alert(data);
       if(JSON.parse( data ).status = 'success' ){
            if(JSON.parse( data ).msg == 'activate' ){
              $(".alerttop1").find(".success_text").html("");
              $(".alerttop1").find(".success_text").append('Selected Data Activated Sucessfully.');
              $(".alerttop1").show();
            }else if(JSON.parse( data ).msg =='deactivate'){
             $(".alerttop1").find(".success_text").html("");
             $(".alerttop1").find(".success_text").append('Selected Data Deactivated Sucessfully.');
             $(".alerttop1").show();
           
            }
        }else if(JSON.parse( data ).status ='failed'){
          $(".alerttop2").find(".error_text").html("");
          $(".alerttop2").find(".error_text").append('Some thing went wrong. please try again later.');
          $(".alerttop2").show();
        }
      });
          });


  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop1').hide();
  });


}); //end function

