$(document).ready(function(){
	function tiggermsgc(type,msg){
				if(type=='info'){
					$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
					$(document).find('.succesfontwrapper').find('.succesfont').append(msg);
					$(document).find('.succesfontwrapper').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.succesfontwrapper').removeClass('default_visible');
					}, 3000);
				} else if(type == 'success'){
					$(document).find('.successwrapperdv').find('.successmsg').html(' ');
					$(document).find('.successwrapperdv').find('.successmsg').append(msg);
					$(document).find('.successwrapperdv').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.successwrapperdv').removeClass('default_visible');
					}, 3000);
				} else if(type == 'error') {
					$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
					$(document).find('.succesfontwrapper').find('.succesfont').append(msg);
					$(document).find('.succesfontwrapper').addClass('default_visible');
					setTimeout(function(){ 
						$(document).find('.succesfontwrapper').removeClass('default_visible');
					}, 3000);
				}
				
			}
	/***********************************************************	*/
	/*WEB SOCKETS USING NODEJS FOR NOTIFICATIONS            		*/
	/*												    			*/
	/*																*/
	/*																*/
	/****************************************************************/
	var base = $('.base').val();
	var origin = $('.empcode').val();
	var originagency = $('.emporiginagency').val();
	var origincode = $('.empcode').val();
	var datetime =  new Date();
	var page = $('.page').val();
	var totalfiles = 0;
	var donefilescount = 0;
	var entity_id;
	var attached_to;
	var currentpage;
	var cuip;
	var socketurl = 'http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000';
	var allowedtyes =['text/plain', 'application/vnd.oasis.opendocument.spreadsheet','image/jpg','image/png','image/jpeg','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/zip', 'application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/x-rar']
	entity_id = $('.entity_id').val();
    attached_to = $('.attached_to').val();
						datatopost = JSON.stringify({ 'endpointorigin' : origin });
				  		var socket = io.connect(socketurl);
				  		var siofu = new SocketIOFileUpload(socket);
				  		 $.ajax({
							    url: socketurl+'/quotes',
							    type: "post",
								contentType: "application/json; charset=utf-8",
							    data: datatopost,
							    beforeSend: function() {
							    	//console.log("Beforsend")
							    },
							    complete: function(){
							    	//console.log("complete")
							    },
							    success: function(data){
							    	//console.log(data);
							    	cuip = data.cuip;
							    	//console.log("success");
								   	//console.log(data);
								   	var out= '';
								   	if(data.data.length > 0){

								   		$.each(data.data, function( index, v ) {
								   			if(v.entity_id == '1'){
				  		 						var url = 'tickets/index';
				  		 					}
				  		 					var from = (v.from == v.to_id) ? 'you' : v.from_name;
								   		 out +='<a href="'+base_url + url +'"><div class="user-img"><img src="'+ base_url +'images/shared/no_image.png" alt="user" class="img-circle"><span class="profile-status online pull-right"></span></div><div class="mail-contnet"><h5>'+v.type+'</h5><span class="mail-desc" title="From: '+v.from_name+'">'+v.logtext+' '+ from +'</span> <span class="time">'+v.created_at +'</span></div></a>';
										});
										$('.notificationswrapper').prepend(out);
								   	}
								},
							    error: function(result){
							        //console.log(result);
							    }
		  					});

				  		 		 socket.on('activity', function (data) {
					  			 	//console.log(data);
						  		 	$(document).find('.actvtspnthst').addClass('point');
						  		 	$(document).find('.actvtshrtbthst').addClass('heartbit');
						  		 	var url ="";
						  		 	//console.log(data);
						  		 	if(data.entity_id == '1'){
						  		 		url = 'tickets/index';
						  		 		//console.log(data.to_id);
						  		 		//console.log(cuip);
						  		 		if(data.to_id == cuip){
								   			if(data.ticket_type == "response")	{
								   				$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
								   				$(document).find('.succesfontwrapper').find('.succesfont').append('Your ticket has been responded');
								   				$(document).find('.succesfontwrapper').find('.succesfont').removeClass('dngerfonts');
								   				$(document).find('.succesfontwrapper').addClass('default_visible');
								   				notifyMe('doers.online(Information)',data.logtext);
								   				setTimeout(function(){ 
								   					$(document).find('.succesfontwrapper').removeClass('default_visible');
								   				}, 3000);
								   			} else {
								   				$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
								   				$(document).find('.succesfontwrapper').find('.succesfont').append('You have new ticket(s)');
								   				$(document).find('.succesfontwrapper').find('.succesfont').removeClass('dngerfonts');
								   				$(document).find('.succesfontwrapper').addClass('default_visible');
								   				//notifyMe('Information','You have new ticket(s)');
								   				notifyMe('doers.online(Information)',data.logtext);
								   				setTimeout(function(){ 
								   					$(document).find('.succesfontwrapper').removeClass('default_visible');
								   				}, 3000);
								   			}
							   			}
						  		 	} else if(data.entity_id == '2'){
						  		 		url = 'admin/activities';
						  		 		//console.log(data.to_id);
						  		 		//console.log(cuip);
						  		 		if(data.to_id == cuip){
								   			//if(data.entity_id == 1)	{
								   				$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
								   				$(document).find('.succesfontwrapper').find('.succesfont').append(data.logtext);
								   				$(document).find('.succesfontwrapper').find('.succesfont').removeClass('dngerfonts');
								   				$(document).find('.succesfontwrapper').addClass('default_visible');
								   				notifyMe('doers.online(Information)',data.logtext);
								   				setTimeout(function(){ 
								   					$(document).find('.succesfontwrapper').removeClass('default_visible');
								   				}, 3000);
								   			//}
							   			}
						  		 	}
						  		 	var totalnotifications = $('.notificationswrapper').find('a').length;
						  		 	//console.log(totalnotifications);
						  		 	if(totalnotifications == 6){
						  		 		$('.notificationswrapper').children().last().remove();
						  		 	}
							  		 	var out = '<a href="'+base_url + url +'"><div class="user-img"><img src="'+ base_url +'images/shared/no_image.png" alt="user" class="img-circle"><span class="profile-status online pull-right"></span></div><div class="mail-contnet"><h5>'+data.type+'</h5><span class="mail-desc" title="From: Sales Team">'+data.logtext+'</span> <span class="time">'+data.created_at +'</span></div></a>';
							  		 	$('.notificationswrapper').prepend(out);
	
								});

	/***
	* -For GET URL PARAMETERS
	* -By : Maninder
	* -on : 28-04-2016
	***/
	function notifyMe(type,message) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }
  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
  var options = {
        body: message,
        dir : "ltr",
        tag : 'Information',
    	icon : base +'images/afterlogin_logo.png'
    };
  var notification = new Notification(type,options);
  }
  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // Whatever the user answers, we make sure we store the information
      if (!('permission' in Notification)) {
        Notification.permission = permission;
      }
      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var options = {
                body: message,
                dir : "ltr",
                tag : 'Information',
    			icon : base +'images/afterlogin_logo.png'
        };
        //console.log(options);
        var notification = new Notification(type,options);
      }
    });
  }
  // At last, if the user already denied any notification, and you
  // want to be respectful there is no need to bother them any more.
}

	/***
	* -For Uploading Files
	* -By : Maninder
	* -on : 19-04-2016
	***/
	 
	 var obj = $("#dragandrophandler");
	 obj.on('drop', function (e) 
	      {
	      	 e.preventDefault();
	      	 //console.log(origin)
		     var files = e.target.files ||  e.originalEvent.dataTransfer.files ; // FileList object.
		     totalfiles = files.length;
		     //console.log(totalfiles);
		     $('.totalfiles').html('');
		     $('.totalfiles').append(totalfiles);
		     $('#inputlebel').css('border', '1px solid black');
        });
	  obj.on('dragenter', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
          
          $('#inputlebel').css('border', '2px dashed #0B85A1');
      });
      $("#dragandrophandler").on('mouseout', function(){
	  		$('#inputlebel').css('border', '1px solid black');
	  });
	  $("#input-file-now-custom-2").on('change', function(){
	  		totalfiles = 1;
	  		$('.totalfiles').html('');
		    $('.totalfiles').append(totalfiles);
	  });
	  currentpage = $(document).find('.currentpage').val();
	if($(document).find('.currentpage') && ($(document).find('.currentpage').val() == 'docadd' || $(document).find('.currentpage').val() == 'projectdetails')){
		

		siofu.listenOnDrop(document.getElementById("dragandrophandler"));
		siofu.listenOnInput(document.getElementById("input-file-now-custom-2"));
		/*siofu.listenOnDrop(document.getElementById("dragandrophandler"));
		siofu.listenOnInput(document.getElementById("input-file-now-custom-2"));*/
		siofu.addEventListener("start", function(event){
			////console.log(event.file);
			$('.prgsswrapper').addClass('default_visible');
			//console.log(origin);
			event.file.meta.origin = origin ;
			event.file.meta.originagency = originagency ;
			event.file.meta.type = event.file.type;
			event.file.meta.entity_id = entity_id;
			event.file.meta.attached_to = attached_to;
			event.file.meta.base_url = base;
			//console.log(event.file.meta);
			 $('.donefiles').html('');
			 $('.donefiles').append(0);
		    //event.file.meta.hello = "world";
		});

		 siofu.addEventListener("progress", function(event){
	        var percent = event.bytesLoaded / event.file.size * 100;
	        $('.upldprgrsprcnt').html('');
			$('.upldprgrsprcnt').append(percent.toFixed(0));
			$(".progress-bar-info").css({"width": percent.toFixed(0)+'%' }); 
	        ////console.log("File is", percent.toFixed(0), "percent loaded");
	    });
		//code to be run on documents page only
		 siofu.addEventListener("start", function(event){
	       if(event.file.size > 10000000){
	       		//console.log(event.file.size);
	       		$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
	       		$(document).find('.succesfontwrapper').find('.succesfont').addClass('dngerfonts');
   				$(document).find('.succesfontwrapper').find('.succesfont').append('Max size must be 10MB');
   				$(document).find('.succesfontwrapper').addClass('default_visible');
   				setTimeout(function(){ 
   					$(document).find('.succesfontwrapper').removeClass('default_visible');
   					$('.prgsswrapper').removeClass('default_visible');
   				}, 3000);
	       } else if(/\.exe$/.test(event.file.name)){
	       		$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
	       		$(document).find('.succesfontwrapper').find('.succesfont').addClass('dngerfonts');
   				$(document).find('.succesfontwrapper').find('.succesfont').append('.exe File(s) Not Allowed');
   				$(document).find('.succesfontwrapper').addClass('default_visible');
   				setTimeout(function(){ 
   					$(document).find('.succesfontwrapper').removeClass('default_visible');
   					$('.prgsswrapper').removeClass('default_visible');
   				}, 3000);
	       } else if(allowedtyes.indexOf(event.file.meta.type) == -1){
	       		tiggermsgc('error','File Type not Allowed!');
	       }
	        //return false;
	    });

		   siofu.addEventListener("progress", function(event){
	        var percent = event.bytesLoaded / event.file.size * 100;
	        $('.upldprgrsprcnt').html('');
			$('.upldprgrsprcnt').append(percent.toFixed(0));
			$(".progress-bar-info").css({"width": percent.toFixed(0)+'%' }); 
	        //console.log("File is", percent.toFixed(0), "percent loaded");
	    });
	    // Do something when a file is uploaded:
	    siofu.addEventListener("complete", function(event){
	        //console.log(totalfiles);
	        if(event.success == true){
	        	donefilescount += 1;
	        	//console.log(donefilescount);
		         $('.donefiles').html('');
			     $('.donefiles').append(donefilescount);
			     if(donefilescount ==  totalfiles){
			     	tiggermsgc('success','New '+ donefilescount +' file(s) Uploaded');
			     	/*$(document).find('.succesfontwrapper').find('.succesfont').html(' ');
	   				$(document).find('.succesfontwrapper').find('.succesfont').append('New '+ donefilescount +' file(s) Uploaded');
	   				$(document).find('.succesfontwrapper').find('.succesfont').removeClass('dngerfonts');
	   				$(document).find('.succesfontwrapper').addClass('default_visible');*/
	   				setTimeout(function(){ 
	   					$('.prgsswrapper').removeClass('default_visible');
	   					setTimeout(function(){ 
	   					//$(document).find('.succesfontwrapper').removeClass('default_visible');
	   					$('.donefiles').html('');
			 			$('.donefiles').append(0);
			 			$('.upldprgrsprcnt').html('');
						$('.upldprgrsprcnt').append(0);
						$('.donefiles').html('');
			     		$('.donefiles').append(0);
			     		$(".progress-bar-info").css({"width": 0 +'%' }); 
			     		$('.totalfiles').html('');
		    			$('.totalfiles').append(0);
		    			$(document).find('.reloaddocs').trigger('click');
		    			donefilescount = 0;
	   					}, 2000);
	   				}, 1000);
	   				
			     	
			     }
			   ////console.log(event.file/*.detail.keyname*/);
			   //console.log(event.detail.keyname);
			   var out ='';
			   
	        }
	        ////console.log(event.file);
	    });
	     siofu.addEventListener("error", function(data){
	     	//console.log(data);
			    if (data.code === 1) {
			        alert("Don't upload such a big file");
			    }
			});
	    $(document).on('click', '.reloaddocs' , function (event) {
	    	event.preventDefault();
	    	datatopost = {task : 'reloaddocs'};
	    	var url = (currentpage == 'projectdetails') ? base_url+'projects/projectdetails/'+attached_to: currentpage;
	    	 $.ajax({
					    url: url,
					    type: "post",
						//contentType: "application/json; charset=utf-8",
					    data: datatopost,
					    beforeSend: function() {
					    	//console.log("Beforsend")
					    },
					    complete: function(){
					    	//console.log("complete")
					    },
					    success: function(data){
					    	////console.log(data);
					    	var result = JSON.parse(data);
					    	  $('.resulssets').html(' ');
						   	if(result.docs.length > 0 && result.status == 'success'){
						   		 var out = '';
					                  $.each(result.docs, function( index, v ) {
					                  	//console.log(v);
					                   // var lebeltext = v.type;
					                    var lebelclass = 'label-info';
					                    if(v.type == 'application/vnd.oasis.opendocument.spreadsheet'){
					                      lebeltext = 'Excel';
					                      lebelclass = 'fa-file-excel-o';
					                    } else if(v.type == 'image/jpg' || v.type == 'image/png' || v.type == 'image/jpeg'){
					                      lebeltext = 'Image';
					                       lebelclass = 'fa-file-image-o';
					                    } else if (v.type == 'application/pdf'){
					                      lebeltext = 'Pdf';
					                       lebelclass = 'fa-file-pdf-o';
					                    } else if (v.type == 'application/msword' || v.type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
					                      lebeltext = 'Ms Word';
					                      lebelclass = 'fa-file-word-o';
					                    } else if (v.type == 'application/zip'){
					                      lebeltext = 'Zip';
					                       lebelclass = 'fa-file-zip-o';
					                    }  else if(v.type == 'application/vnd.ms-excel' || v.type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
					                       lebeltext = 'Excel';
					                       lebelclass = 'fa-file-excel-o';
					                    } else {
					                       lebeltext = 'Unknown';
					                       lebelclass = 'fa-file-image-o';
					                    }
					                    //console.log(lebelclass);
					                    //console.log(v.name);
					                   // out+='<div class="col-md-4 singledoc"><div class="white-box"><div class="col-md-12"><div class="col-md-12 m-t-5"><i class="fa '+ lebelclass +' bigfa" title="'+ lebeltext +'"></i></div><div class="col-md-12 m-t-5"><div class="row"><div class="font-bold orgnalname"><a href="https://doersdocs.s3.amazonaws.com/projects/'+ v.name +'" target ="_blank">' + v.original_name + '</a></div></div><div class="row"><div class="addedby">Added By:'+ v.Creater.first_name +' on '+ v.Creater.last_name +'</div></div>  <div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+ v.id +'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+ (v.description != '') ? v.description : "Add Comment" +'</a></div></div></div></div><div class="col-md-6"></div></div><div class="editbox"></div></div>';
					                   var des = (v.description != null) ? v.description : "Add Comment";
					                   var x = '<div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+ v.id +'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+ des +'</a></div></div></div>';
					                   out+='<div class="col-md-4 singledoc"><div class="white-box"><div class="col-md-12"><a href="'+ v.name +'" class="deldoc pull-right"><i class="fa fa-times"></i></a><div class="col-md-12 m-t-5 text-center"><i class="fa '+ lebelclass +' bigfa" title="'+ lebeltext +'"></i></div><div class="col-md-12 m-t-5 text-center"><div class="row text-center"><div class="font-bold orgnalname"><a href="https://doersdocs.s3.amazonaws.com/projects/'+ v.name +'" target ="_blank">' + v.original_name + '</a></div></div>  <div class="row text-center"><div class="addedby">Added By:'+ v.Creater.first_name +' on '+ v.Creater.last_name +'</div></div> '+ x +' </div></div></div></div>';
					                    //console.log(out);
					                   // out+='<div class="row"><div class="addedby">Added By:'+v.Creater.first_name+'  '+v.Creater.last_name+' on '+v.created+'</div></div>';
					                   // out+='<div class="row"><div class="addcmnt"><a href="#" class="adddes" id="description" data-type="text" data-pk="'+v.id+'" data-placement="right" data-placeholder="Required" data-title="Enter Description">'+(v.description != '') ? v.description: "Add Comment"+'</a></div></div></div></div><div class="col-md-6"></div></div><div class="editbox"></div></div>';
					                });
									
					             
					               $('.resulssets').append(out);
					               editable();
						   	}
						},
					    error: function(result){
					        //console.log(result);
					    }
  					});

	   		 });

	}

	function editable(){
				$('.adddes').editable({
		     validate: function(value) {
		       if($.trim(value) == '') return 'This field is required';
		     },
		     //mode: 'inline',
		     url: base+ 'documents/addescription', 
	           ajaxOptions: {
	               dataType: 'json' //assuming json response
	           },           
	           success: function(data, config) {
	               if(data && data.id) {  //record created, response like {"id": 2}
	                   //set pk
	                   $(this).editable('option', 'pk', data.id);
	                   //remove unsaved class
	                   $(this).removeClass('editable-unsaved');
	                   //show messages
	                   var msg = 'New user created! Now editables submit individually.';
	                   $('#msg').addClass('alert-success').removeClass('alert-error').html(msg).show();
	                   $('#save-btn').hide(); 
	                   $(this).off('save.newuser');                     
	               } else if(data && data.errors){ 
	                   //server-side validation error, response like {"errors": {"username": "username already exist"} }
	                   config.error.call(this, data.errors);
	               }               
	           },
	           error: function(errors) {
	               var msg = '';
	               if(errors && errors.responseText) { //ajax error, errors = xhr object
	                   msg = errors.responseText;
	               } else { //validation error (client-side or server-side)
	                   $.each(errors, function(k, v) { msg += k+": "+v+"<br>"; });
	               } 
	               $('#msg').removeClass('alert-success').addClass('alert-error').html(msg).show();
	           }
		   });
	}





/*
      obj.on('dragenter', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
          $(this).css('border', '2px solid #0B85A1');
      });
      obj.on('dragover', function (e) 
      {

           e.stopPropagation();
           e.preventDefault();
      });
	      obj.on('drop', function (e) 
	      {
	      	 e.preventDefault();
				  var files = e.target.files ||  e.originalEvent.dataTransfer.files ; // FileList object.
				  //console.log(files);
				 for(var x = 0; x < files.length; x++){
				    var currFile = files[x];
				    var type = currFile.type;
				    //console.log(type);
				    var fileName = currFile.name;
				    var reader = new FileReader();
					reader.onload = (function(theFile){
					    var fileName = theFile.name.replace(/ /g,"_").split('/').join('xy7sj7str');
					    return function(e){
					        //console.log(fileName);
					        var buffer =  e.target.result;
					        //console.log(type);
					        // socket.emit('Start',fileName, buffer,type);
					        //uploader.listenOnInput(document.getElementById("siofu_input"));

					    };
					})(currFile);   
					reader.readAsBinaryString(currFile);
				}


        })*/;

	/**************************************************	*/
	/*WEB WORKRS FOR LOADING DASHBOARD data     		*/
	/*												    */
	/**/
	/**/
	/**************************/
//function startWorker() {
	
    if(typeof(Worker) !== "undefined") {
    	localStorage.removeItem(origincode);
    	if(localStorage.getItem(origincode) === null || localStorage.getItem(origincode) == "" ){
    		if(typeof(w) == "undefined") {
	            w = new Worker(base+'js/dashboardata.js'); // d not repeated knowingly
	            w.postMessage(base)
	        }
	        w.onmessage = function(event) {
	        	localStorage.setItem(origincode, event.data);
	        	processhtml(event.data);
	        };
    	} else {
    		processhtml(localStorage[origincode]);
    	}
        
    } else {
        document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Workers...";
    }

    function processhtml(data){
    		data = JSON.parse(data)
			var userdata = data.userData;
			var previousday = data.previousday;
			var nominaltickets = data.allNominalTicketUser;
			var onleavusers = data.onLeaveUsers;
			var completemenu = data.completemenu;
			/*//console.log((Object.keys(nominaltickets).length));*/
			/*NO STATUS UPDATE IN LAST 24 HOURS*/
			nuhtml = "";
			origin = data.currentuser[0]['originid'];
			nuhtml +='<li class="dropdown-header">No Status Update From Last 24 Hours</li>';
			if(userdata.length > 0){
				$.each(userdata, function( index, value ) {
					/*//console.log(new Date(value.status_modified).getTime() - new Date().getTime()-24*3600);*/
			  if(new Date(value.status_modified).getTime() < new Date().getTime()-24*3600){
			  	nuhtml += '<li>'+value.first_name+' '+value.last_name+'</li>';
			  }

			});
			} else{
				nuhtml +='<li>No Users</li>';
			}
			$('.nostatusupdate').html('');
			$('.nostatusupdate').append(nuhtml);
			/*NOMINAL TICKETS DATA*/

			nthtml = "";
			nthtml +='<li class="dropdown-header">Nominal Tickets, Seems No Work:</li>';
			if(Object.keys(nominaltickets).length > 0){
				$.each(nominaltickets, function( index, value ) {
			  	nthtml += '<li>'+value+'</li>';
			});
			} else{
				nthtml +='<li>No Users</li>';
			}
			$('.nominaltickets').html('');
			$('.nominaltickets').append(nthtml);

			/*ON LEAVE USERS*/
			
			olhtml = "";
			olhtml +='<li class="dropdown-header">On Leave: '+data.attendDate+'</li>';
			if(Object.keys(onleavusers).length > 0){
				$.each(onleavusers, function( index, value ) {
			  	olhtml += '<li>'+value+'</li>';
			});
			} else{
				olhtml +='<li>No Users</li>';
			}
			$('.onleavusers').html('');
			$('.onleavusers').append(olhtml);	
			/*Menus Iteration*/
			menushtml = "";
			menushtml +='';
			if(Object.keys(completemenu).length > 0){
				$.each(completemenu, function( index, value ) {
					menushtml += '<li> <a href="#" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu">'+ (value.display_name).toUpperCase() +'<span class="fa arrow"></span> </span></a>';

					if(Object.keys(value.child_modules).length > 0){
							menushtml += '<ul class="nav nav-second-level">';
						$.each(value.child_modules, function( i, v ) {
							if(v.Perms !== null){
								menushtml += '<li><a href="'+ base + v.Perms.controller +'/'+ v.Perms.action +'">'+v.display_name+'</a></li>';	
							}
							
						});
						menushtml += '</ul>';
					}
					menushtml += '</li>';
				});
				menushtml += '<li class="pull-right" ><a href="'+ base +'tickets/index" class="text-danger">Tickets</a></li>';
			} else{
				menushtml +='<li>No Users</li>';
			}
			$('.menusdata').html('');
			$('.menusdata').append(menushtml);	
			 $('#side-menu').metisMenu();


    }


//}

$("#searchBy").change(function(){
	if($(this).val().match(/\.status/gi) != null){
		$('#search_select').show();
		$('#search_selecttype').hide();
		$('#search_input').hide();
	}else if($(this).val().match(/\.user_type/gi) != null){
		$('#search_select').hide();
		$('#search_selecttype').show();
		$('#search_input').hide();
	}else{
	    $('#search_select').hide();
		$('#search_selecttype').hide();
		$('#search_input').show();
	}
});$('.number').keyup(function(){		if(isEmpty($(this).val()) == false && isNumber($(this).val()) == false){			$(this).val('0');		}	});	
});

//disabling enter key
function stopRKey1(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text" || node.type=="checkbox"))  {return false;}
}

// function to set mode
function setSubmitMode(mode){
$('#UserMode').val(mode);
$('#listForm').submit();
}

// round a number to 2 decimal places
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

// Function to check empty string (return true if empty else false)
function isEmpty(value){
	if(value!=undefined && value.replace(/\s*/gi, "") == "") return true;
	return false;
};
// Function to check zip code
function isValidZip(value){
	if(trim(value).match(/^\s*\d{5,6}\s*$/gi) != null)
	return true;
	return false;
};

// Function to check phone
function isValidPhone(value){ 
	if(trim(value).match(/^\d{3}\-\d{3}\-\d{4}$/gi) != null || trim(value).match(/^\d{10}$/i) != null)
	return true;
	return false;
};

// Function to check confirm password
function isValidConfirmPassword(value1,value2){ 
	if(value1 == value2)
	return true;
	return false;
};

// Function to check password length -- 4 character
function isValidPassword(value){ 
	if(trim(value).length > 4)
	return true;
	return false;
};

// Function to check email
function isValidEmail(value){ 
	if(trim(value).match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/gi) != null)
	return true;
	return false;
};

// Function to check url
function isValidURL(value){ 
	value = value.replace('www.','');
	if(trim(value).match(/^((http(s?):\/\/)?(www.)?(([\w-_~]+\.)*[\w-_~]+\.[\w-_]+(:[0-9]+)?))(\/.*)?$/gi) != null)
	return true;
	return false;
};

// return a trimmed string
function trim(value){ 
	return value.replace(/^\s*(.*)\s*$/gi, "$1");
};

// check for valid date
function trim(value){ 
	return value.replace(/^\s*(.*)\s*$/gi, "$1");
};

// Function to check valid bar code(UPC and EAN) (return true if empty else false)
function checkBarCode(value){ 
	if(trim(value).match(/^[0-9]{12,13}$/gi) != null) return true;
	return false;
};

// Function to check valid number(unsigned integer) (return true if empty else false)
function isNumber(value){
	if(trim(value).match(/^[0-9]+$/gi) != null) return true;
	return false;
};
// Function to check valid number(integer + decimal number) (return true if empty else false)
function isFloat(value){
	if(trim(value).match(/^\s*([0-9]*)\.?[0-9]*\s*$/gi) != null) return true;
	return false;
};

// Function to check date (mm/dd/yyyy)
function isValidDate(value){ 
	if(value.match(/^\s*(0?[1-9]|1[0-2])\s*(\/)\s*(([1-2][0-9])|(3[0-1])|(0[1-9])|[1-9])\s*(\/)\s*((19)|(2[0-9]))[0-9]{2}\s*$/gi) != null) return true;
	return false;
};

// get current date
function getCurrentDate(){
	var currentTime = new Date();
	m = "";
	d = "";
	month = currentTime.getMonth()+1;
	month = m + month;
	if(month.length == "1"){
		month = '0'+ month;
	}
	day = currentTime.getDate();
	day = d + day;
	if(day.length == "1"){
		day = '0'+ day;
	}
	date = month+'/'+day+'/'+currentTime.getFullYear();	
	return date;
}

//function to disable calendar if not in edit or add mode.
function calender(){
if ($('#savecancel_buttons').attr('style') == "display: none;") {
	return false;
}else{
	$('#datepicker').datepicker('show');
}
return false;
}

/*
*@Date: 07-jan-2013
*@aim : To retrive all the Users corresponding to selected Roles using ajax.
*/
function chk_id(id)
{

	$('#usrdat').html('');
	var users_selected = $('input[id^="user_"]:checked');
	//console.log(users_selected);
	keys = '';
	$("#id-form .roles:checked").each(function(){
			keys += $(this).attr('id')+',';
	});
	if(keys!="")
	{
		href=base_url+'permissions/fetchUserBasedOnRole/';
		$.post(href,{id:keys},function(data){
			   var x =  jQuery.parseJSON( data );
                   	var content = "<table  width='100%'><tr><th width='10%'> </th><th width='10%'>Name</th><th width='10%'>username</th></tr>"
					   $.each(x, function(index ,value){
					         content += '<tr><td><input type="checkbox" value="'+value.id+'" name="checkbox[]" checked="checked"></td><td>'+value.first_name+' </td><td> '+value.username+'  </td></tr>';
					      });
					content += "</table>"
                 	$('#usrdat').append(content);
		});
	}else{
		$('#usrdat').html("<div align='center' style='color:red'>Please Select Role to Get Users!</div>");
	}
}

	/**
    * @Method : assignFreeCredits 
    * @Purpose:This method is in employees module to assign free credentials to user.
    * @Param: id
	* @Param: neelam thakur
    * @Return: boolean
**/
	
	function assignFreeCredits(old_type,old_username){
		var returndata = "";
		href=base_url+'admin/credentials/freecredits/';
		if($.active>0){
		}else{
			$.post(href,{old_type:old_type,old_username:old_username},function(data){
				var returndata = data
			});
		}
	}

	  