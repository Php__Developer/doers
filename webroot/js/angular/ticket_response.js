var app = angular.module('createTree', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable','ngMaterial','lr.upload','FBAngular','ngFileUpload','blockUI','oitozero.ngSweetAlert','xeditable']) ;
	app.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('&^&');
	    $interpolateProvider.endSymbol('&^&');
	  });
/**/
/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('handleKey', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 || event.which === 27) {
											link: scope.handlekeyPress(event.which,event,scope);
											return false;
									}
							} else if(event.type =="blur"){
								link: scope.focusout(event.which,scope);
							}

            });

        };
    });

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

		/*
		NOTES:Directive name :fileModel
		Usage: for handling file uploding in the scope

		*/
	app.directive('fileModel', ['$parse', function ($parse) {
	            return {
	               restrict: 'A',
	               link: function(scope, element, attrs) {
	                  var model = $parse(attrs.fileModel);
	                  var modelSetter = model.assign;

	                  element.bind('change', function(e){
	                     scope.$apply(function(){
	                        modelSetter(scope, element[0].files[0]);
	                        scope.preview_file = (e.srcElement || e.target).files[0];
	                        if(scope.name =='image' ||  scope.name =='image_popupwindow' || scope.name =='video_popupwindow' || scope.name =='video')
	                        scope.preview_(scope.preview_file);
	                     });
	                  });
	               }
	            };
	         }]);

app.factory('socket', function ($rootScope) {
 var socket = io('http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000');
	 socket.on('reloadticketsok', function (data) {
	});
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function (data) {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
      	////console.log(eventName);
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

app.directive('jquerydatetimepicker', function () {
		return {
	               restrict: 'C',
	               link: function (scope, element, attr) {
	               		 var d =  new Date();
	               		 var hours = d.getHours();
	               		 var minutes = 	d.getMinutes();
	               		 ////console.log(hours +':00');
	               		 var logic = function( currentDateTime ){
							  this.setOptions({
							      minTime: hours +':00'
							    });
							};
			              scope.$watch('triggerdtpicker', function (val) {
			                  if (val){
			                  	////console.log(val);
			              		 $(element).find('input').datetimepicker({
			              		 	dateFormat: 'MM dd, yy',
			              		 	minDate:0,
			              		 	 onChangeDateTime:function(dp,$input){
									   		
									  }
			              		 });   
						      }
							})
			              scope.$watch('triggerdateerror', function (val) {
			                  if (val){
						      }
							})

						}
	            };
	});

app.directive('jquerytimepicker', function () {
		return {
	               restrict: 'C',
	               link: function (scope, element, attr) {
	               		 var d =  new Date();
	               		 var hours = d.getHours();
	               		 var minutes = 	d.getMinutes();
	               		 ////console.log(hours +':00');
	               		
			              scope.$watch('triggertimetpicker', function (val) {
			                  if (val){
			                  	////console.log(val);
			              		 $(element).find('input').datetimepicker({
			              		 	 datepicker:false,
  									 format:'H:i',
			              		 	 onChangeDateTime:function(dp,$input){
									   		//console.log($input);
									  }
			              		 });   
						      }
							})
			              scope.$watch('triggerdateerror', function (val) {
			                  if (val){
						      }
							})

						}
	            };
	});


app.directive('summer', function ($timeout) {
		return {
	               restrict: 'A',
	               link: function (scope, element, attr) {
	               		  $(element).summernote({
					            height: 350,                 // set editor height
					            minHeight: null,             // set minimum height of editor
					            maxHeight: null,             // set maximum height of editor
					            focus: false ,
					            toolbar: [
								    ['style', ['bold', 'italic', 'underline', 'clear']],
								    ['fontsize', ['fontsize']],
								    ['color', ['color']],
								    ['para', ['ul', 'ol', 'paragraph']],
								  ],
								  callbacks: {
										onChange: function(contents, $editable) {
									            	var code = $(element).summernote('code');
									            	var filteredContent = code.replace(/<\/?[^>]+(>|$)/g, "").replace(/&nbsp;/g,"").replace(/\s/g,'');
													//var filteredContent = code.replace(/\s+/g, '').replace('<p><br></p>','').replace('<br>','').replace('<p></p>','');
													if(filteredContent.length > 0) {
													     scope.ticket.desc = contents;
													} else {
														 scope.ticket.desc = "";
													}
											    },
								  }
					              // set focus to editable area after initializing summernote
					        });
	               		  $('.focussummernote').on('click',function(){
	               		  	$(element).summernote('focus');
	               		  });
	               		  scope.$watch('resetsummer', function (val) {
			                  	$timeout(function() {
		                  			  $(element).summernote('reset');
			                  	}, 100);
			                  	
						      //}
							})


	               		 //$('#summernote').summernote('reset');


					}
	        };
	});

app.directive('compiledes', function($compile) {
// directive factory creates a link function
return function(scope, element, attrs) {
    scope.$watch(
        function(scope) {
             // watch the 'compile' expression for changes
            return scope.$eval(attrs.compiledes);
        },
        function(value) {
            // when the 'compile' expression changes
            // assign it into the current DOM
            element.html(value);

            // compile the new DOM and link it to the current
            // scope.
            // NOTE: we only compile .childNodes so that
            // we don't get into infinite loop compiling ourselves
            $compile(element.contents())(scope);
        }
    );
};
});


		/*
		Controller Name : createCtr
		Usage: All the logic is written here
		*/
	app.controller('createCtr', function($scope,$filter,$compile,$window,$http,$timeout,$uibModal, $log, $document,$mdDialog ,upload,Fullscreen,Upload,$q, $anchorScroll, $location,blockUI,SweetAlert) {
		// $window.
	$scope.socketurl ="http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000";
	$scope.allapps =[];
	$scope.t_data = [];
	$scope.names =[];
	$scope.sec_container =[];
	$scope.chap_container =[];
	$scope.top_container =[];
	$scope.loop_index =0;
	$scope.expanded= [];
	$scope.search_dd=[];
	$scope.san_= "";
	$scope.success_class="";
	$scope.hide_toggle_sidebar =true;
	$scope.sidebar_class ="success_default";
	$scope.hideicon_class ="";
	$scope.hide_controls =false;
	$scope.complete_data ={};
	$scope.is_valid = "yes";
	$scope.todelete = ""; // setting default value
	$scope.delete_status = ""; // setting default value
	$scope.beforesort ={};
	$scope.activeMenu = "";
	$scope.activeMenu1 = "";
	$scope.activeMenu2 = "";
	$scope.section_bc = "";
	$scope.chapter_bc = "";
	$scope.topic_bc = "";
	$scope.content = "";
	$scope.selectedIndex= "";
	$scope.title ="";
	$scope.app={};
	$scope.embedded_code = "";
	$scope.external_link = "";
	$scope.apps_of_selected_topic = {}; // we kept variable that long to make it understood for later
	$scope.is_image_fullscreen = "";  // to check wether image is allowed in full screen or not
	$scope.is_video_fullscreen = "";  // to check wether video is allowed in full screen or not
	$scope.is_zip_fullscreen = "";  // to check wether zip is allowed in full screen or not
	$scope.is_grid_fullscreen = "";  // to check wether grid is allowed in full screen or not
	$scope.is_text_fullscreen = "";  // to check wether text is allowed in full screen or not
	$scope.being_edited_app = ""; // url eligible encrypted id of the app
	$scope.being_edited_app_index = ''; // index of the apps in the apps array of a particular topic
	$scope.typingTimer =""; // in autosave when we start typing timer is set
	$scope.autosave_status =""; // to show the status while autosaving texts.
	$scope.selected_sec_index =""; // when we click on + icon of section to expand tree or when we click on name of the section index is recored
	$scope.selected_chap_index =""; // when we click on + icon of chapter to expand tree or when we click on name of the chapter index is recored
	$scope.app_drop_start_index =""; // when we drag an app index is recored here
	$scope.app_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.section_drop_start_index =""; // when we drag an app index is recored here
	$scope.section_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.chapter_drop_start_index =""; // when we drag an app index is recored here
	$scope.chapter_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.search= {};
	$scope.apps_through = '';
	$scope.total_apps =0;
	$scope.currently_searched_data =[];
	$scope.attach_index =""; // when user click on attach app icon index is recorded
	$scope.de_attach_index = "";
	$scope.pagination_status="not_triggered";
	$scope.no_apps=false;
	$scope.success_message ="";
	$scope.startFade ="";
	$scope.isFullscreen = false; // all the apps of selected course will be stored in this topic.
	$scope.hide_all = false;
	$scope.old_edit_value ="";
	$scope.edit_status ="";
	$scope.tree_edit_index ="";
	$scope.is_logo_hidden = false;
	$scope.is_preview_hidden = true;
	$scope.chapter_name = "";
	$scope.restore_fs = true;
	$scope.compress_icon_hidden = true;
	$scope.compress_icon_class = "";
	$scope.compress_highlight_hidden = true;
	$scope.compress_highlight_class ="";
	$scope.fs_through = "";
	$scope.call_edit_after ="";
	$scope.showDetails = false;
	$scope.isclickenable = true;
	$scope.shouldgotop = false;
	$scope.expand_chapter_index = [];
	$scope.expand_topic_index =[];
	$scope.fa_class=[];
	$scope.fa_class_=[];
	$scope.selected_top_index =[];
	$scope.defer = $q.defer();
	$scope.show_tree = 'treereplace_shown';
	$scope.activemenus = [];
	$scope.make_gtp_vi = "";
	$scope.new_apps_class ="treereplace_shown";
	$scope.is_disbaled = false;
	$scope.myFile = 'none';
	$scope.tgl_cntr_loader = "make_hidden";
	$scope.userID ="";
	$scope.beforedrag = [];
	$scope.errors = [];
	$scope.fields = [];
	$scope.success=[];
	$scope.active = [];
	$scope.ticket = [];
	$scope.projectid =[];
	$scope.show = [];
	$scope.from_other = 'no';
	$scope.responders = [];
	$scope.info = [];
	$scope.mandatory = [];
	$scope.triggerdtpicker = false;
	$scope.triggertimetpicker = false;
	
	$scope.entities = [];
	$scope.tags = [];
	$scope.oldbillablehours = 0;
	$scope.olddeadline = "";
	$scope.currentuserData = [];
	$scope.fromdbData = [];
	$scope.schduleData = [];
	$scope.sdata = [];
	$scope.selectedschedule = 0;
	$scope.isclickenable = true;
	$scope.issubmitenable = false;
	$scope.ticketcontent = [];
	$scope.on_ticket = "no";
	$scope.tichtml = [];
	$scope.dynamicfields = [];
	$scope.dynamicdes = [];
	$scope.dynamicdes.fields = [];
	$scope.dynamicdes.errors =[];
	$scope.hidetextdess = true;
	$scope.tagdata = [];
	$scope.amilastreplier = 'no';
	var fromdbData = [];
	$scope.currentuserinfo=[];
	$scope.submitvia = "instant";
	$scope.ip = "";
	$scope.showtag = 'yes';
	$scope.resetsummer = false;
	$scope.ispriorityenabled = true;
    $scope.allstatuses = 	[	
    							{k: '1', v: 'Open'},
								{k: '2', v: 'In Progress'},
								{k: '0', v: 'Closed'}
							];
	$scope.hideloader = false;
	$scope.selectedproject = [];
	$scope.reminderid = 0;
	$scope.currentuserroles = "";
	$scope.ticketip = "";				
	$scope.entityvalue = [];
	$scope.isentityvalueenable = true;
	$scope.amiadmin = 'no';
	$scope.isfirsttime = 'yes';
	//$scope.typeclass = true;
	//$scope.statusclass = true;
		$document.ready(function(){
			// sending a request to get topic content inside the scope.
			// local storage was not taking updated json
			if($scope.name == 'ticketresponse'){
				$scope.all_users=[];
				$scope.type=[];
				$scope.priority=[];
				$scope.users = [];
				$scope.projectid =[];
				$scope.count = [];
				//app.type = 'task';

			}
			$scope.infomsgstart = function(msg,type){
				if(type == 'info'){
					$scope.fields.infowrapper = 'default_visible';
					$scope.info.message = msg;
				} else if(type == 'success'){
					$scope.fields.successwrapper = 'default_visible';
					$scope.success.message = msg;
				}	
			}
			$scope.infomsgclose = function(type){
				//////console.log(type);
				if(type == 'info'){
					$scope.fields.infowrapper = 'default_hidden';
				} else if(type == 'success'){
					$scope.fields.successwrapper = 'default_hidden';
				}
			}

			$scope.get_data = function(){
						//$scope.autosave_status ='Loading Content...';
						$scope.issubmitenable = false;
						$scope.infomsgstart('loading...','info');
						$http({
						method: 'POST',
						url: $scope.base_url + 'tickets/ticketdata',
						async : true,
						data : { origin : $scope.origin }
					}).then(function successCallback(response) {
							if(response.data.status == 'success'){
								if(response.data.page == 'ticketresponse'){
									//console.log(response.data.selectedtag);
									$scope.mandatory = response.data.mandatory;
									$scope.ticket = response.data.resultData;
									$scope.fromdbData = response.data.resultData;
									$scope.currentuser = response.data.current_user;
									$scope.isclickenable = ($scope.ticket.from_id == $scope.currentuser) ? true : false ;
									$scope.disbaletitleedit = ($scope.ticket.from_id == $scope.currentuser) ? false : true;
									$scope.istypeenable = ($scope.ticket.from_id == $scope.currentuser) ? true : false;
									$scope.isscheduletypeenable = ($scope.ticket.from_id == $scope.currentuser) ? true : false;
									//$scope.isstatusenable = ($scope.ticket.from_id == $scope.currentuser) ? true : false;
									$scope.isstatusenable = true;
									fromdbData = response.data.resultData;
									$scope.schduleData = ($scope.fromdbData.ticket_parameters !== '') ? angular.fromJson($scope.fromdbData.ticket_parameters) : [];
									$scope.sdata = ($scope.fromdbData.type == 'Scheduled') ? $scope.schduleData : [];
									$scope.tagdata = response.data.selectedtag;
									$scope.showtag = 'no';
									$scope.ip = response.data.ip_address;
									$scope.currentuserroles = response.data.rls.split(',');
									if(($scope.currentuserroles.indexOf("1") >=  0) == true || ($scope.currentuserroles.indexOf("8") >=  0) == true) {
									  $scope.amiadmin ='yes';
									  $scope.ticketip = $scope.ticket.ipaddress;
									  //console.log($scope.ticketip);
									}

									$scope.count.opened = response.data.open;
									$scope.count.closed = response.data.closed;
									$scope.count.inprogress = response.data.inprogress;
									$scope.count.expired = response.data.expired;
									$scope.count.all = response.data.open + response.data.closed + response.data.inprogress + response.data.expired;
									$scope.all_users = response.data.all_users;
									
									//////console.log($scope.currentuser);
									$scope.show.title="default_hidden";
									$scope.from_other = (response.data.resultData.from_id==$scope.currentuser) ? 'no': 'yes';
									$scope.ticket.deadline = $filter('date')($scope.ticket.deadline, "medium",'+0000');  // for type="date"		
									$scope.projectid.push({id:'0',project_name:'Other'});
									$scope.projectid = [];

									/*angular.forEach(response.data.my_projects, function(v, k) {
										if($scope.ticket.project_id == v.id){
											$scope.selectedproject.push(v);
											$scope.selectedprojectface = v.project_name;
										}
										$scope.projectid.push(v);
									});*/
									angular.forEach(response.data.entityvalue, function(v, k) {
										if($scope.ticket.entity_type == 2){
											if($scope.ticket.entity_id == v.id){
											$scope.selectedproject.push(v);
												if($scope.ticket.entity_type == 2){
													$scope.selectedprojectface = v.project_name;	
												}
											}	
											$scope.projectid.push(v);
											$scope.entityvalue.push({k: v.id , v: v.project_name});

										}else if ($scope.ticket.entity_type == 4){

											if($scope.ticket.entity_id == v.id){
												$scope.selectedproject.push(v);
												if($scope.ticket.entity_type == 4){
													$scope.selectedprojectface = v.name;	
												}
											}
											$scope.projectid.push(v);
											$scope.entityvalue.push({k: v.id , v: v.name});	
										}
										$scope.entityvaluemodel = {k: $scope.ticket.entity_id , v: $scope.selectedprojectface };
										
									});
									
									//$scope.entityvalue = "";
									$scope.type = [
										{k: 'To-Do', v: 'To-Do'},
										{k: 'Scheduled', v: 'Scheduled'},
										{k: 'Mandatory', v: 'Mandatory'}
										];
									$scope.status = $scope.statusddvalues($scope.ticket.status,$scope.ticket.type,$scope.ticket.from_id);

									$scope.priority =[
										{k: 'normal', v: 'Normal : Should be replied within 24 Hours'},
										{k: 'medium', v: 'Medium : Should be replied within 12 Hours'},
										{k: 'urgent', v: 'Urgent : Should be replied within 02 Hours'},
										{k: 'emergency', v: 'Emergency : Should be replied within 01 Hour'}
										];
									$scope.scheduletype =[
										{k: '720', v: 'Once in a Month(After 30 days)'},
										{k: '168', v: 'Once in a Week'},
										{k: '24', v: 'Once in a day'},
										{k: '12', v: 'Twice in a day'},
										{k: '2', v: 'After Every 2 hours'},
										{k: '4', v: 'After Every 4 hours'},
										];
									angular.forEach($scope.scheduletype, function(v, k) {
											if(response.data.resultData.type == 'Scheduled' ){
												if(v.k == $scope.schduleData.after){
													$scope.ticket.scheduletypeface = v.v ;
													$scope.selectedschedule = v.k;	
												}
											} else {
												$scope.ticket.scheduletype = "";
											}	
									});

									$scope.ticket.type = {k: response.data.resultData.type };
									if($scope.ticket.entity_type == 0){
											$scope.ticket.entity_type.name = 'None';
									}
									$scope.entities = [];
									angular.forEach(response.data.entities, function(v, k) {
											if(v.id == $scope.ticket.entity_type){
												$scope.ticket.entity_type = v;
											}

												$scope.entities.push(v);
									});
									$scope.oldbillablehours = $scope.ticket.billable_hours;
									$scope.oldtasktime  = $scope.ticket.tasktime;
									$scope.olddeadline = $scope.ticket.deadline;
									$scope.ticket.projectid = {id: response.data.resultData.project_id };
								}
								  $scope.process();

							}

						}, function errorCallback(response) {
							if(response.status > 299){
								// some error occurred
								$scope.fields.successwrapper = 'default_hidden';
								$scope.errors.message = 'Something went wrong!. Please refresh page(Ctrl + Shift + R) and try again. If still facing same issue, Contact Support team';
								$scope.fields.errorswrapper = 'default_visible';
							}
				}).then(function () {
				});
			}
			$scope.get_data(); // calling function onload
			$scope.process = function(){
			
						$scope.users = [];
					angular.forEach($scope.all_users, function(v, k) {
						if($scope.ticket.to_id == v.id){
								$scope.ticket.to_id = v;
						}
						if($scope.currentuser == v.id){
							$scope.currentuserinfo = v;	
							$scope.currentuserData = v;
							//console.log($scope.currentuserinfo);
						}
						$scope.users.push(v);
					});
					angular.forEach($scope.priority, function(v, k) {
						if(v.k == $scope.ticket.priority){
							$scope.ticket.priority = v;	
						}
					});
				$scope.allstatuses.map(function(i){
						if(i.k == $scope.ticket.status){
							$scope.ticket.status = i;	
						}
					});

				

					$scope.app.to_id = {id: $scope.ticket.to_id };
					//$scope.infomsgclose('info');
					$scope.hideloader = true;
					if(angular.isUndefined($scope.mandatory.eid) == false){
						if($scope.mandatory.last_replier !== parseInt($scope.currentuser)){
							$scope.infomsgstart("Please respond to Mandatory ticket(s) first","info");
							if($scope.mandatory.from_id !== parseInt($scope.currentuser)){
								$scope.typeclass = true;
							}
						}
					} else {
						
					}
					$timeout(function() {
							console.log($scope.isfirsttime);
							if($scope.isfirsttime == 'yes'){
								$scope.infomsgstart('loading Discussions...','info');	
							} else {
								$scope.infomsgstart('Re-loading Discussions...','info');	
							}
							
							var Form_data = { ticket_id : $scope.ticket.id, base_url : $scope.base_url };
							var wheretogo = $scope.socketurl + '/getticdescussion';
							var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'getticdescussion');
						}, 50);

			}
	    $scope.statusddvalues = function(currentstatus,type,from_id){
	    		if(parseInt(currentstatus) == 1){
	    			/*if($scope.currentuserroles.indexOf("1") == -1) {
						 $scope.ticketip = $scope.ticket.ipaddress;
				     }*/
	    			if(((type == 'Mandatory' || type == 'Scheduled') && $scope.currentuser !== from_id )){
						if($scope.amiadmin == 'yes'){
							arr = [{k: '2', v: 'In Progress'},{k: '0', v: 'Closed'}];	
						} else {
							arr = [ {k: '2', v: 'In Progress'}];
						}
	    			} else {
	    				arr = [
								{k: '2', v: 'In Progress'},
								{k: '0', v: 'Closed'}
						  ];
	    			} 
					$scope.ticket.currentstatus = arr[0];
	    			return	arr
				} else if(parseInt(currentstatus) == 2){
					
					if((type == 'Mandatory' || type == 'Scheduled') && $scope.currentuser !== from_id ){
						if($scope.amiadmin == 'yes'){
							arr = [{k: '2', v: 'In Progress'},{k: '0', v: 'Closed'}];	
						} else {
							arr = [ {k: '2', v: 'In Progress'}];
						}
	    			} else {
	    				arr = [ {k: '0', v: 'Closed'}];
	    			}
					$scope.ticket.currentstatus = arr[0];
					return	arr;
				} else if(parseInt(currentstatus) == 0) {
					  arr = [
								{k: '2', v: 'In Progress'}
								
							];
					$scope.ticket.currentstatus = arr[0]
					return	arr;
				}

	    					
	    }		
		$scope.showadmin = function() {
		    var selected = $filter('filter')($scope.users, $scope.ticket.to_id );
		    return ($scope.users && selected.length) ? selected[0].first_name  : 'Not set';
		  };
		$scope.updateadmin = function(data) {
				$scope.infomsgstart('Saving...','info');
				$scope.submitvia = "instant";
				var Form_data = { ticket_id : $scope.origin , field : 'to_id', fielddata : data.id , toname : data.first_name +' '+ data.last_name};
				var wheretogo = $scope.base_url + 'tickets/instantsave';
				var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
		};

		  
		$scope.showtype = function() {
		    var selected = $filter('filter')($scope.type, $scope.ticket.type );
		    return ($scope.type && selected.length) ? selected[0].v  : 'Not set';
		};
		$scope.updatetype = function(data) {
				$scope.infomsgstart('Saving...','info');
				if(data.v == 'Scheduled'){
					$scope.sdata.after ="";
					if(fromdbData.type.k == 'Scheduled'){
						$scope.returnfacesch($scope.schduleData.after,'no');
						//$scope.selectedschedule = $scope.schduleData.after;
						var after = $scope.schduleData.after;
						$scope.ticket.scheduletype = $scope.schduleData.after;
					} else {
						$scope.returnfacesch("24",'no');
						$scope.selectedschedule = 24;
						var after = 24;
						//$scope.ticket.scheduletypeface ='Once in a day';
					}
				} else {
					var after = 0;
					$scope.ticket.scheduletype ="";
					$scope.ticket.scheduletypeface = "" ;
					$scope.selectedschedule = 24;	
				}
				$scope.submitvia = "instant";
				var Form_data = { ticket_id : $scope.origin , field : 'type', fielddata : data.v , toname : data.k , after : after , scheduletypeface : $scope.ticket.scheduletypeface };
				var wheretogo = $scope.base_url + 'tickets/instantsave';
				var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
		};

		$scope.showascheduletype = function() {
			if(fromdbData.type == 'Scheduled' || (angular.isUndefined(fromdbData.type.k) && fromdbData.type.k == 'Scheduled') ){
			 	var selected = $filter('filter')($scope.scheduletype, $scope.ticket.scheduletype );
			} else {
				return "";
			}
		    return ($scope.scheduletype && selected.length) ? selected[0].v  : '';

		};

		$scope.updatescheduletype = function(data) {
				$scope.infomsgstart('Saving...','info');
				////console.log(data);
				$scope.ticket.scheduletypeface = data.v ;
				$scope.returnfacesch(data.k,'yes');
		};
		
		$scope.returnfacesch = function(index,saveornot){
				//////console.log(index);
				var counter = 0 
			angular.forEach($scope.scheduletype, function(v, k) {
							////console.log(v.k);
							////console.log(index);
						if(v.k == index){
							$scope.ticket.scheduletypeface = v.v ;
							$scope.selectedschedule = v.k;	
						}
					counter ++ ;
					
				});	
				////console.log($scope.ticket.scheduletypeface);
			if(saveornot == 'yes'){
				$scope.submitvia = "instant";
				var Form_data = { ticket_id : $scope.origin , field : 'type', fielddata : 'Scheduled' , toname : 'Scheduled' , after : $scope.selectedschedule , scheduletypeface : $scope.ticket.scheduletypeface };
				var wheretogo = $scope.base_url + 'tickets/instantsave';
				var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
			}
		
		}
		 $scope.showpriority = function() {
		    var selected1 = $filter('filter')($scope.priority, $scope.ticket.priority );
		    return ($scope.priority && selected1.length) ? selected1[0].v  : 'Not set';
		  };
		 $scope.updatepriority = function(data) {
		 	$scope.infomsgstart('Saving...','info');
		 	$scope.submitvia = "instant";
			var Form_data = { ticket_id : $scope.origin , field : 'priority', fielddata : data.k , toname : data.v };
			var wheretogo = $scope.base_url + 'tickets/instantsave';
			var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
		  };
		  $scope.updateentityalue = function(data) {
		  	angular.forEach($scope.entityvalue, function(v, k) {
				if(data.k == v.k){
					$scope.entityvaluemodel = v;
				 	$scope.infomsgstart('Saving...','info');
				 	$scope.submitvia = "instant";
					var Form_data = { ticket_id : $scope.origin , field : 'entity_id', fielddata : data.k , toname : data.v };
					var wheretogo = $scope.base_url + 'tickets/instantsave';
					var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
				}
			});




		  	
		  };
		 
		  $scope.showstatus = function() {
		    var selected1 = $filter('filter')($scope.allstatuses, $scope.ticket.status );
		    return ($scope.allstatuses && selected1.length) ? selected1[0].v  : 'Not set';
		  };
		   $scope.updatestatus = function(data) {
		   	//console.log(data);
		 	$scope.infomsgstart('Saving...','info');
		 	$scope.submitvia = "instant";
		 	$scope.ticket.status = data;
		 	$scope.showstatus();
			var Form_data = { ticket_id : $scope.origin , field : 'status', fielddata : data.k , toname : data.v };
			var wheretogo = $scope.base_url + 'tickets/instantsave';
			var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
		  };
		 

		  $scope.showdeadline = function() {
		  	$scope.triggerdtpicker = true;
		    var selected1 = $filter('date')($scope.ticket.deadline, "medium",'+0530');
		    return ($scope.ticket.deadline && selected1.length) ? selected1  : 'Not set';
		  };

		   $scope.updatedeadline = function(data){
		  	var result = $scope.onTimeSet(data , 'none');
		  	////console.log(result);
		  	if(result == false){
				$scope.errors.message = 'Deadline must be a future date.By default,it is 2 hours ahead the current time.'
				$scope.fields.errorswrapper = 'default_visible';
				return "Invalid Deadline!";
		  	} else {
  				 $scope.fields.errorswrapper = 'default_hidden';
				 $scope.olddeadline = $scope.ticket.deadline;
		  	}

		  	$scope.ticket.deadline = $filter('date')(data, "medium",'+0530');
			$scope.infomsgstart('Saving...','info');
			$scope.submitvia = "instant";
			var Form_data = { ticket_id : $scope.origin , field : 'deadline', fielddata : $scope.ticket.deadline  , toname : $scope.ticket.deadline };
			var wheretogo = $scope.base_url + 'tickets/instantsave';
			var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
		  }		


		  $scope.showbillablehours = function() {
		  	$scope.triggertimetpicker = true;
		    var selected1 = $scope.ticket.billable_hours;
		    if($scope.hourminformat($scope.ticket.billable_hours) == false){
				 $scope.errors.message = 'Please Valid Task time i.e 1:00';
				 $scope.fields.errorswrapper = 'default_visible';
				$timeout(function(){
					$scope.ticket.billable_hours = $scope.oldbillablehours;	
				},200);
				} else {
					 $scope.fields.errorswrapper = 'default_hidden';
					 $scope.oldbillablehours = $scope.ticket.billable_hours;
					 return ($scope.ticket.billable_hours && selected1.length) ? selected1  : 'Not set';
				}
		  };
		
		  
		  
		  $scope.updateba = function(data) {
		  	////console.log(data);
		 	  var selected1 = $scope.ticket.billable_hours;
		 	  var result = $scope.hourminformat(data);
		    if( result == false){
				 $scope.errors.message = 'Please Valid Billable Hours i.e 1:00';
				 $scope.fields.errorswrapper = 'default_visible';
				 return "Please Valid Task time i.e 1:00";
				} else {
					 $scope.fields.errorswrapper = 'default_hidden';
					 $scope.oldbillablehours = $scope.ticket.billable_hours;
				}
			$scope.infomsgstart('Saving...','info');
			$scope.submitvia = "instant";
			var Form_data = { ticket_id : $scope.origin , field : 'billable_hours', fielddata : data , toname : data};
			var wheretogo = $scope.base_url + 'tickets/instantsave';
			var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');

		  };
		  $scope.updatetasktime = function(data) {
		 	  var result = $scope.hourminformat(data);
		    if( result == false){
				 $scope.errors.message = 'Please Valid Task time i.e 1:00';
				 $scope.fields.errorswrapper = 'default_visible';
				 return "Please Valid Task time i.e 1:00";
				} else {
					 $scope.fields.errorswrapper = 'default_hidden';
					 $scope.oldtasktime = $scope.ticket.tasktime;
					 $scope.infomsgstart('Saving...','info');
					 $scope.submitvia = "instant";
					var Form_data = { ticket_id : $scope.origin , field : 'tasktime', fielddata :data , toname : data };
					var wheretogo = $scope.base_url + 'tickets/instantsave';
					var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
				}

		  };

		   $scope.deschange = function(contents) {
			    ////console.log('contents are changed:', contents, $scope.editable);
			  };
			$scope.deskeyup = function(e) {
			 ////console.log('Key is released:', e.keyCode); 
			}  
			$scope.desinit = function(e) {
			 ////console.log('Key is released:'); 
			}  
			$scope.goback = function(){
						if($scope.redirect == '/'){
							$timeout(function(){
								$window.location.href = $scope.base_url + 'tickets/index';
						 	},50);
						} else {
							$timeout(function(){
								$window.location.href = $scope.redirect ;
							 },50);	
						}
					};
			$scope.setreminder= function(id){
				//console.log(id);
				$scope.reminderid = id;
				$scope.trigger_sweet_confirmation('Are You Sure To Send Reminder?',"Yes!Send It",'warning','ticindex','yes');
			}		

			$scope.togglepriority = function(){
						//$scope.priorityindex = index;
						//$scope.t_data[index].priority = 'medium';
						var priority = $scope.ticket.priority.k;
						//console.log(priority);
						if($scope.ticket.priority.k == 'normal'){
							 priority = 'medium';
						} else if($scope.ticket.priority.k == 'medium'){
							 priority = 'urgent';
						} else if($scope.ticket.priority.k == 'urgent'){
							 priority = 'emergency';
						}else if($scope.ticket.priority.k == 'emergency'){
							 priority = 'normal';
						} else {
							 priority = 'normal';
						}
						$scope.ispriorityenabled = false;
						var Form_data = { ticket_id : $scope.ticket.id , field : 'priority', fielddata : priority };
						var wheretogo = $scope.base_url + 'tickets/instantsave';
						var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
						$scope.infomsgstart('Saving...','info');
			}
				
			$scope.options = {
            height: 400,
            toolbar: [
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['height', ['height']]
            ]
          };
						// common model function
						$scope.open = function (desired_html,desired_width,redirect,ev) {
										$mdDialog.show({
										controller: DialogController,
										template: desired_html,
										parent: angular.element(document.body),
										targetEvent: ev,
										clickOutsideToClose: false,
      							escapeToClose: false,
										locals: {
										items: {'originidentity': $scope.originidentity},
										},
										fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
										})
										.then(function(response) {
											if(redirect=='edit'){
													$scope.is_valid = "no";
													$scope.isclickenable = true;
													$scope.edit( $scope.tree_edit_index,'edit' );
											}else if(response=='delete'){
													$scope.delete_node();
											}else if(response=='drop'){
												if(response =="drop"){
														if($scope.currently_dragging == 'sections'){
																if($scope.section_drop_start_index < $scope.section_drop_end_index ){
																	// sorting up to down
																	$scope.names.splice($scope.section_drop_end_index + 1, 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index, 1);
																	$scope.posttree("none");
																} else {
																	// sroting down to up
																	$scope.names.splice($scope.section_drop_end_index , 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index + 1, 1);
																	$scope.posttree("none");
																}
															
														} else{
															var counter = 0
															angular.forEach($scope.expanded, function(v, k) {
															$scope.names[v] = $scope.t_data[v];
															counter++;
															});
															if(counter == $scope.expanded.length){
																$scope.posttree("none");
															}
														}
														
													// if user wants to drop
												}
											}else if(response =="no_drop"){
													/*$scope.t_data = JSON.parse(window.localStorage.getItem("beforedrag"));*/
													$scope.t_data = $scope.beforedrag;
											} else if(response == 'drop_app'){
												// if user wants to drop app
												taskId = $scope.selectedIndex;
												var arr = taskId.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var apps =  $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'];
												if($scope.app_drop_start_index < $scope.app_drop_end_index ){
													// sorting up to down
													apps.splice($scope.app_drop_end_index + 1, 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index, 1);
													$scope.posttree("get_apps");
												} else {
													// sroting down to up
													apps.splice($scope.app_drop_end_index , 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index + 1, 1);
													$scope.posttree("get_apps");
												}
											}else if(response == 'no_drop_app'){
												$scope.posttree("get_apps");
												// if user does not want to drop app
											} else if(response == 'attach_app'){
												Form_data = {_token: $window.localStorage.getItem("t") , in_ : $scope.attach_index, search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'attach'}
												var wheretogo ='attach_app';
												$scope.commonPostFunc(wheretogo,Form_data,'attach_app'); // sending a post request
											} else if(response == 'deattach_app'){
												index = $scope.de_attach_index ;
												var arr = $scope.selectedIndex.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var toeditapp = $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'][index];
												$scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'].splice(index, 1);
												$location.hash('wrapper'); // taking pointer to the top passed id
												$anchorScroll(); // taking pointer to the top passed id
												$scope.common_success_meesage("App De-attached Successfully!");
												$timeout(function(){
												$scope.posttree("get_apps");		
												}, 500);
											}
										}, function() {

										});
					  };
						function DialogController($scope, $mdDialog,items) {
							$scope.title ="";
							 $scope.originidentity = items.originidentity;
								 $scope.hide = function() {
									 $mdDialog.hide();
								 };
								 $scope.answer = function(answer) {
									 $mdDialog.hide(answer);
								 };
								 $scope.ok = function (response) {
									 $mdDialog.hide("ok");
								 };
								$scope.delete = function (response) {
									$mdDialog.hide("delete");
								};
								$scope.drop = function (response) {
									$mdDialog.hide("drop");
								};
								$scope.cancel_drop = function (response) {

									$mdDialog.hide("no_drop");
								};
								$scope.drop_app = function (response) {

									$mdDialog.hide("drop_app");
								};
								$scope.cancel_drop_app = function (response) {
									$mdDialog.hide("no_drop_app");
								};
								$scope.go_attach = function (response) {
									$mdDialog.hide("attach_app");
								};
								$scope.deattach_app = function (response) {
									$mdDialog.hide("deattach_app");
								};
								
								$scope.close_model = function (response) {
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;
									$mdDialog.hide("close_dialog");
								};
							 // will be used if cancel button needed to be displayed
								 $scope.cancel = function (response) {
									 $mdDialog.hide('cancel');
								 };
								 $scope.handleenterkey = function(keycode,event,scope){
								 }
							 };

				$scope.new_app_submit = function (response) {
						console.log("Test");
							////console.log($scope.isvaliddata($scope.ticket));
							$scope.infomsgstart('Saving Response...', 'info');
						if($scope.is_disbaled == false && $scope.isvaliddata($scope.ticket) == true){
							 $scope.is_disbaled = false;
							 $scope.issubmitenable = false;
							 //$scope.success_class = 'sucess_mesage';
							//$scope.success_message = "Uploading Content...";
							//$location.hash('anchor_top');
							//$anchorScroll();
						$timeout(function(){
							if($scope.dynamicfields.length > 0){
								$scope.ticket.desc =  $scope.replaceplaceholders($scope.dynamicdes);										
							}
						}, 1000).then(function(){
									var file = $scope.myFile;
									if($scope.enc_status == 'no'){
      								$scope.being_edited_app = $scope.enc;
      							}
								//$scope.app.course_id = $scope.originidentity;
									////console.log($scope.name);
								if($scope.name == 'ticket'){
									  ////console.log($scope.app);
									  $scope.app.to_id = $scope.app.to_id.id;
									  $scope.app.priority = $scope.app.priority.k;
									  $scope.app.projectid = $scope.app.projectid.id;
									  $scope.app.type = $scope.app.type.k;
									  
									  var uploadUrl = $scope.base_url+"tickets/add2";
									  file = 'none';
								} else if($scope.name == 'ticketresponse'){
									  $scope.isfirsttime ='no';
									  $scope.app = $scope.ticket;
									  $scope.app.last_page ='response';
									  //($scope.from_other== 'yes') ? $scope.app.response = $scope.app.desc : $scope.app.description = $scope.app.desc;
									  $scope.app.status = $scope.ticket.status.k;
									  $scope.app.to_id = $scope.ticket.to_id.id;
									  $scope.app.priority = $scope.ticket.priority.k;
									  $scope.app.projectid = $scope.ticket.projectid.id;
									  $scope.app.type = $scope.ticket.type.k;	
									  $scope.app.deadline =  $filter('date')($scope.app.deadline, 'yyyy:MM:dd HH:mm:ss');
									  var uploadUrl = $scope.base_url + "tickets/response";
									  file = 'none';
								} else if($scope.name == 'video'){
									////console.log($scope.app.embedded_code);
									var uploadUrl = $scope.base_url+"/new_videoApp";
									if($scope.app.hasOwnProperty('embedded_code') && /*typeof*/ file == 'none'){
										////console.log("if");
											var file = "none";
									 }else{
									 	 ////console.log("else");
										 $scope.app.embedded_code ="";
									 }
								} else if($scope.name == 'zip'){
									var uploadUrl = $scope.base_url+"/new_zipApp";
									if($scope.app.hasOwnProperty('external_link') && /*typeof*/ file == 'none'){
											var file = "none";
									 }else{
										 $scope.app.external_link ="";
									 }
								} else if($scope.name == 'text'){
									var uploadUrl = $scope.base_url+"/new_textApp";
									var file = "none";
								} else if($scope.name == 'image_popupwindow' || $scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
									// edit image aap
										if(/*typeof*/ file == 'none'){ // if no image selected
											file = 'none';
										}
										$scope.app.cai = $scope.being_edited_app;
										var uploadUrl = $scope.base_url+"/"+$scope.name;
								}
									//////console.log($scope.app.title);
									//$scope.app.title = $scope.app.title.replace('\'', '\\\'');
                   					$scope.uploadfile(file, uploadUrl,$scope.app ,$scope.name);
						});	
					}

		      };

		      	  $scope.replaceplaceholders = function(data){
			  	////console.log(data);
			  	////console.log($scope.tichtml[0]);
			  		var str = $scope.tichtml[0].content;
					var res = [], m, rx = /{{(.*?)}}/g;
						while ((m=rx.exec(str)) !== null) {
						  res.push(m[1]);
						}
					for(i=0; i < res.length; i++){
						var out = '';
						var leftclosure = '{{';
						var rightclosure = '}}';
									inputconfig = res[i].split(',');
									////console.log(data[inputconfig[1]]);
									////console.log(inputconfig[1]);
									if(!angular.isUndefined(data[inputconfig[1]]) ) {
										if(inputconfig[0] == 'userslist'){
											var name ="";
											for(j = 0; j < ($scope.users).length; j++){
												if($scope.users[j]['id'] ==  data[inputconfig[1]]){
													var name = $scope.users[j]['first_name']+' '+$scope.users[j]['last_name'];
												}
												//out+='<option value="'+scope.users[j]['id']+'">'+scope.users[j]['first_name']+' '+scope.users[j]['last_name']+'</option>';
											}
											str = str.replace(leftclosure+res[i]+rightclosure, name);
										} else if(inputconfig[0] == 'current-user'){
											////console.log(inputconfig);
											str = str.replace(leftclosure+res[i]+rightclosure, $scope.currentuserinfo.first_name );	
										}else if(inputconfig[0] == 'radio'){
											////console.log(inputconfig);
											var fieldtxt = res[i];
												var res1= [], m, radiorx = /\[(.*?)\]/g;
												while ((m=radiorx.exec(fieldtxt)) !== null) {
												  res1.push(m[1]);
												}
												//////console.log(res1);
												var fieldsarr = res1[0].split('|');
												var out ='';
												out +='<div class="form-group"><div class="radio-list">';
												////console.log(fieldsarr);
												for(j = 0; j < (fieldsarr).length; j++){
													////console.log(data[inputconfig[1]]);
													////console.log(fieldsarr[j]);
													if(data[inputconfig[1]] == fieldsarr[j]){
														out+='<label class="col-md-12"><div class="col-md-2 p-t-10"><span class=""><i class="fa fa-check"></i>  '+fieldsarr[j]+'</span></div></label>';	
													} else {
														out+='<label class="col-md-12"><div class="col-md-2 p-t-10"><span class=""><i class="fa fa-times"></i> '+fieldsarr[j]+'</span></div></label>';
													}
												}
												out +='</div></div>';
											str = str.replace(leftclosure+res[i]+rightclosure, out);
										}else if(inputconfig[0] == 'checkbox'){
											var out = '' ;
											var yesno = (data[inputconfig[1]] == true) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';
											//out +='<div class="form-group"><label class="col-md-12"><div class="col-md-2 p-r-0 p-l-0"><input type="checkbox" class="form-control " value="'+inputconfig[1]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'"></div><div class="col-md-2 p-t-10"><span class="">'+inputconfig[3]+'</span></div></label><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
											out+='<label class="col-md-12"><div class="col-md-2 p-t-10"><span class="">'+ yesno +'  '+ inputconfig[3]+'</span></div></label>';	
											str = str.replace(leftclosure+res[i]+rightclosure, out);
											//str = str.replace(leftclosure+res[i]+rightclosure, data[inputconfig[1]] );	
										} else {
											str = str.replace(leftclosure+res[i]+rightclosure, data[inputconfig[1]]);	
										}
									} else {
										if(inputconfig[0] == 'checkbox'){

											var out = '' ;
											//console.log(inputconfig[0]);
											var yesno = '<i class="fa fa-times"></i>';
											//console.log(data[inputconfig[1]]);
											//console.log(inputconfig[3]);
											//out +='<div class="form-group"><label class="col-md-12"><div class="col-md-2 p-r-0 p-l-0"><input type="checkbox" class="form-control " value="'+inputconfig[1]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'"></div><div class="col-md-2 p-t-10"><span class="">'+inputconfig[3]+'</span></div></label><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
											out+='<label class="col-md-12"><div class="col-md-2 p-t-10"><span class="">'+ yesno +'  '+ inputconfig[3]+'</span></div></label>';	
											str = str.replace(leftclosure+res[i]+rightclosure, out);
											//str = str.replace(leftclosure+res[i]+rightclosure, data[inputconfig[1]] );	
										} else {
											str = str.replace(leftclosure+res[i]+rightclosure, 'None' );	
										}
									}
							
					}
					return str;
			  }

		      $scope.isvaliddata = function(app){
		      	var areerrors = false;
		      	////console.log(app);
				if($scope.name =='ticketresponse'){
					if(angular.isUndefined(app.title.trim())){
						////console.log("not filled");
						$scope.errors.title = 'Please Fill Out Title';
						$scope.fields.title = 'default_visible';
						areerrors = true;
					} 

				    if(angular.isUndefined(app.deadline)){
						$scope.errors.deadline = 'Please Select Deadline for the Ticket';
						$scope.fields.deadline = 'default_visible';
						areerrors = true;
						//return false;
					}
					 if(angular.isUndefined(app.type)){
						$scope.errors.type = 'Please Select Deadline for the Ticket';
						$scope.fields.type = 'default_visible';
						areerrors = true;
						//return false;
					}
					if(angular.isUndefined(app.priority)){
						$scope.errors.priority = 'Please Select Deadline for the Ticket';
						$scope.fields.priority = 'default_visible';
						areerrors = true;
						//return false;
					}
					if(angular.isUndefined(app.projectid)){
						$scope.errors.projectid = 'Please Select Project for the Ticket';
						$scope.fields.projectid = 'default_visible';
						areerrors = true;
						//return false;
					}
					//////console.log($scope.hourminformat($scope.app.tasktime));
					if($scope.hourminformat(app.tasktime) == false){
							$scope.errors.tasktime = 'Please Valid Task time i.e 1:00';
							$scope.fields.tasktime = 'default_visible';
							areerrors = true;
						}
					if(angular.isUndefined(app.tasktime)){
						$scope.errors.tasktime = 'Please Add Tasktime for the Ticket';
						$scope.fields.tasktime = 'default_visible';
						areerrors = true;
						//return false;
					}
					if($scope.hourminformat(app.billable_hours) == false){
							$scope.errors.billable_hours = 'Please Valid Task time i.e 1:00';
							$scope.fields.billable_hours = 'default_visible';
							areerrors = true;
						}
					if(angular.isUndefined(app.billable_hours)){
						$scope.errors.billable_hours = 'Please Add Billable Hours for the Ticket';
						$scope.fields.billable_hours = 'default_visible';
						areerrors = true;
						//return false;
					}
					/*if(angular.isUndefined(app.desc)){
						$scope.errors.desc = 'Please add response to  Ticket';
						$scope.fields.desc = 'default_visible';
						areerrors = true;
					}*/
					if($scope.dynamicfields.length > 0){
						//console.log("if");
						$scope.errors.dynamicdes =[];
						$scope.fields.dynamicdes = [];
						angular.forEach($scope.dynamicfields, function(value, key) {
								if(value.required == 'yes'){
									if($scope.dynamicdes[value.name] == '' || angular.isUndefined($scope.dynamicdes[value.name])){
										$scope.errors.dynamicdes[value.name] = 'Field can not be left empty.';
										$scope.dynamicdes.fields[value.name] = 'default_visible';
										areerrors = true;
									}
								}
							});	
					} else if(angular.isUndefined(app.desc)){
						//console.log("else");

						$scope.errors.desc = 'Please enter Description';
						$scope.fields.desc = 'default_visible';
						areerrors = true;
						//return false;
					} else if(app.desc.trim() == ''){
						$scope.errors.desc = 'Please enter Description';
						$scope.fields.desc = 'default_visible';
						areerrors = true;	
				    }
					return (areerrors == false) ? true : false;
				}
				return true
		      }
		     

		         $scope.onTimeSet = function(newDate, oldDate){
		    		  var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
				//var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
				/*v.deadline = $filter('date')(v.deadline, 'yyyy:MM:dd HH:mm:ss','')*/
				var diffDays = (new Date(newDate).getTime() - new Date().getTime());
				////console.log(new Date(newDate))
				////console.log(new Date())
				////console.log(diffDays);
					if(diffDays < 0 ){
					
						return false;
					} else {
						return true;
						
					}	
		      }


					$scope.uploadfile = function(file, uploadUrl,other_data,name){
						 var fd = new FormData();
						 if(file !== 'none'){
								if(name =='image'){
										fd.append('image_app', file);
								} else if(name =='video'){
										fd.append('video_app', file);
								} else if(name == 'zip'){
										fd.append('app_zip', file);
								}else if(name == 'image_popupwindow' || name == 'zip_popupwindow'){
									fd.append('url', file);
								} else if(name == 'video_popupwindow'){
									fd.append('video_app', file);
								}
						 }
							angular.forEach(other_data, function(value, key) {
								fd.append(key, value);
							});
						 $http.post(uploadUrl, fd, {
								transformRequest: angular.identity,
								headers: {'Content-Type': undefined}
						 })
						 .success(function(response){
							 if(response.status == 'success' || response.status == 'updated' ){
								//$scope.aftersuccess(response);
								$scope.submitvia = "button";
								//var Form_data = { ticket_id : $scope.ticket.id , replier_id : $scope.currentuser, response_text: 'Ticket Updated By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+'' , created_at : $filter('date')(new Date() , "medium")   };
								var Form_data = { ticket_id : $scope.ticket.id , replier_id : $scope.currentuser, response_text:  $scope.ticket.desc, ip_address : $scope.ip , created_at : $filter('date')(new Date() , "medium")   };
								var wheretogo = $scope.socketurl + '/addtodescussion';
								var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'addtodescussion');

							 }else if(response.status =='failed'){
								$scope.success_class = '';
								$scope.success_message = "";
								 angular.forEach(response.errors, function(value, key) {
									var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
									var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								 var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
								});
								 $scope.is_disbaled = false;
							}

						 }).error(function(){
						 });
					};
			$scope.common_success_meesage = function (message){
				$scope.success_class ="sucess_mesage"
				$scope.success_message = message;
				$timeout(function(){
							$scope.success_message = "";
							$scope.startFade = "";
							 $scope.success_class = 'success_default';
						}, 3000);
			};
			$scope.aftersuccess = function(answer){
				if(answer.status == 'success'){
					if($scope.name == 'ticket'){
						////console.log("successfully Added!");
						$scope.app ={};
						$scope.app.to_id = {id: $scope.currentuser };
						$scope.app.type = {k: 'task' };
						$scope.app.priority = {k: 'normal' };
						$scope.app.projectid = {id: '0' };
						$scope.success.message = 'Ticket Added Successfully!'
						$scope.fields.successwrapper = 'default_visible';

					} else if($scope.name == 'ticketresponse' ){
						////console.log("asdfasd");
						//var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket has been Responded '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , created_at : $filter('date')(new Date() , "medium")   };	
						//var wheretogo = $scope.socketurl + '/addtodescussion';
						//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'addtodescussion');
						//var Form_data = { from : $scope.currentuser ,ticket_type : 'response', logtext : 'Your ticket has been responded ', type: 'information', to_id : $scope.app.from_id.toString() ,'entity_id' : 1, created_at : $filter('date')(new Date() , "medium",'+0530')   };
						//var wheretogo = $scope.socketurl + '/logactivity';
						//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'logactivity');
						$scope.app ={};
						$scope.app.to_id = {id: $scope.currentuser };
						$scope.app.type = {k: 'task' };
						$scope.app.priority = {k: 'normal' };
						$scope.app.projectid = {id: '0' };
						////console.log($scope.app);
						$scope.success.message = 'Ticket Responded Successfully!'
						$scope.fields.successwrapper = 'default_visible';
						

					}

				} else if(answer.status == 'updated'){
					if($scope.name == 'image_popupwindow'){
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = "";
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;

					}else if($scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
						taskId = $scope.selectedIndex;
						var item = $scope.get_names(taskId);
						$scope.get_apps(taskId, item[2] );	
						$scope.common_success_meesage("App Edited Successfully!");
					}
					//$scope.being_edited_app_index

				}
				/*
				not needed for now	
				$scope.is_logo_hidden = false;
				$scope.is_preview_hidden = true;
				$.fancybox.close();
				$scope.app ={};
				$scope.myFile ='none'*/
			};
		  $scope.hourminformat = function(val){
		  	////console.log(val);
		    var regexp = /^([0-9]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
		    //var regexp = /^([01]\d|2[0-3]):?([0-5]\d)$/;
		    ////console.log(/^[a-zA-Z]+$/.test(val));
		    if (/^[a-zA-Z]+$/.test(val) == true)
				{
				   return false;
				}
		          return regexp.test(val);
		  };
		  $scope.closetheerror = function(field){
		  		$scope.fields[field]="default_hidden"; 
		  	 }
		  	 $scope.closedynamicdeserr = function(field){
						////console.log(field);
				  		$scope.dynamicdes.fields[field]="default_hidden"; 
			}

				$scope.commonPostFunc = function(wheretogo,FormData,task){
									$http({
									method: 'POST',
									url: wheretogo,
									async : true,
									data : FormData
								}).then(function successCallback(response) {
									if(task =='autosave'){
										if(response.data.status =='failed'){
											$scope.autosave_status = "Data not saved!";
											angular.forEach(response.data.errors, function(value, key) {
											var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
											var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
											var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
											});

										}else{
											$scope.autosave_status = "saved!";
										}										
										 $timeout(function(){
											 $scope.autosave_status = "";
										 },500);
									}  else if(task == 'drag_ticket'){
										SweetAlert.swal("Done!");
									}else if(task == 'ticket'){
										////console.log("success");
									} else if(task == 'addtodescussion'){
										
										
										$scope.infomsgstart('Saved!','info');
										if($scope.submitvia == 'button'){
											if($scope.currentuser == $scope.ticket.from_id ){
											var notificationto = $scope.ticket.to_id
											} else {
												var notificationto = $scope.ticket.from_id
											}
											console.log(notificationto);
											var Form_data = { from : $scope.currentuser ,ticket_type : 'response', logtext : 'Ticket('+ $scope.ticket.title +') Updated By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+''  , type: 'information', to_id : parseInt(notificationto) ,'entity_id' : 1, entity_value : $scope.ticket.id , ip_address : $scope.ip , created_at : $filter('date')(new Date() , "medium",'+0530')   };	
										} else {
											if($scope.currentuser == $scope.ticket.from_id ){
												var notificationto = $scope.ticket.to_id.id;
											} else {
												var notificationto = $scope.ticket.from_id;
											}
											console.log(notificationto);
											var Form_data = { from : $scope.currentuser ,ticket_type : 'response', logtext : FormData.response_text+'('+ $scope.ticket.title +')' , type: 'information', to_id : parseInt(notificationto) ,'entity_id' : 1, entity_value : $scope.ticket.id , ip_address : $scope.ip, created_at : $filter('date')(new Date() , "medium",'+0530')   };
										}
										
										var wheretogo = $scope.socketurl + '/logactivity';
										var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'logactivity');		
										 $timeout(function(){
										 	$scope.infomsgclose('info');
										 	
											

						 				},200);
									} else if(task == 'getticdescussion'){
											$scope.responders = response.data.resultData;
											var totalresponses = $scope.responders.length;

										    if(totalresponses == 0){
										      $scope.amilastreplier = ($scope.ticket.to_id == parseInt($scope.currentuser)) ? 'yes' : 'no';
											} else {

												angular.forEach($scope.responders, function(v, k) {
													if(parseInt(v.replier_id) == parseInt($scope.currentuser)) {
														 $scope.amilastreplier = 'yes';
													}
													//$scope.projectid.push(v);
												});
											  //$scope.amilastreplier = (parseInt($scope.responders[totalresponses -1].replier_id) == parseInt($scope.currentuser)) ? 'yes' : 'no';	
											}
											
											
											if($scope.ticket.type.k == 'Scheduled' && $scope.tagdata.id > 0 && $scope.amilastreplier == 'no' && $scope.ticket.from_id !== parseInt($scope.currentuser) ){
													 	var tichtml =  angular.fromJson($scope.tagdata.on_ticket);
													  	if(tichtml.on_ticket == 'yes'){
													  		$scope.tichtml.push(tichtml);
													  	}
											} else if($scope.ticket.type.k == 'To-Do' && $scope.tagdata.id > 0 && $scope.amilastreplier == 'no' && $scope.ticket.from_id !== parseInt($scope.currentuser) && $scope.ticket.referencing_ticket > 0){
													var tichtml =  angular.fromJson($scope.tagdata.on_ticket);
													  	if(tichtml.on_ticket == 'yes'){
													  		$scope.tichtml.push(tichtml);
													  	}
											}
											if(angular.isUndefined($scope.mandatory.eid) == false){
												if($scope.mandatory.last_replier !== parseInt($scope.currentuser)){
													$scope.infomsgstart("Please respond to Mandatory ticket(s) first","info");
												/*	if($scope.mandatory.front_id !== parseInt($scope.currentuser)){

													}*/
												}
											} else {
												$scope.infomsgclose('info');
											}
											$scope.issubmitenable = true;
										
									} else if (task == 'instantsave'){
										$scope.infomsgstart('Saved!','info');
										var savetolog = 'no';
										if(FormData.field == 'to_id'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Forwarded To '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'type'){
											var txt = (FormData.fielddata == 'Scheduled') ? '('+ FormData.scheduletypeface +')' : "" ;
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Type Changed To '+ FormData.toname + txt +'   By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+'' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'priority'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Priority Changed To '+ FormData.fielddata +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip , created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'status'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Status Changed To '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											$scope.status = $scope.statusddvalues($scope.ticket.status.k, $scope.ticket.type.k, $scope.ticket.from_id);
											savetolog = 'yes';
										} else if(FormData.field == 'deadline'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Deadline Changed To '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'tasktime'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Task Time Changed To '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'billable_hours'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Billabe Hours Changed To '+ FormData.toname +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'title'){
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Title Changed To '+ FormData.fielddata +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'yes';
										} else if(FormData.field == 'entity_id'){
											//console.log($scope.entityvaluemodel);
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Entity Value Changed To '+ $scope.entityvaluemodel.v +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											$scope.selectedprojectface = $scope.entityvaluemodel.v;
											savetolog = 'yes';
										} else if(FormData.field == 'reminderid'){
											//console.log(FormData.field);
											var Form_data = { ticket_id : $scope.ticket.id , replier_id : 0, response_text: 'Ticket Title Changed To '+ FormData.fielddata +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip ,created_at : $filter('date')(new Date() , "medium")   };	
											savetolog = 'no';
											SweetAlert.swal("Reminder Sent!");
											$scope.infomsgclose('info');
										}

										if(savetolog == 'yes'){
											$scope.isfirsttime ='no';
											$scope.infomsgstart('Saving to Logs...','info'); 
											var wheretogo = $scope.socketurl + '/addtodescussion';
											var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'addtodescussion');	
										}
										
									} else if (task == 'logactivity'){
										if($scope.submitvia == "button" && response.data.response == 'success'){
											//$window.location.href = $scope.base_url + 'tickets/index';
										}
										$scope.infomsgstart('Re-loading Discussions...','info');
										//var Form_data = { ticket_id : $scope.ticket.id };
										//var wheretogo = $scope.socketurl + '/getticdescussion';
										//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'getticdescussion');
										//$scope.resetsummer = ($scope.resetsummer == 'yes') ? 'no' : 'yes';
										$scope.resetsummer = !$scope.resetsummer;
										$scope.ispriorityenabled = true;
										$scope.get_data(); // calling function onload

									} else if (task == 'ticindex'){
										if(response.data.status =='success' ){
											SweetAlert.swal("Reminder Sent!");
										}
									}

										$scope.tgl_cntr_loader = "make_hidden";
									}, function errorCallback(response) {
							});
					}
				

			$scope.validatetitle = function(data){
				//console.log(data);
				if (data == '') {
				return "Please fill asdfasdfasout title";
				} else {
				  var Form_data = { ticket_id : $scope.origin , field : 'title', fielddata : data , toname : $scope.currentuserData.first_name+' '+$scope.currentuserData.last_name };
				  var wheretogo = $scope.base_url + 'tickets/instantsave';
				  var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');	
				}
			}
			

				// sortable options for sections needed to be called differently because these are different
				$scope.sortableOptions = {
					connectWith: ".alltickets",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						/*$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));*/
						$scope.beforedrag = $scope.t_data;
						ui.item.startPos = ui.item.index();
						$scope.section_drop_start_index = ui.item.index();
						////console.log($scope.section_drop_start_index);
						$scope.currently_dragging = "sections";
						ui.item.hide();
						var text = $scope.names[ui.item.startPos]['title'];
						ui.placeholder.html("<td colspan='8' class='hidden-xs'><span>"+text+"</span></td>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");
					},
					update:  function(event, ui){
						$scope.section_drop_end_index = ui.item.sortable.dropindex;
						////console.log($scope.section_drop_end_index);
						var desired_html = '<div id="openModalss" style="height:170px;width:300px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
						desired_width='sm';
						$scope.trigger_sweet_confirmation('Are You Sure To Drop?',"Drop It",'warning','drag_ticket','yes');
					},

				};
			$scope.trigger_sweet_confirmation	= function(thetitle,thebuttontext,thetype,from_,runajax){
					SweetAlert.swal({
						   title: /*"Are you sure?"*/ thetitle,
						   text: 'After confirmation changes will be made instantly',
						   type: thetype,
						   showCancelButton: true,
						   confirmButtonColor: "#DD6B55",
						   confirmButtonText: thebuttontext,
						   closeOnConfirm: false}, 
						function(isConfirm){ 
							//console.log(isConfirm);
							if(isConfirm){
								  if(runajax == 'yes'){
							   		if(from_ == 'drag_ticket'){
							   			if(isConfirm == true){
							   				if($scope.section_drop_start_index < $scope.section_drop_end_index ){
											////console.log("sorting up to down");
											var afterorbefore = 'after';
											// sorting up to down
											//$scope.names.splice($scope.section_drop_end_index + 1, 0, $scope.names[$scope.section_drop_start_index]);
											//$scope.names.splice($scope.section_drop_start_index, 1);
											//$scope.posttree("none");
										} else {
											var afterorbefore = 'before';
											// sroting down to up
											//$scope.names.splice($scope.section_drop_end_index , 0, $scope.names[$scope.section_drop_start_index]);
											//$scope.names.splice($scope.section_drop_start_index + 1, 1);
											//$scope.posttree("none");
										}
											var other_ticket = $scope.names[$scope.section_drop_end_index]['id'];
											var dropped_ticket = $scope.names[$scope.section_drop_start_index]['id'];
											Form_data = {afterorbefore:afterorbefore,other_ticket:other_ticket,dropped_ticket:dropped_ticket};
											////console.log(Form_data);
											var wheretogo ='reordertickets';
											$scope.commonPostFunc(wheretogo,Form_data,'drag_ticket'); // sending a post request
							   			}
										
							   		}else if(from_ == 'ticindex'){
										//var wheretogo = $scope.base_url +'tickets/instantsave';
										//Form_data = {reminderid:$scope.origin}
										//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
										var Form_data = { ticket_id : $scope.origin , field : 'reminderid', fielddata : $scope.origin};
										var wheretogo = $scope.base_url + 'tickets/instantsave';
										var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
							   		}
									
							   }	
							}
						 
						});
				};

			$scope.showmenu = function(){
				$scope.showDetails = !$scope.showDetails;
			}
			$scope.hidemenu = function(){
				$scope.showDetails = false;
			}
			$scope.gototop = function(){
				//angular.element(document.getElementsByTagName('body')).animate({ scrollTop: 0 }, 200); /* taking user to the top of the page*/
				$location.hash('wrapper'); // taking pointer to the top passed id
				$anchorScroll(); // taking pointer to the top passed id
				$scope.make_gtp_vi ="";
			}
			$scope.showicon = function(field){
				////console.log(field);
				$scope.show[field] = 'none';
			}
			$scope.hideicon = function(field){
				////console.log(field);
				$scope.show[field] = 'default_hidden';
			}
			/*$scope.validatetitle = function(data){
				if (data == '') {
				return "Please fill out title";
				}
			}*/
			
				// ended here
	});

    } );
		// directive is used to display apps when get_apps is called.
		app.directive('apipeline', function($compile,$sce,$timeout) {

			/**/
	return {
		replace: true,
		link: function(scope, element,attrs) {	
			var el = angular.element('<div class="media m-b-30 p-t-20">');
			var imageurl =   (scope.appdata.image_url !== '') ? (scope.appdata.replier_id !== 0 ) ?  scope.base_url + 'img/employee_image/' + scope.appdata.image_url : scope.base_url + 'images/' + scope.appdata.image_url :  scope.base_url + 'images/shared/no_image.png';
			var responsetxt = scope.appdata.response_text;
			var ipaddress = (scope.amiadmin == 'yes') ? scope.appdata.ip_address : "" ;
			//$compile(responsetxt)(scope);
			el.append('<a class="pull-left lrtxt" href="#"> <img class="media-object thumb-sm img-circle" ng-src="'+imageurl+'" alt=""> </a><div class="media-body"><span class="media-meta pull-right"><p>&^&appdata.created_at&^&</p><p class="media-meta ipaddrtxt pull-right">'+ipaddress+'</p></span><h4 class="text-danger m-0 m-t-20">&^&appdata.replier_name&^&</h4> </div></div></div><div class="col-md-12"><p>'+responsetxt+'</p></div>');
			$compile(el)(scope);
			element.append(el);


		}
	}
});

app.directive('shceduleafter', function($compile,$sce,$timeout) {
	return {
		replace: true,
		link: function(scope, element,attrs) {	

			var el = angular.element('<a href="#" editable-text="ticket.after" onbeforesave="updateafter($data)"  title="Click to Edit">');
			el.append(scope.sdata.after);
			$compile(el)(scope);
			element.append(el);
		}
	}
});	

app.directive('attachedtags', function($compile,$sce,$timeout) {
	  return {
	  	link: function(scope, element,attrs) {	
	  		scope.$watch('showtag', function (val) {
			                  if (val){
			                  	//console.log(val);
			                  	//console.log(angular.isUndefined(scope.tagdata.id));
							  		//console.log(scope.tagdata);
							  		if(!angular.isUndefined(scope.tagdata.id)){
							  			//console.log("Entered");
							  			var el = angular.element('<div class=" col-md-12">');
							  			//var el = angular.element();
							  			el.append('<span class="pull-right" title="Attached Tag"><i class="fa fa-tags "></i> : '+scope.tagdata.name+'</span>');
										$compile(el)(scope);
										element.append(el);
							  		}
						      }
							})
	  		
		}
	  };
});
		

/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('asText', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
											link: scope.autosave(event.which,event,scope);
											return false;


            });

        };
    });
app.animation('.success_message_here', function() {
  return {
    enter: function(element, done) {
      //element.css('display', 'none');
      //element.fadeIn(5000, done);
      return function() {
        element.stop();
      }
    },
    leave: function(element, done) {
      element.fadeOut(5000, done)
      return function() {
        element.stop();
      }
    }
  }
});
app.directive('resfs', function() {
	return {
    template: '<a id="showData" class="callMe showData" href={{url("/")}} ng-click="toggle_sidebar();$event.preventDefault();$event.stopPropagation()" ng-class="hideicon_class"><i class="fa fa-arrow-circle-left arrow" ng-hide ="restore_fs"></i></a>'
  };

});
app.directive('compressicon', function() {
	return {
    template: '<a href="#" class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="toggleFullScreen();$event.preventDefault();$event.stopPropagation()" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"><i class="fa fa-compress" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"></i></a>'
  };

});
app.directive('hidemenu', function ($window) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var el = element[0];
      // if there is clicked anywhere on the document that will be triggered.
      angular.element($window).bind('click', function(){
      		scope.showDetails = false;
      		 scope.$apply();
      });
    } 

  };

});
app.directive('enteronbutton', function() {
        return function(scope, element, attrs) {
            element.on("keydown", function(event) {

							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.handleenterkey(event.which,event,scope);
											return false;
									}
					} 
            });

        };
    });
		// add new app

app.directive('prevententer', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.new_app_submit();
											return false;
									}
							} 

            });

        };
    });

app.directive('tagcontent', function($compile,$sce,$timeout) {

	return {
		replace: true,
		link: function(scope, element,attrs) {	
				scope.hidetextdes = true;
				$('#desc').hide();
			var el = angular.element('<div>');
			var str = scope.deshtml.content;
			var res = [], m, rx = /{{(.*?)}}/g;
				while ((m=rx.exec(str)) !== null) {
				  res.push(m[1]);
				}
			//////console.log(res);
			for(i=0; i < res.length; i++){
				var out = '';
				var leftclosure = '{{';
				var rightclosure = '}}';
							inputconfig = res[i].split(',');
							scope.dynamicfields.push({type : inputconfig[0], name : inputconfig[1] , required : inputconfig[2]});
							var fieldname = "'"+inputconfig[1]+"'";
						if(inputconfig[0] == 'input'){
							out +=' <div class="form-group"><input type="text" class="form-control" ng-model="dynamicdes.'+inputconfig[1]+'"><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						} else if(inputconfig[0] == 'date'){
							out +='<div class="form-group"><input type="text" class="form-control dateinput" ng-model="dynamicdes.'+inputconfig[1]+'"  ><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						}else if(inputconfig[0] == 'date-time'){
							out +='<div class="form-group"><input type="text" class="form-control date-time" ng-model="dynamicdes.'+inputconfig[1]+'"  ><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						}else if(inputconfig[0] == 'textarea'){
							out +='<div class="form-group"><textarea class="form-control textarea" ng-model="dynamicdes.'+inputconfig[1]+'"  ></textarea><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						}else if(inputconfig[0] == 'radio'){
							var fieldtxt = res[i];
							var res1= [], m, radiorx = /\[(.*?)\]/g;
							while ((m=radiorx.exec(fieldtxt)) !== null) {
							  res1.push(m[1]);
							}
							//////console.log(res1);
							var fieldsarr = res1[0].split('|');
							out +='<div class="form-group"><div class="radio-list">';
							for(j = 0; j < (fieldsarr).length; j++){
								if(j== 0){

									out+='<label class="radio-inline "><input type="radio" value="'+fieldsarr[j]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'"><span class="">'+fieldsarr[j]+'</span></label><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div>';	
								} else {
									out+='<label class="radio-inline"><input type="radio"  value="'+fieldsarr[j]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'"><span class="">'+fieldsarr[j]+'</span></label><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div>';
								}
								
							}
							out +='</div></div>';
							scope.dynamicdes[inputconfig[1]] = fieldsarr[0];
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						}else if(inputconfig[0] == 'checkbox'){
							//out +='<div class="form-group"><div class="checkbox">                                     <input type="checkbox" class="form-control " value="'+inputconfig[1]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'" id ="check'+inputconfig[1]+'" /><label for="check'+inputconfig[1]+'" > '+inputconfig[3]+' </label></div><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							out +='<div class="form-group"><div class="checkbox"><input type="checkbox" class="form-control " value="'+inputconfig[1]+'" ng-model="dynamicdes.'+inputconfig[1]+'" name="'+inputconfig[1]+'" id ="check'+inputconfig[1]+'" ><label class="" for="check'+inputconfig[1]+'" >'+inputconfig[3]+'</label></div><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div></div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						} else if(inputconfig[0] == 'userslist'){
							out+='<select class="form-control" ng-model="dynamicdes.'+inputconfig[1]+'" >';
							for(j = 0; j < (scope.users).length; j++){
								out+='<option value="'+scope.users[j]['id']+'">'+scope.users[j]['first_name']+' '+scope.users[j]['last_name']+'</option>';
							}
							out+='</select><div class="alert alert-danger alert-dismissable default_hidden" ng-class="dynamicdes.fields.'+inputconfig[1]+'"><button type="button" class="close" ng-click ="closedynamicdeserr('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.dynamicdes.'+inputconfig[1]+'&^&</div>';
							str = str.replace(leftclosure+res[i]+rightclosure, out);
						}else if(inputconfig[0] == 'current-user'){
							////console.log(scope.currentuserinfo.first_name);
							str = str.replace(leftclosure+res[i]+rightclosure, scope.currentuserinfo.first_name );
						}
			}
			var ht = str;
		    el.append('<div>'+ht+'</div>');
		    $compile(el)(scope);
			element.append(el);
			$('.checkbox').find('input').css('opacity',0);
			setTimeout(function(){
				//scope.hidetextdess = !scope.hidetextdess;
				//scope.calldatetimepicker();
				if( $('.dynamichtml').find('input').has('.dateinput')){
					//$('#a0').find('.dateinput').datepicker( "destroy" );
					//////console.log("Date");
				 $('.dynamichtml').find('.dateinput').datepicker({
              		 	dateFormat: 'MM dd, yy',
              		 	defaultDate: new Date(),
              		 	showOtherMonths: true,
      					selectOtherMonths: true,
              		 	 onChangeDateTime:function(dp,$input){
						  }
              		 });
				}
				if( $('.dynamichtml').find('input').has('.date-time')){
					//$('#a0').find('.date-time').datetimepicker( "destroy" );
				 $('.dynamichtml').find('.date-time').datetimepicker({
              		 	dateFormat: 'MM dd, yy',
              		 	defaultDate: new Date(),
              		 	showOtherMonths: true,
      					selectOtherMonths: true,
              		 	 onChangeDateTime:function(dp,$input){
						  }
              		 });
				}
				scope.$apply();
			},500);
		}
	}
});

app.directive('scrolly', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var raw = angular.element(document.body);
            
            var body = document.body,
				html = document.documentElement;
            $timeout(function(){
            $document.bind('scroll', function () {
            	/*
            	////console.log("scroplling");*/
			    scroll_top = Math.max( body.scrollTop, html.scrollTop);
			  //  ////console.log("scroll_top"+ scroll_top);
			    height = Math.max( body.scrollHeight, html.scrollHeight);

				var offset = Math.min( body.offsetHeight, html.offsetHeight,$window.pageYOffset );
                if(scroll_top + offset > 1200){
                	scope.make_gtp_vi = 'make_less_opacity';
                	scope.scrolltotop = true;
                	scope.shouldgotop = true;
                	if (scroll_top + offset >= height ) {
                }
                 scope.$apply(attrs.scrolly);
           	 	}else if(scroll_top + offset < 1200){
                	scope.make_gtp_vi = '';	
                	scope.shouldgotop = false;
                	scope.scrolltotop = false;
                	 scope.$apply(attrs.scrolly);
                }

            	});
           },500);
        }
    };
});
app.directive('myCustomer', function() {
  return {
    template: '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>&^&errors.message</div>'
  };
});
app.filter('trim', function () {
    return function(value) {
        if(!angular.isString(value)) {
            return value;
        }  
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
});

//}