/*var myData = localStorage.getItem("tree_json");
*//*localStorage.setItem("t", $('input[name="_token"]').val());*/
//if(localStorage.getItem("courseidentity").length > 0 ){
	//$('.course_id').val(''); //
//}
//if (typeof myData != 'undefined' && myData.length > 0 ) {
	var app = angular.module('createTree', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable','ngMaterial',/*'lr.upload','ckeditor',*//*'FBAngular',*//*'ngFileUpload',*//*'blockUI',*/'oitozero.ngSweetAlert','xeditable','ui.bootstrap.datetimepicker']) ;
	app.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('&^&');
	    $interpolateProvider.endSymbol('&^&');
	  });
	/*var Mydata =   jQuery.parseJSON( myData );*/
/**/
/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});
app.directive('handleKey', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 || event.which === 27) {
											link: scope.handlekeyPress(event.which,event,scope);
											return false;
									}
							} else if(event.type =="blur"){
								link: scope.focusout(event.which,scope);
							}

            });

        };
    });
		/*
		NOTES:Directive name :fileModel
		Usage: for handling file uploding in the scope

		*/
	app.directive('fileModel', ['$parse', function ($parse) {
	            return {
	               restrict: 'A',
	               link: function(scope, element, attrs) {
	                  var model = $parse(attrs.fileModel);
	                  var modelSetter = model.assign;

	                  element.bind('change', function(e){
	                     scope.$apply(function(){
	                        modelSetter(scope, element[0].files[0]);
	                        scope.preview_file = (e.srcElement || e.target).files[0];
	                        if(scope.name =='image' ||  scope.name =='image_popupwindow' || scope.name =='video_popupwindow' || scope.name =='video')
	                        scope.preview_(scope.preview_file);
	                     });
	                  });
	               }
	            };
	         }]);
/*	app.config(function(blockUIConfig) {
  // Tell blockUI not to mark the body element as the main block scope.
  blockUIConfig.autoInjectBodyBlock = false;  
});*/



		/*
		Controller Name : createCtr
		Usage: All the logic is written here
		*/
	app.controller('createCtr', function($scope,$filter,$compile,$window,$http,$timeout,$uibModal, $log, $document,$mdDialog /*,upload,Fullscreen,Upload*/,$q, $anchorScroll, $location,/*blockUI,*/SweetAlert) {
		// $window.
	$scope.allapps =[];
	$scope.t_data = [];
	$scope.names =[];
	$scope.sec_container =[];
	$scope.chap_container =[];
	$scope.top_container =[];
	$scope.loop_index =0;
	$scope.expanded= [];
	$scope.search_dd=[];
	$scope.san_= "";
	$scope.success_class="";
	$scope.hide_toggle_sidebar =true;
	$scope.sidebar_class ="success_default";
	$scope.hideicon_class ="";
	$scope.hide_controls =false;


	$scope.complete_data ={};
	$scope.is_valid = "yes";
	$scope.todelete = ""; // setting default value
	$scope.delete_status = ""; // setting default value
	$scope.beforesort ={};
	$scope.activeMenu = "";
	$scope.activeMenu1 = "";
	$scope.activeMenu2 = "";
	$scope.section_bc = "";
	$scope.chapter_bc = "";
	$scope.topic_bc = "";
	$scope.content = "";
	$scope.selectedIndex= "";
//	$scope.textapp={};  may be not being used
	//$scope.allapps= $scope.apps.split(","); // preparing array for the apps for the dropdown to add new apps.
	$scope.title ="";
	$scope.app={};
	$scope.embedded_code = "";
	$scope.external_link = "";
	$scope.apps_of_selected_topic = {}; // we kept variable that long to make it understood for later
	$scope.is_image_fullscreen = "";  // to check wether image is allowed in full screen or not
	$scope.is_video_fullscreen = "";  // to check wether video is allowed in full screen or not
	$scope.is_zip_fullscreen = "";  // to check wether zip is allowed in full screen or not
	$scope.is_grid_fullscreen = "";  // to check wether grid is allowed in full screen or not
	$scope.is_text_fullscreen = "";  // to check wether text is allowed in full screen or not
	$scope.being_edited_app = ""; // url eligible encrypted id of the app
	$scope.being_edited_app_index = ''; // index of the apps in the apps array of a particular topic
	$scope.typingTimer =""; // in autosave when we start typing timer is set
	$scope.autosave_status =""; // to show the status while autosaving texts.
	$scope.selected_sec_index =""; // when we click on + icon of section to expand tree or when we click on name of the section index is recored
	$scope.selected_chap_index =""; // when we click on + icon of chapter to expand tree or when we click on name of the chapter index is recored
	$scope.app_drop_start_index =""; // when we drag an app index is recored here
	$scope.app_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.section_drop_start_index =""; // when we drag an app index is recored here
	$scope.section_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.chapter_drop_start_index =""; // when we drag an app index is recored here
	$scope.chapter_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.search= {};
	$scope.apps_through = '';
	$scope.total_apps =0;
	$scope.currently_searched_data =[];
	$scope.attach_index =""; // when user click on attach app icon index is recorded
	$scope.de_attach_index = "";
	$scope.pagination_status="not_triggered";
	$scope.no_apps=false;
	$scope.success_message ="";
	$scope.startFade ="";
	$scope.isFullscreen = false; // all the apps of selected course will be stored in this topic.
	$scope.hide_all = false;
	$scope.old_edit_value ="";
	$scope.edit_status ="";
	$scope.tree_edit_index ="";
	$scope.is_logo_hidden = false;
	$scope.is_preview_hidden = true;
	$scope.chapter_name = "";
	$scope.restore_fs = true;
	$scope.compress_icon_hidden = true;
	$scope.compress_icon_class = "";
	$scope.compress_highlight_hidden = true;
	$scope.compress_highlight_class ="";
	$scope.fs_through = "";
	$scope.call_edit_after ="";
	$scope.showDetails = false;
	$scope.isclickenable = true;
	$scope.shouldgotop = false;
	$scope.expand_chapter_index = [];
	$scope.expand_topic_index =[];
	$scope.fa_class=[];
	$scope.fa_class_=[];
	$scope.selected_top_index =[];
	$scope.defer = $q.defer();
	$scope.show_tree = 'treereplace_shown';
	$scope.activemenus = [];
	$scope.make_gtp_vi = "";
	$scope.new_apps_class ="treereplace_shown";
	$scope.is_disbaled = false;
	$scope.myFile = 'none';
	$scope.tgl_cntr_loader = "make_hidden";
	$scope.userID ="";
	$scope.beforedrag = [];
	$scope.active = [];
	$scope.count =[];
	$scope.currenttype = "";
	$scope.reminderid = "";
	$scope.pageno = 1;
	$scope.prebtnclass = "none";
	$scope.nxtbtnclass = "none";
	$scope.totalpages = 0;
	$scope.tiggerprev = true;
	$scope.tiggernxt = true;
	$scope.totalrecords = 0;
	$scope.currentrecodsfrom = 0;
	$scope.currentrecodstill = 0;
	$scope.check_it = 0;
	$scope.filter = "none"
	$scope.currentdate = new Date();
	$scope.success = [];
	$scope.fields =[];
	$scope.today =[];
	$scope.startdate ="none";
	$scope.enddate ="none";
	$scope.allusers = [];
	$scope.user = [];
	$scope.selecteduserid = 'none';
	$scope.errors = [];

	$document.ready(function(){
			// sending a request to get topic content inside the scope.
			// local storage was not taking updated json
		
			$scope.get_data = function(type,check_it,filter,ticket_id,reset_pages,pagetype,startdate,enddate,userid){
						$scope.fields.errorswrapper = 'default_hidden';
						$scope.norecordsmsg ="default_hidden";
						$scope.fields.successwrapper = 'default_visible';
						$scope.success.message = 'Loading...';
						$scope.autosave_status ='Loading Content...';
						(type == 'selected') ? "" : $scope.currenttype = type;
						(check_it == 'selected') ? "" : $scope.check_it = check_it;
						(filter == 'selected') ? "" : $scope.filter = filter;
						$scope.pageno = (reset_pages == 'yes')  ? 1 : (reset_pages == 'selected') ? $scope.pageno :  $scope.pageno + 1;
						$scope.currenttype = ($scope.currenttype == 'none') ? 'all' : $scope.currenttype;
						/*var sdate = (startdate == 'selected') ? $scope.startdate : startdate ;
						var edate = (enddate == 'selected') ? $scope.enddate : enddate ;*/
						(startdate == 'selected') ? "" : $scope.startdate = startdate ;
						(enddate == 'selected') ? "" : $scope.enddate = enddate ;
						(userid == 'selected')? "" : $scope.selecteduserid = userid;
						/*$scope.startdate = (tofromsearch == 'none') ? 'none' : $filter('date')($scope.dateRangeStart, "medium",'+0530') ;
						$scope.enddate = (tofromsearch == 'none') ? 'none' : $filter('date')($scope.dateRangeEnd, "medium",'+0530');*/
						
						console.log($scope.startdate);
						$http({
						method: 'POST',
						url: 'reportdata',
						async : true,
						data : { 	
									originidentity : $scope.originidentity ,
									key : ($scope.currenttype == 'all') ? 'none' : $scope.currenttype , 
									check_it: $scope.check_it,
									filter:$scope.
									filter,ticket_id : ticket_id, 
									pageno : $scope.pageno,
									pagetype:pagetype,
									sdate : $scope.startdate,
									edate:$scope.enddate,
									userid : $scope.selecteduserid
						      },
					}).then(function successCallback(response) {
							if(response.data.status == 'success'){
								//console.log(response.data.resultData.length);

							  $scope.names = response.data.resultData;
							  $scope.userID = response.data.userID;
							  if(type == 'none'){
							  	$scope.active =[]
							  	$scope.active.all = 'active2';
							  } else if(type == 'selected'){
							  	$scope.active =[]
							  	$scope.active[$scope.currenttype] = 'active2';
							  }else{
							  	$scope.active =[]
							  	$scope.active[type] = 'active2';
							  }
							  $scope.count.opened = response.data.open;
							  $scope.count.closed = response.data.closed;
							  $scope.count.inprogress = response.data.inprogress;
							  $scope.count.expired = response.data.expired;
							  $scope.count.all = response.data.open + response.data.closed + response.data.inprogress + response.data.expired;
							  $scope.pageno = response.data.pageno;
							  $scope.totalpages = response.data.totalpages;
							  $scope.currentrecodsfrom = response.data.currentrecodsfrom;
							  $scope.prebtnclass = (response.data.currentrecodsfrom == 1 ) ?  'disabled' : 'none';
							  $scope.tiggerprev = (response.data.currentrecodsfrom == 1 ) ?  false : true;
							  $scope.currentpage = response.data.currentpage;
							  $scope.totalrecords = response.data.totalrecords;
							  $scope.tiggernxt = ($scope.totalpages ==  $scope.pageno) ?  false : true;
							  $scope.nxtbtnclass = ($scope.totalpages ==  $scope.pageno) ?  'disabled' : 'none';
							  $scope.currentrecodstill = ($scope.totalpages ==  $scope.currentpage) ? response.data.totalrecords : response.data.currentrecodsfrom + 19;
							  $scope.process();
							  $scope.today = response.data.today;
							  $scope.allusers = response.data.users;
							  $scope.user.selected = response.data.users[0]['id']; // dropdown will show first user by defult selected
							  if(response.data.resultData.length ==0){
							  	$scope.norecordsmsg ="default_visible";
							  }
							}
							/*var js_data = JSON.parse(response.data[0]);
							$scope.names = JSON.parse(response.data[0]);
							$scope.process();
							$scope.allapps = JSON.parse(response.data[1]); // setting json coming from DB to the tree for iteration
							$scope.search_dd =response.data.search_dd;*/

						}, function errorCallback(response) {
							//console.log(response.status);
							if(response.status > 299){
								// some error occurred
								$scope.fields.successwrapper = 'default_hidden';
								$scope.errors.message = 'Something went wrong!. Please refresh page and try again. If still facing same issue, Contact Support team';
								$scope.fields.errorswrapper = 'default_visible';
							}
						}).then(function () {
					
					/*angular.forEach($scope.allapps, function(value, key) {
					 (value.id == 1 ? $scope.is_image_fullscreen = value.is_fullscreen :'' );
					 (value.id == 2 ? $scope.is_video_fullscreen = value.is_fullscreen :'' );
					 (value.id == 3 ? $scope.is_table_fullscreen = value.is_fullscreen :'' );
					 (value.id == 4 ? $scope.is_zip_fullscreen = value.is_fullscreen :'' );
					 (value.id == 5 ? $scope.is_text_fullscreen = value.is_fullscreen :'' );
					});*/
				});
			}
			$scope.get_data('none',1,'none','none','yes','none','none','none','none'); // calling function onload
			$scope.process = function(){
					$scope.t_data =[];
					angular.forEach($scope.names, function(v, k) {
						var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
						//var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
						/*v.deadline = $filter('date')(v.deadline, 'yyyy:MM:dd HH:mm:ss','')*/
						var diffDays = (new Date(v.deadline).getTime() - new Date($scope.today).getTime());
						
						if(diffDays < 0 ){
							v.isexpired = 'yes';	
						} else {
							v.isexpired = 'no';	
						}
						$scope.t_data.push(v);
						//console.log(v);
					});
				$scope.autosave_status ='';
				
				//$timeout(function() {
				//blockUI.start("Loading...");
				//$scope.loaderclass = 'default_hidden';
					//$timeout(function() {
					//blockUI.stop();
					$scope.fields.successwrapper = 'default_hidden';
					//}, 2000);

				//},2000);
			}
			/**/
	$scope.tofrom = function(){
		$scope.startdate = $filter('date')($scope.dateRangeStart, "medium",'+0530');
		$scope.enddate = $filter('date')($scope.dateRangeEnd, "medium",'+0530');
		$scope.get_data('selected','selected','none','none','yes','none',$scope.startdate,$scope.enddate,'none');
	}
$scope.startDateOnSetTime = function  () {
  //$scope.$broadcast('start-date-changed');
  //console.log($scope.startdate); 
  //$scope.dateRangeStart = $filter('date')($scope.dateRangeStart, "medium",'+0530');  // for type="date"	
}

$scope.endDateOnSetTime = function  () {
  //$scope.$broadcast('end-date-changed');
  //$scope.dateRangeEnd = $filter('date')($scope.dateRangeEnd, "medium",'+0530');  // for type="date"	
}
$scope.closetheerror = function(field){
		  		$scope.fields[field]="default_hidden"; 
}
$scope.startDateBeforeRender = function  ($dates) {
  if ($scope.dateRangeEnd) {
    var activeDate = moment($scope.dateRangeEnd);

    $dates.filter(function (date) {
      return date.localDateValue() >= activeDate.valueOf()
    }).forEach(function (date) {
      date.selectable = false;
    })
  }
}

$scope.endDateBeforeRender = function  ($view, $dates) {
  if ($scope.dateRangeStart) {
    var activeDate = moment($scope.dateRangeStart).subtract(1, $view).add(1, 'minute');

    $dates.filter(function (date) {
      return date.localDateValue() <= activeDate.valueOf()
    }).forEach(function (date) {
      date.selectable = false;
    })
  }
}

$scope.gettickets = function(data){
	$scope.selecteduserid = data;
	$scope.get_data('selected','selected','none','none','yes','none','selected','selected',data); // 9th parameter userid
}


			/*for setting input value to the */
			$scope.edit = function(taskId,type){
						$scope.isclickenable = !$scope.isclickenable;
						$scope.call_edit_after = type;
					if($scope.old_edit_value.length == "" || $scope.edit_status =="" || $scope.is_valid == 'no') {
						$scope.hide_controls=true;
					        var html='<input type="text" handle-key info = "&^&editval&^&" handle-press="onkeypress()" id="'+taskId+'"  ng-model="editval" style="padding: 2px;	border:1px solid #ccc !important;width: 180px !important; font-family: "OpenSans-Semibold"; font-size:14px;">' ;
							var arr = taskId.split("s");
							arr.splice(0, 1);
							var count = arr.length;
							if(count == 1){
								var toeditval = $scope.names[arr[0]]['section'];
							} else if(count == 2){
								var toeditval = $scope.names[arr[0]]['chapters'][arr[1]]['chapter'];
							} else {
								var toeditval = $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic'];
							}
								$timeout(function(){
										 angular.element(document.getElementsByClassName(taskId)).text(""); // emptying the section value
										 var appnd = angular.element(document.getElementsByClassName(taskId)).append($compile(html)($scope));// appending input to the page
										 $scope.editval = toeditval; // setting value in the ng-model
										 angular.element(document.getElementsByClassName(taskId)).find("input").focus(); // focusing current input
										 $timeout(function(){
										 	if(type == 'edit'){
												$scope.get_names(taskId);
										 		angular.element(document.getElementsByClassName(taskId)).find("input").select(); // seleting current input		
										 	}
										 },10);
									 	 $scope.old_edit_value = toeditval;
									 	 $scope.edit_status = "editing";
										 $scope.tree_edit_index =taskId;
								}, 200);
							$scope.sortableOptions.disabled = true;
							$scope.sortableOptions_chapters.disabled = true;
							$scope.sortableOptions_topics.disabled = true;
					}else{
						// if any node is being edited already
						var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Already editing tree!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
						var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
						var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page
						}
				};

					$scope.focusout = function($event){
						var len = (''+$scope.editval).length;
								if( len < 3 || len > 50 ){
									var desired_html = '<div id="openModalss" style="height:170px;width:285px;top:500px;"><div id ="status err_status">ERROR!</div><p class="model_success">Length must be between 3-50 characters!</p><div class="ok_wrapper"><div class="ok_wrapper"><button class="gotIt" ng-click="ok()">Ok</button></div></div>';
									var desired_width ='sm';
									$scope.open(desired_html,desired_width,'edit',$event);
							} else {
								var keyword ="";
								$scope.hide_controls=false;
								if($scope.call_edit_after =='edit'){
									var keyword = "Edited";
								} else{
									var keyword = "Added";
								}
								$scope.autosave_status = keyword+" Successfully!";
								angular.element(document.getElementById($scope.tree_edit_index)).closest('span').html("").text($scope.editval); // emptying the section value
								var updatehtm =	$scope.updatetree($scope.tree_edit_index,$scope.editval ); // calling common tree update function

								$scope.sortableOptions.disabled = false;
								$scope.sortableOptions_chapters.disabled = false;
								$scope.sortableOptions_topics.disabled = false;
							}
					};
					$scope.handlekeyPress = function(keycode,event,scope){
						if(keycode === 13){ // if enterpressed
								var target = event.target;
								target.blur(); // if we blur the input it will automatically call $scope.focusout function
					} else if(keycode === 27){ // if escape is pressed
						//var old_edit_value = $window.localStorage.getItem('old_edit_value');
						var old_edit_value = $scope.old_edit_value;
						var updatehtm =	$scope.updatetree($scope.tree_edit_index,old_edit_value ); // calling common tree update function with old value
						$scope.hide_controls=false;
						$scope.sortableOptions.disabled = false;
						$scope.sortableOptions_chapters.disabled = false;
						$scope.sortableOptions_topics.disabled = false;
						$scope.isclickenable = true;
					}
						//$("#openModals").append('<div id ="status err_status">ERROR!</div><p class="model_success">Length must be between 3-50 characters!</p><div class="ok_wrapper"><div class="ok_wrapper"><button class="gotIt">Ok</button></div>');

					};
					$scope.updatetree = function(index , update ){
						var arr = index.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						if(count == 1){
							$scope.names[arr[0]]['section'] = update;
						} else if(count == 2){
							$scope.names[arr[0]]['chapters'][arr[1]]['chapter'] = update;
						} else {
							$scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic'] = update;
						}
							if($scope.old_edit_value == update){
								angular.element(document.getElementById(index)).closest('span').html("").text(update); // manually replacing input with val
								$scope.old_edit_value = "";
								$scope.edit_status = "";
								$scope.tree_edit_index ="";
						} else {
							$scope.old_edit_value = "";
							$scope.edit_status = "";
							$scope.tree_edit_index ="";
						}
						$scope.hide_controls=false;
							if($scope.call_edit_after == 'add' || $scope.call_edit_after == 'edit' ){
								$scope.apps_of_selected_topic ={};
							if($scope.activemenus.length ==3){
								$scope.topic_bc = ' >> '+update;
								$scope.activeMenu1 = $scope.activemenus[0];
								$scope.activeMenu2 = $scope.activemenus[1];		
								$scope.activeMenu = $scope.activemenus[2];
							} else if($scope.activemenus.length ==2){
								$scope.chapter_bc = ' >> '+update;
								$scope.activeMenu1 = $scope.activemenus[0];
								$scope.activeMenu2 = $scope.activemenus[1];
								$scope.activeMenu =  "";
								$scope.topic_bc = "";

								//$scope.activeMenu2 = $scope.activemenus[1];		
							} else{
								$scope.section_bc = ' >> '+update;
								$scope.activeMenu1 = $scope.activemenus[0];
								$scope.chapter_bc = '';
								$scope.activeMenu2 = "";
								$scope.activeMenu =  "";
								$scope.topic_bc = "";
							}
						
						}
						$scope.posttree("none");
						$scope.isclickenable = true; // enable click event again for get apps.
						return true;
					};
					$scope.add = function(index) {
							$scope.activemenus =[];
							var arr = index.split("s");
							arr.splice(0, 1);
							var count = arr.length;
							$scope.sortableOptions.disabled = true;
							$scope.sortableOptions_chapters.disabled = true;
							$scope.sortableOptions_topics.disabled = true;
							if(count == 1){
								var names = $scope.get_names(index);
								// add icon corresponding to section plus icon was pressed.means user wants to add chapter
									if ($scope.expanded.indexOf(arr[0]) < 0) {
											$scope.load_tree_nodes(index);
											$scope.defer.promise.then(function(response){
												$scope.add_chapter(arr);
											});
									} else{
										$scope.add_chapter(arr);
									}
									$scope.section_bc = ' >> '+names[0];
									$scope.activeMenu1 = $scope.activemenus[0]; // section

							} else if(count == 2){
								var names = $scope.get_names(index);
								// add icon corresponding to chapter plus icon was pressed.means user wants to add subject
									if( $scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('topics') ){
										var edit_in = $scope.names[arr[0]]['chapters'][arr[1]]['topics'].length;
										var x = {"topic": "Subject"};
										$scope.names[arr[0]]['chapters'][arr[1]]['topics'].push(x);
										$timeout($scope.edit('s'+[arr[0]]+'s'+[arr[1]]+'s'+edit_in,'add'), 100);
									}else{
										// if there are no chapters in the section
										var edit_in = 0;
									var src1 = [{topic:"Subject"}]; // source 1
									var chap = {topics:src1};
									angular.extend($scope.names[arr[0]]['chapters'][arr[1]],chap);
									}
									$scope.expand_topic_index[arr[0]][arr[1]] = true;
									$scope.fa_class_[arr[0]][arr[1]] =  'fa-minus-circle';
									$scope.selectedIndex = 's'+arr[0]+'s'+arr[1]+'s'+($scope.names[arr[0]]['chapters'][arr[1]]['topics'].length - 1);
									$scope.section_bc = '>> ' +names[0];
									$scope.chapter_bc = '>> ' +names[1];
									$scope.activemenus.push($scope.selectedIndex);

							} else {
								// add plus icon above tree tapped to add new section.
								var edit_in = $scope.names.length;
								var x = {section: "Section",chapters:[]};
								$scope.names.push(x);
								$scope.t_data.push(x);
								$timeout($scope.edit('s'+edit_in,'add'), 500);
								$scope.expand_topic_index[$scope.names.length - 1] = []; // add another array to expand arr
								$scope.fa_class_[$scope.names.length - 1] = []; // add another array to expand arr 
								var names = $scope.get_names('s'+($scope.names.length - 1));
							}
							// calling the edit function after everything is set up
					};
					$scope.add_chapter = function(arr){
						if( $scope.names[arr[0]].hasOwnProperty('chapters') ){
										var edit_in = $scope.names[arr[0]]['chapters'].length;
										var x = {chapter: "Chapter",topics:[]};
										$scope.names[arr[0]]['chapters'].push(x);
										// calling edit function after adding node
										$timeout($scope.edit('s'+[arr[0]]+'s'+edit_in,'add'), 100);
										} else {
											// if there are no chapters in the section
											var edit_in =0;
										var src1 = [{chapter:"Chapter",topics:[] }]; // source 1
										var chap = {chapters:src1};
										angular.extend($scope.names[arr[0]],chap);
										// calling edit function after adding node
										$timeout($scope.edit('s'+[arr[0]]+'s'+edit_in,'add'), 100);
										}
										$scope.expand_chapter_index[arr[0]] =  true ;
										$scope.fa_class[arr[0]] =  'fa-minus-circle';
										var active_index = 's'+arr[0]+'s'+($scope.names[arr[0]]['chapters'].length -1) ;
										//$scope.activemenus.push('s'+arr[0]);
										$scope.activemenus.push(active_index);
									    //$scope.activeMenu1 = ; // section
					};
					$scope.posttree = function(action){
						//$scope.autosave_status ='Saving...';
						$http({
								  method: 'POST',
								  url: 'post_tree',
									data : { _token: $window.localStorage.getItem("t") ,htmlcontent : JSON.stringify($scope.names) ,course_id : $scope.originidentity }
								}).then(function successCallback(response) {
									$window.localStorage.removeItem("tree_json");
										$window.localStorage.setItem("tree_json",JSON.stringify($scope.names));
										if(action == "get_apps"){
											var arr = $scope.selectedIndex.split("s");
											arr.splice(0, 1);
											$scope.get_apps($scope.selectedIndex,$scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic'])
									}
									$timeout(function(){
										$scope.autosave_status ='';	
									}, 1000);
									$scope.data = "";
								    // this callback will be called asynchronously
								    // when the response is available
										
								  }, function errorCallback(response) {
								    // called asynchronously if an error occurs
								    // or server returns response with an error status.
  						});

					}
					// to delete tree nodes
					$scope.delete = function (taskId,ev) {
						var arr = taskId.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						$scope.todelete ="";
						if(count == 1){
							( $scope.names[arr[0]].hasOwnProperty('chapters') && $scope.names[arr[0]]['chapters'].length > 0) ? $scope.delete_status = "no": $scope.delete_status = "yes";
						} else if(count == 2){
							(  $scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('topics') && $scope.names[arr[0]]['chapters'][arr[1]]['topics'].length > 0 ) ? $scope.delete_status = "no": $scope.delete_status = "yes";
						} else {
							$scope.delete_status = "yes";
						}

						if($scope.delete_status == "yes"){
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Delete?</p><button class="godelete proceed gotIt"  ng-click="delete('+"delete"+')">Delete</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
						}else {
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Information!</div><p class="model_success">Data Inside, Unable to Delete</p><div class="ok_wrapper"><button class="gotIt"  ng-click="cancel('+"cancel"+')">Ok</button></div></div>';
						}
							$scope.todelete = taskId;
							var desired_width ='sm';
						$scope.open(desired_html,desired_width,'delete');
					}
					// delete after confirmation
					$scope.delete_node = function () {
						var arr = $scope.todelete.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						$scope.todelete ="";
						if(count == 1){
							// if section
							$scope.names.splice(arr[0], 1);
							$scope.t_data.splice(arr[0], 1);
							$scope.activeMenu2 ="";
							$scope.activeMenu1 ="";
							$scope.activeMenu ="";
							$scope.section_bc ="";
							$scope.chapter_bc ="";
							$scope.topic_bc = "";
						} else if(count == 2){
							// if chapter
							$scope.names[arr[0]]['chapters'].splice(arr[1], 1);
							$scope.activeMenu2 ="";
							$scope.activeMenu ="";
							$scope.chapter_bc ="";
							$scope.topic_bc = "";
						} else {
							// if subject
							$scope.names[arr[0]]['chapters'][arr[1]]['topics'].splice(arr[2], 1);
							$scope.activeMenu ="";
							$scope.topic_bc = "";

							
						}
						$scope.selectedIndex ="";
						$scope.apps_of_selected_topic={};
						$scope.visibility_class = '';
						$scope.autosave_status ="Deleted Successfully!";
						$scope.posttree("none");
					}

						// common model function
						$scope.open = function (desired_html,desired_width,redirect,ev) {
										$mdDialog.show({
										controller: DialogController,
										template: desired_html,
										parent: angular.element(document.body),
										targetEvent: ev,
										clickOutsideToClose: false,
      							escapeToClose: false,
										locals: {
										items: {'originidentity': $scope.originidentity},
										},
										fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
										})
										.then(function(response) {
											if(redirect=='edit'){
													$scope.is_valid = "no";
													$scope.isclickenable = true;
													$scope.edit( $scope.tree_edit_index,'edit' );
											}else if(response=='delete'){
													$scope.delete_node();
											}else if(response=='drop'){
												if(response =="drop"){
														if($scope.currently_dragging == 'sections'){
																if($scope.section_drop_start_index < $scope.section_drop_end_index ){
																	// sorting up to down
																	$scope.names.splice($scope.section_drop_end_index + 1, 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index, 1);
																	$scope.posttree("none");
																} else {
																	// sroting down to up
																	$scope.names.splice($scope.section_drop_end_index , 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index + 1, 1);
																	$scope.posttree("none");
																}
															
														} else{
															var counter = 0
															angular.forEach($scope.expanded, function(v, k) {
															$scope.names[v] = $scope.t_data[v];
															counter++;
															});
															if(counter == $scope.expanded.length){
																$scope.posttree("none");
															}
														}
														
													// if user wants to drop
												}
											}else if(response =="no_drop"){
													/*$scope.t_data = JSON.parse(window.localStorage.getItem("beforedrag"));*/
													$scope.t_data = $scope.beforedrag;
											} else if(response == 'drop_app'){
												// if user wants to drop app
												taskId = $scope.selectedIndex;
												var arr = taskId.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var apps =  $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'];
												if($scope.app_drop_start_index < $scope.app_drop_end_index ){
													// sorting up to down
													apps.splice($scope.app_drop_end_index + 1, 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index, 1);
													$scope.posttree("get_apps");
												} else {
													// sroting down to up
													apps.splice($scope.app_drop_end_index , 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index + 1, 1);
													$scope.posttree("get_apps");
												}
											}else if(response == 'no_drop_app'){
												$scope.posttree("get_apps");
												// if user does not want to drop app
											} else if(response == 'attach_app'){
												Form_data = {_token: $window.localStorage.getItem("t") , in_ : $scope.attach_index, search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'attach'}
												var wheretogo ='attach_app';
												$scope.commonPostFunc(wheretogo,Form_data,'attach_app'); // sending a post request
											} else if(response == 'deattach_app'){
												index = $scope.de_attach_index ;
												var arr = $scope.selectedIndex.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var toeditapp = $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'][index];
												$scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'].splice(index, 1);
												$location.hash('wrapper'); // taking pointer to the top passed id
												$anchorScroll(); // taking pointer to the top passed id
												$scope.common_success_meesage("App De-attached Successfully!");
												$timeout(function(){
												$scope.posttree("get_apps");		
												}, 500);
											}
										}, function() {

										});
					  };
						function DialogController($scope, $mdDialog,items) {
							$scope.title ="";
							 $scope.originidentity = items.originidentity;
								 $scope.hide = function() {
									 $mdDialog.hide();
								 };
								 $scope.answer = function(answer) {
									 $mdDialog.hide(answer);
								 };
								 $scope.ok = function (response) {
									 $mdDialog.hide("ok");
								 };
								$scope.delete = function (response) {
									$mdDialog.hide("delete");
								};
								$scope.drop = function (response) {
									$mdDialog.hide("drop");
								};
								$scope.cancel_drop = function (response) {

									$mdDialog.hide("no_drop");
								};
								$scope.drop_app = function (response) {

									$mdDialog.hide("drop_app");
								};
								$scope.cancel_drop_app = function (response) {
									$mdDialog.hide("no_drop_app");
								};
								$scope.go_attach = function (response) {
									$mdDialog.hide("attach_app");
								};
								$scope.deattach_app = function (response) {
									$mdDialog.hide("deattach_app");
								};
								
								$scope.close_model = function (response) {
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;
									$mdDialog.hide("close_dialog");
								};
							 // will be used if cancel button needed to be displayed
								 $scope.cancel = function (response) {
									 $mdDialog.hide('cancel');
								 };
								 $scope.handleenterkey = function(keycode,event,scope){
								 }
							 };

			$scope.get_apps = function (taskId,item) {
				$scope.apps_of_selected_topic = {};
				$scope.tgl_cntr_loader = "make_visible";
				var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page	
				names = $scope.get_names(taskId);
				$scope.selectedIndex= taskId;
				var arr = taskId.split("s");
				arr.splice(0, 1);
				var count = arr.length;
				if($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]].hasOwnProperty('apps') ){
					var content =  $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'];
				} else {
					var content = 'Empty';
				}
				$http({
					method: 'POST',
					url: 'get_apps',
					data: { _token: $window.localStorage.getItem("t") ,'jsoncontent' : content , course_id : $scope.originidentity, location : $scope.location },
					}).then(function successCallback(response) {
						$scope.sortableOptions_apps.disabled =false;
						 $scope.success_class = 'sucess_mesage';
						$scope.success_message = "Loading Content...";
						$timeout(function(){
								$scope.apps_through = 'topic';
								if(response.data.allData !== 'Empty'){
									$scope.apps_of_selected_topic = response.data.allData;	
								} else{
									var html = '<div class ="no_apps" class="back_comm section"><i class="fa fa-times"></i>No Apps to display!</div>';
											  angular.element(document.getElementById('no_apps_messages')).append($compile(html)($scope));// appending input to the page		 
									}
									$location.hash('wrapper'); // taking pointer to the top passed id
									$anchorScroll(); // taking pointer to the top passed id
						}, 100).then(function(){
								$timeout(function() {
									angular.element('pre code').each(function(i, block) {
									hljs.highlightBlock(block);
									});
								});
								$scope.visibility_class = '';
						});
						$timeout(function(){
							$scope.success_message = "";
							$scope.startFade = "";
							 $scope.success_class = 'success_default'; 
							 $scope.tgl_cntr_loader = "make_hidden";
						}, 2000);
					}, function errorCallback(response) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
				});

				
				$scope.activeMenu1 = $scope.activemenus[0];
				$scope.activeMenu2 = $scope.activemenus[1];
				$scope.activeMenu = $scope.activemenus[2];
				$scope.section_bc = ' >> '+names[0];
				$scope.chapter_bc = ' >> '+names[1];
				$scope.topic_bc = ' >> '+names[2];
			};
		$scope.ct_apps = function(){
			$scope.get_apps($scope.selectedIndex,'default');
		}
		$scope.section_span = function (taskId,item,index) {
			$scope.expand_chapter_index[index] = ($scope.expand_chapter_index[index] !== true ) ?  true: false;
			$scope.fa_class[index] = ($scope.expand_chapter_index[index] !== true || $scope.expand_chapter_index[index] == undefined ) ? '' : 'fa-minus-circle';
			names = $scope.get_names(taskId);
			$scope.topic_bc = '';
			$scope.chapter_bc = '';
			$scope.section_bc = ' >> '+item;
			$scope.activeMenu1 = taskId;
			$scope.activeMenu2 = "";
			$scope.activeMenu = "";
			$scope.autosave_status ='Loading Content...';
			$scope.selectedIndex ="";
			$scope.apps_of_selected_topic ={};
			$timeout(function(){
					$scope.load_tree_nodes(taskId);
				},100);
		};
		$scope.load_tree_nodes = function(taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			if(count == 1){
				if($scope.names[arr[0]].hasOwnProperty('chapters') ){
					$scope.t_data[arr[0]]['chapters'] = $scope.names[arr[0]]['chapters'];	
				}
				if ($scope.expanded.indexOf(arr[0]) < 0) {
					$scope.expanded.push( arr[0] );
				}
				
				$timeout(function(){
					$scope.autosave_status ='';
				},100);
			} else if(count == 2){
			} else {
			}
			$scope.defer.resolve("loaded");
			return true;
		};
		$scope.chapter_span = function (taskId,item,index) {
			names = $scope.get_names(taskId);
			$scope.topic_bc = "";
			$scope.activeMenu = "";
			//	$scope.expand_topic_index
			$scope.expand_topic_index[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true ) ?  true: false;
			$scope.fa_class_[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true || $scope.expand_topic_index[$scope.selected_sec_index][index] == undefined ) ? '' : 'fa-minus-circle';
			
			$scope.section_bc = ' >> '+names[0];
			$scope.activeMenu1 = $scope.activemenus[0];
			$scope.topic_bc = '';
			$scope.chapter_bc = ' >> '+item;
			$scope.activeMenu2 = $scope.activemenus[1];
			$scope.selectedIndex ="";
			$scope.visibility_class ="";
			$scope.apps_of_selected_topic ={};
		};
		$scope.get_names = function (taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			var activemenus = [];
			$scope.activemenus =[];
			if(count == 1){
				name.push($scope.names[arr[0]]['section']);
				activemenus.push('s'+arr[0]);
				$scope.selected_sec_index = arr[0];
			} else if(count == 2){
				name.push($scope.names[arr[0]]['section']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				activemenus.push('s'+arr[0]);
				activemenus.push('s'+arr[0]+'s'+arr[1]);
			} else {
				name.push($scope.names[arr[0]]['section']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic']);
				activemenus.push('s'+arr[0]);
				activemenus.push('s'+arr[0]+'s'+arr[1]);
				activemenus.push('s'+arr[0]+'s'+arr[1]+'s'+arr[2]);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				$scope.selected_top_index = arr[2];
			}
			$scope.activemenus = activemenus;
			return name;
			};

			$scope.new_app = function (name,ev){
						$scope.app ={};
						$scope.is_logo_hidden = false;
						$scope.is_preview_hidden = true;
						$scope.is_disbaled = false;
						if($scope.selectedIndex.length > 0){
							$scope.name = name;
							if(name.length){
						   		var url = $scope.base_url+'/'+name+'/'+$scope.originidentity;
									$scope.trigger_fancybox(url ,'get','none');
								}
						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a Topic first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}

			};
			 $scope.onReady = function () {
					var url = $scope.base_url+'/fillboat/'+$scope.being_edited_app;
					$http.get(url).then(function(response) {
						$scope.app.content = response.data.text.content;
					});
			  };
				$scope.trigger_fancybox = function (url,method,data) {
					if(method == 'get'){
						$http.get(url).then(function(response) {
							if (response.status == 200) {
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
							}
							});
					} else {
						$http.post(url,data ).then(function(response) {
							if (response.status == 200) {
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
							}
							});
					}

				 };
				$scope.new_app_submit = function (response) {
						if($scope.is_disbaled == false){
							 $scope.is_disbaled = true;
							 $scope.success_class = 'sucess_mesage';
							$scope.success_message = "Uploading Content...";
							$location.hash('anchor_top');
							$anchorScroll();
						$timeout(function(){
						}, 1000).then(function(){
									var file = $scope.myFile;
									console.log(file);
									if($scope.enc_status == 'no'){
      								$scope.being_edited_app = $scope.enc;
      							}
								$scope.app.course_id = $scope.originidentity;
								if($scope.name == 'image'){
									  var uploadUrl = $scope.base_url+"/new_imageApp";
								} else if($scope.name == 'video'){
									console.log($scope.app.embedded_code);
									var uploadUrl = $scope.base_url+"/new_videoApp";
									if($scope.app.hasOwnProperty('embedded_code') && /*typeof*/ file == 'none'){
										console.log("if");
											var file = "none";
									 }else{
									 	 console.log("else");
										 $scope.app.embedded_code ="";
									 }
								} else if($scope.name == 'zip'){
									var uploadUrl = $scope.base_url+"/new_zipApp";
									if($scope.app.hasOwnProperty('external_link') && /*typeof*/ file == 'none'){
											var file = "none";
									 }else{
										 $scope.app.external_link ="";
									 }
								} else if($scope.name == 'text'){
									var uploadUrl = $scope.base_url+"/new_textApp";
									var file = "none";
								} else if($scope.name == 'image_popupwindow' || $scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
									// edit image aap
										if(/*typeof*/ file == 'none'){ // if no image selected
											file = 'none';
										}
										$scope.app.cai = $scope.being_edited_app;
										var uploadUrl = $scope.base_url+"/"+$scope.name;
								}
									//console.log($scope.app.title);
									//$scope.app.title = $scope.app.title.replace('\'', '\\\'');
                   					$scope.uploadfile(file, uploadUrl,$scope.app ,$scope.name);
						});	
					}
		      							
		      };

					$scope.uploadfile = function(file, uploadUrl,other_data,name){
						 var fd = new FormData();
						 if(file !== 'none'){
								if(name =='image'){
										fd.append('image_app', file);
								} else if(name =='video'){
										fd.append('video_app', file);
								} else if(name == 'zip'){
										fd.append('app_zip', file);
								}else if(name == 'image_popupwindow' || name == 'zip_popupwindow'){
									//console.log(file);
									fd.append('url', file);
								} else if(name == 'video_popupwindow'){
									fd.append('video_app', file);
								}
						 }
						 	console.log(other_data); 
							angular.forEach(other_data, function(value, key) {
								//console.log(value);
								fd.append(key, value);
							});
						 $http.post(uploadUrl, fd, {
								transformRequest: angular.identity,
								headers: {'Content-Type': undefined}
						 })
						 .success(function(response){
							 if(response.status == 'success' || response.status == 'updated' ){
								$scope.aftersuccess(response);

							 }else if(response.status =='failed'){
								$scope.success_class = '';
								$scope.success_message = "";
								 angular.forEach(response.errors, function(value, key) {
									var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
									var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								 var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
								});
								 $scope.is_disbaled = false;
							}

						 }).error(function(){
						 });
					};
			$scope.common_success_meesage = function (message){
				$scope.success_class ="sucess_mesage"
				$scope.success_message = message;
				$timeout(function(){
							$scope.success_message = "";
							$scope.startFade = "";
							 $scope.success_class = 'success_default';
						}, 3000);
			};
			$scope.aftersuccess = function(answer){
				if(answer.status == 'success'){
					var arr = $scope.selectedIndex.split("s");
					arr.splice(0, 1);
					var count = arr.length;
					if($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]].hasOwnProperty('apps') ){
						var x = {[answer.type] : answer.app_id};
						$scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'].push(x);
					} else {
						var src1 = [{[answer.type] : answer.app_id}]; // source 1
						var apps = {apps:src1};
						angular.extend($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]],apps);
					}
					$scope.posttree("get_apps");
					$scope.app ={};

				} else if(answer.status == 'updated'){
					if($scope.name == 'image_popupwindow'){
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = "";
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;

					}else if($scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
						taskId = $scope.selectedIndex;
						var item = $scope.get_names(taskId);
						$scope.get_apps(taskId, item[2] );	
						$scope.common_success_meesage("App Edited Successfully!");
					}
					//$scope.being_edited_app_index

				}
				$scope.is_logo_hidden = false;
				$scope.is_preview_hidden = true;
				$.fancybox.close();
				$scope.app ={};
				$scope.myFile ='none'
			};

			$scope.edit_app = function(index,id,type) {
				//console.log($scope.app);
				$scope.app ={};
				$scope.is_logo_hidden = false;
				$scope.is_preview_hidden = true;
				$scope.being_edited_app_index = index;
				$scope.is_disbaled = false;
				var arr = $scope.selectedIndex.split("s");
				arr.splice(0, 1);
				var count = arr.length;
				var toeditapp = $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'][index];
				 var name = "";
				 (type == 1 ? $scope.name = 'image_popupwindow'  :'' );
				 (type == 2 ? $scope.name = 'video_popupwindow'  :'' );
				 (type == 3 ? $scope.name = 'table_popupwindow'  :'' );
				 (type == 4 ? $scope.name = 'zip_popupwindow'  :'' );
				 (type == 5 ? $scope.name = 'text_popupwindow'  :'' );
				 angular.forEach(toeditapp, function(value, key) {
					replace_1 = value.split('/').join('Hxkuy9jdji877jkgha692ahuiyadyu76iuyai6');
					replace_2 = replace_1.split('+').join('iy878ned7ehhsd87e6fd2140jdamnhgHHDysMM');
					replace_3 = replace_2.split('|').join('MMDU897bhhdtHHdtslsatxtas');
					$scope.being_edited_app = replace_3;
					var url = $scope.base_url+'/'+$scope.name+'/'+replace_3;
					$scope.trigger_fancybox(url ,'get','none');
				 });


			}
				$scope.hide_fancy = function(and_) {
					if(and_ == 'get_apps'){
						taskId = $scope.selectedIndex;
						var item = $scope.get_names(taskId);
						$scope.get_apps(taskId, item[2] );
					}
						$scope.is_logo_hidden = false;
						$scope.is_preview_hidden = true;
						  $.fancybox.close();
	      }
				/*
				this function is called edit text app.
				*/
				$scope.autosave = function() {
						var doneTypingInterval = 800;  //time in ms, 0.8 second for example
						$timeout.cancel($scope.typingTimer);
						 $scope.typingTimer = $timeout($scope.doneTyping, doneTypingInterval);
	      }
				$scope.doneTyping = function (){
					var text_content = $scope.app.content;
					var Form_data = { _token: $window.localStorage.getItem("t") , content : text_content , origin: $scope.being_edited_app, title: $scope.app.title, through : $scope.app.through , course_id : $scope.originidentity, location : $scope.location } ;
					var wheretogo = 'autosave_text';
					$scope.autosave_status = "saving...";
					var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'autosave');
				}
				$scope.commonPostFunc = function(wheretogo,FormData,task){
									$http({
									method: 'POST',
									url: wheretogo,
									async : true,
									data : FormData
								}).then(function successCallback(response) {
									if(task =='autosave'){
										if(response.data.status =='failed'){
											$scope.autosave_status = "Data not saved!";
											angular.forEach(response.data.errors, function(value, key) {
											var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
											var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
											var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
											});

										}else{
											$scope.autosave_status = "saved!";
										}										
										 $timeout(function(){
											 $scope.autosave_status = "";
										 },500);
									} else if(task == 'search_apps'){
										var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
										$scope.apps_through = 'search';
										$scope.total_apps = $scope.total_apps + response.data.allData.length;
									 	if(response.data.allData !== 'Empty') {
									 		$scope.common_success_meesage("Loading Content...");
											$scope.apps_of_selected_topic = response.data.allData;
											$timeout(function() {
												angular.element('pre code').each(function(i, block) {
													hljs.highlightBlock(block);
												});
												angular.element('.apps_content').highlight($scope.currently_searched_data.val);
												$scope.visibility_class = 'make_visible';
											});
										} else{
											
											var html = '<div class ="no_apps" class="back_comm section"><i class="fa fa-times"></i>No Apps to display!</div>';
											  angular.element(document.getElementById('no_apps_messages')).append($compile(html)($scope));// appending input to the page		 

										};
									 	$scope.pagination_status ='not_triggered';
									 	
									} else if(response.data.status =='success' && task == 'attach_app'){
										$scope.common_success_meesage("App Attached Successfully!");
										$location.hash('wrapper');
										$anchorScroll();
										$scope.aftersuccess(response.data);
									} else if(task == 'paginate'){
										$scope.apps_through = 'search';
										$scope.total_apps = $scope.total_apps + response.data.allData.length;
										if(response.data.allData !== 'Empty') {
											angular.forEach(response.data.allData, function(value, key) {
											$scope.apps_of_selected_topic.push(value);
											});
												$timeout(function() {
													angular.element('pre code').each(function(i, block) {
														hljs.highlightBlock(block);
													});
													angular.element('.apps_content').highlight($scope.currently_searched_data.val);
												},200);
											$scope.pagination_status = 'not_triggered';
											angular.element('.apps_content').highlight($scope.currently_searched_data.val);
										}
									 	
									} else if(task == 'drag_ticket'){
										SweetAlert.swal("Done!");
									} else if (task == 'ticindex'){
										if(response.data.status =='success' ){
											SweetAlert.swal("Reminder Sent!");
										}
									} else if(task == 'ticketresponse'){
										console.log(response);
										if(response.data.status =='success' ){
											//$window.location.href = $scope.base_url + 'tickets/ticketresponse/' +
										}
									}

										$scope.tgl_cntr_loader = "make_hidden";
									}, function errorCallback(response) {
							});
					}
				
				$scope.search_apps = function(){
					$scope.sortableOptions_apps.disabled =true;
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.selectedIndex.length > 0){
							$scope.currently_searched_data = $scope.search; // because $scope.search is bound to input
							if($scope.search.app == undefined){
								var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select App from drop-down first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

							} else if($scope.search.val == undefined){
								var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please fill the keyword to search!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page								
							}else {
								$scope.total_apps = 0;									// for safer side we store it in another variable.					
								Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.search.val , app : $scope.search.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' , of_ : $scope.total_apps}
								var wheretogo = 'search_apps';
								$scope.apps_of_selected_topic =[];
								$scope.tgl_cntr_loader = "make_visible";
								$scope.commonPostFunc(wheretogo,Form_data,'search_apps'); // sending a post request
							}
						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a Subject first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}
				};
				$scope.attach = function(index,id){
					$scope.attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>Attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="go_attach('+"attach"+')">Attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_attach');

				};
				$scope.deattach = function(index,id){
					$scope.de_attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>De-attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="deattach_app('+"de-attach"+')">De-attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_de-attach');
				};
				$scope.paginate_apps = function(index){
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.total_apps > 9 &&  $scope.total_apps - 3 <= index && $scope.pagination_status !== 'triggered' &&  $scope.apps_through !== 'topic'){
					var wheretogo = 'search_apps';
					Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' ,of_ : $scope.total_apps}
					$scope.tgl_cntr_loader = "make_visible";
					$scope.commonPostFunc(wheretogo,Form_data,'paginate'); // sending a post request
					}
				};
			$scope.toggleFullScreen = function() {
					if($scope.isFullscreen == false){
						$scope.fs_through = 'fullscreen';
						$scope.compress_icon_class = '';
						$scope.isFullscreen = !$scope.isFullscreen;
						$timeout(function(){
							$scope.hide_all = true;
							$scope.sortableOptions_apps.disabled =true;
							angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						}, 200).then(function(){
							$scope.compress_icon_hidden = !$scope.compress_icon_hidden;
						});
					} else {
						$scope.compress_icon_class = 'compress_icon_hide';
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						$timeout(function(){
							$scope.isFullscreen = !$scope.isFullscreen;
						}, 200).then(function(){
						});
					}
				}

				$scope.toggle_sidebar = function(){
					if($scope.hide_toggle_sidebar == true){ // means side bar is just hidden
							$scope.hide_toggle_sidebar = !$scope.hide_toggle_sidebar;
							$timeout(function(){
							angular.element(document.getElementsByTagName('body')).animate({ scrollTop: 0 }, 200); /* taking user to the top of the page*/
							}, 500).then(function(){
								angular.element(document.getElementsByClassName('section-right')).addClass("section_animated");
								angular.element(document.getElementsByClassName('section-right')).addClass("section-right1");
								$scope.restore_fs = !$scope.restore_fs;
							});
						} else{ // means side bar just shown
							$scope.restore_fs = !$scope.restore_fs;
							angular.element(document.getElementsByClassName('section-right')).removeClass("section-right1");
							$timeout(function(){
								angular.element(document.getElementsByClassName('section-right')).removeClass("section_animated");
								$scope.hide_toggle_sidebar = !$scope.hide_toggle_sidebar;
								
							}, 500);
							
						}
				
			};

				$scope.makefullscreen_app = function(index) {
					if(Fullscreen.isEnabled()){
						Fullscreen.cancel();
					}else{
						$scope.fs_through = 'highlight';
						$scope.compress_highlight_class ="";
						Fullscreen.enable( document.getElementById('a'+index) );
						angular.element(document.getElementsByClassName('back_comm')).addClass("override_overflow");
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						$scope.compress_highlight_hidden = !$scope.compress_highlight_hidden; 
					}
				}
				$scope.stopprop= function(){
					//Fullscreen.cancel();
				}

				$scope.preview_ = function(response){
						var file = $scope.myFile;
						var reader = new FileReader();
						reader.readAsDataURL(file);
						$scope.is_logo_hidden = true;
						$scope.is_preview_hidden = false;
						reader.onload = function(e) {
							$scope.$apply(function(){
							$scope.preview_img = e.target.result;
						});
					 }
				}
				
			$scope.showmenu = function(){
				$scope.showDetails = !$scope.showDetails;
			}
			$scope.hidemenu = function(){
				$scope.showDetails = false;
			}
			$scope.gototop = function(){
				//angular.element(document.getElementsByTagName('body')).animate({ scrollTop: 0 }, 200); /* taking user to the top of the page*/
				$location.hash('wrapper'); // taking pointer to the top passed id
				$anchorScroll(); // taking pointer to the top passed id
				$scope.make_gtp_vi ="";
			}
			$scope.updateUser = function(data){
				$scope.get_data('none',0,'none',data,'yes','none','none','none','none'); // calling function onload 4th parameter ticket id
			};
		
			$scope.setreminder= function(id){
				$scope.reminderid = id;
				$scope.trigger_sweet_confirmation('Are You Sure To Send Reminder?',"Yes!Send It",'warning','ticindex','yes');
			}
			$scope.ticketresponse= function(id){
				var wheretogo = 'ticketresponse';
				Form_data = {origin:id}
				/*$window.location.href = $scope.base_url + 'tickets/ticketresponse/' + id;*/
				$window.location.href = $scope.base_url + 'tickets/response/' + id;
				//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'ticketresponse');
			}
				// sortable options for sections needed to be called differently because these are different
				$scope.sortableOptions = {
					connectWith: ".alltickets",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						/*$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));*/
						$scope.beforedrag = $scope.t_data;
						ui.item.startPos = ui.item.index();
						$scope.section_drop_start_index = ui.item.index();
						console.log($scope.section_drop_start_index);
						$scope.currently_dragging = "sections";
						ui.item.hide();
						var text = $scope.names[ui.item.startPos]['title'];
						ui.placeholder.html("<td colspan='8' class='hidden-xs'><span>"+text+"</span></td>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");
					},
					update:  function(event, ui){
						$scope.section_drop_end_index = ui.item.sortable.dropindex;
						console.log($scope.section_drop_end_index);
						var desired_html = '<div id="openModalss" style="height:170px;width:300px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
						desired_width='sm';
						$scope.trigger_sweet_confirmation('Are You Sure To Drop?',"Drop It",'warning','drag_ticket','yes');
					},

				};
			$scope.trigger_sweet_confirmation	= function(thetitle,thebuttontext,thetype,from_,runajax){
					SweetAlert.swal({
						   title: /*"Are you sure?"*/ thetitle,
						   text: 'After confirmation changes will be made instantly',
						   type: thetype,
						   showCancelButton: true,
						   confirmButtonColor: "#DD6B55",
						   confirmButtonText: thebuttontext,
						   closeOnConfirm: false}, 
						function(isConfirm){ 
						   if(isConfirm == true ){
						   		if(from_ == 'drag_ticket'){
						   			if(runajax == 'yes'){
						   				if($scope.section_drop_start_index < $scope.section_drop_end_index ){
										console.log("sorting up to down");
										var afterorbefore = 'after';
									} else {
										var afterorbefore = 'before';
									}
										var other_ticket = $scope.names[$scope.section_drop_end_index]['id'];
										var dropped_ticket = $scope.names[$scope.section_drop_start_index]['id'];
										Form_data = {afterorbefore:afterorbefore,other_ticket:other_ticket,dropped_ticket:dropped_ticket};
										console.log(Form_data);
										var wheretogo ='reordertickets';
										$scope.commonPostFunc(wheretogo,Form_data,'drag_ticket'); // sending a post request
						   			}
									
						   		} else if(from_ == 'ticindex'){
									var wheretogo = 'getOrigin';
									Form_data = {reminderid:$scope.reminderid}
									var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'ticindex');
						   		}
						   }
						});
				};

				$scope.sortableOptions_chapters = {
					connectWith: ".chapters",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						$scope.chapter_drop_start_index = ui.item.index();
						$scope.currently_dragging = "chapters";
						ui.item.hide();
						var text = ui.item.context.innerText;

						ht = "<div class ='drag_div'><span>"+$scope.chapter_name+"</span></div>"
						$compile(ht)($scope);
						ui.placeholder.html(ht);
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.chapter_drop_end_index =ui.item.sortable.dropindex;
							//event.stopImmediatePropagation();
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}


				};
				$scope.chap_name = function(name){
					$scope.chapter_name = name;
				};
				$scope.sortableOptions_topics = {
					connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 2,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						ui.item.hide();
						ui.placeholder.html("<div class ='drag_div'><span>"+ui.item.text().trim()+"</span></div>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");	
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							//event.stopImmediatePropagation();
							$scope.currently_dragging = "topics";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}
				};

				$scope.sortableOptions_apps = {
					//connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 10,
					opacity: 0.8,
					disabled: false,
					placeholder: "back_comm_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag_apps",JSON.stringify($scope.apps_of_selected_topic));
						ui.item.startPos = ui.item.index();
						$scope.app_drop_start_index = ui.item.index();
						ui.item.hide();
						var text = $scope.apps_of_selected_topic[ui.item.startPos]['title'];
						ui.placeholder.html("<div class ='drag_div'><span>"+text+"</span></div>");
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.app_drop_end_index = ui.item.sortable.dropindex;
							event.stopImmediatePropagation();
							$scope.currently_dragging = "apps";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop_app('+"drop_app"+')">Drop</button><button class="cancel" ng-click="cancel_drop_app('+"no_drop_app"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop_app',event);
						}

					}
				};	// ended here
	});

    } );
		// directive is used to display apps when get_apps is called.
		app.directive('apipeline', function($compile,$sce,$timeout) {

			/**/
	return {
		replace: true,
		link: function(scope, element,attrs) {	
			var el = angular.element('<div ng-mouseenter="paginate_apps($index)" ng-click="stopprop()"></div>');
			var fullscreen = scope.base_url+'/images/full.png';
			el.append('<center><a class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="makefullscreen_app();$event.stopPropagation();" ng-hide="compress_highlight_hidden" ng-class="compress_highlight_class"><i class="fa fa-compress" ng-hide="compress_highlight_hidden" ng-class="compress_highlight_class"></i></a></center>');
			el.append('<center><span class = "description_of_app" ng-bind="appdata.title"></span></center>');
			var fulls_ht = '<ul><li><i><a class = "makefullscreen_image" href="" rel="apps_gallery" ng-click="makefullscreen_app($index);$event.stopPropagation();" ng-hide="hide_all"><img  src="'+fullscreen+'" onclick="" title="Highlight Image"></a></i></li></ul>';
			if(scope.apps_through == 'search'){
				
					// if apps being called through search
			  var attach_or_deatach = '<li ng-hide="hide_all"><a href="#"  class="de-attach" ><i class="fa  fa-link" title="attach App" ng-click="attach($index,appdata.id)"></i></a></li>';	
			}else{
				// if apps being called by clicking on topic
			  var attach_or_deatach = '<li ng-hide="hide_all"><a id="test123" class ="editIcon" href=""  title="Edit Image" ng-click="edit_app($index,appdata.id,appdata.app_id)"><i class="fa fa-pencil"></i></a></li><li ng-hide="hide_all"><a href="#"  class="de-attach" ><i class="fa  fa-unlink" title="De-attach App" ng-click="deattach($index,appdata.id)"></i></a></li>';
			}
			$compile(attach_or_deatach)(scope);
			if(scope.appdata.app_id == 1){
				// image app
				if(scope.is_image_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);
				var url = scope.base_url+'/content_images/'+scope.appdata.url;
				el.append('<div class="section-right-top"  ><center><div class = "main_image" ><img id="show" class ="" ng-src="'+scope.base_url+'/content_images/&^&appdata.url&^&" alt="#" allowfullscreen accept="image/*"></div></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
			} else if (scope.appdata.app_id == 2){
				// video app
				if(scope.is_video_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);
				if(scope.appdata.url.match("iframe")){
					// video app embedded code.
					var ht = scope.appdata.url;
					$compile(ht)(scope);
						el.append('<div class="video-right" ><center><div class = "main_image" >'+ht+'</div></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
				} else {
					// video app uploaded file
						//var url = scope.base_url+'/content_videos/'+scope.appdata.url;
						var theurl  = $sce.trustAsResourceUrl(scope.base_url+'/content_videos/'+scope.appdata.url);
						el.append('<div class="video-right" ><center><iframe  src="'+theurl+'"  type="video/mp4" width="auto" height="auto" id="video" frameborder="0" allowfullscreen></iframe></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
				}
			} else if (scope.appdata.app_id == 4){
				// zip app
				if(scope.is_zip_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);
				if(scope.appdata.url.match("link")){
					// zip app is external dropbox link
						var url = scope.appdata.url.split("^&^")[1];
				} else {
					// video app uploaded file
						var url = scope.base_url+'/content_zips/'+scope.appdata.url;
				}
				el.append('<div class="download-portion" ><a href="'+url+'" target="_blank"><p>Download</p><i class="fa fa-download"></i></a></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
			} else if (scope.appdata.app_id == 5){
				// if there is text app_id
				if(scope.is_text_fullscreen == 'No'){
					var fulls_ht ='';
				}
				var ht = scope.appdata.content;
				$compile(ht)(scope);
				el.append('<div class="right-text" ><div class ="main_content" hljs>'+ht+'</div></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
			}
			$compile(el)(scope);
			element.append(el);


		}
	}
});

/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('asText', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
											link: scope.autosave(event.which,event,scope);
											return false;


            });

        };
    });
app.animation('.success_message_here', function() {
  return {
    enter: function(element, done) {
      //element.css('display', 'none');
      //element.fadeIn(5000, done);
      return function() {
        element.stop();
      }
    },
    leave: function(element, done) {
      element.fadeOut(5000, done)
      return function() {
        element.stop();
      }
    }
  }
});
app.directive('resfs', function() {
	return {
    template: '<a id="showData" class="callMe showData" href={{url("/")}} ng-click="toggle_sidebar();$event.preventDefault();$event.stopPropagation()" ng-class="hideicon_class"><i class="fa fa-arrow-circle-left arrow" ng-hide ="restore_fs"></i></a>'
  };

});
app.directive('compressicon', function() {
	return {
    template: '<a href="#" class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="toggleFullScreen();$event.preventDefault();$event.stopPropagation()" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"><i class="fa fa-compress" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"></i></a>'
  };

});
app.directive('hidemenu', function ($window) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var el = element[0];
      // if there is clicked anywhere on the document that will be triggered.
      angular.element($window).bind('click', function(){
      		scope.showDetails = false;
      		 scope.$apply();
      });
    } 

  };

});
app.directive('enteronbutton', function() {
        return function(scope, element, attrs) {
            element.on("keydown", function(event) {

							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.handleenterkey(event.which,event,scope);
											return false;
									}
					} 
            });

        };
    });
		// add new app

app.directive('prevententer', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.new_app_submit();
											return false;
									}
							} 

            });

        };
    });

app.directive('scrolly', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var raw = angular.element(document.body);
            
            var body = document.body,
				html = document.documentElement;
            $timeout(function(){
            $document.bind('scroll', function () {
			    scroll_top = Math.max( body.scrollTop, html.scrollTop);
			    height = Math.max( body.scrollHeight, html.scrollHeight);
				var offset = Math.max( body.offsetHeight, html.offsetHeight,$window.pageYOffset );
                if(scroll_top + offset > 1200){
                	scope.make_gtp_vi = 'make_less_opacity';	
                	scope.shouldgotop = true;
                	if (scroll_top + offset >= height ) {
                }
                 scope.$apply(attrs.scrolly);
           	 	}else if(scroll_top + offset < 1200){
                	scope.make_gtp_vi = '';	
                	scope.shouldgotop = false;
                	 scope.$apply(attrs.scrolly);
                }

            	});
           },500);
        }
    };
});

//}