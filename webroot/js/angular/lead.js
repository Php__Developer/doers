
	var app = angular.module('createTree', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable','ngMaterial'/*,'lr.upload','ckeditor','FBAngular','ngFileUpload'*/,/*'blockUI',*/'oitozero.ngSweetAlert','ui.bootstrap.datetimepicker']) ;
	app.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('&^&');
	    $interpolateProvider.endSymbol('&^&');
	  });
	/*var Mydata =   jQuery.parseJSON( myData );*/
/**/
/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.factory('socket', function ($rootScope) {
 var socket = io('http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000');
	 socket.on('reloadticketsok', function (data) {
	});
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function (data) {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
      	console.log(eventName);
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

app.directive('jquerydatetimepicker', function () {
      return function (scope, element, attr) {
              scope.$watch('triggerdtpicker', function (val) {
                  if (val){
              		 $(element).datetimepicker({
              		 	dateFormat: 'MM dd, yy',
              		 	 onChangeDateTime:function(dp,$input){
              		 	 		scope.app.deadline = $input.val();
						   		scope.onTimeSet(scope.app.deadline , 'none');
						  }
              		 });   
			      }
				})
              scope.$watch('triggerdateerror', function (val) {
                  if (val){
                  		console.log("asdfasd");
                  	 //$(element).datetimepicker('hide');
                  	 // $(element).datetimepicker('reset')
			      }
				})

			}
	});


app.directive('jquerydatepicker', function () {
      return function (scope, element, attr) {
      		console.log("Diretive called");
      			$('#date_timepicker_start').datetimepicker({
				  format:'Y/m/d',
				  onShow:function( ct ){
				   this.setOptions({
				    maxDate:$('#date_timepicker_end').val()?$('#date_timepicker_end').val():false
				   })
				  },
				  timepicker:false
				 });
				 $('#date_timepicker_end').datetimepicker({
				  format:'Y/m/d',
				  onShow:function( ct ){
				   this.setOptions({
				    minDate:$('#date_timepicker_start').val()?$('#date_timepicker_start').val():false
				   })
				  },
				  timepicker:false
				 });
            }
	});


app.directive('tagsinput', function () {
      return function (scope, element, attr) {
      		
              scope.$watch('triggertagsinput', function (val) {
              		console.log(val);
                  if (val){
                  	$(element).tagsinput({
						/*confirmKeys: [13, 188],
						freeInput: false,*/
						allowDuplicates: false,
						itemValue: 'Value',  // this will be used to set id of tag
        				itemText: 'label' // this will be used to set text of tag
					});
					$(element).closest('.form-group').find('.bootstrap-tagsinput').find('input').addClass('form-control');
					$(element).closest('.form-group').find('.bootstrap-tagsinput').css('width','100%');
                  	$(element).closest('.form-group').find('.bootstrap-tagsinput').find('input').autocomplete({
				        source: function(request, response) {
				        	console.log(scope.tagsarr);
				          var results = $.ui.autocomplete.filter(scope.tagsarr, request.term);
				          response(results.slice(0, 10));
				        },
				        autoFocus: true,
				        select: function (event, ui) {
				          var selected =ui.item.Value;
				          var result = $.inArray(selected, scope.selectedtags);
				          if(result == -1){
								scope.selectedtags.push(selected);
			                	$(element).tagsinput('add', {label: ui.item.label ,Value : selected });
							}
				           
				        }
				      });
                  	$(element).on('beforeItemAdd', function(event) {
                  		
						var result = $.inArray(event.item.label.toLowerCase(), scope.tagsnamesonly) != -1;
						if(result == false){
							event.cancel = true;
							console.log("Out Of Array");
					  }
					});
                  	$(element).on('beforeItemRemove', function(event) {
                  		var index = scope.selectedtags.indexOf(event.item.Value);
                  		var removed = scope.rmelfromarr(index, scope.selectedtags);
                  		scope.selectedtags = removed;

					});


                  }  
              });
      }
})

/*app.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});*/
app.directive('handleKey', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 || event.which === 27) {
											link: scope.handlekeyPress(event.which,event,scope);
											return false;
									}
							} else if(event.type =="blur"){
								link: scope.focusout(event.which,scope);
							}

            });

        };
    });
		/*
		NOTES:Directive name :fileModel
		Usage: for handling file uploding in the scope

		*/
	app.directive('fileModel', ['$parse', function ($parse) {
	            return {
	               restrict: 'A',
	               link: function(scope, element, attrs) {
	                  var model = $parse(attrs.fileModel);
	                  var modelSetter = model.assign;

	                  element.bind('change', function(e){
	                     scope.$apply(function(){
	                        modelSetter(scope, element[0].files[0]);
	                        scope.preview_file = (e.srcElement || e.target).files[0];
	                        if(scope.name =='image' ||  scope.name =='image_popupwindow' || scope.name =='video_popupwindow' || scope.name =='video')
	                        scope.preview_(scope.preview_file);
	                     });
	                  });
	               }
	            };
	         }]);
/*	app.config(function(blockUIConfig) {
  // Tell blockUI not to mark the body element as the main block scope.
  blockUIConfig.autoInjectBodyBlock = false;  
});*/



		/*
		Controller Name : createCtr
		Usage: All the logic is written here
		*/
	app.controller('createCtr', function($scope,$filter,$compile,$window,$http,$timeout,$uibModal, $log, $document,$mdDialog /*,upload,Fullscreen,Upload*/,$q, $anchorScroll, $location,/*blockUI,*/SweetAlert,socket,$sce) {
		// $window.
	$scope.socketurl ="http://ec2-35-165-162-233.us-west-2.compute.amazonaws.com:3000";
	$scope.allapps =[];
	$scope.t_data = [];
	$scope.names =[];
	$scope.sec_container =[];
	$scope.chap_container =[];
	$scope.top_container =[];
	$scope.loop_index =0;
	$scope.expanded= [];
	$scope.search_dd=[];
	$scope.san_= "";
	$scope.success_class="";
	$scope.hide_toggle_sidebar =true;
	$scope.sidebar_class ="success_default";
	$scope.hideicon_class ="";
	$scope.hide_controls =false;
	$scope.addticketcontent = {};
	$scope.pageheight = 0;
	$scope.complete_data ={};
	$scope.is_valid = "yes";
	$scope.todelete = ""; // setting default value
	$scope.delete_status = ""; // setting default value
	$scope.beforesort ={};
	$scope.activeMenu = "";
	$scope.activeMenu1 = "";
	$scope.activeMenu2 = "";
	$scope.section_bc = "";
	$scope.chapter_bc = "";
	$scope.topic_bc = "";
	$scope.content = "";
	$scope.selectedIndex= "";
	$scope.title ="";
	$scope.app={};
	$scope.embedded_code = "";
	$scope.external_link = "";
	$scope.apps_of_selected_topic = {}; // we kept variable that long to make it understood for later
	$scope.is_image_fullscreen = "";  // to check wether image is allowed in full screen or not
	$scope.is_video_fullscreen = "";  // to check wether video is allowed in full screen or not
	$scope.is_zip_fullscreen = "";  // to check wether zip is allowed in full screen or not
	$scope.is_grid_fullscreen = "";  // to check wether grid is allowed in full screen or not
	$scope.is_text_fullscreen = "";  // to check wether text is allowed in full screen or not
	$scope.being_edited_app = ""; // url eligible encrypted id of the app
	$scope.being_edited_app_index = ''; // index of the apps in the apps array of a particular topic
	$scope.typingTimer =""; // in autosave when we start typing timer is set
	$scope.autosave_status =""; // to show the status while autosaving texts.
	$scope.selected_sec_index =""; // when we click on + icon of section to expand tree or when we click on name of the section index is recored
	$scope.selected_chap_index =""; // when we click on + icon of chapter to expand tree or when we click on name of the chapter index is recored
	$scope.app_drop_start_index =""; // when we drag an app index is recored here
	$scope.app_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.section_drop_start_index =""; // when we drag an app index is recored here
	$scope.section_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.chapter_drop_start_index =""; // when we drag an app index is recored here
	$scope.chapter_drop_end_index =""; // when we drop app index is recored where it is droped.
	$scope.search= {};
	$scope.apps_through = '';
	$scope.total_apps =0;
	$scope.currently_searched_data =[];
	$scope.attach_index =""; // when user click on attach app icon index is recorded
	$scope.de_attach_index = "";
	$scope.pagination_status="not_triggered";
	$scope.no_apps=false;
	$scope.success_message ="";
	$scope.startFade ="";
	$scope.isFullscreen = false; // all the apps of selected course will be stored in this topic.
	$scope.hide_all = false;
	$scope.old_edit_value ="";
	$scope.edit_status ="";
	$scope.tree_edit_index ="";
	$scope.is_logo_hidden = false;
	$scope.is_preview_hidden = true;
	$scope.chapter_name = "";
	$scope.restore_fs = true;
	$scope.compress_icon_hidden = true;
	$scope.compress_icon_class = "";
	$scope.compress_highlight_hidden = true;
	$scope.compress_highlight_class ="";
	$scope.fs_through = "";
	$scope.call_edit_after ="";
	$scope.showDetails = false;
	$scope.isclickenable = true;
	$scope.shouldgotop = false;
	$scope.expand_chapter_index = [];
	$scope.expand_topic_index =[];
	$scope.fa_class=[];
	$scope.fa_class_=[];
	$scope.selected_top_index =[];
	$scope.defer = $q.defer();
	$scope.show_tree = 'treereplace_shown';
	$scope.activemenus = [];
	$scope.make_gtp_vi = "";
	$scope.new_apps_class ="treereplace_shown";
	$scope.is_disbaled = false;
	$scope.myFile = 'none';
	$scope.tgl_cntr_loader = "make_hidden";
	$scope.userID ="";
	$scope.beforedrag = [];
	$scope.active = [];
	$scope.count =[];
	$scope.currenttype = "";
	$scope.reminderid = "";
	$scope.pageno = 1;
	$scope.prebtnclass = "none";
	$scope.nxtbtnclass = "none";
	$scope.totalpages = 0;
	$scope.tiggerprev = true;
	$scope.tiggernxt = true;
	$scope.totalrecords = 0;
	$scope.currentrecodsfrom = 0;
	$scope.currentrecodstill = 0;
	$scope.check_it = 0;
	$scope.filter = "none"
	$scope.currentdate = new Date();
	$scope.success = [];
	$scope.fields =[];
	$scope.today ;
	$scope.todaydate ;
	$scope.errors =[];
	$scope.addticketpanelclass ="";
	$scope.all_users=[];
	$scope.type=[];
	$scope.priority=[];
	$scope.users = [];
	$scope.projectid =[];
	$scope.isaddpanelinit = false;
	$scope.mddialog ={};
	$scope.entity = [];
	$scope.tagsarr = [];
	$scope.selectedtags = [];
	$scope.tagsnamesonly = [];
	$scope.triggertagsinput=false;
	$scope.triggerdtpicker = false;
	$scope.triggerdateerror = false;
	$scope.info = [];
	$scope.mandatory = [];
	$scope.hideloader = false;
	$scope.tag_id = 0;
	$scope.ispriorityenabled = true;
	$scope.currentuserData =[];
	$scope.ip = "";
	$scope.priorityindex = 0;
	$scope.search = [];
	$scope.searchval = [];
	$scope.searchkey = 'none';
	$scope.searchdd = [];
	$scope.searchddusers =[];
	$scope.amiadmin='no';
	$scope.currentuserroles =[];
	$scope.searchuserid = 'none';
	$document.ready(function(){
	
	var that = this;
	 $scope.picker3 = {
        date: new Date()
    };
	$scope.openCalendar = function(e, picker) {
	 		console.log("called");
	  	    $scope[picker]['open'] = true;
	};


			 socket.on('reloadticketsok', function (data) {
			 	if($scope.currentuser == data.target){
				 	$scope.infomsgstart('You Have New Ticket','info');
					$timeout(function() {
						$scope.fields.successwrapper = 'default_hidden';
						$scope.get_data('selected','selected','selected','none','selected');
					},3000);	
			 	}
			});
			// sending a request to get topic content inside the scope.
			// local storage was not taking updated json
			$scope.infomsgstart = function(msg,type){
				if(type == 'info'){
					$scope.fields.infowrapper = 'default_visible';
					$scope.info.message = msg;
				} else if(type == 'success'){
					$scope.fields.successwrapper = 'default_visible';
					$scope.success.message = msg;
				}	else if(type == 'error'){
					$scope.fields.errorswrapper = 'default_visible';
					$scope.errors.message = msg;
				}	
			}
			$scope.infomsgclose = function(type){
				if(type == 'info'){
					$scope.fields.infowrapper = 'default_hidden';
				} else if(type == 'success'){
					$scope.fields.successwrapper = 'default_hidden';
				}else if(type == 'error'){
					$scope.fields.errorswrapper = 'default_hidden';
				}
			}
			$scope.get_data = function(type,check_it,filter,ticket_id,reset_pages,pagetype,tag_id,searchkey,searchval,searchuserid){
						$scope.fields.errorswrapper = 'default_hidden';
						$scope.norecordsmsg ="default_hidden";
						$scope.infomsgstart('loading...','info');
						$scope.autosave_status ='Loading Content...';
						console.log($scope.search);
						console.log($scope.searchkey);
						console.log(searchkey);
						(type == 'selected') ? "" : $scope.currenttype = type;
						(check_it == 'selected') ? "" : $scope.check_it = check_it;
						(filter == 'selected') ? "" : $scope.filter = filter;
						(tag_id == 'selected') ? "" : $scope.tag_id = tag_id;
						(searchkey == 'selected') ? "" : $scope.searchkey = searchkey;
						(searchkey == 'selected') ? "" : $scope.searchkey = searchkey;
						(searchuserid == 'selected') ? "" : $scope.searchuserid = searchuserid;
						$scope.pageno = (reset_pages == 'yes')  ? 1 : (reset_pages == 'selected') ? $scope.pageno :  $scope.pageno + 1;
						$scope.currenttype = ($scope.currenttype == 'none') ? 'all' : $scope.currenttype
						//console.log($scope.pageno);
						$http({
						method: 'POST',
						url: 'getOrigin',
						async : true,
						data : { originidentity : $scope.originidentity ,key : ($scope.currenttype == 'all') ? 'none' : $scope.currenttype , check_it: $scope.check_it,filter:$scope.filter,ticket_id : ticket_id, pageno : $scope.pageno, pagetype:pagetype, tag_id : $scope.tag_id, searchkey : $scope.searchkey , searchval : $scope.searchval, searchuserid : $scope.searchuserid },
					}).then(function successCallback(response) {
							$scope.mandatory = response.data.mandatory;
							if(response.data.status == 'success'){
							  $scope.names = response.data.resultData;
							   $scope.process();
							  $scope.userID = response.data.userID;
							  $scope.ip = response.data.ip_address;
							  if(type == 'none'){
							  	$scope.active =[]
							  	$scope.active.all = 'active2';
							  } else if(type == 'selected'){
							  	$scope.active =[]
							  	$scope.active[$scope.currenttype] = 'active2';
							  }else{
							  	$scope.active =[]
							  	$scope.active[type] = 'active2';
							  }
							  $scope.all_users = response.data.all_users;
							 /* angular.forEach(response.data.my_projects, function(v, k) {
										$scope.projectid.push(v);
										console.log(v);
							  });	*/
							   $scope.projectid.push({id: '0', name : 'None'});
							   angular.forEach(response.data.entities, function(v, k) {
										$scope.projectid.push(v);
										
							  });
							   $scope.tagsarr = [];
							   angular.forEach(response.data.tags, function(v, k) {
										$scope.tagsarr.push({label: v.name , Value : v.id });
										$scope.tagsnamesonly.push(v.name.toLowerCase());
							   });
							   $scope.triggertagsinput = true;
							   $scope.triggerdtpicker = true;
							    
							  $scope.type = [
										{k: 'task', v: 'Task'},
										{k: 'issue', v: 'Issue'},
										{k: 'bug', v: 'Bug'},
										{k: 'lead', v: 'Lead'},
										{k: 'request', v: 'Request'}
										];
									$scope.priority =[
										{k: 'normal', v: 'Normal : Should be replied within 24 Hours'},
										{k: 'medium', v: 'Medium : Should be replied within 12 Hours'},
										{k: 'urgent', v: 'Urgent : Should be replied within 02 Hours'},
										{k: 'emergency', v: 'Emergency : Should be replied within 01 Hour'}
										];
									  $scope.searchdata = [
										{k: 'to_name', v: 'Bid By'},
										{k: 'title', v: 'Url Title'},
										{k: 'dates', v: 'Bidding Between'},
										{k: 'created', v: 'Created Between'},
										{k: 'modified', v: 'Modified Between'},
										];
										if($scope.searchkey == 'none' &&  $scope.searchuserid == 'none'){
											$scope.selectedsearch = {k: 'to_name', v: 'Bid By'};
											$scope.searchkey = $scope.selectedsearch.k	
											$scope.searchval = '';
											$scope.showdateinput = false;
								    		$scope.showpriority = false;
								    		$scope.hidetextinput = false;
											//$scope.showdateinput = false; 
										}
										
							 $scope.currentuserroles = response.data.rls.split(',');
								if(($scope.currentuserroles.indexOf("1") >=  0) == true || ($scope.currentuserroles.indexOf("8") >=  0) == true) {
								  $scope.amiadmin ='yes';
								  $scope.searchdata.push({k: 'users', v: 'Users'});
								  //console.log($scope.ticketip);
								}	
										
							  $scope.app.type = {k: 'task' };
							  $scope.search.priority = {k: 'normal' };
							  $scope.app.projectid = {id: '0'};
							  $scope.count.opened = response.data.open;
							  $scope.count.closed = response.data.closed;
							  $scope.count.inprogress = response.data.inprogress;
							  $scope.count.scheduled = response.data.scheduled;
							  $scope.count.mandatory = response.data.mandatory_count;
							  $scope.count.expired = response.data.expired;
							  $scope.count.all = response.data.open + response.data.closed + response.data.inprogress;
							  $scope.pageno = response.data.pageno;
							  $scope.totalpages = response.data.totalpages;
							  $scope.currentrecodsfrom = response.data.currentrecodsfrom;
							  $scope.prebtnclass = (response.data.currentrecodsfrom == 1 ) ?  'disabled' : 'none';
							  $scope.tiggerprev = (response.data.currentrecodsfrom == 1 ) ?  false : true;
							  $scope.currentpage = response.data.currentpage;
							  $scope.totalrecords = response.data.totalrecords;
							  $scope.tiggernxt = ($scope.totalpages ==  $scope.pageno) ?  false : true;
							  $scope.nxtbtnclass = ($scope.totalpages ==  $scope.pageno) ?  'disabled' : 'none';
							  $scope.currentrecodstill = ($scope.totalpages ==  $scope.currentpage) ? response.data.totalrecords : response.data.currentrecodsfrom + 19;
							 
							  //$scope.todaydate = response.data.today;
							var twohours = 2*60*60*1000; // hours*minutes*seconds*milliseconds
							var diffDays = (new Date().getTime() + twohours);
							$scope.app.deadline =  $filter('date')(diffDays, 'medium');
							  $scope.today = response.data.today;
							 
							  if(response.data.resultData.length ==0){
							  	$scope.norecordsmsg ="default_visible";
							  }
							 // console.log($scope.today);
							}


						}, function errorCallback(response) {
							if(response.status > 299){
								$scope.fields.successwrapper = 'default_hidden';
								$scope.errors.message = 'Something went wrong!. Please refresh page and try again. If still facing same issue, Contact Support team';
								$scope.fields.errorswrapper = 'default_visible';
							}
				}).then(function () {
					
				});
			}
			$scope.get_data('open',0,'none','none','yes','none',0 , 'none', $scope.search,'none' ); // calling function onload
			$scope.process = function(){
					$scope.t_data =[];
					angular.forEach($scope.names, function(v, k) {
						var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
						var dateis =  new Date($scope.today);
						dateis.setHours(dateis.getHours() + 5.3);
						var diffDays = (new Date(v.deadline).getTime() - (dateis.getTime()) );
						if(diffDays < 0 ){
							v.isexpired = 'yes';	
						} else {
							v.isexpired = 'no';	
						}
						$scope.t_data.push(v);
						//console.log(v);
					});
						$scope.hideloader = true;
					angular.forEach($scope.all_users, function(v, k) {
						$scope.users.push(v);
						$scope.searchddusers.push({k : v.id  , v : v.first_name+' '+ v.last_name})
						if($scope.currentuser == v.id){
							$scope.currentuserData = v;
						}
					});
					if($scope.searchuserid !== 'none'){
						console.log($scope.searchuserid);
						$scope.search.priority = { k : $scope.searchuserid};
					}
					$scope.app.to_id = {id: $scope.currentuser };
					$scope.autosave_status ='';
					$scope.infomsgclose("info");

					//console.log();
					if(angular.isUndefined($scope.mandatory.eid) == false){
						
						if($scope.mandatory.last_replier !== parseInt($scope.currentuser)){
							$scope.infomsgstart("Please respond to Mandatory ticket(s) first","info");
							$timeout(function(){
								$window.location.href = $scope.base_url + 'tickets/response/' +	$scope.mandatory.eid;
							}, 2000)	
						}
						
						
					}
				
			}
			/**/
					$scope.closetheerror = function(field){
				  		$scope.fields[field]="default_hidden"; 
					}

					$scope.rmelfromarr = function(index,arr){
						arr.splice(index, 1);
				  		return arr;
					}

					/*for setting input value to the */
					$scope.entitychange = function($event){
						var Form_data = { userid : $scope.currentuseridenc , entity_id :  $scope.app.projectid.id };
						var wheretogo = $scope.base_url+'tickets/getValueofEntity';
						var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'getValueofEntity');
					};	
						

					$scope.addticketpopup = function(){
						console.log("Called");
						var el = '';
						var desired_width ='sm';
						var el = $scope.base_url+'tickets/add';
						$scope.open(el,desired_width,'addticket');	

					}
					$scope.getdatabytag = function(id){
						$scope.get_data('selected','selected','selected','none','selected','selected',id,'none','none','none'); // calling function onload
					}
					$scope.togglepriority = function(index){
						$scope.priorityindex = index;
						//$scope.t_data[index].priority = 'medium';
						var priority = $scope.t_data[index].priority;
						if($scope.t_data[index].priority == 'normal'){
							 priority = 'medium';
						} else if($scope.t_data[index].priority == 'medium'){
							 priority = 'urgent';
						} else if($scope.t_data[index].priority == 'urgent'){
							 priority = 'emergency';
						}else if($scope.t_data[index].priority == 'emergency'){
							 priority = 'normal';
						}
						$scope.ispriorityenabled = false;
						var Form_data = { ticket_id : $scope.t_data[index].id , field : 'priority', fielddata : priority , index : index };
						var wheretogo = $scope.base_url + 'tickets/instantsave';
						var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'instantsave');
						$scope.infomsgstart('Saving...','info');
					}
					 $scope.showAlert = function(ev,index) {
					 		console.log(index);
					 		var title =$scope.t_data[index].url_title;
					 		var description =$scope.t_data[index].bidding_date;
					 		SweetAlert.swal( {html:true ,title : title , text : description , type : "success" , allowOutsideClick: true});
					 		//SweetAlert.swal("Done!");
					 		/*SweetAlert.swal({
							  title: "Sweet!",
							  text: "Here's a custom image."
							});*/
							//$scope.trigger_sweet_confirmation(title,"ok",'success','showdes','no');
						  };
												// common model function
						$scope.open = function (desired_html,desired_width,redirect,ev) {
										
										$mdDialog.show({
										controller: DialogController,
										templateUrl: desired_html,
										parent: angular.element(document.body),
										targetEvent: ev,
										clickOutsideToClose: false,
      									escapeToClose: false,
										locals: {
										items: {'originidentity': $scope.originidentity,'scope' : $scope},
										},
										fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
										})
										.then(function(response) {
											if(redirect=='addticket'){
													
											}
										}, function() {

										});
					  };
						function DialogController($scope, $mdDialog,items) {
							$scope.title ="";
							$scope.mddialog = $mdDialog;
							$sc = items.scope;
							 $scope.originidentity = items.originidentity;
								 $scope.hide = function() {
									 $mdDialog.hide();
								 };
								 $scope.answer = function(answer) {
									 $mdDialog.hide(answer);
								 };
								 $scope.ok = function (response) {
									 $mdDialog.hide("ok");
								 };
								$scope.delete = function (response) {
									$mdDialog.hide("delete");
								};
								$scope.drop = function (response) {
									$mdDialog.hide("drop");
								};
								$scope.cancel_drop = function (response) {

									$mdDialog.hide("no_drop");
								};
								$scope.drop_app = function (response) {

									$mdDialog.hide("drop_app");
								};
								$scope.cancel_drop_app = function (response) {
									$mdDialog.hide("no_drop_app");
								};
								$scope.go_attach = function (response) {
									$mdDialog.hide("attach_app");
								};
								$scope.deattach_app = function (response) {
									//$mdDialog.hide("deattach_app");
									var res = $sc.new_app_submit();
									console.log(res);

								};
								$scope.app_submit = function (response) {
									var res = $sc.new_app_submit();
								};
								$scope.close_model = function (response) {
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
									$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;
									$mdDialog.hide("close_dialog");
								};
							 // will be used if cancel button needed to be displayed
								 $scope.cancel = function (response) {
									 $mdDialog.hide('cancel');
								 };
								 $scope.handleenterkey = function(keycode,event,scope){
								 }
							 };
/*
			$scope.get_apps = function (taskId,item) {
				$scope.apps_of_selected_topic = {};
				$scope.tgl_cntr_loader = "make_visible";
				var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page	
				names = $scope.get_names(taskId);
				$scope.selectedIndex= taskId;
				var arr = taskId.split("s");
				arr.splice(0, 1);
				var count = arr.length;
				if($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]].hasOwnProperty('apps') ){
					var content =  $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['apps'];
				} else {
					var content = 'Empty';
				}
				$http({
					method: 'POST',
					url: 'get_apps',
					data: { _token: $window.localStorage.getItem("t") ,'jsoncontent' : content , course_id : $scope.originidentity, location : $scope.location },
					}).then(function successCallback(response) {
						$scope.sortableOptions_apps.disabled =false;
						 $scope.success_class = 'sucess_mesage';
						$scope.success_message = "Loading Content...";
						$timeout(function(){
								$scope.apps_through = 'topic';
								if(response.data.allData !== 'Empty'){
									$scope.apps_of_selected_topic = response.data.allData;	
								} else{
									var html = '<div class ="no_apps" class="back_comm section"><i class="fa fa-times"></i>No Apps to display!</div>';
											  angular.element(document.getElementById('no_apps_messages')).append($compile(html)($scope));// appending input to the page		 
									}
									$location.hash('wrapper'); // taking pointer to the top passed id
									$anchorScroll(); // taking pointer to the top passed id
						}, 100).then(function(){
								$timeout(function() {
									angular.element('pre code').each(function(i, block) {
									hljs.highlightBlock(block);
									});
								});
								$scope.visibility_class = '';
						});
						$timeout(function(){
							$scope.success_message = "";
							$scope.startFade = "";
							 $scope.success_class = 'success_default'; 
							 $scope.tgl_cntr_loader = "make_hidden";
						}, 2000);
					}, function errorCallback(response) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
				});

				
				$scope.activeMenu1 = $scope.activemenus[0];
				$scope.activeMenu2 = $scope.activemenus[1];
				$scope.activeMenu = $scope.activemenus[2];
				$scope.section_bc = ' >> '+names[0];
				$scope.chapter_bc = ' >> '+names[1];
				$scope.topic_bc = ' >> '+names[2];
			};*/
		$scope.ct_apps = function(){
			$scope.get_apps($scope.selectedIndex,'default');
		}
		$scope.section_span = function (taskId,item,index) {
			$scope.expand_chapter_index[index] = ($scope.expand_chapter_index[index] !== true ) ?  true: false;
			$scope.fa_class[index] = ($scope.expand_chapter_index[index] !== true || $scope.expand_chapter_index[index] == undefined ) ? '' : 'fa-minus-circle';
			names = $scope.get_names(taskId);
			$scope.topic_bc = '';
			$scope.chapter_bc = '';
			$scope.section_bc = ' >> '+item;
			$scope.activeMenu1 = taskId;
			$scope.activeMenu2 = "";
			$scope.activeMenu = "";
			$scope.autosave_status ='Loading Content...';
			$scope.selectedIndex ="";
			$scope.apps_of_selected_topic ={};
			$timeout(function(){
					$scope.load_tree_nodes(taskId);
				},100);
		};
		$scope.load_tree_nodes = function(taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			if(count == 1){
				if($scope.names[arr[0]].hasOwnProperty('chapters') ){
					$scope.t_data[arr[0]]['chapters'] = $scope.names[arr[0]]['chapters'];	
				}
				if ($scope.expanded.indexOf(arr[0]) < 0) {
					$scope.expanded.push( arr[0] );
				}
				
				$timeout(function(){
					$scope.autosave_status ='';
				},100);
			} else if(count == 2){
			} else {
			}
			$scope.defer.resolve("loaded");
			return true;
		};
		$scope.chapter_span = function (taskId,item,index) {
			names = $scope.get_names(taskId);
			$scope.topic_bc = "";
			$scope.activeMenu = "";
			//	$scope.expand_topic_index
			$scope.expand_topic_index[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true ) ?  true: false;
			$scope.fa_class_[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true || $scope.expand_topic_index[$scope.selected_sec_index][index] == undefined ) ? '' : 'fa-minus-circle';
			
			$scope.section_bc = ' >> '+names[0];
			$scope.activeMenu1 = $scope.activemenus[0];
			$scope.topic_bc = '';
			$scope.chapter_bc = ' >> '+item;
			$scope.activeMenu2 = $scope.activemenus[1];
			$scope.selectedIndex ="";
			$scope.visibility_class ="";
			$scope.apps_of_selected_topic ={};
		};
		$scope.get_names = function (taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			var activemenus = [];
			$scope.activemenus =[];
			if(count == 1){
				name.push($scope.names[arr[0]]['section']);
				activemenus.push('s'+arr[0]);
				$scope.selected_sec_index = arr[0];
			} else if(count == 2){
				name.push($scope.names[arr[0]]['section']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				activemenus.push('s'+arr[0]);
				activemenus.push('s'+arr[0]+'s'+arr[1]);
			} else {
				name.push($scope.names[arr[0]]['section']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic']);
				activemenus.push('s'+arr[0]);
				activemenus.push('s'+arr[0]+'s'+arr[1]);
				activemenus.push('s'+arr[0]+'s'+arr[1]+'s'+arr[2]);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				$scope.selected_top_index = arr[2];
			}
			$scope.activemenus = activemenus;
			return name;
			};

			/*$scope.new_app = function (name,ev){
						$scope.app ={};
						$scope.is_logo_hidden = false;
						$scope.is_preview_hidden = true;
						$scope.is_disbaled = false;
						if($scope.selectedIndex.length > 0){
							$scope.name = name;
							if(name.length){
						   		var url = $scope.base_url+'/'+name+'/'+$scope.originidentity;
									$scope.trigger_fancybox(url ,'get','none');
								}
						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a Topic first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}

			};*/
		/*	 $scope.onReady = function () {
					var url = $scope.base_url+'/fillboat/'+$scope.being_edited_app;
					$http.get(url).then(function(response) {
						$scope.app.content = response.data.text.content;
					});
			  };
				$scope.trigger_fancybox = function (url,method,data) {
					if(method == 'get'){
						$http.get(url).then(function(response) {
							if (response.status == 200) {
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
							}
							});
					} else {
						$http.post(url,data ).then(function(response) {
							if (response.status == 200) {
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
							}
							});
					}

				 };*/
				$scope.new_app_submit = function (response) {
						if($scope.is_disbaled == false && $scope.isvaliddata($scope.app) == true){
							 $scope.is_disbaled = false;
							 $scope.success_class = 'sucess_mesage';
							$scope.success_message = "Uploading Content...";
							$location.hash('anchor_top');
							$anchorScroll();
						$timeout(function(){
						}, 1000).then(function(){
									var file = $scope.myFile;
									if($scope.enc_status == 'no'){
      								$scope.being_edited_app = $scope.enc;
      							}
								if($scope.name == 'ticket'){
									//console.log($scope.app);
									  $scope.app.to_id = $scope.app.to_id.id;
									  $scope.app.priority = $scope.app.priority.k;
									  $scope.app.projectid = $scope.app.projectid.id;
									  $scope.app.type = $scope.app.type.k;	
									  $scope.app.deadline =  $filter('date')($scope.app.deadline, 'yyyy:MM:dd HH:mm:ss');
									  var uploadUrl = $scope.base_url+"tickets/add";
									  file = 'none';
								} else if($scope.name == 'video'){
									var uploadUrl = $scope.base_url+"/new_videoApp";
									if($scope.app.hasOwnProperty('embedded_code') && /*typeof*/ file == 'none'){
											var file = "none";
									 }else{
										 $scope.app.embedded_code ="";
									 }
								} else if($scope.name == 'zip'){
									var uploadUrl = $scope.base_url+"/new_zipApp";
									if($scope.app.hasOwnProperty('external_link') && /*typeof*/ file == 'none'){
											var file = "none";
									 }else{
										 $scope.app.external_link ="";
									 }
								} else if($scope.name == 'text'){
									var uploadUrl = $scope.base_url+"/new_textApp";
									var file = "none";
								} else if($scope.name == 'image_popupwindow' || $scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
									// edit image aap
										if(/*typeof*/ file == 'none'){ // if no image selected
											file = 'none';
										}
										$scope.app.cai = $scope.being_edited_app;
										var uploadUrl = $scope.base_url+"/"+$scope.name;
								}
                   					$scope.uploadfile(file, uploadUrl,$scope.app ,$scope.name);
						});	
					}
		      							
		      };

		        $scope.isvaliddata = function(app){
		      	var areerrors = false;
				if($scope.name =='ticket'){
					if(angular.isUndefined($scope.app.title)){
						$scope.errors.title = 'Please Fill Out Title';
						$scope.fields.title = 'default_visible';
						areerrors = true;
					} 
				    if(angular.isUndefined($scope.app.deadline)){
						$scope.errors.deadline = 'Please Select Deadline for the Ticket';
						$scope.fields.deadline = 'default_visible';
						areerrors = true;
						//return false;
					}
					 if(angular.isUndefined($scope.app.type)){
						$scope.errors.type = 'Please Select Type for the Ticket';
						$scope.fields.type = 'default_visible';
						areerrors = true;
						//return false;
					}
					if(angular.isUndefined($scope.app.priority)){
						$scope.errors.priority = 'Please Select Priority for the Ticket';
						$scope.fields.priority = 'default_visible';
						areerrors = true;
						//return false;
					}
					if(angular.isUndefined($scope.app.projectid)){
						$scope.errors.projectid = 'Please Select Project ID for the Ticket';
						$scope.fields.projectid = 'default_visible';
						areerrors = true;
						//return false;
					}
					//console.log($scope.hourminformat($scope.app.tasktime));
					if($scope.hourminformat($scope.app.tasktime) == false){
							$scope.errors.tasktime = 'Please Valid Task time i.e 1:00';
							$scope.fields.tasktime = 'default_visible';
							areerrors = true;
						}
					if(angular.isUndefined($scope.app.tasktime)){
						$scope.errors.tasktime = 'Please Select Task Time for the Ticket';
						$scope.fields.tasktime = 'default_visible';
						areerrors = true;
						//return false;
					}
					if($scope.hourminformat($scope.app.billable_hours) == false){
							$scope.errors.billable_hours = 'Please Valid Task time i.e 1:00';
							$scope.fields.billable_hours = 'default_visible';
							areerrors = true;
						}
					if(angular.isUndefined($scope.app.billable_hours)){
						$scope.errors.billable_hours = 'Please Select Billable Hours for the Ticket';
						$scope.fields.billable_hours = 'default_visible';
						areerrors = true;
						//return false;
					}
					if(angular.isUndefined($scope.app.desc)){
						$scope.errors.desc = 'Please enter Description';
						$scope.fields.desc = 'default_visible';
						areerrors = true;
						//return false;
					}
					return (areerrors == false) ? true : false;
				}
				return true
		      }
		       $scope.hourminformat = function(val){
			     var regexp = /^([0-9]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
			          return regexp.test(val);
			  };
		      $scope.onTimeSet = function(newDate, oldDate){
				var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds    
				var diffDays = (new Date(newDate).getTime() - new Date().getTime());
					if(diffDays < 0 ){
						$scope.errors.deadline = 'Deadline must be a future date.By default,it is 2 hours ahead the current time.';
						$scope.fields.deadline = 'default_visible';
						$scope.triggerdateerror = true;
						var twohours = 2*60*60*1000; // hours*minutes*seconds*milliseconds    
						var diffDays = (new Date().getTime() + twohours);
						$scope.app.deadline =  $filter('date')(diffDays, 'medium');
						//$scope.apply();
					} else {
						$scope.errors.deadline = '';
						$scope.fields.deadline = 'default_hidden';
				       $scope.app.deadline = $filter('date')($scope.app.deadline, "medium",'+0530');  // for type="date"	
					}
		      

		      }

					$scope.uploadfile = function(file, uploadUrl,other_data,name){
						 var fd = new FormData();
						 if(file !== 'none'){
								if(name =='image'){
										fd.append('image_app', file);
								} else if(name =='video'){
										fd.append('video_app', file);
								} else if(name == 'zip'){
										fd.append('app_zip', file);
								}else if(name == 'image_popupwindow' || name == 'zip_popupwindow'){
									//console.log(file);
									fd.append('url', file);
								} else if(name == 'video_popupwindow'){
									fd.append('video_app', file);
								}
						 }
						
							angular.forEach(other_data, function(value, key) {
								//console.log(value);
								fd.append(key, value);
							});
					
						 $http.post(uploadUrl, fd, {
								transformRequest: angular.identity,
								headers: {'Content-Type': undefined}
						 })
						 .success(function(response){
							 if(response.status == 'success' || response.status == 'updated' ){
								$scope.aftersuccess(response);
								$mdDialog.hide();

							 }else if(response.status =='failed'){
								$scope.success_class = '';
								$scope.success_message = "";
								 angular.forEach(response.errors, function(value, key) {
									var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
									var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								 var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
								});
								 $scope.is_disbaled = false;
							}

						 }).error(function(){
						 });
						 
					};
			$scope.common_success_meesage = function (message){
				$scope.success_class ="sucess_mesage"
				$scope.success_message = message;
				$timeout(function(){
							$scope.success_message = "";
							$scope.startFade = "";
							 $scope.success_class = 'success_default';
						}, 3000);
			};
			$scope.aftersuccess = function(answer){
				if(answer.status == 'success'){
					if($scope.name == 'ticket'){
						if($scope.is_notified == 'Yes'){
							var Form_data = { from : $scope.currentuser , logtext : 'You have new tickets ', type: 'information', to_id : $scope.app.to_id.toString() ,'entity_id' : 1, created_at : $filter('date')(new Date() , "medium",'+0530')   };
							var wheretogo = $scope.socketurl + '/logactivity';
							var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'logactivity');
							socket.emit('reloadtickets',{ target : $scope.app.to_id.toString() });	
						}
						$scope.app ={};
						$scope.app.to_id = {id: $scope.currentuser };
						$scope.app.type = {k: 'task' };
						$scope.app.priority = {k: 'normal' };
						$scope.app.projectid = {id: '0' };
						$scope.success.message = 'Ticket Added Successfully!'
						$scope.fields.successwrapper = 'default_visible';
						$timeout(function(){
							$scope.get_data('selected','selected','modified','none','selected','none',0,'none','none','none');
						 },500);
					}

				} else if(answer.status == 'updated'){
					if($scope.name == 'image_popupwindow'){
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = "";
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;

					}else if($scope.name == 'video_popupwindow' || $scope.name == 'zip_popupwindow' || $scope.name == 'text_popupwindow'){
						taskId = $scope.selectedIndex;
						var item = $scope.get_names(taskId);
						$scope.get_apps(taskId, item[2] );	
						$scope.common_success_meesage("App Edited Successfully!");
					}
					//$scope.being_edited_app_index

				}
				$scope.is_logo_hidden = false;
				$scope.is_preview_hidden = true;
				//$.fancybox.close();
				$scope.app ={};
				$scope.myFile ='none'
			};

				/*
				this function is called edit text app.
				*/
				$scope.autosave = function() {
						var doneTypingInterval = 800;  //time in ms, 0.8 second for example
						$timeout.cancel($scope.typingTimer);
						 $scope.typingTimer = $timeout($scope.doneTyping, doneTypingInterval);
	            }
				$scope.doneTyping = function (){
					var text_content = $scope.app.content;
					var Form_data = { _token: $window.localStorage.getItem("t") , content : text_content , origin: $scope.being_edited_app, title: $scope.app.title, through : $scope.app.through , course_id : $scope.originidentity, location : $scope.location } ;
					var wheretogo = 'autosave_text';
					$scope.autosave_status = "saving...";
					var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'autosave');
				}
				$scope.commonPostFunc = function(wheretogo,FormData,task){
									$http({
									method: 'POST',
									url: wheretogo,
									async : true,
									data : FormData
								}).then(function successCallback(response) {
									if(task == 'drag_ticket'){
										SweetAlert.swal("Done!");
									} else if (task == 'ticindex'){
										if(response.data.status =='success' ){
											SweetAlert.swal("Reminder Sent!");
										}
									} else if(task == 'ticketresponse'){
										if(response.data.status =='success' ){
										}
									} else if (task == 'getValueofEntity'){
										if(response.data.response =='success' ){
											if($scope.app.projectid.id == 2){
												$scope.entity.showhide = 'default_visible';
												$scope.entity.title = 'Please Select Project';
												$scope.entity['value'] = [];
												angular.forEach(response.data.value, function(v, k) {
															console.log(v.id);
															console.log(v.project_name);
															$scope.entity['value'].push({id : v.id , name : v.project_name });
												  });	
												console.log($scope.entity.value);
											} else if($scope.app.projectid == 4){
												$scope.entity.title = 'Please Select Lead';
											}
										}
									} else if(task == 'instantsave'){
										if(response.data.response =='success' ){
											var savetolog ='yes'
											$scope.t_data[FormData.index].priority = FormData.fielddata;
											$scope.infomsgclose('info');
											$scope.infomsgstart('Ticket Priority Changed to '+FormData.fielddata+'','success');
											$timeout(function() {
												$scope.infomsgclose('success');
												$scope.ispriorityenabled = true;
												if(savetolog == 'yes'){
													$scope.infomsgstart('Saving to Logs...','info'); 
													var Form_data = { ticket_id : $scope.t_data[FormData.index].idp , replier_id : 0, response_text: 'Ticket Priority Changed To '+ FormData.fielddata +' By '+$scope.currentuserData.first_name+' '+$scope.currentuserData.last_name+' ' , ip_address : $scope.ip , created_at : $filter('date')(new Date() , "medium")   };	
													var wheretogo = $scope.socketurl + '/addtodescussion';
													var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'addtodescussion');	
												}
											}, 1000);

											
											
										};
									} else if (task == 'addtodescussion'){
										$scope.infomsgstart('Saved!','info');
										var Form_data = { from : $scope.currentuser ,ticket_type : 'response', logtext : FormData.response_text+'('+ $scope.t_data[$scope.priorityindex].title +')' , type: 'information', to_id : parseInt($scope.t_data[$scope.priorityindex].from_id) ,'entity_id' : 1, entity_value : $scope.t_data[$scope.priorityindex].idp , ip_address : $scope.ip, created_at : $filter('date')(new Date() , "medium",'+0530')   };
										var wheretogo = $scope.socketurl + '/logactivity';
										var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'logactivity');		
										 $timeout(function(){
										 	$scope.infomsgclose('info');
						 				},200);
									}

										$scope.tgl_cntr_loader = "make_hidden";
									}, function errorCallback(response) {
							});
					}

				$scope.search_apps = function(){
					$scope.sortableOptions_apps.disabled =true;
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.selectedIndex.length > 0){
							$scope.currently_searched_data = $scope.search; // because $scope.search is bound to input
							if($scope.search.app == undefined){
								var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select App from drop-down first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

							} else if($scope.search.val == undefined){
								var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please fill the keyword to search!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
								var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page								
							}else {
								$scope.total_apps = 0;									// for safer side we store it in another variable.					
								Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.search.val , app : $scope.search.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' , of_ : $scope.total_apps}
								var wheretogo = 'search_apps';
								$scope.apps_of_selected_topic =[];
								$scope.tgl_cntr_loader = "make_visible";
								$scope.commonPostFunc(wheretogo,Form_data,'search_apps'); // sending a post request
							}
						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a Subject first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}
				};
				$scope.attach = function(index,id){
					$scope.attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>Attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="go_attach('+"attach"+')">Attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_attach');

				};
				$scope.deattach = function(index,id){
					$scope.de_attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>De-attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="deattach_app('+"de-attach"+')">De-attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_de-attach');
				};
				$scope.paginate_apps = function(index){
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.total_apps > 9 &&  $scope.total_apps - 3 <= index && $scope.pagination_status !== 'triggered' &&  $scope.apps_through !== 'topic'){
					var wheretogo = 'search_apps';
					Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' ,of_ : $scope.total_apps}
					$scope.tgl_cntr_loader = "make_visible";
					$scope.commonPostFunc(wheretogo,Form_data,'paginate'); // sending a post request
					}
				};
			$scope.toggleFullScreen = function() {
					if($scope.isFullscreen == false){
						$scope.fs_through = 'fullscreen';
						$scope.compress_icon_class = '';
						$scope.isFullscreen = !$scope.isFullscreen;
						$timeout(function(){
							$scope.hide_all = true;
							$scope.sortableOptions_apps.disabled =true;
							angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						}, 200).then(function(){
							$scope.compress_icon_hidden = !$scope.compress_icon_hidden;
						});
					} else {
						$scope.compress_icon_class = 'compress_icon_hide';
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						$timeout(function(){
							$scope.isFullscreen = !$scope.isFullscreen;
						}, 200).then(function(){
						});
					}
				}

				$scope.toggle_sidebar = function(){
					if($scope.hide_toggle_sidebar == true){ // means side bar is just hidden
							$scope.hide_toggle_sidebar = !$scope.hide_toggle_sidebar;
							$timeout(function(){
							angular.element(document.getElementsByTagName('body')).animate({ scrollTop: 0 }, 200); /* taking user to the top of the page*/
							}, 500).then(function(){
								angular.element(document.getElementsByClassName('section-right')).addClass("section_animated");
								angular.element(document.getElementsByClassName('section-right')).addClass("section-right1");
								$scope.restore_fs = !$scope.restore_fs;
							});
						} else{ // means side bar just shown
							$scope.restore_fs = !$scope.restore_fs;
							angular.element(document.getElementsByClassName('section-right')).removeClass("section-right1");
							$timeout(function(){
								angular.element(document.getElementsByClassName('section-right')).removeClass("section_animated");
								$scope.hide_toggle_sidebar = !$scope.hide_toggle_sidebar;
								
							}, 500);
							
						}
				
			};

				$scope.makefullscreen_app = function(index) {
					if(Fullscreen.isEnabled()){
						Fullscreen.cancel();
					}else{
						$scope.fs_through = 'highlight';
						$scope.compress_highlight_class ="";
						Fullscreen.enable( document.getElementById('a'+index) );
						angular.element(document.getElementsByClassName('back_comm')).addClass("override_overflow");
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						$scope.compress_highlight_hidden = !$scope.compress_highlight_hidden; 
					}
				}
				$scope.stopprop= function(){
					//Fullscreen.cancel();
				}

				$scope.preview_ = function(response){
						var file = $scope.myFile;
						var reader = new FileReader();
						reader.readAsDataURL(file);
						$scope.is_logo_hidden = true;
						$scope.is_preview_hidden = false;
						reader.onload = function(e) {
							$scope.$apply(function(){
							$scope.preview_img = e.target.result;
						});
					 }
				}
				
			$scope.showmenu = function(){
				$scope.showDetails = !$scope.showDetails;
			}
			$scope.hidemenu = function(){
				$scope.showDetails = false;
			}
			$scope.gototop = function(){
				//angular.element(document.getElementsByTagName('body')).animate({ scrollTop: 0 }, 200); /* taking user to the top of the page*/
				$location.hash('wrapper'); // taking pointer to the top passed id
				$anchorScroll(); // taking pointer to the top passed id
				$scope.make_gtp_vi ="";
			}
			$scope.updateUser = function(data){
				console.log(data);
				$scope.get_data('none',0,'none',data,'yes'); // calling function onload
			};
			$scope.setreminder= function(id){
				$scope.reminderid = id;
				$scope.trigger_sweet_confirmation('Are You Sure To Send Reminder?',"Yes!Send It",'warning','ticindex','yes');
			}
			$scope.ticketresponse= function(id){
				var wheretogo = 'ticketresponse';
				Form_data = {origin:id}
				/*$window.location.href = $scope.base_url + 'tickets/ticketresponse/' + id;*/
				$window.location.href = $scope.base_url + 'tickets/response/' + id;
				//var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'ticketresponse');
			}
		    $scope.searchselectchng = function(selected){
		    	$scope.selectedsearch = selected;
		    	if($scope.selectedsearch.k == 'dates' || $scope.selectedsearch.k == 'created' || $scope.selectedsearch.k == 'modified'){
		    		$scope.showdateinput = true;
		    		$scope.showpriority = false;
		    		$scope.hidetextinput = true;
		    	} else if($scope.selectedsearch.k == 'priority'){
		    		$scope.showdateinput = false;
		    		$scope.showpriority = true;
		    		$scope.hidetextinput = true;
		    		$scope.searchdd = $scope.priority;
		    		$scope.search.priority = {k : $scope.priority[0].k }
		    	} else if($scope.selectedsearch.k == 'users'){
		    		$scope.showdateinput = false;
		    		$scope.showpriority = true;
		    		$scope.hidetextinput = true;
		    		$scope.searchdd = $scope.searchddusers;
		    		$scope.search.priority = {k : $scope.searchddusers[0].k }
		    	} else {
		    		$scope.showdateinput = false;
		    		$scope.showpriority = false;
		    		$scope.hidetextinput = false;
		    	} 
		    	//console.log(selected);
		    }
		    $scope.searchresults = function(){
		    	console.log($scope.selectedsearch.k);
		    	$scope.searchkey = $scope.selectedsearch.k;
		    	if($scope.selectedsearch.k == 'to_name' || $scope.selectedsearch.k == 'from_name' || $scope.selectedsearch.k == 'title' || $scope.selectedsearch.k == 'ticket_id'){
		    		
		    		isvalid = $scope.validatefield($scope.search ,'searchval' , 'Search Field' );
		    	} else if($scope.selectedsearch.k == 'dates' || $scope.selectedsearch.k == 'created' || $scope.selectedsearch.k == 'modified'){
		    		//console.log($scope.search);
		    		isvalid = $scope.validatefield($scope.search ,'startdate' , 'Search Field' );
		    		if(isvalid){
		    			isvalid = $scope.validatefield($scope.search ,'enddate' , 'Search Field' );	
		    		}
		    	} else if ($scope.selectedsearch.k == 'priority'){
		    		isvalid = true;
		    	} else if ($scope.selectedsearch.k == 'users'){
		    		isvalid = true;
		    	}
		    	//console.log(isvalid);
		    	if(isvalid){
		    		if($scope.searchkey == 'dates' || $scope.selectedsearch.k == 'created' || $scope.selectedsearch.k == 'modified'){
		    				$scope.searchval = {startdate : $scope.search['startdate'] ,  enddate : $scope.search['enddate']};	
		    				$scope.get_data('selected','selected','selected','none','selected','selected','selected', $scope.searchkey , $scope.searchval,'none' ); // calling function onload	
		    		} else if($scope.searchkey == 'ticket_id'){
		    			$scope.get_data('selected','selected','selected', $scope.search['searchval'] ,'selected','selected','selected', 'none' , 'none','none' ); // calling function onload	
		   			 			//$scope.searchval = {startdate : $scope.search['startdate'] ,  enddate : $scope.search['enddate']};
		    		}  else if ($scope.selectedsearch.k == 'priority'){
		    			//isvalid = true;
		    			//console.log($scope.search['priority'].k);
		    			$scope.get_data('selected','selected', $scope.search['priority'].k ,'none','selected','selected','selected','selected','selected' ,'none')
		    		} else if ($scope.selectedsearch.k == 'users'){
		    			//$scope.searchval = {searchval : $scope.search['priority']};	
		    			$scope.get_data('selected','selected','selected','none','selected','selected','selected', 'none' ,'none',$scope.search['priority'].k ); // calling function onload	
		    			//isvalid = true;
		    			//console.log($scope.search['priority'].k);
		    			//$scope.get_data('selected','selected', $scope.search['priority'].k ,'none','selected','selected','selected','selected','selected')
		    		} else {
		    			$scope.searchval = {searchval : $scope.search['searchval']};	
		    			$scope.get_data('selected','selected','selected','none','selected','selected','selected', $scope.searchkey , $scope.searchval,'none' ); // calling function onload	
		    		}
		    		
		    	}
		    }
		    $scope.validatefield = function(obj,field, facevalue){
		    		var isvalid = true;
		    		console.log(obj[field]);
		    		if(obj !== 'none' ){
		    			if(angular.isUndefined(obj[field])){
							$scope.infomsgstart(''+facevalue+' can not be left Empty!','error');
							isvalid = false;
						} 	
		    		}
					
				    return 	isvalid;
		    }
				// sortable options for sections needed to be called differently because these are different
				$scope.sortableOptions = {
					connectWith: ".alltickets",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						/*$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));*/
						$scope.beforedrag = $scope.t_data;
						ui.item.startPos = ui.item.index();
						$scope.section_drop_start_index = ui.item.index();
						console.log($scope.section_drop_start_index);
						$scope.currently_dragging = "sections";
						ui.item.hide();
						var text = $scope.names[ui.item.startPos]['title'];
						ui.placeholder.html("<td colspan='8' class='hidden-xs'><span>"+text+"</span></td>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");
					},
					update:  function(event, ui){
						$scope.section_drop_end_index = ui.item.sortable.dropindex;
						console.log($scope.section_drop_end_index);
						var desired_html = '<div id="openModalss" style="height:170px;width:300px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
						desired_width='sm';
						$scope.trigger_sweet_confirmation('Are You Sure To Drop?',"Drop It",'warning','drag_ticket','yes');
					},

				};
			$scope.trigger_sweet_confirmation	= function(thetitle,thebuttontext,thetype,from_,runajax){
					SweetAlert.swal({
						   title: /*"Are you sure?"*/ thetitle,
						   text: 'After confirmation changes will be made instantly',
						   type: thetype,
						   showCancelButton: true,
						   confirmButtonColor: "#DD6B55",
						   confirmButtonText: thebuttontext,
						   closeOnConfirm: false}, 
						function(isConfirm){ 
						   if(isConfirm == true ){
						   		if(from_ == 'drag_ticket'){
						   			if(runajax == 'yes'){
						   				if($scope.section_drop_start_index < $scope.section_drop_end_index ){
										console.log("sorting up to down");
										var afterorbefore = 'after';
									} else {
										var afterorbefore = 'before';
									}
										var other_ticket = $scope.names[$scope.section_drop_end_index]['id'];
										var dropped_ticket = $scope.names[$scope.section_drop_start_index]['id'];
										Form_data = {afterorbefore:afterorbefore,other_ticket:other_ticket,dropped_ticket:dropped_ticket};
										console.log(Form_data);
										var wheretogo ='reordertickets';
										$scope.commonPostFunc(wheretogo,Form_data,'drag_ticket'); // sending a post request
						   			}
									
						   		} else if(from_ == 'ticindex'){
									var wheretogo = 'getOrigin';
									Form_data = {reminderid:$scope.reminderid}
									var autosave =	$scope.commonPostFunc(wheretogo,Form_data,'ticindex');
						   		}
						   }
						});
				};

				$scope.sortableOptions_chapters = {
					connectWith: ".chapters",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						$scope.chapter_drop_start_index = ui.item.index();
						$scope.currently_dragging = "chapters";
						ui.item.hide();
						var text = ui.item.context.innerText;

						ht = "<div class ='drag_div'><span>"+$scope.chapter_name+"</span></div>"
						$compile(ht)($scope);
						ui.placeholder.html(ht);
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.chapter_drop_end_index =ui.item.sortable.dropindex;
							//event.stopImmediatePropagation();
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}


				};
				$scope.chap_name = function(name){
					$scope.chapter_name = name;
				};
				$scope.sortableOptions_topics = {
					connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 2,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						ui.item.hide();
						ui.placeholder.html("<div class ='drag_div'><span>"+ui.item.text().trim()+"</span></div>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");	
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							//event.stopImmediatePropagation();
							$scope.currently_dragging = "topics";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}
				};

				$scope.sortableOptions_apps = {
					//connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 10,
					opacity: 0.8,
					disabled: false,
					placeholder: "back_comm_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag_apps",JSON.stringify($scope.apps_of_selected_topic));
						ui.item.startPos = ui.item.index();
						$scope.app_drop_start_index = ui.item.index();
						ui.item.hide();
						var text = $scope.apps_of_selected_topic[ui.item.startPos]['title'];
						ui.placeholder.html("<div class ='drag_div'><span>"+text+"</span></div>");
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.app_drop_end_index = ui.item.sortable.dropindex;
							event.stopImmediatePropagation();
							$scope.currently_dragging = "apps";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop_app('+"drop_app"+')">Drop</button><button class="cancel" ng-click="cancel_drop_app('+"no_drop_app"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop_app',event);
						}

					}
				};	// ended here
		});

    } );
		// directive is used to display apps when get_apps is called.
		app.directive('apipeline', function($compile,$sce,$timeout) {

			/**/
			return {
    		link: function(scope, element,attrs) {
			 //console.log(scope.adddata.type);
			// alert(scope.adddata.type);
			 console.log(scope.adddata.type);
			 var el = angular.element('<div class="row">'); 
			if(scope.adddata.type == 'add'){
				el.append('<form action="add">');
				fieldname = 'title';
				el.append('<div class="col-sm-12 col-lg-12 col-xs-12"><div class="form-group"><input type="text" name="title" class="title form-control" placeholder="Title:" ng-model="app.title"><div class="alert alert-danger default_hidden" ng-class="fields.title"><button type="button" class="close" ng-click ="closetheerror('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.title&^&</div><div class="error_title"></div></div></div>');
				fieldname = 'to_id';
				el.append('<div class="col-sm-4 col-lg-4 col-xs-4"><div class="form-group"><select name="app" class="form-control" ng-options="option.first_name for option in users track by option.id"  ng-model="app.to_id"></select><div class="alert alert-danger alert-dismissable default_hidden" ng-class="to_id"><button type="button" class="close" ng-click ="closetheerror('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.to_id&^&</div><div class="error_to"></div></div></div>');
				fieldname = 'task';
				el.append('<div class="col-sm-4 col-lg-4 col-xs-4"><div class="form-group"><select name="app" class="form-control" ng-options="option.v for option in type track by option.k"  ng-model="app.type"></select><div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.type"><button type="button" class="close" ng-click ="closetheerror('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.type&^&</div></div></div>');
				fieldname = 'priority';
				el.append('<div class="col-sm-4 col-lg-4 col-xs-4"><div class="form-group"><select name="app" class="form-control" ng-options="option.v for option in priority track by option.k"  ng-model="app.priority"> </select><div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.priority"><button type="button" class="close" ng-click ="closetheerror('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.priority&^&</div></div></div>');
				fieldname = 'deadline';
				var dropdown2 = '#dropdown2';
				el.append('<div class="col-sm-4 col-lg-4 col-xs-4"><div class="form-group"><div class="dropdown"><a class="dropdown-toggle" id="dropdown2" role="button" data-toggle="dropdown" data-target="#" href="#"><div class="input-group"><input type="text" placeholder="Deadline" id="" class="form-control" data-ng-model="app.deadline" ><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div></a><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"><datetimepicker data-ng-model="app.deadline" data-datetimepicker-config="{ dropdownSelector: '+ dropdown2 +'}" data-before-render="startDateBeforeRender($dates)" data-on-set-time="onTimeSet(newDate, oldDate)"/></ul></div><div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.deadline"><button type="button" class="close" ng-click ="closetheerror('+fieldname+')" aria-hidden="true">&times;</button>&^&errors.deadline&^&</div></div></div>');
				el.append('</form>');
			}
			el.append('</div>');

			$compile(el)(scope);
			element.append(el);
			//scope.$apply();
		}
  			};
	/*return {
		replace: true,
		link: function(scope, element,attrs) {
			//var el = angular.element('<div ng-mouseenter="paginate_apps($index)" ng-click="stopprop()"></div>');
			//var fullscreen = scope.base_url+'/images/full.png';
			 console.log(scope.appdata.type);
			if(scope.appdata.type == 'add'){
				// image app
				//$compile(fulls_ht)(scope);
				el.append('<div class="row">Hello, It is working</div>');
			} 
			$compile(el)(scope);
			element.append(el);



		}
	}*/
});

/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('asText', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
											link: scope.autosave(event.which,event,scope);
											return false;


            });

        };
    });
app.animation('.success_message_here', function() {
  return {
    enter: function(element, done) {
      //element.css('display', 'none');
      //element.fadeIn(5000, done);
      return function() {
        element.stop();
      }
    },
    leave: function(element, done) {
      element.fadeOut(5000, done)
      return function() {
        element.stop();
      }
    }
  }
});
app.directive('resfs', function() {
	return {
    template: '<a id="showData" class="callMe showData" href={{url("/")}} ng-click="toggle_sidebar();$event.preventDefault();$event.stopPropagation()" ng-class="hideicon_class"><i class="fa fa-arrow-circle-left arrow" ng-hide ="restore_fs"></i></a>'
  };

});
app.directive('compressicon', function() {
	return {
    template: '<a href="#" class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="toggleFullScreen();$event.preventDefault();$event.stopPropagation()" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"><i class="fa fa-compress" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"></i></a>'
  };

});
app.directive('hidemenu', function ($window) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var el = element[0];
      // if there is clicked anywhere on the document that will be triggered.
      angular.element($window).bind('click', function(){
      		scope.showDetails = false;
      		 scope.$apply();
      });
    } 

  };

});
app.directive('enteronbutton', function() {
        return function(scope, element, attrs) {
            element.on("keydown", function(event) {

							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.handleenterkey(event.which,event,scope);
											return false;
									}
					} 
            });

        };
    });
		// add new app

app.directive('prevententer', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.new_app_submit();
											return false;
									}
							} 

            });

        };
    });

app.directive('bootstrapselectpicker', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
        	console.log("ddddtrigggere");
          //	$(element).selectpicker();
        }
    };
});

/*app.directive('uidialog', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
        	 $( element).dialog({
			      modal: true,
			      buttons: {
			        Ok: function() {
			          $( this ).dialog( "close" );
			        }
			      }
			    });
        }
    };
});
*/
 
app.directive('scrolly', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var raw = angular.element(document.body);
            
            var body = document.body,
				html = document.documentElement;
            $timeout(function(){
            $document.bind('scroll', function () {
			    scroll_top = Math.max( body.scrollTop, html.scrollTop);
			    height = Math.max( body.scrollHeight, html.scrollHeight);
				var offset = Math.max( body.offsetHeight, html.offsetHeight,$window.pageYOffset );
                if(scroll_top + offset > 1200){
                	scope.make_gtp_vi = 'make_less_opacity';	
                	scope.shouldgotop = true;
                	if (scroll_top + offset >= height ) {
                }
                 scope.$apply(attrs.scrolly);
           	 	}else if(scroll_top + offset < 1200){
                	scope.make_gtp_vi = '';	
                	scope.shouldgotop = false;
                	 scope.$apply(attrs.scrolly);
                }

            	});
           },500);
        }
    };
});

//}