
// start code to add company name 
var app =angular.module('companyApp',[]);




app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('///');
    $interpolateProvider.endSymbol('///');
  });
//directive for company name
app.directive('handleCom', function() {
     return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13) {
											link: scope.saveButton(event.which,event,scope);
											return false;
									}
							}
               });
        };
    });		

//directive for company tagline
app.directive('handleTag', function() {
     return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13) {
											link: scope.saveButtontag(event.which,event,scope);
											return false;
									}
							}
            });
        };
    });	

//directive for company tagline
app.directive('handleUrl', function() {
     return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13) {
											link: scope.saveButtonurl(event.which,event,scope);
											return false;
									}
							}
            });
        };
    });	

//directive for company tagline
app.directive('handleOVer', function() {

     return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13) {
											link: scope.saveButtonover(event.which,event,scope);
											return false;
									}
							}
            });
        };
    });	


app.controller('CompanyController', function($scope, $sce,$document,$http,$window,$compile,$timeout) {
// define varaible as empty before use it 
$scope.theval='';
// that is for company name 
 $scope.procedures = [{icon: $sce.trustAsHtml('<input type="button" value="Add"  id="edit1" class ="comp_btn TaglineCLass"/>')}];
 // end
// start code for the company name 
	 $scope.showButton = function(type){
	 	$scope.thenameoldvalue=$scope.thename;
      angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
			$scope.show = true;
			$scope.save = false;
			$scope.cancel = false;
			// append the input box with company name 
			var html='<input type="text" id="company_name2"  name="name"  ng-model ="thename" required  handle-com>';
			var appnd = angular.element(document.getElementsByClassName("name_company")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("name_company")).append($compile(html)($scope));// appending input to the page
			 var element = $window.document.getElementById("company_name2");
             element.focus();
			// append the save and cancel button with ng click 
			var html='<button type="submit" id="buttonSaveEmai" class ="comp_btn" ng-click="saveButton()"  ng-hide="save">Save</button><button type="submit" id="Cancel_company"  class ="comp_btn" ng-click="cancelButton()"   ng-hide="cancel">Cancel</button>';
			var appnd = angular.element(document.getElementsByClassName("buttons")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("buttons")).append($compile(html)($scope));// appending input to the page

	   };

	   //=============START CODE ==============
	   $scope.saveCompanynamerequest = function() {
	        	 angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
         var html='<div class="success_message_here"><p class="sucess_mesage">Please wait...</p></div>';
    var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
	var appnd = angular.element(document.getElementsByClassName("all_success")).append($compile(html)($scope));// appending input to the page
  if($scope.thename ==  undefined){
		$company="";
	}else{
		$company=$scope.thename;
	}
 $document.ready(function(){
						$http({
					  method: 'POST',
					  url: 'company_data2',
						data : { _token: $window.localStorage.getItem("t"), company_name : $company}
					}).then(function successCallback(response) {
                  if(response.data.status == 'success'){
                  	 angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						$scope.procedures = [{icon: $sce.trustAsHtml('<i class="fa fa-pencil company_button" title="edit"></i>')}];
						 $scope.show = false;
					   $scope.save = true;
				 	   $scope.cancel = true;
						 var html='<h3>'+response.data.name+'</h3>';
						 var appnd = angular.element(document.getElementsByClassName("name_company")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("name_company")).append($compile(html)($scope));// appending input to the page
				        var html='<div class="success_message_here"><p class="sucess_mesage">Data Saved Sucessfully.</p></div>';
				        var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
					    var appnd = angular.element(document.getElementsByClassName("all_success")).append($compile(html)($scope));// appending input to the page
                               $timeout(function(){
				                  angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
									 },500);
				}else if(response.data.status =='failed'){
					if(response.data.errors=="numbers"){
					   var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
							 $scope.save = false;
 	                         $scope.cancel = false;
 	                       	// append the input box with company name 
							var html='<input type="text" id="company_name2" name="name"  ng-model ="thename" value="fill out some data" required handle-com>';
							var appnd = angular.element(document.getElementsByClassName("name_company")).html($compile(" ")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName("name_company")).append($compile(html)($scope));// appending input to the page
				 	       // append the save and cancel button with ng click 
		                  var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please Fill Out Valid Data<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                           var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						   var appnd = angular.element(document.getElementsByClassName("error_gp")).append($compile(html)($scope));// appending input to the page
					      	}else{
					    var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
					      	var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
					         $scope.save = false;
 	                         $scope.cancel = false;
 	                       	// append the input box with company name 
							var html='<input type="text" id="company_name2" name="name"  ng-model ="thename" value="fill out some data" required handle-com>';
							var appnd = angular.element(document.getElementsByClassName("name_company")).html($compile(" ")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName("name_company")).append($compile(html)($scope));// appending input to the page
				 	       // append the save and cancel button with ng click 
		                  var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors.company_name+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                           var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						   var appnd = angular.element(document.getElementsByClassName("error_gp")).append($compile(html)($scope));// appending input to the page
                        }
	               }
             }, function errorCallback(response) {
								});
						});
}
	   //===============END CODE ===============


// when click on save button then ajax request send 
$scope.saveButton = function(keycode,event,scope) {
   if(keycode === 13){ // if enterpressed
       $scope.saveCompanynamerequest();
     }else{
       $scope.saveCompanynamerequest();
     }
  }
  // when we click on cancel button 
  $scope.cancelButton = function() {
  	$scope.thename=$scope.thenameoldvalue;
  	 angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
  	var html='<h3>'+$scope.thename+'</h3>';
 var appnd = angular.element(document.getElementsByClassName("name_company")).html($compile(" ")($scope));// appending input to the page
 var appnd = angular.element(document.getElementsByClassName("name_company")).append($compile(html)($scope));// appending input to the page
   $scope.show = false;
    $scope.save = true;
	$scope.cancel = true;
  }



//===============================================================


// start that is for company tagline
$scope.pro = [{icon: $sce.trustAsHtml('<input type="button" value="Add" onclick="" id="edit" class ="edit2">') }];
// that is for end company tagline
// start code for the company tagline 
	 $scope.showButtonTagline = function(type){
	 $scope.thenametaglineoldvalue=	$scope.thenametagline;
	 	 angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
	 	 if($scope.thenametagline==""){
      $scope.thenametagline="Add a nice Tagline of your Company profile.";
    }
			$scope.showtagline = true;
			$scope.savetag = false;
			$scope.canceltag = false;
			// append the input box with company name 
			var html='<input type="text"  id="tagline2" name="tagline" ng-model ="thenametagline" required handle-tag>';
			var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
			 var element = $window.document.getElementById("tagline2");
             element.focus();
			// append the save and cancel button with ng click 
			var html='<button type="submit" ng-click="saveButtontag()"  ng-hide="savetag" id="buttonSaveE" class ="comp_btn">Save</button> <button type="submit" id="Tagline_company" ng-click="cancelButtontag()"   ng-hide="canceltag"  class ="comp_btn">Cancel</button>';
			var appnd = angular.element(document.getElementsByClassName("buttonstag")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("buttonstag")).append($compile(html)($scope));// appending input to the page

	   };


  // when we click on cancel button  of tagline 
  $scope.cancelButtontag = function() {
  	$scope.thenametagline=$scope.thenametaglineoldvalue;
 angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
  	var html='<h5>'+$scope.thenametagline+'</h5>';
    var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
    var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
    $scope.showtagline = false;
    $scope.savetag = true;
	$scope.canceltag = true;
  }

 // end code of when we click on cancel button  of tagline 

 // ==========================================
 // start common function for send request on when press enter 
 $scope.sendrequest = function() {

	angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
	var html='<div class="success_message_here"><p class="sucess_mesage">Please wait...</p></div>';
    var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
	var appnd = angular.element(document.getElementsByClassName("all_success")).append($compile(html)($scope));// appending input to the page

if($scope.thenametagline ==  undefined){
		$companytag="";
	}else{
		$companytag=$scope.thenametagline;
	}
   $document.ready(function(){
						$http({
					  method: 'POST',
					  url: 'tagline2',
						data : { _token: $window.localStorage.getItem("t"), tagline : $companytag}
					}).then(function successCallback(response) {
				 if(response.data.status == 'success'){
				 	 var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
							$scope.pro = [{
					            icon: $sce.trustAsHtml('<i class="fa fa-pencil tagline_button" title="edit"></i>'),
				             }];
                          $scope.showtagline = false;
					      $scope.savetag = true;
					 	  $scope.canceltag = true;
						  var html='<h5>'+response.data.tagline+'</h5>';
						  var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
						  var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
                         var html='<div class="success_message_here"><p class="sucess_mesage">Data Saved Sucessfully.</p></div>';
				         var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
					     var appnd = angular.element(document.getElementsByClassName("all_success")).append($compile(html)($scope));// appending input to the page
                               $timeout(function(){
				                  angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
									 },500);
                  }else if(response.data.status =='failed'){
                      var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
                     var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
	                  	if(response.data.errors=="first_create your company"){
	                  		$scope.pro = [{  icon: $sce.trustAsHtml('<input type="button" value="Add" onclick="" id="edit" class ="edit2">')}];
							$scope.showtagline = false;
						    $scope.savetag = true;
						 	$scope.canceltag = true;
					 	 var html='<h5>Add a nice Tagline of your Company profile.</h5>';
						 var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
					     var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("error_gp")).append($compile(html)($scope));// appending input to the page
					      	}else if(response.data.errors=="numbers"){
					      	 var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
							    $scope.savetag = false;
						         $scope.canceltag = false;
						         // append the input box with company name 
							var html='<input type="text"  id="tagline2" name="tagline" ng-model ="thenametagline" value="fill out some data" required handle-tag>';
							var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
								// append the save and cancel button with ng click 
		                 var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please Fill Out Valid Data<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("error_gp")).append($compile(html)($scope));// appending input to the page
					   
					      	}else{
					      	var appnd = angular.element(document.getElementsByClassName("all_success")).html($compile(" ")($scope));// appending input to the page
					      		 $scope.savetag = false;
						         $scope.canceltag = false;
						         // append the input box with company name 
							var html='<input type="text"  id="tagline2" name="tagline" ng-model ="thenametagline" value="fill out some data" required handle-tag>';
							var appnd = angular.element(document.getElementsByClassName("name_companytagline")).html($compile(" ")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName("name_companytagline")).append($compile(html)($scope));// appending input to the page
								// append the save and cancel button with ng click 
		                 var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors.tagline+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("error_gp")).append($compile(html)($scope));// appending input to the page
					      	}
					 }

   }, function errorCallback(response) {
				});
		});
}

 // ====================END CODE =======================

// when click on save button of tagline then ajax request send 
$scope.saveButtontag = function(keycode,event,scope) {
		if(keycode === 13){ // if enterpressed
             $scope.sendrequest();
          }else{
    	    $scope.sendrequest();
       }

  }
// end code tagline



// start code to add company map 
$scope.disabled= true;  
$scope.v='';
// start code for the company name 
	 $scope.showButtonMap = function(type){
        $scope.v=$scope.thenamemap;   
          angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
			$scope.showmap = true;
			$scope.savemap = false;
			$scope.cancelmap = false;
			$scope.disabled= false;
		    var element = $window.document.getElementById("pac-input");
             element.focus();
			// append the save and cancel button with ng click 
             var html='<button type="submit" id="cancel_map" class="cancel_map" ng-click="cancelButtonMap()" ng-hide="cancelmap">Cancel</button><button type="submit" id="butSave"  class="map_submit" ng-click="saveButtonMap()"  ng-hide="savemap">Save</button>';
			var appnd = angular.element(document.getElementsByClassName("map_buttons")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("map_buttons")).append($compile(html)($scope));// appending input to the page
     };

// when click on save button of map then ajax request send 
$scope.saveButtonMap = function(keycode,event,scope) {
	if($scope.thenamemap ==  undefined){
		$map="";
	}else{
		$map=$scope.thenamemap;
	}
   $document.ready(function(){
						$http({
					  method: 'POST',
					  url: 'editmap',
						data : { _token: $window.localStorage.getItem("t"), address : $map}
					}).then(function successCallback(response) {
                if(response.data.status == 'success'){
		 	        $scope.showmap = false;
					$scope.savemap = true;
					$scope.cancelmap = true;
					$scope.disabled = true;
					     var html='<div class="success_message_here"><p class="sucess_mesage">Data Saved Sucessfully.</p></div>';
				         var appnd = angular.element(document.getElementsByClassName("all_success_map")).html($compile(" ")($scope));// appending input to the page
					     var appnd = angular.element(document.getElementsByClassName("all_success_map")).append($compile(html)($scope));// appending input to the page
                               $timeout(function(){
				                  angular.element(document.getElementsByClassName("all_success_map")).html($compile(" ")($scope));// appending input to the page
									 },500);
				 }else if(response.data.status =='failed'){
				var appnd = angular.element(document.getElementsByClassName("all_success_map")).html($compile(" ")($scope));// appending input to the page
                    var appnd = angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page
	                  	if(response.data.errors=="first_create your company"){
	                  		$scope.savemap = false;
					      $scope.cancelmap = false;
					 	  var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("display_map")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("display_map")).append($compile(html)($scope));// appending input to the page
					      	}else if(response.data.errors=="numbers"){
					      var appnd = angular.element(document.getElementsByClassName("all_success_map")).html($compile(" ")($scope));// appending input to the page
								$scope.savemap = false;
								$scope.cancelmap = false;
						   			// append the save and cancel button with ng click 
		                 var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please Fill Out Valid Data<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("display_map")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("display_map")).append($compile(html)($scope));// appending input to the page
					   
					      	}else{
					      	 var appnd = angular.element(document.getElementsByClassName("all_success_map")).html($compile(" ")($scope));// appending input to the page
					      		   $scope.savemap = false;
					               $scope.cancelmap = false;
						   		// append the save and cancel button with ng click 
		                 var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors.address+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
                         var appnd = angular.element(document.getElementsByClassName("display_map")).html($compile(" ")($scope));// appending input to the page
						 var appnd = angular.element(document.getElementsByClassName("display_map")).append($compile(html)($scope));// appending input to the page
					      	}  
				 }
   },function errorCallback(response) {
				});


		});
  
  } // end save code  

  // when we click on cancel button  of tagline 
  $scope.cancelButtonMap = function() {
    angular.element(document.getElementsByClassName("display_map")).html($compile(" ")($scope));// appending input to the page 
	if($scope.thenamemap == undefined ){
	   $scope.showmap = false;
       $scope.thenamemap =$scope.v; 
       $scope.savemap = true;
	   $scope.cancelmap = true;
	   $scope.disabled = true;
	}else{
      $scope.thenamemap =$scope.v; 
	  $scope.showmap = false;
	  $scope.savemap = true;
	  $scope.cancelmap = true;
	  $scope.disabled = true;
      }
    }
 // end code of when we click on cancel button  of tagline 

// end code to company map 


//=======start code to add company url =============================
//start when click on edit button
// start code for the company name 
$scope.input_url_box=true;
	 $scope.showButtonurl = function(type){
	 	$scope.thenameurloldname = $scope.thenameurl;
         // angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
			$scope.showcompanyurl = true;
			$scope.saveurl = false;
			$scope.cancelurl = false;
			// append the input box with company name
		     var appnd = angular.element(document.getElementsByClassName("slug_val")).html($compile(" ")($scope));// appending input to the page
		      $scope.input_url_box=false;
		       $scope.thenameurl= $scope.thenameurl;
		      var element = $window.document.getElementById("FillUrl");
               element.focus();
			// append the save and cancel button with ng click 
			var html='<button type="submit" id="SaveUrl" ng-click="saveButtonurl()"  ng-hide="saveurl">Save</button> <button type="submit" id="CancelUrl" ng-click="cancelButtonurl()"   ng-hide="cancelurl">Cancel</button>';
			var appnd = angular.element(document.getElementsByClassName("urlButtons")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("urlButtons")).append($compile(html)($scope));// appending input to the page

	   }; 
// end code click on edit button

//======= start code to click on cancel url button==========
  $scope.cancelButtonurl = function() {
  	 $scope.thenameurl =$scope.thenameurloldname; 
angular.element(document.getElementsByClassName("displayUrlError")).html($compile(" ")($scope));// appending input to the page 
	if($scope.thenameurl == undefined ){
	  $scope.showcompanyurl = false;
	  $scope.thenameurl =$scope.thenameurloldname; 
       var html='<h3>'+$scope.thenameurl+'</h3>';
	    var appnd = angular.element(document.getElementsByClassName("slug_val")).html($compile(" ")($scope));// appending input to the page
	    var appnd = angular.element(document.getElementsByClassName("slug_val")).append($compile(html)($scope));// appending input to the page
          $scope.saveurl = true;
	      $scope.cancelurl = true;
	      $scope.input_url_box=true;
	}else{
      var html='<h3>'+$scope.thenameurl+'</h3>';
	  var appnd = angular.element(document.getElementsByClassName("slug_val")).html($compile(" ")($scope));// appending input to the page
      var appnd = angular.element(document.getElementsByClassName("slug_val")).append($compile(html)($scope));// appending input to the page
       $scope.thenameurl =$scope.thenameurloldname; 
	   $scope.showcompanyurl = false;
	   $scope.saveurl = true;
	   $scope.cancelurl = true;
	   $scope.input_url_box=true;
      }
   }
//==== click on end cancel url code =========



//======= start code to click on save url button==========

$scope.saveButtonurl = function(keycode,event,scope) {
		if(keycode === 13){ // if enterpressed
             var classname="all_success";
    	     var dataseleted={ _token: $window.localStorage.getItem("t"), company_url : $scope.thenameurl,  id:$scope.companyID };
             var urldata='company_url';
             var recognize='company_url';
             var errordiv='displayUrlError';
             $scope.sendrequestcomon(classname,dataseleted,urldata,recognize,errordiv);
           }else{
    	     var classname="all_success";
    	     var dataseleted={ _token: $window.localStorage.getItem("t"), company_url : $scope.thenameurl,  id:$scope.companyID };
             var urldata='company_url';
             var recognize='company_url';
             var errordiv='displayUrlError';
              $scope.sendrequestcomon(classname,dataseleted,urldata,recognize,errordiv);
        }
 }
//======= start code to click on save url button==========
//=========end code to add company url ============================





//========START CODE TO ADD OVERVIEW =========================
 																					          
$scope.disabledover= true; 
//$scope.over=true;
	 $scope.showButtonover = function(type){
	 	$scope.thenameoveroldname = $scope.thenameover.replace(/<br\s*[\/]?>/gi, "\n");
         // angular.element(document.getElementsByClassName("error_gp")).html($compile(" ")($scope));// appending input to the page 
			$scope.showover = true;
			$scope.saveover = false;
			$scope.cancelover = false;
		    $scope.disabledover= false;
			// append the input box with company name
		 	$scope.thenameover = $scope.thenameover.replace(/<br\s*[\/]?>/gi, "\n");
            var element = $window.document.getElementById("notes_name2");
               element.focus();
			// append the save and cancel button with ng click 
            var html='<button type="submit" id="cancel_notes" ng-click="cancelButtonover()"   ng-hide="cancelover">Cancel</button><button type="submit" id="buttonSav" ng-click="saveButtonover()"  ng-hide="saveover" >Save</button>';
			var appnd = angular.element(document.getElementsByClassName("overbuttons")).html($compile(" ")($scope));// appending input to the page
			var appnd = angular.element(document.getElementsByClassName("overbuttons")).append($compile(html)($scope));// appending input to the page

	   }; 


//======= start code to click on cancel overview button==========
  $scope.cancelButtonover = function() {
  	//console.log($scope.thenameover);
  	 $scope.thenameover =$scope.thenameoveroldname;
//angular.element(document.getElementsByClassName("displayUrlError")).html($compile(" ")($scope));// appending input to the page 
	if($scope.thenameover == undefined ){
	  $scope.showover = false;
	  $scope.thenameover =$scope.thenameoveroldname;
       var html='<p class="displaying_gp">'+$scope.thenameover+'</p>';
	    var appnd = angular.element(document.getElementsByClassName("save_notes")).html($compile(" ")($scope));// appending input to the page
	    var appnd = angular.element(document.getElementsByClassName("save_notes")).append($compile(html)($scope));// appending input to the page
          $scope.saveover = true;
	      $scope.cancelover= true;
$scope.disabledover= true
	}else{
      var html='<p class="displaying_gp">'+$scope.thenameover+'</p>';
	  var appnd = angular.element(document.getElementsByClassName("save_notes")).html($compile(" ")($scope));// appending input to the page
      var appnd = angular.element(document.getElementsByClassName("save_notes")).append($compile(html)($scope));// appending input to the page
       $scope.thenameover =$scope.thenameoveroldname;
	   $scope.showover = false;
	   $scope.saveover = true;
	   $scope.cancelover = true;
	$scope.disabledover= true
      }
   }
//==== click on end cancel overview code =========

//======= start code to click on save overview  button==========

$scope.saveButtonover = function(keycode,event,scope) {
		if(keycode === 13){ // if enterpressed
             var classname="all_success_notes";
    	     var dataseleted={ _token: $window.localStorage.getItem("t"), notes : $scope.thenameover };
             var urldata='edittext';
             var recognize='company_overview';
             var errordiv='display_notes';
             $scope.sendrequestcomon(classname,dataseleted,urldata,recognize,errordiv);
           }else{
    	      var classname="all_success_notes";
    	     var dataseleted={ _token: $window.localStorage.getItem("t"), notes : $scope.thenameover };
             var urldata='edittext';
             var recognize='company_overview';
             var errordiv='display_notes';
             $scope.sendrequestcomon(classname,dataseleted,urldata,recognize,errordiv);
              }
 }
//======= start code to click on save overview button==========






//========END  CODE TO ADD OVERVIEW =========================

//======= start common function save url button==========
$scope.sendrequestcomon = function(classname,dataseleted,urldata,recognize,errordiv) {
	angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
	var html='<div class="success_message_here"><p class="sucess_mesage">Please wait...</p></div>';
    var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
	var appnd = angular.element(document.getElementsByClassName(classname)).append($compile(html)($scope));// appending input to the page
        $document.ready(function(){
						$http({
					  method: 'POST',
					  url: urldata,
						data : dataseleted,
					}).then(function successCallback(response) {
						if(recognize=="company_url"){
								if(response.data.status == 'success'){
									angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
									$scope.showcompanyurl = false;
									$scope.saveurl = true;
									$scope.cancelurl = true;
									var html='<h3>'+response.data.slug+'</h3>';
								    var appnd = angular.element(document.getElementsByClassName("slug_val")).html($compile(" ")($scope));// appending input to the page
							        var appnd = angular.element(document.getElementsByClassName("slug_val")).append($compile(html)($scope));// appending input to the page
							        $scope.thenameurl =response.data.slug; //set model value
									 $scope.input_url_box=true;
									var html='<div class="success_message_here"><p class="sucess_mesage">Data Saved Sucessfully.</p></div>';
									var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									var appnd = angular.element(document.getElementsByClassName(classname)).append($compile(html)($scope));// appending input to the page
									$timeout(function(){
									angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									},500);
								}else if(response.data.status =='failed'){
                                    angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
									if(response.data.errors=="first_create your company"){
										$scope.saveurl = false;
										$scope.cancelurl = false;
										var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
										var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
										var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
									}else if(response.data.errors=="numbers"){
											var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
											$scope.saveurl = false;
										    $scope.cancelurl = false;
											// append the save and cancel button with ng click 
											var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please Fill Out Valid Data<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
											var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
											var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
   					      	         }else{
										var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
										$scope.savemap = false;
										$scope.cancelmap = false;
										// append the save and cancel button with ng click 
										var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors.company_url+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
										var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
										var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
									} 
							  }
			             }else if(recognize=="company_overview"){
                               if(response.data.status == 'success'){
									angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
									$scope.showover = false;
									$scope.saveover = true;
									$scope.cancelover = true;
									$scope.disabledover= true;
		                   $scope.thenameover= $scope.thenameover.replace(/<br\s*[\/]?>/gi, "\n");
			                   var html='<div class="success_message_here"><p class="sucess_mesage">Data Saved Sucessfully.</p></div>';
									var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									var appnd = angular.element(document.getElementsByClassName(classname)).append($compile(html)($scope));// appending input to the page
									$timeout(function(){
									angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									},500);
								}else if(response.data.status =='failed'){
                                    angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
									angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
									if(response.data.errors=="first_create your company"){
										$scope.saveover = false;
										$scope.cancelover = false;
										$scope.disabledover= false;
										var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
										var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
										var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
									}else if(response.data.errors=="numbers"){
											var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
											$scope.saveover = false;
										    $scope.cancelover = false;
										    $scope.disabledover= false;
											// append the save and cancel button with ng click 
											var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please Fill Out Valid Data<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
											var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
											var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
   					      	         }else{
										var appnd = angular.element(document.getElementsByClassName(classname)).html($compile(" ")($scope));// appending input to the page
										$scope.saveover = false;
										$scope.cancelover = false;
										$scope.disabledover= false;
										// append the save and cancel button with ng click 
										var html='<div class="error_no"><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+response.data.errors.notes+'<i class="fa fa-times CrossIconGP" aria-hidden="true" title="Cancel"></i></p></div></div>';
										var appnd = angular.element(document.getElementsByClassName(errordiv)).html($compile(" ")($scope));// appending input to the page
										var appnd = angular.element(document.getElementsByClassName(errordiv)).append($compile(html)($scope));// appending input to the page
									} 
							  }
			             } // end recognize braces 
   }, function errorCallback(response) {
				});
		});
}


//======= start common function  on save url button==========






});
// end controller of company 


///////////////////////////////////////
