var myData = localStorage.getItem("tree_json");
localStorage.setItem("t", $('input[name="_token"]').val());
if(localStorage.getItem("quizidentity").length > 0 ){
	//$('.course_id').val(''); //
}

if (typeof myData != 'undefined' && myData.length > 0 ) {
	var app = angular.module('createappTree', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable','ngMaterial','lr.upload','ckeditor','FBAngular','ngFileUpload']) ;
	app.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('&^&');
	    $interpolateProvider.endSymbol('&^&');
	  });
	var Mydata =   jQuery.parseJSON( myData );
/**/
/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('handleKey', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 || event.which === 27) {
											link: scope.handlekeyPress(event.which,event,scope);
											return false;
									}
							} else if(event.type =="blur"){
								link: scope.focusout(event.which,scope);
							}

            });

        };
    });
		/*
		NOTES:Directive name :fileModel
		Usage: for handling file uploding in the scope

		*/
	app.directive('fileModel', ['$parse', function ($parse) {
	            return {
	               restrict: 'A',
	               link: function(scope, element, attrs) {
	                  var model = $parse(attrs.fileModel);
	                  var modelSetter = model.assign;

	                  element.bind('change', function(){
	                     scope.$apply(function(){
	                        modelSetter(scope, element[0].files[0]);
	                     });
	                  });
	               }
	            };
	         }]);




		/*
		Controller Name : createCtr
		Usage: All the logic is written here
		*/
	app.controller('createappCtr', function($scope,$compile,$window,$http,$timeout,$uibModal, $log, $document,$mdDialog ,upload,Fullscreen,Upload,$q, $anchorScroll, $location) {
		// $window.
	$scope.allapps ={};
	$scope.t_data = [];
	$scope.names =[];
	$scope.sec_container =[];
	$scope.chap_container =[];
	$scope.top_container =[];
	$scope.loop_index =0;
	$scope.expanded= [];
	$scope.search_dd=[];
	$scope.san_= "";
	$scope.youtubeTrailer=[];
	//$scope.names = JSON.parse($window.localStorage.getItem("tree_json")); // setting json coming from DB to the tree for iteration
	//console.log($scope.names)
		$document.ready(function(){
			// sending a request to get topic content inside the scope.
			// local storage was not taking updated json
						$window.localStorage.removeItem("old_edit_value"); // emptying last time values if any exits
			//$window.localStorage.setItem("tree_json",'default'); // emptying last time values if any exits
			$window.localStorage.removeItem("edit_index"); // emptying tree edit index last time values if any exits
			//var t = angular.element(document.getElementById("#token")); // focusing current input

			$scope.complete_data ={};
			$scope.is_valid = "yes";
			$scope.todelete = ""; // setting default value
			$scope.delete_status = ""; // setting default value
			$scope.beforesort ={};
			$scope.activeMenu = "";
			$scope.activeMenu1 = "";
			$scope.activeMenu2 = "";
			$scope.section_bc = "";
			$scope.chapter_bc = "";
			$scope.topic_bc = "";
			$scope.content = "";
			$scope.selectedIndex= "";
		//	$scope.textapp={};  may be not being used
			//$scope.allapps= $scope.apps.split(","); // preparing array for the apps for the dropdown to add new apps.
			$scope.title ="";
			$scope.app={};
			$scope.embedded_code = "";
			$scope.external_link = "";
			$scope.apps_of_selected_topic = {}; // we kept variable that long to make it understood for later
			$scope.is_truefalse_fullscreen = "";  // to check wether image is allowed in full screen or not
			$scope.is_objective_fullscreen = "";  // to check wether video is allowed in full screen or not
			$scope.is_zip_fullscreen = "";  // to check wether zip is allowed in full screen or not
			$scope.is_multichoice_fullscreen = "";  // to check wether grid is allowed in full screen or not
			$scope.is_text_fullscreen = "";  // to check wether text is allowed in full screen or not
			$scope.being_edited_app = ""; // url eligible encrypted id of the app
			$scope.being_edited_app_index = ''; // index of the apps in the apps array of a particular topic
			$scope.typingTimer =""; // in autosave when we start typing timer is set
			$scope.autosave_status =""; // to show the status while autosaving texts.
			$scope.selected_sec_index =""; // when we click on + icon of section to expand tree or when we click on name of the section index is recored
			$scope.selected_chap_index =""; // when we click on + icon of chapter to expand tree or when we click on name of the chapter index is recored
			$scope.app_drop_start_index =""; // when we drag an app index is recored here
			$scope.app_drop_end_index =""; // when we drop app index is recored where it is droped.
			$scope.section_drop_start_index =""; // when we drag an app index is recored here
			$scope.section_drop_end_index =""; // when we drop app index is recored where it is droped.
			$scope.chapter_drop_start_index =""; // when we drag an app index is recored here
			$scope.chapter_drop_end_index =""; // when we drop app index is recored where it is droped.
			$scope.search= {};
			$scope.show_tree = 'treereplace_shown';
			$scope.fa_class=[];
			$scope.hide_controls =false;
			$scope.expand_chapter_index = [];
			$scope.activemenus =[];
			$scope.selected_top_index ="";
			$scope.isclickenable = true;
			$scope.defer = $q.defer();
			$scope.apps_through = '';
			$scope.new_apps_class ="treereplace_shown";
			$scope.compress_highlight_class ="";
			$scope.compress_highlight_hidden = true;
			$scope.compress_icon_class=""
			$scope.compress_icon_hidden = true;
			$scope.fs_through = "";
			$scope.isFullscreen = false; // all the apps of selected course will be stored in this topic.
			$scope.restore_fs = true;

			$scope.get_data = function(){
						$http({
						method: 'POST',
						url: 'getOriginapp',
						async : true,
						data : { _token: $window.localStorage.getItem("t"), originidentity : $scope.originidentity }
					}).then(function successCallback(response) {
						//	$scope.names = JSON.parse(response.data[0]); // setting json coming from DB to the tree for iteration
							var js_data = JSON.parse(response.data[0]);
							$scope.names = JSON.parse(response.data[0]);
							console.log($scope.names);
							$scope.process();

							$scope.allapps = JSON.parse(response.data[1]); // setting json coming from DB to the tree for iteration
							//console.log($scope.allapps); 
						}, function errorCallback(response) {
				}).then(function () {
						//var app = {id:"Empty",name:"Select App"};
						//$scope.search_dd.push(app);
					angular.forEach($scope.allapps, function(value, key) {
						var app = {id:value.id,name:value.name};
						$scope.search_dd.push(app);
						//console.log(value.id);
					 (value.id == 1 ? $scope.is_truefalse_fullscreen = value.is_fullscreen :'' );
					 (value.id == 2 ? $scope.is_objective_fullscreen = value.is_fullscreen :'' );
					 (value.id == 3 ? $scope.is_multichoice_fullscreen = value.is_fullscreen :'' );
					 (value.id == 4 ? $scope.is_zip_fullscreen = value.is_fullscreen :'' );
					 (value.id == 5 ? $scope.is_text_fullscreen = value.is_fullscreen :'' );
					});
						//console.log($scope.expanded);
						//$scope.san_ = $scope.expanded[0];
					});
			}
			$scope.get_data(); // calling function onload
			$scope.process = function(){
					$scope.t_data =[];
					angular.forEach($scope.names, function(v, k) {
							if( v.hasOwnProperty('chapters') ){
							
								var src1 = {quizname:v['quizname'],chapters:[]}; // source 1
								//$scope.section+k = false;
						} else {
							// if there are no chapters
								var src1 = {quizname:v['quizname']}; // source 1
						}; // if has chapters
						$scope.t_data.push(src1);
						//angular.extend($scope.t_data[$scope.loop_index],chap);
						//console.log(k);
						});
					
			}




			/**/
			/*for setting input value to the */
			$scope.edit = function(taskId){
				//console.log($window.localStorage.getItem("edit_status"));
					if(!$window.localStorage.getItem('old_edit_value') || $window.localStorage.getItem("edit_status") == 'done' || $scope.is_valid == 'no') {
					var html='<input type="text" handle-key info = "&^&editval&^&" handle-press="onkeypress()" id="'+taskId+'"  ng-model="editval" style="padding: 2px;	border:1px solid #ccc !important;width: 180px !important;">' ;

							var arr = taskId.split("s");
						
							arr.splice(0, 1);
							var count = arr.length;
							if(count == 1){
								var toeditval = $scope.names[arr[0]]['section'];
							} else if(count == 2){
								var toeditval = $scope.names[arr[0]]['chapters'][arr[1]]['chapter'];
							} else {
								var toeditval = $scope.names[arr[0]]['chapters'][arr[1]];

							}
								$timeout(function(){
									angular.element(document.getElementsByClassName(taskId)).text(""); // emptying the section value
										var appnd = angular.element(document.getElementsByClassName(taskId)).append($compile(html)($scope));// appending input to the page
										$scope.editval = toeditval; // setting value in the ng-model
										angular.element(document.getElementsByClassName(taskId)).find("input").focus(); // focusing current input
										$window.localStorage.removeItem("old_edit_value");
									  $window.localStorage.setItem("old_edit_value", toeditval);
										$window.localStorage.setItem("edit_index", taskId);
								}, 200);



					}else{
						// if any node is being edited already
						var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Already editing tree!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
						var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
						var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page
						}
				};
				$scope.focusout = function($event){
					var len = (''+$scope.editval).length;
							if( len < 3 || len > 50 ){
								var desired_html = '<div id="openModalss" style="height:170px;width:285px;top:500px;"><div id ="status err_status">ERROR!</div><p class="model_success">Length must be between 3-50 characters!</p><div class="ok_wrapper"><div class="ok_wrapper"><button class="gotIt" ng-click="ok()" >Ok</button></div></div>';
								var desired_width ='sm';
								$scope.open(desired_html,desired_width,'edit',$event);
						} else {
							angular.element(document.getElementById($window.localStorage.getItem('edit_index'))).closest('span').html("").text($scope.editval); // emptying the section value
							var updatehtm =	$scope.updatetree($window.localStorage.getItem('edit_index'),$scope.editval ); // calling common tree update function
						}
				};$scope.ckoptions = {
					change: function() {
						//console.log("changed");
			 			}
				};
					$scope.handlekeyPress = function(keycode,event,scope){
						if(keycode === 13){ // if enterpressed
								var target = event.target;
								target.blur(); // if we blur the input it will automatically call $scope.focusout function
					} else if(keycode === 27){ // if escape is pressed
							var old_edit_value = $window.localStorage.getItem('old_edit_value');
						var updatehtm =	$scope.updatetree($window.localStorage.getItem('edit_index'),old_edit_value ); // calling common tree update function with old value
					}
						//$("#openModals").append('<div id ="status err_status">ERROR!</div><p class="model_success">Length must be between 3-50 characters!</p><div class="ok_wrapper"><div class="ok_wrapper"><button class="gotIt">Ok</button></div>');

					};
					$scope.updatetree = function(index , update ){
						var arr = index.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						if(count == 1){
							$scope.names[arr[0]]['section'] = update;
						} else if(count == 2){
							$scope.names[arr[0]]['chapters'][arr[1]]['chapter'] = update;
						} else {
							$scope.names[arr[0]]['chapters'][arr[1]] = update;
						}
						if($window.localStorage.getItem('old_edit_value') == update){
								angular.element(document.getElementById(index)).closest('span').html("").text(update); // manually replacing input with val
								$window.localStorage.setItem("edit_status","done");
								$window.localStorage.removeItem("old_edit_value"); // emptying last time values if any exits
								$window.localStorage.removeItem("edit_index"); // emptying tree edit index last time values if any exits
						} else {
							$window.localStorage.removeItem("old_edit_value"); // emptying last time values if any exits
							$window.localStorage.removeItem("edit_index"); // emptying tree edit index last time values if any exits
							$window.localStorage.setItem("edit_status","done");
						}
						$scope.posttree("none");
						return true;

					};
					$scope.add = function(index) {
							var arr = index.split("s");
							 arr.splice(0, 1);
							var count = arr.length;
							$scope.sortableOptions.disabled = true;
							$scope.sortableOptions_chapters.disabled = true;
							$scope.sortableOptions_topics.disabled = true;
							if(count == 1){
								var names = $scope.get_names(index);
								// add icon corresponding to section plus icon was pressed.means user wants to add chapter
									if ($scope.expanded.indexOf(arr[0]) < 0) {
											$scope.load_tree_nodes(index);
											$scope.defer.promise.then(function(response){
												$scope.add_chapter(arr);
											});
									} else{
										$scope.add_chapter(arr);
									}

								
							} else if(count == 2){
								// add icon corresponding to chapter plus icon was pressed.means user wants to add topic

									if( $scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('topics') ){
										var edit_in = $scope.names[arr[0]]['chapters'][arr[1]]['topics'].length;
										var x = {"topic": "Subject"};
										$scope.names[arr[0]]['chapters'][arr[1]]['topics'].push(x);
										$timeout($scope.edit('s'+[arr[0]]+'s'+[arr[1]]+'s'+edit_in), 100);
									}else{
										// if there are no chapters in the section
										var edit_in = 0;
									var src1 = [{topic:"Subject"}]; // source 1
									var chap = {topics:src1};
									angular.extend($scope.names[arr[0]]['chapters'][arr[1]],chap);
									}
							} else {
								// add plus icon above tree tapped.
								var edit_in = $scope.names.length;
								var x = {quizname: "Section",chapters:[]};
								 $scope.names.push(x);
								 $scope.t_data.push(x);
								  $timeout($scope.edit('s'+edit_in), 500);
							}
							// calling the edit function after everything is set up
					};
				$scope.add_chapter = function(arr){
							if( $scope.names[arr[0]].hasOwnProperty('chapters') ){
								console.log($scope.names[arr[0]]);
								var edit_in = $scope.names[arr[0]]['chapters'].length;
								var x = {chapter: "Chapter"};
								$scope.names[arr[0]]['chapters'].push(x);
								// calling edit function after adding node
								$timeout($scope.edit('s'+[arr[0]]+'s'+edit_in), 100);
							} else {
								// if there are no chapters in the section
								var edit_in =0;
								var src1 = [{chapter:"Chapter"}]; // source 1
								var chap = {chapters:src1};
								angular.extend($scope.names[arr[0]],chap);
								// calling edit function after adding node
								$timeout($scope.edit('s'+[arr[0]]+'s'+edit_in), 100);
							}
							$scope.expand_chapter_index[arr[0]] =  true ;
							$scope.fa_class[arr[0]] =  'fa-minus-circle';
							var active_index = 's'+arr[0]+'s'+($scope.names[arr[0]]['chapters'].length -1) ;
							$scope.activemenus.push(active_index);
					};
					$scope.posttree = function(action){
						$scope.autosave_status ='Saving...';
						$http({
								  method: 'POST',
								  url: 'quizpost_tree',
									data : { _token: $window.localStorage.getItem("t") ,htmlcontent : JSON.stringify($scope.names) ,course_id : $scope.originidentity }
								}).then(function successCallback(response) {
									$window.localStorage.removeItem("tree_json");
										$window.localStorage.setItem("tree_json",JSON.stringify($scope.names));
										if(action == "get_apps"){
											var arr = $scope.selectedIndex.split("s");
											arr.splice(0, 1);
											$scope.get_apps($scope.selectedIndex,$scope.names[arr[0]]['chapters'][arr[1]])
										}
									$scope.autosave_status ='';
								    // this callback will be called asynchronously
								    // when the response is available
								  }, function errorCallback(response) {
								    // called asynchronously if an error occurs
								    // or server returns response with an error status.
  						});

					}
					// to delete tree nodes
					$scope.delete = function (taskId,ev) {
						var arr = taskId.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						$scope.todelete ="";
						if(count == 1){
							( $scope.names[arr[0]].hasOwnProperty('chapters') && $scope.names[arr[0]]['chapters'].length > 0) ? $scope.delete_status = "no": $scope.delete_status = "yes";
						} else if(count == 2){
							(  $scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('topics') && $scope.names[arr[0]]['chapters'][arr[1]]['topics'].length > 0 ) ? $scope.delete_status = "no": $scope.delete_status = "yes";
						} else {
							$scope.delete_status = "yes";
						}

						if($scope.delete_status == "yes"){
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Delete?</p><button class="godelete proceed gotIt"  ng-click="delete('+"delete"+')">Delete</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
						}else {
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Information!</div><p class="model_success">Data Inside, Unable to Delete</p><div class="ok_wrapper"><button class="gotIt"  ng-click="cancel('+"cancel"+')">Ok</button></div></div>';
						}
							$scope.todelete = taskId;
							var desired_width ='sm';
						$scope.open(desired_html,desired_width,'delete');
					}
					// delete after confirmation
					$scope.delete_node = function () {
						var arr = $scope.todelete.split("s");
						arr.splice(0, 1);
						var count = arr.length;
						$scope.todelete ="";
						if(count == 1){
							$scope.names.splice(arr[0], 1);
							$scope.t_data.splice(arr[0], 1);
							//$scope.process();
						} else if(count == 2){
							$scope.names[arr[0]]['chapters'].splice(arr[1], 1);
						} else {
							$scope.names[arr[0]]['chapters'][arr[1]]['topics'].splice(arr[2], 1);
						}
						$scope.posttree("none");
					}

						// common model function
						$scope.open = function (desired_html,desired_width,redirect,ev) {
										$mdDialog.show({
										controller: DialogController,
										template: desired_html,
										parent: angular.element(document.body),
										targetEvent: ev,
										clickOutsideToClose: false,
      							escapeToClose: false,
										locals: {
										items: {'originidentity': $scope.originidentity},
										},
										fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
										})
										.then(function(response) {
											if(redirect=='edit'){
													$scope.is_valid = "no";
													$scope.edit( $window.localStorage.getItem('edit_index') );
											}else if(response=='delete'){
													$scope.delete_node();
											}else if(response=='drop'){
												if(response =="drop"){
														//$scope.posttree("none");
														
														$scope.expanded
														if($scope.currently_dragging == 'sections'){
															//console.log($scope.currently_dragging);
																if($scope.section_drop_start_index < $scope.section_drop_end_index ){
																	// sorting up to down
																	$scope.names.splice($scope.section_drop_end_index + 1, 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index, 1);
																	$scope.posttree("none");
																} else {
																	// sroting down to up
																	//console.log($scope.section_drop_start_index);
																	//console.log($scope.section_drop_end_index);
																	$scope.names.splice($scope.section_drop_end_index , 0, $scope.names[$scope.section_drop_start_index]);
																	$scope.names.splice($scope.section_drop_start_index + 1, 1);
																	$scope.posttree("none");
																}
															
														} else{
															var counter = 0
															angular.forEach($scope.expanded, function(v, k) {
															$scope.names[v] = $scope.t_data[v];
															counter++;
															});
															if(counter == $scope.expanded.length){
																$scope.posttree("none");
															}
														}
														
													// if user wants to drop
												}
											}else if(response =="no_drop"){
													$scope.t_data = JSON.parse(window.localStorage.getItem("beforedrag"));
											} else if(response == 'drop_app'){
												// if user wants to drop app
												taskId = $scope.selectedIndex;
												var arr = taskId.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var apps =  $scope.names[arr[0]]['chapters'][arr[1]]['apps'];
												if($scope.app_drop_start_index < $scope.app_drop_end_index ){
													// sorting up to down
													apps.splice($scope.app_drop_end_index + 1, 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index, 1);
													$scope.posttree("get_apps");
												} else {
													// sroting down to up
													//console.log($scope.app_drop_start_index);
													//console.log($scope.app_drop_end_index);
													apps.splice($scope.app_drop_end_index , 0, apps[$scope.app_drop_start_index]);
													apps.splice($scope.app_drop_start_index + 1, 1);
													$scope.posttree("get_apps");
												}
											}else if(response == 'no_drop_app'){
												$scope.posttree("get_apps");
												// if user does not want to drop app
												//console.log("finalized no drop");
											} else if(response == 'attach_app'){
												FormData = {_token: $window.localStorage.getItem("t") , in_ : $scope.attach_index, search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'attach'}
												var wheretogo ='attach_appquiz';
												$scope.commonPostFunc(wheretogo,FormData,'attach_app'); // sending a post request

											} else if(response == 'deattach_app'){
												index = $scope.de_attach_index ;
												var arr = $scope.selectedIndex.split("s");
												arr.splice(0, 1);
												var count = arr.length;
												var toeditapp = $scope.names[arr[0]]['chapters'][arr[1]]['apps'][index];
												$scope.names[arr[0]]['chapters'][arr[1]]['apps'].splice(index, 1);
												$timeout(function(){
												$scope.posttree("get_apps");		
												}, 200);
											}
										//$sc
										//$scope.status = 'You said the information was "' + answer + '".';
										}, function() {
										//$scope.status = 'You cancelled the dialog.';
										});
					  };
						function DialogController($scope, $mdDialog,items) {
							$scope.title ="";
							//console.log(items.originidentity);
							 $scope.originidentity = items.originidentity;
								 $scope.hide = function() {
									 $mdDialog.hide();
								 };
								 $scope.answer = function(answer) {
									 $mdDialog.hide(answer);
								 };
								 $scope.ok = function (response) {
									 $mdDialog.hide("ok");
								 };
								$scope.delete = function (response) {
									$mdDialog.hide("delete");
								};
								$scope.drop = function (response) {
									$mdDialog.hide("drop");
								};
								$scope.cancel_drop = function (response) {

									$mdDialog.hide("no_drop");
								};
								$scope.drop_app = function (response) {

									$mdDialog.hide("drop_app");
								};
								$scope.go_attach = function (response) {
									console.log("attach app");
									$mdDialog.hide("attach_app");
								};
								$scope.deattach_app = function (response) {
									console.log("deattach app");
									$mdDialog.hide("deattach_app");
								};

								$scope.cancel_drop_app = function (response) {
									//console.log("cancel");
									$mdDialog.hide("no_drop_app");
								};
								$scope.close_model = function (response) {
									//console.log(response);	$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = "";
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['title'] = answer.title;
						$scope.apps_of_selected_topic[$scope.being_edited_app_index]['url'] = answer.new_url;
									$mdDialog.hide("close_dialog");
								};
							 // will be used if cancel button needed to be displayed
								 $scope.cancel = function (response) {
									//console.log(response);
									 $mdDialog.hide('cancel');
								 };
							 };

			$scope.get_apps = function (taskId,item) {
				$scope.apps_of_selected_topic = {};
				names = $scope.get_names(taskId);
				$scope.selectedIndex= taskId;
				var arr = taskId.split("s");
				arr.splice(0, 1);
				var count = arr.length;
				angular.element(document.getElementById('no_apps_messages')).html('');
				if($scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('apps') ){
					var content =  $scope.names[arr[0]]['chapters'][arr[1]]['apps'];
					//console.log(content);
				} else {
					var content = 'Empty';
				}
				$http({
					method: 'POST',
					url: 'getquiz_apps',
					data: { _token: $window.localStorage.getItem("t") ,'jsoncontent' : content , course_id : $scope.originidentity, location : $scope.location },
					}).then(function successCallback(response) {
						$scope.apps_through = 'topic';
                         console.log(response.data.allData);
                         if(response.data.allData !== 'Empty'){
                         	$timeout(function(){
								$scope.apps_of_selected_topic = response.data.allData;
							}, 100);
                         }else{
                         	var html = '<div class ="no_apps" class="back_comm section"><i class="fa fa-times"></i>No Apps to display!</div>';
							angular.element(document.getElementById('no_apps_messages')).append($compile(html)($scope));// appending input to the page		 
                         }
						$timeout(function() {
							angular.element('pre code').each(function(i, block) {
							hljs.highlightBlock(block);
							});
						},200);
					}, function errorCallback(response) {
						// called asynchronously if an error occurs
						// or server returns response with an error status.
				});

				$scope.activeMenu1 = $scope.activemenus[0];
				$scope.activeMenu2 = $scope.activemenus[1];
				$scope.section_bc = ' >> '+names[0];
				$scope.chapter_bc = ' >> '+names[1];

			};
		$scope.section_span = function (taskId,item,index) {
			//console.log(item);
			$scope.expand_chapter_index[index] = ($scope.expand_chapter_index[index] !== true ) ?  true: false;
			$scope.fa_class[index] = ($scope.expand_chapter_index[index] !== true || $scope.expand_chapter_index[index] == undefined ) ? '' : 'fa-minus-circle';
			names = $scope.get_names(taskId);
			$scope.topic_bc = '';
			$scope.chapter_bc = '';
			$scope.section_bc = ' >> '+item;
			$scope.activeMenu1 = taskId;
			$scope.activeMenu2 = "";
			$scope.activeMenu = "";
			$scope.autosave_status ='Loading Content...';
			$scope.apps_of_selected_topic ={};
			$timeout(function(){
					$scope.load_tree_nodes(taskId);
				},100);
		};
		$scope.load_tree_nodes = function(taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			if(count == 1){
				if($scope.names[arr[0]].hasOwnProperty('chapters') ){
					$scope.t_data[arr[0]]['chapters'] = $scope.names[arr[0]]['chapters'];	
				}
				if ($scope.expanded.indexOf(arr[0]) < 0) {
					$scope.expanded.push( arr[0] );
				}
				
				$timeout(function(){
					$scope.autosave_status ='';
				},100);
			} 
			$scope.defer.resolve("loaded");
			return true;

		};
		$scope.chapter_span = function (taskId,item,index) {

			names = $scope.get_names(taskId);
			console.log($scope.activemenus);
			$scope.topic_bc = "";
			$scope.activeMenu = "";
			$scope.expand_topic_index[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true ) ?  true: false;
			$scope.fa_class_[$scope.selected_sec_index][index] = ($scope.expand_topic_index[$scope.selected_sec_index][index] !== true || $scope.expand_topic_index[$scope.selected_sec_index][index] == undefined ) ? '' : 'fa-minus-circle';
			$scope.section_bc = ' >> '+names[0];
			$scope.activeMenu1 = $scope.activemenus[0];
			$scope.topic_bc = '';
			$scope.chapter_bc = ' >> '+item;
			$scope.activeMenu2 = $scope.activemenus[1];
			$scope.selectedIndex ="";
			$scope.visibility_class ="";
			$scope.apps_of_selected_topic ={};
		};
		$scope.get_names = function (taskId){
			var arr = taskId.split("s");
			arr.splice(0, 1);
			var count = arr.length;
			var name =[];
			var activemenus = [];
			$scope.activemenus =[];
			var name =[];
			if(count == 1){
				name.push($scope.names[arr[0]]['quizname']);
				activemenus.push('s'+arr[0]);
				$scope.selected_sec_index = arr[0];
			} else if(count == 2){
				//var name = $scope.names[arr[0]]['chapters'][arr[1]]['chapter'];
				name.push($scope.names[arr[0]]['quizname']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				activemenus.push('s'+arr[0]);
				activemenus.push('s'+arr[0]+'s'+arr[1]);
			} else {
				//var name = $scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic'];
				name.push($scope.names[arr[0]]['section']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['chapter']);
				name.push($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic']);
				$scope.selected_sec_index = arr[0];
				$scope.selected_chap_index = arr[1];
				$scope.selected_top_index = arr[2];
				//name.push($scope.names[arr[0]]['chapters'][arr[1]]['topics'][arr[2]]['topic']);
			}
			$scope.activemenus = activemenus;
			return name;
			};
			$scope.new_app = function (name,ev){

						if($scope.selectedIndex.length > 0){
							$scope.name = name;
							//console.log($scope.name);
							if(name.length){
						   		var url = $scope.base_url+'/'+name+'/'+$scope.originidentity;
									$scope.trigger_fancybox(url ,'get','none');
								}
						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a question first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}

			};
			  $scope.onReady = function () {
					var url = $scope.base_url+'/appdatafill/'+$scope.being_edited_app;
					$http.get(url).then(function(response) {
					$scope.answers = JSON.parse(response.data.text.answer);
					console.log($scope.answers);
			           angular.forEach($scope.answers, function(value, key) {
						if(value.answer == "True"){
								$scope.app.answer = 1;	
							} else {
								$scope.app.answer = 0;		
							}
					$scope.app.question = response.data.text.question;	
				 });
			   $scope.options = JSON.parse(response.data.text.options);
						$scope.id = JSON.parse(response.data.text.id);
					});
                };
                	  $scope.onReadyChoice = function () {
					var url = $scope.base_url+'/appdatafillmultichoice/'+$scope.being_edited_app;
					$http.get(url).then(function(response) {
					$scope.answers = JSON.parse(response.data.text.answer);
   					angular.forEach($scope.answers, function(value, key) {
                       if(value.answer0){
							$scope.app.answer0 = value.answer0;	
							} else {
							$scope.app.answer1 = value.answer1;	
							}
						   
				        });
			       $scope.option = JSON.parse(response.data.text.options);
                    angular.forEach($scope.option, function(value, key) {
			         if(value.option0){
							$scope.app.option0 = value.option0;	
							} else if(value.option1){
						$scope.app.option1 = value.option1;	
						
							}else if(value.option2){
	                  $scope.app.option2 = value.option2;	
							}else if(value.option3){
								$scope.app.option3 = value.option3;		
							}
					     });
                        $scope.id = JSON.parse(response.data.text.id);
                        $scope.app.question = response.data.text.question;	
					});
					
                };

//==================

        	  $scope.onReadyObjective = function () {
				var url = $scope.base_url+'/appdatafillobjective/'+$scope.being_edited_app;
					   $http.get(url).then(function(response) {
					   $scope.answers = JSON.parse(response.data.text.answer);
                       angular.forEach($scope.answers, function(value, key) {
							$scope.app.answer=value.answer;
				        });
			       $scope.option = JSON.parse(response.data.text.options);
                    angular.forEach($scope.option, function(value, key) {
			         if(value.option0){
							$scope.app.option0 = value.option0;	
							} else if(value.option1){
						$scope.app.option1 = value.option1;	
						
							}else if(value.option2){
	                  $scope.app.option2 = value.option2;	
							}else if(value.option3){
								$scope.app.option3 = value.option3;		
							}
				        });
						$scope.id = JSON.parse(response.data.text.id);
						 $scope.app.question = response.data.text.question;
					});

                };
///====




				$scope.trigger_fancybox = function (url,method,data) {
				
					if(method == 'get'){
						$http.get(url).then(function(response) {
							if (response.status == 200) {
								//console.log($scope.name);
								
								if($scope.name == 'true_popupwindow'){
									$scope.onReady();
								}else if($scope.name =='multichoice_popupwindow'){
                                     $scope.onReadyChoice();
								}else if($scope.name =='objective_popupwindow'){
                                     $scope.onReadyObjective();
								}
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
								//console.log($window.localStorage.getItem("text_content"));
							}
							});
					} else {
					//	$http.post('/someUrl', data, config);

						$http.post(url,data ).then(function(response) {
							if (response.status == 200) {
								var template = angular.element(response.data);
								var compiledTemplate = $compile(template);
								$.fancybox.open({
									modal: true,
									'autoDimensions':false,
									'autoSize':false,
									height: 750,
									width:1400,
									content: template,
									type: 'iframe'
								});
								compiledTemplate($scope);
							}
							});
					}

				 };

				$scope.new_app_submit = function (response) {
		      			var file = $scope.myFile;
										$scope.app.course_id = $scope.originidentity;
										//console.log($scope.app.course_id );
										if($scope.name == 'image'){
											  var uploadUrl = $scope.base_url+"/new_imageApp";
										} else if($scope.name == 'True-false'){
											var uploadUrl = $scope.base_url+"/new_trueApp";
											if($scope.app.hasOwnProperty('embedded_code') && typeof file == 'undefined'){
													var file = "none";
											 }else{
												 $scope.app.embedded_code ="";
											 }
										} else if($scope.name == 'Objective'){
											
											var uploadUrl = $scope.base_url+"/new_objectiveApp";
											if($scope.app.hasOwnProperty('external_link') && typeof file == 'undefined'){
													var file = "none";
											 }else{
												 $scope.app.external_link ="";
											 }
										} else if($scope.name == 'Multichoice'){
											var uploadUrl = $scope.base_url+"/new_multiApp";
											var file = "none";
										} else if($scope.name == 'true_popupwindow' || $scope.name == 'multichoice_popupwindow' || $scope.name == 'objective_popupwindow'){
											// edit image aap
												if(typeof file == 'undefined'){ // if no image selected
													file = 'none';
												}
												$scope.app.cai = $scope.being_edited_app;
												var uploadUrl = $scope.base_url+"/"+$scope.name;
										}

		                $scope.uploadfile(file, uploadUrl,$scope.app ,$scope.name);
		      };

					$scope.uploadfile = function(file, uploadUrl,other_data,name){
						 var fd = new FormData();
						if(file !== 'none'){
							if(name =='True-false'){
									fd.append('true_app', file);
							} else if(name =='Objective'){
									fd.append('objective_app', file);
							} else if(name == 'Multichoice'){
									fd.append('multichoice_zip', file);
							}else if(name == 'true_popupwindow' || name == 'multichoice_popupwindow' || name =='objective_popupwindow'){
									fd.append('url', file);
								}
						 }
							angular.forEach(other_data, function(value, key) {
								fd.append(key, value);
							});
						 $http.post(uploadUrl, fd, {
								transformRequest: angular.identity,
								headers: {'Content-Type': undefined}
						 })
						 .success(function(response){
						 	var appnd = angular.element(document.getElementsByClassName("options_error")).html($compile(" ")($scope));// appending input to the page
                            var appnd = angular.element(document.getElementsByClassName("answer_errors")).html($compile(" ")($scope));// appending input to the page

							 if(response.status == 'success' || response.status == 'updated' ){
								$scope.aftersuccess(response);
								//alert("sdfdsf");
							 }else if(response.status =='failed'){
                                      	var appnd = angular.element(document.getElementsByClassName("options_error")).html($compile(" ")($scope));// appending input to the page
                                      	var appnd = angular.element(document.getElementsByClassName("answer_errors")).html($compile(" ")($scope));// appending input to the page

								    angular.forEach(response.errors, function(value, key) {

								 	if(key=="option0" || key=="option1" || key=="option2" || key=="option3"){
								 		var appnd = angular.element(document.getElementsByClassName("answer_errors")).html($compile(" ")($scope));// appending input to the page
                                      	var appnd = angular.element(document.getElementsByClassName("options_error")).html($compile(" ")($scope));// appending input to the page
									    var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>options field is required<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								       var appnd = angular.element(document.getElementsByClassName("options_error")).append($compile(html)($scope));// appending input to the page
							     	}
							     	if(key=="answer0" || key=="answer1"){
							         	var appnd = angular.element(document.getElementsByClassName("options_error")).html($compile(" ")($scope));// appending input to the page
                                      	var appnd = angular.element(document.getElementsByClassName("answer_errors")).html($compile(" ")($scope));// appending input to the page
									    var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Answer is required<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								       var appnd = angular.element(document.getElementsByClassName("answer_errors")).append($compile(html)($scope));// appending input to the page
							     	}
									var appnd = angular.element(document.getElementsByClassName(key)).html($compile(" ")($scope));// appending input to the page
									var html = '<div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>'+value+'<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div>';
								   var appnd = angular.element(document.getElementsByClassName(key)).append($compile(html)($scope));// appending input to the page
								});
							}
						 })

						 .error(function(){
						 });
					};

			$scope.aftersuccess = function(answer){

			    if(answer.status == 'success'){
					var arr = $scope.selectedIndex.split("s");
					arr.splice(0, 1);
					var count = arr.length;
					if($scope.names[arr[0]]['chapters'][arr[1]].hasOwnProperty('apps') ){
						var x = {[answer.type] : answer.app_id};
						$scope.names[arr[0]]['chapters'][arr[1]]['apps'].push(x);
					} else {
						var src1 = [{[answer.type] : answer.app_id}]; // source 1
						var apps = {apps:src1};
						angular.extend($scope.names[arr[0]]['chapters'][arr[1]],apps);
					}
					$scope.posttree("get_apps");
					//$scope.app ={};
				} else if(answer.status == 'updated'){
					//alert("updated");
					//console.log(answer.new_url);
					//console.log($scope.apps_of_selected_topic[$scope.being_edited_app_index]['url']);
					if($scope.name == 'true_popupwindow'){
                            taskId = $scope.selectedIndex;
						    var item = $scope.get_names(taskId);
                            $scope.get_apps(taskId, item[2] );
 
					}else if($scope.name == 'multichoice_popupwindow' || $scope.name == 'objective_popupwindow' ){
							taskId = $scope.selectedIndex;
							var item = $scope.get_names(taskId);
							$scope.get_apps(taskId, item[2] );
					}

				//	console.log($scope.apps_of_selected_topic[$scope.being_edited_app_index]);
					//$scope.being_edited_app_index

				}
				$.fancybox.close();
				$scope.app ={};
			};

			$scope.edit_app = function(index,id,type) {
				//console.log(index);
				//var	toeditapp = $scope.apps_of_selected_topic[index];
				$scope.being_edited_app_index = index;
				var arr = $scope.selectedIndex.split("s");
				//console.log(arr);
				arr.splice(0, 1);
				var count = arr.length;
				//alert(count);
				//console.log($scope.names[arr[0]]['chapters'][arr[1]]['apps'][index]);
				var toeditapp = $scope.names[arr[0]]['chapters'][arr[1]]['apps'][index];
				//console.log(toeditapp);
				 var name = "";
				 (type == 1 ? $scope.name = 'true_popupwindow':'' );
				 (type == 2 ? $scope.name = 'objective_popupwindow'  :'' );
				 (type == 3 ? $scope.name = 'multichoice_popupwindow'  :'' );
				
				 angular.forEach(toeditapp, function(value, key) {
				 	//console.log(value);

					 replace_1 = value.split('/').join('Multani');
					 replace_2 = replace_1.split('+').join('Chauhan');
					 replace_3 = replace_2.split('|').join('ENCODED');
					 $scope.being_edited_app = replace_3;
					 //console.log($scope.being_edited_app);
						var url = $scope.base_url+'/'+$scope.name+'/'+replace_3;
						$scope.trigger_fancybox(url ,'get','none');
				 });


			}
				$scope.hide_fancy = function(and_) {
					if(and_ == 'get_apps'){
						taskId = $scope.selectedIndex;
						var item = $scope.get_names(taskId);
						$scope.get_apps(taskId, item[2] );
					}
						  $.fancybox.close();
	      }
				/*
				this function is called edit text app.
				*/
				$scope.autosave = function() {
						var doneTypingInterval = 800;  //time in ms, 0.8 second for example
						$timeout.cancel($scope.typingTimer);
						 $scope.typingTimer = $timeout($scope.doneTyping, doneTypingInterval);
	      }
				$scope.doneTyping = function (){
					var text_content = $scope.app.content;
					var FormData = { _token: $window.localStorage.getItem("t") , content : text_content , origin: $scope.being_edited_app, title: $scope.app.title, through : $scope.app.through , course_id : $window.localStorage.getItem("courseidentity"), location : $scope.location } ;
					var wheretogo = 'autosave_text';
					$scope.autosave_status = "saving...";
					//{'_token': t ,'content' : text_content ,'course_id' : courseid, 'id' : app_id, 'title': title ,'trough' : $('.trough').val()},
				var autosave =	$scope.commonPostFunc(wheretogo,FormData,'autosave');
				//console.log(autosave);
				}
				$scope.commonPostFunc = function(wheretogo,FormData,task){
									$http({
									method: 'POST',
									url: wheretogo,
									async : true,
									data : FormData
								}).then(function successCallback(response) {
									//console.log(task);
									if(task =='autosave'){
										$scope.autosave_status = "saved!";
									 $timeout(function(){
										 $scope.autosave_status = "";
									 },500);
									}else if(task == 'search_appsquiz'){
			                        var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
										$scope.apps_through = 'search';
										$scope.total_apps = $scope.total_apps + response.data.allData.length;
									 	if(response.data.allData !== 'Empty') {
									 		$scope.common_success_meesage("Apps Retrieved Successfully!");
											$scope.apps_of_selected_topic = response.data.allData;
											angular.element('pre code').each(function(i, block) {
													hljs.highlightBlock(block);
												});
										    angular.element('.apps_content').highlight($scope.currently_searched_data.val);
										} else{
											var html = '<div class ="no_apps" class="back_comm section"><i class="fa fa-times"></i>No Apps to display!</div>';
											  angular.element(document.getElementById('no_apps_messages')).append($compile(html)($scope));// appending input to the page		 

										};
									 	$scope.pagination_status ='not_triggered';
									} else if(response.data.status =='success' && task == 'attach_app'){
										$scope.aftersuccess(response.data);
									} else if(task == 'paginate'){
										$scope.apps_through = 'search';
										$scope.total_apps = $scope.total_apps + response.data.allData.length;
										if(response.data.allData !== 'Empty') {
											angular.forEach(response.data.allData, function(value, key) {
											$scope.apps_of_selected_topic.push(value);
											angular.element('pre code').each(function(i, block) {
													hljs.highlightBlock(block);
												});
											angular.element('.apps_content').highlight($scope.currently_searched_data.val);										
										});	
											$scope.pagination_status = 'not_triggered';
										}
									 	
									}
									 	//return response;

									}, function errorCallback(response) {
							});
					}
				

				$scope.common_success_meesage = function (message){
					$scope.success_class ="sucess_mesage"
					$scope.success_message = message;
					$timeout(function(){
								$scope.success_message = "";
								$scope.startFade = "";
								 $scope.success_class = 'success_default';
							}, 3000);
				};



					$scope.search_apps = function(){
						//console.log($scope.selectedIndex.length);
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.selectedIndex.length > 0){
							$scope.currently_searched_data = $scope.search; // because $scope.search is bound to input
							$scope.total_apps = 0;									// for safer side we store it in another variable.					
							Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.search.val , app : $scope.search.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' , of_ : $scope.total_apps}
							var wheretogo = 'search_appsquiz';
							$scope.apps_of_selected_topic = [];
							$scope.commonPostFunc(wheretogo,Form_data,'search_appsquiz'); // sending a post request

						} else {
							var html = '<center><div class="validation_profile"><p><i class="fa fa-exclamation-circle"></i>Please select a Question first!<i class="fa fa-times CrossIconGP" aria-hidden="true"></i></p></div></center> ';
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).html($compile("")($scope));// appending input to the page
							var appnd = angular.element(document.getElementsByClassName('select_topic_error')).append($compile(html)($scope));// appending input to the page

						}
				};

				$scope.attach = function(index,id){
					$scope.attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>Attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="go_attach('+"attach"+')">Attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_attach');

				};
				$scope.deattach = function(index,id){
					$scope.de_attach_index = index;
					var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To <b>De-attach</b> App?</p><button class="godelete proceed gotIt"  ng-click="deattach_app('+"de-attach"+')">De-attach</button><button class="cancel" ng-click="cancel('+"no_delete"+')">Cancel</button></div></div>';
					var desired_width ='sm';
					$scope.open(desired_html,desired_width,'go_de-attach');
				};
				$scope.paginate_apps = function(index){
					var appnd = angular.element(document.getElementById('no_apps_messages')).html($compile(" ")($scope));// appending input to the page		 
					if($scope.total_apps > 9 &&  $scope.total_apps - 3 <= index && $scope.pagination_status !== 'triggered' &&  $scope.apps_through !== 'topic'){
					var wheretogo = 'search_apps';
					Form_data = {_token: $window.localStorage.getItem("t") ,search_val : $scope.currently_searched_data.val , app : $scope.currently_searched_data.app.name , course_id : $scope.originidentity , location : $scope.location ,type : 'search' ,of_ : $scope.total_apps}
					$scope.commonPostFunc(wheretogo,Form_data,'paginate'); // sending a post request
					}
				};
				$scope.showmenu = function(){
					$scope.showDetails = !$scope.showDetails;
				}
				$scope.hidemenu = function(){
					$scope.showDetails = false;
				}
				$scope.makefullscreen_app = function(index) {
					if(Fullscreen.isEnabled()){
						Fullscreen.cancel();
					}else{
						//console.log($scope.compress_highlight_hidden);
						$scope.fs_through = 'highlight';
						$scope.compress_highlight_class ="";
						Fullscreen.enable( document.getElementById('a'+index) );
						angular.element(document.getElementsByClassName('back_comm')).addClass("override_overflow");
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						$scope.compress_highlight_hidden = !$scope.compress_highlight_hidden; 
					}
				}

					$scope.toggleFullScreen = function() {
					if($scope.isFullscreen == false){
						$scope.fs_through = 'fullscreen';
						$scope.compress_icon_class = '';
						$scope.isFullscreen = !$scope.isFullscreen;
						$timeout(function(){
							$scope.hide_all = true;
							$scope.sortableOptions_apps.disabled =true;
							angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						}, 200).then(function(){
							$scope.compress_icon_hidden = !$scope.compress_icon_hidden;
						});
					} else {
						$scope.compress_icon_class = 'compress_icon_hide';
						$scope.hide_all = true;
						$scope.sortableOptions_apps.disabled =true;
						angular.element(document.getElementsByClassName('section-right')).addClass("override_overflow");
						$timeout(function(){
							$scope.isFullscreen = !$scope.isFullscreen;
						}, 200).then(function(){
						});
					}
				}
				// sortable options for sections needed to be called differently because these are different
				$scope.sortableOptions = {
					connectWith: ".sections",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						//console.log("startedsec");
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						$scope.section_drop_start_index = ui.item.index();
						//console.log($scope.section_drop_start_index);
						$scope.currently_dragging = "sections";
						ui.item.hide();
						var text = $scope.names[ui.item.startPos]['section'];
						ui.placeholder.html("<div class ='drag_div'><span>"+text+"</span></div>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");
					},
					update:  function(event, ui){
						$scope.section_drop_end_index = ui.item.sortable.dropindex;
						//	console.log($scope.section_drop_end_index);
						var desired_html = '<div id="openModalss" style="height:170px;width:300px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
						desired_width='sm';
						$scope.open(desired_html,desired_width,'drop',event);

					},

				};
				$scope.sortableOptions_chapters = {
					connectWith: ".chapters",
					cursor: "move",
					revert: 100,
					distance: 5,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						$scope.chapter_drop_start_index = ui.item.index();
						$scope.currently_dragging = "chapters";
						ui.item.hide();
						var text = ui.item.context.innerText;
						ui.placeholder.html("<div class ='drag_div'><span>"+text+"</span></div>");
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.chapter_drop_end_index =ui.item.sortable.dropindex;
							//console.log($scope.chapter_drop_end_index);
							//event.stopImmediatePropagation();
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}


				};
				$scope.sortableOptions_topics = {
					connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 2,
					opacity: 0.8,
					disabled: false,
					placeholder: "new_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						//console.log("startedtopic");
						$window.localStorage.setItem("beforedrag",JSON.stringify($scope.t_data));
						ui.item.startPos = ui.item.index();
						ui.item.hide();
						ui.placeholder.html("<div class ='drag_div'><span>"+ui.item.text().trim()+"</span></div>");
					},
					stop: function(event, ui){
						ui.item.show();
						ui.placeholder.html("");	
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							//event.stopImmediatePropagation();
							$scope.currently_dragging = "topics";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop('+"drop"+')">Drop</button><button class="cancel" ng-click="cancel_drop('+"no_drop"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop',event);
						}
					}
				};

				$scope.sortableOptions_apps = {
					//connectWith: ".topics",
					cursor: "move",
					revert: 100,
					distance: 10,
					opacity: 0.8,
					disabled: false,
					placeholder: "back_comm_placeholder",
					forcePlaceholderSize: true,
					start: function(event, ui){
						$window.localStorage.setItem("beforedrag_apps",JSON.stringify($scope.apps_of_selected_topic));
						ui.item.startPos = ui.item.index();
						$scope.app_drop_start_index = ui.item.index();
						ui.item.hide();
						var text = $scope.apps_of_selected_topic[ui.item.startPos]['title'];
						ui.placeholder.html("<div class ='drag_div'><span>"+text+"</span></div>");
					},
					stop: function(event, ui){

						ui.item.show();
						ui.placeholder.html("");
					},
					update:function(event,ui) {
						if (this === ui.item.parent()[0]) { // to prevent to trigger event multiple times
							$scope.app_drop_end_index = ui.item.sortable.dropindex;
							event.stopImmediatePropagation();
							$scope.currently_dragging = "apps";
							var desired_html = '<div id="openModalss" style="height:170px;width:285px;"><div id ="status">Confirmation!</div><p class="model_success">Are You Sure To Drop?<button class="godelete proceed gotIt"  ng-click="drop_app('+"drop_app"+')">Drop</button><button class="cancel" ng-click="cancel_drop_app('+"no_drop_app"+')">Cancel</button></div></div>';
							desired_width='sm';
							$scope.open(desired_html,desired_width,'drop_app',event);
						}

					}
				};	// ended here
	});

    } );

		// directive is used to display apps when get_apps is called.
		app.directive('apipeline', function($compile,$sce) {
			/**/
	return {
		replace: true,
		link: function(scope, element,attrs) {
			var el = angular.element('<div></div>');
			var fullscreen = scope.base_url+'/images/full.png';
			// that is display the title just uncomment that and it display the title 
			el.append('<center><a class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="makefullscreen_app($index);$event.stopPropagation();" ng-hide="compress_highlight_hidden" ng-class="compress_highlight_class"><i class="fa fa-compress" ng-hide="compress_highlight_hidden" ng-class="compress_highlight_class"></i></a></center>');
			el.append('<center><span class = "description_of_app" ng-bind="appdata.title"></span></center>');
			var fulls_ht = '<ul><li><i><a class = "makefullscreen_image" href="" rel="apps_gallery" ng-click="makefullscreen_app($index);$event.stopPropagation();" ng-hide="hide_all"><img  src="'+fullscreen+'" onclick="" title="Highlight Image"></a></i></li></ul>';
             if(scope.apps_through == 'search'){
					// if apps being called through search
			  var attach_or_deatach = '<li><a href="#"  class="de-attach" ng-hide="hide_all"><i class="fa  fa-link" title="attach App" ng-click="attach($index,appdata.id)" ng-hide="hide_all"></i></a></li>';	
			}else{
				// if apps being called by clicking on topic
			  var attach_or_deatach = '<li><a id="test123" class ="editIcon" href=""  title="Edit Image" ng-click="edit_app($index,appdata.id,appdata.app_id)" ng-hide="hide_all"><i class="fa fa-pencil"></i></a></li><li><a href="#"  class="de-attach" ng-hide="hide_all"><i class="fa  fa-unlink" title="De-attach App" ng-click="deattach($index,appdata.id)"></i></a></li>';
			}
			$compile(attach_or_deatach)(scope);


          	 	 var out='<div class="radio">';
                var values=JSON.parse(scope.appdata.options);
                //console.log(values);
                ran = Math.floor((Math.random()*66655)+1);
				angular.forEach(values, function(value, key) {
							if(scope.appdata.app_id == 3){

								  if(value.option0){
							out += '<label><input type="checkbox" name="kali&'+ran+'"  value="'+value.option0+'"><span class="opt">'+value.option0+'<span></label>';
								  }else if(value.option1){
							out += '<label><input type="checkbox" name="kali&'+ran+'"  value="'+value.option1+'"><span class="opt">'+value.option1+'<span></label>';
								  }else if(value.option2){
							out += '<label><input type="checkbox" name="kali&'+ran+'"  value="'+value.option2+'"><span class="opt">'+value.option2+'<span></label>';
								  }else if(value.option3){
							out += '<label><input type="checkbox" name="kali&'+ran+'"  value="'+value.option3+'"><span class="opt">'+value.option3+'<span></label>';
								  }
						}else if(scope.appdata.app_id == 2){
										  if(value.option0){
							out += '<label><input type="radio" name="radio'+ran+'"  value="'+value.option0+'"><span class="multi">'+value.option0+'<span></label>';
								  }else if(value.option1){
							out += '<label><input type="radio" name="radio'+ran+'"  value="'+value.option1+'"><span class="multi">'+value.option1+'<span></label>';
								  }else if(value.option2){
							out += '<label><input type="radio" name="radio'+ran+'"  value="'+value.option2+'"><span class="multi">'+value.option2+'<span></label>';
								  }else if(value.option3){
							out += '<label><input type="radio" name="radio'+ran+'"  value="'+value.option3+'"><span class="multi">'+value.option3+'<span></label>';
								  }
						}else if (scope.appdata.app_id == 1){
								//console.log(value.option);
							out += '<label><input type="radio" name="kaji'+ran+'"  value="'+value.option+'"><span class="multi">'+value.option+'<span></label>';
			            }
				 });
			   $compile(out)(scope);
			if(scope.appdata.app_id == 1){
				// image app
				if(scope.is_truefalse_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);
				//console.log(scope.appdata.option);
                   el.append('<div class="section-right-top"><center><div class = "main_image" >  <div class="w3-padding-jumbo w3-light-grey"><p class="w3-large" style="margin-bottom:30px;">'+scope.appdata.question+'</p></div>'+out+'</div></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
			} else if (scope.appdata.app_id == 2){
				
				if(scope.is_objective_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);

			       el.append('<div class="section-right-top"><center><div class = "main_image" >  <div class="w3-padding-jumbo w3-light-grey"><p class="w3-large" style="margin-bottom:30px;">'+scope.appdata.question+'</p></div>'+out+'</div></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');

			} else if (scope.appdata.app_id == 3){
				// zip app
				if(scope.is_multichoice_fullscreen == 'No'){
					var fulls_ht ='';
				}
				$compile(fulls_ht)(scope);
			       el.append('<div class="section-right-top"><center><div class = "main_image" >  <div class="w3-padding-jumbo w3-light-grey"><p class="w3-large" style="margin-bottom:30px;">'+scope.appdata.question+'</p></div>'+out+'</div></center></div><div class="add_icon_new" >'+attach_or_deatach+'</div><div class="add-icon">'+fulls_ht+'</div>');
			}

			$compile(el)(scope);
			element.append(el);
		}
	}
});

/*to handle enter and escape key
NOTES : 1. handle-key directive has been added to the appened input
				2.in scope section handle handle keypress is set to &handlepress which corresponds to
				  handle-press attribute of the input which mean the function corresponding to handle-press
					will be is set.
				3. and linked to the scope.
*/
app.directive('asText', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
											link: scope.autosave(event.which,event,scope);
											return false;


            });

        };
    });
app.directive('resfs', function() {
	return {
    template: '<a id="showData" class="callMe showData" href={{url("/")}} ng-click="toggle_sidebar();$event.preventDefault();$event.stopPropagation()" ng-class="hideicon_class"><i class="fa fa-arrow-circle-left arrow" ng-hide ="restore_fs"></i></a>'
  };

});
app.directive('compressicon', function() {
	return {
    template: '<a href="#" class="exitfullscreen" style="" title ="Exit Full Screen"  ng-click="toggleFullScreen();$event.preventDefault();$event.stopPropagation()" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"><i class="fa fa-compress" ng-hide="compress_icon_hidden" ng-class="compress_icon_class"></i></a>'
  };

});
app.directive('hidemenu', function ($window) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var el = element[0];
      // if there is clicked anywhere on the document that will be triggered.
      angular.element($window).bind('click', function(){
      		scope.showDetails = false;
      		 scope.$apply();
      });
    } 

  };

});
app.directive('enteronbutton', function() {
        return function(scope, element, attrs) {
            element.on("keydown", function(event) {

							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.handleenterkey(event.which,event,scope);
											return false;
									}
					} 
            });

        };
    });
		// add new app

app.directive('prevententer', function() {
        return function(scope, element, attrs) {
            element.on("keydown blur", function(event) {
							event.stopImmediatePropagation();
							if(event.type == 'keydown'){
							  	if(event.which === 13 ) {
											link: scope.new_app_submit();
											return false;
									}
							} 

            });

        };
    });

app.directive('scrolly', function ($window,$document,$timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var raw = angular.element(document.body);
            
            var body = document.body,
				html = document.documentElement;
            $timeout(function(){
            $document.bind('scroll', function () {
			    scroll_top = Math.max( body.scrollTop, html.scrollTop);
			    height = Math.max( body.scrollHeight, html.scrollHeight);
				var offset = Math.max( body.offsetHeight, html.offsetHeight,$window.pageYOffset );
                if(scroll_top + offset > 1200){
                	scope.make_gtp_vi = 'make_less_opacity';	
                	scope.shouldgotop = true;
                	if (scroll_top + offset >= height ) {
                }
                 scope.$apply(attrs.scrolly);
           	 	}else if(scroll_top + offset < 1200){
                	scope.make_gtp_vi = '';	
                	scope.shouldgotop = false;
                	 scope.$apply(attrs.scrolly);
                }

            	});
           },500);
        }
    };
});


		// add new app




}