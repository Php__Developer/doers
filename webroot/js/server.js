	var config = require('./config/config'); // Main Configuration FIle
	const express = require('express');
	const bodyParser= require('body-parser');
	var mysql = require('mysql')
	var app = require('express')();
	const MongoClient = require('mongodb').MongoClient;
	var ObjectID = require('mongodb').ObjectID;
	const Server = require('mongodb').Server;
	app.use(bodyParser.urlencoded({extended: true}));
	app.use(express.static('public')); // make public folder accessible to the public
	app.use(bodyParser.json()); // make server to read json data because mongodb does not understands json
	var currentuserid; 
	var db;
	var toemit;
	var toemit1;
	var fs = require('fs');
	var http = require('http').Server(app);
	const httponly = require('http');
	var io = require('socket.io')(http);
	var connectedusers = [];
	var socketobj;
	var extend = require('util')._extend;
	var ceil = require( 'math-ceil' );
	var connection = mysql.createConnection({
	  host     : config.mysql.host,
	  user     : config.mysql.user,
	  password : config.mysql.password,
	  database : config.mysql.database
	});
	var allowedtyes =['text/plain', 'application/vnd.oasis.opendocument.spreadsheet','image/jpg','image/png','image/jpeg','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/zip', 'application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/x-rar']
	const queryString = require('query-string');
	require('events').EventEmitter.prototype._maxListeners = 100;
	var randomstring = require("randomstring");
	var datetime = require('node-datetime');
	 var request = require('request');
	 var aws = require('aws-sdk');
	 var multer = require('multer');
	 aws.config.update({
	    secretAccessKey: config.aws.secretAccessKey,
	    accessKeyId: config.aws.accessKeyId,
	    region: config.aws.region
	});
	 multerS3 = require('multer-s3');
	  var s3 = new aws.S3({ params: {Bucket: 'doersdocs'} });
	  var bucketName = 'doersdocs';
	var storage =   multer.diskStorage({
	  destination: function (req, file, callback) {
	    callback(null, './uploads');
	  },
	  filename: function (req, file, callback) {
	    callback(null, file.name + '-' + Date.now());
	  }
	});
	var upload = multer({ storage : storage}).single('file');	
	var siofu = require("socketio-file-upload");
	 app.use(siofu.router);
	 var totalprojects = 0;
	 var totaldays = 0;
	 var end = 0;
	var start = 0;
	Q = require('q');

	var daywisearr =[];
	var counter1 = 0;
	var dindex = 0;
	var projecsdata = [];
	var daysindex = 0;
	var currentindex = 0;
	var mainreturnedvalue;
	var deferred;
	var daywasiedef;
	var startday = 0;
	var userdata = [];
	var totalusers = 0;
	var usercounter = 1;
	var nullcounter = 0;
	var nonnullcounter = 0;
	var startday = 0;

/*Declations Ended*/

	/*************************************\
	| @Who : Maninder
	| @Function name : MongoClient.connect
	| @why : MongoDb COnnection and Intialize everything
	| @when : 11-07-2017
	\*************************************/	

MongoClient.connect('mongodb://'+config.mongodb.host+'/'+config.mongodb.database, (err, database) => {
  if (err) return console.log(err)
  db = database;
	

io.on('connection', function (socket) {
	socketobj = socket
	var uploader = new siofu();
	uploader.dir = __dirname +"/uploads";
	uploader.listen(socket);
	uploader.maxFileSize = 10000000;
	uploader.resetFileInputs = true;
	uploader.on("start", function(event){
		//console.log(event.file.meta.type);
		if(event.file.size > 10000000){ // max limit 10 MB
			event.file.clientDetail.error = 'Max 5MB Allowed!';
			uploader.abort(event.file.id, socket);
		}
		 if (/\.exe$/.test(event.file.name)) {
		 	event.file.clientDetail.error = '.exe Files Not Allowed!';
		 	uploader.abort(event.file.id, socket);
	    }
	    if(allowedtyes.indexOf(event.file.meta.type) == -1){
	    	event.file.clientDetail.error = 'File Type Not Allowed';
		 	uploader.abort(event.file.id, socket);	
	    }
	});
	uploader.on("saved", function(event){
       //console.log(event.file);
        var oldname = __dirname +"/uploads/"+ event.file.name;
        var keyname = randomstring.generate(12) + new Date().getTime() + randomstring.generate(12)  +event.file.name.replace(/ /g,"_").split('/').join('xy7sj7str');
        var newname = __dirname +"/uploads/"+ keyname
        fs.rename( oldname  ,newname , function (err) {
		  if (err) throw err;
		  var dt = datetime.create();
		  var fomratted = dt.format('Y-m-d H:M:S');
		  var fileStream = fs.createReadStream(newname);
		  var post = {
		  				userid : event.file.meta.origin ,
		  				name : keyname ,
		  				agency_id : event.file.meta.originagency,
		  				last_modifiedby : event.file.meta.origin,
		  				notes:'Empty',
		  				type : event.file.meta.type,
		  				created: fomratted,
		  				modified : fomratted,
		  				entity_id : event.file.meta.entity_id,
		  				attached_to : event.file.meta.attached_to,
		  				original_name: event.file.name
		  			};	
						
						request.post(event.file.meta.base_url+'webservicies/adddoc', {form: post} ,function (error, response, body) {
								 if (error) throw error;
								 if(JSON.parse(response.body).status == 'success'){
										 	var datatoupload = {
											Key: 'projects/'+ keyname, 
											Body: fileStream,
											ContentType : event.file.meta.type
										};
										
											var datatoupload = {
											    Key: 'projects/'+ keyname, 
											    Body: fileStream,
											    ContentType : event.file.meta.type
											  };	
										s3.putObject(datatoupload, function(err, data){
											if (err) { 
												console.log(err);
												console.log('Error uploading data: ', data); 
											} else {
												fs.unlinkSync(newname);
												var return_data = {
												requestUrl : data,
												imageUrl: 'https://s3-us-west-2.amazonaws.com/doersdocs/'+ keyname
											};
											
											console.log('succesfully uploaded the image!');
											}
										});
								 }
								 //JSON.parse(yourJsonString);
							});
		});
		event.file.clientDetail.keyname = keyname;
    });

    // Error handler:
    uploader.on("error", function(event){
        event.file.clientDetail.error = event.error;
    });
    /*FILE UPLOAD SECTION*/

	 socket.on('reloadtickets', function (data) {
	 	connectedusers.forEach(function(v){
				if(v.connecteduserid == data.target){ 
	 				setTimeout(function () {
					   io.emit('reloadticketsok',data );
					}, 100)
										
					 
				}
			});
  	});
	 // WHEN SOMEONE IS DISCONNECTED
	socket.on('disconnect', function () {
			for(var i=0; i < connectedusers.length; i++){
		    	if(connectedusers[i].socketid === socket.id){
		      		connectedusers.splice(i,1); 
		    	}
  			}
  });
	/*UPLOAD CODE START*/

	
	/*UPLOAD CODE END*/
}); 

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : Server Listing Port
| @when : 11-07-2017
\*************************************/	

 /* http.listen(8080, () => {
    console.log('listening on 8080');
  });*/
  var listener = http.listen(config.listeningport, config.listeninghost, function(){
			//console.log("Listening 8080")
	});
})

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : TRANSFORMING REQUEST OPTIONS TO POST
| @when : 11-07-2017
\*************************************/	

app.get('/downloadfile/:id', (req, res) => {
			var params = {
						     Bucket: 'doersdocs', /* required */
							 Key:  'projects/'+req.params.id, /* required */
						  };
		s3.getObject(params, function(err, data) {
		  if (err) /*console.log(err, err.stack)*/ res.json({status : 501, response : 'failed'}); // an error occurred
		  else     res.send(data);           // successful response
		});
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : TRANSFORMING REQUEST OPTIONS TO POST
| @when : 11-07-2017
\*************************************/

app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FETCHING RECORDS OF CURRENT USER
| @when : 11-07-2017
\*************************************/

app.post('/quotes', (req, res) => {
	console.log(req.body);
	 	connection.query('SELECT * from users WHERE employee_code = "'+req.body.endpointorigin+'"', function (err, rows, fields) {
	 		if (err) return console.log(err) ;// res.json({status : 501, response : 'failed'});
	 		//console.log(rows);
	 			 currentuserid = rows[0]['id'];
			 	connectedusers.push({
				      	socketid : socketobj.id,
				      	connecteduserid : currentuserid
				 });
			db.collection('activities').find({ "to_id": rows[0]['id'] }).sort({$natural:-1}).limit(6).toArray((error, theresult) => {
		    if (error){
		    	res.json({status : 501, response : 'failed'});
		    }
		    res.json({status : 200, data : theresult});
		  });	
		});
    	

});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR ACTIVITES
| @when : 11-07-2017
\*************************************/

app.post('/logactivity', (req, res) => {
	var from_name;
	var to_name;
	if(connectedusers.length > 0){
		connection.query('SELECT * from users WHERE id = '+req.body.to_id+'', function (err, rows, fields) {
				to_name = {to_name : rows[0]['first_name'] +' ' +rows[0]['last_name']} ;
		});
		connection.query('SELECT * from users WHERE id = '+req.body.from+'', function (err, rows, fields) {
		  if (err) throw err;
		 
		toemit =  extend(req.body , {from_name : rows[0]['first_name'] +' ' +rows[0]['last_name']} , to_name );
		toemit1 =  extend(req.body , to_name );
		//console.log(req.body);
			db.collection('activities').save(req.body, (err, result) => {
				    if (err) /*return console.log(err)*/ res.json({status : 501, response : 'failed'});;
				    res.json({status : 200, response : 'success'});
			 	 });
			connectedusers.forEach(function(v){
				if(v.connecteduserid == req.body.to_id){
					 io.to(v.socketid).emit('activity',toemit1 );
					 io.emit('reloadactivities',{ message : 'There are New Activities', to : req.body.to_id } );
					 
				}
			});
				
		});
	}
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR RENDERING ACTIVITIES ON THE ACTIVITIES PAGE
| @when : 11-07-2017
\*************************************/


app.post('/activities', (req, res) => {
	 	currentuserid = req.body.originidentity;
	 	var cond = {};
	 	var tofetch = 20;
	 	var start_index = (tofetch * parseInt(req.body.pageno)) - 20;
	 	//console.log(start_index);
	 	var id = parseInt(req.body.originidentity);
	 	if(req.body.check_it == 1) {
	 		//console.log("all");
			cond =  { $or : [ {"to_id":  id } ,  { "from": id }  ] };
	 	} else if(req.body.key == 'open'){ // if key is open, tickets activities will be fetched
	 		//console.log("tickets");
	 		cond =  { $or : [ {"to_id": id , "entity_id" : 1 } ,  { "from": id , "entity_id" : 1 }  ] };	
	 	} else if(req.body.key == 'close'){ // if key is open , Project activities will be fetched
	 		//console.log("projects");
	 		//console.log(isNaN(id));
	 		if (isNaN(id)) {
 				request.post( req.body.base_url+'webservicies/dcrids', {form: req.body } ,function (error, response, body) {
 						if (error) {
 							throw error;
 						}
						var resultdata = JSON.parse(response.body);
					  	var allids = resultdata.ids;
					  	cond = { project_id : parseInt(allids[0]['id']) };
						db.collection('activities').find(cond).sort({$natural:-1}).skip(start_index).limit(20).toArray((error1, theresult) => {
						   if (error1) {
						   	throw error1;
						   }
						     db.collection('activities').find(cond).count((error2, totalcount) => {

						     	if (error2){
						     		throw error2;	
						     	} 
						     	 var pages =  ceil( totalcount / tofetch );
						     	
							     	  res.json({status : 'success' , 
							    		  resultData : theresult ,
							    		  userID : req.body.originidentity,
							    		  totalpages : pages,
							    		  currentpage : req.body.pageno,
							    		  pageno : req.body.pageno,
							    		  totalrecords :  totalcount,
							    		  currentrecodsfrom : start_index + 1
							    		   });
						     });
						  });
				 });
				} else {
					//console.log("else");
					cond =  { $or : [ {"to_id": id , "entity_id" : 2 } ,  { "from": id , "entity_id" : 2 }  ] };		    
				}


	 		
	 	}
	//console.log(cond);
   db.collection('activities').find(cond).sort({$natural:-1}).skip(start_index).limit(20).toArray((error, theresult) => {
	    if (error){
	    	res.json({status : 501, response : 'failed'});
	    }
	     db.collection('activities').find(cond).count((error, totalcount) => {
	     	 var pages =  ceil( totalcount / tofetch );
		     	  res.json({status : 'success' , 
		    		  resultData : theresult ,
		    		  userID : req.body.originidentity,
		    		  totalpages : pages,
		    		  currentpage : req.body.pageno,
		    		  pageno : req.body.pageno,
		    		  totalrecords :  totalcount,
		    		  currentrecodsfrom : start_index + 1
		    		   });

	     });
	   
	  
	  });

});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/projectactivities', (req, res) => {
	 	//console.log(req.body);
	 	currentuserid = req.body.originidentity;
	 	var cond = {};
	 	var tofetch = 20;
	 	var start_index = (tofetch * parseInt(req.body.pageno)) - 20;
	 	var id = parseInt(req.body.originidentity);
	 	if(req.body.key == 'close'){
	 		if (isNaN(id)) {
 				request.post( req.body.base_url+'webservicies/dcrids', {form: req.body } ,function (error, response, body) {
 						if (error) {
 							throw error;
 						}
						var resultdata = JSON.parse(body.toString().trim());
					  	var allids = resultdata.ids;
					  	cond = { project_id : parseInt(allids[0]['id']) };
					  	//console.log(cond);
						db.collection('activities').find(cond).sort({$natural:-1}).skip(start_index).limit(20).toArray((error1, theresult) => {
						   if (error1) {
						   	throw error1;
						   }
						   	
						    //console.log(theresult);
						     db.collection('activities').find(cond).count((error2, totalcount) => {

						     	if (error2){
						     		throw error2;	
						     	} 
						    		//console.log(totalcount);
						     	 var pages =  ceil( totalcount / tofetch );
							     	  res.json({response : 'success' , 
							    		  resultData : theresult ,
							    		  userID : req.body.originidentity,
							    		  totalpages : pages,
							    		  currentpage : req.body.pageno,
							    		  pageno : req.body.pageno,
							    		  totalrecords :  totalcount,
							    		  currentrecodsfrom : start_index + 1
							    		   });
						     });
						  });
				 });
				} 
	 		
	 	}
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/


app.post('/deldoc', (req, res) => {
					var params = {
						     Bucket: 'doersdocs', /* required */
							 Key:  'projects/'+req.body.docid, /* required */
						  };
						 // console.log(req.body.docid);
		connection.query('delete from documents WHERE name = "'+req.body.docid+'"', function (err, rows, fields) {
						if (err){
				    	res.json({status : 501, response : 'failed'});
				    }
				s3.deleteObject(params, function(err, data) {
				  if (err) 	res.json({status : 501, response : 'failed'});  // an error occurred
				  else      res.json({status : 200, response : 'success'});           // successful response
				});
		});				  


		
});	

	

// def = deferred();






/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/reloadstats', (req, res) => {
		console.log('limit' + req.body.limit);
		console.log('offset' + req.body.offset);
		connection.query('SELECT id from projects where agency_id="'+req.body.agency_id+'" LIMIT '+req.body.limit+' OFFSET '+req.body.offset+'', function (err, rows, fields) {
				if (err) 	res.json({status : 501, response : 'failed'});  // an error occurred
					connection.query('SELECT count(*) as totalrecords from projects where agency_id="'+req.body.agency_id+'"', function (perr, prows, pfields) {

							projecsdata = rows;
							//console.log(projecsdata);
							totalprojects = prows[0].totalrecords;
							//console.log(totalprojects);
							var currentindex1 = 0;
							iterateprojects(rows[0]['id'],currentindex1,rows,req, res,totalprojects);

					});

					
				/*	deferred.promise.then(function (text) {
					 // console.log(text); // Bingo!
					 if(text == 'success'){
							res.json({status : 200, response : 'success'});           // successful response
						}
				});*/
		});
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

function iterateprojects(project_id,currentindex1,resultsets,req, res,totalprojects){
				 // console.log(totalprojects);
				  daysindex = 0	 ;// resetting days index;
				  var dt = datetime.create();
				  var currentdate = dt.format('Y-m-d H:M:S');
				  var final15dysticcount = [];
				  var data;
				  daywisearr =[];
				  startday = 0
				 
				  if(currentindex1 < resultsets.length){
				 		daywisecallback(project_id,currentindex1,resultsets,req,res,totalprojects)
				  } else {
				  	console.log("iterateprojects last inex");
				  	res.json({status : 200, response : 'success'});           // successful response
				  	//deferred.resolve("Success");
				  }
			}


/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/
var endday = 0;
function daywisecallback(project_id,currentindex1,resultsets,req,res,totalprojects){
		totaldays = 15;
		startday = dindex;
		dindex = dindex +1;
		endday = dindex
		//console.log("endday" +endday);
		if(startday < 15){
				var dt1 = datetime.create();
				var startdateoffset = 	dt1.offsetInDays(-(startday)); 
				var startdate =  dt1.format('Y-m-d H:M:S');
				var dt1 = datetime.create();
				var enddateoffset = 	dt1.offsetInDays(-(endday));
				var enddate	 =  dt1.format('Y-m-d H:M:S');
				
					connection.query('SELECT count(*) as rwcnt from tickets where project_id="'+project_id+'" AND created >= "'+ enddate+'" AND created <= "'+startdate+'" ', function (err, ticrows, fields) {
					if (err)  console.log(err);	//res.json({status : 501, response : 'failed'});  // an error occurred
						daywisearr.push(ticrows[0].rwcnt);
							daywisecallback(project_id,currentindex1,resultsets,req,res,totalprojects)
					});

			//findtickets(project_id,startday,endday,totaldays,req,res,currentindex,resultsets);	
		} else {
				
				dindex = 0;
				var dt = datetime.create();
				currentdate = dt.format('Y-m-d H:M:S');
				var dwa = daywisearr.join(',');
				var objectId = new ObjectID(); // creating uniuq id for the each document
				//console.log(objectId);
				 //console.log('**************');
				db.collection('job_reports').findOneAndDelete( {project_id: project_id },	
								  (err, result) => {
				 if (err) console.log(err);
					extend(req.body , {project_id : project_id , ticketsData :  dwa , created : currentdate ,_id : objectId} );
					db.collection('job_reports').insert(req.body , (error, results) => {
						    if (error) return console.log(error);
						   currentindex1 +=1;
							//console.log('After Query' + currentindex1);
							//console.log('project_id' + project_id);
							//console.log('**************');
						    if(currentindex1  < resultsets.length ){ // untill we do not reach at last project
								iterateprojects(resultsets[currentindex1]['id'],currentindex1,resultsets,req, res,totalprojects);	// Moving to Next Project
							} else{ // if we reach at last project
								console.log("Ended");
								console.log(totalprojects);
								res.json({status : 200, response : 'success', totalrecords : totalprojects});           // successful response
								
  								
							}
					});
				});
		}
		
}

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

function findtickets(project_id,startday,endday,daysindex,req,res,currentindex,resultsets){
		var dt1 = datetime.create();
		var startdateoffset = 	dt1.offsetInDays(-(startday)); 
		var startdate =  dt1.format('Y-m-d H:M:S');
		var dt1 = datetime.create();
		var enddateoffset = 	dt1.offsetInDays(-(endday));
		var enddate	 =  dt1.format('Y-m-d H:M:S');
			connection.query('SELECT count(*) as rwcnt from tickets where project_id="'+project_id+'" AND created >= "'+ enddate+'" AND created <= "'+startdate+'" ', function (err, ticrows, fields) {
			if (err)  console.log(err);	//res.json({status : 501, response : 'failed'});  // an error occurred
				daywisearr.push(ticrows[0].rwcnt);
					daywisecallback(project_id,currentindex,resultsets,req,res)
			});		
		
}

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/reloadstatsforone', (req, res) => {	
		var postdata = { ids : req.body.project_id };
		request.post( req.body.base_url+'webservicies/dcrids', {form: postdata } ,function (error, response, body) {
			if (error) throw error;
			console.log(response.body);
			//var resultdata = JSON.parse(response.body);
			var resultdata = JSON.parse(body.toString().trim());
			//console.log(body);
		    var allids = resultdata.ids;
		    connection.query('SELECT * from projects where id="'+parseInt(allids[0]['id'])+'"', function (err, rows, fields) {
				if (err) 	res.json({status : 501, response : 'failed'});  // an error occurred
					projecsdata = rows;
					currentindex = 0;
					totalprojects = rows.length;
					iterateprojects(rows[0]['id'],currentindex,rows,req, res);
					deferred.promise.then(function (text) {
						if(text == 'success' ){
							res.json({status : 200, response : 'success'});           // successful response
						}
					 // console.log(text); // Bingo!
				});
			});
		});	
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/getstats', (req, res) => {
					var encids = "";			
					request.post( 
						{
						 url : req.body.base_url+'webservicies/dcrids',	
						 form: req.body,
						 } ,

						function (error, response, body) {
					if (error) throw error;
					  var resultdata = JSON.parse(body.toString().trim());
					  var allids = resultdata.ids;
					  var finaldata = [];
					 if(resultdata.status == 'success'){
					 	processloop( { "project_id" :  parseInt(allids[0]['id']) } , 0 , allids.length , allids ,req,res );
					 }
				 });
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/saveactivity', (req, res) => {
	var from_name;
	var to_name;
	console.log(req.body);
	if(connectedusers.length > 0){
		request.post( req.body.base_url+'webservicies/dcrids', {form: req.body } ,function (error, response, body) {
		 if (error) throw error;
		 var resultdata = JSON.parse(body.toString().trim());
		 console.log(resultdata);
		 var id = resultdata.ids[0]['id'];
		 if(req.body.entity_id == 2){
			 	connection.query('SELECT * from projects where id="'+id+'"', function (err, rows, fields) {
			  			if (err) throw err;
			  			iterateraci(rows,'r',rows[0]['pm_id'],req.body.type,req,res);
			  	});				
			 }

		});
	}
});

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

/*COMMON FUNCTIONS*/
function iterateraci(rowsdata,raci,ids,logtype,req,res){
			  /*r : pm_id
	 			a : engineeringuser
	 			c : salesfrontuser
	 			i : salesbackenduser*/
	connection.query('SELECT * from users where id IN ('+ids+')', function (err, rows, fields) { 
		  			if (err) throw err;
		  			savelog(2,rowsdata,req,res,raci,ids,rows,logtype);
		 });
}

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

function savelog(entity_id,rowsdata,req,res,raci,ids,users,logtype){
	if(entity_id == 2){
		idsarr = ids.split(',');
		//if(raci == 'r'){
			docs = [];
			for(var i=0; i < users.length; i++){
		    	if(raci == 'r'){
		    		var logtxt = 'You have been assigned as RESPONSIBLE in Project : ' + rowsdata[0]['project_name'];
		    	} else if(raci == 'a'){
		    		var logtxt = 'You have been assigned as ACCOUNTABLE in Project : ' + rowsdata[0]['project_name'];
		    	}else if(raci == 'c'){
		    		var logtxt = 'You have been assigned as CONSULTANT in Project : ' + rowsdata[0]['project_name'];
		    	}else if(raci == 'i'){
		    		var logtxt = 'You have been assigned as INFORMER in Project : ' + rowsdata[0]['project_name'];
		    	}
		    		var name = users[i]['first_name']+' ' +users[i]['last_name'];
		    		
		    		 var dt = datetime.create();
		  			 var fomratted = dt.format('Y-m-d H:M:S');
		  			 var objectId = new ObjectID(); // creating uniuq id for the each document
		  			 var finaledata = {entity_id : entity_id , _id : objectId ,to_id : users[i]['id'] , to_name : name , logtext : logtxt , type: logtype , created_at : fomratted , project_id : rowsdata[0]['id'] , role : raci };
		  			 var emitdata = {entity_id : entity_id , _id : objectId ,to_id : users[i]['employee_code'] , to_name : name , logtext : logtxt , type: logtype , created_at : fomratted }
		    		docs.push(finaledata);
		    		connectedusers.forEach(function(v){
						if(v.connecteduserid ==  users[i]['id']){
							 io.to(v.socketid).emit('activity',emitdata );
							 io.emit('reloadactivities',{ message : 'There are New Activities', to : users[i]['id'] } );
						}
					});
  				
			}	
				db.collection('activities').insertMany(docs, (err, result) => {
	  				  if (err){
				    	console.log(err);
				    	//res.json({status : 501, response : 'failed'});
				    }
					if(raci == 'r'){
						iterateraci(rowsdata,'a',rowsdata[0]['engineeringuser'],logtype,req,res);
					}else if(raci == 'a'){
						iterateraci(rowsdata,'c',rowsdata[0]['salesfrontuser'],logtype,req,res);
					}else if(raci == 'c'){
						iterateraci(rowsdata,'i',rowsdata[0]['salesbackenduser'],logtype,req,res);
					}else if(raci == 'i'){
						//iterateraci(rowsdata,'i',rowsdata[0]['salesbackenduser'],logtype);
						console.log("LOg Created Yeppe!");
						 res.json({status : 200, response : 'success'});
					}
			  });
	}	
}
/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/


var finalfetcheddata = {};
function processloop(cond,index,totalresult,result,req,res){
		if(index < totalresult){
			getstats(cond,index,totalresult,result,req,res);			
		} else {
			//console.log("Final Data");
			//console.log(finalfetcheddata);
			res.json({status : 200, response : 'success', data : finalfetcheddata});
		}

}

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

function getstats(cond,index,totalresult,result,req,res){
		db.collection('job_reports').find(cond).sort({$natural:-1}).toArray((error, theresult) => {
			if (error) throw error;
			var newkey = result[index]['dcrid']+'__spark';
			extend(finalfetcheddata , { [newkey] :  theresult[0] });
			index = index + 1;
			if(index < result.length){
				processloop( { "project_id" :  parseInt(result[index]['id']) } , index , result.length ,result,req,res);	
			} else {
				res.json({status : 200, response : 'success', thedata : finalfetcheddata /*JSON.stringify(finalfetcheddata)*/ });
			}
			
		});


				
}
/*Common FUnctions*/
/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : FOR SAVING DATA IN FOR Ticket Discusstions
| @when : 11-07-2017
\*************************************/

app.post('/addtodescussion', (req, res) => {
			if(connectedusers.length > 0){
				if(req.body.replier_id == 0){
					toemit =  extend(req.body , {replier_name : 'Doers.online' , image_url : 'afterlogin_logo.png' , username : 'admin@vlogiclabs.com' });
						db.collection('ticket_discussions').save(req.body, (err, result) => {
							    if (err) return console.log(err);
							    res.json({status : 200, response : 'success'});
						 	 });
				} else {
					connection.query('SELECT * from users WHERE id = '+req.body.replier_id+'', function (err, rows, fields) {
					  if (err) throw err;
						toemit =  extend(req.body , {replier_name : rows[0]['first_name'] +' ' +rows[0]['last_name'] , image_url : rows[0]['image_url'] , username : rows[0]['username'] });
						db.collection('ticket_discussions').save(req.body, (err, result) => {
							    if (err) return console.log(err);
							    res.json({status : 200, response : 'success'});
						 	 });
				 	 });
				}
				 	
			}
});

/*************************************\
| @Who : Maninder
| @Function name : getticdescussion
| @why : to get All Descussions Related to a Particular Ticket ID
| @when : 11-07-2017
\*************************************/
var totaldescusstions = 0,
	currentdescussionindex = 0,
	descussionsarr = [],
	base_url = "";

app.post('/getticdescussion', (req, res) => {
	base_url = req.body.base_url;
	db.collection('ticket_discussions').find({ticket_id: req.body.ticket_id }).toArray((error1, theresult) => {
			   if (error1) {
			   	throw error1;
			   }
	   			  descussionsarr = theresult;
	   			//  console.log(descussionsarr);
				  totaldescusstions =  theresult.length;
				  if(totaldescusstions > 0 ){
				  	checkimg(currentdescussionindex,descussionsarr,req, res);	
				  }else {
				  		res.json({
			     	  		status : 'success' , 
			    		  	resultData : theresult 
			    		   });
				  }
			   
		     	  	
	    
			  });

});

/*************************************\
| @Who : Maninder
| @Function name : checkimg
| @why : Helper Function to getticdescussion. 01b3ee602584eec92e2528721e476d34
| @when : 30-07-2017
\*************************************/


function checkimg(currentdescussionindex,descussionsarr,req, res)	{
		if(descussionsarr[currentdescussionindex].replier_id !== 0){
			connection.query('SELECT id,image_url from users where id="'+descussionsarr[currentdescussionindex].replier_id+'"', function (err, ticrows, fields) {
				console.log(ticrows[0].image_url);
				var img = ticrows[0].image_url;
				console.log(img);
				var fullpath = config.basedir+'webroot/img/employee_image/'+ img;
				console.log(fullpath);
				fs.stat(fullpath, function(error, stat) {
					if(error == null) {
					        //Exist
					        descussionsarr[currentdescussionindex].image_url = ticrows[0].image_url;
					    } else if(error.code == 'ENOENT') {
					    	descussionsarr[currentdescussionindex].image_url = 'no_image.png';
					    } 
					    currentdescussionindex += 1;
						if(currentdescussionindex < totaldescusstions){
						 checkimg(currentdescussionindex,descussionsarr,req, res);
						} else {
							res.json({
				     	  		status : 'success' , 
				    		  	resultData : descussionsarr 
				    		   });
						}
					});
			});	
				
		} else {
			currentdescussionindex += 1;
			if(currentdescussionindex < totaldescusstions){
			 	checkimg(currentdescussionindex,descussionsarr,req,res);
			} else {
						res.json({
			     	  		status : 'success' , 
			    		  	resultData : descussionsarr 
			    		   });
			}
		}
		
		
}

/*************************************\
| @Who : Maninder
| @Function name : descussionscrone
| @why : to get Last replier Information of a Mandatory Ticket
| @when : 11-07-2017
\*************************************/

app.get('/descussionscrone', (req, res) => {
		connection.query('SELECT * from users where status="1"', function (err, rows, fields) {
				if (err) console.log(err);

				//res.json({status : 501, response : 'failed'});  // an error occurred
					userdata = rows;
					totalusers = rows.length;
					iterateusers(rows[0]['id'],0,rows,req,res);
		
				
		});
});


/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : to get Last replier Information of a Mandatory Ticket
| @when : 11-07-2017
\*************************************/

function iterateusers(id,index,rows,req,res){

	connection.query('SELECT * from tickets where to_id="'+id+'"', function (err, ticrows, fields) {
	console.log('ticrows.length'+ticrows.length);
			if (err)  console.log(err);	//res.json({status : 501, response : 'failed'});  // an error occurred
				docs = [];
				var counter = 0;
				var total = 0;
				if(ticrows.length > 0){
					for(var i=0; i < ticrows.length; i++){
		    		 var replier_name = rows[0]['first_name']+' ' +rows[0]['last_name'];
		    		 var ticket_id = ticrows[i]['id'];
		    		 var replier_id = id;
		    		 var response_text = ticrows[i]['response'];
		    		 var dt = datetime.create();
		  			 var fomratted = dt.format('Y-m-d H:M:S');
		  			 var ip_address = ticrows[i]['ip_address'];
		  			 var objectId = new ObjectID(); // creating uniuq id for the each document
		  			 var finaledata = { ticket_id : ticket_id , _id : objectId ,replier_id : replier_id , replier_name : replier_name , response_text : response_text ,  created_at : fomratted , ip_address : ip_address};
		  			nonnullcounter += 1;
		  			if(ticrows[i]['response'] !== 'null') {
		  				if(ticrows[i]['response'] !== null) {
		  					if(ticrows[i]['response'] !== 'NULL') {
		  						nonnullcounter += 1;
		  						docs.push(finaledata);		
		  					}
		  					
		  				}
		  				
		  			}
		  			counter++;
		  			if(docs.length > 0 && counter == ticrows.length){
						db.collection('ticket_discussions').insertMany(docs, (err, result) => {
		  				  if (err){
					    }	
							
							 res.json({status : 200, response : 'success'});
						
				  		});	
					}

					if(usercounter < totalusers && counter == ticrows.length ){
						usercounter += 1;
						newindex = index +1;
						newid = userdata[newindex]['id'];
						iterateusers(newid,newindex,rows,req,res);
					}
				}
				}else{
					if(usercounter < totalusers ){
						usercounter += 1;
						newindex = index +1;
						newid = userdata[newindex]['id'];
						iterateusers(newid,newindex,rows,req,res);
					}
				}
				
				
					
				
			});

}

/*************************************\
| @Who : Maninder
| @Function name : lastrepier
| @why : to get Last replier Information of a Mandatory Ticket
| @when : 11-07-2017
\*************************************/

app.post('/lastrepier', (req, res) => {
	db.collection('ticket_discussions').find({ticket_id: parseInt(req.body.ticket_id) }).sort({$natural:-1}).limit(20).toArray((error1, theresult) => {
			   if (error1) {
			   	throw error1;
			   }
			   		console.log(theresult);
		     	  res.json({response : 'success' , 
		     	  			status : 200,
		    		  		resultData : theresult 
		    		   });
	    
		    });
});
/*FOR SAVING DATA IN FOR Ticket Discusstions*/
