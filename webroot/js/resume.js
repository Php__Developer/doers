$(document).ready(function(){


var base = $(document).find('.base').val();
var page_name = $(document).find('.pe').val();

   

      //console.log(base);

      /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

$(document).on('click', '.firstLISt li', function(event) {
//$('.alldata').on('click', '.firstLISt li', function(event) {
 event.preventDefault();
  var active_class= $(this).find("a span i").attr('class');
  if(active_class=="ti-home"){
          $(this).closest(".nav-tabs").find(".profile").removeClass('active');
          $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
          $(this).closest(".nav-tabs").find(".home").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").addClass('active');
   }else if(active_class=="ti-user"){
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
           $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").addClass('active');
        $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
      $(this).closest('.alldata').find('.tab-content').find(".iprofile").addClass('active');
   }else if(active_class=="ti-email"){
     $(this).closest(".nav-tabs").find(".settings").removeClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").addClass('active');
         $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".isettings").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".imessages").addClass('active');
   }else if(active_class=="ti-settings"){
      $(this).closest(".nav-tabs").find(".settings").addClass('active');
           $(this).closest(".nav-tabs").find(".home").removeClass('active');
           $(this).closest(".nav-tabs").find(".profile").removeClass('active');
           $(this).closest(".nav-tabs").find(".messages").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".iprofile").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".imessages").removeClass('active');
    $(this).closest('.alldata').find('.tab-content').find(".ihome").removeClass('active');
          $(this).closest('.alldata').find('.tab-content').find(".isettings").addClass('active');
   }


   });

  /* @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */


   $(document).on('click', '.drp_list li', function(e) {
  e.preventDefault();

   var clicked_icon=$(this).find("a").attr("class");
  if(clicked_icon==1){
   
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".1").text();
            var b= $(b).find(".1").text();
            return a > b;
            }) ); 
   
   }else if(clicked_icon==2){
    
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".2").text();
            var b= $(b).find(".2").text();
            return a > b;
            }) ); 
   
   }else if(clicked_icon==3){
   
            $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".3").text();
            var b= $(b).find(".3").text();
            return a > b;
            }) ); 
   
     }else if(clicked_icon==4){
                    $(document).find(".parent_data").html(
           $(".alldata").sort(function (a, b) {
            var a= $(a).find(".created").val();
            var b= $(b).find(".created").val();
            return a > b;
            }) ); 
     
        }else if(clicked_icon==5){
           
           $(document).find(".parent_data").html(
            $(".alldata").sort(function (a, b) {
            var a= $(a).find(".5").text();
            var b= $(b).find(".5").text();
            return a > b;
            }) ); 
         
      }  

  //$('[data-toggle="popover"]').popover();

  });


  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop2').hide();
  });

  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop1').hide();
  });


// for delete icon
      /*
     @WHY => use for the search function on category blade
     @WHEN  => 15-02-2016
     @WHO => Gurpreet Kaur
     */

$(document).on('click', '.deleteprojectdata', function(event) {
             event.preventDefault();
             $(document).find('.alldata').removeClass('clickedone');
              $(this).closest('.alldata').addClass('clickedone');
    var ref = $(this).attr('href');
        swal({   
            title: "Are you sure to delete?",   
            text: "You will not be able to recover this Data!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
          },function(){ 
              $.ajax({
              url: ref,
              type: "get",
              beforeSend: function() {
              },
              complete: function(){
              },
              success: function(data){
               var data= JSON.parse( data );
             if(data.status == 'success' ){
                  $('.clickedone').hide();
                  swal("Deleted!", "Data has been Deleted!", "success"); 
                 }
               }
            });
         
        });
    });



}); //end function

