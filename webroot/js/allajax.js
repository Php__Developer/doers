$(document).ready(function(){
	

	/**********************************************************************/
	/* @page : tickets/add
	/* @who :Maninder
	/* @why : for adding new tickets
	**********************************************************************/

  $(".add_ticket").on("click", function() {

  	var base_url = $('.base').text();
	var to_id = $('#toID').val();
		if(notEmpty(to_id,'error_to','Select User.'))
		{ return false; }
		var title = $('.title').val();
		if(notEmpty(title,'error_title','Enter title.'))
		{ return false; }
		var projectid = $('#project').val();
		var tasktime = $('#tasktime').val();
		if(hourminformat(tasktime,'error_time','Enter valid time.'))
		{ return false; }
		var billable_hours = $('#billable_hours').val();
		if(hourminformat(billable_hours,'error_bill','Enter valid time.'))
		{ return false; }
		var desc = $('#desc').val();
		if(notEmpty(desc,'error_desc','Enter description.'))
		{ return false; }
		var deadline = $('#deadline').val();
		if(notEmpty(deadline,'error_dead','Choose deadline.'))
		{ return false; }
		var type = $('#type').val();
		var priority = $('#priority').val();
                $('#postData').hide();
		if($.active>0){
		}else{
			href = base_url+'tickets/add';
			$.post(href,{'to_id':to_id,'title':title,'desc':desc,'deadline':deadline,'type':type,'priority':priority,'tasktime':tasktime,'projectid':projectid,'billable_hours':billable_hours},
			function(data){
				
			if(data == 'success') {

                            var vaccume="";
                                $('.title').val(vaccume);
                                $('#desc').val(vaccume);
                                $('#postData').show();
				window.$('#close-fb').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
                 parent.location.reload(true);
	  $('#message-green').append('<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="green-left">saves successfully.</td><td class="green-right"></tr></tbody></table>');
			}

			});
		}
		});
// update ticket code here 
	/**********************************************************************/
	/* @page : tickets/response
	/* @who :Maninder
	/* @why : for updating tickets
	**********************************************************************/

 $(".update_ticket").on("click", function() {
 	var base_url = $('.base').val();
 	//console.log(base_url+'tickets/response');

 	var to_id = $('#toID').val();
		if(notEmpty(to_id,'error_to','Select User.'))
		{ return false; }
		var projectid = $('#project').val();
		var tasktime = $('#tasktime').val();
		if(hourminformat(tasktime,'error_time','Enter valid time.'))
		{ return false; }
		var billable_hours = $('#billable_hours').val();
		if(hourminformat(billable_hours,'error_bill','Enter valid time.'))
		{ return false; }
		var desc = $('#desc').val();
		var title = $('#title').val();
		if(notEmpty(desc,'error_desc','Enter description.'))
		{ return false; }
		var response = $('#response').val();
		if(notEmpty(response,'error_resp','Enter Response.'))
		{ return false; }
		var status = $('#status').val();
		var deadline = $('#deadline').val();
		var last_page = $('#last_page').val();
		var type = $('#type').val();
		var priority = $('#priority').val();
		var ticket_id = $('.ticket_id').val();
		if(desc || desc=='' )
		{
			data_to_post = {"to_id":to_id,"status":status,"title":title,"description":desc,"deadline":deadline,"id":ticket_id,'last_page':last_page,'type':type,'priority':priority,'tasktime':tasktime,'projectid':projectid,'billable_hours':billable_hours};
		}else if(response || response==''){
			data_to_post = {"to_id":to_id,"status":status,"response":response,"deadline":deadline,"id":ticket_id,'last_page':last_page,'type':type,'priority':priority,'tasktime':tasktime,'projectid':projectid,'billable_hours':billable_hours};
		}else{
			data_to_post = {"to_id":to_id,"status":status,"deadline":deadline,"id":ticket_id,'last_page':last_page,'type':type,'priority':priority,'tasktime':tasktime,'projectid':projectid,'billable_hours':billable_hours};
		}
                //$('#postData').hide();
        //console.log(data_to_post);
		if($.active>0){
		}else{ 
			href = base_url+'tickets/response';
			$.post(href,data_to_post,function(data){
			//	alert(data);
					
			if(data == '1') {
                            var vaccume="";
                                $('#postData').show();
				window.$('#close-fb').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
                 parent.location.reload(true);
	           $('#message-green').append('<table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="green-left">saves successfully.</td><td class="green-right"></tr></tbody></table>');
			}
			});
		}

 });
 	if($(document).find('input').hasClass('mode')  && $(document).find('.mode').val() == 'edit'){
			// while editing
			var timein = $(".time_in_hidden").val();
			var timeout = $(".time_out_hidden").val();
			console.log(timein);
			console.log(timeout);
			var dt = new Date();
			var hours = formatAMPM(dt);	
 	} else{
 			// while adding
			var dt = new Date();
			var hours = formatAMPM(dt);	
 	}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = [hours,minutes,ampm];
  return strTime;
}

	
  	// foreach for hours
  	// to create dynamic date time select lists 
  	function timeselecthour(theHTML,mode,type){
  		if(mode == 'edit'){
  			if(type=='in'){
  				var time = $(".time_in_hidden").val();
  			} else{
  				var time = $(".time_out_hidden").val();
  			}
			var dt = new Date(time);
			 hours = formatAMPM(dt);	
  		}
  		var x = 1;
	  	for (i = x; x < 13; x++) {
	  		x = (x < 10 )? '0'+x : x;
	  		if(hours[0] == x){
	  			theHTML+='<option value="'+x+'" selected ="selected">'+x+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+x+'">'+x+'</option>';	
	  		}
	    }
	    	theHTML+= '</select>';
	    return theHTML;
	  } 

	    
	    // foreach for minutes
	  function timeselectmin(theHTML,mode,type){
	  		if(mode == 'edit'){
  			if(type=='in'){
  				var time = $(".time_in_hidden").val();
  			} else{
  				var time = $(".time_out_hidden").val();
  			}
			var dt = new Date(time);
			console.log(dt);
			 hours = formatAMPM(dt);	
  		}
	    var x = 00;
	  	for (i = x; x < 60; x++) {
	  		x = (x < 10 )? '0'+x : x;
	  		if(hours[1] == x){
	  			theHTML+='<option value="'+x+'" selected ="selected">'+x+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+x+'">'+x+'</option>';	
	  		}
	    }
	     theHTML+= '</select>';
	    return theHTML;
	   }
	         // foreach for am/pm
	   function timeselectmeridian(theHTML,mode,type){
	   		if(mode == 'edit'){
  			if(type=='in'){
  				var time = $(".time_in_hidden").val();
  			} else{
  				var time = $(".time_out_hidden").val();
  			}
			var dt = new Date(time);
			 hours = formatAMPM(dt);	
  		}
	    var x = 00;
	    arr = ['am','pm'];
	  	for (i = x; x < 2; x++) {
	  		if(hours[2] == arr[x]){
	  			theHTML+='<option value="'+arr[x]+'" selected ="selected">'+arr[x]+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+arr[x]+'">'+arr[x]+'</option>';	
	  		}
	    }

	    return theHTML;
		}
	
  	//var theHTML = "";
  	if($(document).find('input').hasClass('mode') ){
  		var mode = $('.mode').val();
  	} else{
  		var mode = 'add';
  	}
  	timeinhour =  timeselecthour('<select id="AttendanceTimeInHour" name="Inhour">',mode,'in');
  	timeinmin =  timeselectmin('<select id="AttendanceTimeInMin" name="Inmin">',mode,'in');
  	timein_ampm =  timeselectmeridian('<select id="AttendanceTimeInMeridian" name="Inmeridian">',mode,'in');
  	var comletetimein = timeinhour+timeinmin+timein_ampm;
    $(".time-in").html('');
    $(".time-in").append(comletetimein);
    // creating select list for the timout
 	timeouthour =  timeselecthour('<select id="AttendanceTimeoutHour" name="Outhour">',mode,'out');
  	timeoutmin =  timeselectmin('<select id="AttendanceTimeoutMin" name="Outmin">',mode,'out');
  	timeout_ampm =  timeselectmeridian('<select id="AttendanceTimeoutMeridian" name="Outmeridian">',mode,'out');
  	var comletetimeout = timeouthour+timeoutmin+timeout_ampm;
    $(".time-out").html('');
    $(".time-out").append(comletetimeout);





/*function formatAMPM(date) {
  var hours = date.getHours();
  //alert(hours);
  var minutes = date.getMinutes();
  //alert(minutes);
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = [hours,minutes,ampm];
  return strTime;
}
 	
	var dt = new Date();
	var hours = formatAMPM(dt);
  	// foreach for hours
  	// to create dynamic date time select lists 
  	function timeselecthour(theHTML){
  		var x = 1;
	  	for (i = x; x < 13; x++) {
	  		if(hours[0] == x){
	  			theHTML+='<option value="'+x+'" selected ="selected">'+x+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+x+'">'+x+'</option>';	
	  		}
	    }
	    	theHTML+= '</select>';
	    return theHTML;
	  } 

	    
	    // foreach for minutes
	  function timeselectmin(theHTML){
	    var x = 0;
	  	for (i = x; x < 60; x++) {
	  		if(hours[1] == x){
	  			theHTML+='<option value="'+x+'" selected ="selected">'+x+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+x+'">'+x+'</option>';	
	  		}
	    }
	     theHTML+= '</select>';
	    return theHTML;
	   }
	         // foreach for am/pm
	   function timeselectmeridian(theHTML){  
	    var x = 0;
	    arr = ['am','pm'];
	  	for (i = x; x < 2; x++) {
	  		if(hours[2] == arr[x]){
	  			theHTML+='<option value="'+arr[x]+'" selected ="selected">'+arr[x]+'</option>';	
	  		} else{
	  			theHTML+='<option value="'+arr[x]+'">'+arr[x]+'</option>';	
	  		}
	    }

	    return theHTML;
		}
	
  	//var theHTML = "";
  	
  	timeinhour =  timeselecthour('<select id="AttendanceTimeInHour" name="Inhour" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	timeinmin =  timeselectmin('<select id="AttendanceTimeInMin" name="Inmin" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	timein_ampm =  timeselectmeridian('<select id="AttendanceTimeInMeridian" name="Inmeridian" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	var comletetimein = timeinhour+timeinmin+timein_ampm;
    $(".time-in").html('');
    $(".time-in").append(comletetimein);
    // creating select list for the timout
 	timeouthour =  timeselecthour('<select id="AttendanceTimeoutHour" name="Outhour" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	timeoutmin =  timeselectmin('<select id="AttendanceTimeoutMin" name="Outmin" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	timeout_ampm =  timeselectmeridian('<select id="AttendanceTimeoutMeridian" name="Outmeridian" class="selectpicker m-b-20 m-r-10" data-style="btn-primary btn-outline">');
  	var comletetimeout = timeouthour+timeoutmin+timeout_ampm;
    $(".time-out").html('');
    $(".time-out").append(comletetimeout);*/

    // appending date function ended 


   /**********************************************************************/
   /* @page :activeleads
   /* @who :Maninder
   /* @why : for changing the status on change of dropdown
    **********************************************************************/

    $('.ourselect_al').on("change", function( event ) {
			
				var status = $(this).val();
				var id = $(this).attr('id');
				temp=id.split("_");
				id=temp[1];
				if(confirmAction()){
					$.post(base_url+'leads/activelead',{'id':id,'status':status},function(data){
						if(data==1){
							$('#stat_'+id).closest("tr").remove();
						}
					});
				}else{
				//$('#stat_'+id).val(status);
				return false;
				}
			});

   /**********************************************************************/
   /* @page :
   /* @who : 
   /* @why : 
    **********************************************************************/

});