$(document).ready(function(){
var base = $(document).find('.base').val();
var socketurl = 'http://localhost:8080';
var originagency = $('.emporiginagency').val();
var tag_id = "";
var selected_projects=[];
var currentpage = $('.currentpage').val();

if($(document).find('input').hasClass('saved_projects')){
		var saved_projects = $('.saved_projects').val();
		var url =  base + 'contacts/getresults';
		var datatopost ={project_id : saved_projects , type : 'saved_projects' }
		postdata(datatopost,url,'saved_projects');	
}




	$('.selectwrapper').on("change", ".agenciesselect", function(event) {
          event.preventDefault();
          event.stopImmediatePropagation();
          var agency_id = $(this).val();
          //console.log(agency_id);
          var datatopost = {"key": 'agencyroles', tag_id :  tag_id , agency_id : agency_id };
		  var url = base + 'tags/manage';
		  var type ='agencyroles';
		  rolesnamesonly =[];
		  finalroles =[];
          postdata(datatopost,url,type);
          //$('#dialog_modal').html('');

          //opendialog($(this).attr('href'),350,600,'yes',true);
   });


	function removeelement1(arr,name,field){
		var _proto = arr;
		for(var i=0; i < arr.length; i++){
	    	if(arr[i][field] == name){
	      		arr.splice(i,1); 
	    	}
			}
		return arr;
	}

	function removeindexel(arr,field){
		
		var index = arr.indexOf(field);
		if (index > -1) {
		    arr.splice(index, 1);
		}
		return arr;
	}




  function opendialog(page,height,width,closebtnneeded,mdbool) {
          
          $('.ui-dialog-buttonset').show();
          $('#dialog_modal').show();
          $("#dialog_modal").css("overflow", "hidden");
          var $dialog = $('#dialog_modal')
          .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
          .dialog({
            show: { effect: "fade", duration: 500 },
            autoOpen: false,
            dialogClass: 'dialog_fixed fixed-dialog cm_responsive',
            modal: mdbool,
            height: height,
            width:width,
            minWidth: 400,
            minHeight: 400,
            draggable:false,
            buttons: { 
                  "Close": function () {   $(this).dialog("close");
                      $(this).dialog('destroy');
                      $('#dialog_modal').html(''); 
                    } 
                  }
                });
          $dialog.dialog('open');
          $(".ui-dialog-titlebar").hide(); /*to hide the title bar on the dialog */
          if(closebtnneeded == 'no'){
            $('.ui-dialog-buttonpane').hide(); /*to hide bottom button bar on the dialog */  
          }
          
        }

  function postdata(datatopost,url,type){

							$.ajax({
							    url: url,
							    type: "post",
							    data: datatopost,
							    beforeSend: function() {
							    	//console.log("Beforsend")
							    },
							    complete: function(){
							    	//console.log("complete")
							    },
							    success: function(data){

							    	if(typeof data =='object'){

										if(data.status =='success'){

											 if (type == 'manage'){
											 	
							    				} else if (type == 'agencyroles'){
							    					triggerautocomplete(".restagsinputwrapper" ,rolesarr,'.restag51nput', 'responsibles');
									    		} else if (type == 'addnewrole'){
									    			tiggermsgc('success','Role Added!');
									    		}  else if (type == 'removerole'){
									    			tiggermsgc('success','Role Removed!');
									    		} else if(type == 'tagcontent'){
									    			tiggermsgc('success','Content Added!');
									    			setTimeout(function(){
									    				$('.cls_host').trigger('click');	
									    			},500)
									    			
									    		} else if (type == 'removeentity'){
									    			tiggermsgc('success','Entity Removed!');
									    		} else if (type == 'addentity'){
									    			tiggermsgc('success','Entity Added!');
									    		} else if (type == 'delete_tag_agency'){
									    			tiggermsgc('success','Agency Removed!');
									    		} else if( type == 'saved_projects'){
									    			setTimeout(function(){
									    				$.each(data.projects, function( index, value ) {
									    					selected_projects.push(value.id);                               
					                                		$('.restag51nput1').tagsinput('add', {Value : value.id , label : value.project_name} );	
									    				});
									    				
									    			},50);
									    			
									    		}
										} 						  
									} else {
											if(JSON.parse(data).status =='success'){
							    		if(type == 'addjob'){
							    			tiggermsgc('success','Project Created Successfully!');	
							    			dp = { project_id : JSON.parse(data).id ,base_url : base};
							    			postdata(dp,socketurl+'/reloadstatsforone','savestatsforproject');
							    			//window.parent.location.href = base + 'projects/projectdetails/'+ JSON.parse(data).id;	
							    		}  
							    		}	
									}
							    
							    	
								},
							    error: function(result){
							        //console.log(result);
							    }
		  				});
			} 

			function triggerautocomplete(classname ,sourcearr,tagsinputclass, finaldataindex){
          ////console.log(tagsarr);
          $(classname).find('.bootstrap-tagsinput').css('width','100%');
             $(classname).find('.bootstrap-tagsinput').find('input').autocomplete({
                source: function(request, response) {
                  $.getJSON( base+'/contacts/getresults', { classname : classname ,term : request.term}, 
                            response);
                },
                //source:base+'/getresults',
                minLength: 2,
                autoFocus: true,
                select: function (event, ui) {
                 // ////console.log(ui.item);
                  var selected = ui.item.Value;
                   var result1 = $.inArray(selected, selected_projects);
                        if(result1 == -1){
                              ////console.log(ui.item);
                              selected_projects.push(selected);
                               
                                $('.restag51nput1').tagsinput('add', ui.item);
                                ////console.log(selectedtags);    
                        } else {
                          ////console.log("Already IN"); 
                          
                        }

                        setTimeout(function(){
                            //$('.restag51nput1').find('.bootstrap-tagsinput').find('input').val(' ')  
                            $('.restagsinputwrapper1').find('.ui-autocomplete-input').val(' ')  
                            
                          },50);

                        console.log(selected_projects);

                }
              });
        }

			  //triggertagsinput('.project_ac');
			  triggertagsinput('.restag51nput1');
			  triggerautocomplete(".restagsinputwrapper1" ,[],'restag51nput1', 'responsibles');

			  function triggertagsinput(classname){
			  	//alert(classname);
			  	//alert($(classname).attr("class"));
				$(classname).tagsinput({
					confirmKeys: [13, 188],
					freeInput: false,
					 itemText: 'label',
          			 itemValue: 'Value'
				});
				$(classname).on('beforeItemRemove', function(event) {
					console.log("Removed");
					console.log(event.item);

				})

				$(classname).on('beforeItemAdd', function(event) {
					
				});
				//triggerautocomplete('.ac_wrapper' ,[],classname, 0)
				


			  }

			  

			    function trigger_error(result,field,display_name){
			  		$(document).find('.'+ field).closest('.form-group').find('.errors').html('');
			  		if(result['reason'] == 'empty'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append(display_name + ' Can not be Empty!');
			  			var txt = display_name + ' Can not be Empty!';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		} else if(result['reason'] == 'outofarray'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append('Please Select '+display_name +' from suggestions');
			  			var txt = 'Please Select '+display_name +' from suggestions';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		} else if(result['reason'] == 'notnumberic'){
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append(display_name +' should be Numeric');
			  			var txt = display_name +' should be Numeric';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  			//$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ display_name +' should be Numeric</div>');
			  		} else if (result['reason'] == 'cannotremoveadmin'){
			  			var txt = display_name +' can not be removed';
			  			$(document).find('.'+ field).closest('.form-group').find('.errors').append('<div class="alert alert-danger alert-dismissable default_visible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+ txt +'</div>');
			  		}
			  		$(document).find('.'+ field).closest('.form-group').find('.bootstrap-tagsinput').find('input').show();
			  }

			      function tiggermsgc(type,msg){
			        if(type=='info'){
			          $(document).find('.succesfontwrapper').find('.succesfont').html(' ');
			          $(document).find('.succesfontwrapper').find('.succesfont').append(msg);
			          $(document).find('.succesfontwrapper').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.succesfontwrapper').removeClass('default_visible');
			          }, 3000);
			        } else if(type == 'success'){
			          $(document).find('.successwrapperdv').find('.successmsg').html(' ');
			          $(document).find('.successwrapperdv').find('.successmsg').append(msg);
			          $(document).find('.successwrapperdv').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.successwrapperdv').removeClass('default_visible');
			          }, 3000);
			        } else if(type == 'error') {
			          $(document).find('.succesfontwrapper').find('.succesfont').html(' ');
			          $(document).find('.succesfontwrapper').find('.succesfont').append(msg);
			          $(document).find('.succesfontwrapper').addClass('default_visible');
			          setTimeout(function(){ 
			            $(document).find('.succesfontwrapper').removeClass('default_visible');
			          }, 3000);
			        }
			        
			      }

});