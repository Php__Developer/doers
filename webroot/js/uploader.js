/********************************************************************************
* This script is brought to you by Vasplus Programming Blog
* Website: www.vasplus.info
* Email: info@vasplus.info
*********************************************************************************/

//This does the file Uploads
$(document).ready(function()
{ 
	var projectid = $('#projectid').val();
	var uploaded_files_location = "../files";
	new AjaxUpload($('#vpb_upload_button'), 
	{ 
		action: baseurl+'admin/projects/import/'+projectid,
		name: 'file_to_upload',
		onSubmit: function(file, file_extensions)
		{  
			$('.vpb_main_demo_wrapper').show(); //This is the main wrapper for the uploaded items which is hidden by default
			//Allowed file formats jpg|png|jpeg|gif|pdf|docx|doc|rtf|txt - You can add as more file formats or remove as you wish.
			if (!(file_extensions && /^(csv)$/.test(file_extensions)))
			{
				//If file format is not allowed then, display an error message to the user
				$('#vpb_uploads_error_displayer').css('display','block');
				$('#vpb_uploads_error_displayer').html('<div class="vpb_error_info" align="left">Sorry, you can only upload CSV format files. Thanks...</div>');
				setTimeout(function() { $('#vpb_uploads_error_displayer').fadeOut('slow'); }, 2000);
				return false;
			}
			else
			{ 
			  $('#vpb_uploads_error_displayer').html('<div class="uplading_image">Uploading <img src="'+baseurl+'app/webroot/images/loadings.gif" align="absmiddle" /></div>');
			  return true;
			}
		},
		onComplete: function(file, response)
		{
			//alert(response);
			if(response==="ERROR")
			{
				window.location.reload();  
		
			}else if(response==="SUCCESS"){
				window.location.reload();  
			}
			else
			{
				//alert(response);
				$('#showres').html(response);
				var tasks = JSON.parse(response).numberoftaks;
				var project = JSON.parse(response).projectName;
				if(getConfirmation("Are you sure to import "+tasks+" of tickets in "+project+" ?")){
					$.post(baseurl+'admin/projects/readcsv/',{mode:'1'},function(data){
							window.location.reload();   
					});
				}else{ 
					jQuery('.uplading_image').css('display','none');
				}
			}
		}
	});
	
});


//This removes all unwanted files
function remove_unwanted_file(id,file)
{
	if(confirm("If you are sure that you really want to remove the file "+file+" then click on OK otherwise, Cancel it."))
	{
		var dataString = "file_to_remove=" + file;
		$.ajax({
			type: "POST",
			url: "remove_unwanted_files.php",
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
				$("#vpb_remove_button"+id).html('<img src="images/loadings.gif" align="absmiddle" />');
			},
			success: function(response) 
			{
				$('div#fileID'+id).fadeOut('slow');	
			}
		});
	}
	return false;
}