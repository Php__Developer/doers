<script type="text/javascript">
	$(document).ready(function() 
	{	
		//fancybox settings
		$(".view_template").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	550, 
				'height' :	650, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
			//fancybox settings for instructions
		$(".view_inst").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	960, 
				'height' :	459, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
		$("#toggleOrder").click(function(){
			var value = $(this).val() ;
			if(value == "Orber by Deadline"){
				$("#toggleOrder").val('Orber by Modified');
				var pathArray = window.location.pathname.split( '/' );
				if(pathArray[5]=="key" ||  pathArray[5]=="modified"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline/'+pathArray[6]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline');
				}
				return true;
			}else if(value == "Orber by Modified"){
				$("#toggleOrder").val('Orber by Deadline');
				var pathArray = window.location.pathname.split( '/' );
				if($("#toggleOpen").val()==1){
					$('#ticketReport').attr('action',base_url+'admin/tickets/');
				}else if(pathArray[5]=="key" ||  pathArray[5]=="deadline"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/modified/'+pathArray[6]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/index/');
				}
				return true;
			}
		});
		
		 $("#toggleOpen").click(function(){
				if($('#toggleOrder').val()=="Orber by Modified"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline');
					$('#ticketReport').submit();
				} 
				$('#ticketReport').submit();
		
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
</script>
<!--  start content-table-inner -->
<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
<?php echo $form->create('Ticket',array('action'=>'','method'=>'POST','onsubmit' => '',"class"=>"login","id"=>"ticketReport")); ?>
<?php 		$session_info = $this->Session->read("SESSION_ADMIN");
		$userName = $session_info[1]; ?>
<div id="content-table-inn">
	<div class="addLinks" style="float:left;margin-left:85em;">
		<?php echo $html->link("View Instructions",
					array('controller'=>'static_pages','action'=>'view','85'),
					array('class'=>'view_inst','alt'=>'Instructions','title'=>'Instructions','style'=>'color:red')
					);	
			?>
	</div>
	<div class="addLinks">
		<?php echo $html->link("Add Ticket",
					array('controller'=>'tickets','action'=>'add'),
					array('class'=>'view_template','alt'=>'ticket')
					);	
			?>
	</div>

	<div id="showLegend" style="float:left; margin-top:-22px;"> 
		<?php $classLink1=""; $classLink2=""; $classLink3=""; $checkurlarray = explode("/", $this->params['url']['url']);  
					if(isset($checkurlarray['4'])){
						if($checkurlarray['4'] == "open")
						$classLink1 = "openlink";
						else if($checkurlarray['4'] == "close")
						$classLink2 = "openlink";
						else if($checkurlarray['4'] == "expire")
						$classLink3 = "openlink";
					}
		?>
		<span>	<?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_blue.png', array('width' => '3%','height'=>'20%'))?>
		<?php echo $html->link("Open",
					array('controller'=>'tickets','action'=>'index','key','open'),
					array('alt'=>'open','class'=>"$classLink1 ")
					);	
			?>
		 </span> 
		<span>   <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_green1.png', array('width' => '3%','height'=>'20%'))?>
			<?php echo $html->link("Closed",
					array('controller'=>'tickets','action'=>'index','key','close'),
					array('alt'=>'close','class'=>"$classLink2 ")
					);	
			?>
		 </span> 
		<span> <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_red1.png', array('width' => '3%','height'=>'20%'))?>
			<?php echo $html->link("Expired",
					array('controller'=>'tickets','action'=>'index','key','expire'),
					array('alt'=>'expire','class'=>"$classLink3")
					);	
			?>
		 </span>
		<span><?php 
			echo $form->input("check_it",array('type'=>'checkbox','id'=>'toggleOpen',"label"=>false,"div"=>false,"hiddenField"=>false));
			echo "<label style='font-weight:bold;color:#000' id='lbl_ticket' for ='check_it'>Show All</label>";
		?></span>
		<span>
			<?php 
		$name ="Orber by Deadline";
			if(isset($this->params['pass'][0]) && $this->params['pass'][0]=='deadline'){
			$name = 'Orber by Modified';
			echo $form->submit($name,array('div'=>false,'class'=>'orderby_modified info-tooltip','id'=>'toggleOrder','name'=>'orderby','title'=>'Orber by Modified')); }else{
			echo $form->submit($name,array('div'=>false,'class'=>'orderby_deadline info-tooltip','id'=>'toggleOrder','name'=>'orderby','title'=>'Orber by Deadline'));
			}
		?>
		</span>
		<span><b>Last Deadline : <?php echo date("d/m/Y h:i a",strtotime($deadline['Ticket']['deadline'])); ?></b></span>
	</div>
	<div class="clear"></div>
	<div id="ticketheader" class="grey-listleft">
		<div id="ticket_hnumber"> Ticket No.</div>
		<div id="ticket_htitle"> Title </div>
		<div id="ticket_replier" style="color:#FFFFFF"> Last Replier </div>
		<div id="ticket_type" style="font-size:12px;font-weight:bold;color:#FFFFFF"> Type </div>
		<div id="ticket_status" style="font-size:12px;font-weight:bold;color:#FFFFFF"> Status </div>
		<div id="ticket_modified" style="font-size:12px;font-weight:bold;color:#FFFFFF"> Last Updated </div>
		<div id="ticket_deadline" style="font-size:12px;font-weight:bold;color:#FFFFFF"> Deadline </div>
		<div id="ticket_hreply" > Response </div>
		<div id="ticket_hremind" > Reminder </div>
		<div id="ticket_hicon" >  </div>
	</div>
<?php echo $form->end(); ?>
 <?php  if(!empty($resultData))  { ?>
	<div id="ticketlist">
	<?php foreach($resultData as $ticketData) { ?>
	<?php if($ticketData['Ticket']['status']==0) { ?>
		<div class="green-listleft">
	<?php }
	elseif(($ticketData['Ticket']['status']==1 || $ticketData['Ticket']['status']==2) && ($ticketData['Ticket']['deadline'] > date("Y-m-d H:i:s")) ) { ?>
		<div class="blue-listleft">
	<?php } else {?>
		<div class="red-listleft">
	<?php } ?>
	
			<div id="ticket_number"><?php echo $ticketData['Ticket']['Ticket_number'];  ?> </div>
			<div id="ticket_title">
				<?php $str = strip_tags($ticketData['Ticket']['title']); echo substr($str,0,25).((strlen($str)>25)?"...":""); ?>
			</div>
			<div id="ticket_replier">
					<?php echo ($ticketData['Last_replier']['first_name']." ".$ticketData['Last_replier']['last_name']); ?>
			</div>
			<div id="ticket_type">
					<?php echo ucfirst($ticketData['Ticket']['type']);  ?>
			</div>
			<div id="ticket_status">
					<?php if($ticketData['Ticket']['status']==0){
									$Ticketstatus = "Closed";
								}elseif($ticketData['Ticket']['status']==1){
									$Ticketstatus = "Open";
								}else{
									$Ticketstatus = "<i>In Progress</i>";
								}
					?>
					<?php  echo $Ticketstatus; ?>
			</div>
			<div id="ticket_modified">
					<?php echo date("d/m/Y h:i a",strtotime($ticketData['Ticket']['modified']));  ?>	
			</div>
			<div id="ticket_deadline">
					<?php echo date("d/m/Y h:i a",strtotime($ticketData['Ticket']['deadline']));  ?>	
			</div>
					<div id="ticket_resp">
					<?php echo $html->link("",
								array('controller'=>'tickets','action'=>'response',$ticketData['Ticket']['Ticket_number'],$this->params["action"]),
								array('class'=>'view_template response-1','alt'=>'response')
							   );  ?>		
					</div>
					<div id="ticket_respond">
					<?php echo $html->link("",
								array('controller'=>'tickets','action'=>'index',$ticketData['Ticket']['Ticket_number']),
								array('class'=>'reminder-1 info-tooltip','title'=>'reminder',"onclick"=>'javascript:return getConfirmation("Are you sure , You want to send Reminder?");')
							   );  ?>
					</div>
					<?php  if($ticketData['Ticket']['from_id'] == $userID) { ?>
					<div id="ticket_assg1"> <?php echo $html->link("",
																		'#A',
																		array('class'=>'assg-2 info-tooltip','title'=>'To '.$ticketData['User_To']['first_name']." ".$ticketData['User_To']['last_name'])
																	   );  ?>
					</div>
					<?php } elseif($ticketData['Ticket']['to_id'] == $userID) {?>
					<div id="ticket_assg2"> <?php echo $html->link("",
																		'#A',
																		array('class'=>'assg-1 info-tooltip','title'=>'From '.$ticketData['User_From']['first_name']." ".$ticketData['User_From']['last_name'])
																	   );  ?> 
					</div>
					<?php } ?>
		
			</div>
		<?php } echo "</div>"; } else {?>
		<br/><br/>
		<div align="center" style='height:200px;font-size:14px;font-weight:bold;'> No Tickets to display </div>
		<?php } ?>
		<div style="clear:both;"></div>
</div>