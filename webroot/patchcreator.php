<?php
ini_set('display_errors',0);
// define some basics
$error=0;
if(isset($_POST['submitted'])){

	$copytofileArray=explode(" ",$_POST['sname']);
	$count = count($copytofileArray);
	$counter = 0;
foreach($copytofileArray as $copytofile){
	$dfile = $_POST['zipname'];
	$info = pathinfo($copytofile);
	$directory = $info['dirname'];

	$file= $info['basename'];

	if (!file_exists($dfile)) {
		mkdir($dfile, 0777, true);
	}
	$sourcefolder = $dfile.'/'.$directory;
	$newfilename = $sourcefolder.'/'.$file;

	if (!file_exists('/'.$copytofile)) {
		if (!file_exists($sourcefolder)) {
			mkdir($sourcefolder, 0777, true);
		}
		if (file_exists($sourcefolder)){
			
			if (!copy($copytofile, $newfilename)) {
				$error=1;
				echo  "<div class='error'>failed to copy $newfilename...\n</div>";
			}
		}
	}else{
			$error=1;
	echo  "<div class='error'>source file not found".'/'.$copytofile.'</div>';
	

	}
$counter++;
 }
 
 if($count == $counter && $error==0 ){
	echo '<div class="success">Successfully patch created</div>';
 }else{
 
	echo '<div class="error">Some thing went wrong</div>';
 }
}
?>
<html>
<head>

</head>
<style>
body{
font-family:Arial, Helvetica, sans-serif; 
font-size:13px;
}
.info, .success, .warning, .error, .validation {
border: 1px solid;
margin: 10px 0px;
padding:15px 10px 15px 50px;
background-repeat: no-repeat;
background-position: 10px center;
}
.info {
color: #00529B;
background-color: #BDE5F8;
background-image: url('info.png');
}
.success {
color: #4F8A10;
background-color: #DFF2BF;
background-image:url('success.png');
}
.warning {
color: #9F6000;
background-color: #FEEFB3;
background-image: url('warning.png');
}
.error {
color: #D8000C;
background-color: #FFBABA;
background-image: url('error.png');
}
</style>
<body>
<div class="info">
<form id='register' action='' method='post'
    accept-charset='UTF-8'>
<fieldset >
<legend>Patch Creator</legend>
<p>
<input type='hidden' name='submitted' id='submitted' value='1'/>
<label for='sname' >Source File(s)*: </label>
<textarea type='text' name='sname' id='sname'  required="required" style="width: 800px; height: 250px;" placeholder="app/controller/REST_Client.php app/view/test.php app/controller/test.php"/>
</textarea>
<br>
</p><p>
<label for='zipname' >Destination folder*:</label>
<input type='text' name='zipname' required="required"  placeholder="myApp"/>
</p>
<input type='submit' name='Submit' value='Create Now' />
 </p>
</fieldset>
</form>
</div>
</body>
</html>