<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class EmployeesTable extends Table
{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('users');
         $this->addBehavior('Timestamp');
        /*
          $this->hasMany('MyAppraisals', [
          'className' => 'Appraisals',
          'foreignKey' =>'user_id',
         'propertyName' => 'MyAppraisals'
            
        ]); */
          $this->hasOne('MyAppraisals', [
             'className' => 'Appraisals',
              'foreignKey' =>'user_id',
              'propertyName' => 'MyAppraisals' // balde contain
            
        ]);

    }

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

$validator
    ->notEmpty('employee_code', "Enter Employee Code.")
    ->notEmpty('first_name', "Enter first name.")
    ->notEmpty('last_name', "Enter last name.")
    ->notEmpty('father_name', "Enter father name.")
    ->allowEmpty('phone')
    ->allowEmpty('accountName')
    ->notEmpty('report_to', "Select Reporting Person.")
    ->notEmpty('full_address', "Enter address.")
    ->notEmpty('personal_email', "Enter Personal email.")
    ->notEmpty('username', "Enter your username or email.")
    //->notEmpty('current_salary', "Enter your current salary.")


    
  /*  ->add('username', [
        'noteasmpty' => [
            'rule' => 'notBlank',
            'last' => true,
            'message' => "Enter your username."
        ],
        'unique' => [
            'rule' => 'isUnique',
            'message' => "Username already exists."
        ]
    ])
    /*->add('gmail', [
        'notempty' => [
            'rule' => 'notBlank',
            'last' => true,
            'message' => "Enter your email."
        ],
        'unique' => [
            'rule' => 'isUnique',
            'message' =>"Email address already exists."
        ]
    ]) */


->add(
        'employee_code', 
        ['unique' => [
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'employee code already exists.',
            'on' => 'create'
            ]
        ]
    )

/*->add(
        'username', 
        ['unique' => [
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'this username already used']
        ]
    )
*/


    ->add('old_password', [
        'notempty' => [
            'rule' => 'notBlank',
            'last' => true,
            'message' => "Enter old password.",
        ],
        'ruleName2' => [
            'rule' => array('isOldPasswordExists'),
            'message' =>"Old password does not exists."
        ]
    ])
    ->add('confirm_password', [
        'notempty' => [
            'rule' => 'notBlank',
            'message' =>"Enter confirm password."
        ],
        'ruleName2' => [
            'rule' => array('matchPasswords','password'),
            'message' => "Password and confirm password must be same."
        ]
    ])
    ->add('dob', 'reg', [
        'rule' =>  function ($field , $provider) {
                    //pr(date_diff(date_create(date('Y-m-d')),date_create(date($field))));
                    //die;
                    //foreach($field as $key => $value){
                   // $v1 = trim($value);
                    if(date($field) > date('Y-m-d')){
                        return false; 
                    }
                     return true;
                 //}
                 },
        'message' => "Date of birth should not be greater than today's date."
    ])
    ->add('summary', 'notBlank', [
        'rule' =>'notBlank',
        'message' => "Enter description."
    ])
     ->add('url', 'notBlank', [
        'rule' =>array('url'),
        'message' => "Enter valid url of website.",
        'allowEmpty' => true,
    ]);
       
        return $validator;
    }








//  var $validate = array(         //   'employee_code' => array(               'rule' => 'notEmpty',               'message' => "Enter Employee Code."         ),
          



/*
 
 public function validationDefault( Validator $validator)
    {
        $validator
            ->add('user_name', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('You need to provide a user_name'),
            ])
            ->add('password', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('A Password is required')
            ]);
        return $validator;
    }
*/
    function matchPasswords($field = array(),$compare_field = null) {
        foreach($field as $key => $value){
        $v1 = trim($value);
        $v2 = trim($this->data[$this->name][ $compare_field ]);
        if($v1 != "" && $v2 !="" && $v1 != $v2){
            return false; 
        }
         return true;
    }
   }

    function isOldPasswordExists($field = array()) {
    // Import Session Component
        //App::import('Component', 'SessionComponent');
        $session = $this->request->session();

        
        $userSession = $session->read("SESSION_ADMIN");

        foreach( $field as $key => $value ){
            $v1 = md5(trim($value));
            $result = $this->find('first', array('conditions' => array('id' => $userSession[0], 'password'=>$v1),'fields'=>array('id')));
            if(!is_array($result)){
                return false; 
             }
             return true;
        }
   }


    function validDOB($field = array()) {
        foreach($field as $key => $value){
        $v1 = trim($value);
        if($v1 != "" && $v1 > date('Y-m-d')){
            return false; 
        }
         return true;
    }
   }
   
        

}


 ;?>