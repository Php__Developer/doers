<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class ModulesTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('modules'); 

      $this->hasMany('SubModules', [
          'className' => 'Modules',
            'foreignKey' => 'parent_module',
            'propertyName' => 'smodules',
            'bindingKey' => 'id'
        ]);
     $this->belongsTo('Perms', [
             'className' => 'Permissions',
              'foreignKey' =>'default_alias',
              'propertyName' => 'Perms' // balde contain
            
        ]);

  }

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

$validator
    ->notEmpty('module_name', "Field can not be left Empty.")
    ->notEmpty('agency_id', "Please select agency form DropDown.")
    ->notEmpty('display_name', "Enter Module Display Name.")
    ->notEmpty('controller', "Please Select Controller Name.")
    ->notEmpty('action', "Please Select Action Name.")
    ->notEmpty('is_enabled', "Please Specify this module is enabled or not.")
    ->notEmpty('is_displayed', "Please Specify this module is displayed or not.")
    ->notEmpty('parent_module', "Please Select Module.")
    ->notEmpty('default_alias', "Please Select Default Alias.")
    ->notEmpty('other_aliases', "Please There must be atleast one other Alias.")
    ->add(
        'module_name', 
        ['unique' => [
            'on' => 'create',
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'this Module Name already used'
            ]
        ]
    )->add(
        'default_alias', 
        ['unique' => [
            'on' => 'create',
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'Alias already exists'
            ]
        ]
    );

        return $validator;
    }


}

 ;?>
