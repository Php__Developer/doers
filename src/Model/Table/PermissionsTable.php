<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class PermissionsTable extends Table
{

 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('permissions'); // if for some reason naming conventiopsn does not work.
         $this->addBehavior('Timestamp');
    }



public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

    $validator
    ->notEmpty('SelectController')
    ->notEmpty('SelectController', 'Please Select Controller')
    ->notEmpty('controller')
    ->notEmpty('controller', 'Please Select Controller Action')
     ->notEmpty('action')
    ->notEmpty('action', 'Please Select Controller Action')

    ->add(
        'alias', 
        ['unique' => [
            'on' => 'create',
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'Alias already exists'
            ]
        ]
    );
       return $validator;

  }
     

}

?>