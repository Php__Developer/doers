<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class LearningCentersTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('learning_centers'); // if for some reason naming conventiopsn does not work.
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
             'propertyName' => 'User_data'
        ]);

    }



}

 ;?>
 