<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class NotesTable extends Table
{

public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->table('notes'); // if for some reason naming conventiopsn does not work.
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
            
        ]);
   
    $this->belongsTo('Creater', [
            'className' => 'Users',
            'foreignKey' => 'userid',
            'propertyName' => 'Creater'

      ]);
        $this->belongsTo('Modifier', [
            'className' => 'Users',
            'foreignKey' => 'last_modifiedby',
            'propertyName' => 'Modifier'
       ]);
        /*$this->belongsTo('Replier', [
           'className' => 'Users',
           'foreignKey' => 'last_replier',
           'propertyName' => 'Last_replier'
        ]);
         $this->belongsTo('Project', [
             'className' => 'Projects',
             'foreignKey' => 'project_id',
             'propertyName' => 'Project'
        ]);*/

    }
    


/* public function validationDefault( Validator $validator)
    {
      $validator = new Validator();
        $validator
            ->add('to_id', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Please, Select To ID .'),
            ])
            ->add('from_id', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Please, Select From ID .')
            ])
             ->add('title', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Enter Ticket title .')
            ])
              ->add('description', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Enter Ticket description .')
            ])
               ->add('response', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Enter Ticket response .')
            ])
               ->add('ticket_id', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Enter Ticket id .')
            ])
             ->add('userID', 'notBlank', [
                'rule' => 'notBlank',
                'message' => __('Enter user ID .')
            ]);
          
         //    ->notEmpty('ticket_id', "Enter Ticket id.");
            
        return $validator;
    }*/
/*



class Ticket extends AppModel {

    var $name = 'Ticket';

    var $validate = array(
        'to_id' => array(

        'rule' => 'notEmpty',

        'message' => "Please, Select To ID ."

      ),
      'from_id' => array(

        'rule' => 'notEmpty',

        'message' => "Please, Select From ID ."

      ),
      'title' => array(

        'rule' => 'notEmpty',

        'message' => "Enter Ticket title ."

      ),
        'description' => array(

        'rule' => 'notEmpty',

        'message' => "Enter Ticket description ."

      ),
        'response' => array(

        'rule' => 'notEmpty',

        'message' => "Enter Ticket response ."

      )
    
    
    );
   var $assocs = array(

    'User_To' => array(

        'type' => 'belongsTo',

        'className' => 'User',

        'foreignKey' => 'to_id'

      ),
    'User_From' => array(
      'type' => 'belongsTo',

        'className' => 'User',

        'foreignKey' => 'from_id'
    ),
    'Last_replier' => array(
      'type' => 'belongsTo',
       'className' => 'User',
       'foreignKey' => 'last_replier'
    ),
    'Project' => array(
      'type' => 'belongsTo',
       'className' => 'Project',
       'foreignKey' => 'project_id'
    )

    );

  

 }


 */



}

 ;?>