<?php
/*
	* Billing Model class
	* PHP versions 5.3.5
	* @date 28-Dec-2011
	* @Purpose:This model handles all the validations regarding Billing management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12
*/
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class BillingsTable extends Table {
	  

	  public function initialize(array $config) {
        parent::initialize($config);
        $this->table('billings');
        $this->addBehavior('Timestamp');
        
   $this->belongsTo('Projects', [
		  'className' => 'Projects',
		  'foreignKey' =>'project_id',
		  'joinType' => 'INNER'
            
        ]);
    }


   /* var $name = 'Billing';
    var $validate = array(
			'project_id' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Project name."
			),
			'week_start' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Week start."
			),
			'max_limit' => array(
				'rule'    => array('comparison', '<=', 1000),
				'allowEmpty' => true,
				'message' => 'Max Limit can not be greater than 1000 hours.'
        	),
			'todo_hours' => array(
				'rule'    => array('comparison', '<=', 1000),
				'allowEmpty' => true,
				'message' => 'Max To do hours can not be greater than 1000 hours.'
			),
			'mon_hour' =>array(
				'rule1' => array(
				'rule'    => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
				),
				'rule2' => array(
					 'rule' => '/^[0-9]{1,2}(:[0-9]{2})?$/i',
					'message' => "Enter valid hours. e.g 12:30"
				)
           ),
            'tue_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
           ),
            'wed_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
           ),
            'thur_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
           ),
            'fri_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
           ),
            'sat_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'
           ),
            'sun_hour' =>array(
				'rule'  => array('comparison', '<=', 24),
				'allowEmpty' => true,
				'message' => 'Can not be greater than 24 hours.'       
				)   
    );
	
	var $assocs = array(
		 'Project' => array(
		'type' => 'belongsTo',
		 'className' => 'Project',
		'foreignKey' =>'project_id'
		)
	);
	
*/
}
;?>