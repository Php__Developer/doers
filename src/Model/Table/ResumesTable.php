<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class ResumesTable extends Table
{

	public function initialize(array $config) {
        parent::initialize($config);
        $this->table('resumes');
         $this->addBehavior('Timestamp');
	}

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('name', "Enter Name.")
    ->notEmpty('contact', "Enter Contact.");
     return $validator;
  }


	/*
	class Resume extends AppModel {
    var $name = 'Resume';
    var $validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Name."
			),
			'contact' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Contact ."
			)
    );
	*/




}