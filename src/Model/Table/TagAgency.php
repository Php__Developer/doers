<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class TagAgencyTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('tag_agency');
        
         $this->belongsTo('Agencyrecord', [
            'className' => 'Agency',
            'foreignKey' => 'agency_id',
            'propertyName' => 'Agencyrecord'
            'bindingKey' => 'id',
        ]);
        $this->belongsTo('Rolerecord', [
            'className' => 'Role',
            'foreignKey' => 'role_id',
            'bindingKey' => 'id',
            'propertyName' => 'Rolerecord'
        ]); 
  }
  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
 /* $validator
    ->notEmpty('name', "Enter Entity Name.");*/
     return $validator;
  }

}


 ;?>


