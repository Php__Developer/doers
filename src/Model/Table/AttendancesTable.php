<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class AttendancesTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);


        $this->table('attendances'); // if for some reason naming conventiopsn does not work.
                $this->addBehavior('Timestamp');
        $this->belongsTo('User_data', [
              'className' => 'Users',
              'foreignKey' => 'user_id',
              'propertyName' => 'User_data'
        ]);
      


    }

 public function validationDefault( Validator $validator)
    {
         $validator
    ->notEmpty('user_id', "Select user name.")
    ->notEmpty('add_date', "Select date.")
    ->notEmpty('leave_reason', "Enter genuine reason for leave.")
    ->add('hours', [
        'notempty' => [
            'rule' => 'notBlank',
            'message' => "Enter your working hours."
        ],
        'ruleName2' => [
            'rule' => array('custom', '/^[0-9]{1,2}(:[0-9]{2})?$/i'),
            'message' =>"Enter valid hours. e.g 09:30"
        ]
    ])


    ->notEmpty('hours', "Enter your working hours.")
    
    ->notEmpty('notes', "Enter tasks description.")
    ->notEmpty('time_in', "Enter In Time.")
    ->notEmpty('time_out', "SEnter Out Time.")
    ->notEmpty('Inhour', "Enter In Time.")
    ->notEmpty('Inmin', "Enter In Time.")
    ->notEmpty('Inmeridian', "Enter In Time.")
    ->notEmpty('Outhour', "Enter Out Time.")
    ->notEmpty('Outmin', "Enter Out Time.")
    ->notEmpty('Outmeridian', "Enter Out Time.");
     return $validator;
    }





function timeMatch($field = array(),$compare_field = null, $date_field = null) {

        foreach($field as $key => $value){
            $time_out = trim($value);
            $time_in = trim($this->data[$this->name][ $compare_field ]);
            $date_val = trim($this->data[$this->name][ $date_field ]);
            //echo strtotime($date_val.' '.$time_out) .'^%&&&&'. strtotime($date_val.' '.$time_in); die;
            if(strtotime($date_val.' '.$time_out) >= strtotime($date_val.' '.$time_in))
            return true;
            else
            return false;
        }
    }
    
    /*
    function getAttendanceReport($date = null){
      /*  $query = "SELECT U.id, U.first_name, U.last_name,A.time_in, A.time_out,A.hours, A.notes,A.date,A.status,A.hrnotes,A.verified
        FROM  users AS U
        Left JOIN  attendances AS A ON  U.id = A.user_id
        Where A.date =  '".$date."' and U.status ='1'

        UNION ALL

        Select id, first_name, last_name, '00:00:00', '00:00:00', '-','---',' ',' ',' ',' ' from users where id not in (SELECT distinct user_id from attendances where date =  '".$date."' )  and status ='1' order by first_name";
        $result = $this->query($query);
        //pr($result->toArray()); die;
        */



      //  return $result->toArray();


   // }

//

/*
  
    function getAttendanceReport($date = null){
        $query = "SELECT U.id, U.first_name, U.last_name,A.time_in, A.time_out,A.hours, A.notes,A.date,A.status,A.hrnotes,A.verified
        FROM  users AS U
        Left JOIN  attendances AS A ON  U.id = A.user_id
        Where A.date =  '".$date."' and U.status ='1'  

        UNION ALL

        Select id, first_name, last_name, '00:00:00', '00:00:00', '-','---',' ',' ',' ',' ' from users where id not in (SELECT distinct user_id from attendances where date =  '".$date."' )  and status ='1' order by first_name";
        $result = $this->query($query);
        return $result;
    }
  */


}

 ;?>