<?php

/**

 * Generic Model class

 */
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class GenericsTable extends Table {

   // var $name = 'Generic';
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('generics');
    }

	
}
;?>