<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class VlogicLabsUsersTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);
        $this->table('vlogiclabsusers');
         $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'employee_code'
            ]);
        $this->belongsTo('Supervisor', [
            'className' => 'Users',
            'foreignKey' => 'employee_code'
            ]);
        $this->addBehavior('Timestamp');


         $this->belongsTo('Credentials', [
            'className' => 'credentials',
            'foreignKey' => 'id'
            ]);
          /*
         $this->hasOne('Appraisals', [
            'className' => 'Appraisals',
            'foreignKey' => 'user_id'
            ]);
        */
         /* $this->hasOne('Appraisals', [
            'className' => 'Appraisals',
            'foreignKey' => 'user_id'
         ]);*/
          $this->hasOne('Appraisals', [
             'className' => 'Appraisals',
              'foreignKey' =>'user_id',
              'propertyName' => 'Appraisals' // balde contain
            
        ]);
    }
    


 

    

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

$validator
    ->notEmpty('employee_code', "Enter Employee Code.")
    ->notEmpty('first_name', "Enter first name.")
    ->notEmpty('last_name', "Enter last name.")
    ->notEmpty('father_name', "Enter father name.")
    ->allowEmpty('phone')
    ->add('phone', 'reg', [
        'rule' => ['phone', '/^\s*[0-9\-\+\s]+$/i', 'us'],
        'message' => 'nter valid phone number.'
    ])
    ->notEmpty('password', "Enter your password.")
    ->notEmpty('full_address', "Enter address.")
    ->notEmpty('personal_email', "Enter Personal email.")
    ->notEmpty('user_name', "Enter your username or email.")
    ->notEmpty('username', "Enter your username or email.")
    ->add('username', [
        'notempty' => [
            'rule' => 'notEmpty',
            'last' => true,
            'message' => "Enter your username."
        ],
        'isUnique' => [
            'rule' => 'isUnique',
            'message' => "Username already exists."
        ]
    ])
    ->add('email', [
        'notempty' => [
            'rule' => 'notEmpty',
            'last' => true,
            'message' => "Enter your email."
        ],
        'isUnique' => [
            'rule' => 'isUnique',
            'message' =>"Email address already exists."
        ]
    ])
    ->add('old_password', [
        'notempty' => [
            'rule' => 'notEmpty',
            'last' => true,
            'message' => "Enter old password.",
        ],
        'ruleName2' => [
            'rule' => array('isOldPasswordExists'),
            'message' =>"Old password does not exists."
        ]
    ])
    ->add('confirm_password', [
        'notempty' => [
            'rule' => 'notEmpty',
            'message' =>"Enter confirm password."
        ],
        'ruleName2' => [
            'rule' => array('matchPasswords','password'),
            'message' => "Password and confirm password must be same."
        ]
    ])
    ->add('dob', 'reg', [
        'rule' => array('validDOB'),
        'message' => "Date of birth should not be greater than today's date."
    ])
    ->add('summary', 'notempty', [
        'rule' =>'notEmpty',
        'message' => "Enter description."
    ])
     ->add('url', 'notempty', [
        'rule' =>array('url'),
        'message' => "Enter valid url of website.",
        'allowEmpty' => true,
    ]);
       
        return $validator;
    }








//  var $validate = array(         //   'employee_code' => array(               'rule' => 'notEmpty',               'message' => "Enter Employee Code."         ),
          



/*
 
 public function validationDefault( Validator $validator)
    {
        $validator
            ->add('user_name', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('You need to provide a user_name'),
            ])
            ->add('password', 'notEmpty', [
                'rule' => 'notEmpty',
                'message' => __('A Password is required')
            ]);
        return $validator;
    }
*/
    function matchPasswords($field = array(),$compare_field = null) {
        foreach($field as $key => $value){
        $v1 = trim($value);
        $v2 = trim($this->data[$this->name][ $compare_field ]);
        if($v1 != "" && $v2 !="" && $v1 != $v2){
            return false; 
        }
         return true;
    }
   }

    function isOldPasswordExists($field = array()) {
    // Import Session Component
        //App::import('Component', 'SessionComponent');
        $session = $this->request->session();

        
        $userSession = $session->read("SESSION_ADMIN");

        foreach( $field as $key => $value ){
            $v1 = md5(trim($value));
            $result = $this->find('first', array('conditions' => array('id' => $userSession[0], 'password'=>$v1),'fields'=>array('id')));
            if(!is_array($result)){
                return false; 
             }
             return true;
        }
   }


    function validDOB($field = array()) {
        foreach($field as $key => $value){
        $v1 = trim($value);
        if($v1 != "" && $v1 > date('Y-m-d')){
            return false; 
        }
         return true;
    }
   }


}

 ;?>