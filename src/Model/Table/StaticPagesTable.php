<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class StaticPagesTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('static_pages'); // if for some reason naming conventiopsn does not work.
    }

 public function validationDefault( Validator $validator)
    {
        $validator
            ->add('title', 'notEmpty', [
                'rule' => 'notBlank',
                'message' => __('Enter page title'),
            ])
            ->add('category', 'notEmpty', [
                'rule' => 'notBlank',
                'message' => __('Select a category.')
            ])
            ->add('content', 'notEmpty', [
                'rule' => 'notBlank',
                'message' => __('Enter necessary content here.')
            ]);
        return $validator;
    }

/*
   var $name = 'StaticPage';
    var $validate = array(
        'title' => array(
        'rule' => 'notEmpty',
        'message' => "Enter page title."
        ),
    'category'=>array(
        'rule' => 'notEmpty',
        'message' => "Select a category."
    ),
    'content' => array(
        'rule' => 'notEmpty',
        'message' => "Enter necessary content here."
    )


*/


}

 ;?>