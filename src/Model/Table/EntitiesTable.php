<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class EntitiesTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('entities');
          $this->hasMany('Entityagencies', [
            'className' => 'EntityAgency',
            'foreignKey' => 'entity_id',
            'propertyName' => 'Entityagencies'
        ]);
      
        /* $this->hasMany('Agencies', [
            'className' => 'EntityAgency',
            'foreignKey' => 'age_id',
            'propertyName' => 'Roles'
        ]);
*/
          /*$this->belongsTo('Entitytable', [
            'className' => 'Agency',
            'foreignKey' => 'agency_id',
            'propertyName' => 'Entitytable'
        ]);*/ 
  }
  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('name', "Enter Entity Name.")
    ->add(
        'name', 
        ['unique' => [
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'Entity name already exists.',
            'on' => 'create'
            ]
        ]
    )
    ->notEmpty('agency_id', "Selete Agency.") 
    ->notEmpty('role_id', "Select Roles.")
    ->notEmpty('alias', "Enter Alias.");
    
     return $validator;
  }

}


 ;?>


