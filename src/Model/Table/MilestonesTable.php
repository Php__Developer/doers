<?php
/*
	* Billing Model class
	* PHP versions 5.3.5
	* @date 28-Dec-2011
	* @Purpose:This model handles all the validations regarding Billing management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12
*/
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class MilestonesTable extends Table {
	  

	  public function initialize(array $config) {
        parent::initialize($config);
        $this->table('milestones');
        $this->addBehavior('Timestamp');
        
   $this->belongsTo('Projects', [
		  'className' => 'Projects',
		  'foreignKey' =>'project_id',
		  
            
        ]);
    $this->belongsTo('Users', [
		  'className' => 'Users',
		  'foreignKey' =>'user_id',
        ]);
    }


   
}
;?>