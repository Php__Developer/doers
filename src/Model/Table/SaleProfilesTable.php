<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class SaleProfilesTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('sale_profiles'); 
          $this->addBehavior('Timestamp');

      $this->hasOne('Projects', [
            'className' => 'projects',
            'foreignKey' => 'profile_id',
            'propertyName' => 'Projects'
        ]);

      $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
            'propertyName' => 'Users'
        ]);

       $this->hasOne('CredentialLogs', [
            'className' => 'CredentialLogs',
            'foreignKey' => 'credential_id',
            'propertyName' => 'CredentialLogs'
        ]);

  }
  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('username', "Enter User Name.")
    ->notEmpty('password', "Enter Password.")
    ->notEmpty('nameonprofile', "Enter Profile Name.")
    ->notEmpty('email', "Enter Email.");
  /*  ->notEmpty('phonenoattached', "Enter Phone Number.");*/
     return $validator;
  }

}


 ?>