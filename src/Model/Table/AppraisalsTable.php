<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class AppraisalsTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('appraisals');


       $this->belongsTo('Users', [
             'className' => 'Users',
            'foreignKey' =>'user_id',
             'propertyName' => 'Users' // balde contain
            
        ]);

$this->belongsTo('Supervisor', [
             'className' => 'Users',
            'foreignKey' =>'supervisor_id',
            'bindingKey' => 'id',
             'propertyName' => 'Supervisor'
            
        ]);
        

        $this->addBehavior('Timestamp');
    }




public function validationDefault(Validator $validator)
    {
        $validator = new Validator();

    $validator
    ->requirePresence('user_id')
    ->notEmpty('user_id', 'Please Select Employee.')
    ->requirePresence('supervisor_id')
    ->notEmpty('supervisor_id', 'Please Select Supervisor')
    ->requirePresence('appraised_amount')
    ->notEmpty('appraised_amount', 'Enter Appraised Amount')
  ->requirePresence('evaluation_to1')
    ->notEmpty('evaluation_to1', 'Please Evaluation To')

  ->requirePresence('technical_comment')
    ->notEmpty('technical_comment', 'Enter Technical Comment')

      ->requirePresence('communication_comment')
    ->notEmpty('communication_comment', 'Enter Communication Comment')

      ->requirePresence('interpersonal_comment')
    ->notEmpty('interpersonal_comment', 'Enter Interpersonal Comment')


      ->requirePresence('supervisor_comment')
    ->notEmpty('supervisor_comment', 'Enter Supervisor Comment')


  ->requirePresence('next_review_date1')
    ->notEmpty('next_review_date1', 'Select Next Review Date')



      ->requirePresence('effective_from1')
    ->notEmpty('effective_from1', 'Enter Effective From')


      ->requirePresence('appraisal_date1')
    ->notEmpty('appraisal_date1', 'Enter Appraisal Date')



      ->requirePresence('new_designaiton')
    ->notEmpty('new_designaiton', 'Select New Designation');




       return $validator;

  }








 }