<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class RememberTokensTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('remember_tokens');
    }
    

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
 	 // /$validator
    //->notEmpty('role', "Enter Role.")
    //->notEmpty('description', "Enter Description.");
     return $validator;
  }
  }?>