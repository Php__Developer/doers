<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ContactsTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);

        $this->table('contacts'); // if for some reason naming conventiopsn does not work.



/*class Contact extends AppModel {
    var $name = 'Contact';
    var $validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Name."
			),
			'primaryemail' => array(
				'ruleName'=>array(
				'rule' => array('email'),
				'allowEmpty' => true,
				'message' => "Enter valid email."
			)),
			'primaryphone' => array(
			'ruleName2'=>array(
				'rule' => 'Numeric',
				'allowEmpty' => true,
				'message' => "Enter Valid  phone number."	
			)
			),
			'cpassword' => array(		
					'rule' => array('passwordExits','name'),
					'message' => "Enter unique client password.",
			),
			'accesspass' => array(		
					'rule' => 'notEmpty',
					'message' => "Enter your access code.",
			),
			'email' => array(
				'ruleName2' => array(
				'rule' => array('email'),
				'allowEmpty' => true,
				'message' => "Enter valid email address."
				),
				'isUnique' => array(
				'rule' => 'isUnique',
				'message' => "Email address already exists."
				)
            ),
    );
	
	  function passwordExits($field = array(),$compare_field = null) {
		    $name = trim($this->data['Contact'][ $compare_field ]); 
			foreach($field as $key => $value){
				$cpass = $this->find('first', array('conditions'=>array('cpassword'=>Sanitize::escape($value),'name !='=>$name)));
				if(!is_array($cpass)){
					return true; 
				}
				else
				{
					return false;
				}
			}

	   }*/
}



public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
 	 $validator
    ->notEmpty('name', "Enter Name.")
   /* ->notEmpty('primaryemail', "Enter Primary Email.")*/
    //->notEmpty('primaryphone', "Enter Primary Phone.")
   	 ->notEmpty('cpassword', "Enter Password.");/*
     ->notEmpty('source', "Enter Source.");*/
   /* ->notEmpty('skype', "Enter Skype id.");*/
     return $validator;
  }
}
?>