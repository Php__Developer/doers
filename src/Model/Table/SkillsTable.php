<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class SkillsTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('skills'); 
  }
  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('skills', "Enter skills.");
     return $validator;
  }

}


 ;?>