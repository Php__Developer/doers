<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class ProjectsTable extends Table
{

public function initialize(array $config) {
        parent::initialize($config);

        $this->table('projects');
$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600); 
//pr($currentWeekMonday);
$this->addBehavior('Timestamp');
$this->belongsTo('Contact', [
      'className' => 'Contacts',
      'foreignKey' =>'client_id',
      'propertyName' => 'Contact'
     ]);






$this->belongsTo('Engineer_User', [
      'className' => 'Users',
      'foreignKey' =>'engineeringuser',
       'propertyName' => 'Engineer_User'
     ]); 

	$this->belongsTo('Salefront_User', [
      'className' => 'Users',
      'foreignKey' =>'salesfrontuser',
       'propertyName' => 'Salefront_User'
      ]); 

	 $this->belongsTo('Saleback_User', [
      'className' => 'Users',
      'foreignKey' =>'salesbackenduser',
      'propertyName' => 'Saleback_User'
     ]); 

	 $this->belongsTo('Last_Updatedby', [
      'className' => 'Users',
      'foreignKey' =>'last_id',
      'propertyName' => 'Last_Updatedby'
     ]); 

	 $this->belongsTo('ProjectManager', [
      'className' => 'Users',
      'foreignKey' =>'pm_id',
      'propertyName' => 'ProjectManager'
     ]); 

   $this->hasMany('Payments', [
            'className' => 'Payments',
            'foreignKey' => 'project_id',
            'propertyName' => 'Payments'
        ]);
     $this->hasMany('Billings', [
            'className' => 'Billings',
            'foreignKey' => 'project_id',
             'conditions' => ['Billings.week_start <=' => $currentWeekMonday, 'Billings.verified'=>0],
            'propertyName' => 'Billings'
        ]);
    
    $this->belongsTo('Users', [
      'className' => 'Users',
      'foreignKey' =>'user_id',
      'propertyName' => 'Users'
     ]); 
    $this->belongsTo('SaleProfiles', [
      'className' => 'SaleProfiles',
      'foreignKey' =>'id',
      'propertyName' => 'SaleProfiles'
     ]); 

        }

  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('project_name', "Enter Project Name.")
    ->notEmpty('client_id', "Enter Client Name.")
    ->notEmpty('pm_id', "Enter Responsible.")
    ->notEmpty('profile_id', "Enter Sale Profile.")
    ->notEmpty('engagagment', "Enter Engagagment.")
    ->notEmpty('platform', "Enter Platform.")
    ->notEmpty('payment', "Enter Payment.")
    ->notEmpty('technology', "Enter Technology.")
    ->notEmpty('engineeringuser', "Enter Accountable.")
    ->notEmpty('salesfrontuser', "Enter Consultant.")
    ->notEmpty('salesbackenduser', "Enter Informer.")
   // ->notEmpty('team_id', "Please select Team.")
    ->notEmpty('totalamount', "Enter Total Amount.")
    ->notEmpty('paid', "Enter Paid Amount.")
    ->notEmpty('left', "Enter Left Amount.")
    ->notEmpty('nextpayment', "Enter Next Payment.")
   // ->notEmpty('nextrelease', "Enter Next Release.")
    //->notEmpty('finishdate', "Enter Last Release.")
    //->notEmpty('credentials', "Enter Credentials.")
    ->notEmpty('status', "Enter Status.");
     return $validator;
  }



    }

 ;?>