<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class CredentialLogsTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);
        $this->table('credential_logs');
             $this->addBehavior('Timestamp');

 $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'id'
        	]);

 

$this->belongsTo('Credentials', [
            'className' => 'Credentials',
            'foreignKey' => 'credential_id',
            'propertyName' => 'Credentials'
        	]);

    }

}
