<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class HardwaresTable extends Table
{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('hardwares');
    }

public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('item', "Enter item.")
    ->notEmpty('description', "Enter description.")
    ->notEmpty('alloted_to', "Select Email.")
    ->notEmpty('unique_id', "Enter unique id.")
    ->notEmpty('current_location', "Enter current location.")
    ->notEmpty('others', "Enter others.")
    ->notEmpty('purchase_price', "Enter  purchase price.")
    ->notEmpty('current_price', "Enter current price.")
     ->notEmpty('legends', "Enter  legends.");
     return $validator;
  }






}
