<?php
/*
	* Project Model class
	* PHP versions 5.3.5
	* @date 20-Aug-2011
	* @Purpose:This model handles all the validations regarding project management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12
*/

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class PaymentsTable extends Table {
	  public function initialize(array $config) {
        parent::initialize($config);
        $this->table('payments');
       // $this->addBehavior('Timestamp');
        
$this->belongsTo('Projects', [
		  'className' => 'Projects',
		  'foreignKey' =>'project_id',
		  'joinType' => 'INNER',
		  'propertyName' => 'Projects'
            
        ]);


$this->belongsTo('Users', [
      'className' => 'Users',
      'foreignKey' =>'responsible_id',
      'propertyName' => 'Users'
     ]); 

    }

  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
  ->notEmpty('project_id', "Enter Project Name.")
  ->notEmpty('responsible_id', "Enter Resource.")
  ->notEmpty('branch', "Enter branch.")
    ->notEmpty('payment_date', "Enter Payment Date.")
    ->notEmpty('currency', "Enter Currency.")
    ->notEmpty('amount', "Enter amount.")
    ->notEmpty('tds_amount', "Enter tds amount .")
    ->notEmpty('note', "Enter notes.");
     return $validator;
  } 
}




;?>