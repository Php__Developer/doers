<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class TagsTable extends Table
{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('tags');

$this->addBehavior('Timestamp');
    }


public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('name', "Enter Tag Name.")
    ->add(
        'name', 
        ['unique' => [
            'rule' => 'validateUnique', 
            'provider' => 'table', 
            'message' => 'Tag name already exists.',
            'on' => 'create'
            ]
        ]
    )
    ->notEmpty('agency_id', "Selete Agency.") 
    ->notEmpty('role_id', "Select Roles.")
    ->notEmpty('alias', "Enter Alias.");
    
     return $validator;
  }




  
}

 ;?>