<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class ProfileauditsTable extends Table{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('profileaudits');

              $this->addBehavior('Timestamp');  

		$this->belongsTo('Users', [
				  'className' => 'Users',
				  'joinType' => 'INNER',
				  'propertyName' => 'Users',
		            'foreignKey' =>'created_by',
		        ]);
		    }

 }

