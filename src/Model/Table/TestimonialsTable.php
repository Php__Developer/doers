<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class TestimonialsTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);
$this->addBehavior('Timestamp');
        $this->table('testimonials'); // if for some reason naming conventiopsn does not work.
   
$this->belongsTo('Clients', [
             'className' => 'Clients',
            'foreignKey' =>'client_id',
            'joinType' => 'INNER'
            
        ]);


    }


public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('author', "Enter Author.")
    ->notEmpty('keyword', "Enter Keyword.")
    ->notEmpty('description', "Enter description.")
    ->notEmpty('Testimonial.value1', "Enter Search Value.");
     return $validator;
  }



/*class Testimonial extends AppModel {
   var $name = 'Testimonial';
    var $validate = array(
			'author' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Author Name."
			),
			'description' => array(
				'rule' => 'notEmpty',
				'message' => "Enter description."
			),
			'client_id' => array(
				'rule' => 'notEmpty',
				'message' => "Enter client id."
			),
			'keyword' => array(
				'rule' => 'notEmpty',
				'message' => "Enter keyword."
						),
			
    
			
			'email_address' => array(
				'ruleName' => array(
				'rule' => 'notEmpty',
				'message' => "Enter email address(s).",
				'last' => true
				),
				'ruleName2' => array(
				'rule' => array('verifyEmails'),
				'message' => 'Some of the email addresses is not valid.'
				)
            )
    );
	var $assocs = array(
     'Client' => array(
      'type' => 'belongsTo',
      'className' => 'Client',
      'foreignKey' =>'client_id'
     ),	 
);
	
	/**

    * @Date: 25-April-2012

    * @Method : verifyEmails

    * @Purpose: Validate Comma seperated email address

    * @Param: $field

    * @Return: boolean

    **/

	//function verifyEmails($field = array()) {

     //   foreach($field as $key => $value){

     //   $v1 = explode(",",$value);
	//	foreach($v1 as $value){
    //   if($v1 != "" && !eregi("^[\'+\\./0-9A-Z^_\`a-z{|}~\-]+@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+){1,3}$",trim($value))){
		//	return false;
        // }
        // return true;
		//}
		//}
	//}  

	
	
}

 ;?>