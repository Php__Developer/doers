<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class EntityAgencyTable extends Table
{

  public function initialize(array $config) {
          parent::initialize($config);
          $this->table('entity_agency');
        
         $this->belongsTo('Agency', [
            //'className' => 'Agency',
            'joinTable' => 'agencies',
            'foreignKey' => 'agency_id',
            'bindingKey' => 'id'
        ]);
        $this->belongsTo('Role', [
            'joinTable' => 'roles',
            'foreignKey' => 'role_id'
        ]); 
  }
  public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
 /* $validator
    ->notEmpty('name', "Enter Entity Name.");*/
     return $validator;
  }

}


 ;?>


