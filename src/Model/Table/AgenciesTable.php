<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class AgenciesTable extends Table
{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('agencies');

$this->addBehavior('Timestamp');
    }


 public function validationDefault( Validator $validator)
    {
        




     $validator
            ->notEmpty('agency_name')
            ->requirePresence('agency_name')
            ->add('agency_name', [
            'length' => [
            'rule' => ['minLength', 4],
            'message' => 'Agency Name need to be at least 4 characters long',
             ],
              'unique' => [
                        'rule' => 'validateUnique', 
                        'provider' => 'table', 
                        'message' => 'this Agency name  is  already used']
                    ]
           )

             ->add('agency_name', 'validFormat',[
                        'rule' => array('custom', '/^[a-zA-Z0-9]*$/'),
                        'message' => 'Only letters ,Numeric,or alphaNumerics. Please enter a valid name'])
            



   
           ->notEmpty('first_name')
            ->requirePresence('first_name')


        ->add('first_name', 'validFormat',[
                        'rule' => array('custom', '/^[a-zA-Z ]*$/'),
                        'message' => 'Only letters or alphaNumerics. Please enter a valid name'])
            

             ->notEmpty('last_name')
            ->requirePresence('last_name')
               ->add('last_name', 'validFormat',[
                        'rule' => array('custom', '/^[a-zA-Z ]*$/'),
                        'message' => 'Only letters or alphaNumerics. Please enter a valid name'])
            

             ->notEmpty('username', 'Email field cannot be left blank .')
            ->requirePresence('username')
            ->add('username', 'validFormat', [
            'rule' => 'email',
            'message' => 'Email must be valid'
            ])
              ->add('username', 'validFormat', [
                       'rule' => 'email',
                        'message' => 'E-mail must be valid',
               'unique' => [ 'rule' => 'validateUnique', 
                      'provider' => 'table', 
                      'message' => 'Email already exists.',
                      'on' => 'create'
                      ]  ])

             ->notEmpty('password')
            ->requirePresence('password')
        ->add('password', 'validFormat',[
                        'rule' => array('custom', '$\S*(?=\S{5,})(?=\S*[a-zA-Z])(?=\S*[\d])(?=\S*[\W])S*$'),
                        'message' => 'Password Should Be Combination Of Number,Letters & Symbols. '])
            

            



           //->notEmpty('confirm_password')
           //->requirePresence('confirm_password')
         /*   ->add('confirm_password', [
            'compare' => [
            'rule' => ['compareWith', 'password']
            ]
            ])*/;
        return $validator;



















    }





  
}

 ;?>