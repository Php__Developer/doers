<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
class PaymentMethodsTable extends Table {
    public function initialize(array $config) {
        parent::initialize($config);
        $this->table('payment_methods');
         $this->addBehavior('Timestamp');
    }
    var $name = 'PaymentMethod';
	var $validate =  array(
        'name' => array(
        'nameRule-1' => array(
            'rule' => 'notEmpty',
            'message' => 'Please enter a name',
            'last' => true
         ),
        'nameRule-2' => array(
            'rule' => array('custom', '|^[a-zA-Z]*$|'),
            'message' => 'Please enter a valid name'
        )
    ),
        'type' => array(
        'rule' => 'notEmpty',
        'message' => 'Please Choose a Payment Method'
    )		
    );


public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('name', "Enter Name.")
      ->notEmpty('type', "Select Payment Method.");
     return $validator;
  }





}
?>
 