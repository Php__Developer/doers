<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class EvaluationsTable extends Table
{

     public function initialize(array $config) {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
        $this->table('evaluations');
              //$this->addBehavior('Timestamp');
    

    // validations are needed to be added

/*var $assocs = array(
        'User' => array(
            'type' => 'belongsTo',
             'className' => 'User',
            'foreignKey' =>'user_id'
        ),
        'Senior' => array(
            'type' => 'belongsTo',
             'className' => 'User',
            'foreignKey' =>'senior_id'
        )
    );
*/

$this->belongsTo('Users', [
             'className' => 'Users',
            'foreignKey' =>'user_id',
             'propertyName' => 'Users'
            
        ]);

$this->belongsTo('Seniors', [
             'className' => 'Users',
            'foreignKey' =>'senior_id',
            'bindingKey' => 'id',
             'propertyName' => 'Seniors'
            
        ]);
}
public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
   ->notEmpty('user_id', "Select User.")
    ->notEmpty('senior_id', "Select Senior.")
   /* ->notEmpty('user_comment', "Enter User Comment.")
    ->notEmpty('senior_comment', "Enter Senior comment.")*/
     ->notEmpty('month', "Select Month.")
    ->notEmpty('year', "Select Year.");
     return $validator;
  }
}
?>