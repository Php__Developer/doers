<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class CredentialsTable extends Table
{

	 public function initialize(array $config) {
        parent::initialize($config);
        $this->table('credentials');
        $this->addBehavior('Timestamp');
        $this->hasMany('CredentialsLogs', [
        	'className' => 'CredentialLogs',
            'foreignKey' => 'credential_id'
        ]);



         $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        	]);

    }

    public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
  $validator
    ->notEmpty('username', "Enter User Name.")
    ->notEmpty('password', "Enter Password.")
    ->notEmpty('forworder_email', "Enter Forworder Email ID.");
     return $validator;
  }

}
