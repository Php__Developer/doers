<?php 
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


class LeadsTable extends Table
{
     public function initialize(array $config) {
        parent::initialize($config);
        $this->table('leads');

$this->belongsTo('Users', [
		  'className' => 'Users',
		  'foreignKey' =>'salesfront_id',
		  'joinType' => 'INNER',
		  'propertyName' => 'Users'
            
        ]);
$this->addBehavior('Timestamp');
    }


 public function validationDefault( Validator $validator)
    {
        $validator
        	 ->notEmpty('bidding_date', 'Enter bidding date.')
           	->notEmpty('url_title', "Enter url title.")
             ->add('perspective_amount', 'notBlank', [
                'rule' =>  array('custom','/^(?:\d+|\d*\.\d+)$/'),
                'allowEmpty' => true,
                'message' => 'Enter valid perspective amount.'
            ])
			->add('time_spent', [
        'notempty' => [
            'rule' => 'notBlank',
            'message' => "Enter your working hours"
        ],
        'ruleName2' => [
            'rule' => array('custom', '/^[0-9]{1,2}(:[0-9]{2})?$/i'),
            'message' =>"Enter valid hours. e.g 09:30"
        ]
    ])
            ;
        return $validator;
    }





/*
	* Lead Model class
	* PHP versions 5.3.5
	* @date 28-Dec-2011
	* @Purpose:This model handles all the validations regarding lead management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12
*/

/*class Lead extends AppModel {
    var $name = 'Lead';
    var $validate = array(
			'bidding_date' => array(
				'rule' => 'notEmpty',
				'message' => "Enter bidding date."
			),
			'url_title' => array(
				'rule' => 'notEmpty',
				'message' => "Enter url title."
			),  'rule' => '/^[0-9]{1,2}(:[0-9]{2})?$/i',
			'perspective_amount' => array(
				'ruleName2' => array(
					 'rule' => '/^(?:\d+|\d*\.\d+)$/',
					 'allowEmpty' => true,
					 'message' => "Enter valid perspective amount. "
					)
			),
			'time_spent' => array(
				'ruleName2' => array(
					 'rule' =>'/^[0-9]{1,2}(:[0-9]{2})?$/i',
					 'allowEmpty' => true,
					 'message' => "Enter valid time spent. "
					)
			)
    );
	
	var $assocs = array(
		 'User' => array(
		  'type' => 'belongsTo',
		  'className' => 'User',
		  'foreignKey' =>'salesfront_id'
		 ) 	 
	);
*/	
  
}

 ;?>