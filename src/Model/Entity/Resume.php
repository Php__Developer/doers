<?php 
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
class Resume extends Entity
{

}


/*
	* Resume Model class
	* PHP versions 5.3.5
	* @date 28-Dec-2011
	* @Purpose:This model handles all the validations regarding Resume management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12


class Resume extends AppModel {
    var $name = 'Resume';
    var $validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Name."
			),
			'contact' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Contact ."
			)
    );
	
	
  
}
*/
?>