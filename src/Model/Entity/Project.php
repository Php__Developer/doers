<?php 
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Project extends Entity
{
    //var $name = 'Project';
    var $validate = array(
			'project_name' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Project name."
			),
			'client_id' => array(
				'rule' => 'notEmpty',
				'message' => "Select client ."
			),
			'engineeringuser' => array(
				'rule' => 'notEmpty',
				'message' => "Select Engineering User."
			),
                        'salesfrontuser' => array(
				'rule' => 'notEmpty',
				'message' => "Select Sales front-user."
			),
			'totalamount' => array(
			'ruleName' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Total Amount.",
				 'last' => true
			),
			'ruleName2' => array(
				 'rule' => '/^-?(?:\d+|\d*\.\d+)$/',
				'message' => "Enter valid amount. "
				)
			)	
    );
	/*var $assocs = array(
     'Contact' => array(
      'type' => 'belongsTo',
      'className' => 'Contact',
      'foreignKey' =>'client_id'
     ),	
	 'Engineer_User' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'engineeringuser'
     ),	 
	 'Salefront_User' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'salesfrontuser'
     ),
	  'Saleback_User' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'salesbackenduser'
     ),
	 'Last_Updatedby' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'last_id'
     ),
	  'ProjectManager' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'pm_id'
     )
);
	
 var $hasMany = array('Billing');	
*/
}
?>