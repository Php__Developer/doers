<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class LearningCenter extends Entity {
   // var $name = 'LearningCenter';
    var $validate = array(
			'skill' => array(
				'rule' => 'notEmpty',
				'message' => "Select skill."
			),
			'title' => array(
				'rule' => 'notEmpty',
				'message' => "Enter title."
			),
			'content' => array(
				'rule' => 'notEmpty',
				'message' => "Enter content."
			)
	);
	// var $assocs = array(

		//'User' => array(

			  //'type' => 'belongsTo',

			//  'className' => 'User',
//
			 // 'foreignKey' => 'user_id'

      //)

    //);
}
?>

