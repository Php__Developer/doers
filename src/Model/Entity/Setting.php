<?php 
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
class Setting extends Entity
{
	public function validationDefault(Validator $validator)
    {
      
 $validator = new Validator();
   $validator
    ->requirePresence('email')
    ->add('email', 'validFormat', [
        'rule' => 'email',
        'message' => 'E-mail must be valid'
    ])
    ->requirePresence('password')
    ->notEmpty('name', 'Enter Password.');

$errors = $validator->errors($this->request->data());
if (empty($errors)) {
    // Send an email.
}
}
}

;?>