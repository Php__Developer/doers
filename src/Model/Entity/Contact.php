<?php
/*
	* Contact Model class
	* PHP versions 5.3.5
	* @date 28-Dec-2011
	* @Purpose:This model handles all the validations regarding contact management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12
*/
 
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Contact extends Entity
{



/*class Contact extends AppModel {
    var $name = 'Contact';
    var $validate = array(
			'name' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Name."
			),
			'primaryemail' => array(
				'ruleName'=>array(
				'rule' => array('email'),
				'allowEmpty' => true,
				'message' => "Enter valid email."
			)),
			'primaryphone' => array(
			'ruleName2'=>array(
				'rule' => 'Numeric',
				'allowEmpty' => true,
				'message' => "Enter Valid  phone number."	
			)
			),
			'cpassword' => array(		
					'rule' => array('passwordExits','name'),
					'message' => "Enter unique client password.",
			),
			'accesspass' => array(		
					'rule' => 'notEmpty',
					'message' => "Enter your access code.",
			),
			'email' => array(
				'ruleName2' => array(
				'rule' => array('email'),
				'allowEmpty' => true,
				'message' => "Enter valid email address."
				),
				'isUnique' => array(
				'rule' => 'isUnique',
				'message' => "Email address already exists."
				)
            ),
    );
	
	  function passwordExits($field = array(),$compare_field = null) {
		    $name = trim($this->data['Contact'][ $compare_field ]); 
			foreach($field as $key => $value){
				$cpass = $this->find('first', array('conditions'=>array('cpassword'=>Sanitize::escape($value),'name !='=>$name)));
				if(!is_array($cpass)){
					return true; 
				}
				else
				{
					return false;
				}
			}

	   }*/
}
?>