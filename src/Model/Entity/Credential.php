<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Credential extends Entity
{



}




/*
	* Credential Model class
	* PHP versions 5.3.5
	* @date 20-Aug-2011
	* @Purpose:This model handles all the validations regarding credential management.
	* @filesource
	* @author  VLL Solutions
	* @revision
	* @version 1.3.12


class Credential extends AppModel {
    var $name = 'Credential';
    var $validate = array(
			'password' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Password."
			),
			'type' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Type."
			),
			'keyword' => array(
				'rule' => 'notEmpty',
				'message' => "Enter Keyword."
			),
			'forworder_email' => array(
				'rule' => 'email',
				'message' => "Enter correct Email Id."
			),
			'username' => array(
				'ruleName' => array(
				'rule' => 'notEmpty',
				'message' => "Enter User Name.",
				'last' => true
				)
            )
    );
	

	function verifyUniqueUsername($field = array(),$compare_field = null,$compare_field1 = null) {
		$type = trim($this->data['Credential'][ $compare_field]);  
		$user = trim($this->data['Credential'][ $compare_field1]);  
       foreach($field as $key => $value){
				$res = $this->find('first', array('conditions'=>array('username'=>Sanitize::escape($value),'type'=>$type,'user_id !='=>$user)));
				//pr($res); die;
				if(!is_array($res)){
					return true; 
				}
				else
				{
					return false;
				}
			}
	}
	
	var $assocs = array(	
	 'User' => array(
      'type' => 'belongsTo',
      'className' => 'User',
      'foreignKey' =>'user_id'
     )
	
	);
	
}
*/
?>