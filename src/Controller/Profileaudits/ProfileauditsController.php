<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller\Profileaudits;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use App\Controller\Component\CommonComponent;


class ProfileauditsController extends AppController {

    var $name = "Profileaudits";    // Controller Name    
  
    /**
     * @Date: 28-Dec-2011    
     * @Method : beforeFilter    
     * @Purpose: This function is called before any other function.    
     * @Param: none   
     * @Return: none    
     * */
    function beforeFilter(Event $event) {
    // star to check permission
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
 
    // end code to check permissions
                parent::beforeFilter($event);
                $this->viewBuilder()->layout('layout_admin');
                $this->set('common', $this->Common);
                
                Router::parseNamedParams($this->request);
                    $session = $this->request->session();
                    $userSession = $session->read("SESSION_ADMIN");
                    if($userSession==''){
                    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
                }
            }

       public function initialize()
         {
        parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); 
        $this->loadModel('Evaluations'); 
        $this->loadModel('Users');
        $this->loadModel('Sales');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('ProfilesAudit');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
    }




    #_________________________________________________________________________#   
    /**
     * @Date: 26-Aug-2014       
     * @Method : index         
     * @Purpose: This function is the default function of the controller  
     * @Param: none                                     
     * @Return: none                                          
     * */

    function index() {
        $this->render('login');
        if ($this->Session->read("SESSION_USER") != "") {
            $this->redirect('dashboard');
        }
    }

    #_________________________________________________________________________#
    /** 	
     * @Date: 18-Nov-2014	
     * @Method : admin_auditdetails
     * @Purpose: This function is to view sale Profile from admin section.	
     * @Param: $id	
     * @Return: none 	
     * @Return: none 
     * */

    function auditdetails($id = null) {
    
        $this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $this->set('session',$session);
        $session_info = $session->read("SESSION_ADMIN");
        $resolveby = $session_info[0];
         
        $this->set("audit_id", $id);
        $id = $id;
      
        $credentials= TableRegistry::get('Users');
        $userData = $credentials->find("all");
        $this->set("userData", $userData);

        $res = $this->Profileaudits->find("all" , [
                  'conditions' => ['Profileaudits.profile_id' => $id]
                 // 'fields' => ['todo_hours','project_id'],
                 //  'groupField' => 'project_id'
               ])->all();

        $this->set("audit_list", $res);    

        if ($this->request->is('ajax')) { 
            $this->request->data['profile_id'] =  $this->request->data['profile_id'];
            $this->request->data['auditpoint'] =  $this->request->data['auditpoint'];
            $this->request->data['status'] = '1';
            $this->request->data['created_by'] = $resolveby;
            $this->request->data['created'] = date('Y-m-d H:i:s');
            $this->request->data['modified'] = date('Y-m-d H:i:s');
            $this->request->data['agency_id'] = $session_info[3];
            
            $users      = $this->Profileaudits->newEntity($this->request->data(), ['validate' => false]);       
            $result     = $this->Profileaudits->save($users);
            $last_id    = $users->id;

            $last_res = $this->Profileaudits->find('all',[
                'conditions' => ['Profileaudits.id' => $last_id]
            ])->contain('Users')->first();
          
            $last_audit_id = $last_res['id'];
         
            if ($last_res['status'] == 1) {
                $colorchange = "audit_open";
            }else{
                $colorchange = "audit_close";
            }

            $last_row = "<tr  id=togle_color_".$last_audit_id." class=" . $colorchange . ">
                            <td>" . $last_res['auditpoint'] . "</td>
                            <td>" . ""."</td>
                            <td>" . $last_res['Users']['first_name'] . "</td>
                            <td>" . $last_res['Users']['modified'] . "</td>
                            <td><span id='status_$last_audit_id'>";

            if($last_res['status']==1){

            $last_row .= '<button type="button" class="change_button_size btn btn-danger"  id="open_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');"  style="display: none;">Close</button>';
            $last_row .= '<button type="button" class="change_button_size btn btn-danger" id="close_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');"">Open</button>';
            }else{
            $last_row .= '<button type="button" class="change_button_size btn btn-danger" id="open_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');" >Close</button>';
            $last_row .= '<button type="button"  class="change_button_size btn btn-danger" id="close_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');" style="display: none;">Open</button>';
            }
            $last_row.= '<a href="javascript:void(0)" class="info-tooltip icon-2 delete" title="Delete" id="del_'.$last_audit_id .'" onclick="if(is_delete()){deleteRecord('.$last_audit_id.');}"></a>';         
            $last_row .= "</span></td>
                        </tr>";
            if ($result) {
               echo $last_row;
                exit;
            }else{
                echo "0";
                exit;
            }
       }
    }

    #_________________________________________________________________________#
    /** 	
     * @Date: 14-Apr-2015	
     * @Method : admin_changestatus
     * @Purpose: This function is to change the status of profile audit.	
     * @Param: $id	
     * @Return: none 	
     * @Return: none 
     * */

    function changestatus() {

            if ($this->request->is('ajax')) {

                $id = $this->request->data['id'] = $this->request->data['id'];

                $status = $this->request->data['status'] = $this->request->data['status'];

                $session = $this->request->session();
                $this->set('session',$session);
                $userSession = $session->read("SESSION_ADMIN");
                $resolveby = $userSession[0];

                if ($this->request->data['status'] == 1) {
                    $status = 2;
                    $temp = "closed";
                    $resolvedby1= $this->request->data['resolvedby'] = $resolveby;

                }else if ($this->request->data['status'] == 2) {
                    $status = 1;
                    $temp = "open";
                    $resolvedby1= $this->request->data['resolvedby'] = $resolveby;
                    }

                $credentials = TableRegistry::get('Profileaudits');
                $update = ['status' => $status,'resolvedby' => $resolvedby1];
                $result=$credentials->updateAll($update,["`id`" =>$id]);

                    if ($result) {
                        echo "1";
                        exit;
                    } else {
                        echo "0";
                        exit;
                    }
                }     
            }
    
    #_________________________________________________________________________#
    /** 	
     * @Date: 29-Apr-2015	
     * @Method : admin_delete
     * @Purpose: This function is to delete profile audits from admin section.	
     * @Param: $id	
     * @Return: none 	
     * @Return: none 
     * */

    function delete() {
            if ($this->request->is('ajax')) { 
            if (isset($this->request->data['key']) && $this->request->data['key'] == 'delete') {
                    
            $id=$this->request->data['id'];

            $entity = $this->Profileaudits->get($id);

            $res = $this->Profileaudits->delete($entity);  
                        if ($res)
                            echo 1;
                        else
                            echo 0;
                        exit;
                     }
                }
          }
    }

