<?php
namespace App\Controller\Reports;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Inflector;


class ReportsController extends AppController {




	/********************** START FUNCTIONS **************************/


	#_________________________________________________________________________#

    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
      	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	/*if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");

    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}*/
    // end code to check permissions
    	Router::parseNamedParams($this->request);
    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('layout_admin');

    	}

    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }
      /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is the default function of the controller
    * @Param: none                                                
    * @Return: none                                               
    **/
    public function initialize()
    {
    	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $this->loadComponent('Upload');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }
    

    /**                         
    * @Date: 20-sep-2016     
    * @Method : index      
    * @Purpose: This function is to show the list of Admin Reports
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function index(){ 
    	//$this->viewBuilder()->layout('new_layout_admin');
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$userSession = $session->read("SESSION_ADMIN");
    	$role = explode(",",$userSession['2']);
    	if(in_array("1", $role)){
    		$this->viewBuilder()->layout('new_layout_report');
    		$this->set('title_for_layout','Report Listing');      
    		$this->set("pageTitle","Report Listing");     
    		$this->set("search1", "");                           
    		$this->set("search2", "");                                 
		$criteria = "1"; //All Searching	  
		$session->delete('SESSION_SEARCH');	
	}else{
		//$this->Flash->error('Sorry, You are not authorized to access.');
		$this->redirect(array('controller'=>'Users','action'=>'dashboard'));
	}
}                                              

/**                         
    * @Date: 20-sep-2016       
    * @Method : admin_salary_report      
    * @Purpose: This function is to show salary report
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	public function admin_salary_report()
	{
		$this->layout= 'layout_graph';

	}



	/**                         
    * @Date: 20-sep-2016      
    * @Method : admin_lead_report_datawise      
    * @Purpose: This function is to show lead report data wise
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	public function admin_lead_report_datawise()
	{
		$this->layout= 'layout_graph';

	}

/**                         
    * @Date: 20-sep-2016    
    * @Method : admin_leave_report_userwise      
    * @Purpose: This function is to show leave report user wise
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	public function admin_leave_report_userwise()
	{
		$this->layout= 'layout_graph';

	}
/**                         
    * @Date: 20-sep-2016        
    * @Method : admin_performance_report_datawise      
    * @Purpose: This function is to show performance report data wise
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	public function admin_performance_report_datawise()
	{
		$this->layout= 'layout_graph';

	}
	
/**                         
    * @Date: 20-sep-2016        
    * @Method : admin_performance_report_user_month_wise      
    * @Purpose: This function is to show performance report user and month wise
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	public function admin_performance_report_user_month_wise()
	{
		$this->layout= 'layout_graph';

	}

	/**                       
	* @Date: 20-sep-2016    
	* @Method : admin_learningcenterreport_date      
    * @Purpose: This function is to show the learning center Reports Date Wise.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_learningcenterreport_date(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016      
	* @Method : admin_learningcenterreport_user      
    * @Purpose: This function is to show the learning center Reports User Wise.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_learningcenterreport_user(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016       
	* @Method : admin_userticketreport_date      
    * @Purpose: This function is to show the User Ticket Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_userticketreport_date() {
		$this->layout='layout_graph';
	}
	

	/**                       
	* @Date: 20-sep-2016      
	* @Method : admin_billingreport_week      
    * @Purpose: This function is to show the Billing Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_billingreport_week(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016      
	* @Method : admin_paymentreport_week      
    * @Purpose: This function is to show the Payment Reports Week Wise.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_paymentreport_week(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016      
	* @Method : admin_bidspiechart_report      
    * @Purpose: This function is to show the Pie Chart of no of Bids Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_bidspiechart_report(){
		$this->layout='layout_graph';
	}
	

	/**                       
	* @Date: 20-sep-2016       
	* @Method : admin_activelead_report      
    * @Purpose: This function is to show the Active Lead Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_activelead_report(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016       
	* @Method : admin_winclose_report      
    * @Purpose: This function is to show the Reports of Win/Closed Project.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_winclose_report(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date:20-sep-2016     
	* @Method : admin_overleadcountuser_report     
    * @Purpose: This function is to show the Over Lead Count Reports User Wise.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_overleadcountuser_report(){
		$this->layout='layout_graph';
	}
	

	/**                       
	* @Date: 20-sep-2016      
	* @Method : admin_companywidelead_report    
    * @Purpose: This function is to show the Company Wide Lead Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_companywidelead_report(){
		$this->layout='layout_graph';
	}
	
	
	/**                       
	* @Date: 20-sep-2016       
	* @Method : admin_leave_report     
    * @Purpose: This function is to show the Leave Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function admin_leave_report(){
		$this->layout='layout_graph';
	}

	/*----------------code to test -------------------*/	
	public function admin_report()
	{

	}

	public function admin_market()
	{
		$this->layout= false; 
	}
	public function admin_final()
	{
		
	}
	public function admin_report3()
	{

	} 

}
