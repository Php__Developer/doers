<?php
namespace App\Controller\PaymentMethods;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Routing\Router;

class Payment_methodsController extends AppController
{


 var $name = "PaymentMethods";    // Controller Name    
    /**
     * Specifies helpers classes used in the view pages 
     * @access public         
     */

    /**      
     * Specifies components classes used
     * @access public    
     */
    var $components = array('RequestHandler', 'Email', 'Common');
    //var $paginate        =  array(); //pagination             
    var $uses = array('PaymentMethod', 'Project', 'Contact', 'User', 'StaticPage'); // For Default Model    
 /**
  * @WHY = 
  * @WHEN  => 15-09-2016
  * @Who => Priyanka Sharma
  */

    function beforeFilter(Event $event) {
      parent::beforeFilter($event);
        // star to check permission
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
            $controller=$this->request->params['controller'];
            $action=$this->request->params['action'];
            $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
            if($permission!=1){
        
        $this->Flash->success_new("You have no permisson to access this.");
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
            }
    // end code to check permissions
       Router::parseNamedParams($this->request);
       if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

        $this->checkUserSession();
    } else {
        $this->viewBuilder()->layout('layout_admin');     
    }
   
    $session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");

    if($userSession==''){
    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common', $this->Common);
    }

 /**
  * @WHY = 
  * @WHEN  => 15-09-2016
  * @Who => Priyanka Sharma 
  */
public function initialize()
{
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); 
        $this->loadModel('Evaluations'); 
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('PaymentMethods');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Resumes');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        
        $this->set('session',$session);
        $this->loadComponent('Paginator');
     }

 /**
  * @WHY = Add payment method 
  * @WHEN  => 15-09-2016
  * @Who => Priyanka Sharma 
  */
    function paymentMethod() {
    
        $this->set('title_for_layout', 'Add Payment Method');
        $this->set("pageTitle", "Add Payment Method");
        $this->viewBuilder()->layout('new_layout_admin');
        $mode = "add";
        $session = $this->request->session();      
        $this->set('session',$session);  
        $userSession = $session->read("SESSION_ADMIN");
        if($this->request->data){
            $name = $this->request->data['name'];
            $type = $this->request->data['type'];
            $this->request->data['agency_id'] = $userSession[3];
            $PermissionsValidations = $this->PaymentMethods->newEntity($this->request->data());
            if(empty($PermissionsValidations->errors())){
                $this->PaymentMethods->save($PermissionsValidations);
                $this->Flash->success_new("Payment Method has been created successfully.");
                $this->setAction('paymentMethodlist');
            }else{
                $this->set("errors", $PermissionsValidations->errors());
            }
        }

    } 


     /**
  * @WHY = display payment method listing
  * @WHEN  => 15-09-2016
  * @Who => Priyanka Sharma
  */
    function paymentMethodlist() {

        $this->set("title_for_layout", "Payment Method Listing");
        $this->set("pageTitle", "Payment Method Listing");
        $this->viewBuilder()->layout('new_layout_admin');

        $paymentMethod = TableRegistry::get('payment_methods');

        $query  =  $this->PaymentMethods->find("all");
  
        $p=isset($query)? $query->toArray():array();

        $this->set('resultData', $p);
              $this->set('pagename', "paymentMethodlist");
    }


    /**
  * @WHY = delete payment
  * @WHEN  => 16-09-2016
  * @Who => Priyanka Sharma 
  */

   function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
        $employee = $this->PaymentMethods->get($id);
     if ($this->PaymentMethods->delete($employee)) {
              $credentials = TableRegistry::get('Credentials');
                $update = ['user_id' => 0 , 'status' => 0];
              $credentials->updateAll($update,['user_id IN' => $id]);
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 



 /**
  * @WHY =  edit user
  * @WHEN  => 16-09-2016
  * @Who => Priyanka Sharma
  */
     function edit($id = null) {  
           $this->viewBuilder()->layout('new_layout_admin');
        $this->set('title_for_layout','Edit Payment Method');     
        $this->pageTitle = "Edit Payment Methods";      
        $this->set("pageTitle","Edit Payment Methods");     
        $mode = "edit"; 
        if($this->request->data){
            $date=date_create($this->request->data['modified']);
           
            $this->request->data['modified'] = date('Y-m-d');
            $this->request->data['created'] = date_format($date,"Y-m-d");
            
            $paymentMethod = $this->PaymentMethods->get($this->request->data['id']);
            $paymentMethod = $this->PaymentMethods->patchEntity($paymentMethod,$this->request->data());
            
            if(empty($paymentMethod->errors())){
                $this->PaymentMethods->save($paymentMethod);
                $this->Flash->success_new("Payment Method has been updated successfully.");
                $this->setAction('paymentMethodlist');
            }else{                                      
               $this->set("errors", $paymentMethod->errors());
           }

       }else if(!empty($id)){  

        $paymentMethod = $this->PaymentMethods->get($id);
        $this->set('paymentMethod',$paymentMethod);                                           
        if(!$paymentMethod){                                                         
            $this->redirect(array('action' => 'paymentMethodlist'));                             
        }                                                                         
     }
  $this->set("id", $id);
  }
}
?>
