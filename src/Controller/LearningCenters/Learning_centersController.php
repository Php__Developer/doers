<?php 

/**           
	* Learning Center Controller class
	* PHP versions 5.3.5          
	* @date 18-Mar-2012           
	* @Purpose:This controller handles all the functionalities regarding learning Center management.
	* @filesource                                                                          
	* @author  Sandeep Srivastava                                                          
	* @revision                                                                            
	* @version 1.3.12                                                                      
**/

//App::import('Sanitize'); // To get rid of Malicious Data

namespace App\Controller\LearningCenters;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Routing\Router;

//use Cake\Mailer\Email;

class  Learning_centersController extends AppController {

   // var $name       	=  "LearningCenters";    // Controller Name       	

    /**
    * Specifies helpers classes used in the view pages
    * @access public                                  
    */                                                      
    //var $helpers    	=  array('Html', 'Form', 'Javascript', 'Session', 'General');
	

    /**
	  * Specifies components classes used
	  * @access public
    */              
  //  var $components 	 =  array('RequestHandler','Email','Common');   

  //  var $paginate		   =  array(); //pagination
                                            
  //  var $uses       	 =  array('LearningCenter','Role','User'); // For Default Model
    	

/******************************* START FUNCTIONS **************************/


	#_________________________________________________________________________#

    /**                                                           
    * @Date: 19-Apr-2012                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event){ // This  function is called first before parsing this controller file
    	 parent::beforeFilter($event);
    	   // star to check permission
  $session = $this->request->session();
  $userSession = $session->read("SESSION_ADMIN");

    // end code to check permissions
		if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){

			$this->checkUserSession();
		}else{

			$this->viewBuilder()->layout('layout_admin');
		//$this->layout="layout_front";
		}
		Router::parseNamedParams($this->request);
$session = $this->request->session();
$userSession = $session->read("SESSION_ADMIN");
if($userSession==''){
return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
}
	$this->set('common',$this->Common); 	
	
    }





 public function initialize()
    {
        parent::initialize();
        $this->loadModel('LearningCenters'); // for importing Model
       // $this->loadModel('Attendances'); // for importing Model
        //$this->loadModel('Tickets'); // for importing Model
       // $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        // $this->loadModel('AlternateLogin');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
    }



    #_________________________________________________________________________#

    /**      
    * @Date: 19-Apr-2012        
    * @Method : index           
    * @Purpose: This function is the default function of the controller
    * @Param: none                                                     
    * @Return: none                                                    
    **/

    function index() {
    
      $this->render('login');
      
      if($this->Session->read("SESSION_USER") != ""){      
      $this->redirect('dashboard');                        
      }    
    }



	

	#_________________________________________________________________________#

    /**                         
    * @Date: 19-Apr-2012        
    * @Method : admin_list      
    * @Purpose: This function is to show list of LearningCenter in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function list(){ 
    	//pr("helo");
        $session = $this->request->session();
        $this->set('session',$session);
//pr("helo");
      $this->set('title_for_layout','Learning Center');      
      $this->set("pageTitle","Learning Center");             
      $this->set("search1", "");                                 
      $this->set("search2", "");                                 
      $criteria = "1"; //All Searching
	  $session->delete('SESSION_SEARCH');
	  //pr($session);
	  $session_admin = $session->read("SESSION_ADMIN");
	  pr($session_admin);
	//pr($this->params); die;
	 // pr($$session->read("SESSION_ADMIN"))->toArray();
      if(isset($this->data['LearningCenter']) || !empty($this->params['named'])) {      
      if(!empty($this->data['LearningCenter']['fieldName']) || isset($this->params['named']['field'])){                                                                 
        if(trim(isset($this->data['LearningCenter']['fieldName'])) != ""){                
        $search1 = trim($this->data['LearningCenter']['fieldName']);                      
        }elseif(isset($this->params['named']['field'])){                              
        $search1 = trim($this->params['named']['field']);                             
        }
        
        $this->set("search1",$search1);                                               
      }
      
      if(isset($this->data['LearningCenter']['value1']) || isset($this->data['LearningCenter']['value2']) || isset($this->params['named']['value'])){                  
      if(isset($this->data['LearningCenter']['value1']) || isset($this->data['LearningCenter']['value2'])){                                                            
      $search2 = ($this->data['LearningCenter']['fieldName'] != "LearningCenter.status")?trim($this->data['LearningCenter']['value1']):$this->data['LearningCenter']['value2'];      
      }elseif(isset($this->params['named']['value'])){       
      $search2 = trim($this->params['named']['value']);      
      }                                                      
      $this->set("search2",$search2);                        
      }
      
      /* Searching starts from here */                                               
      if(!empty($search1) && (!empty($search2) || $search1 == "LearningCenter.status")){                                                                           
      $criteria = $search1." LIKE '%".Sanitize::escape($search2)."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
      }else{      
      $this->set("search1","");      
      $this->set("search2","");      
      }
      
      }
      
      if(isset($this->params['named'])){      
          $urlString = "/";                       
          $completeUrl  = array();
          
          if(!empty($this->params['named']['page']))      
          $completeUrl['page'] = $this->params['named']['page'];
          
          if(!empty($this->params['named']['sort']))                  
          $completeUrl['sort'] = $this->params['named']['sort'];
          
          if(!empty($this->params['named']['direction']))             
          $completeUrl['direction'] = $this->params['named']['direction'];
          
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          
          foreach($completeUrl as $key=>$value){                      
          $urlString.= $key.":".$value."/";                           
          }
      }
      $urlString = "/";
      $this->set('urlString',$urlString);   
		 $completeUrl  = array();
      $cond=""; $orderby="";
      $user_id = $session_admin['0'];   
	  if($user_id != ADMIN_ID){ 
		$orderby = "LearningCenter.title asc";
	  }else{
		$orderby = "LearningCenter.modified desc";
	  }
     //$cond = "(FIND_IN_SET ($user_id,user_id) OR is_default='1')";
	  if($user_id != ADMIN_ID) $cond = " LearningCenter.status = '1'";
       $paginate = [
      'conditions'=>$cond,                                     
      'fields' => [                                              
       'LearningCenter.id',   
	   'LearningCenter.title',
	   'LearningCenter.content',
	   'LearningCenter.created',
	   'LearningCenter.modified',
	   'LearningCenter.status',
	   'LearningCenter.skill', 
	   'User.first_name', 
	   'User.last_name', 	   
      ],      
      'page'=> 1,'limit' => RECORDS_PER_PAGE,
      'order' => ['LearningCenter.status' => 'desc',$orderby]
	  //'group' => 'category'
      ];
    // pr($paginate);
//$query = $articles->find('all')->contain(['Comments']);

	$query = $this->LearningCenters->find('all')->contain(['Users']);

//pr($this->LearningCenters->find('all')->contain(['Users']));
//pr($this->LearningCenters->find('all')->contain(['Users']));die;


   // $this->LearningCenter->expects(array("User"));	

     $data = $this->paginate($query, ['criteria' => $criteria]); 
    // $data = $this->paginate('LearningCenter',$criteria); 
     //pr($data); die;
      $this->set('resultData', $data);

	  if($user_id != ADMIN_ID){
	  	//pr(ADMIN_ID);

		$this->render('temp_list'); 
	  }
	  
	}                                                 
	

	#_________________________________________________________________________#

	/**
    * @Date: 22-Apr-2012        
    * @Method : admin_changeStatus    
    * @Purpose: This function is used to delete LearningCenters.
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
     
		function admin_changeStatus(){
			//pr($this->params); die;
				if(isset($this->params['form']['publish'])){      
					  $setValue = array('status' => "'1'");             
					  $messageStr = "Selected LearningCenter(s) have been activated.";    
					  //$this->redirect('list');   
					  }elseif(isset($this->params['form']['unpublish'])){
					  $setValue = array('status' => "'0'");             
					  $messageStr = "Selected LearningCenter(s) have been deactivated."; 
						//$this->redirect('list');   
					  }
																	   
					  if(!empty($setValue)){                            
						  if(isset($this->params['form']['IDs'])){          
						  $saveString = implode("','",$this->params['form']['IDs']);      
						  }
						  
						  if($saveString != ""){                                          
						  $this->LearningCenter->updateAll($setValue,"LearningCenter.id in ('".$saveString."')");                                                                 
						  $this->Session->setFlash($messageStr, 'layout_success');  
						   $this->redirect('list');   
						  }
					}
		}
  	#_________________________________________________________________________#
	
	/**

    * @Date: 22-Apr-2012        
    * @Method : admin_delete    
    * @Purpose: This function is used to delete LearningCenters.
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
     
		 function admin_delete()
		{
				 // if(isset($this->params['form']['delete']) || isset($this->params['pass'][0]) == "delete"){                                                      
				  if(isset($this->params['form']['IDs'])){                         
					$deleteString = implode("','",$this->params['form']['IDs']);      
				  }elseif(isset($this->params['pass'][0])){                         
					$deleteString = $this->params['pass'][0];                         
				  }
				  
				  if(!empty($deleteString)){      
					$this->LearningCenter->deleteAll("LearningCenter.id in ('".$deleteString."')");
					$this->Session->setFlash("<div class='success-message flash notice'>LearningCenter(s) deleted successfully.</div>", 'layout_success');
					$this->redirect('list');                                                  
				  }
				  
			//	  }
		}
  
  	#_________________________________________________________________________#

    /**

    * @Date: 22-Apr-2012        
    * @Method : admin_add      
    * @Purpose: This function is to send LearningCenters to users
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
        
	
	function add() {
		//pr("here");

		  $session = $this->request->session();
		 // pr($session);
          $this->set('session',$session);
//pr("here");
		     $this->set('title_for_layout','Add Templates');
			 $this->pageTitle = "Add LearningCenter";
			 $this->set("pageTitle","Add LearningCenter");
			// pr("here");
			 $session_admin = $session->read("SESSION_ADMIN");
			 //pr("here");
			 $user_id = $session_admin['0']; 

//pr($user_id );
			 $userName = $session_admin['1']; 
			//pr("here");
			if($this->request->data){
//pr("here");
				$this->request->data['LearningCenter']['meta_keywords'] = $this->request->data['LearningCenter']['title'];
//pr("here");
				 $this->request->data['LearningCenter']['meta_description'] =  $this->request->data['LearningCenter']['title'];
				 $this->request->data['LearningCenter']['user_id'] = $user_id;
				if($user_id != ADMIN_ID){
					 $this->request->data['LearningCenter']['status'] = 2;
				}
			$this->LearningCenter->set( $this->request->data['LearningCenter']);

			//$validator = $this->Employees->validator('default');
			$isValidated=$this->LearningCenter->validates();
		   // $errors = $validator->errors($this->request->data());
			//pr($this->data); die;
			if($isValidated){
					 //  $this->data['LearningCenter']['role_id'] = null;
					 //  $this->data['Permission']['user_id'][] = ADMIN_ID;
					   $this->LearningCenter->save($this->data['LearningCenter'], array('validate'=>false)); 
					    if($user_id != ADMIN_ID){
							$this->Session->setFlash("Thanks for sharing your knowledge! Admin will publish the blog after reviewing the same.",'layout_success'); 
                        }else{
                           $this->Session->setFlash("Template has been created successfully.",'layout_success'); 
						}
					   $this->redirect('list');                
				}else{                                      
					$this->set("Error",$this->LearningCenter->invalidFields());
				}

		}                                                                 
	}
  
  	#_________________________________________________________________________#
	
   /**

    * @Date: 22-Apr-2012        
    * @Method : admin_edit
    * @Purpose: This function is to send LearningCenters to users
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
        
	
	function admin_edit($id = null) {
		    $this->set('title_for_layout','Edit Templates');      
			$this->pageTitle = "Edit LearningCenter";
			$this->set("pageTitle","Edit LearningCenter");
			$getRoles = $this->Role->find('list', array('fields'=>array('id','role')));
			$this->set('resultData',$getRoles);
			if($this->data){ //pr($this->data); die;
				$this->data['LearningCenter']['meta_keywords'] = $this->data['LearningCenter']['title'];
				$this->data['LearningCenter']['meta_description'] = $this->data['LearningCenter']['title'];
				$this->LearningCenter->set($this->data['LearningCenter']);
				$isValidated=$this->LearningCenter->validates();
		 
				if($isValidated){
						    $this->LearningCenter->save($this->data['LearningCenter'], array('validate'=>false));                                         
						    $this->Session->setFlash("LearningCenter has been updated successfully.",'layout_success');                                      
						    $this->redirect('list');                
					}else{                                      
						   $this->set("Error",$this->LearningCenter->invalidFields());
					}

			}else if(!empty($id)){ 
			
			$this->data = $this->LearningCenter->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));
			//	pr($this->data); die;
			if(!empty($this->data['LearningCenter']['user_id'])){
				//$this->data['LearningCenter']['role_id'] = explode(",",$this->data['LearningCenter']['role_id']);
				$users= $this->data['LearningCenter']['user_id'];
				$cond = "WHERE id in ($users)";
				$result = $this->User->find('all', 
					array(
						'fields' => array('User.id', 'User.employee_code','User.first_name','User.last_name','User.username','User.status','User.role_id'),
						'conditions' => $cond,
				));
				$this->data['LearningCenter']['userData'] = $result ;
			}
			//print_r($this->data);die;         
			//SELECT * FROM `users` WHERE `id` in (3,4,5)
			if(!$this->data){                                                         
				$this->redirect(array('action' => 'list'));                             
			}                                                                     
		}                                                                           
	}
  
	#_________________________________________________________________________#
	
   /**

    * @Date: 18-Jan-2013
    * @Method : admin_setPermission
    * @Purpose: This function is to set LearningCenters permissions to user
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
	  function admin_setPermission() {
		  if(isset($this->params['form']['IDs'])){  
		  
		   $pages = $this->params['form']['IDs'];
		   $pagesID = implode(",",$pages);
		   
		   $cond = "WHERE LearningCenter.id in ($pagesID)";
		   $pageResult = $this->LearningCenter->find('list', array(
			'fields' => array('LearningCenter.id', 'LearningCenter.user_id'),
			'conditions' => $cond
		   ));
		   //pr($pageResult); 
		   $userID = $this->data['LearningCenter']['user_id']; 
		   
		   foreach($pages as $val)
		   {
			$pageResult[$val] .= ",".$userID;
			$users = explode(",",$pageResult[$val]);
			$user_id = implode(",",array_unique($users));
			$result = $this->LearningCenter->updateAll(
			 array('LearningCenter.user_id' => "'$user_id'"),
			 array('LearningCenter.id =' => $val)
			); 
		   }
		   if($result){
			$this->Session->setFlash("<div class='success-message flash notice'>LearningCenter(s) have been assigned permissions successfully.</div>", 'layout_success');
			$this->redirect('list');                                                  
		   }
	  }
	}

    /**

    * @Date: 28-Dec-2011

    * @Method : admin_view

    * @Purpose: Function to view a static page.

    * @Param: $id

    * @Return: none

    **/

    

    function admin_view($id = null) 
	{
		$this->layout = false;
		if(isset($id) && is_numeric($id)){
			$result = $this->LearningCenter->findById($id);
			if($result){
				$this->set('result',$result);
			}else{

				$this->redirect('static_pages');
			}
		}else{

			$this->redirect('static_pages');
		}

	}
	#_________________________________________________________________________#



    /**

    * @Date: 8-Sep-2010

    * @Method : view

    * @Purpose: Function to view a static page.

    * @Param: $id

    * @Return: none

    **/

    

    function view($slug= null) {
	$this->layout="layout_inner";

	if(isset($slug)){

	     if(!empty($this->data)){

			$this->contactus($id);
		   }
		$result = $this->LearningCenter->findBySlug($slug);
		if($result){

			$this->set('result',$result);
		}else{

			$this->redirect('/');
		}
	}else{
		$this->redirect('/');

	}

    }
 
 #_________________________________________________________________________#



    /**

    * @Date: 23-Nov-2010

    * @Method : contactus

    * @Purpose: This function is used to show list of Credits 

    * @Param: none

    * @Return: none 

    **/

    function contactus($id) {
	$this->LearningCenter->set($this->data['LearningCenter']);

	$isValidated = $this->LearningCenter->validates();

	if($isValidated){

	$subject = "Notification: Contact Us";

	$message = "Dear Admin,<br/><br/>A user has tried to contact you. Please find the details below:<br/>

	<table cellpadding='4' cellspacing='1' border='0' style='font-size:12px;'>

	<tr><td align='right'><b>Subject:</b></td><td>".$this->data['LearningCenter']['subject']."</td></tr>

	<tr><td align='right'><b>Name:</b></td><td>".ucwords($this->data['LearningCenter']['name'])."</td></tr>

	<tr><td align='right'><b>Email address:</b></td><td>".$this->data['LearningCenter']['email']."</td></tr>

	<tr><td align='right'><b>Website:</b></td><td>".$this->data['LearningCenter']['website']."</td></tr>

	<tr><td align='right'><b>Phone:</b></td><td>".$this->data['LearningCenter']['phone']."</td></tr>

	<tr><td align='right'><b>Notes/Questions/Feedback:</b></td><td>".$this->data['LearningCenter']['feedback']."</td></tr>

	</table><br/>Thanks,<br/>Dog Booking Support";



	$this->Email->to   = ADMIN_EMAIL;

	$this->Email->subject  = $subject;

	$this->Email->replyTo  = ADMIN_EMAIL;

	$this->Email->from     = ADMIN_EMAIL;

	$this->Email->fromName = ADMIN_NAME;

	$this->Email->sendAs   = 'html';

	$this->Email->send($message);

	$this->Session->setFlash("<div class='success-message flash notice'>Thank you for contacting us. One of our representative will contact you soon!!!</div>");

	$this->redirect('/static_pages/page/'.$id);

	}else{

		$this->set('error',$this->LearningCenter->invalidFields());

	}
	
	
   }

  #_________________________________________________________________________#  
 /**                       
 * @Date: 03-Sep-2014       
 * @Method : admin_learningcenterreport_date  
    * @Purpose: This function is to show the Company Wide Learning_center Reports.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  
 
	function admin_learningcenterreport_date(){
	$this->layout='layout_graph';
	$start_date=$this->data['LearningCenter']['start_date'];
	$end_date=$this->data['LearningCenter']['end_date'];
	$leadselect=$this->data['LearningCenter']['select'];
	$this->set('fromDate',$start_date);
	$this->set('toDate',$end_date);
	$cur=date('Y/m/d');
	$cond = array('LearningCenter.status'=>'1');
	$cond1 = array('LearningCenter.created between ? and ?' => array($start_date, $end_date));
 if(empty($start_date)&&empty($end_date))
 {
  $resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
		'User.first_name',
		'LearningCenter.title',
		'LearningCenter.content',
		'LearningCenter.created'
      ),
    'joins' => array (
    array (
		'table' => 'users',
		'alias' => 'User',
		'type'=>'INNER',
		'foreignKey' => false,
		'conditions' => array ('LearningCenter.user_id = User.id')
	)),
		'conditions' =>array('LearningCenter.created'=>$cur,$cond),
		'group'=>array('LearningCenter.id'))); 
	$usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
		'count(LearningCenter.id)',
		'User.first_name'
      ),
    'joins' => array (
    array (
		'table' => 'users',
		'alias' => 'User',
		'type'=>'INNER',
		'foreignKey' => false,
		'conditions' => array ('LearningCenter.user_id = User.id')
	)),
		'conditions' =>array('LearningCenter.created'=>$cur,$cond),
		'group'=>array('LearningCenter.user_id'))); 
	}
	else
		{
	$resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
		'User.first_name',
		'count(LearningCenter.id)',
		'LearningCenter.title',
		'LearningCenter.content',
		'LearningCenter.created'
      ),
    'joins' => array (
    array (
		'table' => 'users',
		'alias' => 'User',
		'type'=>'INNER',
		'foreignKey' => false,
		'conditions' => array ('LearningCenter.user_id = User.id')
	)),
		'conditions'=>array($cond,$cond1),
		'group'=>array('LearningCenter.id')));
	$usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
		'count(LearningCenter.id)',
		'User.first_name'
      ),
	'joins' => array (
    array (
		'table' => 'users',
		'alias' => 'User',
		'type'=>'INNER',
		'foreignKey' => false,
		'conditions' => array ('LearningCenter.user_id = User.id')
	)),
		'conditions'=>array($cond,$cond1),
		'group'=>array('LearningCenter.user_id'))); 
	}
	$this->set('resultData', $resultData);
	$this->set('usercount', $usercount);
}         
#_________________________________________________________________________#  
 /**                       
 * @Date: 03-Sep-2014       
 * @Method : admin_learningcenterreport_user 
    * @Purpose: This function is to show the Company Wide Learning_center Reports.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  
 
	function learningcenterreportuser(){

  // pr($this->request);die();
  //$this->viewBuilder()->layout('layout_admin');
	$this->viewBuilder()->layout('layout_graph');
  
  if(count($this->request->data) >0){
  $userselect=$this->request->data['user_id'];
  $start_date=$this->request->data['start_date'];
  $end_date=$this->request->data['end_date'];
  $leadselect=$this->request->data['select'];
    
  } else {
  $userselect="";
  $start_date="";
  $end_date="";
  $leadselect="";
   
  }
  //pr( $userselect); die;
	$this->set('fromDate',$start_date);
  $this->set('toDate',$end_date);
  $cur=date("Y-m-d H:i:s");
  //pr($cur);
  $cond1 = array('LearningCenters.created between ? and ?' => array($start_date, $end_date));
  $cond = array('LearningCenters.status'=>'1');
 if(empty($userselect)&&empty($start_date)&&empty($end_date))
 {
/*$resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'CONCAT(User.first_name, " ", User.last_name) as first_name',
      'LearningCenter.title',
    'LearningCenter.content',
    'LearningCenter.created',
    'LearningCenter.id'
      ),
    'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
   )),
    'conditions' =>array('User.status'=>1,'LearningCenter.created'=>$cur,$cond),
    'group'=>array('LearningCenter.id'))); 

  */
    $resultData = $this->LearningCenters->find('all', [
    'conditions'=>array_merge($cond ,['Users.status'=>1,'LearningCenters.created'=>$cur   ] ) ,
    ])
  ->group(['LearningCenters.id'])
  ->contain(['Users'])->toArray();
  //pr($resultData);die();

/*
  $usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'count(LearningCenter.id)',
    'CONCAT(User.first_name, " ", User.last_name) as first_name'
      ),
    'joins' => array (
  array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions' =>array('User.status'=>1,'LearningCenter.created'=>$cur,$cond),
    'group'=>array('LearningCenter.user_id'))); 
  */
 //$usercount = $this->LearningCenters->find('all')->count();
 //pr($usercount);

  $usercount = $this->LearningCenters->find('all',
  [
  'conditions'=>array_merge($cond ,['Users.status'=>1,'LearningCenters.created'=>$cur   ] ) ,


   ])
   ->contain(['Users'])
    
  ->group(['LearningCenters.id']) 
  ->toArray();
 // pr($usercount);


 }


elseif(empty($userselect)&&!empty($start_date)&&!empty($end_date))
 {
 // pr("hello");
 $resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'CONCAT(User.first_name, " ", User.last_name) as first_name',
    'count(LearningCenter.id)',
    'LearningCenter.title',
    'LearningCenter.content',
    'LearningCenter.created',
    'LearningCenter.id'
      ),
    'joins' => array (
  array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions' =>array('User.status'=>1,$cond,$cond1),
    'group'=>array('LearningCenter.id')));
  $usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'count(LearningCenter.id)',
    'CONCAT(User.first_name, " ", User.last_name) as first_name'
      ),
    'joins' => array (
      array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions' =>array('User.status'=>1,$cond,$cond1),
    'group'=>array('LearningCenter.user_id'))); 
 }
  elseif(!empty($userselect) && $leadselect!='select')
    {
      // pr("hello");
  $resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'CONCAT(User.first_name, " ", User.last_name) as first_name',
    'count(LearningCenter.id)',
    'LearningCenter.title',
    'LearningCenter.content',
    'LearningCenter.created',
    'LearningCenter.id'
  ),
    'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions' =>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond,$cond1),
    'group'=>array('LearningCenter.id')));
  $usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'count(LearningCenter.id)',
    'CONCAT(User.first_name, " ", User.last_name) as first_name'
      ),
    'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions' =>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond,$cond1),
    'group'=>array('LearningCenter.user_id'))); 
  }
  elseif(!empty($userselect) &&$start_date==$cur)
    {
      // pr("hello");
  $resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'CONCAT(User.first_name, " ", User.last_name) as first_name',
    'count(LearningCenter.id)',
    'LearningCenter.title',
    'LearningCenter.content',
    'LearningCenter.created',
    'LearningCenter.id'
  ),
    'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions'=>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond),
    'group'=>array('LearningCenter.id')));
  $usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'count(LearningCenter.id)',
    'CONCAT(User.first_name, " ", User.last_name) as first_name'
  ),
    'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions'=>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond),
    'group'=>array('LearningCenter.user_id'))); 
  }
  else
  {
    // pr("hello");
  $resultData = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'CONCAT(User.first_name, " ", User.last_name) as first_name',
    'count(LearningCenter.id)',
    'LearningCenter.title',
    'LearningCenter.content',
    'LearningCenter.created',
    'LearningCenter.id'
      ),
  'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions'=>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond,$cond1),
    'group'=>array('LearningCenter.id')));
  $usercount = $this->LearningCenter->find('all',
    array(
      'fields'=>array(
    'count(LearningCenter.id)',
    'CONCAT(User.first_name, " ", User.last_name) as first_name'
      ),
  'joins' => array (
    array (
    'table' => 'users',
    'alias' => 'User',
    'type'=>'INNER',
    'foreignKey' => false,
    'conditions' => array ('LearningCenter.user_id = User.id')
  )),
    'conditions'=>array('User.status'=>1,'LearningCenter.user_id'=>$userselect,$cond,$cond1),
    'group'=>array('LearningCenter.user_id'))); 
 }

  $this->set('resultData', $resultData);
  $this->set('usercount', $usercount);
  }
	#_________________________________________________________________________#  
 /**                       
 * @Date: 10-Oct-2014       
 * @Method : admin_reportmore 
    * @Purpose: This function is to show the Company Wide Learning_center Reports.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  
 
	function admin_reportmore($id = null){
	$this->layout='layout_graph';
	$resultData = $this->LearningCenter->find('all',
		array(
      'fields'=>array(
		'LearningCenter.content',
		'LearningCenter.id'
	  ),
		'joins' => array (
		array (
		'table' => 'users',
		'alias' => 'User',
		'type'=>'INNER',
		'foreignKey' => false,
		'conditions' => array ('LearningCenter.user_id = User.id')
	)),
		'conditions' =>array('LearningCenter.id'=>$id),
)); 
	$this->set('resultData', $resultData);
	}
}         