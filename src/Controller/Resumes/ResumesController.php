<?php

namespace App\Controller\Resumes;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;



class ResumesController extends AppController{   

	
	/******************** START FUNCTIONS **************************/	
	
	/**   
	* @Date: 20-sep-2016 
    * @Method : beforeFilter   
	* @Purpose: This function is called before any other function.    
	* @Param: none   
	* @Return: none   
	**/   

	function beforeFilter(Event $event) {
		parent::beforeFilter($event);
              	// star to check permission
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		$controller=$this->request->params['controller'];
		$action=$this->request->params['action'];
		$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
		if($permission!=1){
			$this->Flash->error("You have no permisson to access this Page.");

			return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
		}
    // end code to check permissions
		Router::parseNamedParams($this->request);
		if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

			$this->checkUserSession();
		} else {
			$this->viewBuilder()->layout('new_layout_admin');

		}

		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		if($userSession==''){
			return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
		}
		$this->set('common', $this->Common);
	}
    	/**   
	* @Date: 13-sept-2016
    * @Method : initialize   
	* @Purpose: This function is called initialize  function. it is like constructor   
	* @Param: none   
	* @Return: none   
	**/   


	public function initialize()
	{
		parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Resumes');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


	/**  
	* @Date: 20-sep-2016    
	* @Method : index   
	* @Purpose: This function is the default function of the controller  
	* @Param: none 
	* @Return: none     
	**/	
	function index() {

		$this->render('login');

		if($this->Session->read("SESSION_USER") != ""){      
			$this->redirect('dashboard');                        
		}    
	}
	

	
	/**    
	* @Date: 20-sep-2016  
	* @Method : list  
	* @Purpose: This function is to show list of Resumes in system.  
	* @Param: none   
	* @Return: none  
	**/
	function list(){     
		//echo "asdf"; die;
		$this->set('title_for_layout','Resume Listing');     
		$this->set("pageTitle","Resume Listing");                
		$this->set("search1", "");                        
		$this->set("search2", "");                       
	$criteria = "1";  //All Searching	  
	$session = $this->request->session();
	$session->delete('SESSION_SEARCH');
	$userSession = $session->read("SESSION_ADMIN");

	if(isset($this->request->data['Resumes']) || !empty($this->request->query)) {  

		if(!empty($this->request->data['Resumes']['fieldName']) || isset($this->request->query['field'])){      
			if(trim(isset($this->request->data['Resumes']['fieldName'])) != ""){    
				$search1 = trim($this->request->data['Resumes']['fieldName']);    	              
			}elseif(isset($this->request->query['field'])){                              
				$search1 = trim($this->request->query['field']);                             
			}
			$this->set("search1",$search1);                                               
		}    

		if(isset($this->request->data['Resumes']['value1']) || isset($this->request->data['Resumes']['value2']) || isset($this->request->query['value'])){     
			if(isset($this->request->data['Resumes']['value1']) || isset($this->request->data['Resumes']['value2'])){                          
				$search2 = ($this->request->data['Resumes']['fieldName'] != "Resumes.status")?trim($this->request->data['Resumes']['value1']):$this->request->data['Resumes']['value2'];       
			}elseif(isset($this->request->query['value'])){         
				$search2 = trim($this->request->query['value']);        
			}                                                
			$this->set("search2",$search2);                         
		}        
		/* Searching starts from here */               
		if(!empty($search1) && (isset($search2) || $search1 == "Resumes.status")){    
			$query  =    $this->Resumes->find("all" , [
				'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'agency_id' => $userSession[3] ],
				]);                 

			$session->write('SESSION_SEARCH', $criteria);   
		}else{
			$query  =  $this->Resumes->find("all",[
					'conditions' => ['agency_id' => $userSession[3]]
				]);
			$this->set("search1","");        
			$this->set("search2","");       
		}          
	}  else  {
			$query  =  $this->Resumes->find("all",[
					'conditions' => ['agency_id' => $userSession[3]]
				]);
	}
	$urlString = "/";
	if(isset($this->request->query)){ 
		$completeUrl  = array();                  
		if(!empty($this->request->query['page']))        
			$completeUrl['page'] = $this->request->query['page'];      
		if(!empty($this->request->query['sort']))                      
			$completeUrl['sort'] = $this->request->query['sort'];              
		if(!empty($this->request->query['direction']))               
			$completeUrl['direction'] = $this->request->query['direction'];       
		if(!empty($search1))                                   
			$completeUrl['field'] = $search1;      
		if(isset($search2))               
			$completeUrl['value'] = $search2;    
		foreach($completeUrl as $key=>$value){   
			$urlString.= $key.":".$value."/";          
		}    
	}
	$this->set('urlString',$urlString);
	$data = $this->paginate($query,[
		'page' => 1, 'limit' => 100,
		'order' => ['id' => 'desc'],
		'paramType' => 'querystring'
		]);
	$this->set('resultData', $data);
	
}	


	/**  
		* @Date: 20-sep-2016     
		* @Method : add    
		* @Purpose: This function is to add Resumes from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
		**/  

		function add() {	

			$this->set('title_for_layout','Add Resume');		
			$this->pageTitle = "Add Resume";		
			$this->set("pageTitle","Add Resume");	
			$mode = "add";		
			$session = $this->request->session();
			$this->set('session',$session);
			if($this->request->data){
				$this->request->data['created'] = date('Y-m-d');
				$userSession = $session->read("SESSION_ADMIN");
				//$postdata = $this->request->data();
            	$this->request->data['agency_id'] = $userSession[3];
				$skills = $this->Resumes->newEntity($this->request->data());
				if(empty($skills->errors())) {
					$this->Resumes->save($skills);
					$this->Flash->success("Resume has been created successfully.",'layout_success');
					$this->setAction('list');         
				}else{                                 
					$this->set("errors", $skills->errors());
				}
			}         		

		}		


	/**                         
    * @Date: 20-sep-2016        
    * @Method : edit    
    * @Purpose: This function is to edit Resume.
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	function edit($id = null) {
		$this->set('title_for_layout','Edit Resume');      
		$this->pageTitle = "Edit Resume";
		$this->set("pageTitle","Edit Resume");
		$mode = "edit";
		if($this->request->data){
			//pr($this->request->data); die;
			$date=date_create($this->request->data['created']);
			$this->request->data['created'] = date_format($date,"Y-m-d");
			$resume = $this->Resumes->get($this->request->data['id']);
			$resumes = $this->Resumes->patchEntity($resume,$this->request->data());
			if(empty($resume->errors())){
				$this->Resumes->save($resume);
				$this->Flash->success("Skill has been updated successfully.");
				//$this->setAction('list');
				$this->redirect(array('action' => 'list'));        
			}else{                                      
				$this->set("errors", $resume->errors());
			}

		}else if(!empty($id)){       
			$resume = $this->Resumes->get($id);
			$this->set('resume',$resume);                                           
			if(!$resume){                                                         
				$this->redirect(array('action' => 'list'));                             
			}                                                                         
		}                                                                                               
	}
	
	/**                         
    * @Date: 20-sep-2016         
    * @Method : delete   
    * @Purpose: This function is to delete Resume.
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	
	/*function delete($id = null){
		$employee = $this->Resumes->get($id);
		if ($this->Resumes->delete($employee)) {
			$this->Flash->success("Resume(s) deleted successfully.");
			$this->setAction('list');

		}

	}*/

	  function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
         $role = $this->Resumes->get($id);
        if ($this->Resumes->delete($role)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 
	
	

	/**
    * @Date: 20-sep-2016        
    * @Method : changeStatus    
    * @Purpose: This function is used to activate/deactivate Resumes.
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  

	function changeStatus(){
		if(isset($this->request->data['publish'])){      
			$setValue = 1;
			$messageStr = "Selected Resume(s) have been activated.";              
		}elseif(isset($this->request->data['unpublish'])){
			$setValue = 0;           
			$messageStr = "Selected Resume(s) have been deactivated.";            
		}									   
		if(isset($this->request->data['publish']) || isset($this->request->data['unpublish'])) {
			if(isset($this->request->data['IDs'])){          
				$saveString = implode("','",$this->request->data['IDs']);      
			}
			if($saveString != ""){   
				$credentials = TableRegistry::get('Resumes');
				$update = ['status' => $setValue];
				$credentials->updateAll($update,['id IN' => $this->request->data['IDs']]);
				$this->Flash->success($messageStr,'layout_success');
				$this->setAction('list');
			}
		}
	}


	/**
    * @Date: 20-sep-2016        
    * @Method : service_Resume
    * @Purpose: This function is used to fetch Resumes.
    * @Param: none                                      
    * @Return: none
    * @Return: none
	**/  
	function service_Resume(){
		$username ='admin';
		$password = md5('admin');
		//pr($this->params); die;
		$keyword = $this->params['form']['keyword'];
		App::import('Model', 'User');
		$this->User = new User();
		$userData = $this->User->find('all',array(
			'fields'=>array('first_name','last_name'),
			'conditions' => array('User.username' => $username,'User.password' => $password)
			));
		if($userData){
			$condition = "WHERE `keywords` LIKE '%$keyword%' and `status`=1 order by star DESC";
			$ResumeData = $this->Resume->find('all',array(
				'conditions' => $condition
				));
			if($ResumeData){
				$finalreturndata=array('result'=>$ResumeData);
			}else{
				$finalreturndata=array('result'=>null);
			}
			exit(json_encode($finalreturndata));
		}else{
			echo "Invalid username or password";
			exit;  
		}
		
	}
	


}