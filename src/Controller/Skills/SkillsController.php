<?php

namespace App\Controller\Skills;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;




class SkillsController extends AppController {

var $paginate		   =  array(); //pagination

    var $uses       	 =  array('Skill'); // For Default Model


    /******************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
            	  	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");

    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    // end code to check permissions
    	Router::parseNamedParams($this->request);
    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('layout_admin');

    	}

    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize  function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Skills');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    /**                         
    * @Date: 20-sep-2016        
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function list(){     
    	$this->set('title_for_layout','Skills Listing');      
    	$this->set("pageTitle","Skills Listing");     
    	$this->set("search1", "");                                 
    	$this->set("search2", "");                                 
		$criteria = "1"; //All Searching
		$session = $this->request->session();
		$session->delete('SESSION_SEARCH');

		if(isset($this->request->data['Skill']) || !empty($this->request->query)) {

			if(!empty($this->request->data['Skill']['fieldName']) || isset($this->request->query['field'])){                                                                 
				if(trim(isset($this->request->data['Skill']['fieldName'])) != ""){    

					$search1 = trim($this->request->data['Skill']['fieldName']);    

				}elseif(isset($this->request->query['field'])){                              
					$search1 = trim($this->request->query['field']);                  

				}
				$this->set("search1",$search1);                                               
			}
			if(isset($this->request->data['Skill']['value1']) || isset($this->request->data['Skill']['value2']) || isset($this->request->query['value'])){                  
				if(isset($this->request->data['Skill']['value1']) || isset($this->request->data['Skill']['value2'])){                                                            
					$search2 = ($this->request->data['Skill']['fieldName'] != "Skill.status")?trim($this->request->data['Skill']['value1']):$this->request->data['Skill']['value2'];      
				}elseif(isset($this->request->query['value'])){       
					$search2 = trim($this->request->query['value']);      
				}                                                      
				$this->set("search2",$search2);                        
			}
			if(isset($this->request->data['Skill']['value1']) || isset($this->request->data['Skill']['value2']) || isset($this->request->query['value'])){                  
				if(isset($this->request->data['Skill']['value1']) || isset($this->request->data['Skill']['value2'])){                                                            
					$search2 = ($this->request->data['Skill']['fieldName'] != "Skill.publish")?trim($this->request->data['Skill']['value1']):$this->request->data['Skill']['value2'];      
				}elseif(isset($this->request->query['value'])){       
					$search2 = trim($this->request->query['value']);      
				}                                                      
				$this->set("search2",$search2);                        
			}
			/* Searching starts from here */                                               
			if(!empty($search1) && (!empty($search2) || $search1 == "Skill.publish")){ 
				$query  =    $this->Skills->find("all" , [
					'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
					]);
				$result = isset($query)? $query->toArray():array();

				$criteria = $search1." LIKE '%".$search2."%'"; 
				$session->write('SESSION_SEARCH', $criteria);
			}else{ 
				$query  =  $this->Skills->find("all");    
				$this->set("search1","");      
				$this->set("search2","");      
			}

		}


		$urlString = "/";		
		if(isset($this->request->query)){
			$completeUrl  = array();	
			if(!empty($this->request->query['page']))
				$completeUrl['page'] = $this->request->query['page'];	
			if(!empty($this->request->query['sort']))		
				$completeUrl['sort'] = $this->params['named']['sort'];		
			if(!empty($this->params['named']['direction']))		
				$completeUrl['direction'] = $this->request->query['direction'];	
			if(!empty($search1))
				$completeUrl['field'] = $search1;
			if(isset($search2))
				$completeUrl['value'] = $search2;
			foreach($completeUrl as $key=>$value){		
				$urlString.= $key.":".$value."/";	
			}	
		}

		$this->set('urlString', $urlString);

		$data = $this->paginate($query,[
			'page' => 1, 'limit' => 100,
			'order' => ['Skills.modified' => 'desc'],
			'paramType' => 'querystring'
			]);
		$this->set('resultData', $data);	
	}                                                                      
	
		/**	
		* @Date: 20-sep-2016
		* @Method : add	
		* @Purpose: This function is to add Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	


		function add() {       
			$this->set('title_for_layout','Add Skill');
			$this->pageTitle = "Add Skill";
			$this->set("pageTitle","Add Skill");
			$mode = "add";

			if($this->request->data){
				$skills = $this->Skills->newEntity($this->request->data());
				if(empty($skills->errors())){
					$this->Skills->save($skills);
					$this->Flash->success("Skill has been created successfully.",'layout_success');
					$this->setAction('list');
				}else{
					$this->set("errors", $skills->errors());
				}
			}     
		}


	/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function edit($id = null) {	
			$this->set('title_for_layout','Edit Skill');     
			$this->pageTitle = "Edit Skill";		
			$this->set("pageTitle","Edit Skill");		
			if($this->request->data){	
				$skill = $this->Skills->get($this->request->data['id']);
				$skills = $this->Skills->patchEntity($skill,$this->request->data());
				if(empty($skills->errors())) {
					$this->Skills->save($skill);
					$this->Flash->success("Skill has been updated successfully.");
					$this->setAction('list');
				}else{                                    
					$this->set("errors", $skills->errors());
				}	
			}else if(!empty($id)){ 
				$SkillsData = $this->Skills->get($id);
				$this->set('SkillsData',$SkillsData); 
				if(!$SkillsData){             
					$this->redirect(array('action' => 'list'));                 
				}                                                      
			}                                                          
		}    
		

		/**	
		* @Date: 20-sep-2016	
		* @Method : delete
		* @Purpose: This function is to delete Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function delete($id = null) {
			$employee = $this->Skills->get($id);
			if ($this->Skills->delete($employee)) {
				$this->Flash->success("Skill(s) deleted successfully.");
				$this->setAction('list');

			}

		}
	}
