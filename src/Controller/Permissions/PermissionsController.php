<?php 
namespace App\Controller\Permissions;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use App\Controller\Admin\UsersController;
use App\Controller\Skills\SkillsController;
use App\Controller\Employees\EmployeesController;
use App\Controller\Evaluations\EvaluationsController;
use App\Controller\Generics\GenericsController;
use App\Controller\Hardwares\HardwaresController;
use App\Controller\Milestones\MilestonesController;
use App\Controller\Component\ComponentController;
use App\Controller\CredentialAudits\CredentialAuditsController;
use App\Controller\Billings\BillingsController;
use App\Controller\Contacts\ContactsController;
use App\Controller\Resumes\ResumesController;
use App\Controller\StaticPages\Static_pagesController;
use App\Controller\Testimonials\TestimonialsController;
use App\Controller\Tickets\TicketsController;
use App\Controller\Permissions\PermissionsController;
use App\Controller\PaymentMethods\Payment_methodsController;
use App\Controller\Payments\PaymentsController;
use App\Controller\Reports\ReportsController;
use App\Controller\Profileaudits\ProfileauditsController;
use App\Controller\Sales\SalesController;
use App\Controller\LearningCenters\Learning_centersController;
use App\Controller\Leads\LeadsController;
use App\Controller\Credentials\CredentialsController;
use App\Controller\Attendances\AttendancesController;
use App\Controller\Appraisals\AppraisalsController;
use App\Controller\Projects\ProjectsController;

class PermissionsController extends AppController
{

	/******************************* START FUNCTIONS **************************/	
	#_________________________________________________________________________#   
	/**   
	* @Date: 28-Dec-2011
    * @Method : beforeFilter   
	* @Purpose: This function is called before any other function.    
	* @Param: none   
	* @Return: none   
	**/   

      function beforeFilter(Event $event) {
        // star to check permission
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
            $controller=$this->request->params['controller'];
            $action=$this->request->params['action'];
            $role = explode(",",$userSession['2']);

             // end code to check permissions
            parent::beforeFilter($event);
            Router::parseNamedParams($this->request);
          
          if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

            $this->checkUserSession();
          }else {
            $this->viewBuilder()->layout('new_layout_admin');
          }
       
          $session = $this->request->session();
          $userSession = $session->read("SESSION_ADMIN");
          
          if($userSession==''){
          return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
        }
        $this->set('common', $this->Common);
    }
    	/**   
	* @Date: 13-sept-2016
    * @Method : beforeFilter   
	* @Purpose: This function is called before any other function. it is like constructor   
	* @Param: none   
	* @Return: none   
	**/   


    public function initialize()
      {
        parent::initialize();
        $this->loadModel('StaticPages'); 
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations');
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Projects');
        $this->loadModel('Reports');
        $this->loadModel('Component');
        $this->loadModel('Static_pages');
        $this->loadModel('Resumes');
        $this->loadModel('Billings');
        $this->loadModel('Contacts');
        $this->loadModel('Sales');
        $this->loadModel('Settings');
        $this->loadModel('CredentialAudits');
        $this->loadModel('Modules');
        $this->loadModel('Profileaudits');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }

	 /**
     * @Date: 16-Dec-2014   
     * @Method : admin_setting    
     * @Purpose: This function is for setting of ip addresses and notifications from admin section.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
    function setting() {
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");

        $this->viewBuilder()->layout('new_layout_admin');
        $this->set("title_for_layout", "Admin Settings");
        $this->set("pageTitle", "Admin Settings");

		    $ip = $this->Settings->find('all', [
		      'fields'=>['key','value'],
		      'conditions' =>['Settings.key' => 'ip_address' ,'agency_id'=>$userSession[3]]
		      ]);

        $notify = $this->Settings->find('all', [
		        'fields'=>['key','value'],
		       'conditions' =>['Settings.key' => 'notification' ,'agency_id'=>$userSession[3]]
		        ]);

        $email_add = $this->Settings->find('all', [
		        'fields'=>['key','value'],
		     'conditions' =>['Settings.key' => 'email_address' ,'agency_id'=>$userSession[3]]
		        ]);

            $this->set('notify', $notify);
            $this->set('ip_add', $ip);
            $this->set('email_add', $email_add);
      if ($this->request->data) {
        
        $ipAdd = $this->request->data['ip_address'];
        $notice = $this->request->data['notification'];
        $mail_add = $this->request->data['mail_add'];
        $credentials = TableRegistry::get('Settings');
        	  
      if ($this->request->data) {
				$update = ['value' => $ipAdd];
				
        $result=$credentials->updateAll($update,["`key`" =>'ip_address','agency_id'=>$userSession[3]]);
      
        $update_not = ['value' => $notice];
				
       $credentials->updateAll($update_not,["`key`" =>'notification','agency_id'=>$userSession[3]]);
        
        $update_email = ['value' => $mail_add];
				
       $credentials->updateAll($update_email,["`key`" =>'email_address','agency_id'=>$userSession[3]]);
        if ($result) {
					 $this->Flash->success_new("Settings has been updated successfully");

          	$this->setAction('setting');
              }
          }        
       }  
    }

/**
     * @Date: 27 march 2017   
     * @Method : admin_add   
     * @ BY :Gurpreet Kaur 
     * @Purpose: This function is to add new permission from admin section.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */

    function permissionsdetails(){
       $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
       //'FIND_IN_SET(\''. $userSession[0] .'\',Projects.team_id)'
       $modules = $this->Modules->find('all',[
          'conditions' => ['Modules.parent_module !=' => 0, 'FIND_IN_SET(\''. $userSession[3] .'\',Modules.agency_id)']
        ])->contain(['Perms']);
       $filtered = [];
       foreach($modules as $module){
        if(in_array($userSession[3], explode(',', $module['agency_id']))){
          $filtered[] = $module;
          /*$ids[] = */
        }
       }
       /*pr($filtered); die;*/
       $this->set('modules',$filtered);
    }

    /**
     * @Date: 04-Dec-2014    
     * @Method : admin_list   
     * @Purpose: This function is to show all permission from admin section.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
    function list() {
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
      $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);  
      $this->set('title_for_layout','User Permission Listing');
      $this->set("pageTitle","User Permission Listing");   
      $this->set("search1", "");    
      $this->set("search2", "");  
      $criteria = ['is_deleted'=>'No'];

    //pr($criteria); die;
    // Delete user and its licences and orders(single/multiple)

    if(isset($this->request->data) || !empty($this->request->query)) {
        if(!empty($this->request->data['Permissions']['fieldName']) || isset($this->request->query['field'])){

            if(trim(isset($this->request->data['Permissions']['fieldName'])) != ""){
                $search1 = trim($this->request->data['Permissions']['fieldName']);
            }elseif(isset($this->request->query['field'])){
                $search1 = trim($this->request->query['field']);
            }
            $this->set("search1",$search1);
        }

        if(isset($this->request->data['Permissions']['value1']) || isset($this->request->data['Permissions']['value2']) || isset($this->request->query['value'])){
            if(isset($this->request->data['Permissions']['value1']) || isset($this->request->data['Permissions']['value2'])){
                $search2 = ($this->request->data['Permissions']['fieldName'] != "Permissions.status")?trim($this->request->data['Permissions']['value1']):$this->request->data['Permissions']['value2'];
            }elseif(isset($this->request->query['value'])){
                $search2 = trim($this->request->query['value']);
            }
            $this->set("search2",$search2);
        }
        //echo $search1."------".$search2;
        /* Searching starts from here */
    
        if(!empty($search1) && (!empty($search2) || $search1 == "Permissions.status")){

 $query  =    $this->Permissions->find('all' , [
                  'conditions' => ['is_deleted'=>'No',$search1 .' Like' =>'%'.$search2.'%' ],
               ]);
      $result = isset($query)? $query->toArray():array();
      $criteria = ['is_deleted'=>'No',$search1." LIKE '%".$search2."%'"]; 
      $session->write('SESSION_SEARCH', $criteria);
     
 }else{  
       $query  =  $this->Permissions->find('all');   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }

  
 $urlString = "/";
      if(isset($this->request->query)){                   
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }

  $this->set('urlString', $urlString);
      $urlString = "/";  
      if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
    $this->set('urlString', $urlString);  

$this->paginate = [           
/*    'fields' => [ 
    'id',
    'agency_name',
    'first_name',
    'last_name',
    'username'
    ''
],*/     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     

$data = $this->paginate('Permissions',['conditions' => $criteria]);  
$this->set('resultData', $data);  
$this->set('pagename',"Permissionlist");  

    }
 

     /**
     * @Date: 04-Dec-2014    
     * @Method : admin_add   
     * @ BY :Gurpreet Kaur 
     * @Purpose: This function is to add new permission from admin section.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
    function add() {
          $this->viewBuilder()->layout('new_layout_admin');
        $this->set('title_for_layout', 'Add Permission');
        $this->pageTitle = "Add Permission";
        
        $mode = "add";
        if($this->request->data){
            
        $controllerGo = $this->request->data['controller'];
       
        $actionGO = $this->request->data['action'];
        
        $aliasGo = $this->request->data['alias'];
        
        $user_idGO = $this->request->data['user_id'];

        $PermissionsValidations = $this->Permissions->newEntity($this->request->data());

        if(empty($PermissionsValidations->errors())){
        
        $this->Permissions->save($PermissionsValidations);
        $this->Flash->success_new("Permissions has been created successfully.");
        $this->setAction('list');
        }else{
        $this->set("errors", $PermissionsValidations->errors());
          }
        }
    }



    function setPermissions() {
        $this->viewBuilder()->layout('new_layout_admin');
        $this->set('title_for_layout', 'Add Permission');
        $this->pageTitle = "Add Permission";
        
        $ref = $_SERVER['DOCUMENT_ROOT']; // get root directory
        $base=$this->request->base; // get base of site
        $base_url=$ref.$base;  // concatinate both
        $dir = $base_url."/src/Controller";  // give path of controller 
        $files1 = scandir($dir); // scan dir and get folder name 
        unset($files1[0]);
        unset($files1[1]);
        $name=array();
        foreach($files1 as $key=>$v){
           $name[$v]=$v;
        }
        $this->set('folders',$name); // send folder name on view file
        // define in the header of name space of all the controller 

        if ($this->request->is('ajax')) { 

        if(isset($this->request['data']['value'])){

        $folder_name=$this->request['data']['value'];
            
            if($folder_name=="Skills"){
                $class = new SkillsController();
            }elseif($folder_name=="Resumes"){
                $class = new ResumesController();
            }elseif($folder_name=="StaticPages"){
                $class = new Static_pagesController();
            }elseif($folder_name=="Testimonials"){
                $class = new TestimonialsController();
            }elseif($folder_name=="Tickets"){
                $class = new TicketsController();
            }elseif($folder_name=="Permissions"){
                $class = new PermissionsController();
            }elseif($folder_name=="PaymentMethods"){
                $class = new Payment_methodsController();
            }elseif($folder_name=="LearningCenters"){
                $class = new Learning_centersController();
            }elseif($folder_name=="Leads"){
                $class = new LeadsController();
            }elseif($folder_name=="Credentials"){
                $class = new CredentialsController();
            }elseif($folder_name=="Attendances"){
                $class = new AttendancesController();
            }elseif($folder_name=="Appraisals"){
                $class = new AppraisalsController();
            }elseif($folder_name=="Admin"){
                $class = new UsersController();
            }elseif($folder_name=="Employees"){
                $class = new EmployeesController();
            }elseif($folder_name=="Billings"){
                $class = new BillingsController();
            }elseif($folder_name=="Contacts"){
                $class = new ContactsController();
            }/*elseif($folder_name=="Component"){
                $class = new ComponentController();
            }*/elseif($folder_name=="CredentialAudits"){
                $class = new CredentialAuditsController();
            }elseif($folder_name=="Evaluations"){
                $class = new EvaluationsController();
            }elseif($folder_name=="Generics"){
                $class = new GenericsController();
            }elseif($folder_name=="Hardwares"){
                $class = new HardwaresController();
            }elseif($folder_name=="Milestones"){
                $class = new MilestonesController();
            }elseif($folder_name=="Payments"){
                $class = new PaymentsController();
            }elseif($folder_name=="Profileaudits"){
                $class = new ProfileauditsController();
            }elseif($folder_name=="Projects"){
                $class = new ProjectsController();
            }elseif($folder_name=="Reports"){
                $class = new ReportsController();
            }elseif($folder_name=="Sales"){
                $class = new SalesController();
            }
            if(isset($class)){
                  $class_methods = get_class_methods($class); 
                unset($class_methods[0]);
                unset($class_methods[1]);
                $selected=$class_methods;
                foreach ($selected as $key =>$value) {
                  if ($value == 'viewOptions' || $value == 'createView' || $value == 'set' || $value == 'viewBuilder' || $value == 'requestAction' || $value == 'modelType' || $value == 'modelFactory' || $value == 'loadModel' || $value == '_setModelClass'|| $value == '_mergePropertyData' || $value == '_mergeVars' || $value == 'log' || $value == 'tableLocator' || $value == 'dispatchEvent'|| $value == 'eventManager'|| $value == 'afterFilter' || $value == 'beforeRedirect' || $value == 'isAction' || $value == 'paginate' || $value == 'referer' || $value == '_viewPath' || $value == 'render' || $value == 'setAction' || $value == 'redirect' || $value == 'shutdownProcess' || $value == 'startupProcess'  || $value == '_loadComponents' || $value == 'implementedEvents' || $value == '_mergeControllerVars' || $value == 'invokeAction' || $value == 'setRequest' || $value == '__set' || $value == '__get' || $value == 'components' || $value == '__construct' || $value == 'beforeRender'  || $value == '_mergeProperty'  || $value == 'loadComponent'){
                   unset($selected[$key]);
                  }
                }

                echo json_encode($selected);
                       exit;  
                   }else{
                    echo "empty";
                       exit;  
                   }
                 }
              } // ajax check request

        if($this->request->data){
              //  pr($this->request->data); die;
                $SelectControllerGO = strtolower($this->request->data['SelectController']);
                
                $controllerGO = $this->request->data['controller'];
                
                $credentials = TableRegistry::get('Permissions');
                
                $FindDAta = $credentials ->find()->where(['controller' => $SelectControllerGO,'action'=>$controllerGO])->first();
               // pr($FindDAta); die;
        if(count($FindDAta)>0){
                
                $id = $FindDAta->id;
                
                $entity = $this->Permissions->get($id);
                $this->request->data['user_id'] = $this->request->data['user_ids']/*implode(',', $this->request->data['user_ids'])*/;
                $entities = $this->Permissions->patchEntity($entity,$this->request->data());
        if(empty($entities->errors())){
                        $checkboxGO = $this->request->data['user_ids'];
                $user_id=$this->request->data['user_ids'];
                $update = ['user_id' => $user_id];
                
                $result=$credentials->updateAll($update,['id' => $id]);
                $message="Settings has been updated successfully";
                
                $this->Flash->success_new($message);
                //$this->setAction('setPermissions');
                    }else{
                // pr("Permissions error");
                //pr($entities->errors()); die;
                $this->set("errors", $entities->errors());
                    }

                }else{
                $entities = $this->Permissions->newEntity($this->request->data());
                
                if(empty($entities->errors())){
                
                $SelectControllerGO = strtolower($this->request->data['SelectController']);
                
                $controllerGO = $this->request->data['controller'];   
                
                $checkboxGO = $this->request->data['user_ids'];
                
                $user_id=implode(",",$checkboxGO); // convert into string with comma seperate
                $this->request->data['controller'] = $SelectControllerGO;
                $this->request->data['action'] =  $controllerGO;
                $this->request->data['alias'] =  $controllerGO;
                $this->request->data['user_id'] =  $user_id;

                $users = $this->Permissions->newEntity($this->request->data(), ['validate' => true]);
                        $this->Permissions->save($users);
                $message="Settings has been Inserted successfully";
                        $this->Flash->success_new($message);
                 }else{
                        $this->set("errors", $entities->errors());
                 } 
               }    
            }           
        }



   /**
     * @Date: 07-jan-2013
     * @Method : fetchUserBasedOnAction
     * @Purpose: This ajax function will retreive all actions for specific controller .
     * @Param: none
     * @Return: none 
     * */
    function fetchUserBasedOnAction() {
          $this->viewBuilder()->layout(false);
        
           if ($this->request->is('ajax')) { 

           if(isset($this->request['data']['actID'])){
                
                $function_name=$this->request['data']['actID'];
                $controller = $this->request['data']['controller'];
                $Permissions = TableRegistry::get('Permissions');
               $query = $Permissions->find()->where(['action' => $function_name,'controller'=> $controller])->first();
                   if(count($query)>0){
                    $Array=explode(",",$query->user_id);
                    $USers = TableRegistry::get('Users');
                    $get_user = $USers->find()->where(['id IN' => $Array ]);
                    //pr($get_user->toArray()); die;
                    if(isset($get_user)){
                     echo json_encode($get_user);
                    exit;
                    }else{
                    echo "<div align='center' style='color:red'>Please Select Role to Get Users!</div>";
                     exit;
                       }
                    }else{
                      echo "nouser";
                     exit;
                    }
                }
            }
          }

function fetchUserBasedOnRole() {

    if ($this->request->is('ajax')) {  

           if(isset($this->request['data']['role_id'])){
                //$roleId=rtrim($this->request['data']['role_id'],',');
                $roles = explode(",", $this->request['data']['role_id']);
                          $c = ['status' => 1];

                            $cond = "status='1' AND (";
                            $char = "";
                            $c['OR'] = [];
                            for ($i = 0; $i < count($roles); $i++) {
                              $c['OR'][] = ['FIND_IN_SET(\'' . $roles[$i] . '\',role_id)'];/*
                                $char = "OR ";
                                $cond.="FIND_IN_SET ($roles[$i],role_id) " . $char;*/
                            }
                $result = $this->Users->find('all', [
                'fields'=>['Users.id','Users.employee_code', 'Users.first_name', 'Users.last_name', 'Users.username', 'Users.status', 'Users.role_id'],
                'conditions' =>$c
                  ]);
    if(isset($result)){
                echo json_encode($result);
                   exit;
                }else{
                   echo "<div align='center' style='color:red'>Please Select Role to Get Users!</div>";
                     exit;
                }               
           }
       }
       
    }

function subactionscrone(){
  $arr = ["Resumes",'StaticPages','Testimonials','Tickets','Permissions','PaymentMethods','LearningCenters','Leads','Credentials','Attendances','Appraisals','Admin','Employees','Billings','Contacts','Component','CredentialAudits','Evaluations','Hardwares','Milestones','Payments','Profileaudits','Projects','Reports','Sales'];
  foreach($arr as $folder_name){ 
    if($folder_name=="Skills"){
                $class = new SkillsController();
            }elseif($folder_name=="Resumes"){
                $class = new ResumesController();
            }elseif($folder_name=="StaticPages"){
                $class = new Static_pagesController();
            }elseif($folder_name=="Testimonials"){
                $class = new TestimonialsController();
            }elseif($folder_name=="Tickets"){
                $class = new TicketsController();
            }elseif($folder_name=="Permissions"){
                $class = new PermissionsController();
            }elseif($folder_name=="PaymentMethods"){
                $class = new Payment_methodsController();
            }elseif($folder_name=="LearningCenters"){
                $class = new Learning_centersController();
            }elseif($folder_name=="Leads"){
                $class = new LeadsController();
            }elseif($folder_name=="Credentials"){
                $class = new CredentialsController();
            }elseif($folder_name=="Attendances"){
                $class = new AttendancesController();
            }elseif($folder_name=="Appraisals"){
                $class = new AppraisalsController();
            }elseif($folder_name=="Admin"){
                $class = new UsersController();
            }elseif($folder_name=="Employees"){
                $class = new EmployeesController();
            }elseif($folder_name=="Billings"){
                $class = new BillingsController();
            }elseif($folder_name=="Contacts"){
                $class = new ContactsController();
            }/*elseif($folder_name=="Component"){
                $class = new ComponentController();
            }*/elseif($folder_name=="CredentialAudits"){
                $class = new CredentialAuditsController();
            }elseif($folder_name=="Evaluations"){
                $class = new EvaluationsController();
            }elseif($folder_name=="Generics"){
                $class = new GenericsController();
            }elseif($folder_name=="Hardwares"){
                $class = new HardwaresController();
            }elseif($folder_name=="Milestones"){
                $class = new MilestonesController();
            }elseif($folder_name=="Payments"){
                $class = new PaymentsController();
            }elseif($folder_name=="Profileaudits"){
                $class = new ProfileauditsController();
            }elseif($folder_name=="Projects"){
                $class = new ProjectsController();
            }elseif($folder_name=="Reports"){
                $class = new ReportsController();
            }elseif($folder_name=="Sales"){
                $class = new SalesController();
            }elseif($folder_name=="Modules"){
                $class = new ModulesController();
            }
            if(isset($class)){
                  $class_methods = get_class_methods($class); 
                unset($class_methods[0]);
                unset($class_methods[1]);
                $selected=$class_methods;
                //pr($selected); die;
                foreach ($selected as $key =>$value) {
                  if ($value == 'viewOptions' || $value == 'createView' || $value == 'set' || $value == 'viewBuilder' || $value == 'requestAction' || $value == 'modelType' || $value == 'modelFactory' || $value == 'loadModel' || $value == '_setModelClass'|| $value == '_mergePropertyData' || $value == '_mergeVars' || $value == 'log' || $value == 'tableLocator' || $value == 'dispatchEvent'|| $value == 'eventManager'|| $value == 'afterFilter' || $value == 'beforeRedirect' || $value == 'isAction' || $value == 'paginate' || $value == 'referer' || $value == '_viewPath' || $value == 'render' || $value == 'setAction' || $value == 'redirect' || $value == 'shutdownProcess' || $value == 'startupProcess'  || $value == '_loadComponents' || $value == 'implementedEvents' || $value == '_mergeControllerVars' || $value == 'invokeAction' || $value == 'setRequest' || $value == '__set' || $value == '__get' || $value == 'components' || $value == '__construct' || $value == 'beforeRender'  || $value == '_mergeProperty'  || $value == 'loadComponent'){
                   unset($selected[$key]);
                  }
                }
                //echo json_encode($selected);
                $imp = implode(',', $selected);
                //pr($imp); die;
                $modules = $this->Modules->find('all')->all();
                foreach($modules as $module){
                    if($folder_name == 'Admin'){
                            if($module['controller'] == 'users'){
                              $module['related_actions'] = $imp;
                              $this->Modules->save($module);
                            }




                         /* if($module['controller'] == 'users'){
                              $modulesusers = $this->Modules->find('all',[
                                  'conditions' => ['controller' => 'users']
                                ])->all();
                             if(count($modulesusers) > 0) {
                               foreach($modulesusers as $moduleuser){
                                  $data['related_actions'] = $imp;
                                  $record = $this->Modules->get($moduleuser->id);
                                  $this->Modules->patchEntity($record,$data);
                                  $this->Modules->save($record);
                              }
                             }
                          } */                    
                    } else if($folder_name == 'PaymentMethods'){
                          if($module['controller'] == 'payment_methods'){
                              $module['related_actions'] = $imp;
                              $this->Modules->save($module);
                          }                     
                    } else {
                        if($module['controller'] == strtolower($folder_name) ){
                            $module['related_actions'] = $imp;
                            $this->Modules->save($module);
                        }
                    }



                }


              }
          }
          die("done");
   }

   function including($id){
      $session = $this->request->session();
      $this->viewBuilder()->layout(false);
      $userSession = $session->read("SESSION_ADMIN");
      $result=  $this->Users->find("list", [
                          'keyField' => 'id',
                          'valueField' => 'id',
                          'conditions' => ['status'=>'1','agency_id' => $userSession[3]],
                          'fields' => ['id','username']
                            ]);
      $allusersofcurrentagency = isset($result)? $result->toArray():array();  
      if($this->request->data){
          $module = $this->Modules->get($this->request->data['id']);
        $except = explode(',', $module['including_user_id']);
        if((!isset($this->request->data['including_user_id']) || count($this->request->data['including_user_id']) == 0) && count($except) == 0){
          $this->request->data['including_user_id'] = [];
        } else {
          if( !isset($this->request->data['including_user_id']) ){
              // if all users from exeption list are deslected /no new user added just removed all
              if(count($except) > 0){
                  foreach($except as $userid){
                    if(in_array($userid, array_keys($allusersofcurrentagency))){
                         $a=array_search($userid, $except);
                          unset($except[$a]);
                    }
                  }
              } 
               $this->request->data['including_user_id'] = $except;
          } else {
            // if there are some users added or removed from the list
           foreach($except as $userid){
                    if(in_array($userid, array_keys($allusersofcurrentagency)) && !in_array($userid, $this->request->data['including_user_id'] )){
                         $a=array_search($userid, $except);
                          unset($except[$a]);
                    }
                  }
          foreach($this->request->data['including_user_id'] as $userid){
            if(!in_array($userid, $except)){
              array_push($except, $userid);
            }
          }
          $this->request->data['including_user_id'] = $except;
          }
      }
        /*pr($this->request->data); die;*/
        $datatosave = ['including_user_id' => implode(',', $this->request->data['including_user_id'])/*,'except_user_id' => implode(',',$exclude)*/];
        $moduleData = $this->Modules->updateAll($datatosave , ['id' => $this->request->data['id']]);
        $this->set('status','done');
      }
      if(!empty($id)){
        $moduleData = $this->Modules->find('all',[
        'conditions' => ['id' => $id]
        ])->first();
        if(count($moduleData) > 0){
           $otherusers = $this->Common->getusersexceptrole($moduleData->role_id,$userSession);  
           $this->set('options',$otherusers);
           $cont_cats = $moduleData->including_user_id;
           $this->set('cont_cats',explode(',', $cont_cats) );
           $this->set('moduleData',$moduleData);
        }
      }
      
   }
function except($id){
      $session = $this->request->session();
      $this->viewBuilder()->layout(false);
      $userSession = $session->read("SESSION_ADMIN");
      $result=  $this->Users->find("list", [
                                  'keyField' => 'id',
                                  'valueField' => 'id',
                                  'conditions' => ['status'=>'1','agency_id' => $userSession[3]],
                                  'fields' => ['id','username']
                                    ]);
             $allusersofcurrentagency = isset($result)? $result->toArray():array();  
      if($this->request->data){
        $module = $this->Modules->get($this->request->data['id']);
        $except = explode(',', $module['except_user_id']);
        if((!isset($this->request->data['except_user_id']) || count($this->request->data['except_user_id']) == 0) && count($except) == 0){
          $this->request->data['except_user_id'] = [];
        } else {

          if(!isset($this->request->data['except_user_id']) /*&& count($this->request->data['except_user_id']) == 0*/   ){
              // if all users from exeption list are deslected /no new user added just removed all
              if(count($except) > 0){
                  foreach($except as $userid){
                    if(in_array($userid, array_keys($allusersofcurrentagency))){
                         $a=array_search($userid, $except);
                          unset($except[$a]);
                    }
                  }
              } 
               $this->request->data['except_user_id'] = $except;
          } else {
           
            // if there are some users added or removed from the list
           foreach($except as $userid){
                    if(in_array($userid, array_keys($allusersofcurrentagency)) && !in_array($userid, $this->request->data['except_user_id'] )){
                         $a=array_search($userid, $except);
                          unset($except[$a]);
                    }
                  }
          foreach($this->request->data['except_user_id'] as $userid){
            if(!in_array($userid, $except)){
              array_push($except, $userid);
            }
          }
          $this->request->data['except_user_id'] = $except;
          }


           //$this->request->data['except_user_id'] = array_unique(array_merge($except,[])); 
        }
        $moduleData = $this->Modules->updateAll(['except_user_id' => implode(',', $this->request->data['except_user_id'])],['id' => $this->request->data['id']]);
        $this->set('status','done');
      }
      if(!empty($id)){

        $moduleData = $this->Modules->find('all',[
        'conditions' => ['id' => $id]
        ])->first();
        if(count($moduleData) > 0){
           $otherusers = $this->Common->getusersasrole($moduleData->role_id,$userSession);  
           $this->set('options',$otherusers);
           $cont_cats = $moduleData->except_user_id;
           $this->set('cont_cats',explode(',', $cont_cats) );
           $this->set('moduleData',$moduleData);
        }
      }
      
   }

public function billingscrone(){
  $billings = $this->Billings->find('all',[
      'conditions' => ['agency_id' => 0]
    ])->all();
  foreach($billings as $billing){
    $projectdata = $this->Projects->get($billing['project_id']);
    $data['agency_id'] = $projectdata['agency_id'];
    $this->Billings->updateAll($data,['id' => $billing['id']]);
  }
  die("done");
}




function edit($id = null) {
       $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Edit Permission');     
            $this->pageTitle = "Edit Role";    
            $this->set("pageTitle","Edit Role");   
$arr = ['Tags'=> 'Tags','Agencies' => 'Agencies',"Resumes"=> "Resumes",'StaticPages'=> 'StaticPages','Testimonials'=>'Testimonials','Tickets'=>'Tickets','Permissions'=>'Permissions','PaymentMethods'=>'PaymentMethods','LearningCenters'=>'LearningCenters','Leads'=>'Leads','Credentials'=>'Credentials','Attendances'=>'Attendances','Appraisals'=>'Appraisals','Admin'=>'Admin','Employees'=>'Employees','Billings'=>'Billings','Contacts'=>'Contacts','Component'=>'Component','CredentialAudits'=>'CredentialAudits','Evaluations'=>'Evaluations','Hardwares'=>'Hardwares','Milestones'=>'Milestones','Payments'=>'Payments','Profileaudits'=>'Profileaudits','Projects'=>'Projects','Reports'=>'Reports','Sales'=>'Sales','Modules' => 'Modules','Roles' => 'Roles'];
     $mode = "add";
      if($this->request->data){
     
        if(in_array($this->request->data['controller'], array_keys($arr))){
          $controller = $this->request->data['controller'];
          $action = $this->request->data['action'];
          $thedata = $this->Common->getprefix($this->request->data['controller']);
          $this->request->data['controller'] = $thedata['controller'];
          $this->request->data['prefix'] = $thedata['prefix'];
          $validateca = $this->Permissions->find('all',[
            'conditions' => ['controller' => $this->request->data['controller'] , 'action' => $this->request->data['action'] ]
          ])->first();
        } else{
          $this->request->data['controller'] = "";
          $this->request->data['prefix'] = "";  
        }
        $this->request->data['parent_module'] = 0;


     $role = $this->Permissions->get($this->request->data['id']);
      $roles = $this->Permissions->patchEntity($role,$this->request->data()); 
        if(count($validateca) > 0){
            $errors =[];
            $error['controller'] = ['_combination' => "Combination of This controller and action aleardy exists"];
            $errorarr = array_merge($roles->errors(),$error);
          }
        if(empty($errorarr)){
          $this->Permissions->save($role);
                  $this->Flash->success_new("Permission has been updated successfully.");
                  $this->redirect(array('action' => 'list'));  
        }else{
          $this->set("errors", $errorarr);
          $this->set('controller',$controller);
        }

      }else if(!empty($id)){ 
          $RolesData = $this->Permissions->get($id);
          $controller = $this->Common->getpostfix($RolesData->controller);
          $action = $this->Common->getpostfix($RolesData->action);
          $this->set('RolesData',$RolesData); 
          $this->set('controller',$controller); 
          $this->set('action',$RolesData->action); 
            if(!$RolesData){             
              $this->redirect(array('action' => 'list'));                 
          }                                                      
      }
      $this->set('controllers',$arr);


/*

      if($this->request->data){ 
                 $role = $this->Permissions->get($this->request->data['id']);
                $roles = $this->Permissions->patchEntity($role,$this->request->data()); 
                if(empty($roles->errors())) {
                  $this->Permissions->save($role);
                  $this->Flash->success_new("Permission has been updated successfully.");
                  $this->redirect(array('action' => 'list'));  
              }else{                                    
                 $this->set("errors", $roles->errors());
             } 
         }else if(!empty($id)){ 
            $RolesData = $this->Permissions->get($id);
            $controller=$RolesData->controller;
            $this->set('RolesData',$RolesData); 
            if(!$RolesData){             
              $this->redirect(array('action' => 'list'));                 
          }                                                      
      }

*/





  }  




    function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
      $role = $this->Permissions->get($id);
      $ROLEID=$role->id;
      $modules = TableRegistry::get('Modules');
      $query = $modules->find()
      ->where([
        'OR' => [['FIND_IN_SET(\''. $ROLEID .'\',Modules.other_aliases)'],['FIND_IN_SET(\''. $ROLEID .'\',Modules.default_alias)']],
        ]);
      $results=isset($query)? $query->toArray():array();

      if(count($results)>0){
        echo json_encode(['status'=> 'exists']); die;
      }else{
        $credentials = TableRegistry::get('Permissions');
        $update=['is_deleted'=>'Yes'];
        $credentials->updateAll($update,['id' => $ROLEID]);
        echo json_encode(['status'=> 'success']); die;
      }

    } 




} //last

