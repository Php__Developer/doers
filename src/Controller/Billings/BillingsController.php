<?php 

namespace App\Controller\Billings;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
class BillingsController extends AppController {




	/******************************* START FUNCTIONS **************************/


	#_________________________________________________________________________#

    /**                                                           
    * @Date: Nov,2016                                        
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/
    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
    	Router::parseNamedParams($this->request);
    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {
    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('layout_admin');
    	}
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }

        #_________________________________________________________________________#

    /**                         
    * @Date: Nov,2016        
    * @Method : initialize      
    * @Purpose: This function is to show list of Billings in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/


    public function initialize()
    {
    	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('Projects');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Payments');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
  		$this->loadModel('Leads'); // for importing Model
  		$this->loadModel('Billings'); // for importing Model

  	}

  /**                         
    * @Date: Nov,2016        
    * @Method : billingslist      
    * @Purpose: This function is to show list of Billings in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

  function billingslist(){
  	$this->viewBuilder()->layout('new_layout_admin');
  	$this->set('title_for_layout','Billings Listing');      
  	$this->set("pageTitle","Billings Listing");             
  	$this->set("search1", "");                                 
  	$this->set("search2", "");                                 
      $criteria = "1"; //All Searching
      $session = $this->request->session();
      $session->delete('SESSION_SEARCH');
      $urlString = "/";
      if (isset($this->request->query)) {
      	$completeUrl = array();
      	if (!empty($this->request->query['page']))
      		$completeUrl['page'] = $this->request->query['page'];
      	if (!empty($this->request->query['sort']))
      		$completeUrl['sort'] = $this->request->query['sort'];
      	if (!empty($this->request->query['direction']))
      		$completeUrl['direction'] = $this->request->query['direction'];
      	if (!empty($search1))
      		$completeUrl['field'] = $search1;
      	if (isset($search2))
      		$completeUrl['value'] = $search2;
      	foreach ($completeUrl as $key => $value) {
      		$urlString.= $key . ":" . $value . "/";
      	}
      }
      $this->set('urlString', $urlString); 
      if(isset($this->request->data['publish'])){      
      	$setValue = 1;
      	$messageStr = "Selected billings(s) have been activated.";
      }elseif(isset($this->request->data['unpublish'])){
      	$setValue = 0;           
      	$messageStr = "Selected billings(s) have been deactivated.";            
      }                     
      if(isset($this->request->data['publish']) || isset($this->request->data['unpublish'])) {
      	if(isset($this->request->data['IDs'])){          
      		$saveString = implode("','",$this->request->data['IDs']);      
      	}
      	if($saveString != ""){   
      		$credentials = TableRegistry::get('Billings');
      		$update = ['status' => $setValue];
      		$credentials->updateAll($update,['id IN' => $this->request->data['IDs']]);
      		$this->Flash->success_new($messageStr);
      		return $this->redirect( ['prefix'=>'billings' ,'controller' => 'billings', 'action' => 'billingslist']);
      	}
      }                                               
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
      $userID = $userSession[0];
      $usersRes  = 	$this->Projects->find("all" , [
      	'conditions' => [

      	'OR' => [['engineeringuser' => $userID], ['salesbackenduser' => $userID]  , ['engineeringuser' => $userID] ,[ 'pm_id' => $userID],
      	['salesfrontuser' => $userID]   ,["FIND_IN_SET($userID,team_id)"]     ],
      	'AND'=>[['status'=>'current']],

      	],

      	]);
 // get by order is pending in this list order= status current 
      $projectIdStr = "";
      foreach($usersRes as $projects){
      	$projectIdStr[] = $projects['id'];
      } 
      $projectIdStr = (!empty($projectIdStr)) ? join(",",$projectIdStr) : $projectIdStr = "0" ; 
      if(date('N')==1)
      	$monday = date('Y-m-d',time()+(1- date('w'))*24*3600); 
      else 
      	$monday = date('Y-m-d',strtotime('last monday'));

      $query  = $this->Billings->find("all" , [
      	'conditions' => [

      	"project_id in ($projectIdStr)",
      	'AND'=>[['week_start'=>"$monday"]],

      	],
      	])->contain(['Projects']); 

      $data = $this->paginate($query,[
      	'page' => 1, 'limit' => 50,
      	'order' => ['id' => 'desc'],
      	'paramType' => 'querystring'
      	]);
      $this->set('resultData', $data);
  }




	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : admin_add	
		* @Purpose: This function is to add billing details from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	


		function admin_add() {       
			$this->set('title_for_layout','Add Billing');
			$this->pageTitle = "Add Billing";
			$this->set("pageTitle","Add Billing");
			$mode = "add";
			if($this->request->data){
					isset($this->request->data['Billing']['week_start']) && !empty($this->request->data['Billing']['week_start'])?$this->request->data['Billing']['week_start'] = date("Y-m-d",strtotime($this->request->data['Billing']['week_start'])) : ''; 	//date to sql format
					$this->Billing->set($this->request->data['Billing']);
					$isValidated=$this->Billing->validates();
					if($isValidated){
					   $data=$this->request->data['Billing'];//post
					   $this->Billing->save($this->request->data['Billing'], array('validate'=>false));                                         
					   $this->Session->setFlash("Billing has been created successfully.",'layout_success');       
					   $this->redirect('billingslist');                
					}else{                                      
						$this->set("Error",$this->Billing->invalidFields());
					}
				}                                                                                                                                    
			}

	#___________________________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : quickadd	
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	

		function quickadd($id = null) {
			$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
			$session = $this->request->session();
			$this->set('session',$session);	
			$userSession = $session->read("SESSION_ADMIN");
			$userID = $userSession[0];
			$this->set('userSession',$userSession);
			$this->viewBuilder()->layout(false);
			$verify = $this->Billings->find('all',[
				'conditions'=>['verified'=>0,'Projects.id'=> $id] 
				])
			->contain(['Projects'])->toArray();
			$this->set('verified',$verify);
			if(!empty($id)){ 
				$res = $this->Projects->find('all',['conditions'=>['Projects.id'=>$id] ])->first();
				$this->request->data['Billing']['project_name'] =$res['project_name']; 
				$this->request->data['Billing']['project_id'] = $id ;
			}
			if($this->request->is('ajax')==true){
				$data['Billing']['project_id']     = (is_numeric($this->request->data['project_id']) ) ? $this->request->data['project_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['project_id']));
				$data['Billing']['user_id']        =$this->request->data['user_id']; 
				$data['Billing']['week_start']     = date("Y-m-d",strtotime($this->request->data['week_start'])); 
				$data['Billing']['max_limit']      =$this->request->data['max_limit'];
				$data['Billing']['currency']       =$this->request->data['currency'];
				$data['Billing']['todo_hours']     =$this->request->data['todo_hours']; 
				$data['Billing']['mon_hour']       =$this->request->data['mon_hour']; 
				$data['Billing']['tue_hour']       =$this->request->data['tue_hour'];
				$data['Billing']['wed_hour']       =$this->request->data['wed_hour']; 
				$data['Billing']['thur_hour']      =$this->request->data['thur_hour'];
				$data['Billing']['fri_hour']       =$this->request->data['fri_hour'];
				$data['Billing']['sat_hour']       =$this->request->data['sat_hour']; 
				$data['Billing']['sun_hour']       =$this->request->data['sun_hour']; 
				$data['Billing']['notes']          =$this->request->data['notes'];
				$data['Billing']['week_end']       = date("Y-m-d",strtotime($this->request->data['week_start']));
				$data['Billing']['agency_id']      = $userSession[3];
				/*$resData = $this->Billings->find('all',[
					'conditions'=>['Projects.id'=> $id,'week_start' => date("Y-m-d",strtotime($this->request->data['week_start']))
					] ] )
				->contain(['Projects'])->count();*/
				//pr($id); 
				//pr($this->request->data); die;
				//pr(date_create($this->request->data['week_start'])->format('Y-m-d H:i:s') );
				$resData = $this->Billings->find('all',[
					'conditions'=>['project_id'=> $this->request->data['project_id'],'week_start' => date_create($this->request->data['week_start'])->format('Y-m-d H:i:s')
					] ] )->count();
				//pr($resData); die;
				if($resData >= 1){
					echo "error"; die;
				}else{
					$Billings  = $this->Billings->newEntity($data['Billing']); 
					$this->Billings->save($Billings);

					if($Billings['Billing']){
						$this->Flash->error("Some error ocuured.");		    
						echo "1"; die;
					}else{
						$this->Flash->success_new("Billing has been created successfully.");		    
						echo "0"; die;
					}    	
				}
			}	                                             
		}    

		#_________________________________________________________________________#
			/**	
		* @Date: Nov,2016	
		* @Method : quickview	
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function quickview($id = null) {
			$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
			$session = $this->request->session();
			$this->set('session',$session);	
			$this->viewBuilder()->layout(false);
			if(!empty($id)){     
				$result = $this->Billings->find('all',[
					'conditions'=>['Projects.id'=> $id] 
					])
				->order(['Billings.modified' => 'desc'])
				->contain(['Projects'])->toArray();
				//pr($result); die;
			} 
			$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);	
			$this->set('currentWeekMonday',$currentWeekMonday);
			$verifydata = $this->Billings->find('all',[
				'conditions'=>['Projects.id'=> $id] 
				])
			->contain(['Projects']);

			$verify= isset($verifydata)? $verifydata->toArray():array();
			$verifyStatus=array();
			$verificationWeek=array();
			foreach($verify as $verf){
				$verifyStatus[$verf['id']]=$verf['verified'];
				$verificationWeek[$verf['id']]=date("Y-m-d",strtotime($verf['week_start'])); 
			}
			$this->set('verified',$verifyStatus);			
			$this->set('verificationWeek',$verificationWeek);	
			if($this->request->is('ajax')){
				if(isset($this->request->data['key']) && $this->request->data['key'] == 'delete'){
					$res = $this->Billings->deleteAll(["id IN" => [$this->request->data['id'] ] ]);
					if($res==1)
						echo 1;
					else
						echo 0;
					exit;
				}
				if(isset($this->request->data['key']) && $this->request->data['key'] == 'unverify'){
					$res = $this->Billings->updateAll(['verified'=>0],
						['id'=>$this->request->data['id']] );
					$billing_data = $this->Billings->find('all',[
						'fields'=>['currency','mon_hour','tue_hour','wed_hour','thur_hour','fri_hour','sat_hour','sun_hour','project_id','user_id','id','currency'],
						'conditions'=>['id'=>$this->request->data['id']]
						])->first()->toArray();

					$billing_sum = $billing_data['mon_hour']+$billing_data['tue_hour']+$billing_data['wed_hour']
					+$billing_data['thur_hour']+$billing_data['fri_hour']+$billing_data['sat_hour']+$billing_data['sun_hour'];
					$data=$this->Projects->find('all',[
						'fields'=>['hourly_rate','notes','created','modified','id'],
						'conditions'=>['id' => $billing_data['project_id'] ] 
						])->first();
					$project_data=	 isset($data)? $data->toArray():array();
					$datapay=$this->Payments->find('list',[
						'conditions'=>['billing_id' =>$billing_data['id'] ] ]);
					$payments_data=	 isset($datapay)? $datapay->toArray():array();
					if(count($payments_data)>0){
						$ids             =  array_values($payments_data);
						$sessionAry      =  $session->read("SESSION_ADMIN");
						$this->Payments->deleteAll(['id IN' => $ids]);
					}
					if($res==1)
						echo 1;
					else
						echo 0;
					exit;

				}else{
					$sessionAry  = $session->read("SESSION_ADMIN");
					$result = $this->Billings->updateAll( [
						'max_limit'  => ($this->request->data['max_limit']),
						'todo_hours' => ($this->request->data['todo_hours']),
						'mon_hour'   => ($this->request->data['mon_hour']),
						'tue_hour'   => ($this->request->data['tue_hour']),
						'wed_hour'   => ($this->request->data['wed_hour']),
						'thur_hour'  => ($this->request->data['thur_hour']),
						'fri_hour'   => ($this->request->data['fri_hour']),
						'sat_hour'   => ($this->request->data['sat_hour']),
						'sun_hour'   => ($this->request->data['sun_hour']),
						'notes'      => ($this->request->data['notes']),
						'modified'   => (date("Y-m-d H:i:s")),
						],
						['id ' => ($this->request->data['id'])]
						);
						$billing_data = $this->Billings->find('all',[
						'fields'=>['currency','mon_hour','tue_hour','wed_hour','thur_hour','fri_hour','sat_hour','sun_hour','project_id','user_id','id','currency'],
						'conditions'=>['id'=>$this->request->data['id']]
						])->first()->toArray();

					$billing_sum = $billing_data['mon_hour']+$billing_data['tue_hour']+$billing_data['wed_hour']
					+$billing_data['thur_hour']+$billing_data['fri_hour']+$billing_data['sat_hour']+$billing_data['sun_hour'];
					$data=$this->Projects->find('all',[
						'fields'=>['hourly_rate','notes','created','modified','id'],
						'conditions'=>['id' => $billing_data['project_id'] ] 
						])->first();
					$project_data=	 isset($data)? $data->toArray():array();
					$datapay=$this->Payments->find('list',[
						'conditions'=>['billing_id' =>$billing_data['id'] ] ]);
					$payments_data=	 isset($datapay)? $datapay->toArray():array();
					if(count($payments_data)>0){
						$ids             =  array_values($payments_data);
						$sessionAry      =  $session->read("SESSION_ADMIN");
						$sessionAry      =  $session->read("SESSION_ADMIN");
					$total_payment   =  $project_data['hourly_rate']*$billing_sum;
					$payment_data['project_id']     =  $project_data['id'];
					$payment_data['responsible_id'] =  $billing_data['user_id'];
					$payment_data['entry_id']       =  $billing_data['user_id'];
					$payment_data['billing_id']     =  $billing_data['id'];
					$payment_data['payment_date']   =  date('Y-m-d');
					$payment_data['amount']         =  $total_payment;
					$payment_data['tds_amount']     =  0;
					$payment_data['currency']       =  $billing_data['currency'];
					$payment_data['note']           =  "Received on Upwork";
					$payment_data['modified']       =  date('Y-m-d H:i:s');
					$this->Payments->updateAll($payment_data,['id IN' => $ids]);
					}
					if($result)
						echo 1; 
					else
						echo 0; 
					die;
				}
			}
			$this->set('resultData',$result);      
		}
#___________________________________________________________________________________________#
		/**	
		* @Date:  23-09-2016
		* @Method : verifybilling	
		* @Purpose: This function is to verify last billings.	
		* @Param: $id	
		* @Return: none 
		* @who :Maninder	
		* @Return: none 
		**/	
		function verifybilling($id = null) {	
			$this->viewBuilder()->layout(false);
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);	
			$result=$this->Billings->find('all',array(
				'fields' => array(
					'Billings.id',
					'Billings.week_start',
					'Billings.max_limit',
					'Billings.todo_hours',
					'Billings.mon_hour',
					'Billings.tue_hour',
					'Billings.wed_hour',
					'Billings.thur_hour',
					'Billings.fri_hour',
					'Billings.sat_hour',
					'Billings.sun_hour',
					'Billings.notes',
					'Projects.project_name'
					),
				'conditions' =>array('Billings.agency_id' => $userSession[3],'Billings.verified'=>0,'AND' => array('Billings.week_start !=' => $currentWeekMonday.' 00:00:00'))
				))->contain(['Projects'])->toArray();
			$session = $this->request->session();
			$this->set('session',$session);
			if($this->request->is('ajax'))
			{
				if(isset($this->request->data['key']) && $this->request->data['key'] == 'verify'){
					$res = $this->Billings->updateAll(['verified'=>1],
						['id'=>$this->request->data['id']] );
					$billing_data = $this->Billings->find('all',[
						'fields'=>['verified','currency','mon_hour','tue_hour','wed_hour','thur_hour','fri_hour','sat_hour','sun_hour','project_id','user_id','id','currency'],
						'conditions'=>['id'=>$this->request->data['id']]
						])->first()->toArray();
					$billing_sum = $billing_data['mon_hour']+$billing_data['tue_hour']+$billing_data['wed_hour']
					+$billing_data['thur_hour']+$billing_data['fri_hour']+$billing_data['sat_hour']+$billing_data['sun_hour'];
					$data=$this->Projects->find('all',[
						'conditions'=>['id' => $billing_data['project_id'] ] 
						])->first();
					$project_data=	 isset($data)? $data->toArray():array();
					$datapay=$this->Payments->find('list',[
						'fields'=>['id'],	
						'conditions'=>['billing_id' =>$billing_data['id'] ] ]);
					$payments_data=	 isset($datapay)? $datapay->toArray():array();
					$sessionAry      =  $session->read("SESSION_ADMIN");
					$total_payment   =  $project_data['hourly_rate']*$billing_sum;
					$payment_data['project_id']     =  $project_data['id'];
					$payment_data['responsible_id'] =  $billing_data['user_id'];
					$payment_data['entry_id']       =  $billing_data['user_id'];
					$payment_data['billing_id']     =  $billing_data['id'];
					$payment_data['payment_date']   =  date('Y-m-d');
					$payment_data['amount']         =  $total_payment;
					$payment_data['tds_amount']     =  0;
					$payment_data['currency']       =  $billing_data['currency'];
					$payment_data['note']           =  "Received on Upwork";
					$payment_data['modified']       =  date('Y-m-d H:i:s');
					if(count($payments_data)>0){
						$ids             =  array_values($payments_data);
						
						$this->Payments->updateAll($payment_data,['id IN' => $ids]);
					} else{
					$projects = $this->Payments->newEntity($payment_data,['validate' => false]);
					$this->Payments->save($projects);
					}
					if($res==1)
						echo 1;
					else
						echo 0;
					exit;
				}
			}
			$this->set('resultData',$result);   
		}
		
#_________________________________________________________________________#
	/**	
		* @Date: 23-09-2016
		* @Method : admin_edit
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function admin_edit($id = null) {	
			$this->set('title_for_layout','Edit Billing');     
			$this->pageTitle = "Edit Billing";		
			$this->set("pageTitle","Edit Billing");		
			if($this->request->data){	
				$this->Billing->set($this->request->data['Billing']);		
				$isValidated=$this->Billing->validates();		 
				if($isValidated){			
					$this->request->data['Billing']['week_start'] = date("Y-m-d",strtotime($this->request->data['Billing']['week_start'])); //date to sql format
					$data=$this->request->data['Billing'];   //post	
					$this->Billing->save($this->request->data['Billing'], array('validate'=>false));          
					$this->Session->setFlash("Billing has been updated successfully.",'layout_success');		                            
					$this->redirect('billingslist');             
				}else{                                    
					$this->set("Error",$this->Billing->invalidFields());	
				}	
			}else if(!empty($id)){       	
				$this->request->data = $this->Billing->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));	 
				$this->request->data['Billing']['week_start'] = date("d-m-Y",strtotime($this->request->data['Billing']['week_start']));  
				if(!$this->request->data){             
					$this->redirect(array('action' => 'billingslist'));                 
				}                                                      
			}                                                          
		}    
		
				#_________________________________________________________________________#
		/**	
		* @Date: 23-09-2016	
		* @Method : admin_delete
		* @Purpose: This function is to delete projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function admin_delete() {	
			if(isset($this->params['form']['delete']) || isset($this->params['pass'][0]) == "delete"){		
				if(isset($this->params['form']['IDs'])){	
					$deleteString = implode("','",$this->params['form']['IDs']);		
				}elseif(isset($this->params['pass'][0])){
					$deleteString = $this->params['pass'][0];	
				}		
				if(!empty($deleteString)){	
					$this->Billing->deleteAll("Billing.id in ('".$deleteString."')");	
					$this->Session->setFlash("<div class='success-message flash notice'>Billing(s) deleted successfully.</div>", 'layout_success');	
					$this->redirect('billingslist');	
				}
			}
		}
		
	#_________________________________________________________________________#  
	/**                       
	* @Date: 23-09-2016    
	* @Method : billingreportweek      
    * @Purpose: This function is to show the Pie Chart of no of Bids by Users.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function billingreportweek(){
		$this->viewBuilder()->layout(false);
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		if(isset($this->request->data['start_date'])){ 
			$start_date = $this->request->data['start_date'];
			$date = strtotime($start_date);
			$date = strtotime("next sunday", $date);
			$end_date=date('Y-m-d',$date);
			$todate = str_replace("/","-",$this->request->data['start_date']);
			$create=date_create($todate);
			$start_date = $create->format('Y-m-d');
			$create=date_create($end_date);
			$end_date = $create->format('Y-m-d');

		} else {
			$start_date = date('Y-m-d', time() + ( 1 - date('w')) * 24 * 3600);
			if (date('N') == 7) {
				$start_date = date('Y-m-d', strtotime("last monday"));

			}
			$curr_sun = strtotime($start_date);
			$curr_sun = strtotime("next sunday", $curr_sun);
			$end_date = date('Y-m-d', $curr_sun);
		}
		//$this->set('toDate',$toDate);
		$cond = ['Billings.week_start >=' => $start_date, 'Billings.week_start <=' => $end_date,'Billings.agency_id' => $userSession[3]];
		$this->set('fromDate',$start_date);
		$resultData = $this->Billings->find()
		->select(['Billings.id', 'Billings.week_start', 'Billings.max_limit',
			'Billings.todo_hours', 'Billings.mon_hour', 'Billings.tue_hour',
			'Billings.wed_hour', 'Billings.thur_hour', 'Billings.fri_hour', 'Billings.sat_hour',
			'Billings.sun_hour', 'Projects.project_name'
			])
		->group('Billings.project_id')
		->where($cond)->contain(['Projects'])->toArray();
		$this->set('resultData', $resultData);

	}
}
