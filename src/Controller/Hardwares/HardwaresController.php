<?php


namespace App\Controller\Hardwares;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Inflector;

class HardwaresController extends AppController{   


	/*********************** START FUNCTIONS **************************/	
	#_________________________________________________________________________#   
	/**   
	* @Date: 21-sep-2016
    * @Method : beforeFilter   
	* @Purpose: This function is called before any other function.    
	* @Param: none   
	* @Return: none   
	**/   
	public function beforeFilter(Event $event) 	{
		parent::beforeFilter($event);

	// star to check permission
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");

    // end code to check permissions
		if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin" ){

			$this->checkUserSession();
		}else{
			$this->viewBuilder()->layout('layout_admin');

		}
		Router::parseNamedParams($this->request);
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		if($userSession==''){
			return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
		}
		$this->set('common',$this->Common);
		$this->set("title_for_layout","ERP : Vlogic Labs - Relying on Commitment");


	}
	/**   
	* @Date: 21-sep-2016
    * @Method : initialize   
	* @Purpose: This function is the default function of the controller    
	* @Param: none   
	* @Return: none   
	**/   
	public function initialize()
	{
		parent::initialize();
        $this->loadModel('Tickets'); // for importing Model
$this->loadModel('Projects'); // for importing Model
$this->loadModel('Users');
$this->loadModel('Hardware');
$this->loadComponent('RequestHandler');
$this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Testimonials');
        $this->loadModel('Contacts');

    }
	#_________________________________________________________________________#   
	/**  
	* @Date: 21-sep-2016   
	* @Method : index   
	* @Purpose: This function is the default function of the controller  
	* @Param: none 
	* @Return: none     
	**/	
	function index() {
		$this->render('login');
		if($this->Session->read("SESSION_USER") != ""){      
			$this->redirect('dashboard');                        
		}    
	}
	
	#_________________________________________________________________________#  
	
	/**    
	* @Date: 21-sep-2016 
	* @Method : hardwareslist  
	* @Purpose: This function is to show list of Hardwares in system.  
	* @Param: none   
	* @Return: none  
	**/
	function hardwareslist(){    
         $this->viewBuilder()->layout('new_layout_admin');
		$session = $this->request->session();
		 $userSession = $session->read("SESSION_ADMIN");
		$this->set('session',$session);
		$this->set("title_for_layout", "Hardware Listing");
		$this->set("pageTitle", "Hardware Listing");
		$this->set("search1", "");
		$this->set("search2", "");
        $criteria = "1"; //All Searching
        $session->delete('SESSION_SEARCH');
        $resultCredit  =  $this->Hardwares->find('all', array(

        	'fields' => array('Hardwares.id','Hardwares.purchase_price','Hardwares.current_price','Hardwares.unique_id','Hardwares.alloted_to','Hardwares.description','Hardwares.item')
        	));  

        $totalcost=isset($resultCredit)? $resultCredit->toArray():array();

        $this->set('resultData', $totalcost);
        $this->set('resultCredit',$resultCredit);       
        
        if (isset($this->request->data['Hardwares']) || !empty($this->request->query)) {

        	if (!empty($this->request->data['Hardwares']['fieldName']) || isset($this->request->query['field'])) {
        		if (trim(isset($this->request->data['Hardwares']['fieldName'])) != "") {
        			$this->set("search1", $this->request->data['Hardwares']['fieldName']);
        			if (trim($this->request->data['Hardwares']['fieldName']) == "Hardwares.itemname") {
        				$search1 = "Hardwares.item";
        			} elseif (trim($this->request->data['Hardwares']['fieldName']) == "Hardwares.uniqueid") {
        				$search1 = "Hardwares.unique_id";
        			} elseif (trim($this->request->data['Hardwares']['fieldName']) == "Hardwares.allotedto") {
        				$search1 = "Hardwares.alloted_to";
        			} else {
        				$search1 = trim($this->request->data['Hardwares']['fieldName']);
        			}
        		} elseif (isset($this->request->query['field'])) {
        			$search1 = trim($this->request->query['field']);
        			$this->set("search1", $search1);
        		}

        	}

        	if (isset($this->request->data['Hardwares']['value1']) || isset($this->request->data['Hardwares']['value2']) || isset($this->request->query['value'])) {
        		if (isset($this->request->data['Hardwares']['value1']) || isset($this->request->data['Hardwares']['value2'])) {
        			$search2 = ($this->request->data['Hardwares']['fieldName'] != "Hardwares.unique_id") ? trim($this->request->data['Hardwares']['value1']) : $this->request->data['Hardwares']['value2'];
        		} elseif (isset($this->request->query['value'])) {
        			$search2 = trim($this->request->query['value']);
        		}
        		$this->set("search2", $search2);
        	}
        	/* Searching starts from here */
        	if (!empty($search1) && (!empty($search2) || $search1 == "Hardwares.unique_id")) {
        		$query  =    $this->Hardwares->find("all" , [
        			'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'agency_id' => $userSession[3]],
        			]);

        		$result = isset($query)? $query->toArray():array();

        		$session->write('SESSION_SEARCH', $criteria);
        	} else {
        		$query  =  $this->Hardwares->find("all", [
        			'conditions' => ['agency_id' => $userSession[3]],
        			]);
        		$this->set("search1", "");
        		$this->set("search2", "");
        	}

        } else {
            // if action is accessed without searching or sorting
        	$query  =  $this->Hardwares->find("all",[
        			'conditions' => ['agency_id' => $userSession[3]],
        			]);

        }
        $urlString = "/";
        if (isset($this->request->query)) {

        	$completeUrl = array();

        	if (!empty($this->request->query['page']))
        		$completeUrl['page'] = $this->request->query['page'];

        	if (!empty($this->request->query['sort']))
        		$completeUrl['sort'] = $this->request->query['sort'];

        	if (!empty($this->request->query['direction']))
        		$completeUrl['direction'] = $this->request->query['direction'];

        	if (!empty($search1))
        		$completeUrl['field'] = $search1;

        	if (isset($search2))
        		$completeUrl['value'] = $search2;

        	foreach ($completeUrl as $key => $value) {
        		$urlString.= $key . ":" . $value . "/";
        	}
        }

        $this->set('urlString', $urlString);
        $data = $this->paginate($query,[
        	'page' => 1, 'limit' => 20,
        	'order' => ['Hardwares.id' => 'desc'],
        	'paramType' => 'querystring'
        	]);
        $this->set('resultData', $data);
         $this->set('pagename', 'hardwareslist');
    }


	#_________________________________________________________________________#   

	/**  
		* @Date: 21-sep-2016   
		* @Method : add    
		* @Purpose: This function is to add Hardwares from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
		**/  

		function add() {	
			$this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Add Hardware');		
			$this->pageTitle = "Add Hardware";		
			$this->set("pageTitle","Add Hardware");	
			$mode = "add";		
			$session = $this->request->session();
      	    $this->set('session',$session);
			 $userSession = $session->read("SESSION_ADMIN");
			if($this->request->data){
				$this->request->data['agency_id'] = $userSession[3];
				/*pr($this->request->data); die;*/
				$Hardwares = $this->Hardwares->newEntity($this->request->data());
				if(empty($Hardwares->errors())){
					$this->Hardwares->save($Hardwares);
					$this->Flash->success_new("Hardware has been created successfully.");

					return $this->redirect( ['action' => 'hardwareslist']);
				}else{

					$this->set("errors", $Hardwares->errors());
				}
			}     	
			

		}	




	#_________________________________________________________________________#   

	/**                         
    * @Date: 21-sep-2016        
    * @Method : edit    
    * @Purpose: This function is to edit Hardware.
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	function edit($id = null) {
	  $this->viewBuilder()->layout('new_layout_admin');
		$this->set('title_for_layout', 'Edit Hardware');
		$this->pageTitle = "Edit Hardware";
		$this->set("pageTitle", "Edit Hardware");
		$mode = "edit";  

		if($this->request->data){  

			$skill = $this->Hardwares->get($this->request->data['id']);
			$skills = $this->Hardwares->patchEntity($skill,$this->request->data());
			if(empty($skills->errors())) {
				$this->Hardwares->save($skill);
				$this->Flash->success_new("Hardwares has been updated successfully.");

				$this->redirect(array('action' => 'hardwareslist'));
			}else{                                    
				$this->set("errors", $skills->errors());
			}   
		}else if(!empty($id)){ 
			$credential = $this->Hardwares->get($id);

			$this->set('credential',$credential); 
			if(!$credential){             
				$this->redirect(array('action' => 'hardwareslist'));                 
			}                                                      
		}
		$this->set("id", $id);                                                          
	}    
//

	
	/**                         
    * @Date: 21-sep-2016     
    * @Method : delete   
    * @Purpose: This function is to delete Hardware.
    * @Param: none                                                        
    * @Return: none                                                       
    **/
	

   function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
       $employee = $this->Hardwares->get($id);
    if ($this->Hardwares->delete($employee)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 









}