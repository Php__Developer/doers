<?php

namespace App\Controller;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Inflector;
use Cake\Auth\DefaultPasswordHasher;

class WebserviciesController extends AppController {

    
    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : beforeFilter
     * @Purpose: This function is called before any other function.
     * @Param: none
     * @Return: none 
     * */
    function beforeFilter(Event $event) {
      parent::beforeFilter($event);
    

     Router::parseNamedParams($this->request);
     if($this->request->params['action'] == 'adddoc'){
                 $resonse = $this->adddoc($this->request);
     } else if($this->request->params['action'] == 'dcrids'){
                 $resonse = $this->dcrids($this->request);
     }

    $this->viewBuilder()->layout('new_layout_admin');
    $this->set('common', $this->Common);
}

 /**
     * @Date: 26-sep-2016  
     * @Method : initialize
     * @Purpose: This function is called initialize  function.
     * @Param: none
     * @Return: none 
     * */

 public function initialize()
 {
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Agencies');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Documents');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $this->loadComponent('Upload');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }



    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : index
     * @Purpose: This page will render home page
     * @Param: none
     * @Return: none 
     * */
    function index() {
        $this->set("title_for_layout", "ERP : Vlogic Labs - Relying on Commitment");
    }

    public function add(){
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
            $agencyid = $userSession[3];
            $today = date("Y-m-d H:i:s");
            $docs = $this->Documents->find('all',[
                'conditions' => ['Documents.agency_id' => $agencyid],
                'order' => ['Documents.created desc'],
                ])->contain(['Modifier','Creater']);
            if($this->request->is('ajax') == true){
                echo json_encode(['status' => 'success', 'docs' => $docs]);
                die;
            }
            $this->set('docs',$docs);
    }
    public function addescription(){
        if($this->request->is('ajax') == true){
            $this->Documents->updateAll(['description' => $this->request->data['description']],['id' => $this->request->data['doc_id'] ]);
            echo json_encode(['status' => 'success']);
                die;
            }    
    }
    public function adddoc($response){
        if($response->data()){
                $response->data['attached_to'] = $this->Common->DCR($this->Common->dcrForUrl(($response->data['attached_to']) ) ) ;
                $projects = $this->Documents->newEntity($response->data(),['validate' => false ]);
                if($this->Documents->save($projects)){
                    $resultJ = json_encode(['status' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response; 
                }
                //echo json_encode(['status' => 'failed']);
                    $resultJ = json_encode(['status' => 'failed']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response; 
        }
    }
    public function dcrids($response){
         $this->autoRender = false; // avoid to render view
         //pr($response->data()); die;
        if($response->data()){
            $data = $response->data();
            $exp = explode('|||', $data['ids'] ) ;
            $dcrids = [];
            $dc = [];
            $counter = 0;
            foreach($exp as $id){
                //$id = $id
                if(is_numeric($this->Common->DCR($this->Common->dcrForUrl($id.'==') ) ) ) {
                    $dc['id'] = $this->Common->DCR($this->Common->dcrForUrl($id.'=='));
                    $dc['dcrid'] = $id;
                    $dcrids[] = $dc;    
                }
                $counter++;
                if($counter == count($exp)){
                    //echo $counter;
                  //  echo count($exp);
                
                    $resultJ = json_encode(['status' => 'success' , 'ids' => $dcrids]);
                   // $resultJ = ['status' => 'success' , 'ids' => $dcrids];
                    //echo $resultJ; die;
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;   
                }
                
            }
            //pr($dcrids); die;
            

            //echo json_encode(['status' => 'success' , 'ids' => $dcrids]); die;

        }
    }
    
} //end
