<?php
namespace App\Controller\Roles;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Routing\Router;


class RolesController extends AppController
{
    var $name       	=  "Roles";
    /**
	* Specifies helpers classes used in the view pages
	  *@access public
    */
	
   // var $helpers    	=  array('Html', 'Form', 'Javascript', 'Session');
	
    /**
	* Specifies components classes used
	 * @access public
    */
	
    //var $components 	=  array('RequestHandler','Email','Common');

   // var $paginate		=  array();
    
    //var $uses       	=  array('Role'); // For Default Model

	
/******************************* START FUNCTIONS **************************/

	#_________________________________________________________________________#
    /**
    * @Date: 28-Dec-2011
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none
    * @Return: none 
    **/

 function beforeFilter(Event $event) {
    // star to check permission
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
 
    // end code to check permissions
                parent::beforeFilter($event);
                $this->viewBuilder()->layout('layout_admin');
                $this->set('common', $this->Common);
                
                Router::parseNamedParams($this->request);
                    $session = $this->request->session();
                    $userSession = $session->read("SESSION_ADMIN");
                    if($userSession==''){
                    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
                }
            }

  public function initialize()
         {
        parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); 
        $this->loadModel('Evaluations'); 
        $this->loadModel('Users');
        $this->loadModel('Sales');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('ProfilesAudit');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
    }
 
    #_________________________________________________________________________#
    /**
    * @Date: 28-Dec-2011
    * @Method : index
    * @Purpose: This function is the default function of the controller
    * @Param: none
    * @Return: none 
    **/
	function index() {
		$this->render('login');
		if($this->Session->read("SESSION_USER") != ""){
			$this->redirect('dashboard');
		}
    }
	
	#_________________________________________________________________________#
    /**
    * @Date: 28-Dec-2011
    * @Method : admin_index
    * @Purpose: This is the default function of the administrator section for users
    * @Param: none
    * @Return: none 
    **/
	function admin_index() {
		$this->render('admin_login');
		if($this->Session->read("SESSION_ADMIN") != ""){
			$this->redirect('dashboard');
		}
    }
	
	#_________________________________________________________________________#
    /**
    * @Date: 5-Jan-2012
    * @Method : admin_list
    * @Purpose: This function is to show list of roles in system.
    * @Param: none
    * @Return: none 
    **/


   function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
         $role = $this->Roles->get($id);
        if ($this->Roles->delete($role)) {
              $credentials->updateAll($update,['user_id IN' => $id]);
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 

    function list(){
      $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);  
      $this->set('title_for_layout','User Roles Listing');
      $this->set("pageTitle","User Roles Listing");   
      $this->set("search1", "");    
      $this->set("search2", "");  
      $criteria = "1";

	//pr($criteria); die;
	// Delete user and its licences and orders(single/multiple)

	if(isset($this->request->data['Role']) || !empty($this->request->query)) {
		if(!empty($this->request->data['Role']['fieldName']) || isset($this->request->query['field'])){

			if(trim(isset($this->request->data['Role']['fieldName'])) != ""){
				$search1 = trim($this->request->data['Role']['fieldName']);
			}elseif(isset($this->request->query['field'])){
				$search1 = trim($this->request->query['field']);
			}
			$this->set("search1",$search1);
		}

		if(isset($this->request->data['Role']['value1']) || isset($this->request->data['Role']['value2']) || isset($this->request->query['value'])){
			if(isset($this->request->data['Role']['value1']) || isset($this->request->data['Role']['value2'])){
				$search2 = ($this->request->data['Role']['fieldName'] != "Role.status")?trim($this->request->data['Role']['value1']):$this->request->data['Role']['value2'];
			}elseif(isset($this->request->query['value'])){
				$search2 = trim($this->request->query['value']);
			}
			$this->set("search2",$search2);
		}
		//echo $search1."------".$search2;
		/* Searching starts from here */
     



		if(!empty($search1) && (!empty($search2) || $search1 == "Role.status")){

 $query  =    $this->Roles->find('all' , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
               ]);
             $result = isset($query)? $query->toArray():array();
      $criteria = $search1." LIKE '%".$search2."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
     
 }else{  
       $query  =  $this->Roles->find('all');   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }

  
 $urlString = "/";
      if(isset($this->request->query)){                   
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }

  $this->set('urlString', $urlString);
      $urlString = "/";  
      if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
	$this->set('urlString', $urlString);  

$this->paginate = [           
    'fields' => [ 
    'id',
    'role',
    'description',
    'created',
    'status'
],     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     

$data = $this->paginate('Roles',['conditions' => $criteria]);  
$this->set('resultData', $data);  
$this->set('pagename',"rolelist");  

	}
	






     function add() {   
      $this->viewBuilder()->layout('new_layout_admin');
           $this->set('title_for_layout','Add Role');    
           $this->pageTitle = "Add Role";      
           $this->set("pageTitle","Add Role"); 
           $mode = "add";   
           if($this->request->data){  
            $roless = $this->Roles->newEntity($this->request->data());
            if(empty($roless->errors())){
             $this->Roles->save($roless);

             $this->Flash->success_new("Role has been created successfully.");
             $this->redirect(array('action' => 'list'));   
         }else{
          $this->set("errors", $roless->errors());
      }
  }     
}    






	
	function edit($id = null) {
       $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Edit Role');     
            $this->pageTitle = "Edit Role";    
            $this->set("pageTitle","Edit Role");   
            if($this->request->data){ 
                $role = $this->Roles->get($this->request->data['id']);
                $roles = $this->Roles->patchEntity($role,$this->request->data());
                if(empty($roles->errors())) {
                  $this->Roles->save($role);
                  $this->Flash->success_new("Role has been updated successfully.");
                  $this->redirect(array('action' => 'list'));  
              }else{                                    
                 $this->set("errors", $roles->errors());
             } 
         }else if(!empty($id)){ 
            $RolesData = $this->Roles->get($id);
            $this->set('RolesData',$RolesData); 
            if(!$RolesData){             
              $this->redirect(array('action' => 'list'));                 
          }                                                      
      }                                                          
  }  


	/*	if(!empty($id) || !empty($this->data['id'])){
		   $this->set('title_for_layout','Edit Role');      
			 $this->pageTitle = "Edit Role";
			 $this->set("pageTitle","Edit Role");
			 $mode = "edit";
		}else{            
		   $this->set('title_for_layout','Add Role');
			 $this->pageTitle = "Add Role";
			 $this->set("pageTitle","Add Role");
			 $mode = "add";
		}

			if($this->data){

			$this->Role->set($this->data['Role']);
			$isValidated=$this->Role->validates();
		 
        if($isValidated){
				   $data=$this->data['Role'];//post
				   $this->Role->save($this->data['Role'], array('validate'=>false));                                         
					if($mode == "add"){                    
						$this->Session->setFlash("Role has been created successfully.",'layout_success');
					}else{                                  
						$this->Session->setFlash("Role has been updated successfully.",'layout_success');
					}                                       
				  $this->redirect('list');                
			}else{                                      
				$this->set("Error",$this->Role->invalidFields());
			}

		}else if(!empty($id)){       
			$this->data = $this->Role->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));	                                                
			if(!$this->data){                                                         
				$this->redirect(array('action' => 'list'));                             
			}                                                                         
		}  */

     /*    if($this->request->data){ 
         //pr($this->request->data);die;
                $role = $this->Roles->get($this->request->data['id']);
                //pr($role);
                $roles = $this->Roles->patchEntity($role,$this->request->data());
                if(empty($roles->errors())) {
                  $this->Roles->save($contact);
          /*if($mode == "add"){                    
           $this->Flash->success("Role has been created successfully.");
                  $this->redirect(array('action' => 'list')); 
          }else{                                  
            $this->Flash->success("Role has been updated successfully.");
                  $this->redirect(array('action' => 'list')); 
          }                                    
*/

                  
        /*      }else{                                    
                 $this->set("errors", $roles->errors());
             } 
         }else if(!empty($id)){ 
            $RolesData = $this->Roles->get($id);
            $this->set('RolesData',$RolesData); 
            if(!$RolesData){             
              $this->redirect(array('action' => 'list'));                 
          }                                                      
      }                                                                           
	
    }  */            
	
	
}