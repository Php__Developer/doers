<?php

namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Security;


class AppController extends Controller
{

/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : initialize
    * @Purpose: This function is called initialize function.
    * @Param: none                                                
    * @Return: none                                               
    **/
  public function initialize()
    {
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'users',
                'action' => 'dashboard'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login',
                'prefix' => 'admin'
              
            ]
        ]);
    }
     /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/
   public function beforeFilter(Event $event)
    {


   /*   $this->response->cors($this->request)
    ->allowOrigin(['*'])
    ->allowMethods(['GET', 'POST'])
    ->allowHeaders(['X-CSRF-Token'])
    ->allowCredentials()
    ->exposeHeaders(['Link'])
    ->maxAge(300)
    ->build();*/
   $session = $this->request->session();
  $userSession = $session->read("SESSION_ADMIN");
  $controller=$this->request->params['controller'];
  $action=$this->request->params['action'];
  
    if(($controller!="Users" && $action !="login")){

            if($userSession==''){
            ////pr($controller); pr($action); die;
            return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
            }
        $role = explode(",",$userSession['2']);

        $moduleperms = /*$this->Common->modulepermissions($controller,$action)*/ true;
        if($moduleperms == true || in_array(8, $role)){

              $multiloginsession = $session->read("adminlogin");
              
              $permission= $this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
              /*var_dump($permission); die;*/
        if(($permission == false &&  !in_array(1, $role)  && !isset($multiloginsession)) ){
          
             if($userSession !=''){
            $this->Flash->error("You have no permisson to access this Page.");
             return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
            }else{
                return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
            }
           
          }
          
        } else {
             $this->Flash->error("You have no permisson to access this Page.");
             return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
        }

    }

        $this->Auth->allow(['forgotPassword', 'getCode','login','index','goto','detailspage','register','checkuserstatus','adminlogin','adddoc','webdevelopment','mobiledevelopment','erp','training','placement','financialservices','cloudhandling','digitalmarketing','contentwriting']);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
}
