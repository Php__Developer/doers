<?php
namespace App\Controller\Evaluations;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
class EvaluationsController extends AppController
{   
	
	

	/************************ START FUNCTIONS **************************/

	#_________________________________________________________________________#
	
    /**   
		* @Date: 13-sept-2016    
		* @Method : beforeFilter    
		* @Purpose: This function is called before any other function.    
		* @Param: none   
		* @Return: none    
		**/
    function beforeFilter(Event $event){ // This  function is called first before parsing this controller file

    	parent::beforeFilter($event);
       	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");

    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    // end code to check permissions

    	if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){

    		$this->checkUserSession();
    	}else{

    		$this->viewBuilder()->layout('layout_admin');

    	}
    	Router::parseNamedParams($this->request);
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common',$this->Common); 

    }

    #_________________________________________________________________________#

    /**    
		* @Date: 13-sept-2016    
		* @Method : initialize   
		* @Purpose: This function is the default function of the controller    
		* @Param: none    
		* @Return: none     
		**/

		public function initialize()
		{
			parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Leads'); 
        $this->loadModel('Projects');
  		$this->loadModel('Payments');// for importing Model
  	}

/**    
	* @Date: 13-sept-2016   
	* @Method : index    
	* @Purpose: This is the default function of the administrator section for users    
	* @Param: none    
	* @Return: none     
	**/

	function index() {
		$this->render('login');		
		if($session->read("SESSION_USER") != ""){					
			$this->redirect('dashboard');		
		}    
	}


	#_________________________________________________________________________#
	
    /**    
	* @Date: 13-sept-2016    
	* @Method : evaluationslist    
	* @Purpose: This function is to show list of salaries given to the employee.   
	* @Param: none   
	* @Return: none     
	**/ 
	
	function evaluationslist(){
		$this->viewBuilder()->layout('new_layout_admin');
		$session = $this->request->session();
		$this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];
         $this->set('session_info',$session_info);

        $this->set('title_for_layout','Evaluations Listing');	
		$this->set("pageTitle","Evaluations Listing");   
		$this->set("search1", "");	$this->set("search2", "");	
		$criteria = "1";	
		$lastSearch = "";
if(isset($this->request->data) || isset($this->request->query)) {
      $search2 = ['Evaluations.id !=' => 0,'Evaluations.agency_id'=>$userAgency];
      if(!empty($this->request->data['user_id']) || !empty($this->request->query['user_id'])){
        if(!empty($this->request->data['user_id'])){
        	$name= $this->request->data['user_id'];
        }else if(!empty($this->request->query['user_id'])){
           $name=$this->request->query['user_id'];
        }
        $cond = ['Users.first_name Like' => '%'.$this->Common->sanitizeInput($name).'%',//'OR' =>['Users.last_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['user_id']).'%'] 
        'Users.agency_id'=>$userAgency
        ];
          $search2 = array_merge($search2, $cond);
      }
   if(!empty($this->request->data['month']) || !empty($this->request->query['month'])){
   	   if(!empty($this->request->data['month'])){
        	$month= $this->request->data['month'];
        }else if(!empty($this->request->query['month'])){
           $month=$this->request->query['month'];
        }
        $cond = ['Evaluations.month Like' => '%'.$this->Common->sanitizeInput($month).'%','Evaluations.agency_id'=>$userAgency];
        $search2 = array_merge($search2, $cond);
      }
      if(!empty($this->request->data['year'])|| !empty($this->request->query['year'])){        
       if(!empty($this->request->data['year'])){
        	$year= $this->request->data['year'];
        }else if(!empty($this->request->query['year'])){
           $year=$this->request->query['year'];
        }
        $cond = ['Evaluations.year Like' => '%'.$this->Common->sanitizeInput($year).'%','Evaluations.agency_id'=>$userAgency];
        $search2 = array_merge($search2, $cond);
      }
      $this->set("search2",$search2);      
    }

        if((!empty($search2))){	
			$query  =    $this->Evaluations->find("all" , [
				'conditions' => $search2,
				'order' => ['Evaluations.modified' => 'DESC'],
				])->contain(['Users','Seniors']);
			$result = isset($query)? $query->toArray():array();
			$criteria = $search2; 		
			$session->write('SESSION_SEARCH', $criteria);
		}else{			
			$query  =  $this->Evaluations->find('all',[
				'conditions'=> ['Evaluations.agency_id'=>$userAgency],
				'order' => ['Evaluations.modified' => 'DESC'],
				])->contain(['Users','Seniors']);
			$this->set("search2","");		
		}	
		$urlString = "/";          
		if(isset($this->request->query)){ 
			$completeUrl  = array();
			if(!empty($this->request->query['page']))      
				$completeUrl['page'] = $this->request->query['page'];

			if(!empty($this->request->query['sort']))                  
				$completeUrl['sort'] = $this->request->query['sort'];

			if(!empty($this->request->query['direction']))             
				$completeUrl['direction'] = $this->request->query['direction'];

			foreach($completeUrl as $key=>$value){                      
				$urlString.= $key.":".$value."/";                           
			}
		}
$this->set('urlString',$urlString); 
$this->set('pagename','evalist');
$paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/200);
$this->set('paginatecount',$paginate);




$data = $this->paginate($query,[
			'page' => 1, 'limit' => 200,
			'order' => ['Evaluations.modified' => 'DESC'],
			'paramType' => 'querystring'
			]);

		  foreach($data as $d){      
        $d['id'] = $d->id; 
        $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));
          $arr[] = $d; 
       }
         $this->set('resultData', $arr);


		     if($this->request->is('ajax')){
          if(isset($this->request->query['page'])){
             $pageing=$this->request->query['page'];
          }else{
            $pageing="";
          }
        echo json_encode(['resultData'=>$arr,'page'=>$pageing,'paginatecount'=>$paginate]);
         die;
      }

	} 


/**  
		* @Date: 13-sept-2016   
		* @Method : performancereport    
		* @Purpose: This function is to do HRM Settings from admin section.   
		* @Param: none 
		* @Author: Neelam Thakur  
		* @Return: none     
		**/
		function performancereport(){
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$this->viewBuilder()->layout('new_layout_admin');
			$session = $this->request->session();
			$this->set('session',$session);
			$this->set('title_for_layout','Performance Report');
			$this->pageTitle = "Performance Report";
			$criteria =1;
			$this->set("search1", "");	$this->set("search2", "");	
			$criteria = "1";	
			$lastSearch = "";
			$lastmonth = date("m",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
			$this->Evaluations->find('all')->contain(['Users','Seniors'])->toArray();
			$evalData  = $this->Evaluations->find('all',[
				
				'conditions'=>['OR'=>[ ['Evaluations.status'=>'1'],['Evaluations.status'=>'2'] ],'Evaluations.agency_id' => $userSession[3] ]
				])->contain(['Users','Seniors'])->toArray();	 
			if(isset($this->request->data)) {
				$search2 = ['Evaluations.id !=' => 0,'Evaluations.agency_id' => $userSession[3]];
				if(!empty($this->request->data['user_id'])){
					
					$cond = ['Users.first_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['user_id']).'%','OR' =>['Users.last_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['user_id']).'%'] ];
					$search2 = array_merge($search2, $cond);
					
				}
				if(!empty($this->request->data['senior_id'])){
					$cond = ['Seniors.first_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['senior_id']).'%','OR' =>['Seniors.last_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['senior_id']).'%'] ];
					$search2 = array_merge($search2, $cond);

				}
				if(!empty($this->request->data['month1']) && !empty($this->request->data['year1']) && !empty($this->request->data['month2']) && !empty($this->request->data['year2'])){ 				

					$month1 = $create->format('Y-m-d');
					$create=date_create($this->request->data['month1']);
					$month2 = $create->format('Y-m-d');
					$create=date_create($this->request->data['month2']);

					$year1 = $create->format('Y-m-d');
					$create=date_create($this->request->data['year1']);
					$year2 = $create->format('Y-m-d');
					$create=date_create($this->request->data['year2']);
					
					$cond = ['Evaluations.month >=' => $month1 && $month2 , 'Evaluations.year <=' => $year1 && $year2];	
					
					$search2 = array_merge($search2, $cond);
					pr($cond);die();

				}else if( !empty($this->request->data['month1']) &&  !empty($this->request->data['year1']) ){
					
					
					$cond = ['Evaluations.month Like' => '%'.$this->Common->sanitizeInput($this->request->data['month']).'%'];
					$search2 = array_merge($search2, $cond);

				}else if( !empty($this->request->data['month2']) &&  !empty($this->request->data['year2']) ){
					
					$cond = ['Evaluations.month Like' => '%'.$this->Common->sanitizeInput($this->request->data['month']).'%'];
					$search2 = array_merge($search2, $cond);

				}

				$this->set("search2",$search2);      
			}

			if(!empty($this->request->data['month1']) && !empty($this->request->data['year1']) && !empty($this->request->data['month2']) && !empty($this->request->data['year2'])){ 

				$create=date_create($this->request->data['month1']);
				$to = $create->format('Y-m-d');
				$create=date_create($this->request->data['month2']);

				$tom = $create->format('Y-m-d');

				$create=date_create($this->request->data['year1']);
				$from = $create->format('Y-m-d');
				$create=date_create($this->request->data['year2']);

				$fromy = $create->format('Y-m-d');

				$cond = ['Evaluations.month1 >=' => $to, 'Evaluations.month2 <=' => $tom]&&['Evaluations.year1 >=' => $from, 'Evaluations.year2 <=' => $fromy];
				$search2 = array_merge($search2, $cond);

			}

			if((!empty($search2))){

				$query  =    $this->Evaluations->find("all" , [
					'conditions' => $search2,
					])->contain(['Users','Seniors']);
				$result = isset($query)? $query->toArray():array();
				$criteria = $search2; 		
				$session->write('SESSION_SEARCH', $criteria);	
			}else{
				$query  =  $this->Evaluations->find("all")->contain(['Users','Seniors']);
				$this->set("search2","");	
			}

			$data = $this->paginate($query,[
				'page' => 1, 'limit' => 200,
				'conditions'=>['OR'=>[ ['Evaluations.status'=>'1'],['Evaluations.status'=>'2'] ] ],
				'paramType' => 'querystring'
				]);
			$this->set('resultData', $data);


			$paymentData = [];

			$userData = $this->Users->find('all',[
				"fields" => ['id','first_name','role_id'],
				"conditions" => ['status'=>'1']
				])->toArray();

			$userPerformanceData  = [];
			foreach($userData as $v){

				$rolesAry = (!empty($v['role_id'])) ? explode(",",$v['role_id']) : []; 


				$userPerformanceData['Roles'][$v['Users']['id']] = $rolesAry;

				if(in_array('5',$rolesAry)){

					$projectData = $this->Projects->find('list',[
						'keyField' => 'id',
						'valueField' => 'project_name',
						"fields" => ['id','project_name'],
						"conditions" => ['salesbackenduser'=>$v['id'] ]
						])->toArray();

					$projectIDs = "";

					foreach($projectData as $k1=>$v1){

						$projectIDs  .= $k1.",";
					} 

					$projectsIDs = trim($projectIDs,","); 

					if(!empty($projectsIDs)){


						$paymentData = $this->Payments->find('all',[

							"conditions" => ['project_id IN' => explode(',', $projectsIDs), 'MONTH(payment_date)'=> $lastmonth,'YEAR(payment_date)' => date('Y') ]
							])->toArray(); 

					}
					$userPerformanceData[$v['id']] = $paymentData;

					$biddingCount = "";

					$biddingCount = $this->Leads->find('all',[
						"fields" => ['id','bidding_date'],
						"conditions" => ['salesfront_id'=>$v['id'] ]
						])->count();

					$userPerformanceData[$v['id']] = $biddingCount;

				}
				else{

					$paymentData = $this->Payments->find('all',[
						"fields" => ['responsible_id','currency','payment_date','amount'],
						"conditions" =>[ 'responsible_id' =>$v['id']]
						])->toArray();

					$userPerformanceData[$v['id']] = $paymentData;
					$userPerformanceData['Biddings'][$v['id']] = 0;

				}
				$cond = "";

				$resultTicket = $this->Tickets->find('all',[
					"conditions" =>['OR'=>[ ['to_id'=>$v['id'] ],['from_id'=>$v['id'] ] ],'MONTH(created)'=>$lastmonth,'YEAR(created)'=>date('Y')],
					"fields" => ['id','to_id','from_id','deadline','last_replier','status'],
					])->toArray();

				$userPerformanceData[$v['id']] = $resultTicket;

			} 

			$this->set('biddingCount',$biddingCount);
			$this->set('rolesAry',$rolesAry);
			$this->set('evalData',$evalData);
			$this->set('paymentData',$paymentData);
			$this->set('resultTicket',$resultTicket);
			$this->set('userPerformanceData',$userPerformanceData);
		}




    /**    
	* @Date: 13-sept-2016    
	* @Method : add    
	* @Purpose: This function is to add/edit Evaluation from admin section.   
	* @Param: $id   
	* @Return: none    
	* @Return: none  
	**/
	
	function add() {
	$this->viewBuilder()->layout('new_layout_admin');	
		$session = $this->request->session();
		$this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];
         $this->set('session_info',$session_info); 
		$this->set('title_for_layout','Add Evaluation');		
		$this->pageTitle = "Add Evaluation";			
		$this->set("pageTitle","Add Evaluation");	
		$mode = "add";	
		if($this->request->data){	
			//pr($this->request->data); die;
			$this->request->data['status'] = 0;
			$this->request->data['agency_id'] = $userAgency;
			$evaluations = $this->Evaluations->newEntity($this->request->data());
			if(empty($evaluations->errors())){
				$this->Evaluations->save($evaluations);

				$this->Flash->success_new("Evaluation has been created successfully.");

				$this->redirect(array('action' => 'evaluationslist')); 
			}else{
				$this->set("errors", $evaluations->errors());
			}
		}     
	}                    

		 /**    
	* @Date: 13-sept-2016    
	* @Method : edit    
	* @Purpose: This function is to add/edit Evaluation from admin section.   
	* @Param: $id   
	* @Return: none    
	* @Return: none  
	**/
	
	function edit($id = null) { 
		$this->viewBuilder()->layout('new_layout_admin');
		$this->set('title_for_layout','Edit Evaluation');     
		$this->pageTitle = "Edit Evaluation";    
		$this->set("pageTitle","Edit Evaluation");   
		if($this->request->data){ 
			

			$evaluation = $this->Evaluations->get($this->request->data['id']);
			$evaluations = $this->Evaluations->patchEntity($evaluation,$this->request->data());
			if(empty($evaluations->errors())) {
				$this->Evaluations->save($evaluation);
				$this->Flash->success_new("Evaluation has been updated successfully.");
				$this->redirect(array('controller' => 'evaluations', 'action' => 'evaluationslist')); 
			}else{                                    
				$this->set("errors", $evaluations->errors());
			} 
		}else if(!empty($id)){ 
			$EvaluationsData = $this->Evaluations->get($id);
			$this->set('EvaluationsData',$EvaluationsData); 
			if(!$EvaluationsData){             
				$this->redirect(array('controller' => 'evaluations', 'action' => 'evaluationslist'));                 
			}                                                      
		}                                                          
	}    
	
			#_____________________________________________________________________________________________# 

		/**  
		* @Date: 13-sept-2016   
		* @Method : delete    
		* @Purpose: This function is to delete apprasials from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
		**/  


		    function delete($id = null) {
        $id = $id;
        $this->viewBuilder()->layout('new_layout_admin');
        $employee = $this->Evaluations->get($id);
        if ($this->Evaluations->delete($employee)) {
              echo json_encode(['status'=> 'success','page'=>'Appraisals']); die;
        }else { 
             echo json_encode(['status'=> 'failed']); die;
         }   
    }



/**  
		* @Date: 13-sept-2016   
		* @Method : changeStatus    
		* @Purpose: This function is to changeStatus from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
		**/  



 function changeStatus() {
    if($this->request->is('ajax')) {
     $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif($this->request->data['eORd'] =='No') {
       $updateCredentials = 1;
       $setValue = 0;
       $messageStr = 'deactivate';
      }elseif ($this->request->data['eORd'] =='re') {
       $updateCredentials = 1;
       $setValue = 2;
       $messageStr ='responded';
      }
       if ($updateCredentials == 1) {
                $credentials = TableRegistry::get('Credentials');
                $update = ['user_id' => 0 , 'status' => 0];
                $credentials->updateAll($update,['user_id IN' => $id]);
            }
     $users = TableRegistry::get('Evaluations');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;
      }
 }


	#_____________________________________________________________________________________________# 
	/**  
		* @Date: 13-sept-2016    
		* @Method : admin_hrmsettings    
		* @Purpose: This function is to do HRM Settings from admin section.   
		* @Param: none 
		* @Author: Neelam Thakur  
		* @Return: none     
		**/ 
		function admin_hrmsettings(){
			$this->set('title_for_layout','HRM Settings');
			$this->pageTitle = "HRM Settings";
			if($this->request->data){
				$Userdata = $this->User->find('all',array(
					'fields'=>array('User.id','User.report_to'),
					'conditions' =>array('User.status'=>'1')
					));
				$datatosave = array();
				$previousMonth = date("m",strtotime("-1 month"));
				$curYear = date("Y");
				$existedData = $this->Evaluation->find('first',array('conditions'=>array('month'=>$previousMonth)));
				if($existedData > 0){
					$this->Flash->error("Previous month data exists,so evaluations cannot be enabled.");	
				}else{
					foreach($Userdata as $k=>$v){
						$this->Evaluation->id = null;
						$this->request->data['Evaluation']['user_id'] = $v['User']['id'];
						$this->request->data['Evaluation']['senior_id'] = $v['User']['report_to'];
						$this->request->data['Evaluation']['status'] = '0';
						$this->request->data['Evaluation']['month'] = $previousMonth;
						$this->request->data['Evaluation']['year'] = $curYear;
						$savedData = $this->Evaluation->save($this->request->data['Evaluation']);	
					} 
					if($savedData){
						$this->Flash->success_new("Evaluations has been enabled.");	
					}
				}
				$this->redirect(array('controller'=>'employees','action'=>'settings'));
			}
			$this->render('/employess/settings');
		}
	#__________________________________________________________________________________________#
	/**  
		* @Date: 13-sept-2016    
		* @Method : monthlyperformance    
		* @Purpose: This function is to do monthlyperformance from admin section.   
		* @Param: none 
		* @Author: Neelam Thakur  
		* @Return: none     
		**/ 

        function monthlyperformance($id = null){
			$this->viewBuilder()->layout('new_layout_admin');
			$session = $this->request->session();
		$this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3]; 
			$this->set('title_for_layout','Submit Performance Report');
			$this->pageTitle = "Submit Performance Report";
			$criteria = 1;
			$userData = $session->read("SESSION_ADMIN");
			$sessionID = $userData[0];
			$query = $this->Evaluations->find('all',[
				'conditions'=>['Evaluations.senior_id'=>$sessionID,'Evaluations.status'=>'0','Evaluations.agency_id'=>$userAgency],
				])->contain(['Users','Seniors']);
 
	$this->set('pagename','monthlyperformance');
	$paginate_count=isset($query)? $query->toArray():array();
	$paginate=ceil(count($paginate_count)/200);
	$this->set('paginatecount',$paginate);
     $data = $this->paginate($query,[
				'page' => 1, 'limit' => 200,
				'paramType' => 'querystring'
				]);
       foreach($data as $d){      
          $d['id'] = $d->id; 
          $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));
          $arr[] = $d; 
       }

       $this->set('resultData', $arr);
      
          // $this->render('evaluationslist');
}


        /**  
		* @Date: 13-sept-2016    
		* @Method : giveratings    
		* @Purpose: This function is to do giveratings from admin section.   
		* @Param: none 
		* @Author: Neelam Thakur  
		* @Return: none     
		**/ 

		function giveratings($id = null) {
          $this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Submit Performance Report');     
			$this->pageTitle = "Submit Performance Report";    
			$this->set("pageTitle","Submit Performance Report");   
			if($this->request->data){ 

				$this->request->data['status'] = '1';

				$evaluationrating = $this->Evaluations->get($this->request->data['id']);

				$evaluationratings = $this->Evaluations->patchEntity($evaluationrating,$this->request->data());
				if(empty($evaluationratings->errors())) {
					$this->Evaluations->save($evaluationrating);


					$this->Flash->success_new('Ratings has been updated successfully.');
					$this->redirect(array('controller' => 'evaluations', 'action' => 'monthlyperformance')); 
				}else{                                    
					$this->set("errors", $evaluationratings->errors());
				} 


			}else if(!empty($id)){ 

				$EvaluationsData = $this->Evaluations->find('all',['conditions'=>['Evaluations.id'=>$id] ])->contain(['Users','Seniors'])->first();
				$this->set('EvaluationsData',$EvaluationsData); 

				if(!$EvaluationsData){ 

					$this->Flash->success_new('Ratings has been updated successfully.');
					$this->redirect(array('controller' => 'evaluations', 'action' => 'monthlyperformance'));                 
				}   

			}                                                          
		}    



		#__________________________________________________________________________________________#
	/**  
		* @Date: 13-sept-2016   
		* @Method : myperformance    
		* @Purpose: This function is to do myperformance from admin section.   
		* @Param: none 
		* @Author: Neelam Thakur  
		* @Return: none     
		**/ 
		function myperformance(){
			$this->viewBuilder()->layout('new_layout_admin');
			$session = $this->request->session();
			$this->set('session',$session);

			$this->set('title_for_layout','My Performance');
			$this->pageTitle = "Performance Report";
			$criteria =1;
			$userData = $session->read("SESSION_ADMIN");

			$sessionID = $userData[0];

			$rolesAry = explode(",",$userData[2]);

			$lastmonth = date("m",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));

			$this->Evaluations->find('all')->contain(['Users','Seniors'])->toArray();

			$evalData  = $this->Evaluations->find('all',[
				'conditions'=>[['Evaluations.user_id'=>$sessionID],'OR'=>[ ['Evaluations.status'=>'1'],['Evaluations.status'=>'2'] ] ],
					'order' => ['Evaluations.modified' => 'DESC'],
				])->contain(['Users','Seniors'])->toArray();	 

			$paymentData = [];

			$userData = $this->Users->find('all',[
				"fields" => ['Users.id','Users.first_name','Users.role_id'],
				"conditions" => ['Users.status'=>'1']
				])->toArray();


			$userPerformanceData  = [];
			foreach($userData as $v){

				$rolesAry = (!empty($v['role_id'])) ? explode(",",$v['role_id']) : []; 


				if(in_array('5',$rolesAry)){

					$projectData = $this->Projects->find('list',[
						'keyField' => 'id',
						'valueField' => 'project_name',
						"fields" => ['id','project_name'],
						"conditions" => [['salesbackenduser'=>$v['id']]]
						])->toArray();


					$projectIDs = "";

					foreach($projectData as $k1=>$v1){

						$projectIDs  .= $k1.",";
					} 

					$projectsIDs = trim($projectIDs,","); 

					if(!empty($projectsIDs)){

						$paymentData = $this->Payments->find('all',[
							"fields" => ['id','responsible_id','currency','payment_date','amount','project_id'],
							"conditions" => ['project_id IN' => explode(',', $projectsIDs), 'MONTH(payment_date)'=> $lastmonth,'YEAR(payment_date)' => date('Y') ]
							])->toArray(); 

					}
					$userPerformanceData[$v['id']] = $paymentData;

					$biddingCount = "";


					$biddingCount = $this->Leads->find('all',[
						"fields" => ['id','bidding_date'],
						"conditions" => (['salesfront_id'=>$sessionID
							]) ])->toArray();


					$this->set('biddingCount',$biddingCount);

				}
				else{

					$paymentData = $this->Payments->find('all',[
						"fields" => ['responsible_id','currency','payment_date','amount'],
						"conditions" =>[ ['responsible_id' =>$v['id']]]
						])->toArray();

					$userPerformanceData[$v['id']] = $paymentData;
					$userPerformanceData['Biddings'][$v['id']] = 0;

				}

				$resultTicket = $this->Tickets->find('all',[
					"conditions" =>['OR'=>[ ['to_id'=>$v['id'] ],['from_id'=>$v['id'] ] ],'MONTH(created)'=>$lastmonth,'YEAR(created)'=>date('Y')],
					"fields" => ['id','to_id','from_id','deadline','last_replier','status'],
					])->toArray();


			} 
			$this->set('biddingCount',$biddingCount);
			$this->set('evalData',$evalData);
			$this->set('paymentData',$paymentData);
			$this->set('resultTicket',$resultTicket);
			$this->set('userPerformanceData',$userPerformanceData);
		}
			#____________________________________________________________________________________________________#
	/**  
		* @Date: 13-sept-2016    
		* @Method : userresponse    
		* @Purpose: This function is to do userresponse from admin section.   
		* @Param: None 
		* @Author: Neelam Thakur  
		* @Return: None     
		**/ 
		function userresponse($id = null,$key = null){

			$this->viewBuilder()->layout(false);

			$this->Evaluations->find('all')->contain(['Users','Seniors'])->toArray();

			$evalData  = $this->Evaluations->find('all',[

				'conditions'=>['Evaluations.id'=>$id] ])->contain(['Users','Seniors'])->first();

			$this->set('evalData',$evalData);
			if($this->request->is('ajax')==true){

				$user_comment = $this->request->data['user_comment'];

				$result = $this->Evaluations->updateAll(
					['status' => '2',
					'user_comment' => $this->request->data['user_comment']],
					['id' =>$this->request->data['id']] );

				if($result){

					$this->Flash->success_new("Thankyou for your response.");
					echo "1"; die;
				}
			}
			if($key == "response"){

				$this->render('userresponse');
			}elseif($key == "detail"){
				$this->render('details');
			}
		}


	/**
    * @Date: 13-sept-2016
    * @Method : performancereportdatewise(date wise)
    * @Purpose: This function is to show user wise performance report .
    * @Param: none
    * @Return: none
    **/
	function performancereportdatewise()
	{ 

		$session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session->read("SESSION_ADMIN");

		$this->viewBuilder()->layout(false);
		if(isset($this->request->data['user_id'])){ 

			$userselect   =$this->request->data['user_id'];

			$start_date    =$this->request->data['start_date'];

			$end_date      =$this->request->data['end_date'];

			$leadselect     =$this->request->data['select'];

			$userid1       =$this->request->data['user_id'];

			$dateRange    = $this->request->data['select'];


			$cur=date('Y-m-d');

		} else {
			$userselect="";

			$start_date="";

			$end_date="";

			$leadselect="";

			$userid1="";

			$dateRange ="";

			$cur="";
		}
		$this->set('fromDate',$start_date);
		$this->set('toDate',$end_date);
		$this->set('leadselect',$leadselect);

		$first_date=date("Y/m/d", mktime(0, 0, 0, date("m")-1, 1, date("Y")));
		
		$last_date=date("Y/m/d", mktime(0, 0, 0, date("m"), 0, date("Y")));
		
		$cond = array('Evaluations.created >= '=> $start_date,'Evaluations.created <='=>$end_date);
		
		$cond1 = array('Evaluations.created >= '=> $first_date,'Evaluations.created <='=>$last_date);
		
		if(empty($userselect) && empty($start_date)&&empty ($end_date))
		{
			$data=$this->Evaluations->find() 
			->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5','Users.first_name','Users.last_name','Evaluations.user_id']) 

			->where(['Users.status'=>'1',$cond1,'Evaluations.agency_id' => $userSession[3]]) 
			->group('Evaluations.user_id')

			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Evaluations.user_id =Users.id')

				])->toArray();


		}
		else if(empty($userselect) && !empty($start_date)&&!empty ($end_date))
		{
			
			$data  = $this->Evaluations->find() 
			->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5','Users.first_name','Users.last_name','Evaluations.user_id']) 
			->where(['Users.status'=>'1',$cond,'Evaluations.agency_id' => $userSession[3]]) 
			->group('Evaluations.user_id')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Evaluations.user_id =Users.id')
				])->toArray();

		}
		elseif(!empty($userselect) && $leadselect!='select')
		{	


			$data=$this->Evaluations->find() 
			->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5','Evaluations.user_id']) 
			->where(['Users.status'=>'1','Evaluations.User_id'=>$userselect,$cond,'Evaluations.agency_id' => $userSession[3]]) 
			->group('Evaluations.user_id')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Evaluations.user_id =Users.id')
				])->toArray();

		}
		else if(!empty($userselect) && $start_date==$cur)
		{

			$data=$this->Evaluations->find() 
			->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5','Evaluations.user_id']) 
			->where(['Users.status'=>'1','Evaluations.User_id'=>$userselect,'Evaluations.agency_id' => $userSession[3]]) 
			->group('Evaluations.user_id')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Evaluations.user_id =Users.id')
				])->toArray();

		}
		else
		{

			$data=$this->Evaluations->find() 
			->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5','Users.first_name','Users.last_name','Evaluations.user_id']) 
			->where(['Users.status'=>'1','Evaluations.User_id'=>$userselect,$cond,'Evaluations.agency_id' => $userSession[3]]) 
			->group('Evaluations.user_id')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Evaluations.user_id = Users.id')
				])->toArray();

			
		}
		$this->set('resultData',$data);

	}
//-------------------------------------------------------------------------//
/**
    * @Date: 13-sept-2016
    * @Method : performancereportmonth
    * @Purpose: This function is to show month wise performance report of users.
    * @Param: none
    * @Return: none
    **/
function performancereportmonth()
{
	$this->viewBuilder()->layout(false);
	$session = $this->request->session();
	$this->set('session',$session);
	$userSession = $session->read("SESSION_ADMIN");
	if(isset($this->request->data['user_id'])){ 
		$selectedUser   =$this->request->data['user_id'];
		$start_month    =$this->request->data['month1'];
		$end_month      =$this->request->data['month2'];
		$start_year     =$this->request->data['year1'];
		$end_year       =$this->request->data['year2'];
	} else {
		$selectedUser="";
		$start_month="";
		$end_month="";
		$start_year="";
		$end_year="";
	}
	$this->set('start_month',$start_month);
	$this->set('end_month',$end_month);
	$this->set('start_year',$start_year);
	$this->set('end_year',$end_year);
	$first_date = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m")-1, 1, date("Y")));
	$last_date = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"), 0, date("Y")));
	$cond = array('Evaluations.month >= ' =>$start_month,'Evaluations.month >= '=> $end_month,'Evaluations.year >= ' =>$start_year,'Evaluations.year >=  '=> $end_year);
	$cond1 = array('Evaluations.month >= ' =>$start_month,'Evaluations.month >= ' =>$end_month);
	$cond2 = array('Evaluations.year >=' =>$start_year,'Evaluations.year >=' =>$end_year);
	$cond3 = array('Evaluations.created  >=' =>$first_date,'Evaluations.created  >=' => $last_date);
	if(empty($selectedUser) && empty($start_month)&&empty ($start_year)  && empty($end_month)&&empty ($end_year))
	{
		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1',$cond3,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month')

		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();

	}
	elseif(empty($selectedUser) && !empty($start_month)&&!empty ($start_year) && !empty($end_month)&&!empty ($end_year))
	{

		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1',$cond,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month','Evaluations.year')

		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();

	}
	elseif(empty($selectedUser) && !empty($start_month)&&!empty ($end_month))
	{
		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1',$cond1,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month','Evaluations.year')

		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();

	}

	elseif(empty($selectedUser) && !empty($start_year)&&!empty ($end_year))
	{

		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1',$cond2,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month')

		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();
	}
	elseif(!empty($selectedUser) && empty($start_month)&&empty ($start_year) && empty($end_month)&& empty ($end_year))
	{

		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1','Evaluations.user_id' => $selectedUser,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month')
		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();

	}
	elseif(!empty($selectedUser) && !empty($start_year)&&!empty ($end_year))
	{
		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1','Evaluations.user_id' => $selectedUser,$cond2,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month')
		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();
	}
	elseif(!empty($selectedUser) && !empty($start_month)&&!empty ($end_month))
	{

		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1','Evaluations.user_id' => $selectedUser,$cond1,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month','Evaluations.year')
		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();

	}
	if(!empty($selectedUser) && !empty($start_month)&&!empty ($start_year) && !empty($end_month)&&!empty ($end_year))
	{   
		$data=$this->Evaluations->find() 
		->select(['Evaluations.performance_5','Evaluations.process_5','Evaluations.knowledge_5','Evaluations.discipline_5','Evaluations.hractivities_5', 'Evaluations.month','Users.first_name','Users.last_name']) 

		->where(['Users.status'=>'1','Evaluations.user_id' => $selectedUser,$cond,'Evaluations.agency_id' => $userSession[3]]) 
		->group('Users.first_name','Evaluations.month','Evaluations.year')
		->join([
			'table' => 'users',
			'alias' => 'Users',
			'type'=>'INNER',
			'foreignKey' => false,
			'conditions' => array ('Evaluations.user_id =Users.id')

			])->toArray();
	}
	$this->set('resultData',$data); 
}	
}
