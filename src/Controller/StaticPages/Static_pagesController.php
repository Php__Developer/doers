<?php 


namespace App\Controller\StaticPages;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Cache\Cache;

class Static_pagesController extends AppController {

 
 

  /*********************** START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event){ // This  function is called first before parsing this controller file

      parent::beforeFilter($event);
        // star to check permission
      $session = $this->request->session();
      $userSession = $session->read("SESSION_ADMIN");
      $controller=$this->request->params['controller'];
      $action=$this->request->params['action'];
      $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
      if($permission!=1){
        $this->Flash->error("You have no permisson to access this Page.");

        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
      }
    // end code to check permissions
      if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){
       
       $this->checkUserSession();
     }else{
       
      $this->viewBuilder()->layout('layout_admin');
      
    }
    Router::parseNamedParams($this->request);
    $session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");
    if($userSession==''){
      return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common',$this->Common); 

  }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize function.
    * @Param: none                                                
    * @Return: none                                               
    **/
  public function initialize()
  {
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
           $this->loadModel('Settings'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
      }


    /**      
    * @Date: 20-sep-2016     
    * @Method : index           
    * @Purpose: This function is the default function of the controller
    * @Param: none                                                     
    * @Return: none                                                    
    **/

    function index() {
      $this->render('login');
      
      if($this->Session->read("SESSION_USER") != ""){      
        $this->redirect('dashboard');                        
      }    
    }



    /**                         
    * @Date: 20-sep-2016       
    * @Method : staticlist      
    * @Purpose: This function is to show list of StaticPage in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function staticlist(){
    	$session = $this->request->session();
      $this->set('session',$session);

      $this->set('title_for_layout','Knowledge Base');      
      $this->set("pageTitle","Knowledge Base");             
      $this->set("search1", "");                                 
      $this->set("search2", "");                                 
      $criteria = "1"; //All Searching
      $session->delete('SESSION_SEARCH');

      
      $session_admin = $session->read("SESSION_ADMIN");
      

      if(isset($this->request->data['StaticPage']) || !empty($this->params['named'])) {      
        if(!empty($this->request->data['StaticPage']['fieldName']) || isset($this->params['named']['field'])){                                                                 
          if(trim(isset($this->request->data['StaticPage']['fieldName'])) != ""){                
            $search1 = trim($this->request->data['StaticPage']['fieldName']);                      
          }elseif(isset($this->params['named']['field'])){                              
            $search1 = trim($this->params['named']['field']);                             
          }
          
          $this->set("search1",$search1);                                               
        }
        
        if(isset($this->request->data['StaticPage']['value1']) || isset($this->request->data['StaticPage']['value2']) || isset($this->params['named']['value'])){                  
          if(isset($this->request->data['StaticPage']['value1']) || isset($this->request->data['StaticPage']['value2'])){                                                            
            $search2 = ($this->request->data['StaticPage']['fieldName'] != "StaticPage.status")?trim($this->request->data['StaticPage']['value1']):$this->request->data['StaticPage']['value2'];      
          }elseif(isset($this->params['named']['value'])){       
            $search2 = trim($this->params['named']['value']);      
          }                                                      
          $this->set("search2",$search2);                        
        }
        
        /* Searching starts from here */                                               
        if(!empty($search1) && (!empty($search2) || $search1 == "StaticPage.status")){                                                                           
          $criteria = $search1." LIKE '%".Sanitize::escape($search2)."%'"; 
          $this->Session->write('SESSION_SEARCH', $criteria);
        }else{      
          $this->set("search1","");      
          $this->set("search2","");      
        }
        
      }
      $urlString = "/";
      if(isset($this->params['named'])){      
        
        $completeUrl  = array();
        
        if(!empty($this->params['named']['page']))      
          $completeUrl['page'] = $this->params['named']['page'];
        
        if(!empty($this->params['named']['sort']))                  
          $completeUrl['sort'] = $this->params['named']['sort'];
        
        if(!empty($this->params['named']['direction']))             
          $completeUrl['direction'] = $this->params['named']['direction'];
        
        if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
        
        if(isset($search2))                                         
          $completeUrl['value'] = $search2;
        
        foreach($completeUrl as $key=>$value){                      
          $urlString.= $key.":".$value."/";                           
        }
      }
      
      $this->set('urlString',$urlString); 

      
      $cond="";
      $user_id = $session_admin['0'];   
      
      $cond = "(FIND_IN_SET ($user_id,user_id) OR is_default='1')";
      if($user_id != ADMIN_ID) $cond.= " AND status = '1'";
      
      $paginate = [
      'conditions'=>$cond,                                     
      'fields' => [                                             
      'id',   
      'title',
      'content',
      'created',
      'modified',
      'status',
      'category'           
      ],      
      'page' => 1, 'limit' => RECORDS_PER_PAGE,
      'order' => ['category' => 'asc', 'title' => 'asc']
      ];
      
      
      $data = $this->paginate('StaticPages', ['criteria' => $criteria]); 
      $this->set('resultData', $data);
      if($user_id != ADMIN_ID){
        $this->render('temp_list'); 
        
      }
      
    }                                                 
    
    
/**
    * @Date:20-sep-2016        
    * @Method : admin_changeStatus    
    * @Purpose: This function is used to delete StaticPages.
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  

function admin_changeStatus(){
 
  if(isset($this->params['form']['publish'])){      
   $setValue = array('status' => "'1'");             
   $messageStr = "Selected StaticPage(s) have been activated.";    
   
 }elseif(isset($this->params['form']['unpublish'])){
   $setValue = array('status' => "'0'");             
   $messageStr = "Selected StaticPage(s) have been deactivated."; 
   
 }
 
 if(!empty($setValue)){                            
  if(isset($this->params['form']['IDs'])){          
    $saveString = implode("','",$this->params['form']['IDs']);      
  }
  
  if($saveString != ""){                                          
    $this->StaticPage->updateAll($setValue,"StaticPage.id in ('".$saveString."')");                                                                 
    $this->Session->setFlash($messageStr, 'layout_success');  
    $this->redirect('staticlist');   
  }
}
}


	/**
   * @Date: 20-sep-2016       
    * @Method : admin_delete    
    * @Purpose: This function is used to delete StaticPages.
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
 
 function admin_delete()
 {
  
  if(isset($this->params['form']['IDs'])){                         
   $deleteString = implode("','",$this->params['form']['IDs']);      
 }elseif(isset($this->params['pass'][0])){                         
   $deleteString = $this->params['pass'][0];                         
 }
 
 if(!empty($deleteString)){      
   $this->StaticPage->deleteAll("StaticPage.id in ('".$deleteString."')");
   $this->Session->setFlash("<div class='success-message flash notice'>StaticPage(s) deleted successfully.</div>", 'layout_success');
   $this->redirect('staticlist');                                                  
 }
}



    /**
    * @Date: 20-sep-2016        
    * @Method : admin_add      
    * @Purpose: This function is to send StaticPages to users
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
    
    
    function admin_add() {
      
     $this->set('title_for_layout','Add Templates');
     $this->pageTitle = "Add StaticPage";
     $this->set("pageTitle","Add StaticPage");
     $getRoles = $this->Role->find('list', array('fields'=>array('id','role')));
     $this->set('resultData',$getRoles);
     
     if($this->request->data){
       $this->StaticPage->set($this->request->data['StaticPage']);
       $isValidated=$this->StaticPage->validates();
       
       if($isValidated){
         
        $this->request->data['StaticPage']['role_id'] = null;
        $this->request->data['Permission']['user_id'][] = ADMIN_ID;
        $this->request->data['StaticPage']['user_id'] = implode(",",array_unique($this->request->data['Permission']['user_id']));
        $this->StaticPage->save($this->request->data['StaticPage'], array('validate'=>false));                                         
        $this->Session->setFlash("StaticPage has been created successfully.",'layout_success');                  
        $this->redirect('staticlist');                
      }else{                                      
       $this->set("Error",$this->StaticPage->invalidFields());
     }

   }                                                                 
 }
 
 
 
   /**
    * @Date: 20-sep-2016        
    * @Method : admin_edit
    * @Purpose: This function is to send StaticPages to users
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
   
   
   function admin_edit($id = null) {
     
    $this->set('title_for_layout','Edit Templates');      
    $this->pageTitle = "Edit StaticPage";
    $this->set("pageTitle","Edit StaticPage");
    $getRoles = $this->Role->find('list', array('fields'=>array('id','role')));
    $this->set('resultData',$getRoles);
    
    if($this->request->data){
      $this->StaticPage->set($this->request->data['StaticPage']);
      $isValidated=$this->StaticPage->validates();
      
      if($isValidated){
        $this->request->data['StaticPage']['role_id'] = null;
        $this->request->data['Permission']['user_id'][] = ADMIN_ID;
        $this->request->data['StaticPage']['user_id'] = implode(",",array_unique($this->request->data['Permission']['user_id']));
        $this->StaticPage->save($this->request->data['StaticPage'], array('validate'=>false));                                         
        $this->Session->setFlash("StaticPage has been updated successfully.",'layout_success');                                      
        $this->redirect('staticlist');                
      }else{                                      
       $this->set("Error",$this->StaticPage->invalidFields());
     }

   }else if(!empty($id)){ 
     
     $this->request->data = $this->StaticPage->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));
     
     if(!empty($this->request->data['StaticPage']['user_id'])){
      
      $users= $this->request->data['StaticPage']['user_id'];
      $cond = "WHERE id in ($users)";
      $result = $this->User->find('all', 
       array(
        'fields' => array('User.id', 'User.employee_code','User.first_name','User.last_name','User.username','User.status','User.role_id'),
        'conditions' => $cond,
        ));
      $this->request->data['StaticPage']['userData'] = $result ;
    }
    
    if(!$this->request->data){                                                         
      $this->redirect(array('action' => 'staticlist'));                             
    }                                                                     
  }                                                                           
}


   /**
     * @Date: 20-sep-2016
    * @Method : admin_setPermission
    * @Purpose: This function is to set StaticPages permissions to user
    * @Param: $id                                       
    * @Return: none
    * @Return: none
    **/  
   function admin_setPermission() {
    if(isset($this->params['form']['IDs'])){  
      
     $pages = $this->params['form']['IDs'];
     $pagesID = implode(",",$pages);
     
     $cond = "WHERE StaticPage.id in ($pagesID)";
     $pageResult = $this->StaticPage->find('list', array(
       'fields' => array('StaticPage.id', 'StaticPage.user_id'),
       'conditions' => $cond
       ));
     
     $userID = $this->request->data['StaticPage']['user_id']; 
     
     foreach($pages as $val)
     {
       $pageResult[$val] .= ",".$userID;
       $users = explode(",",$pageResult[$val]);
       $user_id = implode(",",array_unique($users));
       $result = $this->StaticPage->updateAll(
        array('StaticPage.user_id' => "'$user_id'"),
        array('StaticPage.id =' => $val)
        ); 
     }
     if($result){
       $this->Session->setFlash("<div class='success-message flash notice'>StaticPage(s) have been assigned permissions successfully.</div>", 'layout_success');
       $this->redirect('staticlist');                                                  
     }
   }
 }

    /**
    * @Date: 20-sep-2016
    * @Method : staticView
    * @Purpose: Function to view a static page.
    * @Param: $id
    * @Return: none
    **/
    function staticView($id = null) 
    {
      
      $this->viewBuilder()->layout(false);
      if(isset($id) && is_numeric($id)){
       
       $result = $this->StaticPages->get($id);

       if($result){
         
        $this->set('result',$result->toArray());
      }else{

        $this->redirect('static_pages');
      }
    }else{

     $this->redirect('static_pages');
   }

 }
	 /**
    * @Date: 20-sep-2016
    * @Method : view
    * @Purpose: Function to view a static page.
    * @Param: $id
    * @Return: none
    **/

   function view($slug= null) {

     
    $this->viewBuilder()->layout(false);
    if(isset($slug)){

      if(!empty($this->request->data)){

       $this->contactus($id);
     }

     $data=TableRegistry::get('StaticPages');
     $result=$data->find()->where(['id'=>$slug])->first();
    if($result){

       $this->set('result',$result);
     }else{

       $this->redirect('/');
     }
   }else{
    $this->redirect('/');

  }

}

/**
    * @Date: 20-sep-2016
    * @Method : contactus
    * @Purpose: This function is used to show list of Credits 
    * @Param: none
    * @Return: none 
    **/

function contactus($id) {
	$this->StaticPage->set($this->request->data['StaticPage']);

	$isValidated = $this->StaticPage->validates();

	if($isValidated){

   $subject = "Notification: Contact Us";

   $message = "Dear Admin,<br/><br/>A user has tried to contact you. Please find the details below:<br/>

   <table cellpadding='4' cellspacing='1' border='0' style='font-size:12px;'>

   <tr><td align='right'><b>Subject:</b></td><td>".$this->request->data['StaticPage']['subject']."</td></tr>

   <tr><td align='right'><b>Name:</b></td><td>".ucwords($this->request->data['StaticPage']['name'])."</td></tr>

   <tr><td align='right'><b>Email address:</b></td><td>".$this->request->data['StaticPage']['email']."</td></tr>

   <tr><td align='right'><b>Website:</b></td><td>".$this->request->data['StaticPage']['website']."</td></tr>

   <tr><td align='right'><b>Phone:</b></td><td>".$this->request->data['StaticPage']['phone']."</td></tr>

   <tr><td align='right'><b>Notes/Questions/Feedback:</b></td><td>".$this->request->data['StaticPage']['feedback']."</td></tr>

   </table><br/>Thanks,<br/>Dog Booking Support";



   $this->Email->to   = ADMIN_EMAIL;

   $this->Email->subject  = $subject;

   $this->Email->replyTo  = ADMIN_EMAIL;

   $this->Email->from     = ADMIN_EMAIL;

   $this->Email->fromName = ADMIN_NAME;

   $this->Email->sendAs   = 'html';

   $this->Email->send($message);

   $this->Session->setFlash("<div class='success-message flash notice'>Thank you for contacting us. One of our representative will contact you soon!!!</div>");

   $this->redirect('/static_pages/page/'.$id);

 }else{

  $this->set('error',$this->StaticPage->invalidFields());

}


}

    /**
    * @Date: 20-sep-2016
    * @Method : dashboardeditor
    * @Purpose: This function is used to dashborad page to edit.
    * @Param: none
	* @Author: Neelam Thakur
    * @Return: none 
    **/
	function dashboardeditor(){
    $session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");
    $this->viewBuilder()->layout('new_layout_admin');
		$this->set('title_for_layout','Dashboard Editor');
		$this->set("pageTitle","Dashboard Editor");
    $this->set('session',$session);
    //$dashboardPageID = 19;

     $content = $this->Settings->find('all', [
          'fields'=>['key','value'],
          'conditions' =>['Settings.key' => 'dashboard_content','agency_id'=> $userSession[3]]
          ])->first();
$data = isset($content)? $content->toArray():array();;

    if($this->request->data){
$credentials = TableRegistry::get('Settings');
     $content=$this->request->data['content'];
     $update = ['value' => $content];
     $result=$credentials->updateAll($update,["`key`" =>'dashboard_content','agency_id'=>$userSession[3]]);
if($result==1) {
       Cache::delete("dashcontent_$userSession[3]",'long'); 
       $this->Flash->success_new("Dashborad has been updated successfully.");
       $this->redirect(array('controller'=>'users','action' => 'dashboard','prefix'=> 'admin'));
     }else{                                    
     $this->Flash->error("Some thing Went Wrong .Please try again.");
       $this->redirect(array('controller'=>'users','action' => 'dashboard','prefix'=> 'admin'));
        }


   }
   
   //$SkillsData = $this->StaticPages->get($dashboardPageID);
   $this->set('data',$data); 
 }
 

function addsettingstables(){
  
}

function customerror(){
  
}








     /**
    * @Date: 20-sep-2016
    * @Method : dashboardeditor
    * @Purpose: This function is used to dashborad page to edit.
    * @Param: none
  * @Author: Neelam Thakur
    * @Return: none 
    **/
  function postAcceptor(){

    $this->viewBuilder()->layout('new_layout_admin');
    $this->set('title_for_layout','Dashboard Editor');
    $this->set("pageTitle","Dashboard Editor");
    $session = $this->request->session();
    $this->set('session',$session);

  // $this->set('SkillsData',$SkillsData); 
 }


 /**
    * @Date: 20-sep-2016
    * @Method : sampledocument
    * @Purpose: This function is used for Call API.
    * @Param: none
	* @Author: Karamvir Singh
    * @Return: none 
    **/
	
	function sampledocument(){
		$this->set('title_for_layout','Sample Document');
		$this->set("pageTitle","Sample Document");
		$settings = TableRegistry::get('Settings');
		$query = $settings->find()
		->select(['value'])
		->orWhere(['id' => 7])
		->orWhere(['id' => 8])
		->orWhere(['id' => 9])
		->orWhere(['id' => 10])
		->orWhere(['id' => 11]);
   $result = isset($query)? $query->toArray():array();
   $this->set("tagArry",$result);
 }
 
}


