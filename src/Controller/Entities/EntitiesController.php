<?php

namespace App\Controller\Entities;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Admin\UsersController;
use App\Controller\Skills\SkillsController;
use App\Controller\Employees\EmployeesController;
use App\Controller\Evaluations\EvaluationsController;
use App\Controller\Generics\GenericsController;
use App\Controller\Hardwares\HardwaresController;
use App\Controller\Milestones\MilestonesController;
use App\Controller\Component\ComponentController;
use App\Controller\CredentialAudits\CredentialAuditsController;
use App\Controller\Billings\BillingsController;
use App\Controller\Contacts\ContactsController;
use App\Controller\Resumes\ResumesController;
use App\Controller\StaticPages\Static_pagesController;
use App\Controller\Testimonials\TestimonialsController;
use App\Controller\Tickets\TicketsController;
use App\Controller\Permissions\PermissionsController;
use App\Controller\PaymentMethods\Payment_methodsController;
use App\Controller\Payments\PaymentsController;
use App\Controller\Reports\ReportsController;
use App\Controller\Profileaudits\ProfileauditsController;
use App\Controller\Sales\SalesController;
use App\Controller\LearningCenters\Learning_centersController;
use App\Controller\Leads\LeadsController;
use App\Controller\Credentials\CredentialsController;
use App\Controller\Attendances\AttendancesController;
use App\Controller\Appraisals\AppraisalsController;
use App\Controller\Projects\ProjectsController;
use Cake\Cache\Cache;



class EntitiesController extends AppController {

var $paginate		   =  array(); //pagination

    var $uses       	 =  array('Modules'); // For Default Model


    /******************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
            	  	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");

    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    // end code to check permissions
    	Router::parseNamedParams($this->request);
    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('layout_admin');

    	}

    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize  function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Agencies');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Skills');
        $this->loadModel('Modules');
        $this->loadModel('Tags');
        $this->loadModel('Entities');
        $this->loadModel('Roles');
        $this->loadModel('EntityAgency');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Permissions');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    /**                         
    * @Date: 24-feb -2017     
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    public function register(){
    	if($this->request->data){
    		//pr($this->request->data); die;
    	}
    }

    function list(){
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
      $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);  
      $this->set('title_for_layout','User Entities Listing');
      $this->set("pageTitle","User Entities Listing");   
      $this->set("search1", "");    
      $this->set("search2", "");  
      $criteria = ['is_deleted' => 'No'];

    //pr($criteria); die;
    // Delete user and its licences and orders(single/multiple)

    if(isset($this->request->data) || !empty($this->request->query)) {
        if(!empty($this->request->data['Entities']['fieldName']) || isset($this->request->query['field'])){

            if(trim(isset($this->request->data['Entities']['fieldName'])) != ""){
                $search1 = trim($this->request->data['Entities']['fieldName']);
            }elseif(isset($this->request->query['field'])){
                $search1 = trim($this->request->query['field']);
            }
            $this->set("search1",$search1);
        }

        if(isset($this->request->data['Entities']['value1']) || isset($this->request->data['Entities']['value2']) || isset($this->request->query['value'])){
            if(isset($this->request->data['Entities']['value1']) || isset($this->request->data['Entities']['value2'])){
                $search2 = ($this->request->data['Entities']['fieldName'] != "Entities.status")?trim($this->request->data['Entities']['value1']):$this->request->data['Entities']['value2'];
            }elseif(isset($this->request->query['value'])){
                $search2 = trim($this->request->query['value']);
            }
            $this->set("search2",$search2);
        }
        //echo $search1."------".$search2;
        /* Searching starts from here */
     



        if(!empty($search1) && (!empty($search2) || $search1 == "Entities.status")){

 $query  =    $this->Entities->find('all' , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
               ]);
             $result = isset($query)? $query->toArray():array();
      $criteria = $search1." LIKE '%".$search2."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
     
 }else{  
       $query  =  $this->Entities->find('all');   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }

  
 $urlString = "/";
      if(isset($this->request->query)){                   
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }

  $this->set('urlString', $urlString);
      $urlString = "/";  
      if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
    $this->set('urlString', $urlString);  

$this->paginate = [           
/*    'fields' => [ 
    'id',
    'agency_name',
    'first_name',
    'last_name',
    'username'
    ''
],*/     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     
//pr( $criteria); die;
$data = $this->paginate('Entities',['conditions' => $criteria]);  
$this->set('resultData', $data);  
$this->set('pagename',"Entitieslist");  

    }
    
     function add() {   
         $this->viewBuilder()->layout('new_layout_admin');
           $this->set('title_for_layout','Add Entity');    
           $this->pageTitle = "Add Entity";      
           $this->set("pageTitle","Add Role"); 
            $agencies = $this->Agencies->find('list',[
                'keyField' => 'id',
                'valueField' => 'agency_name',
            'conditions' => []
            ])->toArray();
            $roles = $this->Roles->find('list',[
                'keyField' => 'id',
                'valueField' => 'role',
                'conditions' => []
            ])->toArray();
           $mode = "add";   
           if($this->request->data){
            $entity = $this->Entities->newEntity($this->request->data());
            if(empty($entity->errors())){
             if($this->Entities->save($entity)){
                  foreach ($this->request->data['agency_id'] as $agency_id) {
                    foreach ($this->request->data['role_id'] as $role_id) {
                            $eadata['agency_id'] = $agency_id;
                            $eadata['role_id'] = $role_id;
                            $eadata['entity_id'] = $entity->id;
                            $entityagency = $this->EntityAgency->newEntity($eadata);
                            $this->EntityAgency->save($entityagency);
                    }
                      
                   }
                   Cache::clearGroup('short','short');
                   $this->Flash->success_new("Entities has been created successfully.");
                   $this->redirect(array('action' => 'list'));   
             }
         }else{
          $this->set("errors", $entity->errors());
         }
      } 
    $entities =  $this->Entities->find('list', [
                  'keyField' => 'id',
                  'valueField' => 'name',
                  'conditions' => [],
                  'fields' => ['id','name']
              ])->toArray();
    $this->set('entities',$entities);
    $this->set('agencies',$agencies);
    $this->set('roles',$roles);
} 
 



 function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
         $id = (is_numeric($id) ) ?  $id : $this->Common->DCR($this->Common->dcrForUrl( $id ) );
        if ($this->Entities->updateAll(['is_deleted' => 'Yes'],['id' => $id ]) ) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }

function edit($id = null) { 
   $session = $this->request->session();
   $userSession = $session->read("SESSION_ADMIN");
  if($this->request->is('ajax') == true){
    //  pr($this->request->data); die;
      if($this->request->data['type'] == 'name'){
        $this->Entities->updateAll(['name' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      } else if ($this->request->data['type'] == 'alias'){
          $this->Entities->updateAll(['alias' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      } else if ($this->request->data['type'] == 'notes'){
          $this->Entities->updateAll(['notes' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      }
      
      } 

}

function manage($id = null){
  $this->viewBuilder()->layout(false);
      if($this->request->is('ajax') == true){
        $agencies = $this->Agencies->find('list',[
                'keyField' => 'id',
                'valueField' => 'agency_name',
            'conditions' => []
            ])->toArray();
            
            $roles = $this->Roles->find('list',[
                'keyField' => 'id',
                'valueField' => 'role',
                'conditions' => []
            ])->toArray();

        $this->viewBuilder()->layout(false);
        $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));
            if($this->request->data['key'] =='init'){
                $entityagencies = $this->EntityAgency->find('all',[
                    'conditions' => ['entity_id' => $id]
                  ]);
                $entity = $this->Entities->get($id);
                $finalarr = [];
                foreach ($entityagencies as $value) {
                    $agency = $this->Agencies->get($value['agency_id']);
                    $role = $this->Roles->get($value['role_id']);
                    $value['Agency'] = $agency;
                    $value['Role'] = $role;
                    $finalarr[] = $value;
                }
                if(count(explode(',', $entity->tag_id)) > 0 ){
                    $selectedtags = $this->Tags->find('all',[
                    //'conditions' => ['FIND_IN_SET(\''. $id .'\',entity_id)']
                      'conditions' => ['id IN' => explode(',', $entity->tag_id)]
                    ])->all();
                }
                
                /*if(count($selectedtags) > 0){
                  $counter = 0;
                  foreach ($selectedtags as $tag) {
                    if(!in_array($id, explode(',', $tag['entity_id'])) )    {
                        unset($tag[$counter]);
                    }
                    $counter++ ;
                  }
                }*/
                    $tags = $this->Tags->find('all',[
                          'conditions' => ['is_deleted' => 'No']
                      ])->all();
                    $resultJ = json_encode(['response' => 'success','data' => $finalarr,'agencies' => $agencies,'roles' =>$roles ,'tags' => $tags , 'selectedtags' =>$selectedtags ]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='agencyroles'){
              $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));

                $entityagencies = $this->EntityAgency->find('all',[
                    'conditions' => ['entity_id' => $id ,'agency_id' => $this->request->data['agency_id']]
                  ]);
                    $finalarr = [];
                    $rolesonly = [];
                 foreach ($entityagencies as $value) {
                    $agency = $this->Agencies->get($value['agency_id']);
                    $role = $this->Roles->get($value['role_id']);
                    $value['Agency'] = $agency;
                    $value['Role'] = $role;
                    $rolesonly[] = $role['role'];
                    $finalarr[] = $value;
                }
                    $resultJ = json_encode(['response' => 'success','data' => $finalarr,'agencies' => $agencies,'roles' =>$roles , 'selectedroles' => $rolesonly ]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='addnewrole'){
                    $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));
                    $this->request->data['entity_id'] = $id;
                    $eadata = [];
                    $eadata['agency_id'] = $this->request->data['agency_id'];
                    $eadata['role_id'] = $this->request->data['role_id'];
                    $eadata['entity_id'] =  $this->request->data['entity_id'];
                    $entityagency = $this->EntityAgency->newEntity($eadata);
                    $this->EntityAgency->save($entityagency);

                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='removerole'){
                    $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));
                     $role = $this->Roles->find('all',[
                             'conditions'  => ['role' => ucwords($this->request->data['name']) ]
                      ])->first();
                    $e = $this->EntityAgency->find('all',[
                             'conditions'  => ['role_id' => $role['id'] , 'entity_id' => $id, 'agency_id' => $this->request->data['agency_id'] ]
                      ])->first();

                    $EntityAgency = $this->EntityAgency->get($e['id']);
                    $result = $this->EntityAgency->delete($EntityAgency);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if($this->request->data['key'] == 'addtag'){
                    $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));
                    $tag = $this->Entities->get($id);
                    $exp = explode(',', $tag['tag_id']);
                    $exp[] = $this->request->data['id'];
                    $this->Entities->updateAll(['tag_id' => implode(',', $exp) ],['id' => $id ]);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if($this->request->data['key'] == 'removetag'){
                    $id = (is_numeric($this->request->data['entity_id']) ) ? $this->request->data['entity_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id']));
                    $tag = $this->Entities->get($id);
                    $exp = explode(',', $tag['tag_id']);
                    $key  = array_search($this->request->data['id'], $exp);
                    if($key !== false){
                      unset($exp[$key]);
                    }
                    $this->Entities->updateAll(['tag_id' => implode(',', $exp) ],['id' =>  $id ]);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            }
            
      }
   $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));        
   $entity = $this->Entities->get($id);
    $this->set('entity',$entity);    
}



function edit_old($id = null) { 
    $this->viewBuilder()->layout('new_layout_admin');
    if($this->request->data){ 
      $id = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id']));  
          $entitydata = $this->Entities->get($id, [
                  'contain' => ['Entityagencies']
         ]);
          $this->request->data['id'] = $id;
          $entity = $this->Entities->patchEntity($entitydata,$this->request->data);
        if(empty($entity->errors())) {
          if($this->Entities->save($entity)){
              $agencies =[];
              $roles =[];
              foreach ($entitydata['Entityagencies'] as $e) {
                      if(!in_array($e['agency_id'],$this->request->data['agency_id'])){
                          $this->EntityAgency->deleteAll([
                                'id' => $e['id']
                            ]);
                      }
                      if(!in_array($e['role_id'],$this->request->data['role_id'])){
                          $this->EntityAgency->deleteAll([
                                'id' => $e['id']
                            ]);
                      }

                      $agencies[] = $e['agency_id'];
                      $roles[] = $e['role_id'];
                   }
              foreach ($this->request->data['agency_id'] as $agency_id) {
                //pr($agencies); die;
                      if(!in_array($agency_id , array_unique($agencies)) ){
                           foreach ($this->request->data['role_id'] as $role_id) {
                              //if(!in_array($role_id , array_unique($roles))){
                                $eadata['agency_id'] = $agency_id;
                                $eadata['role_id'] = $role_id;
                                $eadata['entity_id'] = $id;
                                $entityagency = $this->EntityAgency->newEntity($eadata);
                                $this->EntityAgency->save($entityagency);  
                         }
                      } else {
                         foreach ($this->request->data['role_id'] as $role_id) {
                              if(!in_array($role_id , array_unique($roles))){
                                $eadata['agency_id'] = $agency_id;
                                $eadata['role_id'] = $role_id;
                                $eadata['entity_id'] = $id;
                                $entityagency = $this->EntityAgency->newEntity($eadata);
                                $this->EntityAgency->save($entityagency);  
                              }
                         }
                      }   
                   }
          }
        $this->Flash->success_new(" updated successfully.");
        $this->redirect(array('controller' => 'Entities', 'action' => 'list')); 

      }else{ 

        $this->set("errors", $entity->errors());
      } 
    }else if(!empty($id)){
      $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));  
      $agencies = $this->Agencies->find('list',[
                'keyField' => 'id',
                'valueField' => 'agency_name',
            'conditions' => []
            ])->toArray();
            $roles = $this->Roles->find('list',[
                'keyField' => 'id',
                'valueField' => 'role',
                'conditions' => []
            ])->toArray();

           $mode = "add";  
        $this->set('roles',$roles);
        $this->set('agencies',$agencies);
        $RolesData = $this->Entities->get($id, [
                  'contain' => ['Entityagencies']
         ]);
     
      $this->set('RolesData',$RolesData); 
      if(!$RolesData){             
        $this->redirect(array('controller' => 'Entities', 'action' => 'list'));                 
      }                                                      
    }                                                          
  }    

   
    


} /**/// class ending brace

