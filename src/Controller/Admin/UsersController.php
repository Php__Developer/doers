<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please sloginee the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Admin;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Cache\Cache;
//use Cake\Http\Response;
use Cake\Auth\DefaultPasswordHasher;
//App::uses('Sanitize', 'Utility');
/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

//App::uses('Sanitize', 'Utility');
class UsersController extends AppController
{
		#_________________________________________________________________________#

    /**
    * @Date: 28-Dec-2011
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none
    * @Return: none 
    **/
    public function beforeFilter(Event $event) 	{
    	
    	parent::beforeFilter($event);
	    $this->Auth->allow(['forgotPassword', 'getCode','login','index','goto','detailspage','register']);
    	if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin" ){
    		$this->checkUserSession();
    	}else{
    		$this->viewBuilder()->layout('layout_admin');
	
    	}
    	$this->set('common',$this->Common);
    	$this->set("title_for_layout","ERP : Vlogic Labs - Relying on Commitment");


    }

    public function initialize()
    {
    	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('Employees');
        $this->loadModel('VlogicLabsUsers');
        $this->loadModel('VlogicLabsProjects');
        $this->loadModel('VlogicLabsContacts');
        $this->loadModel('VlogicCredentials');
        $this->loadModel('VlogicSaleProfiles');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Appraisals');
        $this->loadModel('Hardwares');
        $this->loadModel('Leads');
        $this->loadModel('Milestones');
        $this->loadModel('Modules');
        $this->loadModel('Payments');
        $this->loadModel('Projects');
        $this->loadModel('CredentialAudits');
        $this->loadModel('Contacts');
        $this->loadModel('Agencies');
        $this->loadModel('Resumes');
        $this->loadModel('Billings');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Settings');
        $this->loadModel('Profileaudits');
        $this->loadModel('Testimonials');
        $this->loadModel('Permissions');
        $this->loadModel('Modules');
        $this->loadModel('RememberTokens');
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
    }
    /*MERGING DBS*/
	/*public function ticketsdatacorrect($id){
		$tickets = $this->Tickets->find('all',[
			'conditions' => ['agency_id' => 1],
			'limit' => 20000,
			'offset' => $id
		])->all();	
		$allotherids = [];
		$todelete = [];
		foreach($tickets as $ticket){
			if(!in_array($ticket['other_id'], $allotherids)){
				array_push($allotherids, $ticket['other_id']);
			} else{
				array_push($todelete, $ticket['id']);
			}
		}
		 $this->Tickets->deleteAll(['id IN' => $todelete]);
		 die("OK"); 
	}*/

	/*public function oldalloteduserid(){
		$connection = ConnectionManager::get('test');
		$results = $connection->execute('SELECT * FROM sale_profiles')->fetchAll('assoc');
		//pr($results); die;
		foreach($results as $project){
		$resultdata= 	$this->SaleProfiles->find('all',[
					'conditions' => ['other_id' => $project['id'], 'agency_id' =>  $project['agency_id'] ]
				])->first();
		pr($resultdata['id']); 
		
			$data['alloteduserid'] = $project['alloteduserid'] ;
			$this->SaleProfiles->updateAll( $data,['id'=> $resultdata['id']]);

		}
		die("done")	;
	}*/


/*
	public function salesdatacorrect($id){
		$report_to = $this->SaleProfiles->find('list',[
					'keyField' => 'id',
					'valueField' => 'alloteduserid',
					'conditions' => ['agency_id' => $id]
				])->toArray();
		foreach($report_to as $k =>$v){
			if(!empty($v)){
				$exp = explode(',', $v);
				$employees = $this->Employees->find('list',[
					'keyField' => 'id',
					'valueField' => 'other_id',
					'conditions' => ['other_id IN' => $exp,'agency_id' => $id]
				])->toArray();
				$this->SaleProfiles->updateAll( ["alloteduserid"=>implode(',', array_keys($employees))],['id'=>$k]);
			}
		}
		die("DOne");
	}*/

/*	public function oldprojectid(){
		$connection = ConnectionManager::get('test');
		$results = $connection->execute('SELECT * FROM projects')->fetchAll('assoc');
		foreach($results as $project){
		$resultdata= 	$this->Projects->find('all',[
					'conditions' => ['other_id' => $project['id'], 'agency_id' =>  $project['agency_id'] ]
				])->first();
	
			$data['profile_id'] = $project['profile_id'] ;
			$this->Projects->updateAll( $data,['id'=> $resultdata['id']]);

		}
		die("done")	;
	}*/

/*	public function newprofileid($id){
			$results = 	$this->Projects->find('all',[
					'conditions' => ['agency_id' =>  $id ]
				,'fields' =>	['id','profile_id']
				])->all();
			//pr($results); die;
		foreach($results as $project){
		$resultdata = 	$this->SaleProfiles->find('all',[
					'conditions' => ['other_id' => $project['profile_id'], 'agency_id' =>  $id ]
				])->first();
		pr($project['id']);
			$data['profile_id'] = $resultdata['id'] ;
			$this->Projects->updateAll( $data,['id'=> $project['id']]);

		}
		die("done")	;
	}*/

/*	public function updateagencyid(){
		$this->Appraisals->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Attendances->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Billings->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Contacts->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Credentials->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->CredentialAudits->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->CredentialLogs->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Evaluations->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Leads->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Payments->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Profileaudits->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Projects->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->SaleProfiles->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->StaticPages->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Testimonials->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Users->updateAll( ["agency_id"=>3],['id !='=>0]);
		$this->Tickets->updateAll( ["agency_id"=>3],['id !='=>0]);
		pr("DONE BRO");
		die;
	}*/
	/*public function credentialscrone(){
		$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM credentials')->fetchAll('assoc');
	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
	    		$user['forworder_email'] = (!empty($user['forworder_email'])) ? $user['forworder_email'] : 'Empty' ;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;

				$empoyess = $this->Credentials->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Credentials->newEntity($user);
           if (empty($users->errors())){
           			 $this->Credentials->save($users);
         		  } else {
         		  	pr($user->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("Credentials Import DONE");
	}*/



/*    public function setusercrone(){
    $connection = ConnectionManager::get('test');
	$result = $connection->execute('SELECT * FROM users WHERE status = :status', ['status' => 1])->fetchAll('assoc');
    	foreach($result as $user){
    		if(!in_array($user['id'], [0])){
	    		$id = $user['id'];
	    		 unset($user['id']);
	    		if(!empty($user['skype'])){
	    			$skype = $this->Credentials->find('all',[
					'conditions' => ['other_id' => $user['skype'],'agency_id' => 1 ]
					])->first();	
					if(count($skype) > 0){
						$user['skype'] = $skype->id;
					}	
	    		}
	    		if(!empty($user['dropbox'])){
	    			$dropbox = $this->Credentials->find('all',[
					'conditions' => ['other_id' => $user['dropbox'],'agency_id' => 1 ]
					])->first();	
					if(count($dropbox) > 0){
						$user['dropbox'] = $dropbox->id;
					}	
	    		}
	    		$user['other_id'] = $id;
				$user['dob'] = date_create($user['dob'])->format('Y-m-d');
				$user['doj'] = date_create($user['doj'])->format('Y-m-d');
				$user['dor'] = date_create($user['dor'])->format('Y-m-d');
				$user['bankName'] = (!empty($user['bankName'])) ? $user['bankName'] : 'Empty' ;
				$user['branch'] = (!empty($user['branch'])) ? $user['branch'] : 'Empty' ;
				$user['full_address'] = (!empty($user['full_address'])) ? $user['full_address'] : 'Empty' ;
				$user['ifsc'] = (!empty($user['ifsc'])) ? $user['ifsc'] : 'Empty' ;
				$user['personal_email'] = (!empty($user['personal_email'])) ? $user['personal_email'] : 'Empty' ;
				$user['username'] = (!empty($user['username'])) ? $user['username'] : 'NA' ;
				$user['password'] = substr($user['first_name'],0,1).'3651';
				$empoyess = $this->Employees->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
				} else{
			$users = $this->Employees->newEntity($user);
           if (empty($users->errors())){
           			 $this->Employees->save($users);
         		  } else {
         		  	pr($user['other_id']);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
			
    		}
    		
    	}
    	die("DONE");
    }*/

/*    public function credentialsuseridmerge($id){

	$empoyess = $this->Credentials->find('all',[
		'conditions' => ['agency_id' => $id]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['user_id'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['user_id'],'agency_id' => 1]
				])->first();
				if(count($report_to) > 0){
					$data['user_id'] = $report_to->id;
					$skill = $this->Credentials->get($user['id']);
					$skills = $this->Credentials->patchEntity($skill,$data,['validate' => false]);
					$this->Credentials->save($skill);
				}
			}
		}
		die("credentialsuseridmerge done");
}*/





/*
public function reporttomerge($id){
	$empoyess = $this->Employees->find('all',[
		'conditions' => ['agency_id' => $id]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['report_to'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['report_to'],'agency_id' => 1]
				])->first();
				if(count($report_to) > 0){
					$data['report_to'] = $report_to->id;
					$data['is_updated'] = 'Yes';
					$skill = $this->Employees->get($user['id']);
					$skills = $this->Employees->patchEntity($skill,$data,['validate' => false]);
					$this->Employees->save($skill);
				}
			}
		}
		die("DDD");
}*/

/*public function contactssmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM contacts')->fetchAll('assoc');

	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
	    		$user['source'] = (!empty($user['source'])) ? $user['source'] : 'Empty' ;
	    		$user['cpassword'] = (!empty($user['cpassword'])) ? $user['cpassword'] : 'Empty' ;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Contacts->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
				} else{
			$users = $this->Contacts->newEntity($user);
           if (empty($users->errors())){
           			 $this->Contacts->save($users);
         		  } else {
         		  	pr($user->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("Contacts Import DONE");
}*/

/*public function saleprofilesmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM sale_profiles')->fetchAll('assoc');
	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->SaleProfiles->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 2]
					])->all();
				pr($empoyess); die;
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
					pr($user); die;
			$users = $this->SaleProfiles->newEntity($user);
           if (empty($users->errors())){
           			 $this->SaleProfiles->save($users);
         		  } else {
         		  	pr($user->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("SaleProfiles Import DONE");
}*/

/*public function projectsmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM projects')->fetchAll('assoc');

	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
	    		$user['credentials'] = (!empty($user['credentials'])) ? $user['credentials'] : 'Empty' ;
	    		$contact = $this->Contacts->find('all',[
	    			'conditions' => ['other_id' => $user['client_id'] ,'agency_id' => 1]
				])->first();
				$user['client_id'] = (count($contact) > 0) ? $contact['id'] : 0;
				$empoyess = $this->Projects->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
				} else{
			$users = $this->Projects->newEntity($user);
           if (empty($users->errors())){
           			 $this->Projects->save($users);
         		  } else {
         		  	pr($user->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("ALL DONE");
}*/
/*FOR UPDATING pm_id field in the projects table*/
/*public function pmidmerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['pm_id'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['pm_id'],'agency_id' => 1]
				])->first();
				//if(count($report_to) > 0){
					$data['pm_id'] = (count($report_to) > 0) ? $report_to['id'] : 0;
					//$data['pm_id'] = $report_to->id;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("DDD");
}*/

/*FOR UPDATING pm_id field in the projects table*/
/*public function engineeringusermerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['engineeringuser'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['engineeringuser'],'agency_id' => 1]
				])->first();
				//pr($user); 
				//pr($report_to);die;
				$data['engineeringuser'] = (count($report_to) > 0) ? $report_to['id'] : 0;
				//if(count($report_to) > 0){
					//$data['engineeringuser'] = $report_to->id;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("engineeringuser Updated");
}*/

/*public function salesfrontusermerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['salesfrontuser'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['salesfrontuser'],'agency_id' => 1]
				])->first();
					$data['salesfrontuser'] = (count($report_to) > 0) ? $report_to['id'] : 0;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("salesfrontuser Updated");
}*/

/*public function salesbackendusermerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['salesbackenduser'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['salesbackenduser'],'agency_id' => 1]
				])->first();
					$data['salesbackenduser'] = (count($report_to) > 0) ? $report_to['id'] : 0;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("salesbackenduser Updated");
}*/

/*public function teamidmerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['team_id'])){
				$exp = explode(',', $user['team_id']);
				$report_to = $this->Employees->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['other_id IN' => $exp,'agency_id' => 1],
					['id'],
				])->toArray();
					$data['team_id'] = (count($report_to) > 0) ? implode(',', array_keys($report_to)) : 0;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("team id  Updated");
}*/

/*public function lastidmerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['last_id'])){
				$report_to = $this->Employees->find('all',[
					'conditions' => ['other_id' => $user['last_id'],'agency_id' => 1]
				])->first();
					$data['last_id'] = (count($report_to) > 0) ? $report_to['id'] : 0;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("lastidmerge Updated");
}*/

/*public function profileidmerge(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	foreach($empoyess as $user){
			if(!empty($user['profile_id'])){
				$report_to = $this->SaleProfiles->find('all',[
					'conditions' => ['other_id' => $user['profile_id'],'agency_id' => 1]
				])->first();

				//if(count($report_to) > 0){
					$data['profile_id'] = (count($report_to) > 0) ? $report_to['id'] : 0;
					//$data['profile_id'] = $report_to->id;
					$data['is_updated'] = 'Yes';
					$skill = $this->Projects->get($user['id']);
					$skills = $this->Projects->patchEntity($skill,$data,['validate' => false]);
					$this->Projects->save($skill);
				//}
			}
		}
		die("profileidmerge Updated");
}*/

/*public function ticketsmerge(){
	$empoyess = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1]
				])->all();
	$alreadyinserted = $this->Tickets->find('list',[
					'keyField' => 'other_id',
					'valueField' => 'other_id',
					'conditions' => ['agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
	$connection = ConnectionManager::get('test');
	foreach($empoyess as $user){
	ini_set('max_execution_time', 300);	
	$result = $connection->execute('SELECT * FROM tickets WHERE to_id = :to_id', ['to_id' => $user['other_id']  ] )->fetchAll('assoc');
		if(count($result)){
			foreach($result as $ticket){
				if(!in_array($ticket['id'], array_keys($alreadyinserted))){
		
				$id = $ticket['id'];
	    		 unset($ticket['id']);
	    		$ticket['other_id'] = $id;
	    		$ticket['response'] = ltrim($ticket['response']);
	    		$ticket['response']=  rtrim($ticket['response']);
	    		$ticket['title'] = ltrim($ticket['title']);
	    		$ticket['title']=  rtrim($ticket['title']);

	    		$ticket['title'] = (empty($ticket['title']) ) ? 'untitled' : $ticket['title']  ;
	    		$ticket['description'] = (empty($ticket['description']) ) ? 'description' : $ticket['description']  ;
	    		$ticket['response'] = ($ticket['response'] !== null ) ? $ticket['response'] : 'No Response Yet!' ;
	    		$ticket['response'] = ($ticket['response'] == '' || $ticket['response'] == ' ' ) ? 'No Response Yet!' : $ticket['response'] ;
	    		$ticket['response'] = (isset($ticket['response'] ) ) ? $ticket['response'] :'No Response Yet!'  ;
				$tickets = $this->Tickets->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($tickets)  > 0){
					// do nothing if there is already a record
				} else{
				//pr($ticket); die;
			$users = $this->Tickets->newEntity($ticket);
           if (empty($users->errors())){
           			 $this->Tickets->save($users);
           			 pr($user['id'].' Tickets Updated !' );
           			// die("addd");
         		  } else {
         		  	pr($user['other_id']);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}

			}

			}
		}
			
		}
		die("ticketsmerge Updated");
}*/

/*public function toidmerge(){
	$empoyess = $this->Employees->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	ini_set('max_execution_time', 300);	
	foreach($empoyess as $user){
			if(!empty($user['other_id'])){
				$report_to = $this->Tickets->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['to_id' => $user['other_id'],'agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
				if(count($report_to) > 0){
					$this->Tickets->updateAll( ["to_id"=>$user['id'],'is_updated' => 'Yes'],['id IN'=>array_keys($report_to)]);
					pr($user['id'].' Tickets Updated !' );
				}
			}
		}
		die("toidmerge Updated");
}*/

/*public function fromidmerge(){
	$empoyess = $this->Employees->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	ini_set('max_execution_time', 300);	
	foreach($empoyess as $user){
			if(!empty($user['other_id'])){
				$report_to = $this->Tickets->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['from_id' => $user['other_id'],'agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
				//pr($report_to);
				if(count($report_to) > 0){
					$this->Tickets->updateAll( ["from_id"=>$user['id'],'is_updated' => 'Yes'],['id IN'=>array_keys($report_to)]);
					pr($user['id'].' Tickets Updated !' );
				}
			}
		}
		die("fromidmerge Updated");
}*/

/*public function lastrepliermerge(){
	$empoyess = $this->Employees->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	ini_set('max_execution_time', 300);	
	foreach($empoyess as $user){
			if(!empty($user['other_id'])){
				$report_to = $this->Tickets->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['last_replier' => $user['other_id'],'agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
				//pr($report_to);
				if(count($report_to) > 0){
					$this->Tickets->updateAll( ["last_replier"=>$user['id'],'is_updated' => 'Yes'],['id IN'=>array_keys($report_to)]);
					pr($user['id'].' Tickets Updated !' );
				}
			}
		}
		die("lastrepliermerge Updated");
}*/

/*public function projectofticket(){
	$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 1]
		])->all();
	ini_set('max_execution_time', 300);	
	foreach($empoyess as $user){
			if(!empty($user['other_id'])){
				$report_to = $this->Tickets->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['project_id' => $user['other_id'],'agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
				if(count($report_to) > 0){
					$this->Tickets->updateAll( ["project_id"=>$user['id'],'is_updated' => 'Yes'],['id IN'=>array_keys($report_to)]);
					pr($user['id'].' Tickets Updated !' );
				}
			}
		}
		die("lastrepliermerge Updated");
}*/

/*public function appraisalsmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM appraisals')->fetchAll('assoc');
	foreach($results  as $user){
		ini_set('max_execution_time', 300);	
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['user_id']]
				])->first();
				$user['user_id'] = isset($userid) ? $userid['id'] : 0;
				$supervisorid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['supervisor_id']]
				])->first();
				$user['supervisor_id'] = isset($supervisorid) ? $supervisorid['id'] : 0;
				$lastupdated = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['last_updated']]
				])->first();
				$user['last_updated'] = (isset($user['last_updated']) ) ? $user['last_updated'] : 0;
				$user['technical_comment'] = (isset($user['technical_comment']) && !empty($user['technical_comment']) ) ? $user['technical_comment'] : 'No Comments';
				$user['communication_comment'] = (isset($user['communication_comment']) && !empty($user['communication_comment']) ) ? $user['communication_comment'] : 'No Comments';
				$user['interpersonal_comment'] = (isset($user['interpersonal_comment']) && !empty($user['interpersonal_comment']) ) ? $user['interpersonal_comment'] : 'No Comments';
				$user['supervisor_comment'] = (isset($user['supervisor_comment']) && !empty($user['supervisor_comment'])) ? $user['supervisor_comment'] : 'No Comments';

	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Appraisals->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				//pr($user); die;
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Appraisals->newEntity($user);
           if (empty($users->errors())){
           			 $this->Appraisals->save($users);
         		  } else {
         		  	var_dump($user['technical_comment']); die;
         		  	pr($user->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("Appraisals Import DONE");
}*/


/*public function attendancemerge(){
	$empoyess = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1]
				])->all();
	$alreadyinserted = $this->Attendances->find('list',[
					'keyField' => 'other_id',
					'valueField' => 'other_id',
					'conditions' => ['agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
	$connection = ConnectionManager::get('test');
	foreach($empoyess as $user){
		
	$result = $connection->execute('SELECT * FROM attendances WHERE user_id = :user_id', ['user_id' => $user['other_id']  ] )->fetchAll('assoc');
	ini_set('max_execution_time', 300);	
		if(count($result)){
			foreach($result as $ticket){
				if(!in_array($ticket['id'], array_keys($alreadyinserted))){
				$id = $ticket['id'];
	    		 unset($ticket['id']);
	    		$ticket['other_id'] = $id;
	    		$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $ticket['user_id']]
				])->first();
				$ticket['user_id'] = isset($userid) ? $userid['id'] : 0;
				$ticket['leave_reason'] = (isset($ticket['leave_reason']) && !empty($ticket['leave_reason']) ) ? $ticket['leave_reason'] : 'Empty';
				$tickets = $this->Attendances->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($tickets)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Attendances->newEntity($ticket);
           if (empty($users->errors())){
           			 $this->Attendances->save($users);
         		  } else {
         		  	pr($ticket);
         		  	pr($ticket->other_id);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}

			}

			}
		}
			
		}
		die("attendancemerge Updated");
}*/


/*public function billingssmerge(){
	$connection = ConnectionManager::get('test');
	ini_set('max_execution_time', 300);	
	$results = $connection->execute('SELECT * FROM billings')->fetchAll('assoc');
	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['user_id']]
				])->first();
				$user['user_id'] = isset($userid) ? $userid['id'] : 0;
				pr($user['project_id']);
				$projectid = $this->Projects->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['project_id']]
				])->first();
				$user['project_id'] = (isset($projectid) && count($projectid) > 0) ? $projectid['id'] : 0;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Billings->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();

				
				if(count($empoyess)  > 0 || $user['project_id'] == 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Billings->newEntity($user);
           if (empty($users->errors())){
           			 $this->Billings->save($users);
         		  } else {
         		  	pr($id);
         		  	pr($user);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("billingssmerge Import DONE");
}*/


/*public function evaluationsmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM evaluations')->fetchAll('assoc');
	foreach($results  as $user){
				ini_set('max_execution_time', 300);	
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['user_id']]
				])->first();
				$user['user_id'] = isset($userid) ? $userid['id'] : 0;
				$seniorid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['senior_id']]
				])->first();
				$user['senior_id'] = isset($seniorid) ? $seniorid['id'] : 0;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Evaluations->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				//pr($user); die;
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Evaluations->newEntity($user);
           if (empty($users->errors())){
           			 $this->Evaluations->save($users);
         		  } else {
         		  	//var_dump($user['technical_comment']); die;
         		  	//pr($user->other_id);
         		  	pr($user);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("evaluationsmerge Import DONE");
}*/

/*public function hardwaresmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM hardwares')->fetchAll('assoc');
	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['alloted_to']]
				])->first();
				$user['alloted_to'] = isset($userid) ? $userid['id'] : 0;
				$user['current_location'] = (isset($user['current_location']) && !empty($user['current_location']) ) ? $user['current_location'] : 'Empty';
				$user['others'] = (!empty($user['others']) ) ? $user['others'] : 'NONE' ;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Hardwares->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Hardwares->newEntity($user);
           if (empty($users->errors())){
           			 $this->Hardwares->save($users);
         		  } else {
         		  	//var_dump($user['technical_comment']); die;
         		  	//pr($user->other_id);
         		  	pr($user);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("evaluationsmerge Import DONE");
}
*/

/*public function leadsmerge(){ 

		$empoyess = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1]
				])->all();
	$alreadyinserted = $this->Leads->find('list',[
					'keyField' => 'other_id',
					'valueField' => 'other_id',
					'conditions' => ['agency_id' => 1 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($alreadyinserted); die;
	$connection = ConnectionManager::get('test');
	foreach($empoyess as $user){
		
	$result = $connection->execute('SELECT * FROM leads WHERE salesfront_id = :user_id', ['user_id' => $user['other_id']  ] )->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				ini_set('max_execution_time', 300);	
				if(!in_array($user['id'], array_keys($alreadyinserted))){
						
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$lead['salesfront_id'] = $user['id'];
					$profileid = $this->SaleProfiles->find('all',[
					'conditions' => ['agency_id' => 1,'other_id' => $lead['profile_id']]
					])->first();
					$lead['profile_id'] = isset($profileid) ? $profileid['id'] : 0;
					$lead['perspective_amount'] = (isset($lead['perspective_amount']) && !empty($lead['perspective_amount']) ) ? $lead['perspective_amount'] : 0 ;
					$lead['time_spent'] = (isset($lead['time_spent']) && !empty($lead['time_spent']) && $lead['time_spent'] !== null && $lead['time_spent'] !== NULL) ? $lead['time_spent'] : "00:00" ;
					
		    		$lead['created'] = (!empty($lead['created']) || $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) || $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->Leads->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					//pr($user); die;
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->Leads->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->Leads->save($users);
	         		  } else {
	         		  	var_dump($user['time_spent']);
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			}

			}
		}
			
		}
		die("leadsmerge Updated");
}*/

/*public function milestonesmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM milestones')->fetchAll('assoc');
	foreach($results  as $user){
				ini_set('max_execution_time', 300);	
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['user_id']]
				])->first();
				$user['user_id'] = isset($userid) ? $userid['id'] : 0;
				$projectid = $this->Projects->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['project_id']]
				])->first();
				$user['project_id'] = isset($projectid) ? $projectid['id'] : 0;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Milestones->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Milestones->newEntity($user);
           if (empty($users->errors())){
           			 $this->Milestones->save($users);
         		  } else {
         		  	//var_dump($user['technical_comment']); die;
         		  	//pr($user->other_id);
         		  	pr($user);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("billingssmerge Import DONE");
}*/

/*public function paymentsmerge(){
	$connection = ConnectionManager::get('test');
	$results = $connection->execute('SELECT * FROM payments')->fetchAll('assoc');
	foreach($results  as $user){
				$id = $user['id'];
	    		 unset($user['id']);
	    		$user['other_id'] = $id;
				$userid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['responsible_id']]
				])->first();
				$user['responsible_id'] = isset($userid) ? $userid['id'] : 0;

				$entryid = $this->Employees->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['entry_id']]
				])->first();
				$user['entry_id'] = isset($entryid) ? $entryid['id'] : 0;
				$projectid = $this->Projects->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['project_id']]
				])->first();
				$user['project_id'] = isset($projectid) ? $projectid['id'] : 0;

				$billingid = $this->Billings->find('all',[
				'conditions' => ['agency_id' => 1,'other_id' => $user['billing_id']]
				])->first();
				$user['billing_id'] = isset($billingid) ? $billingid['id'] : 0;
				
				$user['tds_amount'] = (!empty($user['tds_amount'])) ? $user['tds_amount'] : 0 ;
				$user['note'] = (!empty($user['note'])) ? $user['note'] : 'NA' ;
	    		$user['created'] = (!empty($user['created']) || $user['created'] !== null) ? $user['created'] : date('Y-m-d H:i:s') ;
          		$user['modified'] = (!empty($user['modified']) || $user['modified'] !== null) ? $user['modified'] : date('Y-m-d H:i:s') ;
				$empoyess = $this->Payments->find('all',[
					'conditions' => ['other_id' => $id,'agency_id' => 1]
					])->all();
				//pr($user); die;
				if(count($empoyess)  > 0){
					// do nothing if there is already a record
				} else{
			$users = $this->Payments->newEntity($user);
           if (empty($users->errors())){
           			 $this->Payments->save($users);
         		  } else {
         		  	//var_dump($user['technical_comment']); die;
         		  	//pr($user->other_id);
         		  	pr($user);
         		  	pr($users->errors());
         		  	die;
         		  }	
				}
	}
	die("billingssmerge Import DONE");
}*/

/*public function credentialsauditsmerge(){
		$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM credential_audits')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				//if(!in_array($user['id'], array_keys($alreadyinserted))){
					ini_set('max_execution_time', 300);			
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$profileid = $this->SaleProfiles->find('all',[
					'conditions' => ['agency_id' => 1,'other_id' => $lead['profile_id']]
					])->first();
					$lead['profile_id'] = isset($profileid) ? $profileid['id'] : 0;
					
					$entryid = $this->Employees->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['resolvedby']]
						])->first();
					$lead['resolvedby'] = isset($entryid) ? $entryid['id'] : 0;

					$createdby = $this->Employees->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['created_by']]
						])->first();
					$lead['created_by'] = isset($createdby) ? $createdby['id'] : 0;

		    		$lead['created'] = (!empty($lead['created']) || $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) || $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->CredentialAudits->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					//pr($user); die;
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->CredentialAudits->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->CredentialAudits->save($users);
	         		  } else {
	         		  	var_dump($user['time_spent']);
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("credentials audit merge done")	;
}*/


/*public function credentiallogsmerge(){
		$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM credential_logs')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				//if(!in_array($user['id'], array_keys($alreadyinserted))){
					ini_set('max_execution_time', 300);		
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$entryid = $this->Employees->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['user_id']]
						])->first();
					$lead['user_id'] = isset($entryid) ? $entryid['id'] : 0;

					$createdby = $this->Credentials->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['credential_id']]
						])->first();
					$lead['credential_id'] = isset($createdby) ? $createdby['id'] : 0;

		    		$lead['created'] = (!empty($lead['created']) || $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) || $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->CredentialLogs->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					//pr($user); die;
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->CredentialLogs->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->CredentialLogs->save($users);
	         		  } else {
	         		  
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("credentials logs merge done")	;
}
*/
/*
public function profilesauditsmerge(){
		$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM profileaudits')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				//if(!in_array($user['id'], array_keys($alreadyinserted))){
					ini_set('max_execution_time', 300);		
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$profileid = $this->SaleProfiles->find('all',[
					'conditions' => ['agency_id' => 1,'other_id' => $lead['profile_id']]
					])->first();
					$lead['profile_id'] = isset($profileid) ? $profileid['id'] : 0;
					
					$entryid = $this->Employees->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['resolvedby']]
						])->first();
					$lead['resolvedby'] = isset($entryid) ? $entryid['id'] : 0;

					$createdby = $this->Employees->find('all',[
						'conditions' => ['agency_id' => 1,'other_id' => $lead['created_by']]
						])->first();
					$lead['created_by'] = isset($createdby) ? $createdby['id'] : 0;

		    		$lead['created'] = (!empty($lead['created']) || $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) || $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->Profileaudits->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					//pr($user); die;
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->Profileaudits->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->Profileaudits->save($users);
	         		  } else {
	         		  	var_dump($user['time_spent']);
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("Profileaudits audit merge done")	;
}*/

/*public function resumesmerge(){
		$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM resumes')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				//if(!in_array($user['id'], array_keys($alreadyinserted))){
						//pr($lead); die;
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
		    		$lead['agency_id'] = 1;
		    		if(isset($lead['interview_Date'])){ 
		    			if($lead['interview_Date'] == '0000-00-00')	{
						$lead['interview_Date'] = date('Y-m-d');
						}
		    		} else{
		    			$lead['interview_Date'] = date('Y-m-d');
		    		}
					if(isset($lead['created'])){
						if($lead['created'] == '0000-00-00 00:00:00')	{
							$lead['created'] = date('Y-m-d H:i:s');
						}
					}else{
						$lead['created'] = date('Y-m-d H:i:s');
					}
					
					$empoyess = $this->Resumes->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
						
				$users = $this->Resumes->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->Resumes->save($users);
	         		  } else {
	         		  	var_dump($user['time_spent']);
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("resumesmerge audit merge done")	;
}
*/
/*public function resumesdatacorrect(){
		//$connection = ConnectionManager::get('test');
		$result = $this->Resumes->find('all',[
			])->all();
		if(count($result) > 0){
			foreach($result as $lead){
						$id = $lead['id'];
		    		 unset($lead['id']);
					$lead['interview_Date'] = date('Y-m-d');
						$lead['created'] = date('Y-m-d H:i:s');
					$empoyess = [];
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
						//pr($lead); die;
					$skill = $this->Resumes->get($id);
					$skills = $this->Resumes->patchEntity($skill,$lead->toArray(),['validate' => false]);
					$this->Resumes->save($skill);
					}	




			//}

			}
		}
	die("resumesdatacorrect audit merge done")	;
}*/
/*
public function settingstablemerge(){
		$connection = ConnectionManager::get('test');
		$result = $this->Settings->find('all',[
			])->all();
		if(count($result) > 0){
			foreach($result as $lead){
				if($lead['key'] == 'ip_address'){
					$exp = explode(',', $lead['value']);
					$results = $connection->execute('SELECT * FROM settings')->fetchAll('assoc');

					foreach($results as $data){
						if($data['key'] == 'ip_address'){
							$exp1 = explode(',', $data['value']);
							$datas['value'] = implode(',', array_merge($exp,$exp1));
							$result= $this->Settings->find("all" , [
							'conditions' => ['Settings.key' => 'ip_address'],
							])->first();
							$skill = $this->Settings->get($result->id);
							$skills = $this->Settings->patchEntity($skill,$datas,['validate' => false]);
							$this->Settings->save($skill);
						}	
					}
				}

				if($lead['key'] == 'email_address'){
					$exp = explode(',', $lead['value']);
					$results = $connection->execute('SELECT * FROM settings')->fetchAll('assoc');
					//pr($results); die;
					foreach($results as $data){
						if($data['key'] == 'email_address'){
							$exp1 = explode(',', $data['value']);
							$data2['value'] = implode(',', array_merge($exp,$exp1));
							
							$result= $this->Settings->find("all" , [
							'conditions' => ['Settings.key' => 'email_address'],
							])->first();
							$skill = $this->Settings->get($result->id);
							$skills = $this->Settings->patchEntity($skill,$data2,['validate' => false]);
							$this->Settings->save($skill);
						}	
					}
				}
			}
		}
	die("settingstablemerge audit merge done")	;
}
*/

/*public function staticpagesmerge(){
				$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM static_pages')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$exp = explode(',', $lead['user_id']); 

					$usersdata = $this->Employees->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['other_id IN' => $exp ],
					'fields' => ['id','other_id']
					])->toArray();

					$lead['user_id'] = isset($usersdata) ? implode(',', array_keys($usersdata)) : 0;
					$lead['title'] = (!empty($lead['title'])) ? $lead['title'] : 'NONE' ;
					$lead['category'] = (!empty($lead['category'])) ? $lead['category'] : 'NONE' ;
		    		$lead['created'] = (!empty($lead['created']) || $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) || $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->StaticPages->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					//pr($user); die;
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->StaticPages->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->StaticPages->save($users);
	         		  } else {
	         		  	
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("StaticPages audit merge done")	;
}
*/

/*
public function testimonialsmerge(){
				$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM testimonials')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
				//if(!in_array($user['id'], array_keys($alreadyinserted))){
						
					$id = $lead['id'];
		    		 unset($lead['id']);
		    		$lead['other_id'] = $id;
					$usersdata = $this->Employees->find('all',[
					'conditions' => ['other_id' => $lead['user_id'] ],
					'fields' => ['id','other_id']
					])->first();
					$lead['user_id'] = isset($usersdata) ? $usersdata['id'] : 0;
		    		$lead['created'] = (!empty($lead['created']) && $lead['created'] !== null) ? $lead['created'] : date('Y-m-d H:i:s') ;
	          		$lead['modified'] = (!empty($lead['modified']) && $lead['modified'] !== null) ? $lead['modified'] : date('Y-m-d H:i:s') ;
					$empoyess = $this->Testimonials->find('all',[
						'conditions' => ['other_id' => $id,'agency_id' => 1]
						])->all();
					if(count($empoyess)  > 0){
						// do nothing if there is already a record
					} else{
				$users = $this->Testimonials->newEntity($lead);
	           if (empty($users->errors())){
	           			 $this->Testimonials->save($users);
	         		  } else {
	         		  	
	         		  	//pr($user->other_id);
	         		  	pr($lead);
	         		  	pr($users->errors());
	         		  	die;
	         		  }	
					}	




			//}

			}
		}
	die("testimonialsmerge audit merge done")	;
}*/

/*public function permissionsmerge(){
				$connection = ConnectionManager::get('test');
		$result = $connection->execute('SELECT * FROM permissions')->fetchAll('assoc');

		if(count($result) > 0){
			foreach($result as $lead){
					$exp = explode(',', $lead['user_id']); 
					unset($lead['id']);
					$usersdata = $this->Employees->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['other_id IN' => $exp ],
					'fields' => ['id','other_id']
					])->toArray();

					$othertableusers = (isset($usersdata) && count($usersdata) > 0) ? array_keys($usersdata) : [];
					$currentperms = $this->Permissions->find('all',[
						'conditions' => ['alias' => $lead['alias']]
						])->first();
					$finalarr = [];
					
					//($lead['action'] == 'employeelist') ? pr($currentperms) : "";
					//$currenttbleusers = (isset($currentperms) && count($currentperms) > 0) ? explode(',', $currentperms['user_id']) : [];
					//$finalarr = array_merge($currenttbleusers,$othertableusers);
					if(count($currentperms) > 0){
						$currentexplodedids = explode(',', $currentperms['user_id']);
						if(count($currentperms) > 0){
							foreach($othertableusers  as $exid){
								if(!in_array($exid, $currentexplodedids)){
									array_push($currentexplodedids, $exid);
								}
							}
						}

						$data2['user_id'] = implode(',', $currentexplodedids);
						$this->Permissions->updateAll( $data2,['alias'=>$lead['alias']]);
						//$skill = $this->Permissions->get($currentperms->id);
						//$skills = $this->Permissions->patchEntity($skill, $data2 ,['validate' => false]);
						//$this->Permissions->save($skill);
					}else{
							$users = $this->Permissions->newEntity($lead);
				           if (empty($users->errors())){
				           			 $this->Permissions->save($users);
				         		  } else {
				         		  	var_dump($user['time_spent']);
				         		  	//pr($user->other_id);
				         		  	pr($lead);
				         		  	pr($users->errors());
				         		  	die;
				         		  }
						// if no record found related to current alias  here will be new record insertion
						
					}

			}
		}
	die("StaticPages audit merge done")	;
}
*/

/*public function deleteifneeded($table){
	if($table == 'users'){
		$empoyess = $this->Employees->find('all',[
		'conditions' => ['agency_id' => 2]
		])->all();
	//pr($empoyess); die;
		foreach($empoyess as $user){
			$entity = $this->Employees->get($user->id);
			$result = $this->Employees->delete($entity);
		}
		die("DELETED");
	} else if($table == 'cre'){
			$empoyess = $this->Credentials->find('all',[
		'conditions' => ['agency_id' => 2]
		])->all();
		foreach($empoyess as $user){
			$entity = $this->Credentials->get($user->id);
			$result = $this->Credentials->delete($entity);
		}
		die("DELETED");
	} else if($table == 'projects'){
			$empoyess = $this->Projects->find('all',[
		'conditions' => ['agency_id' => 2]
		])->all();
		foreach($empoyess as $user){
			$entity = $this->Projects->get($user->id);
			$result = $this->Projects->delete($entity);
		}
		die("Projects DELETED");
	} else if($table == 'contacts'){
			$empoyess = $this->Contacts->find('all',[
			'conditions' => ['agency_id' => 2]
		])->all();
		foreach($empoyess as $user){
			$entity = $this->Contacts->get($user->id);
			$result = $this->Contacts->delete($entity);
		}
		die("Contacts DELETED");
	} else if($table == 'saleprofiles'){
			$empoyess = $this->SaleProfiles->find('all',[
			'conditions' => ['agency_id' => 2]
		])->all();
		foreach($empoyess as $user){
			$entity = $this->SaleProfiles->get($user->id);
			$result = $this->SaleProfiles->delete($entity);
		}
		die("SaleProfiles DELETED");
	} else if($table == 'tickets'){

		 $this->Tickets->deleteAll(['agency_id' => 2]);
		die("tickets DELETED");
	}else if($table == 'resumes'){

		 $this->Resumes->deleteAll(['agency_id' => 2]);
		die("tickets DELETED");
	}



}*/

/*public function updatesettodefault($table){
	if($table == 'users'){
		$employees = $this->Employees->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->Employees->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("Employees Table UPdated");
	} else if($table == 'cre'){
			$employees = $this->Credentials->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->Credentials->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("Credentials Table UPdated");
	} else if($table == 'projects'){
		$employees = $this->Projects->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->Projects->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("Projects Table UPdated");
	} else if($table == 'contacts'){
			$employees = $this->Contacts->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->Contacts->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("Contacts Table UPdated");
	} else if($table == 'saleprofiles'){
			$employees = $this->SaleProfiles->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->SaleProfiles->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("SaleProfiles Table UPdated");
	} else if($table == 'tickets'){
		$employees = $this->Tickets->find('list',[
					'keyField' => 'id',
					'valueField' => 'id',
					'conditions' => ['agency_id' => 2 ],
					'fields' => ['id','other_id']
				])->toArray();
	//pr($empoyess); die;
		$this->Tickets->updateAll( ["is_updated"=>'No'],['id IN'=>array_keys($employees)]);
		die("tickets Updated is_updated field");
	}	
}
*/

/*MERGING DBS*/
	public function dashbaorddata(){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		$dashboarddata = Cache::read("dashboarddata_$userSession[0]",'short');
		if ($dashboarddata !== false) {
			/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
			//echo "IGOTYOU"; die;
		    echo $dashboarddata; die;
		}


		$condition = "id = $userSession[0]";
		$users = TableRegistry::get('Users');
		$data  =  $users->find("all" , [
			'conditions' => ['id' => $userSession[0]],
			'fields' => ['freefrom','current_status','status_modified','role_id','agency_id','id','employee_code'],
		]);
		$result = isset($data)? $data->toArray():array();
		$freefrom = $result[0]['freefrom']; 
		$diff = abs(time()-strtotime($result[0]['status_modified']));
                 if ($diff < 0)$diff = 0;
                 $dl = floor($diff/60/60/24);
                 $hl = floor(($diff - $dl*60*60*24)/60/60);
                 $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);
                 $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));
               // OUTPUT
			   $hl = ($dl *24)+$hl;
      $diff = array('hours'=>$hl, 'minutes'=>$ml, 'seconds'=>$sl);
      $current_status = $result[0]['current_status'];
      $currentuser = $result;
	$conn = ConnectionManager::get('default');	
		if((HOLIDAY == '0') && (date('l') !="Sunday")){
			//get all users
							if($userSession[3] == 0){
								$conds =  [
								'AND'=>[['status'=>1],["!FIND_IN_SET('1',role_id)"] ],
								];
							} else {
								$conds =  ['agency_id' => $userSession[3],
								'AND'=>[['status'=>1],["!FIND_IN_SET('1',role_id)"] ],
								];
							}
							
							$credentials = TableRegistry::get('Users');
								$data = $credentials->find("all")
								->where(['status'=> 1,'agency_id' => $userSession[3]])
								 ->select(['id','first_name','last_name','status_modified','employee_code'])
								->order(['first_name' => 'ASC']);

							$userData = isset($data)? $data->toArray():array();
							/*pr($userData); die;*/
							(date('l') =="Monday") ? $last_date = date("Y-m-d",time()-(2*24*3600)) : $last_date = date("Y-m-d",time()-(24*3600));   //condition for monday and other days
							if($userSession[3] == 0){
								$attenconds = ['date'=>$last_date];
							} else {
								$attenconds =  ['date'=>$last_date,'agency_id' => $userSession[3]];
							}	

					   $attendanceresult = $this->Attendances->find('list',[
						 	'keyField' => 'user_id',
							'valueField' => 'status',
							'conditions' => $attenconds,
							'fields' => ['user_id','status','date','agency_id']
						 ]);
				 		$AttenceArray = isset($attendanceresult)? $attendanceresult->toArray():array();
				 		/*pr($attendanceresult->toArray()); die;*/
				       $onLeaveUser=array();
				         if(count($userData) > 0 ){
				            foreach($userData as $usr){
				            	//pr($AttenceArray); die;
				          if (in_array($usr['id'],array_keys($AttenceArray))) {
				          	/*pr($AttenceArray[$usr['id']]);*/
				            if($AttenceArray[$usr['id']] == "leave"){
				              $onLeaveUser[$usr['employee_code']] = $usr['first_name'].' '.$usr['last_name'];
				            } else{
				            }
				          }else{
				            $onLeaveUser[$usr['employee_code']] = $usr['first_name'].' '.$usr['last_name'];
				          }
				          } 
				        } else {
				          $onLeaveUser =[];
				        }
				        $today = date("Y-m-d H:i:s");
						if($userSession[3] == 0){
							$ticconds = ['status IN ' => [1,2] ];
						} else {
							$ticconds = ['agency_id' => $userSession[3],'status IN ' => [1,2] ];
						}
				         $query = $this->Tickets->find();
						$query->select([
						    'count' => $query->func()->count('id'),
						    'to_id' => 'to_id'
						])
						->where($ticconds)
						->group('to_id')
						->having(['count <' => 5]);  
						$openTicketUsers = $query->toArray();
						 $tic =  (count($openTicketUsers) > 0) ? array_column($openTicketUsers, 'to_id'):[];
						 	   $allusersData = [];
				               foreach($userData as $user){
				                if(in_array($user['id'], $tic) ){
				                   $allNominalTicketUser[$user['employee_code']] = $user['first_name'].' '.$user['last_name'];
				                }
				                unset($user['id']);
				                $allusersData[] = $user;
				           }

              } else{
        		if($userSession[3] == 0){
						$leaveconds = ['status' => 1, '!FIND_IN_SET (1,role_id)'];
					} else {
						$leaveconds = ['status' => 1, '!FIND_IN_SET (1,role_id)','agency_id' => $userSession[3] ];
					} 	
			        $last_date = date("Y-m-d",time()-(24*3600) );
			        $onLeaveUser = [];
			        $allusersData = $conn
			            ->newQuery()
			            ->select(['first_name','last_name','status_modified'])
			            ->from('users')
			            ->where($leaveconds)
			            ->order(['first_name' => 'asc'])
			            ->execute()
			            ->fetchAll('assoc');
      }
      $sessionID = $userSession[0];
      $rolesAry = explode(",",$userSession[2]);
      $lastmonth = date("m",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
     $evalData = $conn
    ->newQuery()
    ->select('*')
    ->from('evaluations')
   ->where( ['user_id'=>$sessionID,'month'=>$lastmonth,'year'=>date('Y'),'user_comment'=>NULL,'senior_comment !='=>' ','OR'=>[
        ['status'=>'1'],['status'=>'2']
        ]] )
    ->execute()
    ->fetchAll('assoc');
      $noticeficationCout= count($evalData);

    /*Menues Data*/

    $modules = $this->Common->get_menus($result);
    $completemenu = [];
    foreach($modules as $menu){
    	$child_modules = $this->Common->child_modules_fl($menu->id,$result);
    	$menu['child_modules'] = $child_modules;
    	unset($menu['role_id']);
    	unset($menu['agency_id']);
    	unset($menu['is_core']);
    	unset($menu['id']);
    	$completemenu[] = $menu;
    }
 
if(isset($allNominalTicketUser)){
	$allNominalTicketUser=$allNominalTicketUser;
}else{
	$allNominalTicketUser='';
}
//pr(); die;
unset($currentuser[0]['role_id']);
unset($currentuser[0]['current_status']);
$currentuser[0]['originid'] = $currentuser[0]['id'];
unset($currentuser[0]['id']);

//pr($currentuser); die;
/*
$currentuserinfo = [];
$currentuserinfo['employee_code'] = $currentuser['employee_code'];
$currentuserinfo['agency_id'] = $currentuser['agency_id'];*/
//'freefrom','current_status','status_modified','role_id','agency_id','id','employee_code'
//if (($posts = Cache::read('dashboarddata','short')) === false) {
    //$posts = $someService->getAllPosts();



    $data = json_encode(array('previousday'=> date("Y-m-d H:i:s ",time()-(24*3600)), 
   							"date"=> date('Y-m-d'),
							'attendDate'=>$last_date,
							'onLeaveUsers'=>$onLeaveUser,
							'userData'=>$allusersData,
							'allNominalTicketUser'=>$allNominalTicketUser,
							'currentuser'=> $currentuser, 
							'completemenu' => $completemenu 
							));
    Cache::write("dashboarddata_$userSession[0]", $data,'short');
    echo $data; die;
//}
  
  

	}



 public function register(){


     	$this->viewBuilder()->layout('beforelogin');
    	if($this->request->data){

    		 $agency = $this->Agencies->newEntity($this->request->data());
           if (empty($agency->errors()) ){
           	$agree=$this->request->data['is_agree'];
           	if(!empty($agree=="on")){
           		$this->request->data['is_agree']="Yes";
           	}else{
                $this->request->data['is_agree']="No";
              }


       $user = $this->Agencies->find("all" , [
    				'conditions' => ['username' => $this->request->data['username']]
    				])->first();
             
if(count($user)>0){
  $this->Flash->error_new("this username has already taken. please try with another username");
	$this->setAction('register');
}


           	$data =$this->Agencies->save($agency);
             $AgencyId=$data['id'];
             $UserName=$data['username'];
			$this->request->data['username']=$this->request->data['username'];
			$this->request->data['password']=$this->request->data['password'];
			$this->request->data['agency_id']=$AgencyId;
			$this->request->data['role_id']='1';
			$this->request->data['first_name']=$this->request->data['first_name'];
			$this->request->data['last_name']=$this->request->data['last_name'];
			$this->request->data['status']='0';
			$empCOde=$this->request->data['first_name']."".rand(100,100000000);
			$this->request->data['employee_code']=$empCOde;   
       $user = $this->Users->find("all" , [
    				'conditions' => ['username' => $this->request->data['username']]
    				])->first();
 if(count($user)>0){
  $this->Flash->error_new("this username has already taken. please try with another username");
	$this->setAction('register');
 }else{
 	$users = $this->Users->newEntity($this->request->data(), ['validate' => false]);
	$this->Users->save($users);
 }

 $connection = ConnectionManager::get('default'); // Database object for custom query
  $query ="INSERT INTO `settings` ( `key`, `value`, `agency_id`, `created`, `modified`) VALUES
( 'designations', 'Trainee, Software Engineer, Senior Software Engineer, Team Lead, Project Manager, Business Delivery Head, System Analyst, Sales Lead, Senior System Analyst, HR Head, Process Head, QA,QA Lead, Designer, Operation Head, System Administrator, Office Boy, CEO, Vice President, HR Executive, Junior Accountant, Chairman, President, Finance Head',  $AgencyId, '2013-06-21 00:00:00', '2013-06-21 00:00:00'),
( 'process_head', '', $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
( 'hr_head', '',  $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
( 'operation_head', '',  $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
( 'ip_address', '',  $AgencyId, '2014-12-16 00:00:00', '2014-12-16 00:00:00'),
( 'notification', '<strong><font color=red>Cab Facility is only on mentioned timings. Get more details from Operation Head!</font></strong>,\r\nRead Knowledge base on Regular basis for recent Company Policy update.,\r\nRead & Share your skill in Learning Center.,\r\nUpdate [from when you are free] on dashboard of Company ERP.,\r\nUpdate your dropbox for current projects.,\r\nView Company ERP Dashboard for recent announcements.,\r\nPlease update your TICKET STATUS daily.,\r\nPlease update Attendance on daily basis.,\r\nPlease update your PROJECT DEADLINE.,\r\nAttach your ticket(s) to respective project.,\r\nSwitch off your Computer before leaving.,\r\nSwitch off AC / Fan / Light when not needed.',$AgencyId, '2014-12-16 00:00:00', '2014-12-16 00:00:00'),
( 'email_address', '$UserName',  $AgencyId, '2014-12-22 00:00:00', '0000-00-00 00:00:00'),
('dashboard_content', 'welcome',  $AgencyId, '2017-03-24 00:00:00', '2017-03-24 00:00:00')";

  $openTicketUsers = $connection->execute($query);


  $query  =  $this->Modules->find('all',[
          		'conditions'=> ['Modules.is_core' => 'Yes']
          	]
          	);
$datamodules=$query->toArray();
foreach($datamodules as $v){
	    $takenArray=array();
        $allagencies=$v->agency_id;
         $explede=explode(" ",$allagencies);
        $takenArray[]=$explede[0];
        $takenArray[]=$AgencyId;
        $combineagency= implode(",", $takenArray); 
       	$update = ['agency_id' => $combineagency];
		$credentials = TableRegistry::get('Modules');
        $result=$credentials->updateAll($update,['id'=>$v->id]);

}
$this->Flash->success_new("Your Agency has been created Successfully.Please wait untill your account is verified.");
	$this->setAction('login');
              } else {
           	 $this->set("errors", $agency->errors());
           }
    	}
    }

    
   function detailspage(){
   	$this->viewBuilder()->layout('beforelogin');
   }
   function webdevelopment(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function mobiledevelopment(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function erp(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function training(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function placement(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   
function cloudhandling(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function digitalmarketing(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function contentwriting(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }
   function financialservices(){
   	$this->viewBuilder()->layout('new_beforelogin');
   }

   function goto(){

   		if(isset($this->request->data) && count($this->request->data()) > 0){
   			$agency = $this->Agencies->find('all',[
   				'conditions' => ['agency_name' => $this->request->data['agency']]
   				])->first();
   			if(count($agency) > 0){
   				return $this->redirect(array('controller'=>'users','action'=>'login','prefix'=>'admin',$this->request->data['agency']));	
   			} else{
   				$this->set("error",'Invalid Agency Name');
   				$this->setAction('index');
   				//return $this->redirect(array('controller'=>'users','action'=>'index','prefix'=>'admin'));
   				//return $this->redirect('/');
   			}
   			
   		}
   		$this->setAction('index');
   }
   function index() {
   		$session = $this->request->session();
   		$this->response->charset('UTF-8');
   		$this->response->Cache('-1 minute', '+5 days');
   		if($session->read("SESSION_ADMIN") != ""){
    		return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    	}
	    $this->viewBuilder()->layout('beforelogin');
   }

   
   function login($agency = null){

   		$this->set('agency',$agency);
   		$session = $this->request->session();
        $this->set('session',$session);
    	$this->set('title_for_layout','Login');	
    	$this->viewBuilder()->layout('before_login');
    	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
    	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    	} else {
    		$ip_address = $_SERVER['REMOTE_ADDR'];
    	}
    	$this->loadModel('Settings');
    	$this->set('session',$session);
    	if($session->read("INTENDED") !== ""){
    		$this->set('intended', $session->read("INTENDED")['url']);	
    	} else {
    		$this->set('intended', "" );
    	}
    	if($session->read("SESSION_ADMIN") != ""){
    		return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    	} else {
    		if(isset($_COOKIE['icbl'])) {
    			$exp = explode('--', $_COOKIE['icbl']);
    			$token = $exp[0];
    			$value = $exp[1];
    			$date = new \DateTime();
    			$user_agent = $_SERVER['HTTP_USER_AGENT'];
    			$lookup = $this->RememberTokens->find('all',[
					'conditions' => [
									 'token' => $token,
									 'value' => $value,
									 'expires >' => date('Y-m-d H:i:s'),
									]
				])->first();
				/*pr($lookup); die;*/
				if(count($lookup) > 0){
				   $remember = $this->rememberme($lookup->user_id,$ip_address,$lookup->id);
				   return $this->redirect(['controller' => 'users', 'action' => 'checkuserstatus','id' => $this->Common->encForUrl($this->Common->ENC($lookup->user_id))]);
				}
			}
    		
    	}           
    	if($this->request->data){

    		if(!empty($this->request->data['agency']) ) {
    			$agencyrecord = $this->Agencies->find('all',[
    				'conditions' => ['agency_name' => $this->request->data['agency']]
    				])->first();
    				/*pr($agencyrecord['id']); die;*/
    				//pr()
    				$this->set('agency',$this->request->data['agency']);
    			if(count($agencyrecord) == 0){
    				 $this->Flash->error_new("Invalid Agency Name!");
    				return  $this->redirect(array('controller'=>'users','action'=>'login','prefix'=>'admin',$this->request->data['agency']));
    			}
	    		$result= $this->Settings->find("all" , [
	    			'conditions' => ['Settings.key' => 'ip_address','agency_id' =>  $agencyrecord['id']],
	    		'fields' => ['value']
	    		])->first();
	    			$address = $result['value'];
	    			$add=explode(",",$address);
	    			//pr($add); die;
	    		$mail_check=$this->Settings->find("all", [
		    		'conditions' => ['Settings.key' => 'email_address','agency_id' =>  $agencyrecord['id']],
		    		'fields' => ['value']
	    		])->first();
		    	$mail_exist = $mail_check['value'];
		    	$mail_add=explode(",",$mail_exist);
    		} else{
    			$this->Flash->error_new("Agency name can not be left blank");
    			return $this->redirect(array('controller'=>'users','action'=>'login','prefix'=>'admin'));
    		}
    		if(!empty($this->request->data['username']) && !empty($this->request->data['password']))
    		{
    			$user = $this->Users->find("all" , [
    				'conditions' => ['username' => $this->request->data['username'],'status'=>1,'agency_id' =>  $agencyrecord['id']
					],
    				])->first();
    			/*pr($user); die;*/
    			if($user==""){
                   $this->Flash->error_new("The username or password you entered is incorrect");
    			}else{
    				$usermail=$user->username;
    			 if (in_array($ip_address, $add) || in_array($usermail,$mail_add)) {
                    if((new DefaultPasswordHasher)->check($this->request->data['password'],$user->password)){
                     	$this->Auth->setUser($user);
				   		$session = $this->request->session();
				   		$this->set('session',$session);
				   		$session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'), $this->Auth->user('agency_id'), 'auth' ]);
				   		$remember = $this->rememberme($this->Auth->user('id'),$ip_address,0);
				   		//if(isset($this->request->data['remember_me']) && $this->request->data['remember_me'] == 'on'){
				   				//$remember = $this->rememberme($this->Auth->user('id'),$ip_address);
				   		//}
				   		$this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
					}else{
						
				   		$this->Flash->error_new("The username or password you entered is incorrect");
				   	}
				   }else{

				   	echo "<center><h1>Sorry ! You are not authorized to access this page.</h1></center>";
				   	exit;
				   }
    			}
				}else{

					$this->Flash->error_new("Please enter the username and password");
				}
		
			}	

		}

		function rememberme($userid,$ip,$id){
			$token = bin2hex(random_bytes(32));
   			$random = md5(rand(99999,99999999999999).date('YmdHis').$this->Auth->user('id'));
   			$date = new \DateTime();
            $expires =  $date->modify("+720 hours"); // cookies will be set for 1 month 24 * 30 = 720
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
   			$data = [
   						'token' => $token,
   						'value' => $random,
   						'ip' => $ip,
   						'user_id' => $userid,
   						'expires' => $expires,
   						'user_agent' => $user_agent
   					];
//   		    pr($data); die;
   			$cookie_name = "icbl"; // Instant Cookie Based Login
			$cookie_value = $token.'--'.$random;
			setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day, Set for 30 days
			
			if($id > 0){
				$this->RememberTokens->updateAll($data,['id' => $id]);
			} else {
				//pr("ELse"); die;
				$users = $this->RememberTokens->newEntity();
				$users->token = $token;
				$users->value = $random;
				$users->ip = $ip;
				$users->user_id = $userid;
				$users->expires = $expires;
				$users->user_agent = $user_agent;
				$this->RememberTokens->save($users);	
			}
			
			return "success";
		}

	   function adminlogin($agency = null){
   		$this->set('agency',$agency);
   		$session = $this->request->session();
        $this->set('session',$session);
    	$this->set('title_for_layout','Login');	
    	/*$this->viewBuilder()->layout('beforelogin');*/
    	$this->viewBuilder()->layout('before_login');
    	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
    	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    	} else {
    		$ip_address = $_SERVER['REMOTE_ADDR'];
    	}
    	$this->loadModel('Settings');
    	
    	$this->set('session',$session);

    	if($session->read("SESSION_ADMIN") != ""){
    			/*pr("asdfas"); die;*/
    		return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    	}           
    	if($this->request->data){
    		
    		if(!empty($this->request->data['username']) && !empty($this->request->data['password']))
    		{
    			$user = $this->Users->find("all" , [
    				'conditions' => ['username' => $this->request->data['username'],'status'=>1, 'FIND_IN_SET (8,role_id)'
					],
    				])->first();
    			/*pr($user); die;*/
    			if($user==""){
                   $this->Flash->error_new("The username or password you entered is incorrect");
    			}else{
    				$usermail=$user->username;
    			 //if (in_array($ip_address, $add) || in_array($usermail,$mail_add)) {
                    if((new DefaultPasswordHasher)->check($this->request->data['password'],$user->password)){
                     	$this->Auth->setUser($user);
				   		$session = $this->request->session();
				   		$this->set('session',$session);
				   		$session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),0 ]);
				   		$this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
					}else{
						
				   		$this->Flash->error_new("The username or password you entered is incorrect");
				   	}
				  /* }else{

				   	echo "<center><h1>Sorry ! You are not authorized to access this page.</h1></center>";
				   	exit;
				   }*/
    			}
				}else{

					$this->Flash->error_new("Please enter the username and password");
				}
		
			}	

		}	



			#_________________________________________________________________________#

	    /**
	    * @Date: 17-Aug-2010
	    * @Method : admin_forgot_password
	    * @Purpose: This function is to reset admin's password.
	    * @Param: none
	    * @Return: none
	    **/

	    function forgotPassword(){
 	    	$this->set('title_for_layout','forgot_password');	
	    	//$this->viewBuilder()->layout('layout_admin_withoutlogin');
	    	$this->viewBuilder()->layout('before_login');
	    	if($this->request->data){
	    		$this->request->data['user_name'];
	    			$condition = "username='".$this->request->data['user_name']."' AND status = '1'";
	    			$emp_details =	$this->Users->find("all" , [
	    				'conditions' => ['username' => $this->request->data['user_name'],
	    				'status'=> 1],
	    				])->first();
	    			if($emp_details){
	    				$resetPassword = rand(100000,999999);
	    				$pwd= (new DefaultPasswordHasher)->hash($resetPassword);
	    				$subject = "Forgot your Password!";
	    				$name    = $emp_details['first_name']." ".$emp_details['last_name'];
	    				$message = "Dear ".$name.",Your password has been reset successfully.Please find the following login details:Email: ".$emp_details['username']."Password: ".$resetPassword."Thanks, Support Team";
	    				$email = new Email('default');
	    				$email->from(ADMIN_EMAIL)
	    				->to (trim($emp_details['username']))
	    				->subject($subject)
	    				->send($message);
	    				if ($email->send($message)) {
	    					$query = $this->Users->query();
	    					$query->update()
	    					->set(['password' => $pwd])
	    					->where(['id' => $emp_details['id']])
	    					->execute();
	    					$this->Flash->success_new('Your password has been reset and successfully sent to your email id.');
	    				}
	    				//$this->redirect(array('controller'=>'users','action'=>'forgot_password','prefix'=>'admin'));
	    				$this->redirect(array('controller'=>'users','action'=>'login','prefix'=>'admin'));
	    			}else{
	    				$this->Flash->error_new('The email doesnot exists.');
	    				$this->redirect(array('controller'=>'users','action'=>'login','prefix'=>'admin'));

	    		 }
	    	 }

	    }

		public function checkuserstatus($id){
		//Cache::clear(false);
		$id = (is_numeric($id) ) ?  $id : $this->Common->DCR($this->Common->dcrForUrl($id) );
   		$session = $this->request->session();
        $this->set('session',$session);
    	$this->set('title_for_layout','Login');	
    	/*$this->viewBuilder()->layout('beforelogin');*/
    	$this->viewBuilder()->layout('before_login');

    	
        $mail_check=$this->Settings->find("all", [
    		'conditions' => ['Settings.key' => 'email_address'],
    		'fields' => ['value']
    		])->first()->toArray();
    	$mail_exist = $mail_check['value'];
    	$mail_add=array();
    	$mail_add=explode(",",$mail_exist);
    	$this->set('session',$session);

    	if($session->read("SESSION_ADMIN") != ""){
    			/*pr("asdfas"); die;*/
    			if($session->read("INTENDED") == ''){
    				return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);		
    			} else {
    				return $this->redirect($session->read("INTENDED")['url']);		
    			}
    		
    		//return $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
    	}           
    	if(!empty($id)){
    
    		$a = 1;
    		if($id)
    		{
    			$user = $this->Users->find("all" , [
    				'conditions' => ['id' => $id
					],
    				])->first();
    			/*pr($user); die;*/
    			if($user==""){
                   $this->Flash->error_new("The username or password you entered is incorrect");
    			}else{
    				$usermail=$user->username;
    			 if (/*in_array($ip_address, $add) || in_array($usermail,$mail_add)*/ $a ==1) {
                    if(/*(new DefaultPasswordHasher)->check($this->request->data['password'],$user->password)*/ $a ==1){
                     	$this->Auth->setUser($user);
				   		$session = $this->request->session();
				   		$this->set('session',$session);
				   		$session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id') ,  'cookie' ]);
				   		$this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
					}else{
						
				   		$this->Flash->error_new("The username or password you entered is incorrect");
				   	}
				   }else{

				   	echo "<center><h1>Sorry ! You are not authorized to access this page.</h1></center>";
				   	exit;
				   }
    			}
				}else{

					$this->Flash->error_new("Please enter the username and password");
				}
		
			}	
		}
	    

	    function admin_sngadnVLL2013() {
	    	$this->Session->write("MAINTENANCE_MODE","1");
	    	$this->redirect('login');
	    }

		 #_________________________________________________________________________#

	    /**
	    * @Date: 28-Dec-2011
	    * @Method : index
	    * @Purpose: This function is the default function of the controller
	    * @Param: none
	    * @Return: none
	    **/

	    function home() {
	    	if($this->Session->read("SESSION_USER") != ""){

	    	}

	    }


		#_________________________________________________________________________#

	    /**
	    * @Date: 28-Dec-2011
	    * @Method : admin_index
	    * @Purpose: This is the default function of the administrator section for users
	    * @Param: none
	    * @Return: none
	    **/

	    function admin_index(){
	    	$this->render('admin_login');
	    	if($this->Session->read("SESSION_ADMIN") != ""){

	    		$this->redirect('dashboard');
	    	}
	    }

		#_________________________________________________________________________#

		/**
	    * @Date: 28-Dec-2011
	    * @Method : admin_login
	    * @Purpose: used for authorizing administrator section
	    * @Param: none
	    * @Return: none
	    **/

		function admin_login(){


			$this->set('title_for_layout','Login');
			$this->layout = "layout_admin_withoutlogin";

			$ip_address=$this->RequestHandler->getClientIp();
			//exit;

			$result=$this->Setting->find("all",array('fields'=>array('value'),

				'conditions'=>array('Setting.key'=>'ip_address')
				));


			foreach($result as $res){

				$address=$res['Setting']['value'];

			}

			$add=array();
			$add=explode(",",$address);


			$mail_check=$this->Setting->find("all",array('fields'=>array('value'),

				'conditions'=>array('Setting.key'=>'email_address')
				));

			foreach($mail_check as $mail){

				$mail_exist=$mail['Setting']['value'];
			}

			$mail_add=array();
			$mail_add=explode(",",$mail_exist);

		//echo "<pre>";print_r($mail_add);
			if($this->Session->read("SESSION_ADMIN") != ""){

				$this->redirect(array('controller'=>'users','action'=>'dashboard'));

			}

			if($this->data){

				$this->User->set($this->data['User']);

				$isValidated = $this->User->validates();

				if($isValidated){

					// validates user login information
					$valid_user=$this->data['User']['user_name'];

					//exit;
					$password    = md5($this->data['User']['password']);

					$condition = "(username='".Sanitize::escape($this->data['User']['user_name'])."') AND password='".$password."'  AND status = '1'";

					$user_details = $this->User->find('first', array("conditions" => $condition, "fields" => array("id","first_name","role_id")));


					if (in_array($ip_address, $add)||in_array($valid_user,$mail_add)) {
						if(is_array($user_details) && count($user_details) > 0){

							$this->Session->write("SESSION_ADMIN", array($user_details['User']['id'],$user_details['User']['first_name'],$user_details['User']['role_id']));

							$this->redirect(array('controller'=>'users','action'=>'dashboard'));

						}else{

							$this->Session->setFlash("<div class='error-message flash notice'>The username or password you entered is incorrect.</div>");

						}


					}
					else{
						echo "<center><h1>Sorry ! You are not authorized to access this page.</h1></center>";
						exit;
					}
				}
			}


		}



		#_________________________________________________________________________#

	    /**
	    * @Date: 28-Dec-2011
	    * @Method : admin_dashboard
	    * @Purpose: This function is to show Admin dashboard.
	    * @Param: none
	    * @Return: none
	    **/

	  	    function dashboard(){
	  	    $session = $this->request->session();
	  	    $this->response->Cache('-1 minute', '+5 days');
            $userSession = $session->read("SESSION_ADMIN");
	    	$this->viewBuilder()->layout('new_layout_admin');
	    	$this->set("title_for_layout", "ERP Dashboard");
	    	$this->set("page", "dashboard");
	    	$allNominalTicketUser=array();
	    	if($userSession[3] == 0){
	    		$cond = ['Settings.key' => 'dashboard_content','agency_id'=> 3];
	    	} else{
	    		$cond =  ['Settings.key' => 'dashboard_content','agency_id'=> $userSession[3]];
	    	}
	    	$dashcontent = Cache::read("dashcontent_$userSession[3]",'long');
	    	if ($dashcontent !== false) {
	    		$data = $dashcontent;
		    	/*$qcount = Cache::read("querycount",'verylong');
		    		if ($qcount !== false) {
		    			$qcount += 1;
		    			Cache::delete("querycount",'verylong');
		    			Cache::write("querycount",$qcount,'verylong'); 
		    		} else {
	    			$qcount = Cache::write("querycount",1,'verylong');
	    		}*/
	    		//pr($data); die;
	    	} else {
	    		$content = $this->Settings->find('all', [
			      'fields'=>['Settings.key','Settings.value'],
			      'conditions' => $cond
			      ])->first();
				$data = isset($content)? $content->toArray():array();
				Cache::write("dashcontent_$userSession[3]", $data,'long');
	    	}
            $this->set('dashcontent',$data);
			$condition = "id = $userSession[0]";
			$result= $this->Users->find("all" , [
				'conditions' => ['id' => $userSession[0]],
				'fields' => ['freefrom','current_status','status_modified','employee_code']
				])->first()->toArray();
			$this->set('userinfo',$result);
			$date=$result['freefrom'];
			$diff = $this->Common->expiredTime($result['status_modified']); //login
			if((HOLIDAY == '0') && (date('l') !=="Sunday")){
             $condition = "WHERE Users.status = 1 and !FIND_IN_SET ('1',role_id)";
             	$userData = $this->Users->find('all',array(
					'fields' => ['id','first_name','last_name','status_modified'],
					'conditions' => ['status' => 1, '!FIND_IN_SET (1,role_id)','agency_id' => $userSession[3] ],
					'order' => ['Users.first_name' => 'asc']
					))->toArray();
       		(date('l') =="Monday") ? $last_date = date("Y-m-d",time()-(2*24*3600)) : $last_date = date("Y-m-d",time()-(24*3600));   //condition for monday and other days
			 $users = TableRegistry::get('Attendances');
			        $lastDayAttendance1= $users->find("list", [
						'keyField' => 'user_id',
						'valueField' => 'user_id',

			         'conditions' => ['date'=>$last_date],

			         'fields' => ['user_id','status'],
			 ]);
			$lastDayAttendance = isset($lastDayAttendance1)? $lastDayAttendance1->toArray():array();
			$onLeaveUser=array();
         	if(count($userData) > 0 ){
						foreach($userData as $usr){
					if (in_array($usr['id'],array_keys($lastDayAttendance))) {
						if($lastDayAttendance[$usr['id']] == "leave"){
							$onLeaveUser[$usr['id']] = $usr['first_name'].' '.$usr['last_name'];
						} else{
							/////////////////
						}
					}else{
						$onLeaveUser[$usr['id']] = $usr['first_name'].' '.$usr['last_name'];
					}
					}	
				} else {
					$onLeaveUser =[];
				}
				$today = date("Y-m-d H:i:s");
				$connection = ConnectionManager::get('default'); // Database object for custom query
				$query ="SELECT * FROM (Select count(`id`) as tot , MIN(`to_id`), MIN(`title`) ,MIN(`deadline`),MIN(`status`) FROM  tickets group by `tickets`.`to_id`) `total` WHERE `total`.`tot` < 5 "  ; // working
				$openTicketUsers = $connection->execute($query)->fetchAll('assoc');
                $allNominalTicketUser=array();
                foreach($openTicketUsers as $ticket){ 
					$tic[] = $ticket['MIN(`to_id`)'];
				}
               foreach($userData as $user){
			         foreach($tic as $t){
			         	if($user['id']==$t){
			         		 $allNominalTicketUser[$user['id']] = $user['first_name'].' '.$user['last_name'];
			         	}
			         }
			     }
              } else{
              	$userData = [];
              	$allNominalTicketUser = [];
				$last_date = date("Y-m-d",time()-(24*3600) );
				$onLeaveUser = [];
				
			}
			/*$open_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 1
					];
			$inprogress_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 2
					];
			$closed_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 0
			];
			// Tickets Related Code 
			$oderby = ['deadline' => 'asc'];
			$open = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($open_cond)
					->order($oderby)->count();
			$inprogress = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($inprogress_cond)
					->order($oderby)->count();
			$closed = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($closed_cond)
					->order($oderby)->count();

			$this->set('open',$open);
			$this->set('inprogress',$inprogress);
			$this->set('closed',$closed);*/
			// Ticket Related Code
			// Projects Related Code
			$total_projects = $this->Projects->find() 
			->select([ 'Projects.id', 'Projects.project_name','Projects.dailyupdate','Projects.dropbox','Projects.vooap','Projects.onalert','Projects.processnotes','Projects.created','Projects.modified','Projects.finishdate','Projects.nextrelease','Projects.totalamount','Engineer_User.first_name','Engineer_User.last_name','Last_Updatedby.first_name','Last_Updatedby.last_name'])
			->where([ 
			'OR' => [ ['pm_id' => $userSession[0] ],['engineeringuser' =>
			$userSession[0] ],['salesbackenduser' =>$userSession[0] ],['salesfrontuser' => $userSession[0] ], 'FIND_IN_SET(\''. $userSession[0] .'\',Projects.team_id)'],
			['Projects.status' => 'current'] ])
			->order(['Projects.modified' => 'DESC'])->count();
			
			$onalert_projects = $this->Projects->find() 
			->select([ 'Projects.id', 'Projects.project_name','Projects.dailyupdate','Projects.dropbox','Projects.vooap','Projects.onalert','Projects.processnotes','Projects.created','Projects.modified','Projects.finishdate','Projects.nextrelease','Projects.totalamount','Engineer_User.first_name','Engineer_User.last_name','Last_Updatedby.first_name','Last_Updatedby.last_name'])
			->where([ 
			'OR' => [ ['pm_id' => $userSession[0] ],['engineeringuser' =>
			$userSession[0] ],['salesbackenduser' =>$userSession[0] ],['salesfrontuser' => $userSession[0] ], 'FIND_IN_SET(\''. $userSession[0] .'\',Projects.team_id)'],
			['Projects.status' => 'current'], ['Projects.onalert' => 1]])
			->order(['Projects.modified' => 'DESC'])->count();
			// Projects Related Code
			//pr($onalert_projects); die;
			$this->set('onalert_projects',$onalert_projects);
			$this->set('total_projects',$total_projects);
           $sessionID = $userSession[0];
			$rolesAry = explode(",",$userSession[2]);
			$lastmonth = date("m",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));

		/*	$evalData  = $this->Evaluations->find('all',[
				'conditions'=>['user_id'=>$sessionID,'OR'=>[
				['status'=>'1'],['status'=>'2']
				]
				,'month'=>$lastmonth,'year'=>date('Y'),'user_comment'=>NULL,'senior_comment !='=>' ']
				])->all()->toArray();*/
             $this->set("date",$date);  
           // $noticeficationCout= count($evalData);
			//$this->set('noticeficationCout',$noticeficationCout);
			$this->set('attendDate',$last_date);
		    $this->set('onLeaveUsers',$onLeaveUser);
			$this->set('userData',$userData);
		    $this->set('allNominalTicketUser',$allNominalTicketUser);
			//$this->set('resultdata',$data);
			$this->set('date',$date);
		}

		#_________________________________________________________________________#

	    /**
        * @Date: 20-09-2016
	    * @Method : admin_profile
	    * @Purpose: This function is to show Admin profile.
	    * @Param: none
	    * @Return: none
	    *@who : Priyanka Sharma
	    **/
	    function profile(){
	    	$session = $this->request->session();
	    	$this->viewBuilder()->layout('new_layout_admin');
	    	$this->set('session',$session);
	    	$user = $session->read("SESSION_ADMIN");
	    	$this->set("title_for_layout", "My Profile");
	    	$resultCredit  =  $this->Users->find('all', array(
	    		'conditions' => array('Users.id'=>$user[0])
			))->contain(['Credentials']);
	    	$record=isset($resultCredit)? $resultCredit->toArray():array();
	    	$result  =  $this->Appraisals->find('all', array(
	    		'conditions' => array('Appraisals.user_id'=>$user[0]),
	    		'fields' => array('Appraisals.appraised_amount','Appraisals.new_designaiton')
	    		))->first();
	    	$ab=isset($result)? $result->toArray():array();
	    	$this->set('resultData', $record);
	    	$this->set('resultCredit',$resultCredit);
	    	$this->set('result', $result);
	    	$this->set('appresult', $ab);
		}
		#_________________________________________________________________________#

	    /**
	    * @Date: 17-Aug-2010
	    * @Method : admin_forgot_password
	    * @Purpose: This function is to reset admin's password.
	    * @Param: none
	    * @Return: none
	    **/

	    function admin_forgot_password(){

	    	$this->layout = "layout_admin_withoutlogin";
	    	$this->pageTitle = "Forgot password";

	    	if($this->data){

	    		$this->User->set($this->data['User']);
	    		$isValidated = $this->User->validates();
	    		if($isValidated){
	    			$condition = "username='".Sanitize::escape($this->data['User']['user_name'])."' AND status = '1'";
	    			$emp_details = $this->User->find('first', array("conditions" => $condition, "fields" => array("id","username","first_name","last_name")));
	    			if($emp_details){
	    				$resetPassword = rand(100000,999999);
	    				$subject = "Forgot your Password!";
	    				$name    = $emp_details['User']['first_name']." ".$emp_details['User']['last_name'];
	    				$this->Email->to       = trim($emp_details['User']['username']);
	    				$this->Email->subject  = $subject;
	    				$this->Email->replyTo  = ADMIN_EMAIL;
	    				$this->Email->from     = SITE_NAME."<".ADMIN_EMAIL.">";
	    				$this->Email->fromName = SITE_NAME;
	    				$this->Email->sendAs   = 'html';
	    				$message = "Dear <span style='color:#666666'>".$name."</span>,<br/><br/>Your password has been reset successfully.<br/><br/>Please find the following login details:<br/><br/>Email: ".$emp_details['User']['username']."<br/>Password: ".$resetPassword."<br/><br/>Thanks, <br/>Support Team";
	    				if ($this->Email->send($message)) {
	    					$this->User->updateAll(array("password"=>"'".md5($resetPassword)."'"),array("id"=>$emp_details['User']['id']));
	    					$this->Session->setFlash("<div class='success-message flash notice'>Your password has been reset and successfully sent to your email id.</div>");
	    				}
	    			}else{

	    				$this->Session->setFlash("<div class='error-message flash notice'>The email doesn&#39;t exists.</div>");

	    			}
	    		}
	    	}

	    }


		#_________________________________________________________________________#

	    /**
	    * @Date: 19-Dec-2014
	    * @Method : admin_get_code
	    * @Purpose: This function is to get code who did not get alloted ip address.
	    * @Param: none
	    * @Return: none
	    **/

	    function getCode(){
	    	$this->set('title_for_layout','Get Code');	
	    	$this->viewBuilder()->layout('layout_admin_withoutlogin');
	    	if($this->request->data){
	    		$from=$this->request->data['user_name'];
	    		$isValidated = $this->Users->validator('default');
	    		if($isValidated){
	    			$subject = "Get code for Instant Access";
	    			$unique=date('Y').date('m').date('d').date('h').date('s').date('i');
	    			$securitycode = rand(1000000000,9999999999).$unique;
	    			$message ="<div><h5>Please go to(Instant Access URL for $from )</h5>http://www.dli.vlogiclabs.com/admin/authontications/securitytoken?code=$securitycode </div>";
	    			$email = new Email();
	    			$email->from(ADMIN_EMAIL)
	    			->to ('process@vlogiclabs.com')
	    			->replyTo(ADMIN_EMAIL)
	    			->subject($subject)
	    			->send($message);
	    			if ($email->send($message)) {
	    				$status=0;
	    				$AlternateLoginsTable = TableRegistry::get('AlternateLogins');
	    				$AlternateLogin = $AlternateLoginsTable->newEntity();
	    				$AlternateLogin->email_add = $this->request->data['user_name'];
	    				$AlternateLogin->security_code = $securitycode;
	    				$AlternateLogin->status = '0';
	    				if ($AlternateLoginsTable->save($AlternateLogin)) {
	    				}

	    				$this->Flash->success_new("An verification email has been sent to Admin for this email Id.");

	    			}
	    			else{

	    				$this->Flash->success_new("<div class='error-message flash notice'>Mail can not send.</div>");

	    			}
	    		}
	    	}
	    }

		#______________________________________________________________________________________________________________#


		#______________________________________________________________________________________________________________#

	    /**
	    * @Date: 11-Aug-2011
	    * @Method : admin_list
	    * @Purpose: This function is to show list of users in system.
	    * @Param: none
	    * @Return: none
	    **/

	    function admin_list(){

	    	$this->set("title_for_layout", "Employees Listing");
	    	$this->set("search1", "");
	    	$this->set("search2", "");
	    	$criteria = "1";

		// Delete user and its licences and orders(single/multiple)

	    	if(isset($this->params['form']['delete']) || isset($this->params['pass'][0]) == "delete"){

	    		if(isset($this->params['form']['IDs'])){

	    			$deleteString = implode("','",$this->params['form']['IDs']);
	    		}elseif(isset($this->params['pass'][1])){

	    			$deleteString = $this->params['pass'][1];
	    		}

	    		if(!empty($deleteString)){

	    			$this->User->deleteAll("User.id in ('".$deleteString."')");
	    			$this->Session->setFlash("<div class='success-message flash notice'>User(s) deleted successfully.</div>");
	    			$this->redirect('list');
	    		}
	    	}

	    	if(isset($this->data['User']) || !empty($this->params['named'])) {

	    		if(!empty($this->data['User']['fieldName']) || isset($this->params['named']['field'])){

	    			if(trim(isset($this->data['User']['fieldName'])) != ""){

	    				$search1 = trim($this->data['User']['fieldName']);
	    			}elseif(isset($this->params['named']['field'])){

	    				$search1 = trim($this->params['named']['field']);
	    			}
	    			$this->set("search1",$search1);
	    		}

	    		if(isset($this->data['User']['value1']) || isset($this->data['User']['value2']) || isset($this->params['named']['value'])){

	    			if(isset($this->data['User']['value1']) || isset($this->data['User']['value2'])){

	    				$search2 = ($this->data['User']['fieldName'] != "User.status")?trim($this->data['User']['value1']):$this->data['User']['value2'];
	    			}elseif(isset($this->params['named']['value'])){

	    				$search2 = trim($this->params['named']['value']);
	    			}
	    			$this->set("search2",$search2);

	    		}

	    		/* Searching starts from here */

	    		if(!empty($search1) && (!empty($search2) || $search1 == "User.status")){

	    			$criteria = $search1." LIKE '%".Sanitize::escape($search2)."%'";
	    		}else{

	    			$this->set("search1","");
	    			$this->set("search2","");

	    		}

	    	}

	    	if(isset($this->params['named'])){

	    		$urlString = "/";
	    		$completeUrl  = array();

	    		if(!empty($this->params['named']['page']))

	    			$completeUrl['page'] = $this->params['named']['page'];
	    		if(!empty($this->params['named']['sort']))

	    			$completeUrl['sort'] = $this->params['named']['sort'];
	    		if(!empty($this->params['named']['direction']))

	    			$completeUrl['direction'] = $this->params['named']['direction'];
	    		if(!empty($search1))

	    			$completeUrl['field'] = $search1;
	    		if(isset($search2))

	    			$completeUrl['value'] = $search2;
	    		foreach($completeUrl as $key=>$value){
	    			$urlString.= $key.":".$value."/";

	    		}

	    	}
	    	$this->set('urlString',$urlString);

	    	if(isset($this->params['form']['publish'])){

	    		$setValue = array('status' => "'1'");
	    		$messageStr = "Selected user(s) have been activated.";
	    	}elseif(isset($this->params['form']['unpublish'])){

	    		$setValue = array('status' => "'0'");
	    		$messageStr = "Selected user(s) have been deactivated.";
	    	}

	    	if(!empty($setValue)){

	    		if(isset($this->params['form']['IDs'])){

	    			$saveString = implode("','",$this->params['form']['IDs']);
	    		}
	    		if($saveString != ""){

	    			$this->User->updateAll($setValue,"User.id in ('".$saveString."')");
	    			$this->Session->setFlash($messageStr, 'layout_success');
	    		}
	    	}

	//pr($this->Session->read('SESSION_ADMIN'));

	// If Admin is login then all records else only loggedin user record

	    	$this->paginate = array(
	    		'fields' => array(
	    			'User.id',
	    			'User.first_name',
	    			'User.role_id',
	    			'User.employee_code',
	    			'User.current_salary',
	    			'User.username',
	    			'User.last_visit',
	    			'User.current_salary',
	    			'User.status',
	    			'User.created',
	    			'User.registration_date'
	    			),
	    		'conditions' => array(),
	    		'page'=> 1,'limit' => RECORDS_PER_PAGE,
	    		'order' => array('User.id' => 'desc')
	    		);
	    	$data = $this->paginate('User',$criteria);
	    	$this->set('resultData', $data);
	    }


		#_________________________________________________________________________#

	    /**
	    * @Date: 24-Aug-2010
	    * @Method : admin_change_password
	    * @Purpose: This function is to change admin password.
	    * @Param: none
	    * @Return: none
	    **/
	    function userdetails($id){
		    $this->set('session',$session);
	    	$this->set('title_for_layout','Login');	
	    	$user= $this->Users->get($id);
	    	$this->Auth->setUser($user->toArray());
	    	$session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id') ]);
			$this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));

	    }
	    function admin_change_password(){

	    	$this->set('title_for_layout','Change password');
	    	$this->set("pageTitle","Change password");

	    	if($this->data){

	    		$this->User->set($this->data['User']);
	    		$isValidated = $this->User->validates();
	    		if($isValidated){
	    			$userSession = $this->Session->read("SESSION_ADMIN");
					// Update new password
	    			$result = $this->User->updateAll(array("password"=>"'".md5(Sanitize::escape($this->data['User']['password']))."'"),array('id'=>$userSession[0]));
	    			if($result){
	    				$this->Session->setFlash("<div class='success-message flash notice'>Your password has been changed successfully.</div>",'layout_success');
	    				$this->redirect(array("action"=>"change_password"));
	    			}
	    		}
	    	}
	    }
		// Update new profile
	    function admin_change_profile(){
	    	$userSession = $this->Session->read("SESSION_ADMIN");

	    	$user_id=$userSession[0];
	    	$this->set('title_for_layout','Change profile');
	    	$this->set("pageTitle","Change profile");

	    	if($this->data){

	    		$this->User->set($this->data['User']);
	    		$isValidated = $this->User->validates();
	    		if($isValidated){
	    			$this->data['User']['id']=$user_id;
					//$this->User->save($this->data['Category'], array('validate'=>false));
	    			$result = $this->User->save($this->data['User'], array('validate'=>false));

	    			if($result){
	    				$this->Session->setFlash("<div class='success-message flash notice'>Your profile has been changed successfully.</div>",'layout_success');
	    				$this->redirect(array("action"=>"dashboard"));
	    			}
	    		}
	    	}else{

	    		$this->data = $this->User->find('first', array('conditions'=>array('id'=>Sanitize::escape($user_id))));
	    	}
	    }



	    /**
	    * @Date: 24-Aug-2010
	    * @Method : admin_change_password
	    * @Purpose: This function is to change admin password.
	    * @Param: none
	    * @Return: none
	    **/

	    function gallery(){

	    	$this->pageTitle = "Gallery";
	    	$this->layout="layout_inner";
	    	$result = $this->Gallery->find('all');
	    	$this->set("resultData",$result);
	    }
				#_________________________________________________________________________#

	    /**
	    * @Date: 28-Dec-2011
	    * @Method : admin_logout
	    * @Purpose: This function is used to destroy admin session.
	    * @Param: none
	    * @Return: none
	    **/

	    function logout(){
	    	if (isset($_COOKIE['icbl'])) {
	    		$exp = explode('--', $_COOKIE['icbl']);
    			$token = $exp[0];
    			$value = $exp[1];
    			$date = new \DateTime();
    			$lookup = $this->RememberTokens->find('all',[
					'conditions' => [
									 'token' => $token,
									 'value' => $value,
									 'expires >' => date('Y-m-d H:i:s'),
									]
				])->first();
				if(count($lookup) > 0){
				   $this->RememberTokens->deleteAll(['id IN' => [ $lookup->id ]  ]);
				}
			    unset($_COOKIE['icbl']);
			    setcookie('icbl', '', time() - 3600, '/'); // empty value and old timestamp
			}
	    	$session = $this->request->session();
	    	$userSession=$session->read("SESSION_ADMIN");
	    	$user_id=$userSession[0];
	    	$session->delete("SESSION_ADMIN");
	    	$session->delete("adminlogin");
	    	$users = TableRegistry::get('Users');
	    	$user = $users->get($user_id);
	    	$user->last_visit = date("Y-m-d H:i:s");
			    $users->save($user); // notice the 's' in the users and user
			    return $this->redirect($this->Auth->logout());
			    //$this->redirect(array('controller'=>'users','action' => 'login'));

			}


		 /**
	    * @Date: 30-Apr-2012
	    * @Method : current status
	    * @Purpose: This function is used to upload employee pic
	    * @Param: $id
	    * @Return: none
	    **/

		 function addCurrentStatus(){
		 	$session = $this->request->session();
	    	$userSession=$session->read("SESSION_ADMIN");
		 	if($this->request->data['current_status']){
		 		$modified_time = date("Y-m-d H:i:s");
		 		$ipaddress = IP_ADDRESS;
	$result = $this->Users->updateAll( ["freefrom"=>date("Y-m-d",strtotime($this->request->data['current_status'])),
		 			"status_modified"=>$modified_time,
		 			"ipaddress" =>$ipaddress
		 			],['id'=>$userSession[0]]);
		 if($result ==1){
			$data=TableRegistry::get('Users');
			$existing_records=$data->find()
			->where(['id'=>$userSession[0]]);
			$data_exists=$existing_records->toArray();
			$date=$data_exists[0]['freefrom']->format('Y-m-d');
			$this->set("date",$date);
	    }

		 		if($result==1){
		 			echo date("d/m/Y h:i a",strtotime($modified_time));
		 		}
		 		else{
		 			echo "0";
		 		}
		 		exit;


		 	}
		 }
		#_____________________________________________________________________________________________________________#
		/**
	    * @Date: 30-Apr-2012
	    * @Method : current status
	    * @Purpose: This function is used to upload employee pic
	    * @Param: $id
	    * @Return: none
	    **/


		function report($id = null){
			$this->viewBuilder()->layout('new_layout_admin');
			$this->set("title_for_layout", "Company Snapshot");
			//$userSession = $this->Session->read("SESSION_ADMIN");
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			if(!empty($id)){
				//$this->data = $this->User->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));
				$query  =  $this->Users->find("all",[
					'conditions' => [ 'id' => $id,'agency_id'=>$userSession[3] ]
					])->first();
				$data = isset($query)? $query->toArray():array();
				// Send mail to User
				$name    = $data['first_name']." ".$data['last_name'];
				$now = date("F j, Y, g:i a");                                                                                      // current date and time
				
				$message = "Dear <span style='color:#666666'>".$name."</span>,<br/><br/>You are requested to update your current working status on ERP.<br/><br/><span style='font-size:11px;color:#999999;'>Note: You should update your ERP Working Status in each 2 hours.</span><br/><br/>Thanks,<br/>Regards, <br/>VLL ERP Team";
				
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($data['username'])
				->subject($userSession[1]." requested for Status Update at ".$now)
				->replyTo(ADMIN_EMAIL)
				->from(ADMIN_EMAIL)
				->send($message);
					//$this->Session->setFlash("Employee has been notified successfully.",'layout_success');
				$this->Flash->success_new("Employee has been notified successfully.",'layout_success');
				return $this->redirect(
					['controller' => 'Users', 'action' => 'report' ,'prefix' =>'admin']
					);
			} else{
				$query  =  $this->Users->find("all",[
					'conditions' => ['status' => 1 , 'first_name !=' => '','agency_id'=>$userSession[3] ]
					]);
				$data = $this->paginate($query,[
					'page' => 1, 'limit' => RECORDS_PER_PAGE,
					'order' => ['status_modified' => 'desc']
					]);
				// to get time difference between current and modified time
				$this->set('resultData', $data);
			}
		}

		#_____________________________________________________________________________________________________________#

		/* Function for maintainance mode */
		function admin_maintenancemode(){
			$this->layout = "layout_admin_withoutlogin";
			if(MAINTENANCE == "0"){
				$this->redirect('logout');
			}
		}

		function admin_employee_salary(){



			$this->set('title_for_layout','Salary Calculation');

			if(!empty($this->data['User']['month2'])) {
				$mnth=  $this->data['User']['month2'];

			}
			else{

				$mnth=date('m');

			}

			$this->set('month_act',$mnth);

			if(!empty($this->data['User']['year'])) {
				$year=  $this->data['User']['year'];

			}
			else{

				$year=date('Y');

			}

			$this->set('year_act',$year);


			$selected_date=$year."-".$mnth."-01";


		 //$selected_date=date('Y')."-".$mnth."01";


			$this->User->bindModel(
				array ('hasMany' => array(
					'Attendance' => array(
						'className' => 'Attendance',
						'foreignKey' => 'user_id',
						'conditions' => array('MONTH(Attendance.date)' => $mnth, 'YEAR(Attendance.date)'=>$year),
						'order' => 'Attendance.created ASC',

						'dependent' => true
						)
					),

				'hasOne' => array(
					'Appraisal' => array(
						'className' => 'Appraisal',
						'conditions' => array('Appraisal.effective_from <' => $selected_date),

						'foreignKey' => 'user_id',
						'dependent' => true

						)
					),
				'belongsTo' => array(


					'Leave' => array(
						'foreignKey' => false,
						'type'=>'LEFT',
						'conditions' => array('Leave.user_id = User.id','Leave.month='.$mnth)
						)

					)


				)

				);




			$user=$this->Session->read("SESSION_ADMIN");

			$permission= $this->Common->menu_permissions('employees','admin_list',$user[0]);


			if($permission!="1"){



				$res1=$this->User->find('all', array('conditions' => array('User.status' =>'1','User.id'=>$user['0'])));



			}
			else{

				$res1=$this->User->find('all', array('conditions' => array('User.status' =>'1')));



			}



			$this->set('Emp_record', $res1);




		}
		function admin_download()
		{
			$this->layout = "";
	        // filename for download
			$filename = "Export_salary" . date('Ymd') . ".xls";
			header("Content-Disposition: attachment; filename=\"$filename\"");
			header("Content-Type: application/vnd.ms-excel");
			$this->User->bindModel(
				array ('hasMany' => array(
					'Attendance' => array(
						'className' => 'Attendance',
						'foreignKey' => 'user_id',
						'conditions' => array('MONTH(Attendance.date)' => date('n'), 'YEAR(Attendance.date)'=>date('Y')),
						'order' => 'Attendance.created ASC',

						'dependent' => true
						)
					),
				'hasOne' => array(
					'Appraisal' => array(
						'className' => 'Appraisal',
						'group'=>'Appraisal.user_id',

						'foreignKey' => 'user_id',

						'dependent' => true
						)
					)

				)

				);

			$user=$this->Session->read("SESSION_ADMIN");
			$role_array=explode(',', $user[2]);
			$d=count($role_array);
			if($d<2){
				if($user[2]==3){
					$res1=$this->User->find('all', array('conditions' => array('User.status' =>'1')));
				}
				else {
					$res1=$this->User->find('all', array('conditions' => array('User.status' =>'1','User.id'=>$user['0'])));

				}

			}

			else{

				$res1=$this->User->find('all', array('conditions' => array('User.status' =>'1')));

			}
			/************************************  Salary sheet heading  *****************/
			$d_heading=array();

			for($i=0;$i<date('j')/2; $i++){

				array_push($d_heading,"");

			}
			array_push($d_heading,"Salary Calculartion  ".date(F)."-".date(Y));
			$flag = false;
			if(!$flag) {
				echo implode("\t", array_keys($row)) . "\r\n"; $flag = true;
			}
			array_walk($row, 'cleanData'); echo implode("\t", $d_heading) . "\r\n";
			/************************************  Salary sheet heading end    *****************/
			/************************************  Salary sheet heading start       *****************/
			$d_head=array();
			array_push($d_head,"Name");
			array_push($d_head,"Salary");
			for($i=0;$i<date('j'); $i++){
				array_push($d_head,($i+1));
			}
			array_push($d_head,"Total");
			$flag = false;
			if(!$flag) {
			// display field/column names as first row
				echo implode("\t", array_keys($row)) . "\r\n"; $flag = true;

			}

			array_walk($row, 'cleanData'); echo implode("\t", $d_head) . "\r\n";

			/************************************   Salary sheet heading end      *****************/

			$cmp=0;
			foreach($res1 as $emp_detail){

				$data=array();

				$user=$res1[$cmp]['User']['id'];
				$user1=$res1[$cmp+1]['User']['id'];
				$cmp++;
				if($user!=$user1)
				{
					$name= $emp_detail['User']['first_name']." ".$emp_detail['User']['last_name'];
					array_push($data,$name);
					$salary= $emp_detail['Appraisal']['appraised_amount'];
					array_push($data,$salary);
					$day_amount=$emp_detail['Appraisal']['appraised_amount']/30;
					$salary=array();
					$salary2=array();
					$c=0;
					foreach($emp_detail['Attendance'] as $attendence ) {
						$c++;
						$dt=$c."".date('M Y');
						$ad=date('D', strtotime($dt));
						if($ad=="Sun"){
							$c++;
							$actual_salary2=1*$day_amount;
							$salary2[]=$actual_salary2;
							$day_string= $p."*".round($day_amount,2)."=".round($actual_salary2,2);
							array_push($data,"Sun");
						}
						if($attendence['hours']>5){$p=1;}else{$p=0.5;}
						$actual_salary=$p*$day_amount;
						$salary[]=$actual_salary;
						$date_string= $p."*".round($day_amount,2)."=".round($actual_salary,2);
						array_push($data,$p);
					}
					$ext_td=date('j')-$c;
					for($k=0;$k<$ext_td;$k++){
						array_push($data,"0");
					}
					$grand_total[]=round(array_sum($salary)+array_sum($salary2),2);
					array_push($data,round(array_sum($salary)+array_sum($salary2),2));
				}
				$flag = false;
				if(!$flag) {
	// display field/column names as first row
					echo implode("\t", array_keys($row)) . "\r\n"; $flag = true;
				}
				array_walk($row, 'cleanData'); echo implode("\t", $data) . "\r\n";
			}
			fclose($filename);
			exit();
		}

		function cleanData(&$str) { $str = preg_replace("/\t/", "\\t", $str); $str = preg_replace("/\r?\n/", "\\n", $str); if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; }
		function admin_employee_leave_other()
		{
			$this->set('title_for_layout','Employee Leave');
			$this->User->bindModel(
				array ('hasMany' => array(
					'Leave_other' => array(
						'className' => 'Leave_other',
						'foreignKey' => 'user_id',

						)
					),
				)
				);
			$leave=$this->User->find('all', array('conditions' => array('User.status' =>'1')));
			$this->set("Emp_record",$leave);
		}


		function admin_update_leave($id = null)
		{
			$id=$this->params['form']['id'];
			$this->layout = false;
			if(!empty($id)){
				$result = $this->Leave_other->find('all', array(
					'conditions'=>array('user_id'=>Sanitize::escape($id))

					));
			}

			if($this->RequestHandler->isAjax()==true){
				if(isset($this->params['form']['key']) && $this->params['form']['key'] == 'delete'){
					$res = $this->Leave_other->deleteAll("Leave_other.user_id in ('".$this->params['form']['id']."')");
					if($res)
						echo 1;
					else
						echo 0;
					exit;
				}else{

					$rcheck=  $this->Leave_other->find('first', array(
						'conditions'=>array('user_id'=>Sanitize::escape($id))

						));
					$user=$rcheck['Leave_other']['user_id'];
					$this->Leave_other->create();
					if(!empty($user)){
						$result = $this->Leave_other->updateAll(array(
							'Leave_other.january' => "'".Sanitize::escape($this->params['form']['january_leave'])."'",
							'Leave_other.february' => "'".Sanitize::escape($this->params['form']['february_leave'])."'",
							'Leave_other.march' => "'".Sanitize::escape($this->params['form']['march_leave'])."'",
							'Leave_other.april' => "'".Sanitize::escape($this->params['form']['april_leave'])."'",
							'Leave_other.may' => "'".Sanitize::escape($this->params['form']['may_leave'])."'",
							'Leave_other.june' => "'".Sanitize::escape($this->params['form']['june_leave'])."'",
							'Leave_other.july' => "'".Sanitize::escape($this->params['form']['july_leave'])."'",
							'Leave_other.august' => "'".Sanitize::escape($this->params['form']['august_leave'])."'",
							'Leave_other.september' => "'".Sanitize::escape($this->params['form']['september_leave'])."'",
							'Leave_other.october' => "'".Sanitize::escape($this->params['form']['october_leave'])."'",
							'Leave_other.november' => "'".Sanitize::escape($this->params['form']['november_leave'])."'",
							'Leave_other.december' => "'".Sanitize::escape($this->params['form']['december_leave'])."'",
							'Leave_other.comment' => "'".Sanitize::escape($this->params['form']['notes'])."'"

							),
array('Leave_other.user_id ' => Sanitize::escape($this->params['form']['id']))
);
}
else{





	$Leave_other=array('id'=>null,'user_id'=>$this->params['form']['id']);
	$d=array('Leave_other'=>$Leave_other);

	$inserted = $this->Leave_other->save($d
		);

	$result = $this->Leave_other->updateAll(array(
		'Leave_other.january' => "'".Sanitize::escape($this->params['form']['january_leave'])."'",
		'Leave_other.february' => "'".Sanitize::escape($this->params['form']['february_leave'])."'",
		'Leave_other.march' => "'".Sanitize::escape($this->params['form']['march_leave'])."'",
		'Leave_other.april' => "'".Sanitize::escape($this->params['form']['april_leave'])."'",
		'Leave_other.may' => "'".Sanitize::escape($this->params['form']['may_leave'])."'",
		'Leave_other.june' => "'".Sanitize::escape($this->params['form']['june_leave'])."'",
		'Leave_other.july' => "'".Sanitize::escape($this->params['form']['july_leave'])."'",
		'Leave_other.august' => "'".Sanitize::escape($this->params['form']['august_leave'])."'",
		'Leave_other.september' => "'".Sanitize::escape($this->params['form']['september_leave'])."'",
		'Leave_other.october' => "'".Sanitize::escape($this->params['form']['october_leave'])."'",
		'Leave_other.november' => "'".Sanitize::escape($this->params['form']['november_leave'])."'",
		'Leave_other.december' => "'".Sanitize::escape($this->params['form']['december_leave'])."'",
		'Leave_other.comment' => "'".Sanitize::escape($this->params['form']['notes'])."'"

		),
array('Leave_other.user_id ' => Sanitize::escape($this->params['form']['id']))
);









}


if($result)
	echo 1;
else
	echo 0;
die;
}
}
$this->set('resultData',$result);
}

function admin_update_salary_comment($id = null)
{
	$id=$this->params['form']['id'];
	$this->layout = false;
	if(!empty($id)){
		$result = $this->Leave_other->find('all', array(
			'conditions'=>array('user_id'=>Sanitize::escape($id))

			));
	}

	if($this->RequestHandler->isAjax()==true){
		if(isset($this->params['form']['key']) && $this->params['form']['key'] == 'delete'){
			$res = $this->Leave->deleteAll("Leave_other.user_id in ('".$this->params['form']['id']."')");
			if($res)
				echo 1;
			else
				echo 0;
			exit;
		}else{

			$rcheck=  $this->Leave->find('first', array(
				'conditions'=>array('user_id'=>Sanitize::escape($id))

				));
			$user=$rcheck['Leave']['user_id'];
			$this->Leave->create();
			if(!empty($user)){
				$result = $this->Leave->updateAll(array(

					'Leave.comment' => "'".Sanitize::escape($this->params['form']['notes'])."'"

					),
				array('Leave.user_id ' => Sanitize::escape($this->params['form']['id']),'month'=>Sanitize::escape($this->params['form']['month']))
				);
			}
			else{





				$Leave=array('id'=>null,'user_id'=>$this->params['form']['id'],'month'=>$this->params['form']['month']);
				$d=array('Leave'=>$Leave);

				$inserted = $this->Leave->save($d
					);

				$result = $this->Leave->updateAll(array(

					'Leave.comment' => "'".Sanitize::escape($this->params['form']['notes'])."'"

					),
				array('Leave.user_id ' => Sanitize::escape($this->params['form']['id']),'month'=>Sanitize::escape($this->params['form']['month']))
				);









			}


			if($result)
				echo 1;
			else
				echo 0;
			die;
		}
	}
	$this->set('resultData',$result);




}

	 // gurpreet

function changepassforall(){	

	if ($this->request->is('ajax')) {			
		if(isset($this->request['data']['key'])&& $this->request['data']['key'] == 'yes'){
			$res = $this->Users->find('all', [
				'fields'=>['id','first_name','username'],
				'conditions' =>['status' => 1 ]
				]);
			//pr($res); die;
			$change=array();
			$i=0;
			foreach($res as $pass){
			    $resetPassword = rand(100000,999999);
				$change[$i]['id']=$pass['id'];
				$change[$i]['pass']=$resetPassword;
				$change[$i]['email']=$pass['username'];
				$change[$i]['name']=$pass['first_name'];
				$credentials = TableRegistry::get('Users');
				$pwd= (new DefaultPasswordHasher)->hash($resetPassword);
				$update = ['password' =>$pwd];
                $credentials->updateAll($update,['id' =>$pass['id']]);
                $i++;
			}
			$subject = "Updated Password For all users";
			$email = new Email();
			$email->template('all_pass_change')
			->emailFormat('html')
			->to([ADMIN_EMAIL,'dev.drawtopic@gmail.com'])
			->from('dev.drawtopic@gmail.com')
			->viewVars(['content' => $change])
			->send();
			if (count($email)>0){
				$message="Password has been changed and mail is send at info@vlogiclabs.com";
				$this->Flash->success_new($message,'layout_success');
				echo '1';exit;
			}else{
				$message="Some error occurred.";
				$this->Flash->error_new($message,'layout_error');		  
				echo '0';exit;
			}
		}
	}
}


function activities($id = null, $key = null){
    	if(!empty($this->request->query['key'])){
    		$key = $this->request->query['key'];
    	}
    	$this->viewBuilder()->layout('new_layout_admin');
    	$this->set("title_for_layout", "My Tickets");
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$this->set('session_info',$session_info);
    	$userID = $session_info[0];
    	$condition_check = [];
    	
	}


function getOrigin($id = null, $key = null){
    		if(isset($this->request->data['key'])){
    			$key = $this->request->data['key'];	
    		} else{
    			$key ="";
    		}
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
    	$condition_check = [];
		if(isset($this->request->data['check_it']) && $this->request->data['check_it']=='1'){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			];
		}else {
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1,2]
			];
		}
		/*pr($cond); die;*/
	$deadline =  $this->Tickets->find("all")
	->where([
		'OR' => [['from_id' => $userID], ['to_id' => $userID]],
		'status IN' => [1,2]
		])
	->order(['created' => 'DESC'])->first();
	
	isset($deadline)? $deadline->toArray():array();
	$oderby = ['Tickets.modified' => 'DESC'];
	if(isset($this->request->data['filter']) && $this->request->data['filter'] !== 'none') {
		$result = array();
		$oderby = ($this->request->data['filter'] == 'modified') ? ['Tickets.modified' => 'asc'] : ['Tickets.deadline' => 'asc'];
		if(!empty($key) && $this->request->data['check_it'] !== 1) {
		
			$today = date("Y-m-d 00:00:00");
			if($key == "open"){
				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [1],
				'AND' => ['deadline >' =>$today]
				] ;
			}elseif($key == "close"){

				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [0]
				] ;

			}elseif($key == "expire"){


				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [1,2],
				'AND' => ['Tickets.deadline <' =>$today]
				] ;
			}elseif($key == "inprogress"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [2]
			];
			}
		}
	}elseif(!empty($key)) {
		$today = date("Y-m-d 00:00:00");
		if($key == "open"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1],
			'AND' => ['deadline >' =>$today]
			] ;
		}elseif($key == "close"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [0]
			];
		}elseif($key == "expire"){
			$cond =	[
			'OR' => [['Tickets.from_id' => $userID], ['Tickets.to_id' => $userID]]/*,['deadline <' =>  $today]*/,
			'Tickets.status IN' => [1,2],
			"Tickets.deadline <" =>  $today
			] ;
		} elseif($key == "inprogress"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [2]
			];
		}
	}elseif(!empty($this->request->data['reminderid']) && $this->request->data['reminderid'] != 'none'){
		$info = $this->Tickets->find()
		->where([' Tickets.id'=> $this->Common->DCR($this->Common->dcrForUrl($this->request->data['reminderid'])) ])
		->select(['Tickets.title','Tickets.to_id','Tickets.from_id',
			'Tickets.modified','User_To.id','User_To.first_name','User_To.last_name',
			'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username'])->contain(['User_To','User_From'])->first();
		if($userID == $info['to_id'] || $userID == $info['from_id']){
			if($userID == $info['User_To']['id'])
			{
				$receivername = $info['User_From']['first_name']." ".$info['User_From']['last_name'];
				
				$receiverMail = $info['User_From']['username'];
			}else{
				$receivername = $info['User_To']['first_name']." ".$info['User_To']['last_name'];
				$receiverMail = $info['User_To']['username'];
			}
			$sender =  $this->Users->find()
			->where(['Users.id'=>$userID])
			->select(['Users.first_name','Users.last_name','Users.username'])->first();
			$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has requested update to following ticket.<br/><br/>
				".$info['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail)
				->bcc( $sender['username'])
				->subject("Ticket Reminder (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message);

				// set values to be used on email template
				if ($email->send($message)) {
					echo json_encode(['status' => 'success']);
				}else{
					echo json_encode(['status' => 'failed']);
				}
				die;
				/*$this->redirect(array('action' => 'index'));*/
			}else{
				
				$this->redirect(array('action' => 'index'));

			}
		}

		if(!empty($this->request->data['ticket_id']) && $this->request->data['ticket_id'] != 'none'){
			$cond =['Tickets.id' => $this->request->data['ticket_id']];
		}
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$today = date("Y-m-d 00:00:00");
			$open_cond = [
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1],
			'AND' => ['deadline >' =>$today]
			];
			/*[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 1
					];*/
			$inprogress_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 2
					];
			$closed_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 0
			];
			$expired_cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],['deadline' < '$today'],
			'Tickets.status IN' => [1,2],
			'AND' => ['deadline <' =>$today]
			] ;
			// Tickets Related Code 
			/*$oderby = ['deadline' => 'asc'];*/
			$open = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($open_cond)
					->order($oderby)->count();
			$inprogress = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($inprogress_cond)
					->order($oderby)->count();
			$closed = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($closed_cond)
					->order($oderby)->count();
			$expired = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($expired_cond)
					->order($oderby)->count();

			$this->set('open',$open);
			$this->set('inprogress',$inprogress);
			$this->set('closed',$closed);
			
		/*pr($cond); die;*/
		if(isset($this->request->data['pagetype']) &&  $this->request->data['pagetype'] == 'pre') {
			$this->request->data['pageno'] = $this->request->data['pageno'] - 2;
			/*why 2? because we will increase 1 at $next_page var*/
		}
		$tofetch = 20;
		$start_index = ($tofetch * $this->request->data['pageno']) - 20;
		$result = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name'],
			'offset' => $start_index,
			'limit' => 20,
			])->contain(['User_To','User_From','Replier'])->all()->toArray();
		$paginatedarr = $result;
		$arr = [];
		$tofetch = 20;

		 /*else{
			$start_index = ($tofetch * $this->request->data['pageno']) - 20;
		}*/
	/*	$start_index = ($tofetch * $this->request->data['pageno']) - 20;*/
		$resultcount = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name']
			])->count();


		$totalpages = ceil($resultcount/20);
		$next_page = $this->request->data['pageno'] ;


		foreach($result as $ticket){
			$ticket['idp'] = $ticket->id;
			$ticket['id'] = $this->Common->encForUrl($this->Common->ENC($ticket->id));
			$arr[] = $ticket;
		}
		echo json_encode(['status'=> 'success',
						'deadline'=> $deadline,
						'userID' => $userID,
						'resultData'=> $paginatedarr,
						'open' => $open, 
						'inprogress' => $inprogress,
						'closed'=> $closed,
					    'expired' => $expired,
					    'totalrecords' => $resultcount,
					    'pageno' => $next_page,
					    'totalpages' => ($totalpages == 0 ) ? 1: $totalpages ,
					    'currentpage' => $this->request->data['pageno'],
					    'currentrecodsfrom' => $start_index + 1,
					    'tofetch' => $tofetch,
					    'today' => $today = date("Y-m-d 00:00:00")
					 ]);
		die;
		$this->set('deadline',$deadline);
		$this->set('userID',$userID);
		$this->set('resultData',$result);
	}










}