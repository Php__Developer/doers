<?php

namespace App\Controller\CredentialAudits;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email; 
use Cake\Routing\Router;                       



class CredentialAuditsController extends AppController {

  

    /* * *********** START FUNCTIONS ************************* */

    #_________________________________________________________________________#  
    /**
     * @Date: Nov,2016               
     * @Method : beforeFilter   
     * @Purpose: This function is called before any other function.  
     * @Param: none                                    
     * @Return: none                                       
     * */

    function beforeFilter(Event $event) { 
     // This  function is called first before parsing this controller file

 // start to check permission
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
        $controller=$this->request->params['controller'];
        $action=$this->request->params['action'];
        $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
        if($permission!=1){
           $this->Flash->error("You have no permisson to access this Page.");
           return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
       }
    // end code to check permissions
       
       if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

        $this->checkUserSession();
    } else {
       $this->viewBuilder()->layout('layout_admin');
       
   }
   Router::parseNamedParams($this->request);
   $session = $this->request->session();
   $userSession = $session->read("SESSION_ADMIN");
   if($userSession==''){
    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
}
$this->set('common', $this->Common);
}

 /**
     * @Date: Nov,2016               
     * @Method : initialize   
     * @Purpose: This function is called initialize any other function.  
     * @Param: none                                    
     * @Return: none                                       
     * */
 public function initialize()
 {
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Credentials');
        $this->loadModel('CredentialLogs');
        $this->loadModel('CredentialAudits');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $this->loadModel('CredentialAudits');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }

	 #_________________________________________________________________________#   
    /**
     * @Date: Nov,2016       
     * @Method : index         
     * @Purpose: This function is the default function of the controller  
     * @Param: none                                     
     * @Return: none                                          
     * */

    function index() {
        $this->render('login');
        if ($this->Session->read("SESSION_USER") != "") {
            $this->redirect('dashboard');
        }
    }

	 #_________________________________________________________________________#   
    /**
     * @Date: Nov,2016     
     * @Method : auditdetails        
     * @Purpose: This function is for audits details                                         
     * */
    
    function auditdetails($id = null) {
      $id = $this->Common->mc_decrypt($id, ENCRYPTION_KEY);
       // pr($id);
      $session = $this->request->session();
      $this->set('session',$session);  

      $this->viewBuilder()->layout(false);
      $this->set("audit_id", $id);
      $userData = $this->Users->find('all')->toArray();
      $this->set('userData', $userData);
      $res = $this->CredentialAudits->find('all',[
        'conditions' => ['CredentialAudits.profile_id' => $id] ])
      ->order(['CredentialAudits.modified' => 'desc'])
      ->contain(['Users'])->toArray();
      $this->set("audit_list", $res);
      if($this->request->is('ajax')==true){
        $userSession = $session->read("SESSION_ADMIN");
        $resolveby = $userSession[0];
        $data['CredentialAudit']['profile_id'] = ($this->request->data['profile_id']);
        $data['CredentialAudit']['auditpoint'] = ($this->request->data['auditpoint']);
        $data['CredentialAudit']['status'] = '1';
        $data['CredentialAudit']['created_by'] = $resolveby;
        $data['CredentialAudit']['created'] = date('Y-m-d H:i:s');
        $data['CredentialAudit']['modified'] = date('Y-m-d H:i:s');
        $CredentialAudits  = $this->CredentialAudits->newEntity($data['CredentialAudit']); 
        $result=  $this->CredentialAudits->save($CredentialAudits);
        $last_id = $result->id;
        $last_res = $this->CredentialAudits->find('all',[
            'conditions' => ['CredentialAudits.id' => $last_id] ])
        ->contain(['Users'])->first()->toArray();
        $last_audit_id = $last_res['id'];
        if ($last_res['status'] == 1) {
            $colorchange = "audit_open";
        } else {
            $colorchange = "audit_close";
        }
        $last_row = "<tr  id=togle_color_".$last_audit_id." class=" . $colorchange . ">
        <td>" . $last_res['auditpoint'] . "</td>
        <td>" . "". "</td>
        <td>" . $last_res['name'] . "</td>
        <td>" . $last_res['modified'] . "</td>
        <td><span id='status_$last_audit_id'>";
        if($val['status']==1){
            $last_row .= '<button type="button" class="change_button_size"  id="open_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');"  style="display: none;">Close</button>';
            $last_row .= '<button type="button" class="change_button_size" id="close_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');"">Open</button>';
        }else{
            $last_row .= '<button type="button" class="change_button_size" id="open_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');" >Close</button>';
            $last_row .= '<button type="button"  class="change_button_size" id="close_'.$last_audit_id.'" onclick="change_status(\'' . $last_audit_id . '\',\'' . $last_res['status'] .'\');" style="display: none;">Open</button>';
        }
        
        $last_row.= '<a href="javascript:void(0)" class="info-tooltip icon-2 delete" title="Delete" id="del_'.$last_audit_id .'" onclick="if(is_delete()){deleteRecord('.$last_audit_id.');}"></a>';         
        $last_row .= "</span></td>
        </tr>";
        if ($result) {
         
            echo $last_row;
            exit;
        } else {
         
            echo "0";
            exit;
        }
    }
    
}

    #_________________________________________________________________________#
    /** 	
     * @Date: Nov,2016
     * @Method : changeStatus
     * @Purpose: This function is to change the status of profile audit.	
     * @Param: $id	
     * @Return: none 	
     * @Return: none 
     * */

    function changeStatus() {
     $session = $this->request->session();
     $this->set('session',$session); 
     if($this->request->is('ajax')==true){
        $id = $this->request->data['id'] = ($this->request->data['id']);
        $status = $this->request->data['status'] = ($this->request->data['status']);
        $userSession = $session->read("SESSION_ADMIN");
        $resolveby = $userSession[0];
        if (($this->request->data['status']) == 1) {
            $status = 2;
            $temp = "closed";
            
            $resolvedby1= $this->request->data['resolvedby'] = $resolveby;
        } else if (($this->request->data['status']) == 2) {
            $status = 1;
            $temp = "open";
            $resolvedby1= $this->request->data['resolvedby'] = $resolveby;
        }
        $result = $this->CredentialAudits->updateAll(['status' => $status,'resolvedby'=> $resolvedby1], ['id' => $id]);
        if ($result) {
          
            echo "1";
            $this->request->data['resolvedby'] = $resolveby;
            exit;
        } else {
          
            echo "0";
            exit;
        }
    }
}



    #_________________________________________________________________________#
    /** 	
     * @Date: Nov,2016	
     * @Method : delete
     * @Purpose: This function is to delete profile audits from admin section.	
     * @Param: $id	
     * @Return: none 	
     * @Return: none 
     * */
    function delete($id = null) {
       $credentialaudit = $this->CredentialAudits->get($this->request->data['id']);
       if ($this->CredentialAudits->delete($credentialaudit)) {
        $this->Flash->success("CredentialAudits(s) deleted successfully.");
        $this->setAction('auditdetails');

    }

}
}