<?php

namespace App\Controller\Tags;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Admin\UsersController;
use App\Controller\Skills\SkillsController;
use App\Controller\Employees\EmployeesController;
use App\Controller\Evaluations\EvaluationsController;
use App\Controller\Generics\GenericsController;
use App\Controller\Hardwares\HardwaresController;
use App\Controller\Milestones\MilestonesController;
use App\Controller\Component\ComponentController;
use App\Controller\CredentialAudits\CredentialAuditsController;
use App\Controller\Billings\BillingsController;
use App\Controller\Contacts\ContactsController;
use App\Controller\Resumes\ResumesController;
use App\Controller\StaticPages\Static_pagesController;
use App\Controller\Testimonials\TestimonialsController;
use App\Controller\Tickets\TicketsController;
use App\Controller\Permissions\PermissionsController;
use App\Controller\PaymentMethods\Payment_methodsController;
use App\Controller\Payments\PaymentsController;
use App\Controller\Reports\ReportsController;
use App\Controller\Profileaudits\ProfileauditsController;
use App\Controller\Sales\SalesController;
use App\Controller\LearningCenters\Learning_centersController;
use App\Controller\Leads\LeadsController;
use App\Controller\Credentials\CredentialsController;
use App\Controller\Attendances\AttendancesController;
use App\Controller\Appraisals\AppraisalsController;
use App\Controller\Projects\ProjectsController;
use App\Controller\Roles\RolesController;
use App\Controller\Agencies\AgenciesController;
use App\Controller\Tags\TagsController;
use App\Controller\Entities\EntitiesController;
use Cake\Cache\Cache;

class TagsController extends AppController {

var $paginate      =  array(); //pagination

    var $uses          =  array('Modules'); // For Default Model


    /******************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
      parent::beforeFilter($event);
                  // star to check permission
      

      $session = $this->request->session();
      $userSession = $session->read("SESSION_ADMIN");
      $controller=$this->request->params['controller'];
      $action=$this->request->params['action'];
     /* $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
      if($permission!=1){
        $this->Flash->error("You have no permisson to access this Page.");

        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
      }*/
    // end code to check permissions
      Router::parseNamedParams($this->request);
      if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

        $this->checkUserSession();
      } else {
        $this->viewBuilder()->layout('layout_admin');

      }

      $session = $this->request->session();
      $userSession = $session->read("SESSION_ADMIN");
      if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
      }
      $this->set('common', $this->Common);
    }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize  function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
  parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Agencies');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Skills');
        $this->loadModel('Modules');
        $this->loadModel('Tags');
        $this->loadModel('Entities');
        $this->loadModel('Roles');
        $this->loadModel('EntityAgency');
        $this->loadModel('TagAgency');
        
        $this->loadModel('CredentialLogs');
        $this->loadModel('Permissions');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    /**                         
    * @Date: 24-feb -2017     
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    public function register(){
      if($this->request->data){
        //pr($this->request->data); die;
      }
    }

    function list(){
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
      $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);  
      $this->set('title_for_layout','User Tags Listing');
      $this->set("pageTitle","User Tags Listing");   
      $this->set("search1", "");    
      $this->set("search2", "");  
      $criteria = ['is_deleted' => 'No'];

    //pr($criteria); die;
    // Delete user and its licences and orders(single/multiple)

    if(isset($this->request->data) || !empty($this->request->query)) {
        if(!empty($this->request->data['Tags']['fieldName']) || isset($this->request->query['field'])){

            if(trim(isset($this->request->data['Tags']['fieldName'])) != ""){
                $search1 = trim($this->request->data['Tags']['fieldName']);
            }elseif(isset($this->request->query['field'])){
                $search1 = trim($this->request->query['field']);
            }
            $this->set("search1",$search1);
        }

        if(isset($this->request->data['Tags']['value1']) || isset($this->request->data['Tags']['value2']) || isset($this->request->query['value'])){
            if(isset($this->request->data['Tags']['value1']) || isset($this->request->data['Tags']['value2'])){
                $search2 = ($this->request->data['Tags']['fieldName'] != "Tags.status")?trim($this->request->data['Tags']['value1']):$this->request->data['Tags']['value2'];
            }elseif(isset($this->request->query['value'])){
                $search2 = trim($this->request->query['value']);
            }
            $this->set("search2",$search2);
        }
        //echo $search1."------".$search2;
        /* Searching starts from here */
     



        if(!empty($search1) && (!empty($search2) || $search1 == "Tags.status")){

 $query  =    $this->Tags->find('all' , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
               ]);
             $result = isset($query)? $query->toArray():array();
      $criteria = $search1." LIKE '%".$search2."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
     
 }else{  
       $query  =  $this->Tags->find('all');   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }

  
 $urlString = "/";
      if(isset($this->request->query)){                   
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }

  $this->set('urlString', $urlString);
      $urlString = "/";  
      if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
    $this->set('urlString', $urlString);  

$this->paginate = [           
/*    'fields' => [ 
    'id',
    'agency_name',
    'first_name',
    'last_name',
    'username'
    ''
],*/     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     
//pr( $criteria); die;
$data = $this->paginate('Tags',['conditions' => $criteria]);  
$this->set('resultData', $data);  
$this->set('pagename',"Tagslist");  

    }
    
     function add() {   
           $this->viewBuilder()->layout('new_layout_admin');
           $this->set('title_for_layout','Add Entity');    
           $this->pageTitle = "Add Entity";      
           $this->set("pageTitle","Add Role"); 
           $mode = "add";   
           if($this->request->data){
                  $isticket = [];
                   $isticket['on_ticket'] = 'no';
                   $isticket['content'] = "";
                   $this->request->data['on_ticket'] = json_encode($isticket);
            $entity = $this->Tags->newEntity($this->request->data());
            if(empty($entity->errors())){
             if($this->Tags->save($entity)){
                  foreach ($this->request->data['agency_id'] as $agency_id) {
                    foreach ($this->request->data['role_id'] as $role_id) {
                            $eadata['agency_id'] = $agency_id;
                            $eadata['role_id'] = $role_id;
                            $eadata['tag_id'] = $entity->id;
                            $TagAgency = $this->TagAgency->newEntity($eadata);
                            $this->TagAgency->save($TagAgency);
                    } 
                   }
                   if(count($this->request->data['entity_id']) > 0){
                        foreach($this->request->data['entity_id'] as $entity_id){
                          $dbentity = $this->Entities->get($entity_id);
                          $exp = explode(',', $dbentity->tag_id);
                          $exp[] = $entity->id;
                          $dbentity->tag_id = implode(',', $exp);
                          $this->Entities->save($dbentity);
                        }
                   }
                    Cache::clearGroup('short','short');
                   $this->Flash->success_new("Tags has been created successfully.");
                   $this->redirect(array('action' => 'list'));   
             }
         }else{
          $this->set("errors", $entity->errors());
         }
         //DELETE CACHES 


      }
    $session = $this->request->session();
    $this->set('session',$session);
    $session_info = $session->read("SESSION_ADMIN");
    $userroles = explode(',', $session_info[2]);
    $agencies = $this->Agencies->find('list',[
        'keyField' => 'id',
        'valueField' => 'agency_name',
        'conditions' => []
    ])->toArray();
    $roles = $this->Roles->find('list',[
        'keyField' => 'id',
        'valueField' => 'role',
        'conditions' => []
    ])->toArray(); 
    //pr($entitiesids->toArray()); die;
    $entities = $this->Entities->find('list',[
      'keyField' => 'id',
      'valueField' => 'name',
      'conditions' => ['is_deleted' => 'No']
      ])->toArray();
    $Tags =  $this->Tags->find('list', [
                  'keyField' => 'id',
                  'valueField' => 'name',
                  'conditions' => [],
                  'fields' => ['id','name']
              ])->toArray();
    $this->set('Tags',$Tags);
    $this->set('agencies',$agencies);
    $this->set('roles',$roles);
    $this->set('entities',$entities);
} 
 



 function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
         $id = (is_numeric($id) ) ?  $id : $this->Common->DCR($this->Common->dcrForUrl( $id ) );
        if ($this->Tags->updateAll(['is_deleted' => 'Yes'],['id' => $id ]) ) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }

function edit($id = null) { 
   $session = $this->request->session();
   $userSession = $session->read("SESSION_ADMIN");
  if($this->request->is('ajax') == true){
    //  pr($this->request->data); die;
      if($this->request->data['type'] == 'name'){
        $this->Tags->updateAll(['name' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      } else if ($this->request->data['type'] == 'alias'){
          $this->Tags->updateAll(['alias' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      } else if ($this->request->data['type'] == 'notes'){
          $this->Tags->updateAll(['notes' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
          echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
          die;   
      }
      
      } 

}

function manage($id = null){
  $this->viewBuilder()->layout(false);
      if($this->request->is('ajax') == true){
        $agencies = $this->Agencies->find('list',[
                'keyField' => 'id',
                'valueField' => 'agency_name',
            'conditions' => []
            ])->toArray();
            
            $roles = $this->Roles->find('list',[
                'keyField' => 'id',
                'valueField' => 'role',
                'conditions' => []
            ])->toArray();

        $this->viewBuilder()->layout(false);
        $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));
            if($this->request->data['key'] =='init'){
                $entityagencies = $this->TagAgency->find('all',[
                    'conditions' => ['tag_id' => $id]
                  ]);
                $finalarr = [];
                foreach ($entityagencies as $value) {
                    $agency = $this->Agencies->get($value['agency_id']);
                    $role = $this->Roles->get($value['role_id']);
                    $value['Agency'] = $agency;
                    $value['Role'] = $role;
                    $finalarr[] = $value;
                }
                $allentities = $this->Entities->find('list',[
                    'keyField' => 'id',
                    'valueField' => 'name'
                  ])->toArray();
                $entities = $this->Entities->find('all',[
                    'fields' => ['id','name','tag_id'],
                    'conditions' => ['FIND_IN_SET(\'' . $id . '\',tag_id)']
                  ])->toArray();
                $finalentities = [];
                    foreach ($entities as $entity) { 
                      $exp = explode(',', $entity->tag_id);
                      if(in_array($id, $exp)){
                          $finalentities[$entity->id] = $entity->name;
                      }
                    }
                    $resultJ = json_encode(['response' => 'success','data' => $finalarr,'agencies' => $agencies,'roles' =>$roles,'entities' => $finalentities ,'allentities' => $allentities]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='agencyroles'){
              $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));

                $entityagencies = $this->TagAgency->find('all',[
                    'conditions' => ['tag_id' => $id ,'agency_id' => $this->request->data['agency_id']]
                  ]);
                    $finalarr = [];
                    $rolesonly = [];
                 foreach ($entityagencies as $value) {
                    $agency = $this->Agencies->get($value['agency_id']);
                    $role = $this->Roles->get($value['role_id']);
                    $value['Agency'] = $agency;
                    $value['Role'] = $role;
                    $rolesonly[] = $role['role'];
                    $finalarr[] = $value;
                }
                    $resultJ = json_encode(['response' => 'success','data' => $finalarr,'agencies' => $agencies,'roles' =>$roles , 'selectedroles' => $rolesonly ]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='addnewrole'){
                    $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));
                    $this->request->data['tag_id'] = $id;
                    $eadata = [];
                    $eadata['agency_id'] = $this->request->data['agency_id'];
                    $eadata['role_id'] = $this->request->data['role_id'];
                    $eadata['tag_id'] =  $this->request->data['tag_id'];
                    $TagAgency = $this->TagAgency->newEntity($eadata);
                    $this->TagAgency->save($TagAgency);

                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='removerole'){
                    $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));
                     $role = $this->Roles->find('all',[
                             'conditions'  => ['role' => ucwords($this->request->data['name']) ]
                      ])->first();
                    $e = $this->TagAgency->find('all',[
                             'conditions'  => ['role_id' => $role['id'] , 'tag_id' => $id, 'agency_id' => $this->request->data['agency_id'] ]
                      ])->first();

                    $TagAgency = $this->TagAgency->get($e['id']);
                    $result = $this->TagAgency->delete($TagAgency);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if ($this->request->data['key'] =='addentity'){
                    $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));  
                    $entity = $this->Entities->get($this->request->data['entity_id']);
                    $tags = $entity->tag_id;
                    $exp =  explode(',', $tags);
                    $exp[] = $id;
                    $entity->tag_id = implode(',', array_unique($exp));
                    $this->Entities->save($entity);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            } else if($this->request->data['key'] =='removeentity'){
                    $id = (is_numeric($this->request->data['tag_id']) ) ? $this->request->data['tag_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['tag_id']));  
                    $entity = $this->Entities->get($this->request->data['entity_id']);
                    $tags = $entity->tag_id;
                    $exp =  explode(',', $tags);
                    $key  = array_search($id, $exp);
                    if($key !== false){
                      unset($exp[$key]);
                    }
                    $entity->tag_id = implode(',', array_unique($exp));
                    $this->Entities->save($entity);
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            }else if($this->request->data['key'] =='delete_tag_agency'){
                    $agency = $agencies = $this->Agencies->find('all',[
                                  'conditions' => ['agency_name' => $this->request->data['agency_id'] ]
                              ])->first();
                    if(count($agency) > 0){
                        $tagagency = $this->TagAgency->find('all',[
                          'conditions' => ['tag_id' => $id,'agency_id' => $agency['id'] ] 
                        ]);
                        if(count($tagagency) > 0){
                          foreach($tagagency as $t){
                              $TagAgency = $this->TagAgency->get($t['id']);
                              $result = $this->TagAgency->delete($TagAgency);                          
                          }
                        }
                          
                    }






                     $resultJ = json_encode(['response' => 'success']);
                      $this->response->type('json');
                      $this->response->body($resultJ);
                      return $this->response; 
            } 
            
      }
   $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));        
   $entity = $this->Tags->get($id);
    $this->set('entity',$entity);    
}



function edit_old($id = null) { 
    $this->viewBuilder()->layout('new_layout_admin');
    if($this->request->data){ 
      $id = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id']));  
          $entitydata = $this->Tags->get($id, [
                  'contain' => ['Entityagencies']
         ]);
          $this->request->data['id'] = $id;
          $entity = $this->Tags->patchEntity($entitydata,$this->request->data);
        if(empty($entity->errors())) {
          if($this->Tags->save($entity)){
              $agencies =[];
              $roles =[];
              foreach ($entitydata['Entityagencies'] as $e) {
                      if(!in_array($e['agency_id'],$this->request->data['agency_id'])){
                          $this->TagAgency->deleteAll([
                                'id' => $e['id']
                            ]);
                      }
                      if(!in_array($e['role_id'],$this->request->data['role_id'])){
                          $this->TagAgency->deleteAll([
                                'id' => $e['id']
                            ]);
                      }

                      $agencies[] = $e['agency_id'];
                      $roles[] = $e['role_id'];
                   }
              foreach ($this->request->data['agency_id'] as $agency_id) {
                //pr($agencies); die;
                      if(!in_array($agency_id , array_unique($agencies)) ){
                           foreach ($this->request->data['role_id'] as $role_id) {
                              //if(!in_array($role_id , array_unique($roles))){
                                $eadata['agency_id'] = $agency_id;
                                $eadata['role_id'] = $role_id;
                                $eadata['tag_id'] = $id;
                                $TagAgency = $this->TagAgency->newEntity($eadata);
                                $this->TagAgency->save($TagAgency);  
                         }
                      } else {
                         foreach ($this->request->data['role_id'] as $role_id) {
                              if(!in_array($role_id , array_unique($roles))){
                                $eadata['agency_id'] = $agency_id;
                                $eadata['role_id'] = $role_id;
                                $eadata['tag_id'] = $id;
                                $TagAgency = $this->TagAgency->newEntity($eadata);
                                $this->TagAgency->save($TagAgency);  
                              }
                         }
                      }   
                   }
          }
        $this->Flash->success_new(" updated successfully.");
        $this->redirect(array('controller' => 'Tags', 'action' => 'list')); 

      }else{ 

        $this->set("errors", $entity->errors());
      } 
    }else if(!empty($id)){
      $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));  
      $agencies = $this->Agencies->find('list',[
                'keyField' => 'id',
                'valueField' => 'agency_name',
            'conditions' => []
            ])->toArray();
            $roles = $this->Roles->find('list',[
                'keyField' => 'id',
                'valueField' => 'role',
                'conditions' => []
            ])->toArray();

           $mode = "add";  
        $this->set('roles',$roles);
        $this->set('agencies',$agencies);
        $RolesData = $this->Tags->get($id, [
                  'contain' => ['Entityagencies']
         ]);
     
      $this->set('RolesData',$RolesData); 
      if(!$RolesData){             
        $this->redirect(array('controller' => 'Tags', 'action' => 'list'));                 
      }                                                      
    }                                                          
  }    

  public function tagcontent($id=null) {

     $this->viewBuilder()->layout(false);
      if($this->request->data){ 
         $id = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id']));
         $tag = $this->Tags->get($id);
                   $isticket = [];
                   $isticket['on_ticket'] = 'yes';
                   $isticket['content'] = $this->request->data['content'];
                   $this->request->data['on_ticket'] = json_encode($isticket);
                   //pr( $this->request->data); die;
                   $this->Tags->updateAll(['on_ticket' => $this->request->data['on_ticket'] ],['id' => $id ]);
            //if ( ) {
                //echo json_encode(['response'=> 'success']); die;
                    $resultJ = json_encode(['response' => 'success']);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
             //}else { 
                //echo json_encode(['response'=> 'failed']); die;
              //}

      }
     $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));  
     $tag = $this->Tags->get($id);
     $this->set('tag',$tag);
  }
    


} /**/// class ending brace

