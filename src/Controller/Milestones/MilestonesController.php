<?php   

namespace App\Controller\Milestones;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;


class MilestonesController extends AppController {

    /**************** START FUNCTIONS **************************/

   /** * @Date: 13-sept-2016   
   * @Method : beforeFilter    
  * @Purpose: This function is called before any other function.  
   * @Param: none   
  * @Return: none 
  **/ 
   function beforeFilter(Event $event) {
      parent::beforeFilter($event);
      Router::parseNamedParams($this->request);
      if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

        $this->checkUserSession();
    } else {
        $this->viewBuilder()->layout('layout_admin');
    }
    $session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");
    if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common', $this->Common);
}


        /** 
        * @Date: 13-sept-2016
        * @Method : initialize
        * @Purpose: This function is the default function of the controller
        * @Param: none  
        * @Return: none     
        **/ 
        public function initialize()
        {
            parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Billings'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Payments');
        $this->loadModel('Milestone');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Leads'); // for importing Model
        $this->loadModel('Projects');
    }

    #_______________________________________________________________________#   
    /**
     * @Date: 13-sept-2016      
     * @Method : index         
     * @Purpose: This function is the default function of the controller  
     * @Param: none                                     
     * @Return: none                                          
     * */

    function index() {
        $this->render('login');
        if ($this->Session->read("SESSION_USER") != "") {
            $this->redirect('dashboard');
        }
    }

    
    /**
     * @Date: 13-sept-2016   
     * @Method : milestoneslist    
     * @Purpose: This function is to view milestone projects.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
    public function milestoneslist($id = null) {

      $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id)); 
      $this->set('title_for_layout','Edit Lead');     
      $this->pageTitle = "Edit Lead";    
      $this->set("pageTitle","Edit Lead");
      $this->viewBuilder()->layout(false);
      $res = $this->Milestones->find('all',[
        'conditions' => ['project_id' => $id]
        ])
      ->contain(['Users','Projects'])->toArray();
      $verifyStatus = array();
      foreach ($res as $verify) {
        $verifyStatus[$verify['id']] = $verify['verified'];
    }
    $this->set('verified', $verifyStatus);
    $this->set('resultData', $res);
     $this->set('project_id', $id);
}

   /**
     * @Date: 13-sept-2016    
     * @Method : add 
     * @Purpose: This function is to add milestone.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
   function add($id = null) {
      $session = $this->request->session();
      $this->set('session',$session);  
      $userSession = $session->read("SESSION_ADMIN");
    if($this->request->is('ajax')==true){
       $this->request->data['currency']  = $this->request->data['currency'];
       $this->request->data['amount']  = $this->request->data['amount'];
       $this->request->data['notes']   = $this->request->data['notes'];
       $this->request->data['project_id']  = (is_numeric($this->request->data['project_id']) ) ? $this->request->data['project_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['project_id'])); 
       $this->request->data['user_id']  = $this->request->data['user_id'];
       $this->request->data['verified'] = '0';
       $this->request->data['created']   = date('Y-m-d-h-s-i');
       $this->request->data['milestonedate'] =  date('Y-m-d'); 
       $this->request->data['agency_id'] = $userSession[3];
       $data = $this->Milestones->newEntity($this->request->data);
       $res = $this->Milestones->save($data);
       $last_id  = $res->id;
       $Milestones = TableRegistry::get('Milestones');
       $last_res = $this->Milestones->find('all',[
        'conditions' => ['Milestones.id' => $last_id],
        'fields' => ['Milestones.id','Milestones.milestonedate','Milestones.currency','Milestones.amount','Milestones.notes','Projects.project_name','Users.first_name']
        ])->contain(['Users','Projects'])->first()->toArray();
       $id = $last_res['id'];
       $last_res['id'] = $this->Common->encForUrl($this->Common->ENC($last_res['id']));
      /* unset($last_res['project']['id']);
       unset($last_res['project']['agency_id']);
       unset($last_res['project']['client_id']);
       unset($last_res['project']['team_id']);
       unset($last_res['user']['id']);*/
      // pr($last_res); die;
     /* $lastRow = "<tr>
       <td><span id='week_$id'>" . $last_res['project']['project_name'] . "</span></td>
       <td><span id='user_edit_$id'>" . $last_res['user']['first_name'] . "</span></td>
       <td><span id='milestonedate_$id'>" . $last_res['milestonedate'] . "</span></td>
       <td><span id='currency_$id'>" . $last_res['currency'] . "</span></td>
       <td><span id='amount_$id'>" . $last_res['amount'] . "</span></td>
       <td><span id='notes_$id'>" . $last_res['notes'] . "</span></td>

       <td style='text-align:center'>";*/
      /* if ($last_res['verified'] == 0) {
        $lastRow.='<span  class="text-danger" id="show_img_'.$id.'">Deactivated</span>';
        $lastRow.=' <span  style="display:none;"class="text-success" id="hide_img_'.$id.'">Activated</span>';
    } else if ($last_res['verified'] == 1) {
       $lastRow.=' <span  class="text-success" id="hide_img_'.$id.'">Activated</span>';
       $lastRow.='<span  style="display:none;" class="text-danger" id="show_img_'.$id.'>Deactivated</span>';
   }
   $lastRow.="</td>
   <td>";
   $lastRow.="<a href ='javascript:void(0)' class='icon-1 info-tooltip' title='Edit' id='edit_".$id."' onclick='javascript : editRecord(".$id.");'></a>";
   $lastRow.="<a href ='javascript:void(0)' class='info-tooltip icon-2 delete' title='Delete' id='del_".$id."' onclick='if(is_delete()){deleteRecord(".$id.");}'></a>";


   if ($last_res['verified'] == 0) {
    $lastRow.="<a href ='javascript:void(0)' class='verifyimg' title='verify' id='verify_".$id."' onclick='if(is_verify()){verifyRecord($id)};'></a>";
    $lastRow.="<a style='display:none' href ='javascript:void(0)' class='unverifyimg' title='unverify' id='unverify_".$id."' onclick='if(is_unverify()){UnverifyRecord($id)};' ></a>";

} else if ($last_res['verified'] == 1) {
    $lastRow.="<a href ='javascript:void(0)' class='unverifyimg' title='unverify' id='verify_".$id."' onclick='if(is_unverify()){UnverifyRecord($id)};'></a>";
    $lastRow.="<a  style='display:none' href ='javascript:void(0)' class='verifyimg' title='verify' id='unverify_".$id."' onclick='if(is_verify()){verifyRecord($id)};'></a>";
}

$lastRow.="</td>
</tr>";*/
if ($res) {

     echo json_encode(['status' => 'success','milestonedata' => $last_res]); 
      die;
} else {
    echo 0;
    exit;
}
}else {
    $error = array();
    $error['error'] = $this->Milestones->invalidFields();
                //  echo "<pre>";print_r($error);exit;
    $this->set("Error", $this->Milestones->invalidFields());
    $this->render('admin_list');
}

}


    /**
     * @Date: 13-sept-2016    
     * @Method : edit    
     * @Purpose: This function is to records of milestone.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
    function edit($id = null) {
        if (!empty($id)) {
          $id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id)); 
        }
        if($this->request->is('ajax')==true){

           $this->request->data['currency']  =  $this->request->data['currency'];
           $this->request->data['amount']   = $this->request->data['amount'];
           $this->request->data['id'] = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])); 
           $this->request->data['notes']  = $this->request->data['notes'];
           $this->request->data['user_id']   = $this->request->data['user_id'];
           $this->request->data['milestonedate'] = date("Y-m-d", strtotime($this->request->data['milestonedate']));
         //  pr($this->request->data); die;
           $update = (['amount' => $this->request->data['amount'],
            'currency' => $this->request->data['currency'],
            'notes' => $this->request->data['notes'],
            'user_id' => $this->request->data['user_id'],
            'milestonedate' => $this->request->data['milestonedate'],
            ]);
           // pr($this->request->data); die;
           $result =  $this->Milestones->updateAll($update,['id' => $this->request->data['id']]);
         //  if ($this->Milestones->updateAll($update,['id' => $this->request->data['id']]) ) {

               $verf = $this->Milestones->find('all', [
                'conditions' => ['id' => $this->request->data['id']    ] ])->first();
               //pr($verf); die;
               $payment_id = $this->Payments->find('all', [
                'fields' => [
                'id'
                ],
                'conditions' => [ 'project_id' => $verf['project_id']]
                ])->first();
              // pr($payment_id); die;
               $payment_id=isset($payment_id)?$payment_id->toArray():array();
             
               if(!empty($payment_id)){
               // pr($payment_id); die;
                $data =[];
                 $data['project_id'] = $verf['project_id'];
                 $data['responsible_id'] = $verf['user_id'];
                 $session = $this->request->session();
                 $sessionAry = $session->read("SESSION_ADMIN");
                 $data['entry_id'] = $sessionAry['0'];
                 $data['billing_id'] = $verf['id'];
                 $data['payment_date'] = date('Y-m-d');
                 $data['amount'] = $verf['amount'];
                 $data['tds_amount'] = 0;
                 $data['currency'] = $verf['currency'];
                 $data['note'] = "Received on Upwork";
                 $data['created'] = date('Y-m-d');
                 $data['modified'] = date('Y-m-d');
                 $data['id'] = $payment_id['id'];

                 $payment_change = $this->Payments->updateAll($data,['id' => $payment_id['id'] ]);

                 //if ($payment_change) {
                
                /*}else{
                   echo "0";
               }*/
              // exit;
           }
               $responsble = $this->Users->find('all',[
                    'conditions' => ['id' =>  $this->request->data['user_id'] ],
                    'fields' => ['first_name','last_name']
                    ])->first();
                   echo json_encode(['status' => 'success','responsble' => $responsble , 'currency' => $this->request->data['currency']]); 
              die;
        }
   
}


  /**
     * @Date: 13-sept-2016   
     * @Method : delete    
     * @Purpose: This function is to delete from milestones table.   
     * @Param: none 
     * @Return: none  
     * @Return: none     
     * */
  function delete() {

    if($this->request->is('ajax')==true){
      $this->request->data['id'] = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])); 
        if (isset($this->request->data['key']) && $this->request->data['key'] == 'delete') {
            $res = $this->Milestones->deleteAll("id IN ('" . $this->request->data['id'] . "')");

            if ($res){
              echo json_encode(['status' => 'success']); 
              die;
            }
               echo json_encode(['status' => 'failed']); 
              die; 
        }
    }
}

    /**     
     * @Date: 13-sept-2016
     * @Method : verifymilestone  
     * @Purpose: This function is to verify last billings.  
     * @Param: $id  
     * @Return: none    
     * @Return: none 
     * */

   
    function verifymilestone() {
        if($this->request->is('ajax')==true){
            $this->request->data['id'] = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])); 
            if (isset($this->request->data['key']) && $this->request->data['key'] == 'verify') {
                $milestone_rec = $this->Milestones->find('all', [
                    'conditions' =>[ 'id' => $this->request->data['id'] ] ])->first()->toArray();
                $data =[];
                $data['project_id'] = $milestone_rec['project_id'];
                $data['responsible_id'] = $milestone_rec['user_id'];
                $session = $this->request->session();
                $sessionAry = $session->read("SESSION_ADMIN");
                $data['entry_id'] = $sessionAry['0'];
                $data['entry_id'] = $sessionAry['0'];
                $data['billing_id'] = $milestone_rec['id'];
                $data['payment_date'] = date('Y-m-d');
                $data['amount'] = $milestone_rec['amount'];
                $data['tds_amount'] = 0;
                $data['currency'] = $milestone_rec['currency'];
                $data['note'] = "Received on Upwork";
                $data['created'] = date('Y-m-d');
                $data['modified'] = date('Y-m-d');
                 $Payments  = $this->Payments->newEntity($data); 
        if($this->Payments->save($Payments)){
                $res = $this->Milestones->updateAll( ['verified' =>'1'], ['id' => $milestone_rec['id'] ]);
                //if ($res) {
                    echo json_encode(['status' => 'success', 'key' =>  'unverify' ]); 
                      die;
                } else {
                    echo 0;
                    exit;
                }
            } else if (isset($this->request->data['key']) && $this->request->data['key'] == 'unverify') {

            $milestone_rec = $this->Milestones->find('all', [
                'conditions' => ['id' => $this->request->data['id'] ] ])->first();
           // pr($milestone_rec); die;
            $data =[];
            $data['project_id'] = $milestone_rec['project_id'];
            $data['responsible_id'] = $milestone_rec['user_id'];
            $data['billing_id'] = $milestone_rec['id'];
            $data['amount'] = $milestone_rec['amount'];
            $data['currency'] = $milestone_rec['currency'];
             $save_mile = $this->Payments->find('all', [
              'conditions' => $data ])->toArray();
             if(count($save_mile) > 0 ){
                foreach($save_mile as $pament){
                  $entity = $this->Payments->get($pament->id);
                  $result = $this->Payments->delete($entity);
                } 
             }
            $res = $this->Milestones->updateAll(['verified' => '0'], ['id' => $milestone_rec['id'] ]);
                echo json_encode(['status' => 'success', 'key' =>  'verify' ]); 
                exit;
         }
        }
    }

    /**     
     * @Date: 13-sept-2016   
     * @Method : unverifymilestone  
     * @Purpose: This function is to unverifymilestone last billings.  
     * @Param: $id  
     * @Return: none    
     * @Return: none 
     * */

    function unverifymilestone() {

     if($this->request->is('ajax')==true){
        if (isset($this->request->data['key']) && $this->request->data['key'] == 'unverify') {

            $milestone_rec = $this->Milestones->find('all', [
                'conditions' => ['id' => $this->request->data['id'] ] ])->first()->toArray();
           // pr($milestone_rec); die;
            $data =[];
            $data['project_id'] = $milestone_rec['project_id'];
            $data['responsible_id'] = $milestone_rec['user_id'];
            //$session = $this->request->session();
            //$sessionAry = $session->read("SESSION_ADMIN");
            //$data['entry_id'] = $sessionAry['0'];
            $data['billing_id'] = $milestone_rec['id'];
            //$data['payment_date'] = date('Y-m-d');
            $data['amount'] = $milestone_rec['amount'];
            //$data['tds_amount'] = 0;
            $data['currency'] = $milestone_rec['currency'];
            //$data['note'] = "Received on Upwork";
            //$data['created'] = date('Y-m-d');
            //$data['modified'] = date('Y-m-d');
            //$save_mile = $this->Payments->deleteAll($data);
             $save_mile = $this->Payments->find('all', [
              'conditions' => $data ])->toArray();
             //pr($save_mile); die;
             if(count($save_mile) > 0 ){
                foreach($save_mile as $pament){
                  $entity = $this->Payments->get($pament->id);
                  $result = $this->Payments->delete($entity);
                } 
             }
            $res = $this->Milestones->updateAll(['verified' => '0'], ['id' => $milestone_rec['id'] ]);

            if ($res) {
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }
}
    /**     
     * @Date: 13-sept-2016 
     * @Method : fixedcontract  
     * @Purpose: This function is to fixedcontract  billings.  
     * @Param: $id  
     * @Return: none    
     * @Return: none 
     * */

    function fixedcontract() {
       $this->viewBuilder()->layout(false);
       $session = $this->request->session();
      $this->set('session',$session);  
      $userSession = $session->read("SESSION_ADMIN");
       if(isset($this->request->data['start_date'])){ 
        $start_date = $this->request->data['start_date'];
        $date = strtotime($start_date);
        $date = strtotime("next sunday", $date);
        $end_date=date('Y-m-d',$date);
        $todate = str_replace("/","-",$this->request->data['start_date']);
        $create=date_create($todate);
        $from = $create->format('Y-m-d');
        $create=date_create($end_date);
        $to = $create->format('Y-m-d');
    } else {
        $from = date('Y-m-d', time() + ( 1 - date('w')) * 24 * 3600);
        if (date('N') == 7) {
            $from = date('Y-m-d', strtotime("last monday"));
        }
        $curr_sun = strtotime($from);
        $curr_sun = strtotime("next sunday", $curr_sun);
        $to = date('Y-m-d', $curr_sun);
    }

    $cond = ['Milestones.milestonedate >=' => $from, 'Milestones.milestonedate <=' => $to ,'Projects.engagagment !=' => 'hourly' ,'Milestones.agency_id' => $userSession[3] ];
    $this->set('fromDate',$from);

    $fixed_res =   $this->Milestones->find()
    ->select(['Milestones.id', 'Milestones.currency', 'Milestones.amount',
        'Milestones.notes', 'Milestones.verified', 'Milestones.milestonedate',
        'Milestones.user_id', 'Milestones.project_id', 'Projects.project_name', 'Projects.platform',
        'Projects.technology'
        ])
    ->where($cond)
    ->order(['Projects.modified' => 'desc'])

    ->join([
        'table' => 'projects',
        'alias' => 'Projects',
        'type' => 'INNER',
        'foreignKey' => false,
        'conditions' => array('Projects.id = Milestones.project_id')
        ])->toArray();
    $this->set('resultData', $fixed_res);
}


    /**     
     * @Date: 13-sept-2016
     * @Method : verifyallmilestone   
     * @Purpose: This function is to verify all unverified milestones.  
     * @Param: $id  
     * @Return: none    
     * @Return: none 
     * */

    function  verifyallmilestone() {
         $session = $this->request->session();
      $this->set('session',$session);  
      $userSession = $session->read("SESSION_ADMIN");

        $this->viewBuilder()->layout(false);
        $unverify_mile =   $this->Milestones->find()
        ->select(['Milestones.id', 'Milestones.currency', 'Milestones.amount',
            'Milestones.notes', 'Milestones.verified', 'Milestones.milestonedate',
            'Milestones.user_id', 'Milestones.project_id', 'Projects.project_name', 'Projects.platform',
            'Projects.technology'
            ])
        ->where(['Milestones.verified' => 0,'Milestones.agency_id' => $userSession[3]])
        ->order(['Projects.modified' => 'desc'])

        ->join([
            'table' => 'projects',
            'alias' => 'Projects',
            'type' => 'INNER',
            'foreignKey' => false,
            'conditions' => array('Projects.id = Milestones.project_id')
            ])->toArray();

        $this->set('resultData', $unverify_mile);
        if($this->request->is('ajax')==true){

         if(isset($this->request->data['key']) && $this->request->data['key'] == 'verify'){           

            $milestone_rec = $this->Milestones->find('all',[

                'conditions' => ['id' => $this->request->data['id'] ] ])->first()->toArray();
            
            $this->request->data['project_id'] = $milestone_rec['project_id'];
            $this->request->data['responsible_id'] = $milestone_rec['user_id'];
            $session = $this->request->session();
            $sessionAry = $session->read("SESSION_ADMIN");
            $this->request->data['entry_id']       = $sessionAry['0'];
            $this->request->data['billing_id']  = $milestone_rec['id'];
            $this->request->data['payment_date']   = date('Y-m-d');
            $this->request->data['amount']  = $milestone_rec['amount'];
            $this->request->data['tds_amount']   = 0;
            $this->request->data['currency']  = $milestone_rec['currency'];
            $this->request->data['note']  = "Received on Upwork";
            $this->request->data['created']  = date('Y-m-d');
            $this->request->data['modified']  = date('Y-m-d');
            $this->request->data['agency_id'] = $userSession[3];
            $data = $this->Milestones->newEntity($this->request->data);
            $save_mile = $this->Payments->save($data);
            $res = $this->Milestones->updateAll(array('verified' =>  '1'), array('id' => $milestone_rec['id']));
            if ($res) {
                echo 1;
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }
}

}
