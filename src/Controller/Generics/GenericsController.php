<?php


namespace App\Controller\Generics;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Validation\Validator;
class GenericsController extends AppController {



    /*     * ************* START FUNCTIONS ************************* */



    #_________________________________________________________________________#

    /**
     * @Date: 13-sept-2016
     * @Method : beforeFilter
     * @Purpose: This function is called before any other function.
     * @Param: none
     * @Return: none 
     * */
    function beforeFilter(Event $event) {
     parent::beforeFilter($event);
     $this->viewBuilder()->layout('layout_admin');
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common', $this->Common);
}
 /**
     * @Date: 13-sept-2016
     * @Method : initialize
     * @Purpose: This function is the default function of the controller    
     * @Param: none
     * @Return: none 
     * */
 public function initialize()
 {
     parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Appraisals');
        $this->loadModel('Generics');
        $this->loadModel('Settings');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
    }
    
/**
     * @Date: 13-sept-2016
     * @Method : index
     * @Purpose: This is the default function of the administrator section for users
     * @Param: none
     * @Return: none 
     * */
function index() {
    $this->set("title_for_layout", "ERP : Vlogic Labs - Relying on Commitment");
}


/**
     * @Date: 13-sept-2016
     * @Method : tagcloud
     * @Purpose: This function is the tagcloud function of the controller    
     * @Param: none
     * @Return: none 
     * */

function tagcloud(){
  $this->set('title_for_layout','Tag Cloud');		
  $this->pageTitle = "Tag Cloud";	
  $getData  =  $this->Settings->find()
  ->select(['Settings.value'])
  ->where([
    'OR' => [['id' => 1], ['id' => 2], ['id' => 7], ['id' => 8], ['id' => 9], ['id' => 10], ['id' => 11]] 
    ])->all()->toArray();

  $tagAry = array();
  $tagAry['tag1'] = $getData[2]['value'];
  $tagAry['tag2'] = $getData[3]['value']; 
  $tagAry['tag3'] = $getData[4]['value']; 
  $tagAry['tag4'] = $getData[5]['value']; 
  $tagAry['tag5'] = $getData[6]['value'];

  $this->set('tagAry',$tagAry);
}

/**
     * @Date: 13-sept-2016
     * @Method : adminsetting
     * @Purpose: These are sales settings.
     * @Param: none
     * @Return: none 
     * @who:Priyanka Sharma
     * */

function adminsetting(){
$this->viewBuilder()->layout('new_layout_admin');
   $this->set('title_for_layout','Settings');		
   $this->pageTitle = "Settings";		
   $credentials = TableRegistry::get('Settings');
   $getSettings = $credentials->find()->where(['id IN' => [7,8,9,10,11] ]);
   $result = array();
   foreach($getSettings as $setting){
    if($setting['id'] == 7) $result['tag1']   = $setting['value'];	
    if($setting['id'] == 8) $result['tag2']   = $setting['value'];
    if($setting['id'] == 9) $result['tag3']   = $setting['value'];
    if($setting['id'] == 10) $result['tag4']   = $setting['value'];		
    if($setting['id'] == 11) $result['tag5']   = $setting['value'];				
}

$this->set("resultData",$result);
if($this->request->data){ 

   $tag1 = $this->request->data['tag1'];
   $tag2 = $this->request->data['tag2'];
   $tag3 = $this->request->data['tag3'];
   $tag4 = $this->request->data['tag4'];
   $tag5 = $this->request->data['tag5'];
   if(!empty($tag1)){
    $result = $this->Settings->updateAll(array(
        'value' =>$tag1),
    array('id ' => 7));

} 
if(!empty($tag2)){
    $result = $this->Settings->updateAll(array(
        'value' =>$tag2),
    array('id ' => 8));

} 
if(!empty($tag3)){
    $result = $this->Settings->updateAll(array(
        'value' =>$tag3),
    array('id ' => 9));

} 
if(!empty($tag4)){
    $result = $this->Settings->updateAll(array(
        'value' =>$tag4),
    array('id ' => 10));

} 
if(!empty($tag5)){
    $result = $this->Settings->updateAll(array(
        'value' =>$tag5),
    array('id ' =>11 ));

} 

if($result){
    $message="Settings has been updated successfully";
    $this->Flash->success_new($message,'layout_success');
}
}

}
}


