<?php

namespace App\Controller\Credentials;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Routing\Router;

class CredentialsController extends AppController {

    var $name = "Credentials";    // Controller Name        

    
    var $paginate = array(); //pagination


    /*     * ***************************** START FUNCTIONS ************************* */


    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016                                          
     * @Method : beforeFilter
     * @Purpose: This function is called before any other function.
     * @Param: none                                                
     * @Return: none                                               
     * */
    function beforeFilter(Event $event) { 
    // This  function is called first before parsing this controller file
        // star to check permission
      $session = $this->request->session();
      $userSession = $session->read("SESSION_ADMIN");
      $controller=$this->request->params['controller'];
      $action=$this->request->params['action'];
      $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
      if($permission!=1){
        $this->Flash->error("You have no permisson to access this Page.");

        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
      }
    // end code to check permissions
      if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

        $this->checkUserSession();
      } else {
       $this->viewBuilder()->layout('new_layout_admin');

     }
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
      return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common', $this->Common);
  }

/**
     * @Date: Nov,2016                                          
     * @Method : initialize
     * @Purpose: This function is called initialize  function.
     * @Param: none                                                
     * @Return: none                                               
     * */
public function initialize()
{
  parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
         $this->loadModel('Employees');
        $this->loadModel('Credentials');
        $this->loadModel('CredentialLogs');
        $this->loadModel('CredentialAudits');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

      }



    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016        
     * @Method : index           
     * @Purpose: This function is the default function of the controller
     * @Param: none                                                     
     * @Return: none                                                    
     * */
    function index() {

      $this->render('login');

      if ($this->Session->read("SESSION_USER") != "") {
        $this->redirect('dashboard');
      }
    }

    #_________________________________________________________________________#



 /**
     * @Date: Nov,2016       
     * @Method : credentialslist      
     * @Purpose: This function is to show list of Credentials in the System
     * @Who:Priyanka Sharma                                                        
    * */


 function credentialslist(){   
$this->viewBuilder()->layout('new_layout_admin');
  $session = $this->request->session();
  $this->set('session',$session);  

  $this->set('title_for_layout','Credential Listing');      
  $this->set("pageTitle","Credential Listing");     
  $this->set("search1", "");                           
  $this->set("search2", "");     
  $userSession = $session->read("SESSION_ADMIN");
  $options  =  $this->CredentialAudits->find("all")->toArray(); 
  $this->set("options",$options);  
    $criteria = "1"; //All Searching    
    $session->delete('SESSION_SEARCH');   

    if(isset($this->request->data['Credential']) || !empty($this->request->query)) {  
      if(!empty($this->request->data['Credential']['fieldName']) || isset($this->request->query['field'])){        

        if(trim(isset($this->request->data['Credential']['fieldName'])) != ""){      
          $search1 = trim($this->request->data['Credential']['fieldName']);    
        }elseif(isset($this->request->query['field'])){   
          $search1 = trim($this->request->query['field']);    
        }
        $this->set("search1",$search1);      
      }   


      if(isset($this->request->data['Credential']['value1']) || isset($this->request->data['Credential']['value2']) || isset($this->request->query['value'])){  

        if(isset($this->request->data['Credential']['value1']) || isset($this->request->data['Credential']['value2'])){ 

          $search2 = ($this->request->data['Credential']['fieldName'] != "Credential.status")?trim($this->request->data['Credential']['value1']):$this->request->data['Credential']['value2']; 
        }elseif(isset($this->request->query['value'])){ 
          $search2 = trim($this->request->query['value']);  
        }   
        $this->set("search2",$search2);
      }   

      /* Searching starts from here */    
      if(!empty($search1) && (!empty($search2) || $search1 == "Users.status")){ 


        $query  =  $this->Credentials->find('all' , [
          'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'Credentials.agency_id' => $userSession[3]],
           'order' => ['Credentials.modified' => 'DESC']
          ])->contain(['Users']);

        $result = isset($query)? $query->toArray():array();


        $criteria = $search1." LIKE '%".$search2."%'";     
        $session->write('SESSION_SEARCH', $criteria);
      }else{  
       $query  =  $this->Credentials->find("all",[
          'conditions' => ['Credentials.agency_id' => $userSession[3]],
         'order' => ['Credentials.modified' => 'DESC']
      ])->contain(['Users']);   

       $this->set("search1","");      
       $this->set("search2","");      
     }

   }else{  
     $query  =  $this->Credentials->find("all",[
         'conditions' => ['Credentials.agency_id' => $userSession[3]],
         'order' => ['Credentials.modified' => 'DESC']
      ])->contain(['Users']);    

   }
   
   $urlString = "/";
   if(isset($this->request->query)){                   
     $completeUrl  = array();

     if(!empty($this->request->query['page']))
      $completeUrl['page'] = $this->request->query['page'];

    if(!empty($this->request->query['sort']))  
     $completeUrl['sort'] = $this->request->query['sort']; 

   if (!empty($this->request->query['direction']))        
    $completeUrl['direction'] = $this->request->query['direction'];   

  if(isset($search2))                                         
    $completeUrl['value'] = $search2;

  foreach($completeUrl as $key => $value) {                      
    $urlString.= $key.":".$value."/";                           
  }
}
$this->set('urlString', $urlString); 


$urlString = "/";  

if(isset($this->request->query)){
  $completeUrl  = array();  
  if(!empty($setValue)){                            
    if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
    }
  }
}

$this->set('urlString', $urlString);   

 /*$this->paginate=[
  'page' => 1, 'limit' => 50,
  'order' => ['Credentials.modified' => 'DESC'],
  'paramType' => 'querystring'
  ];
   $data = $this->paginate('Credentials',['conditions' => $query]); */
   $data = $this->paginate($query,[
    'page' => 1, 'limit' => 100,
    'order' => ['id' => 'desc'],
    'paramType' => 'querystring'
    ]);
    $arr=[];


foreach($data as $d){      
  $d['id'] = $d->id; 
    $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));   
   $arr[] = $d; 
  }

$this->set('resultData', $arr);
$this->set('pagename','credlist');


}
    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016       
     * @Method : sendemail       
     * @Purpose: This function is to send credentials to users
     * @Param: $id                                       
     * @Return: none
     * @Return: none
     * */
    function sendemail($id = null) {
        // setting layout to false as we have to open the same in fancy box
      $this->viewBuilder()->layout(false);

      if (!empty($id) || !empty($this->request->data['Credential']['id'])) {

        $id = (!empty($this->request->data['Credential']['id']) ? $this->request->data['Credential']['id'] : $id);

        $crData = $this->Credentials->find('all',['conditions' => ['Credentials.id' => $id] ])->contain(['Users'])->first()->toArray();
      
       // pr($crData);die;
        if ($crData) {
                $this->set('crData', $crData); //Setting Credential Data                             
              }
            }

            if($this->request->data){

             $credentialss = $this->Credentials->newEntity($this->request->data());
             $crData = $this->Credentials->find('all',['conditions' => ['id' => $this->request->data['id'] ] ])->first();

             if(empty($credentialss->errors())){
              $result = explode(",", $this->request->data['email_address']);
              $subject = $crData['type'] . " Credential";
              foreach ($result as $value) {
                $message = "Dear User,<br/><br/>Please find below credentials.<br/>Username: " . $crData['username'] . "<br/>Password: " . $crData['password'] . "<br/>Message: " . $this->request->data['message'] . "<br/>Thanks, <br/>" . SITE_NAME . "<br/>" ;
                $email = new Email();
                $email->from(ADMIN_EMAIL)
                ->to($value)
                ->replyTo(ADMIN_EMAIL)
                ->subject($subject)
                ->send($message);
                if ($email->send($message)) {
                 $this->Flash->success_new("Credential has been sent successfully.");
               } else {
                 $this->Flash->error("Sorry, some error occured.");
               }
             }
          return $this->redirect(['action' => 'sendemail',$this->request->data['id']]);
           }
         }
       }

    #_______________________________________________________________________________________________________________________#
    /**
     * @Date: Nov,2016    
     * @Method : add    
     * @Purpose: This function is to add credentials from admin section.   
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

    function add() {  
      $this->viewBuilder()->layout('new_layout_admin');     
      $this->set('title_for_layout', 'Add Credential');
      $this->pageTitle = "Add Credential";
      $this->set("pageTitle", "Add Credential");
      $mode = "add";
      $session = $this->request->session();
      $this->set('session',$session);  
       $userSession = $session->read("SESSION_ADMIN");
      if($this->request->data){
         $alloteduserid=(isset($this->request->data['other_alloted_user']) && !empty($this->request->data['other_alloted_user'])) ? implode(",",$this->request->data['other_alloted_user']): "";
         $this->request->data['other_alloted_user'] = $alloteduserid;
         $this->request->data['agency_id'] = $userSession[3];
       $credentialss = $this->Credentials->newEntity($this->request->data());
       if(empty($credentialss->errors())){
        $this->Credentials->save($credentialss);
        $this->Flash->success_new("Credentials has been created successfully.");
        return $this->redirect( ['action' => 'credentialslist']);
      }else{
        $this->set("errors", $credentialss->errors());
      }
    }     
  }


    /**
     * @Date: 23-sep-2016   
     * @Method : edit    
     * @Purpose: This function is to edit credentials from admin section.
     * @Param: none 
     * @author: Priyanka Sharma  
     * @Return: none     
     * */
    function edit($id = null) { 
      $this->viewBuilder()->layout('new_layout_admin');
     $this->set('title_for_layout', 'Edit Credential');
     $this->pageTitle = "Edit Credential";
     $this->set("pageTitle", "Edit Credential");
     $mode = "edit"; 
     if($this->request->data){ 
  $this->request->data['status'] = (!empty($this->request->data['user_id'])) ?  1 :  0;
  $alloteduserid=(isset($this->request->data['other_alloted_user']) && !empty($this->request->data['other_alloted_user'])) ? implode(",",$this->request->data['other_alloted_user']): "";
$this->request->data['other_alloted_user'] = $alloteduserid;
//pr($this->request->data); die;
    $skill = $this->Credentials->get($this->request->data['id']);
    $skills = $this->Credentials->patchEntity($skill,$this->request->data());
      if(empty($skills->errors())) {
        $this->Credentials->save($skill);
        $this->Flash->success_new("Credentials has been updated successfully.");
        $this->redirect(array('action' => 'credentialslist'));
      }else{                                    
       $this->set("errors", $skills->errors());
     }   
   }else if(!empty($id)){ 
    $credential = $this->Credentials->get($id);

    $this->set('credential',$credential); 
    if(!$credential){             
      $this->redirect(array('action' => 'credentialslist'));                 
    }                                                      
  }
  $this->set("id", $id);                                                              
}    


    #_________________________________________________________________________# 
    /**
     * @Date: 23-sep-2016          
     * @Method : changeStatus    
     * @Purpose: This function is used to activate/deactivate Portfolios.
     * @Param: $id                                       
     * @Return: none
     * @Return: none
     * */

    function changeStatus() {
 
     if($this->request->is('ajax')) {
       $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $updateCredentials = 1;
      $setValue = 0;
      $messageStr = 'deactivate';
      }
      if ($updateCredentials == 1) {
        $credentials = TableRegistry::get('Credentials');
        $update = ['user_id' => 0 , 'status' => 0];
        $credentials->updateAll($update,['user_id IN' => $id]);
      }
     $users = TableRegistry::get('Credentials');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;

      }
     $this->setAction('credentialslist'); 
   }



















  #_______________________________________________________________________________________________________________________#



/**
     * @Date: 26-sep-2016   
     * @Method : delete   
     * @Purpose: This function is to delete credentials from admin section.   
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
      $employee = $this->Credentials->get($id);
      if ($this->Credentials->delete($employee)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }

    #_______________________________________________________________________________________________________________________#
    /**
     * @Date: 26-sep-2016
     * @method      : download
     * @description : Used to download credentials record
     * @param       : type
     * return       : none
     * */
    function download() {
     if (empty($this->request->query['pass'])) {

      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");
      $crData = $this->Credentials->find('all', [
        'conditions' =>[$search[0] ]
        ]);
    } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => $this->request->query['pass'][0] ], 'order' => 'username']); //fetching all related data                                           
          }
        $this->set('crData', $crData->toArray()); //Setting Credential Data                             
        $this->viewBuilder()->options([
          'pdfConfig' => [
          'orientation' => 'portrait',
          'filename' => 'Invoice_' ,
          ]
          ]);

        $this->render();
      }

/**
     * @Date: 26-sep-2016   
     * @Method : cyberoam   
     * @Purpose: Used to download cyberoam record
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

public function cyberoam(){
 if (empty($this->request->query['pass'])) {

  $session = $this->request->session();
  $this->set('session',$session);
  $search = $session->read("SESSION_ADMIN");
  $crData = $this->Credentials->find('all', [
    'conditions' =>['type' =>'gmail']
    ]);

} else {
  $crData = $this->Credentials->find('all', [
            'conditions' => ['type' =>'gmail'], 'order' => 'username']); //fetching all related data   


}
        $this->set('crData', $crData->toArray()); //Setting Credential Data                             
        $this->viewBuilder()->options([
          'pdfConfig' => [
          'orientation' => 'portrait',
          'filename' => 'Invoice_' ,
          ]
          ]);

        $this->render();

      }
  /**
     * @Date: 26-sep-2016   
     * @Method : dropbox   
     * @Purpose: This function is to dropbox credentials .   
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

  public function dropbox()
  {
   if (empty($this->request->query['pass'])) {

    $session = $this->request->session();
    $this->set('session',$session);
    $search = $session->read("SESSION_ADMIN");
    $crData = $this->Credentials->find('all', [
      'conditions' =>['type' =>'dropbox']
      ]);

  } else {
    $crData = $this->Credentials->find('all', [
            'conditions' => ['type' =>'gmail'], 'order' => 'username']); //fetching all related data   
  }
        $this->set('crData', $crData->toArray()); //Setting Credential Data                             
        $this->viewBuilder()->options([
          'pdfConfig' => [
          'orientation' => 'portrait',
          'filename' => 'Invoice_' ,
          ]
          ]);

        $this->render();

      }
  /**
     * @Date: 26-sep-2016   
     * @Method : skype   
     * @Purpose: This function is to dropbox credentials .      
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

  public function skype()
  {
   if (empty($this->request->query['pass'])) {

    $session = $this->request->session();
    $this->set('session',$session);
    $search = $session->read("SESSION_ADMIN");
    $crData = $this->Credentials->find('all', [
      'conditions' =>['type' =>'skype']
      ]);

  } else {
    $crData = $this->Credentials->find('all', [
            'conditions' => ['type' =>'gmail'], 'order' => 'username']); //fetching all related data   
  }
        $this->set('crData', $crData->toArray()); //Setting Credential Data                             
        $this->viewBuilder()->options([
          'pdfConfig' => [
          'orientation' => 'portrait',
          'filename' => 'Invoice_' ,
          ]
          ]);

        $this->render();

      }
  /**
     * @Date: 26-sep-2016   
     * @Method : webmail   
     * @Purpose: This function is to webmail credentials .      
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

  public function webmail()
  {

   if (empty($this->request->query['pass'])) {

    $session = $this->request->session();
    $this->set('session',$session);
    $search = $session->read("SESSION_ADMIN");
    $crData = $this->Credentials->find('all', [
      'conditions' =>['type' =>'webmail']
      ]);

  } else {
    $crData = $this->Credentials->find('all', [
            'conditions' => ['type' =>'gmail'], 'order' => 'username']); //fetching all related data   
  }
        $this->set('crData', $crData->toArray()); //Setting Credential Data                             
        $this->viewBuilder()->options([
          'pdfConfig' => [
          'orientation' => 'portrait',
          'filename' => 'Invoice_' ,
          ]
          ]);

        $this->render();

      }

// end of download function
    #________________________________________________________________________________________________________________________________#

    /**
     * @Date: 26-sep-2016 
     * @Method : exportci
     * @Purpose: This function is to exportci credentials .   
     * @Param: none
     * @Return: none
     * */
    function exportci() {
      $this->viewBuilder()->layout(false);
      if (empty($this->params['pass'][0])) {


        $session = $this->request->session();
        $this->set('session',$session);
        $search = $session->read("SESSION_ADMIN");

        $crData = $this->Credentials->find('all', [
          'conditions' =>[$search[0] ]
          ]);          
      } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => $this->request->query['pass'][0] ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                                          

          }

        //setting excel parametres
          $this->set('filename', "CredentialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('export_xls');

      }
   /**
     * @Date: 26-sep-2016   
     * @Method : exportcigmail   
     * @Purpose: This function is to exportcigmail credentials .   
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

   function exportcigmail() {
    $this->viewBuilder()->layout(false);
    if (empty($this->params['pass'][0])) {


      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");
      $crData = $this->Credentials->find('all', [
        'conditions' =>['type' =>'gmail']
        ]);

    } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => 'gmail' ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                                          

          }

        //setting excel parametres
          $this->set('filename', "CredentialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('export_xls');

      }
   /**
     * @Date: 26-sep-2016   
     * @Method : exportcidropbox   
     * @Purpose: This function is to exportcidropbox credentials .      
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

   function exportcidropbox() {
    $this->viewBuilder()->layout(false);
    if (empty($this->params['pass'][0])) {


      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");
      $crData = $this->Credentials->find('all', [
        'conditions' =>['type' =>'dropbox']
        ]);

    } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => 'dropbox' ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                                          

          }

        //setting excel parametres
          $this->set('filename', "CredentialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('export_xls');

      }
   /**
     * @Date: 26-sep-2016   
     * @Method : exportciskype   
     * @Purpose: This function is to exportciskype credentials .      
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

   function exportciskype() {
    $this->viewBuilder()->layout(false);
    if (empty($this->params['pass'][0])) {

      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");
      $crData = $this->Credentials->find('all', [
        'conditions' =>['type' =>'skype']
        ]);

    } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => 'skype' ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                                          

          }

        //setting excel parametres
          $this->set('filename', "CredentialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('export_xls');

      }
   /**
     * @Date: 26-sep-2016   
     * @Method : exportciwebmail   
     * @Purpose: This function is to exportciwebmail credentials .      
     * @Param: none 
     * @author: Priyanka Sharma 
     * @Return: none     
     * */

   function exportciwebmail() {
    $this->viewBuilder()->layout(false);
    if (empty($this->params['pass'][0])) {

      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");
      $crData = $this->Credentials->find('all', [
        'conditions' =>['type' =>'webmail']
        ]);

    } else {
            $crData = $this->Credential->find('all', ['conditions' => ['type' => 'webmail' ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                                          

          }

        //setting excel parametres
          $this->set('filename', "CredentialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('export_xls');

      }

 #________________________________________________________________________________________________________________________________#

    /**
     * @Date: 26-sep-2016   
     * @Method : freecredits
     * @Purpose: This function is to freecredits credentials .   
     * @Param: none
     * @Return: none
     * */
    function freecredits($key = null) {
     $session = $this->request->session();
     $this->set('session',$session);
     $user = $session->read("SESSION_ADMIN");

      $this->viewBuilder()->layout(false);
      if ($this->request->is('ajax')) {
        if (!empty($this->params['form']['old_type']) && !empty($this->params['form']['old_username'])) {
          echo $result = $this->Credential->updateAll(array('status' => 0, 'user_id' => 0), array('Credential.username' => Sanitize::escape($this->params['form']['old_username']), 'Credential.type' => $this->params['form']['old_type']));
        }

        die;
      }
      if (isset($key) && !empty($key)) {
        $resultData = $this->Credentials->find('all', array('conditions' => array('type' => $key, 'status' => 0,'agency_id' => $user[3])))->all()->toArray();
      }


      $this->set('resultData', $resultData);
      $this->set('key', $key);

    }

    #_________________________________________________________________________#
    /**   
     * @Date: 26-sep-2016   
     * @Method : allotedto
     * @Purpose: This function is to allotedto credentials .   
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */

    function allotedto($id = null) {
      $id = $this->Common->mc_decrypt($id, ENCRYPTION_KEY); 
      $this->viewBuilder()->layout(false);
      $allotedRes = $this->Credentials->find("all",[
        'conditions' => ['Credentials.id' => $id] ])->contain(['Users'])->first();
      //print_r($allotedRes); die;
      $allotedUserid = explode(',', $allotedRes['other_alloted_user']);
      //print_r($allotedUserid); die;
      $cond = ['Users.id IN' => $allotedUserid];
      $userdata = $this->Users->find('all',[
            'conditions' => $cond
        ])->toArray();
      $clogData = $this->CredentialLogs->find('all',[
        'conditions' => ['CredentialLogs.credential_id' => $id,'CredentialLogs.type' => 'credential'] ])->contain(['Users','Credentials'])->toArray();
      $this->set('clogs', $clogData);
      $this->set('allotedData', $userdata);
      $this->set('cre_data', $allotedRes);
    }
  #_________________________________________________________________________#
    /**   
     * @Date: 26-sep-2016     
     * @Method : credentialsauditreport
     * @Purpose: This function is to show allotted credentials to the users.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */
    function credentialsauditreport() {
     $this->viewBuilder()->layout(false);
     $session = $this->request->session();
     $this->set('session',$session);
     $user = $session->read("SESSION_ADMIN");
     $CredentialAudit = $this->CredentialAudits;  
     $this->set('options23', $CredentialAudit->find('all',array('fields'=>array('profile_id','auditpoint','status','modified')))->toArray());
     $res = $this->Credentials->find('all');
     $this->set('runningprofile', $res);
   }

 }
