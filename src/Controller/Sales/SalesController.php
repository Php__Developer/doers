<?php
namespace App\Controller\Sales;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Component\CommonComponent;

class SalesController extends AppController {

    var $name = "Sales";    // Controller Name    
    var $paginate = array();
    var $uses = array('Sales', 'User'); // For Default Model

    /*     * *************** START FUNCTIONS ********************* */


    /**
     * @Date: 20-sep-2016   
     * @Method : beforeFilter    
     * @Purpose: This function is called before any other function.    
     * @Param: none   
     * @Return: none    
     * */
    function beforeFilter(Event $event) {

     parent::beforeFilter($event);
          // star to check permission
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     $controller=$this->request->params['controller'];
     $action=$this->request->params['action'];
     $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
     if($permission!=1){
       $this->Flash->error("You have no permisson to access this Page.");

       return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
     }
    // end code to check permissions
     $this->viewBuilder()->layout('layout_admin');
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
      return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common', $this->Common);

  }
/**
     * @Date: 20-sep-2016   
     * @Method : initialize    
     * @Purpose: This function is called initialize function.    
     * @Param: none   
     * @Return: none    
     * */
public function initialize()
{
  parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('Sales');
        $this->loadModel('SaleProfiles');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Projects');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Employees');
        
        $this->loadModel('Appraisals');
        $this->loadModel('ProfilesAudits');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        
      }


    /**
     * @Date: 20-sep-2016      
     * @Method : index         
     * @Purpose: This function is the default function of the controller  
     * @Param: none                                     
     * @Return: none                                          
     * */

    function index() {
      $this->render('login');
      if ($this->Session->read("SESSION_USER") != "") {
        $this->redirect('dashboard');
      }
    }


    /**
     * @Date: 20-sep-2016      
     * @Method : saleslist      
     * @Purpose: This function is to show list of sale profiles   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function saleslist() {
      $this->viewBuilder()->layout('new_layout_admin');
      $this->set('title_for_layout','Profile Management');
      $this->set("pageTitle","Profile Management");

      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
      $userID = $userSession[0];
      $instruct = ['133', '4', '66', '179','186'];
      $this->set("search1","");        
      $this->set("search2","");       
      $hint = $this->StaticPages->find('all', [
        'fields' => ['id',
        'title'],
        'conditions' => ['id IN' => $instruct,'agency_id' => $userSession[3]]
        ])->toArray();
      $this->set('tooltip', $hint);
      $projects = $this->Projects->find('all', [
        'fields' => [
        'Projects.profile_id'
        ],
        'conditions' => ['Projects.status' => 'current','agency_id' => $userSession[3]]
        ])->toArray();
      $this->set('projects', $projects);

      if(isset($this->request->data['SaleProfiles']) || !empty($this->request->query)) {  

        if(!empty($this->request->data['SaleProfiles']['fieldName']) || isset($this->request->query['field'])){      
          if(trim(isset($this->request->data['SaleProfiles']['fieldName'])) != ""){    
            $search1 = trim($this->request->data['SaleProfiles']['fieldName']);                    
          }elseif(isset($this->request->query['field'])){                              
            $search1 = trim($this->request->query['field']);                             
          }
          $this->set("search1",$search1);                                               
        }    

        if(isset($this->request->data['SaleProfiles']['value1']) || isset($this->request->data['SaleProfiles']['value2']) || isset($this->request->query['value'])){     
          if(isset($this->request->data['SaleProfiles']['value1']) || isset($this->request->data['SaleProfiles']['value2'])){                          
            $search2 = ($this->request->data['SaleProfiles']['fieldName'] != "SaleProfiles.status")?trim($this->request->data['SaleProfiles']['value1']):$this->request->data['SaleProfiles']['value2'];       
          }elseif(isset($this->request->query['value'])){         
            $search2 = trim($this->request->query['value']);        
          }                                                
          $this->set("search2",$search2);                         
        }        
        /* Searching starts from here */               
        if(!empty($search1) && (isset($search2) || $search1 == "SaleProfiles.status")){    
         /* $query  =    $this->SaleProfiles->find("all" , [
            'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'agency_id' => $userSession[3] ],
            ]);                 
          */
          $cond = ['agency_id' => $userSession[3] ,$search1 .' Like' =>'%'.$search2.'%'];
         // $session->write('SESSION_SEARCH', $criteria);   
        }else{
          $cond = ['agency_id' => $userSession[3]];
          $this->set("search1","");        
          $this->set("search2","");       
        }          
      }  else  {
           $cond = ['agency_id' => $userSession[3]];
      }
      $res = $this->SaleProfiles->find('all', [
        'fields' => [
        'id',
        'nameonprofile',
        'username',
        'profileurl'
        ],
        'conditions' => ['SaleProfiles.status' => 1,'agency_id' => $userSession[3]],
        'order' => ['status' => 'DESC' ,'nameonprofile' => 'ASC' ,'modified' => 'DESC']
        ])->toArray();

      $user_ids = array();
      foreach ($res as $userresult) {
        $user_ids[] = $userresult['id'];
      }
      $ids = implode(',', $user_ids);
      if (date('N') == 1) {
        $twoweekdate = date('Y-m-d', strtotime("-2 week monday "));
      } else {
        $twoweekdate = date('Y-m-d', strtotime("-3 week monday "));
      }
      $credentials= TableRegistry::get('Projects');
      $profileauditreport = $credentials->find();
      $profileauditreport->select(['profile_id',
        'created',
        'modified',
        'status',
        'project_name']) 
      ->where([
        'profile_id IN' => $ids,'agency_id' => $userSession[3],
        'OR' => [['status' => 'current'], ['status' => 'closed']
        ],
        'AND'=>[[`created`>='$twoweekdate' OR `modified`>='$twoweekdate']],
        ]);
      $profileauditreportdata = array();
      $profileresult = array();
      foreach ($profileauditreport as $result) {
       $profileauditreportdata[$result['profile_id']][$result['status']][] = $result['project_name'];
     }
     $i = 0;
     foreach ($res as $val) {
      $profileresult[] = $val;
      $profileresult[$i]['current_project'] = isset($profileauditreportdata[$val['id']]['current']) ? implode(',', $profileauditreportdata[$val['id']]['current']) : '' ;
      $profileresult[$i]['closed_project'] = isset($profileauditreportdata[$val['id']]['closed']) ? implode(',', $profileauditreportdata[$val['id']]['closed']): '' ;
      $i++;
    }
    $this->set('profileresult', $profileresult);
    $Profileaudit = TableRegistry::get('Profileaudits');
    $pro = $Profileaudit->find('all',['fields'=>['status','profile_id','resolvedby'],
            'conditions' => ['agency_id' => $userSession[3]]
          ]);
    $this->set('options', $pro);
    $Project = TableRegistry::get('Projects');
    $this->set('ProjectOption', $Project->find('all',['fields'=>['project_name','status','profile_id'],
          'conditions' => ['agency_id' => $userSession[3]]
      ])
    );
    //pr($cond); die;

    $this->paginate = [           
      'page'=> 1,'limit' => 50,     
      'order' => ['status' => 'DESC' ,'nameonprofile' => 'ASC' ,'modified' => 'DESC']          
    ];     

$res = $this->paginate('SaleProfiles',['conditions' => $cond]);


    /*$res = $this->SaleProfiles->find('all', [
      'conditions' => $cond,
      'order' => ['status' => 'DESC' ,'nameonprofile' => 'ASC' ,'modified' => 'DESC']]);*/
    $resultData=array();
    foreach($res as $sales){
      $sales['Projects']['status']= $this->_getProjectStatusByProfile($sales['id']);
      $resultData[] = $sales;
    }
    $counter=0; 
    $totalcount = 0;
    foreach($resultData as $masterData){ 
     $resultData[$counter]['encrption'] = $this->Common->mc_encrypt($masterData['id'],ENCRYPTION_KEY);
     $id = $this->Common->mc_decrypt($resultData[$counter]['encrption'], ENCRYPTION_KEY);
     $id_ = $masterData['id'];
      $count =0;
       foreach($pro as $option){
              if($option['status']==1){
               $ids=$option['profile_id'];
             
               if($ids==$id_){ 
                  $count++;                                          
               } 
          }
       }
     $totalcount = $totalcount+$count;
     $counter++;
   }
   //echo $totalcount; die;
   $this->set('totalcount', $totalcount); 
   $this->set('resultData', $resultData);
   $this->set('pagename', "saleslist");       
 }

 /**
     * @Date: 20-sep-2016      
     * @Method : _getProjectStatusByProfile      
     * @Purpose: This function is to show list of sale profiles   
     * @Param: none                                                       
     * @Return: none                                                  
     * */
 function _getProjectStatusByProfile($id){
   $Project = TableRegistry::get('Projects');
   $projects= $this->Projects->find()
   ->select(['project_name','status','profile_id'])
   ->where(['profile_id'=>$id])
   ->all();
   $satus = 'no';
   foreach($projects as $pro){
    if($pro['Project']['status']=='current'){
      $satus='current';
      break;
    }
  }
  if($satus=='current')
    return $satus;
  foreach($projects as $pro){
    if($pro['Project']['status']=='closed'){
      $satus='closed';
      break;
    }
  }

  if($satus=='closed'){
    return $satus;
  }else{
    return 'no';
  }

    //Alotted By Counter 
  $allotedRes = $this->saleProfile->find('all', array(
    'fields' => array(
      'saleProfile.id',
      'saleProfile.alloteduserid',
      'saleProfile.nameonprofile',
      'saleProfile.quota'
      ),
    'conditions' => array('saleProfile.id' => $id)
    ));
    // Runining Project Notification counter:
  $projects = $this->Project->find('all', array(
    'fields' => array(
      'Project.profile_id'
      ),
    'conditions' => array('Project.status' => 'current')
    ));
  $this->set('projects', $projects);
  $res = $this->saleProfile->find('all', array(
    'fields' => array(
      'id',
      'nameonprofile',
      'username',
      'profileurl'
      ),
    'conditions' => array('saleProfile.status' => 1)
    ));
  $user_ids = array();
  foreach ($res as $userresult) {
    $user_ids[] = $userresult['saleProfile']['id'];
  }
  $ids = implode(',', $user_ids);


  if (date('N') == 1) {
    $twoweekdate = date('Y-m-d', strtotime("-2 week monday "));
  } else {
    $twoweekdate = date('Y-m-d', strtotime("-3 week monday "));
  }

  $profileauditreport = $this->saleProfile->query("SELECT `profile_id`,`created`, `modified`,`status`,`project_name` FROM `projects` WHERE `profile_id` in ($ids) AND (`status`='current' OR (`status`='closed' AND (`created`>='$twoweekdate' OR `modified`>='$twoweekdate' )))");

  $profileauditreportdata = array();
  $profileresult = array();
  foreach ($profileauditreport as $result) {
    $profileauditreportdata[$result['projects']['profile_id']][$result['projects']['status']][] = $result['projects']['project_name'];
  }
  $i = 0;
  foreach ($res as $val) {
    $profileresult[] = $val;
    $profileresult[$i]['saleProfile']['current_project'] = implode(",", $profileauditreportdata[$val['saleProfile']['id']]['current']);
    $profileresult[$i]['saleProfile']['closed_project'] = implode(",", $profileauditreportdata[$val['saleProfile']['id']]['closed']);
    $i++;
  }

  $this->set('profileresult', $profileresult);

}


    /**
     * @Date: 20-sep-2016       
     * @Method : add      
     * @Purpose: This function is to add new sale profiles  
     * @Param: none                                                       
     * @Return: none                                                  
     * */


    function add() {   
    $this->viewBuilder()->layout('new_layout_admin');  
      $session = $this->request->session();
      $this->set('session',$session);  
      $userSession = $session->read("SESSION_ADMIN");
      $this->set('title_for_layout','Add Sale Profile');       
      $this->pageTitle = "Add sale Profile";           
      $this->set("pageTitle","sale Profile");  
      $mode = "add";
      if($this->request->data){
       $alloteduserid=(isset($this->request->data['alloteduserid']) && !empty($this->request->data['alloteduserid'])) ? implode(",",$this->request->data['alloteduserid']): "";
       $this->request->data['alloteduserid'] = $alloteduserid;

       if(!empty($this->request->data['lastauditdate'])){
        $date =  date_format(date_create($this->request->data['lastauditdate']),"Y-m-d");    
      } else{
        $date =  date("Y-m-d") ;    
      }

      $this->request->data['lastauditdate'] = $date;
      $this->request->data['agency_id'] = $userSession[3];
      $sales = $this->SaleProfiles->newEntity($this->request->data());
      if(empty($sales->errors())){
        $this->SaleProfiles->save($sales);
        if(isset($this->request->data['alloteduserid']) && !empty($this->request->data['alloteduserid'])){
          $status= $this->request->data['status']; 
          $other_alloted_user =  explode(',',$this->request->data['alloteduserid']);
          if(count($other_alloted_user) > 0){
            foreach($other_alloted_user as $other_alloted_user1){
              $data['user_id']=$other_alloted_user1;
              $data['credential_id']=$sales->id;
              $data['type']='saleProfile';
              $data['status']=$status;           
              $data['allocatedon'] =date("Y-m-d H:i:s");
              $c_logs = $this->CredentialLogs->newEntity($data);
              $this->CredentialLogs->save($c_logs);

            }
          }

        }
        $this->Flash->success_new("Sale Profile has been created successfully.",'layout_success');
        return $this->redirect(
         ['controller' => 'Sales', 'action' => 'saleslist']
         );     
      }else{
        $this->set("errors", $sales->errors());
      }
    }     
  }     
 /**  
     * @Date: 20-sep-2016
     * @Method : quickedit
     * @Purpose: This function is to edit sales Profile  from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */


 function quickedit($id = null) { 

  $this->viewBuilder()->layout(false);

  if($this->request->data){ 
    $SaleProfile = $this->SaleProfiles->get($this->request->data['id']);
    $already_alloted = explode(',', $SaleProfile['alloteduserid']);
    $status= $this->request->data['status']; 
    if(isset($this->request->data['alloteduserid']) && !empty($this->request->data['alloteduserid'])){
     $other_alloted_user =  $this->request->data['alloteduserid'];
     if(count($already_alloted) > 0){
      foreach($already_alloted as $other_alloted_user1){
        if(!in_array($other_alloted_user1, $other_alloted_user)){
          $cre = $this->CredentialLogs->find('all',[
            'conditions' => ['credential_id' => $SaleProfile->id ,'type' => 'saleprofile','user_id' => $other_alloted_user1]
            ])->first();
          if(count($cre) > 0){
            $cre_log = $skill = $this->CredentialLogs->get($cre->id);
            $data['user_id']=0;
            $data['credential_id']=$SaleProfile->id;
            $data['type']='saleProfile';
            $data['status']=$status;           
            $data['deallocatedon'] =date("Y-m-d H:i:s");
            $skills = $this->CredentialLogs->patchEntity($cre_log,$data);
            $this->CredentialLogs->save($cre_log); 
          }
        } 
      }
    }
    if(count($other_alloted_user) > 0){
      foreach($other_alloted_user as $other_alloted_user1){
        if(!in_array($other_alloted_user1, $already_alloted)){
         $data['user_id']=$other_alloted_user1;
         $data['credential_id']=$SaleProfile->id;
         $data['type']='saleProfile';
         $data['status']=$status;           
         $data['allocatedon'] =date("Y-m-d H:i:s");
         $c_logs = $this->CredentialLogs->newEntity($data);
         $this->CredentialLogs->save($c_logs);
       } 
     }
   }


 } else {
  $cre = $this->CredentialLogs->find('all',[
    'conditions' => ['credential_id' => $SaleProfile->id ,'type' => 'saleprofile'/*'user_id' => $other_alloted_user1*/]
    ])->all();
  if(count($cre) > 0){
    foreach($cre as $cre_each){
      $cre_log = $skill = $this->CredentialLogs->get($cre_each->id);
      $data['user_id']=0;
      $data['credential_id']=$SaleProfile->id;
      $data['type']='saleProfile';
      $data['status']=$status;           
      $data['deallocatedon'] =date("Y-m-d H:i:s");
      $skills = $this->CredentialLogs->patchEntity($cre_log,$data);
      $this->CredentialLogs->save($cre_log); 
    }
  } 

}
$alloteduserid=(isset($this->request->data['alloteduserid']) && !empty($this->request->data['alloteduserid'])) ? implode(",",$this->request->data['alloteduserid']): "";
$this->request->data['alloteduserid'] = $alloteduserid;
//pr($this->request->data()); die;
$SaleProfiles = $this->SaleProfiles->patchEntity($SaleProfile,$this->request->data());
if(empty($SaleProfiles->errors())) {
  $this->SaleProfiles->save($SaleProfile);
  $this->Flash->success_new("SaleProfile has been updated successfully.");
  echo 1;
  die;
}else{   
 echo json_encode(['status'=>'failed','errors'=>$SaleProfiles->errors()]);
 die;

} 
}else if(!empty($id)){
  $SaleProfilesData = $this->SaleProfiles->get($id);
  $resultData = $SaleProfilesData->toArray();
  $this->set('SaleProfilesData',$SaleProfilesData); 
  $this->set('resultData',$resultData); 
  if(isset($SaleProfilesData['created'])){
    $nextrelease= $SaleProfilesData['created']->format('Y-m-d');
  }
  else{
   $nextrelease= '';
 }
 if(isset($SaleProfilesData['modified'])){
  $finishdate= $SaleProfilesData['modified']->format('Y-m-d');
}
else{
 $finishdate= '';
}
if(isset($SaleProfilesData['lastauditdate'])){
  $lastauditdate= $SaleProfilesData['lastauditdate']->format('Y-m-d');
}
else{
 $finishdate= '';
}
$this->set('nextrelease',$nextrelease);
$this->set('finishdate',$finishdate);
$this->set('lastauditdate',$lastauditdate);

if(!$resultData){             

  return $this->redirect(
   ['controller' => 'Sales', 'action' => 'saleslist']
   );     

}                                                     
}                                                          
}    

 /**  
     * @Date: 20-sep-2016
     * @Method : csdased
     * @Purpose: This function is to csdased sales Profile  from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */
 function csdased(){
  $SaleProfiles = $this->SaleProfiles->find('all')->all();
  foreach ($SaleProfiles as $saleprofile) {
    $cre = $this->CredentialLogs->find('all',[
     'conditions' => ['credential_id' => $saleprofile->id ,'type' => 'saleprofile']
     ])->all();
    if(count($cre) > 0){
      foreach($cre as $cre_each){
        $cre_log = $skill = $this->CredentialLogs->get($cre_each->id);
                 // $toarr = $cre_log->toArray() ;
        $data['user_id']=0;
        $data['credential_id']=$saleprofile->id;
        $data['type']='saleProfile';
        $data['status']=$cre_log->status;           
        $data['deallocatedon'] =date("Y-m-d H:i:s");
        $skills = $this->CredentialLogs->patchEntity($cre_log,$data);
        $this->CredentialLogs->save($cre_log); 
      }
    }
    if(isset($saleprofile['alloteduserid']) && !empty($saleprofile['alloteduserid'])){
     $other_alloted_user =  explode(',', $saleprofile['alloteduserid']);

     if(count($other_alloted_user) > 0){
      foreach($other_alloted_user as $other_alloted_user1){

        $data['user_id']=$other_alloted_user1;
        $data['credential_id']=$saleprofile->id;
        $data['type']='saleProfile';
        $data['status']=1;           
        $data['allocatedon'] =date("Y-m-d H:i:s");
        $c_logs = $this->CredentialLogs->newEntity($data);
        $this->CredentialLogs->save($c_logs);

      }
    }


  } 

}
die("done");

}

    /**   
     * @Date: 20-sep-2016 
     * @Method : delete
     * @Purpose: This function is to delete sale Profile from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */




   function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
        $sale = $this->SaleProfiles->get($id);
    if ($this->SaleProfiles->delete($sale)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 



  function admin_delete1() {

    if (isset($this->params['pass'][0])) {
      $deleteString = $this->params['pass'][0];

    }
    if (!empty($deleteString)) {

      $this->saleProfile->deleteAll("saleProfile.id in ('" . $deleteString . "')");
      $this->Session->setFlash("<div class='success-message flash notice'>Sales Profile(s) deleted successfully.</div>", 'layout_success');
      $this->redirect('profiledetail');
    }

  }


    /**   
     * @Date: 20-sep-2016
     * @Method : view
     * @Purpose: This function is to view sale Profile from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */

    function view($id = null) { 
      $this->viewBuilder()->layout(false);
      $session = $this->request->session();
      $session_info = $session->read("SESSION_ADMIN");
      $viewdata = $this->SaleProfiles->find('all') 
      ->where(['id' => $id])->first();
      $allotedUserid = explode(',', $viewdata['alloteduserid']);
      $cond = array('Users.id' => $allotedUserid);
        /*$userdata = $this->Users->find('all')//, [
        -> select(['id','first_name','last_name'])->toArray();*/
        $userdata = $this->Employees->find('all',[
          'conditions' => ['id IN' => $allotedUserid,'agency_id' => $session_info[3],'status' => '1']
          ])->all();
        $this->set('resultData', $viewdata);
        $this->set('userdata', $userdata);
      }


    /**   
     * @Date: 20-sep-2016
     * @Method : changestatus
     * @Purpose: This function is to activate or deactivate sale Profile from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */


/*
    function changestatus($id = null) {
     if($this->request->is('ajax')){ 
      if (isset($this->request->data['key']) && $this->request->data['key'] == 'activate') {
       $res = $this->SaleProfiles->updateAll(['status' => 1], ['id' => $this->request->data['id']]);
       if ($res) {
         $this->Flash->success('Profile has been activated successfully.','layout_success');
         echo '1';
         exit;
       } else {
         $this->Flash->success('Some error occurred.','layout_error');
         echo '0';
         exit;
       }
     }
     if (isset($this->request->data['key']) && $this->request->data['key'] == 'deactivate') {

      $res = $this->SaleProfiles->updateAll(['status' => 0], ['id' => $this->request->data['id']]);
      if ($res) {
        $this->Flash->success('Profile has been deactivated successfully.','layout_success');
        echo '1';
        exit;
      } else {
        $this->Flash->success('Some error occurred.','layout_error');
        echo '0';
        exit;
      }
    }
  }
        $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
         $res = $this->SaleProfiles->updateAll(['status' => 1], ['id' => $id]);
    if ($res) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 



}*/
    function changestatus() {
 
     if($this->request->is('ajax')) {
       $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $updateCredentials = 1;
      $setValue = 0;
      $messageStr = 'deactivate';
      }
    
     $users = TableRegistry::get('SaleProfiles');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;

      }
     $this->setAction('saleslist'); 
   }

    /**   
     * @Date: 20-sep-2016
     * @Method : shareunshare
     * @Purpose: This function is to share or unshare sale Profile from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */

   function share($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
       $res = $this->SaleProfiles->updateAll(['is_shared' => 1], ['id' => $id]);
       if ($res) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
        } 
    } 

   function unshare($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
       $res = $this->SaleProfiles->updateAll(['is_shared' => 0], ['id' => $id]);
       if ($res) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
        } 
    } 


    /**   
     * @Date: 20-sep-2016 
     * @Method : allotedto
     * @Purpose: This function is to show alloted project to any sale Profile from admin section. 
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */


    function allotedto($id = null) {

      $this->viewBuilder()->layout(false);

 $session = $this->request->session();
      $session_info = $session->read("SESSION_ADMIN");

      $allotedRes = $this->SaleProfiles->find('all')
      ->where(['SaleProfiles.id' => $id,'SaleProfiles.agency_id'=>$session_info[3]])
      ->first()->toArray();
      /*pr($allotedRes); die;*/
      $allotedUserids = explode(',', $allotedRes['alloteduserid']);
      $this->set('allotedUserids',$allotedUserids);
      if(count($allotedUserids)){
        $usersdata = $this->Employees->find('all',[
          'conditions' => ['id IN' => $allotedUserids,'agency_id' => $session_info[3],'status' => '1']
          ])->all();
      
        $this->set('usersdata',$usersdata);
      }
      $finalData = array();
      $query=TableRegistry::get('CredentialLogs');
      $cdata=$query->find();
     

      $cdata->select(['Users.first_name','Users.id','Users.status','CredentialLogs.id','CredentialLogs.type','CredentialLogs.allocatedon','CredentialLogs.deallocatedon'])
      ->where(['CredentialLogs.credential_id' => $id  /*'CredentialLogs.user_id IN ' => $allotedUserids*/])
      ->order(['CredentialLogs.id DESC'])
      ->group('Users.first_name')
      ->join([
        'table' => 'users',
        'alias' => 'Users',
        'foreignKey' => false,
        'conditions' => array ('Users.id = CredentialLogs.user_id','Users.status' => '1')
        ]);
     /* echo "<pre>";
      print_r($cdata->toArray()); die;*/
      $this->set('cdata', $cdata->toArray());
    }


    /**   
     * @Date: 20-sep-2016
     * @Method : profilereportbyallottedto
     * @Purpose: This function is to show profile report by allotted ids from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */

    function profilereportbyallottedto($id = null) {
      $this->viewBuilder()->layout(false);
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session ->read("SESSION_ADMIN");

      $user_id = $this->Users->find('all', [
        'fields' => [
        'id',  
        'first_name',
        'last_name'
        ],
        'conditions' => ['agency_id'=>$userSession[3],'role_id Like' => '%5%','status' => 1 ]])->toArray();
      $allotedRes = $this->SaleProfiles->find('all', [
        'fields' => [
        'id',
        'alloteduserid',
        'nameonprofile',
        'quota'
        ],  'conditions' => ['agency_id'=>$userSession[3]]])->toArray();


      $ids = array();
      $result = array();
      foreach ($user_id as $user) {

        $id = $user['id'];
        $name = $user['first_name']." ". $user['last_name'];
        foreach ($allotedRes as $allot) {
          $ids = explode(',', $allot['alloteduserid']);
          if (in_array($id, $ids)) {
           $result[$name][] = $allot['nameonprofile'] . "(" . $allot['quota'] . ")";
         }
       }
     }
     $this->set('allotedprfile', $result);
     $this->set('user_id', $user_id);

   }

   
    /**   
     * @Date: 20-sep-2016
     * @Method : runningprojects
     * @Purpose: This function is to show running projects to any sale Profile from admin section.  
     * @Param: $id  
     * @Return: none  
     * @Return: none 
     * */

    function runningprojects($id = null) {
     $this->viewBuilder()->layout(false);
     $id = $id; 
     if (date('N') == 1) {
      $twoweekdate = date('Y-m-d', strtotime("-2 week monday "));
    } else {
      $twoweekdate = date('Y-m-d', strtotime("-3 week monday "));
    }
    $current = date("Y-m-d");
    $cond = array('Projects.status' => 'current');
    $runningprojects =   $this->Sales->find()
    ->select([
     'Sales.id',
     'Sales.profileurl',
     'Projects.profile_id',
     'Projects.project_name',
     'Projects.id',
     'Projects.client_id',
     'Contacts.name',
     ])
    ->where(['Sales.id'=>$id,$cond])
    ->join([
      'table' => 'projects',
      'alias' => 'Projects',
      'type' => 'INNER',
      'foreignKey' => false,
      'conditions' => array('Sales.id = Projects.profile_id')
      ])
    ->join([
      'table' => 'contacts',
      'alias' => 'Contacts',
      'type' => 'INNER',
      'foreignKey' => false,
      'conditions' => array('Contacts.id = Projects.client_id')
      ])->toArray();

    $closedcond = array('Projects.status' => 'closed', 'AND' => array('or' => array('Projects.modified >' => $twoweekdate, 'Projects.created >' => $twoweekdate,)));
    $closedprojects =   $this->Sales->find()
    ->select([
     'Sales.id',
     'Sales.profileurl',
     'Projects.profile_id',
     'Projects.project_name',
     'Projects.id',
     'Projects.client_id',
     'Contacts.name',
     ])
    ->where([ 'Sales.id'=>$id,$cond])
    ->join([
      'table' => 'projects',
      'alias' => 'Projects',
      'type' => 'INNER',
      'foreignKey' => false,
      'conditions' => array('Sales.id = Projects.profile_id')
      ])
    ->join([
      'table' => 'contacts',
      'alias' => 'Contacts',
      'type' => 'INNER',
      'foreignKey' => false,
      'conditions' => array('Contacts.id = Projects.client_id')
      ])->toArray();

    $this->set('runningprojects', $runningprojects);
    $this->set('closedprojects', $closedprojects);
  }


    /**
     * @Date: 20-sep-2016       
     * @Method : profiledetail    
     * @Purpose: This function is to show details of sale profile   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function profiledetail() {
    $this->viewBuilder()->layout('new_layout_admin');
      $this->set('title_for_layout', 'Profile Details');
      $this->set("pageTitle", "Profile Details");
      $this->viewBuilder()->layout = 'layout_graph';
      $session = $this->request->session();
      $this->set('session',$session);
      $session_info = $session->read("SESSION_ADMIN");
      $userID = $session_info[0];
      $this->set("userID", $userID);
      $credentials= TableRegistry::get('Profileaudits');
      $options = $credentials->find("all")->toArray();
       $this->set("options", $options); //status ,profileaudir,resolved by
       $res = $this->Sales->find('all', array(
        'conditions' => array('status' => 1)
        ))->toArray();
       $ids = array();
       $finalid = array();
       $id_array = array();
       foreach ($res as $val) {
        if (in_array($userID, explode(",", $val['alloteduserid']))) {
          $id_array[$val['id']] = $val['alloteduserid'];
        }
      }
      if(count($id_array)>0){
       $ids[] = array_keys($id_array);
       $credentials= TableRegistry::get('Sales');
       $result = $credentials->find()->where(['id IN' => $ids[0] ]);
       if(count($result)>0){
         $this->set('profiledetail', $result);
       }else{
         $this->set('profiledetail', $result);
       }
     }else{
      $this->set('profiledetail', []);
    }

$this->set('pagename', 'profiledetail');

  }
  

    /**
     * @Date: 20-sep-2016     
     * @Method : suggestions    
     * @Purpose: This function is to suggestions given by sales person   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function suggestions($id = null) {

      $session = $this->request->session();
      $this->set('session',$session);
      $session_info = $session->read("SESSION_ADMIN");
      $userID = $session_info[0];
      $this->set("userID", $userID);
      $this->set("id", $id);

      $this->viewBuilder()->layout(false);

      if (!empty($id)) {
        $data = $this->Sales->find()->where(['id' => $id])->first();
        $this->set("salesreppoints", $data['salesreppoints']);
      }

      if ($this->request->is('ajax')) { 
       $credentials = TableRegistry::get('Sales');
       $update = ['salesreppoints' =>  $this->request->data['salesreppoints']];
       $result= $credentials->updateAll($update,['id' => $this->request->data['id']]);
       if ($result) {
        $messageStr="Suggestion  has been added successfully.";
        $this->Flash->success_new($messageStr);  
        echo "1";
        exit;
      } else {
        $messageStr="You have made no changes to save.";
        $this->Flash->error($messageStr);
        echo "0";
        exit;
      }

    }
  }


    /**
     * @Date: 20-sep-2016      
     * @Method : salesprofileauditreport    
     * @Purpose: This function is to suggestions given by sales person   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function salesprofileauditreport() {
     $this->viewBuilder()->layout(false);
       $session = $this->request->session();
        $this->set('session',$session);
    $userSession = $session ->read("SESSION_ADMIN");
     $Profileaudits = TableRegistry::get('Profileaudits');
    /* $this->set('options23', $Profileaudits->find('all',
      ['fields'=>['profile_id','auditpoint','status','modified']]));
*/
  $pro=$Profileaudits->find('all', [
        'fields' => [
      'profile_id','auditpoint','status','modified'
        ],
        'conditions' => ['agency_id'=>$userSession[3]]
        ]);

  $this->set('options23',$pro);


$res = $this->SaleProfiles->find('all', [
        'conditions' => ['agency_id'=>$userSession[3]]
        ])->toArray();

   /*  $res = $this->SaleProfiles->find('all')->toArray();*/
     $this->set('runningprofile', $res);
   }

    /**
     * @Date: 20-sep-2016       
     * @Method : profilereportbyrunnningproject    
     * @Purpose: This function is to show profile report according to running projects.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function profilereportbyrunnningproject() {
      $this->viewBuilder()->layout(false);
      $session = $this->request->session();
        $this->set('session',$session);
    $userSession = $session ->read("SESSION_ADMIN");

      $projects = $this->Projects->find('all', [
        'fields' => [
        'profile_id'
        ],
        'conditions' => ['status' => 'current','agency_id'=>$userSession[3]]
        ])->toArray();
      $this->set('projects', $projects);
      $res = $this->SaleProfiles->find('all', [
        'fields' => [
        'id',
        'nameonprofile',
        'username',
        'profileurl'
        ],
        'conditions' => ['status' => 1,'agency_id'=>$userSession[3]]
        ])->toArray();
      $user_ids = array();
      foreach ($res as $userresult) {
        $user_ids[] = $userresult['id'];
      }
      $ids = implode(',', $user_ids);
      if (date('N') == 1) {
        $twoweekdate = date('Y-m-d', strtotime("-2 week monday "));
      } else {
        $twoweekdate = date('Y-m-d', strtotime("-3 week monday "));
      }
      $credentials= TableRegistry::get('Projects');
      $profileauditreport = $credentials->find();
      $profileauditreport->select(['profile_id',
        'created',
        'modified',
        'status',
        'project_name']) 
      ->where([

        'OR' => [['status' => 'current'], ['status' => 'closed']
        ],
        'AND'=>[[`created`>='$twoweekdate' OR `modified`>='$twoweekdate'],['agency_id'=>$userSession[3]]],
        ]);
      $profileauditreportdata = array();
      $profileresult = array();
      foreach ($profileauditreport as $result) {
        $profileauditreportdata[$result['profile_id']][$result['status']][] = $result['project_name'];
      }
      $i = 0;
      foreach ($res as $val) {
        $profileresult[] = $val;
        $profileresult[$i]['current_project'] = implode(",", $profileauditreportdata[$val['id']]['current']);
        $profileresult[$i]['closed_project'] = implode(",", $profileauditreportdata[$val['id']]['closed']);
        $i++;
      }
      $this->set('profileresult', $profileresult);
    }


    /**
     * @Date: 20-sep-2016       
     * @Method : verifiedUnverifiedreport    
     * @Purpose: This function is to show profile report according to verified and unverified Id's.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function verifiedUnverifiedreport() {
        $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session ->read("SESSION_ADMIN");

     $this->viewBuilder()->layout(false);

     $verified = $this->SaleProfiles->find('all', ['fields' =>
       [
       'id',
       'username',
       'idverified',
       'nameonprofile',
       'profileurl',
       ],
       'conditions' => ['idverified' => 1,'agency_id'=>$userSession[3]]
       ]);
     $unverified = $this->SaleProfiles->find('all', ['fields' =>
       [
       'id',
       'username',
       'idverified',
       'nameonprofile',
       'profileurl',
       ],
       'conditions' => ['idverified' => 0,'agency_id'=>$userSession[3]]
       ]);

     $this->set('verified', $verified);
     $this->set('unverified', $unverified);
   }


    /**
     * @Date: 20-sep-2016       
     * @Method : technologyreport    
     * @Purpose: This function is to show profile report according technology.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function technologyreport() {
     $this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session ->read("SESSION_ADMIN");

     $this->set('title_for_layout', 'Technologywise Report');
     $this->set("pageTitle", "Technologywise Report");
     $this->viewBuilder()->layout = 'layout_graph';
     $technology = $this->SaleProfiles->find('all', ['fields' =>
      [
      'username',
      'nameonprofile',
      'profileurl',
      'technologyfor'
      ],
      'conditions' => ['status' => 1,'agency_id'=>$userSession[3]],
      'order' => ['technologyfor']
      ])->toArray();

     $this->set('technology', $technology);

   }

    /**
     * @Date: 20-sep-2016       
     * @Method : paymentreport    
     * @Purpose: This function is to show payment report for profiles.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function paymentreport() {
      $this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session ->read("SESSION_ADMIN");
      $this->set('title_for_layout', 'payment Report');
      $this->set("pageTitle", "payment Report");
      $this->viewBuilder()->layout = 'layout_graph';
      $payment = $this->SaleProfiles->find('all', ['fields' =>
       [
       'username',
       'nameonprofile',
       'associated_paypal',
       'payment_remark'
       ],
         'conditions' => ['agency_id'=>$userSession[3]]
       ]);
      $this->set('payment', $payment);
      
    }

    
    /**
     * @Date: 20-sep-2016       
     * @Method : admin_NotRunnining_projects    
     * @Purpose: This function is to show profile id's does not have running projects.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function admin_NotRunnining_projects() {
      $this->layout = 'layout_graph';
      $projects = $this->Project->find('all', array(
        'fields' => array(
          'Project.profile_id'
          ),
        'conditions' => array('Project.status' => 'current')
        ));

      $running = array();
      $unique = array();
      foreach ($projects as $val) {
        $running[] = $val['Project']['profile_id'];
      }
      $unique = array_unique($running);
      $running_projects = $this->saleProfile->find('all', array('fields' =>
        array(
          'saleProfile.username',
          'saleProfile.nameonprofile',
          'saleProfile.profileurl'
          ),
        'conditions' => array("NOT" => array('saleProfile.id' => $unique), array('saleProfile.status' => 1))
        ));

      $this->set('running_projects', $running_projects);
    }


    /**
     * @Date: 20-sep-2016       
     * @Method : salesedit    
     * @Purpose: This function is to suggestions given by sales person   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function salesedit($id = null) {

      $session = $this->request->session();
      $this->set('session',$session);
      $session_info = $session->read("SESSION_ADMIN");
      $userID = $session_info[0];
      $this->set("userID", $userID);
      $this->set("id", $id);

      $this->viewBuilder()->layout(false);
      if (!empty($id)) {

        $data = $this->Sales->find()->where(['id' => $id])->first();
        
        $this->set("rating", $data['rating']);
        $this->set("contract_on_hold", $data['contract_on_hold']);
        $this->set("status", $data['status']);

      }

      if ($this->request->is('ajax')) {  

       $credentials = TableRegistry::get('Sales');

       $update = ['contract_on_hold' =>  $this->request->data['contract_on_hold'],
       'stats' =>  $this->request->data['stats'],
       'rating' =>  $this->request->data['rating']
       ];
       $result= $credentials->updateAll($update,['id' => $this->request->data['id']]);
       if ($result) {
        $messageStr="Suggestion  has been added successfully.";
        $this->Flash->success_new($messageStr);  
         echo "1";
        exit;
      } else {
        $messageStr="You have made no changes to save.";
        $this->Flash->error($messageStr);
        echo "0";
        exit;
      }

    }

  }

  public function report(){
       $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session->read("SESSION_ADMIN");
        $role = explode(",",$userSession['2']);
          $this->viewBuilder()->layout('new_layout_report');
          $this->set('title_for_layout','Profile Report');      
          $this->set("pageTitle","Profile Report");     
          $this->set("search1", "");                           
          $this->set("search2", ""); 
         $criteria = "1"; //All Searching    
         $session->delete('SESSION_SEARCH'); 
         $allactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'agency_id'=>$userSession[3]]
          ])->count();
         $alldeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'agency_id'=>$userSession[3]]
          ])->count();

         $upworkactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'type' => 'upwork','agency_id'=>$userSession[3]]
          ])->count();

         $upworkdeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'type' => 'upwork','agency_id'=>$userSession[3]]
          ])->count();


         $elanceactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'type' => 'elance','agency_id'=>$userSession[3]]
          ])->count();

         $elancedeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'type' => 'elance','agency_id'=>$userSession[3]]
          ])->count();

         $PPHactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'type' => 'PPH','agency_id'=>$userSession[3]]
          ])->count();

         $PPHdeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'type' => 'PPH','agency_id'=>$userSession[3]]
          ])->count();

         $Guruactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'type' => 'Guru','agency_id'=>$userSession[3]]
          ])->count();

         $Gurudeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'type' => 'Guru','agency_id'=>$userSession[3]]
          ])->count();

         $Freelanceractive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 1,'type' => 'Freelancer','agency_id'=>$userSession[3]]
          ])->count();

         $Freelancerdeactive = $this->SaleProfiles->find('all',[
                  'conditions' => ['status' => 0,'type' => 'Freelancer','agency_id'=>$userSession[3]]
          ])->count();
          $this->set('allactive',$allactive);
          $this->set('alldeactive',$alldeactive);
          $this->set('upworkactive',$upworkactive);
          $this->set('upworkdeactive',$upworkdeactive);
          $this->set('elanceactive',$elanceactive);
          $this->set('elancedeactive',$elancedeactive);
          $this->set('PPHactive',$PPHactive);
          $this->set('PPHdeactive',$PPHdeactive);
          $this->set('Guruactive',$Guruactive);
          $this->set('Gurudeactive',$Gurudeactive);
          $this->set('Freelanceractive',$Freelanceractive);
          $this->set('Freelancerdeactive',$Freelancerdeactive);

        
         //pr($active); die;
  }

}