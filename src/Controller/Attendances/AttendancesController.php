<?php
namespace App\Controller\Attendances;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;

class AttendancesController extends AppController

{

    /******************************* START FUNCTIONS **************************/

 #_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none
    * @Return: none 
    **/

    function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        Router::parseNamedParams($this->request);
        if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {
            $this->checkUserSession();
        } else {
           $this->viewBuilder()->layout('layout_admin');
           // $this->viewBuilder()->layout('new_layout_admin');
        }
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
        if($userSession==''){
            return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
        }
        $this->set('common', $this->Common);
    }
        /**   
    * @Date: 13-sept-2016
    * @Method : initialize   
    * @Purpose: This function is called initialize  function. it is like constructor   
    * @Param: none   
    * @Return: none   
    **/   
        public function initialize()
        {
            parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Resumes');
        $this->loadModel('Tickets');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    #_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : index
    * @Purpose: This function is the default function of the controller
    * @Param: none
    * @Return: none 
    **/

    function index() {

        $this->render('login');

        if($this->Session->read("SESSION_USER") != ""){

            $this->redirect('dashboard');

        }

    }




    #_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : admin_list
    * @Purpose: This function is to show list of contacts in system.
    * @Param: none
    * @Return: none 
    **/

    function admin_list(){

        $this->set('title_for_layout','Attendance Listing');

        $this->set("pageTitle","Attendance Listing");

        $this->set("search1", "");

        $this->set("search2", "");

        $criteria = "1";
// Delete user and its licences and orders(single/multiple)
        if(isset($this->params['form']['delete']) || isset($this->params['pass'][0]) == "delete"){
            if(isset($this->params['form']['IDs'])){
                $deleteString = implode("','",$this->params['form']['IDs']);
            }elseif(isset($this->params['pass'][1])){
                $deleteString = $this->params['pass'][1];
            }
            if(!empty($deleteString)){
                $this->Attendance->deleteAll("Attendance.id in ('".$deleteString."')");
                $this->Session->setFlash("<div class='success-message flash notice'>Payment(s) deleted successfully.</div>");
                $this->redirect('list');
            }
        }

        if(isset($this->request->data) || !empty($this->params['named'])) {
            if(!empty($this->request->data['fieldName']) || isset($this->params['named']['field'])){
               if(trim(isset($this->request->data['fieldName'])) != ""){
                $search1 =($this->request->data['fieldName']!="Attendance.leave")?trim($this->request->data['fieldName']):"Attendance.leave_reason";
            }elseif(isset($this->params['named']['field'])){
                $search1 = trim($this->params['named']['field']);
            }
            $this->set("search1",$search1);
        }

        if(isset($this->request->data['value1']) || isset($this->request->data['value2']) || isset($this->params['named']['value'])){
            if(isset($this->request->data['value1']) || isset($this->request->data['value2'])){
               $search2 = ($this->request->data['fieldName'] != "Attendance.status")?trim($this->request->data['value1']):$this->request->data['value2'];
           }elseif(isset($this->params['named']['value'])){
            $search2 = trim($this->params['named']['value']);
        }
        $this->set("search2",$search2);

    }
    /* Searching starts from here */
    if(!empty($search1) && (!empty($search2) || $search1 == "Attendance.status")){
        $criteria = $search1." LIKE '%".Sanitize::escape($search2)."%'"; 

    }else{
       $this->set("search1","");
       $this->set("search2","");
   }
}
if(isset($this->params['named'])){
    $urlString = "/";
    $completeUrl  = array();
    if(!empty($this->params['named']['page']))
       $completeUrl['page'] = $this->params['named']['page'];
   if(!empty($this->params['named']['sort']))
    $completeUrl['sort'] = $this->params['named']['sort'];
if(!empty($this->params['named']['direction']))
   $completeUrl['direction'] = $this->params['named']['direction'];
if(!empty($search1))
   $completeUrl['field'] = $search1;
if(isset($search2))
    $completeUrl['value'] = $search2;
foreach($completeUrl as $key=>$value){
    $urlString.= $key.":".$value."/";
}
}

$this->set('urlString',$urlString);
if(isset($this->params['form']['publish'])){
    $setValue = array('status' => "'1'");
    $messageStr = "<div class='success-message flash notice'>Selected Payment(s) have been activated.</div>";
}elseif(isset($this->params['form']['unpublish'])){
   $setValue = array('status' => "'0'");
   $messageStr = "<div class='success-message flash notice'>Selected Payment(s) have been deactivated.</div>";
}
if(!empty($setValue)){
   if(isset($this->params['form']['IDs'])){
       $saveString = implode("','",$this->params['form']['IDs']);
   }
   if($saveString != ""){
       $this->Attendance->updateAll($setValue,"Attendance.id in ('".$saveString."')");
       $this->Session->setFlash($messageStr);
   }
}
$this->paginate = array(

    'fields' => array(

        'Attendance.id',
        'User.username',
        'Attendance.date',
        'Attendance.time_in',
        'Attendance.time_out',
        'Attendance.status',
        'Attendance.leave_reason',
        'Attendance.notes',

        'Attendance.created',
        'Attendance.modified'

        ),

    'page'=> 1,'limit' => 10,

    'order' => array('id' => 'desc')

    );
$this->Attendance->expects(array("User"));
$data = $this->paginate('Attendance',$criteria);
$this->set('resultData', $data);
}

 #_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : admin_delete
    * @Purpose: This function is to add/edit contacts from admin section.
    * @Param: $id
    * @Return: none 
    * @Return: none 
    **/
    function admin_delete($id)
    {
        if($this->Attendance->delete($id)){
            $this->Session->setFlash('The Attendance with id: '.$id .'  has been  deleted');
            $this->redirect(array('action'=>'list'));
        }   
    }   

    #_________________________________________________________________________#
     /**
    * @Date: 13-sept-2016
    * @Method : add
    * @Purpose: This function is to add attendance from admin section.
    * @Param: $id
    * @Return: none 
    * @Return: none 
    **/

     function add() {
        $this->viewBuilder()->layout('new_layout_admin');
        $session = $this->request->session();
        $this->set('session',$session);
        $session_admin = $session->read("SESSION_ADMIN");
        $cond_bet = ['Tickets.deadline >=' => date("Y-m-d 00:00:00") ];
        $cond=['to_id'=>$session_admin[0],'status IN' =>[1,2] ];
        $cond = array_merge($cond,$cond_bet);
        $close_cond_bet = array('Tickets.deadline >' =>date("Y-m-d 00:00:00") /*, 'Tickets.deadline <' =>date('Y-m-d 23:59:59')*/);
        $close_cond=array('Tickets.to_id'=>$session_admin[0],'Tickets.status' =>0);
        $close_cond = array_merge($close_cond,$close_cond_bet);
        $open_ticket  = $this->Tickets->find('all',[
           'conditions' => $cond,
           ])->count();
        $close_ticket  = $this->Tickets->find('all',[
            'conditions' => $close_cond,
            ])->count();
      /*  pr(date('Y-m-d 00:00:00 +0530'));
        pr(date('Y-m-d 00:00:00'));
        pr($open_ticket);
        pr($close_ticket); die;*/
        $this->set('active_ticket',$open_ticket);
        $this->set('close_ticket',$close_ticket);
        $this->set('title_for_layout','Add Attendance');
        $this->pageTitle = "Add Attendance";
        $this->set("pageTitle","Add Attendance");
        if($this->request->data){
           //pr($this->request->data); die;
            //var_dump($this->request->data['add_date']); die;
            $date = date_create($this->request->data['time_in'])->format('Y-m-d');
            $this->request->data['time_in'] = date_create($this->request->data['time_in'])->format('H:i:s');
            $this->request->data['time_out'] = date_create($this->request->data['time_out'])->format('H:i:s');

             /* $hour = $this->request->data['Inhour'];
            $min = $this->request->data['Inmin'];
            $this->request->data['add_date'] = date_create($this->request->data['add_date'])->format('d/m/Y');
            $intime = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']).' '.$hour.':'.$min.' '.$this->request->data['Inmeridian']; 
            $dateintime=date_create($intime);
            $this->request->data['time_in'] = $dateintime->format('H:i:s');
           hours
            $outtime = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']).' '.$this->request->data['Outhour'].':'.$this->request->data['Outmin'].' '.$this->request->data['Outmeridian']; 
            $dateintime=date_create($outtime);
            $this->request->data['time_out'] = $dateintime->format('H:i:s');
            //pr($this->request->data['time_out']); die;
            unset($this->request->data['Outhour'],$this->request->data['Outmin'],$this->request->data['Outmeridian']);
            unset($this->request->data['Inhour'],$this->request->data['Inmin'],$this->request->data['Inmeridian'] );
            $this->request->data['date'] = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']);
            $date=date_create(preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']));
            */
            $this->request->data['date'] = $date;
            $this->request->data['ipaddress'] = IP_ADDRESS;
            $this->request->data['user_id']=$session_admin[0];
            $this->request->data['agency_id']=$session_admin[3];

            unset($this->request->data['Outhour'],$this->request->data['Outmin'],$this->request->data['Outmeridian']);
            unset($this->request->data['Inhour'],$this->request->data['Inmin'],$this->request->data['Inmeridian'] );

            $result  = $this->Attendances->find('all' , [
                'conditions' => ['user_id' => $session_admin[0],'date'=>$this->request->data['date']],
                ])->first();
            if(!empty($result)){
                $this->request->data['id'] = $result['id'];
                if($result['verified'] == 1){
                    $this->Flash->success_new('Attendance for this date cannot be modified as it is already verified by HR', 'layout_error');
                    //$this->setAction('add');
                    return $this->redirect(['controller' => 'attendances', 'action' => 'add']);
                } else {
                    $resume = $this->Attendances->get($this->request->data['id']);
                    $attendance = $this->Attendances->patchEntity($resume,$this->request->data());
                }
            } else {
                $attendance = $this->Attendances->newEntity($this->request->data());   
            }
            if(empty($attendance->errors())){
                if(($open_ticket + $close_ticket) < 6){
                    return $this->redirect(['controller' => 'tickets', 'action' => 'index','prefix' => 'tickets' ]);
                }else{
                    $this->Attendances->save($attendance);
                     $this->Flash->success_new("Attendance has been created successfully.");
                    return $this->redirect(['controller' => 'attendances', 'action' => 'add']);
                }
            }else{
                //pr($attendance->errors()); die;
                $this->set("errors",$attendance->errors());
            }
        }
    }
    
    
    #_________________________________________________________________________#
     /**
    * @Date: 13-sept-2016
    * @Method : addall
    * @Purpose: This function is to add attendance for any users (Mainly By HR / Admins)
    * @Param: $id
    * @Return: none 
    * @Return: none 
    **/
     function addall($id = null,$date = null,$mode=null) {
        $this->viewBuilder()->layout('new_layout_admin');
         $session = $this->request->session();
        $this->set('session',$session);  
        $userSession = $session->read("SESSION_ADMIN");
        if((!empty($id) || !empty($this->request->data['id'])) && $mode == 'edit'){
            $this->set('selecteduser',$id);
            $this->set('title_for_layout','Edit Attendance');      
            $this->pageTitle = "Edit Attendance";
            $this->set("pageTitle","Edit Attendance");
            $mode = "edit";
        }else{
            $this->set('selecteduser',$id);           
            $this->set('title_for_layout','Add Attendance');
            $this->pageTitle = "Add Attendance";
            $this->set("pageTitle","Add Attendance");
            $mode = "add";
        }
        $this->set('mode',$mode);
        if($this->request->data){ 
            // pr($this->request->data); die;
            $hour = $this->request->data['Inhour'];
            $min = $this->request->data['Inmin'];
            $intime = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']).' '.$hour.':'.$min.' '.$this->request->data['Inmeridian']; 
            $dateintime=date_create($intime);
            $this->request->data['time_in'] = $dateintime->format('H:i:s');
            
            $outtime = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']).' '.$this->request->data['Outhour'].':'.$this->request->data['Outmin'].' '.$this->request->data['Outmeridian']; 
            $dateintime=date_create($outtime);
            $this->request->data['time_out'] = $dateintime->format('H:i:s');
                //pr($this->request->data['time_out']); die;
            unset($this->request->data['Outhour'],$this->request->data['Outmin'],$this->request->data['Outmeridian']);
            unset($this->request->data['Inhour'],$this->request->data['Inmin'],$this->request->data['Inmeridian'] );
            $this->request->data['date'] = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']);
            //pr($this->request->data['date']);
            $this->request->data['agency_id'] = $userSession[3];
            if(!empty($this->request->data['id'])){
                //pr("edit");
                $attendance = $this->Attendances->get($this->request->data['id']);
                $attendances = $this->Attendances->patchEntity($attendance,$this->request->data());
            } else {
               // pr("add");

                unset($this->request->data['id']);
                $this->request->data['date'] = date_create($this->request->data['date'])->format('Y-m-d');
                $attendances = $this->Attendances->newEntity($this->request->data());
            }
            //die;
            if(empty($attendances->errors())) {
                // pr($this->request->data); die;
                        $this->Attendances->save($attendances);
                        if($mode == "add"){ 
                             return $this->redirect(['controller' => 'attendances', 'action' => 'report']);
                            $this->Flash->success_new("Attendance has been created successfully.",'layout_success');                 
                        }else{
                            $this->Flash->success_new("Attendance has been updated successfully.",'layout_success'); 
                        }                                       
                    return $this->redirect(['controller' => 'attendances', 'action' => 'report']);
                }else{
                    //pr($attendances->errors()); die;
                    $this->set("errors", $attendances->errors());
                }
            }else if(!empty($id) && $mode == 'edit'){

                $date = (empty($date)? date("Y-m-d") : $date ) ;
                $resultData = $this->Attendances->find('all',[
                    'conditions'=>['user_id'=>$id,'date'=> $date]
                    ]);

                $result = isset($resultData)? $resultData->first()->toArray():array();
                //pr($result); die;
                $this->set("resultData",$resultData);
                $this->set("result",$result);
                if(!$resultData){
                    $this->redirect(array('action' => 'addall'));
                }
            }
        }

    #_________________________________________________________________________#
         /**
    * @Date: 13-sept-2016
    * @Method : admin_edit
    * @Purpose: This function is to edit attendance from admin section.
    * @Param: $id
    * @Return: none 
    * @Return: none 
    **/
         function admin_edit($id = null) {
            $this->set('title_for_layout','Edit Attendance');
            $this->pageTitle = "Edit Attendance";
            $this->set("pageTitle","Edit Attendance");

            if($this->request->data){ 
                $this->request->data['date'] = preg_replace("/^([\d]+)\/([\d]+)\/([\d]+)$/i","$3-$2-$1", $this->request->data['add_date']);
                $this->Attendance->set($this->request->data);

                $isValidated=$this->Attendance->validates();

                if($isValidated){
                    $session_admin = $this->Session->read("SESSION_ADMIN");
                    $this->request->data['user_id']=$session_admin[0];
                          // If attendence already exists
                    $result = $this->Attendance->find('first', array('conditions'=>array('user_id'=>$session_admin[0], 'date'=>$this->request->data['date']))); 
                    if(!empty($result)){
                        $this->request->data['id'] = $result['Attendance']['id'];
                    }
                          // Insert or update attendance
                    $this->Attendance->save($this->request->data, array('validate'=>false));
                    $this->Session->setFlash('Attendance has been updated successfully.', 'layout_success');
                    $this->redirect('edit');

                }else{

                    $this->set("Error",$this->Attendance->invalidFields());

                }

            }else if(!empty($id)){
                $this->request->data = $this->Attendance->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));   
                if(!$this->request->data){
                    $this->redirect(array('action' => 'list'));
                }

            }
        }

    #_________________________________________________________________________#
    /**
    * @Date: 13-sept-2016
    * @Method : report
    * @Purpose: This function is to fetch attendance report from admin section.
    * @Param: none
    * @Return: none
    **/
    function report() {
        $this->viewBuilder()->layout('new_layout_admin');
        $this->set('title_for_layout','Attendance Report');
        $session = $this->request->session();
        $this->set('session',$session);  
        $userSession = $session->read("SESSION_ADMIN");

        if($this->request->is('ajax')){
          
          // $date =  /*date("Y-m-d",strtotime($this->request->data['date'])); */
           $date =  date_create($this->request->data['date'])->format('Y-m-d');
          // pr($date);
            $user_id = $this->request->data['user_id'];

            $verified = $this->request->data['verified'];
           // pr($verified);   pr($user_id); die;
            $find = $this->Attendances->find('all',[
                'conditions' => ['user_id'=>$user_id,'date' =>$date,'agency_id' => $userSession[3]]
                ])->first();
            $result = $this->Attendances->updateAll(['verified'=>$verified],
                ['id'=> $find->id]);
            //pr($result);die;
            if($result)
             echo    json_encode(['status' => 'success', 'verified' => $verified]);
            else
                echo json_encode(['status' => 'failed']);
            die;
            
        }   
        if(isset($this->request->data['datetoshow'])){
            $str = str_replace("/","-",$this->request->data['datetoshow']);
            $date = date("Y-m-d",strtotime($str));
        }
        $dated = (!empty($date)?$date:date('Y-m-d'));
       $connection = ConnectionManager::get('default'); // Database object for custom query
       $query ="SELECT U.id, U.first_name,U.agency_id, U.last_name,A.time_in, A.time_out,A.hours, A.notes,A.date,A.status,A.hrnotes,A.verified
       FROM  users AS U
       Left JOIN  attendances AS A ON  U.id = A.user_id
       Where A.date =  '".$dated."' and U.status ='1' and U.agency_id = '".$userSession[3]."'

       UNION ALL

        Select id, first_name,agency_id , last_name, '00:00:00', '00:00:00', '-','---',' ',' ',' ',' ' from users where id not in (SELECT distinct user_id from attendances where date =  '".$dated."' )  and status ='1'  and agency_id = '".$userSession[3]."' order by first_name "; // working
        $result = $connection->execute($query)->fetchAll('assoc');
       /* pr($result); die;*/
        $reportData = $result;
        $closeToday = $this->Tickets->find('all',array(
            'fields' => array('id','closedate','to_id'),
            'conditions' => array('status' =>'0','closedate' => $dated,'agency_id' => $userSession[3])
            ))->toArray();
        $this->set('closeToday', $closeToday);
        $this->set('date', $dated);
        $this->set('report', $reportData);
    }
    
    #_________________________________________________________________________#
    
    /**
    * @Date: 13-sept-2016
    * @Method : userview
    * @Purpose: This function is to fetch attendance report from admin section.
    * @Param: none
    * @Return: none
    **/
    
    function userview($id=null) {
       // pr($id);die();
        $session = $this->request->session();
        $this->set('session',$session);
        $this->viewBuilder()->layout(false);

        $session_admin = $session->read("SESSION_ADMIN");
        $user_id = (empty($id)) ? $session_admin[0] : $id ;  // user id

        $previousMonth = date("Y-m-d", mktime(0, 0, 0, date('m') - 1, 01, date('Y'))); 

        $currentDate = date("Y-m-d");


        $condition = "`user_id` = '$user_id' and (`date` >= '$previousMonth' and   `date` <= '$currentDate')";

        $resultAttendance  = $this->Attendances->find("all" , [
            'conditions' => $condition,
            'order' => ['date' => 'DESC']
            ])->all();


        $closeToday  = $this->Tickets->find("all" , [
            'conditions' => [ 'to_id' => $user_id ,'closedate >=' => $previousMonth, 'status' => 0,
            'OR' =>[['to_id' => $user_id ,'closedate <=' => $currentDate,'status' => 0]]
            ],
            'order' => ['closedate' => 'DESC']
            ])->all();
        $this->set('closeToday', $closeToday);
        $this->set('userID', $user_id);

        $this->set('report', $resultAttendance);
        
    }
    #_________________________________________________________________________#
    
    /**
    * @Date: 13-sept-2016
    * @Method : userattendance
    * @Purpose: This function is to fetch attendance report from admin section.
    * @Param: none
    * @Return: none
    **/
    
    function userattendance() {
        $this->viewBuilder()->layout(false);
        if($this->request->data){

            $todate = str_replace("/","-",$this->request->data['datetoshow']);
            $fromdate = str_replace("/","-",$this->request->data['datefromshow']);
            $create=date_create($todate);
            $to = $create->format('Y-m-d');
            $create=date_create($fromdate);
            $from = $create->format('Y-m-d');
            
            $user_id = $this->request->data['user_id'];

            $condition = "`user_id` = '$user_id' and (`date` >= '$todate' and   `date` <= '$fromdate')";

            $resultAttendance = $this->Attendances->find("all",[
                'fields' => ['id','time_in','time_out','date','hours','status','notes','hrnotes','verified'],
                'conditions' => [ 'user_id' => $user_id, 'date >=' => $to, 'date <=' => $from],
                'order' => 'date DESC'
                ])->toArray();
            $this->set('todate', $to);
            $this->set('fromdate', $from);
            $this->set('report', $resultAttendance);
        }
        
        
    }
    #_________________________________________________________________________#
    
    /**
    * @Date: 13-sept-2016
    * @Method : admin_sendattendance
    * @Purpose: 
    * @Param: none
    * @Return: none
    **/
    
    function  admin_sendattendance($id = null) {
$this->layout= false;  // setting layout to false as we have to open the same in fancy box
if(!empty($id) || !empty($this->request->data['id'])){

    $id = (!empty($this->request->data['id'])?$this->request->data['id']:$id);

    $crData = $this->Attendance->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));
    if($crData){                                                         
        $this->set('crData',$crData); 

    }                                                                         
}

if($this->request->data){
    $this->request->data['date']=date("Y-m-d ", strtotime("-2 hours"));
    $this->Attendance->set($this->request->data);

    $isValidated=$this->Attendance->validates();

    if($isValidated){

        $data=$this->request->data;

        $this->Attendance->save($this->request->data, array('validate'=>false));
    }
    $this->Session->setFlash("Attendance has been sent successfully.", 'layout_success');
    $this->set('success','1');
}
}

    /**
    * @Date: 13-sept-2016
    * @Method : monthlyattendance
    * @Purpose: This function is to fetch attendance report for particluar month only.
    * @Param: none
    * @Return: none
    **/
    
    function monthlyattendance($id = null,$month = null,$year = null) {

        $this->viewBuilder()->layout(false);



        $resultAttendance = $this->Attendances->find('all',[
            'conditions' => ['MONTH(date)'=>$month,'YEAR(date)'=>$year,'user_id'=>$id],
            'order' => 'date DESC'
            ])->toArray();

        $this->set('report', $resultAttendance);

    }


    /**
    * @Date: 13-sept-2016
    * @Method : leavereport(user wise)
    * @Purpose: This function is to show user wise leave report .
    * @Param: none
    * @Return: none
    **/

    function leavereport(){
       $this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session->read("SESSION_ADMIN");
       if(isset($this->request)){ 
        $selectedUser=$this->request->data['user_id'];
        $start_date=$this->request->data['start_date'];
        $end_date=$this->request->data['end_date'];
        $dateRange=$this->request->data['select'];
        $current=date('Y-m-d');
    }else{
      $selectedUser="";
      $start_date="";
      $end_date="";
      $dateRange="";
      $current="";
  }
  $this->set('fromDate',$start_date);
  $this->set('toDate',$end_date);
  $this->loadComponent('Common');
  $options = $this->Common->getDateRange();

  $this->set('options', $options);
  $last_day=date("Y/m/d", mktime(0, 0, 0, date("m"), 0, date("Y")));
  $first_day=date("Y/m/d", mktime(0, 0, 0, date("m")-1, 1, date("Y")));
  if(empty($selectedUser) && empty($start_date)&&empty ($end_date)){
 $query=TableRegistry::get('Attendances');
    $data=$query->find();
     $data->select(['Attendances.status','Users.first_name','Users.last_name','Users.status','NOofStatus' => $data->func()->count('Attendances.status')])  //count as status a
     ->where([
        'Attendances.date >= '=> $first_day,'Attendances.date <='=>$last_day,'Users.status'=>1,
        'Attendances.status IN'=>['leave','halfday','short leave'],'Attendances.agency_id' => $userSession[3]
        ])
     ->group('Attendances.user_id','Attendances.status')
     ->join([
      'table' => 'users',
      'alias' => 'Users',
      'type'=>'INNER',
      'foreignKey' => false,
      'conditions' => array ('Attendances.user_id = Users.id')
      ]);


 }elseif(empty($selectedUser) && !empty($start_date)&& !empty($end_date))
 {


    $query=TableRegistry::get('Attendances');
    $data=$query->find();
    $data->select(['Attendances.status','Users.first_name','Users.last_name','Users.status','NOofStatus' => $data->func()->count('Attendances.id')])
    ->where(['Attendances.date >= '=> $start_date,'Attendances.date <='=>$end_date,
       'Attendances.status IN'=>['leave','halfday','short leave'],'Attendances.agency_id' => $userSession[3]]) 
    ->group('Attendances.user_id')
    ->join([
      'table' => 'users',
      'alias' => 'Users',
      'type'=>'INNER',
      'foreignKey' => false,
      'conditions' => array ('Attendances.user_id = Users.id')
      ]);
}
elseif(!empty($selectedUser) && $dateRange!='select')
{   


  $query=TableRegistry::get('Attendances');
  $data=$query->find();
  $data->select(['Attendances.status','Users.first_name','Users.last_name','Users.status','NOofStatus' => $data->func()->count('Attendances.status')])  //count as status a
  ->where(['Attendances.user_id'=>$selectedUser, 'Attendances.status IN'=>['leave','halfday','short leave'],'Attendances.agency_id' => $userSession[3]]) 
  ->group('Attendances.status')
  ->join([
      'table' => 'users',
      'alias' => 'Users',
      'type'=>'INNER',
      'foreignKey' => false,
      'conditions' => ('Attendances.user_id = Users.id')
      ]);


}
            else if(!empty($selectedUser) && $start_date==$current)//user select date not
            {

                $query=TableRegistry::get('Attendances');
                $data=$query->find();
  $data->select(['Attendances.status','Users.first_name','Users.last_name','Users.status','NOofStatus' => $data->func()->count('Attendances.status')])  //count as status a
  ->where(['Users.status'=>1, 'Attendances.status IN'=>['leave','halfday','short leave'],'Attendances.agency_id' => $userSession[3]]) 

  ->group('Attendances.status')
  ->join([
      'table' => 'users',
      'alias' => 'Users',
      'type'=>'INNER',
      'foreignKey' => false,
      'conditions' => array ('Attendances.user_id = Users.id')
      ]);

}
else
{

   $query=TableRegistry::get('Attendances');
   $data=$query->find();
  $data->select(['Attendances.status','Users.first_name','Users.last_name','Users.status','NOofStatus' => $data->func()->count('Attendances.status')])  //count as status a
  ->where([  'Attendances.date >= '=> $start_date,'Attendances.date <='=>$end_date,'Users.id'=>$selectedUser, 'Attendances.status IN'=>['leave','halfday','short leave'],'Attendances.agency_id' => $userSession[3]]) 
  ->group('Attendances.status')

  ->join([
      'table' => 'users',
      'alias' => 'Users',
      'type'=>'INNER',
      'foreignKey' => false,
      'conditions' => array ('Attendances.user_id = Users.id')
      ]);
}

$this->set('resultData',$data->toArray()); 

}
}
