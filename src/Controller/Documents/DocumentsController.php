<?php

namespace App\Controller\Documents;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Inflector;
use Cake\Auth\DefaultPasswordHasher;

class DocumentsController extends AppController {

    
    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : beforeFilter
     * @Purpose: This function is called before any other function.
     * @Param: none
     * @Return: none 
     * */
    function beforeFilter(Event $event) {
      parent::beforeFilter($event);
     // pr($this->request); die;
      $this->response->cors($this->request)
    ->allowOrigin(['*'])
    ->allowMethods(['GET', 'POST'])/*
    ->allowHeaders(['X-CSRF-Token'])
    ->allowCredentials()
    ->exposeHeaders(['Link'])*/
    ->maxAge(300)
    ->build();
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     /*NOTIFICATION CHECK*/
    $controller=$this->request->params['controller'];
    $action=$this->request->params['action'];
    $permissions = TableRegistry::get('Permissions');
    $data  = $permissions->find("all" , [
          'conditions' => ['controller' => strtolower($controller), 'action' => strtolower($action)]
    ])->first();
    $is_notified = (isset($data)) ? $data['is_notified'] : 'No';
    $this->set('is_notified',$is_notified);
     /*NOTIFICATION CHECK*/
     if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }

    if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {
     
     
        $this->checkUserSession();
    } else {
        $this->viewBuilder()->layout('new_layout_admin');
        
    }
    $this->set('common', $this->Common);
}

 /**
     * @Date: 26-sep-2016  
     * @Method : initialize
     * @Purpose: This function is called initialize  function.
     * @Param: none
     * @Return: none 
     * */

 public function initialize()
 {
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Agencies');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Documents');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Notes');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $this->loadComponent('Upload');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }



    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : index
     * @Purpose: This page will render home page
     * @Param: none
     * @Return: none 
     * */
    function index() {
        $this->set("title_for_layout", "ERP : Vlogic Labs - Relying on Commitment");
    }

    public function add(){
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
            $agencyid = $userSession[3];
            $today = date("Y-m-d H:i:s");
            $docs = $this->Documents->find('all',[
                'conditions' => ['Documents.agency_id' => $agencyid],
                'order' => ['Documents.created desc'],
                ])->contain(['Modifier','Creater']);
            if($this->request->is('ajax') == true){
                echo json_encode(['status' => 'success', 'docs' => $docs]);
                die;
            }
            $this->set('docs',$docs);
    }
    public function addescription(){
        //pr($this->Common->DCR($this->request->data['pk'] )) ; die;
         $session = $this->request->session();
         $userSession = $session->read("SESSION_ADMIN");
        if($this->request->is('ajax') == true){
            $this->Documents->updateAll(['description' => $this->request->data['value'],'last_modifiedby' => $userSession[0] ],['id' => $this->Common->DCR($this->request->data['pk']) ]);
            echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
                die;
            }    
    }

    public function addnotedes(){
         $session = $this->request->session();
         $userSession = $session->read("SESSION_ADMIN");
        //pr($this->Common->DCR($this->request->data['pk'] )) ; die;
        if($this->request->is('ajax') == true){
            $this->Notes->updateAll(['description' => $this->request->data['value'] ,'last_modifiedby' => $userSession[0] ],['id' => $this->Common->DCR($this->request->data['pk']) ]);
            echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
                die;
            }    
    }

    
    public function adddoc(){
            //echo "adddoc"; die;
        //pr($this->request->data()); die;
        if($this->request->data()){
                $this->request->data['attached_to'] = $this->Common->DCR($this->request->data['attached_to']);
                //pr($this->request->data()); die;
                $projects = $this->Documents->newEntity($this->request->data(),['validate' => false ]);
                if($this->Documents->save($projects)){
                     echo json_encode(['status' => 'success']);
                     die;
                }
                echo json_encode(['status' => 'failed']);
                die;
        }
    }

    public function addnote(){
           if($this->request->data()){
                // pr($this->request->data); 
                $session = $this->request->session();
                $userSession = $session->read("SESSION_ADMIN");
                $agencyid = $userSession[3];
                $today = date("Y-m-d H:i:s");
                $attached_to = $this->Common->DCR($this->Common->dcrForUrl($this->request->data['attached_to']) );
                $this->request->data['userid'] = $userSession[0];
                $this->request->data['agency_id'] = $agencyid;
                $this->request->data['last_modifiedby'] = $userSession[0];
                $this->request->data['attached_to'] =   $attached_to; 
                $projects = $this->Notes->newEntity($this->request->data(),['validate' => false ]);
                if($this->Notes->save($projects)){
                     echo json_encode(['status' => 'success']);
                     die;
                }
                echo json_encode(['status' => 'failed']);
                die;
        } 
    }
 public function getnote(){
     if($this->request->is('ajax') == true){
        $note = $this->Notes->get($this->Common->DCR($this->request->data['origin']));
        if(count($note) > 0){
            $note['id'] = $this->Common->ENC($note['id']);
             echo json_encode(['status' => 'success','note' => $note]);
             die;
        }
        echo json_encode(['status' => 'failed','note' => 'No Record Found']);
        die;
     }
 }
  public function editnote(){
     if($this->request->is('ajax') == true){
        //pr($this->request->data); die;
                 $session = $this->request->session();
                $userSession = $session->read("SESSION_ADMIN");
                $agencyid = $userSession[3];
                $today = date("Y-m-d H:i:s");
                $attached_to = $this->Common->DCR($this->Common->dcrForUrl($this->request->data['attached_to']) );
                $this->request->data['userid'] = $userSession[0];
                $this->request->data['agency_id'] = $agencyid;
                $this->request->data['last_modifiedby'] = $userSession[0];
                $this->request->data['attached_to'] =   $attached_to; 
               // pr($this->Common->DCR($this->request->data['origin']) ); die;
            $note = $this->Notes->get($this->Common->DCR($this->request->data['origin']));

             $notes = $this->Notes->patchEntity($note,$this->request->data);
        if(empty($notes->errors()) ){
             $this->Notes->save($note);
             echo json_encode(['status' => 'success']);
             die;
        }
        echo json_encode(['status' => 'failed','note' => 'No Record Found']);
        die;
     }
 }
      
public function delnote(){
    if($this->request->is('ajax') == true){
        //pr( $this->Common->DCR($this->request->data['docid'])); die;
        $this->Notes->deleteAll(['id' => $this->Common->DCR($this->request->data['docid']) ] );
           /* $entity = $this->Notes->get($this->Common->DCR($this->request->data['docid']));
            $result = $this->Notes->delete($entity);*/
         echo json_encode(['status' => 'success']);
        die;
    }
}

} //end