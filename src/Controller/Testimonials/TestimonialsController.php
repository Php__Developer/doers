<?php 


namespace App\Controller\Testimonials;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Routing\Router;


class TestimonialsController extends AppController {

  
    var $paginate		   =  array(); //pagination
    
    var $uses       	 =  array('Testimonial'); // For Default Model
    

    /******************************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

   function beforeFilter(Event $event){ // This  function is called first before parsing this controller file
     parent::beforeFilter($event);
        // star to check permission
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     $controller=$this->request->params['controller'];
     $action=$this->request->params['action'];
     $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
     if($permission!=1){
      $this->Flash->error("You have no permisson to access this Page.");

      return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    // end code to check permissions
    if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){

      $this->checkUserSession();
    }else{

     $this->viewBuilder()->layout('layout_admin');
     
   }
   Router::parseNamedParams($this->request);
   $session = $this->request->session();
   $userSession = $session->read("SESSION_ADMIN");
   if($userSession==''){
    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
  }
  $this->set('common',$this->Common); 

}

/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : initialize
    * @Purpose: This function is called initialize function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
  parent::initialize();
        $this->loadModel('Tickets'); // for importing Model
        
        $this->loadModel('Projects'); // for importing Model
        
        $this->loadModel('Users');
        
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Testimonials');
        $this->loadModel('Credentials');

      }

    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : index
    * @Purpose: This function is called index function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function index() {
      $this->render('login');
      if( $session->read("SESSION_USER") != ""){      
        $this->redirect('dashboard');                        
      }    
    }

    

	/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : testimonialslist
    * @Purpose: This function is called testimonialslist function.display testimonials data.
    * @Param: none                                                
    * @Return: none                                               
    **/
  function testimonialslist(){
     $this->viewBuilder()->layout('new_layout_admin');
 $session = $this->request->session();
    $this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];

    $this->set('title_for_layout','Testimonials Listing');      
    $this->set("pageTitle","Testimonials Listing");             
    $this->set("search1", "");                                 
    $this->set("search2", "");                                 
      $criteria = ['Testimonials.agency_id'=>$userAgency]; //All Searching
      $session->delete('SESSION_SEARCH');
      
      if(isset($this->request->data['Testimonial']) || !empty($this->request->query)) {    
        
        if(!empty($this->request->data['Testimonial']['fieldName']) || isset($this->request->query['field'])){                                                                
          if(trim(isset($this->request->data['Testimonial']['fieldName'])) != ""){                
            $search1 = trim($this->request->data['Testimonial']['fieldName']);   
            
          }elseif(isset($this->request->query['field'])){                              
           $search1 = trim($this->request->query['field']);
           
         }
         
         $this->set("search1",$search1);                                               
       }
       
       if(isset($this->request->data['Testimonial']['value1']) || isset($this->request->data['Testimonial']['value2']) || isset($this->request->query['value'])){                  
        if(isset($this->request->data['Testimonial']['value1']) || isset($this->request->data['Testimonial']['value2'])){                                                            
          $search2 = ($this->request->data['Testimonial']['fieldName'] != "Testimonial.status")?trim($this->request->data['Testimonial']['value1']):$this->request->data['Testimonial']['value2'];
          
        }elseif(isset($this->request->query['value'])){         
          $search2 = trim($this->request->query['value']);     
        }                                                      
        $this->set("search2",$search2);                       
      }
      

      /* Searching starts from here */                                               
      if(!empty($search1) && (!empty($search2) || $search1 == "Testimonial.status")){      
        $query  =    $this->Testimonials->find('all' , [
          'conditions' => [$search1 .' Like' =>'%'.$search2.'%','Testimonials.agency_id'=>$userAgency],
          ]);
        
        $result = isset($query)? $query->toArray():array();
        $criteria = [$search1." LIKE '%".$search2."%'",'Testimonials.agency_id'=>$userAgency]; 
        $session->write('SESSION_SEARCH', $criteria);
      }else{  
        $query  =    $this->Testimonials->find('all' , [
          'conditions' => ['Testimonials.agency_id'=>$userAgency],
          ]);
        
       
       $this->set("search1","");      
       $this->set("search2","");      
     }
     
   }
   $urlString = "/";
   if(isset($this->request->query)){                   
     $completeUrl  = array();
     
     if(!empty($this->request->query['page']))      
      $completeUrl['page'] = $this->request->query['page'];

    if(!empty($this->request->query['sort']))
      $completeUrl['sort'] = $this->request->query['sort'];   
    
    if (!empty($this->request->query['direction']))        
      $completeUrl['direction'] = $this->request->query['direction']; 
    
    if(!empty($search1))                                        
      $completeUrl['field'] = $search1;
    
    if(isset($search2))                                         
      $completeUrl['value'] = $search2;
    foreach($completeUrl as $key => $value) {                      
      $urlString.= $key.":".$value."/";                           
    }

  }
  $this->set('urlString', $urlString);
  $urlString = "/";  
  if(isset($this->request->query)){
    $completeUrl  = array();  
    if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
        $saveString = implode("','",$this->params['form']['IDs']);      
      }
    }
  }
  $this->set('urlString', $urlString);
  if(isset($query)){
  $query=$query;
}else{
   $query  =    $this->Testimonials->find('all' , [
          'conditions' => ['Testimonials.agency_id'=>$userAgency],
          ]);
 
        
}


$paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/100);
$this->set('paginatecount',$paginate);

  $this->paginate = [                                        
     
  'page'=> 1,'limit' => 100,      
  'order' => ['id' => 'desc'],
  'paramType' => 'querystring'      
  ];
  $data = $this->paginate('Testimonials',['conditions' => $criteria]); 



foreach($data as $d){      
  $d['id'] = $d->id; 
    $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));   
   $arr[] = $d; 
  }

  $this->set('resultData', $arr);
  if($this->request->is('ajax')){
          if(isset($this->request->query['page'])){
             $pageing=$this->request->query['page'];
          }else{
            $pageing="";
          }
        echo json_encode(['resultData'=>$arr,'page'=>$pageing,'paginatecount'=>$paginate]);
         die;
      }

}                                                   
/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : add
    * @Purpose: This function is called add function.
    * @Param: none                                                
    * @Return: none                                               
    **/




    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : add
    * @Purpose: This function is called add function.
    * @Param: none                                                
    * @Return: none                                               
    **/

function add() {     
$this->viewBuilder()->layout('new_layout_admin');  
$this->set('title_for_layout','Add Testimonial'); 
 $this->pageTitle = "Add Testimonial";
 $this->set("pageTitle","Add Testimonial");
$session = $this->request->session();
    $this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];
 $mode = "add";

 if($this->request->data){
  $sessionAdmin = $session->read("SESSION_ADMIN");
  $user_id = $sessionAdmin['0'];
  $this->request->data['user_id'] = $user_id;
  $this->request->data['agency_id'] = $userAgency;
  $testimonials = $this->Testimonials->newEntity($this->request->data());
  if(empty($testimonials->errors())){
    $this->Testimonials->save($testimonials);
    $this->Flash->success_new("Testimonial has been created successfully.");
    return $this->redirect( ['action' => 'testimonialslist']);
  }else{
    $this->set("errors", $testimonials->errors());
  }
}     
}


/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : edit
    * @Purpose: This function is called edit function.
    * @Param: none                                                
    * @Return: none                                               
    **/
function edit($id = null) {
$this->viewBuilder()->layout('new_layout_admin'); 
  $this->set('title_for_layout','Edit Testimonial');     
  $this->pageTitle = "Edit Testimonial";    
  $this->set("pageTitle","Edit Testimonial");   
  if($this->request->data){ 
    $testimonial = $this->Testimonials->get($this->request->data['id']);
    $testimonials = $this->Testimonials->patchEntity($testimonial,$this->request->data());
    if(empty($testimonials->errors())) {
      $this->Testimonials->save($testimonial);
      $this->Flash->success_new("Testimonial has been updated successfully.");
      $this->redirect(array('action' => 'testimonialslist')); 

    }else{                                    
     $this->set("errors", $testimonials->errors());
   } 
 }else if(!empty($id)){ 
  $TestimonialsData = $this->Testimonials->get($id);
  $this->set('TestimonialsData',$TestimonialsData); 
  if(!$TestimonialsData){             
    return $this->redirect( ['action' => 'testimonialslist']);  

  }                                                      
}                                                          
}    
    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : delete
    * @Purpose: This function is called delete function.
    * @Param: none                                                
    * @Return: none                                               
    **/

      function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
       $project = $this->Testimonials->get($id);
      if ($this->Testimonials->delete($project)) {
                echo json_encode(['status'=> 'success','page'=>'Testimonials']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 
  

 /**
   * @method      : download
   * @description : Used to download credentials record
   * @param       : type
   * return       : none
 **/
 function download($type = null)
 {   
  $session = $this->request->session();
  $this->set('session',$session);
  
  if(empty($this->request->query['pass'][0])){
    $search = $session->read('SESSION_SEARCH');
    
	   $crData = $this->Testimonials->find('all', ['conditions'=>$search]); //fetching all data
   }else{
    $crData = $this->Testimonials->find('all' ,['conditions'=>array('type'=>$this->request->query['pass'][0]),'order'=>'author']); //fetching all related data                                       
  }	
    $this->set('crData',$crData); //Setting Testimonial Data                             
    $this->viewBuilder()->layout = 'pdf'; 
   $this->set("title_for_layout","Testimonial Report PDF");//this will use the pdf.ctp layout
   $this->render();	
	}	// end of download function
/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : elance
    * @Purpose: This function is called elance function.
    * @Param: none                                                
    * @Return: none                                               
    **/
function elance($type = null)
{   
  $session = $this->request->session();
  $this->set('session',$session);  
  if(empty($this->request->query['pass'][0])){
    $search = $session->read('SESSION_SEARCH');
    
    $crData = $this->Testimonials->find('all', [
      'conditions' =>['type' =>'elance']
      ]); //fetching all data
  }else{
    $crData = $this->Testimonials->find('all' ,['conditions'=>array('type'=>$this->request->query['pass'][0]),'order'=>'author']); //fetching all related data                                        
  } 
    $this->set('crData',$crData); //Setting Testimonial Data                             
    $this->viewBuilder()->layout = 'pdf'; 
   $this->set("title_for_layout","Testimonial Report PDF");//this will use the pdf.ctp layout
   $this->render();  
  } // end of download function


  /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : other
    * @Purpose: This function is called other function.
    * @Param: none                                                
    * @Return: none                                               
    **/
  function other($type = null)
  {   
    $session = $this->request->session();
    $this->set('session',$session);   
    if(empty($this->request->query['pass'][0])){
      $search = $session->read('SESSION_SEARCH');
      
      $crData = $this->Testimonials->find('all', [
        'conditions' =>['type' =>'other']
      ]); //fetching all data
    }else{
    $crData = $this->Testimonials->find('all' ,['conditions'=>array('type'=>$this->request->query['pass'][0]),'order'=>'author']); //fetching all related data                                        
  } 
    $this->set('crData',$crData); //Setting Testimonial Data                             
    $this->viewBuilder()->layout = 'pdf'; 
   $this->set("title_for_layout","Testimonial Report PDF");//this will use the pdf.ctp layout
   $this->render();  
  } // end of download function

  
   #_________________________________________________________________________#


    /**
    * @Date: 23-Apr-2012
    * @Method : exportciodesk
    * @Purpose: This function is used to show Testimonial Report in Excel format
    * @Param: none
    * @Return: none
    **/
    function exportciodesk($type = null){

     $this->viewBuilder()->layout(false);
     if (empty($this->params['pass'][0])) {
      
      $session = $this->request->session();
      $this->set('session',$session);
      $search = $session->read("SESSION_ADMIN");

      $crData = $this->Testimonials->find('all', [
        'conditions' =>[$search[0] ]
        ]);          
    } else {
            $crData = $this->Testimonials->find('all', ['conditions' => ['type' => $this->request->query['pass'][0] ], 'order' => 'username']); //fetching all related data 
            $this->response->download("export.xls");                         
          }
        //setting excel parametres
          $this->set('filename', "TestimonialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('exportciodesk_xls');
      }
    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : exportcielance
    * @Purpose: This function is called exportcielance function.
    * @Param: none                                                
    * @Return: none                                               
    **/
    function exportcielance($type = null){

      $this->viewBuilder()->layout(false);
      if (empty($this->params['pass'][0])) {
       
        $session = $this->request->session();
        $this->set('session',$session);
        $search = $session->read("SESSION_ADMIN");

        $crData = $this->Testimonials->find('all', [
          'conditions' =>['type' =>'elance']
          ]);    
      } else {
            $crData = $this->Testimonials->find('all', ['conditions' => ['type' => 'elance'], 'order' => 'username']); //fetching all related data 
            $this->response->download("exportcielance.xls");                                   
          }
          
          $this->set('filename', "TestimonialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('exportcielance_xls');
      }

    /**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : exportciother
    * @Purpose: This function is called exportciother function.
    * @Param: none                                                
    * @Return: none                                               
    **/
    function exportciother($type = null){

      $this->viewBuilder()->layout(false);
      if (empty($this->params['pass'][0])) {
        
        $session = $this->request->session();
        $this->set('session',$session);
        $search = $session->read("SESSION_ADMIN");

        $crData = $this->Testimonials->find('all', [
          'conditions' =>['type' =>'other']
          ]); 
      } else {
            $crData = $this->Testimonials->find('all', ['conditions' => ['type' => 'other'], 'order' => 'username']); //fetching all related data 
            $this->response->download("exportciother.xls");                                        
          }
        //setting excel parametres
          $this->set('filename', "TestimonialReport_" . date("Ymd"));
        $this->set('crData', $crData->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('exportciother_xls');
      }

/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : details
    * @Purpose: This function is called details function.
    * @Param: none                                                
    * @Return: none                                               
    **/
function details($id=null) {

  $this->viewBuilder()->layout="layout_inner";	
  $this->set("title_for_layout","VLL Software");
  $conditions = array(
    
   'fields' => array(                                              
    'id',           
    'author', 
    'type', 
    'keyword', 	  
    'description'        
    ),      
   
   'order' => array('id' => 'desc')
   );
  
  $data = $this->Testimonial->find("all",$conditions);
  $this->set('resultData', $data);
  
}
/**                                                           
    * @Date: 20-sep-2016                                         
    * @Method : changeStatus
    * @Purpose: This function is called changeStatus function.
    * @Param: none                                                
    * @Return: none                                               
    **/
function changeStatus() {
  if (isset($this->request->data['publish'])) {
    $updateCredentials = 0;
    $setValue = 1;
    $messageStr = "Selected testimonials(s) have been activated.";
  } elseif (isset($this->request->data['unpublish'])) {
    $updateCredentials = 1;
    $setValue = 0;
    $messageStr = "Selected testimonials(s) have been deactivated.";
  }
  if (isset($this->request->data['publish']) || isset($this->request->data['unpublish'])) {
    if (isset($this->request->data['IDs'])) {
      $saveString = implode("','", $this->request->data['IDs']);
    }
    if ($updateCredentials == 1) {
      $credentials = TableRegistry::get('Credentials');
      $update = ['user_id' => 0 , 'status' => 0];
      $credentials->updateAll($update,['user_id IN' => $this->request->data['IDs']]);
    }
    if ($saveString != "") {
     $users = TableRegistry::get('Testimonials');
     $update = ['status' => $setValue];
     $users->updateAll($update,['id IN' => $this->request->data['IDs']]);
     $this->Flash->success_new($messageStr);
     $this->setAction('testimonialslist');
   }
 }
}

    function status(){
      if($this->request->is('ajax')) {
       $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $updateCredentials = 1;
      $setValue = 0;
      $messageStr = 'deactivate';
      }
      if ($updateCredentials == 1) {
        $credentials = TableRegistry::get('Credentials');
        $update = ['user_id' => 0 , 'status' => 0];
        $credentials->updateAll($update,['user_id IN' => $id]);
      }
     $users = TableRegistry::get('Testimonials');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;

      }
     $this->setAction('testimonialslist'); 
   }

}// last 
