<?php 

namespace App\Controller\Contacts;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Routing\Router;

class ContactsController extends AppController { 
	var $name       	=  "Contacts";   
     // Controller Name    
	
    /********************** START FUNCTIONS **************************/
     /**
     * @Date: Nov,2016  
     * @Method : beforeFilter    
     * @Purpose: This function is called before any other function.    
     * @Param: none   
     * @Return: none    
     * */
	function beforeFilter(Event $event){ // This  function is called first before parsing this controller file
       parent::beforeFilter($event);
       	// star to check permission
       $session = $this->request->session();
       $userSession = $session->read("SESSION_ADMIN");
       $controller=$this->request->params['controller'];
       $action=$this->request->params['action'];
       $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
       if($permission!=1){
         $this->Flash->error("You have no permisson to access this Page.");
         return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
     }
    // end code to check permissions
     if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){
         $this->checkUserSession();
     }else{
         $this->viewBuilder()->layout('layout_admin');

     }
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common',$this->Common); 

}

 /**
     * @Date: Nov,2016     
     * @Method : initialize    
     * @Purpose: This function is called initialize  function.    
     * @Param: none   
     * @Return: none    
     * */

 public function initialize()
 {
    parent::initialize();
        $this->loadModel('Tickets'); // for importing Model
        // for importing Model
        $this->loadModel('Projects'); // for importing Model
       // for importing Model
        $this->loadModel('Users');

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Testimonials');
        $this->loadModel('Contacts');

    }

	#_________________________________________________________________________#   
	/**        
	* @Date: Nov,2016     
	* @Method : index         
	* @Purpose: This function is the default function of the controller  
	* @Param: none                                     
	* @Return: none                                          
	**/    
	function index() {
     $this->render('login'); 
     if($session->read("SESSION_USER") != ""){      
      $this->redirect('dashboard');             
  }    
}

	#_________________________________________________________________________#  
	/**                       
	* @Date: Nov,2016        
	* @Method : contactslist      
    * @Purpose: This function is to show list of Credentials in the System   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  

	function contactslist(){   
$this->viewBuilder()->layout('new_layout_admin');
		$session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");
       $this->set('session',$session);  
       $this->set('title_for_layout','Clients Listing');      
       $this->set("pageTitle","Clients Listing");     
       $this->set("search1", "");                           
       $this->set("search2", "");                                 
		$criteria = ['agency_id' => $userSession[3]]; //All Searching	  
		$session->delete('SESSION_SEARCH');	  
        if(isset($this->request->data['Contact']) || !empty($this->request->query)) {  
          if(!empty($this->request->data['Contact']['fieldName']) || isset($this->request->query['field'])){        
              if(trim(isset($this->request->data['Contact']['fieldName'])) != ""){			
                 $search1 = trim($this->request->data['Contact']['fieldName']);    
             }elseif(isset($this->request->query['field'])){		
                $search1 = trim($this->request->query['field']);		
            }
            $this->set("search1",$search1);      
        }		
        if(isset($this->request->data['Contact']['value1']) || isset($this->request->data['Contact']['value2']) || isset($this->request->query['value'])){	
            if(isset($this->request->data['Contact']['value1']) || isset($this->request->data['Contact']['value2'])){	
                $search2 = ($this->request->data['Contact']['fieldName'] != "Contact.status")?trim($this->request->data['Contact']['value1']):$$this->request->data['Contact']['value2'];	
            }elseif(isset($this->request->query['value'])){	
              $search2 = trim($this->request->query['value']);	
          }		
          $this->set("search2",$search2);
      }		

      /* Searching starts from here */		
      if(!empty($search1) && (!empty($search2) || $search1 == "Contact.status")){	
        $query  =    $this->Contacts->find('all' , [
          'conditions' => [
                            $search1 .' Like' =>'%'.$search2.'%' ,'agency_id' => $userSession[3] ,
                            'agency_id' => $userSession[3]
                          ],
          ]);

        $result = isset($query)? $query->toArray():array();
        $criteria = $search1." LIKE '%".$search2."%'"; 		
        $session->write('SESSION_SEARCH', $criteria);
    }else{	
        $query  =  $this->Contacts->find("all",[
            'conditions' => ['agency_id' => $userSession[3]],
          ]); 		
        $this->set("search1","");      
        $this->set("search2","");      
    }

} else {
    $query  =  $this->Contacts->find("all",[
            'conditions' => ['agency_id' => $userSession[3]],
          ]);     
        $this->set("search1","");      
        $this->set("search2","");      
}
    $urlString = "/";
    if(isset($this->request->query)){                   
     $completeUrl  = array();
     if(!empty($this->request->query['page']))
        $completeUrl['page'] = $this->request->query['page'];
    if(!empty($this->request->query['sort']))	
     $completeUrl['sort'] = $this->request->query['sort']; 
    if (!empty($this->request->query['direction']))        
      $completeUrl['direction'] = $this->request->query['direction']; 	
    if(isset($search2))                                         
      $completeUrl['value'] = $search2;
             // pr($search2);
    foreach($completeUrl as $key => $value) {                      
      $urlString.= $key.":".$value."/";                           
    }
    }
    $this->set('urlString', $urlString); 
    $urlString = "/";  
    if(isset($this->request->query)){
        $completeUrl  = array();  
        if(!empty($setValue)){                            
          if(isset($this->params['form']['IDs'])){          
              $saveString = implode("','",$this->params['form']['IDs']);      
          }
      }
    }

    /*$this->set('urlString', $urlString);      
    $this->paginate = [           
    'fields' => [	
    'id',	
    'name',	
    'primaryemail',	
    'primaryphone',	
    'skype',
    'othercontact',	
    'source',		
    'notes',
    'cpassword',
    'type',	
    'status',
    'created'	
    ],     
    'page'=> 1,'limit' => 100,     
    'order' => ['id' => 'desc']          
    ];     
    $data = $this->paginate('Contacts',['conditions' => $criteria]); */

    $this->set('urlString',$urlString);
  $data = $this->paginate($query,[
    'page' => 1, 'limit' => 100,
    'order' => ['id' => 'desc'],
    'paramType' => 'querystring'
    ]);
  $this->set('resultData', $data);

    foreach($data as $d){      
      $d['id'] = $d->id; 
      $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));   
      //print_r(explode(',', $d['project_id'])); die;
      $projects_ids = $this->Projects->find("list", [
                              'keyField' => 'id',
                              'valueField' => 'id',

                             'conditions' => ['client_id' =>  $d['id'] , 'agency_id' => $userSession[3]],

                             'fields' => ['id','id']

                            ])->toArray();


      $d['projects'] = $this->Projects->find('all',[
                      'conditions' => [ 'id IN' =>  array_unique(array_merge($projects_ids, explode(',', $d['project_id']))) , 'agency_id' => $userSession[3] ],
                      'fields' => ['project_name']
        ]);

       /*= $this->Projects->find('all',[
                      'conditions' => [ 'client_id' =>  $d['id'] , 'agency_id' => $userSession[3]],
                      'fields' => ['project_name']
        ]);*/
       
       $arr[] = $d; 
      }

    $this->set('resultData', $arr);
    $this->set('pagename', "contactlist");  	
}


	#_________________________________________________________________________#  


	/**  
		* @Date: Nov,2016   
		* @Method : add    
		* @Purpose: This function is to add contacts from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
       **/  

       function add() {	
       $this->viewBuilder()->layout('new_layout_admin');	
           $this->set('title_for_layout','Add Contact');		
           $this->pageTitle = "Add Contact";			
           $this->set("pageTitle","Add Contact");	
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
           $mode = "add";		
           if($this->request->data){	
            //print_r($this->request->data()); die;
            $this->request->data['agency_id'] = $userSession[3];
            $contactss = $this->Contacts->newEntity($this->request->data());
            if(empty($contactss->errors())){
             $this->Contacts->save($contactss);

             $this->Flash->success_new("Contacts has been created successfully.");
             $this->redirect(array('action' => 'contactslist'));   
             $resultJ = json_encode(['status' => 'success']);
         }else{
          $this->set("errors", $contactss->errors());
           $resultJ = json_encode(['status' => 'failed' , 'errors' => $contactss->errors() ,'reason' => 'validation'  ]); 
      }
      if($this->request->is('ajax')) {              
              $this->response->type('json');
              $this->response->body($resultJ);
              return $this->response;
          }
  }     
}
    function quickadd() {  
       $this->viewBuilder()->layout(false);  
           $this->set('title_for_layout','Add Contact');    
           $this->pageTitle = "Add Contact";      
           $this->set("pageTitle","Add Contact"); 
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
           $mode = "add";   
           if($this->request->data){  
            $this->request->data['agency_id'] = $userSession[3];
            $this->request->data['iscurrent'] = '1';
            $contactss = $this->Contacts->newEntity($this->request->data());
            if(empty($contactss->errors())){
             $this->Contacts->save($contactss);
             echo json_encode(['status' => 'success','id' => $this->Common->ENC($contactss->id), 'name' => $contactss->name]);
             die;
         }else{
          echo json_encode(['status' => 'failed']);
            die;
          //$this->set("errors", $contactss->errors());
      }
  }     
}  
                                                            

#_____________________________________________________________________________________________#  

	/**  
		* @Date: Nov,2016  
		* @Method : edit    
		* @Purpose: This function is to edit contacts from admin section. * @Param: none 
		* @Return: none  
		* @Return: none     
       **/  


        function edit($id = null) { 
          $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Edit Contact');     
            $this->pageTitle = "Edit Contact";    
            $this->set("pageTitle","Edit Contact"); 
            $this->set('id',$id);  
            if($this->request->data){ 
                $contact = $this->Contacts->get($this->request->data['id']);
                $contacts = $this->Contacts->patchEntity($contact,$this->request->data());
                if(empty($contacts->errors())) {
                  $this->Contacts->save($contact);
                  $this->Flash->success_new("Contact has been updated successfully.");
                  $this->redirect(array('action' => 'contactslist'));  
              }else{                                    
                 $this->set("errors", $contacts->errors());
             } 
         }else if(!empty($id)){ 
            $ContactsData = $this->Contacts->get($id);
            $this->set('ContactsData',$ContactsData); 
            if(!$ContactsData){             
              $this->redirect(array('action' => 'contactslist'));                 
          }                                                      
      }                                                          
  }    


		#_____________________________________________________________________________________________# 

		/**  
		* @Date: Nov,2016 
		* @Method : delete    
		* @Purpose: This function is to delete contacts from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
       **/  
function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
     $contact = $this->Contacts->get($id);
      if ($this->Contacts->delete($contact)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }


/**  
        * @Date: Nov,2016 
        * @Method : changeStatus    
        * @Purpose: This function is to changeStatus from admin section.   
        * @Param: none 
        * @Return: none  
        * @Return: none     
    **/  
function changeStatus() {
 

 if($this->request->is('ajax')) {
       $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $updateCredentials = 1;
      $setValue = 0;
      $messageStr = 'deactivate';
      }
      if ($updateCredentials == 1) {
        $credentials = TableRegistry::get('Credentials');
        $update = ['user_id' => 0 , 'status' => 0];
        $credentials->updateAll($update,['user_id IN' => $id]);
      }
     $users = TableRegistry::get('Contacts');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;

      }
     $this->setAction('contactslist'); 


}




	#_______________________________________________________________________________________#
	/**
		* @Date: Nov,2016 
		* @Method : clientlogin    
		* @Purpose: This function is used for client login.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/  
     function clientlogin(){
      $this->layout = "layout_client_withoutlogin";
      if($this->Session->read("SESSION_CLIENT") != ""){
       $this->redirect(array('controller'=>'contacts','action'=>'clientdashboard'));
   }
   if($this->data){

       $this->Contact->set($this->data['Contact']);
       $isValidated = $this->Contact->validates();
       if($isValidated){ 	
        $cpassword    = $this->data['Contact']['accesspass'];
        $condition = "cpassword='".$cpassword."'";
        $client_details = $this->Contact->find('first', array("conditions" => $condition, "fields" => array("id","name")));

        if(is_array($client_details) && count($client_details) > 0){
         $this->Session->write("SESSION_CLIENT", array($client_details['Contact']['id'],$client_details['Contact']['name']));
         $this->redirect(array('controller'=>'contacts','action'=>'clientdashboard'));
     }else{
         $this->Session->setFlash("<div class='error-message flash notice'>The access code you entered is incorrect.</div>");

     }
 }

}
}

	#____________________________________________________________________________________#
	/**
		* @Date: Nov,2016     
		* @Method : clientdashboard    
		* @Purpose: This function is used to show client dashborad.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/  
     function clientdashboard(){
      $this->layout = "layout_clientadmin";
      $this->set('title_for_layout','Dashboard');		
      $clientSession = $this->Session->read("SESSION_CLIENT"); 
      $client_id  = $clientSession[0];
      if($clientSession == ""){
       $this->redirect(array('controller'=>'contacts','action'=>'clientlogin'));
   }else{
       $projects = $this->Project->find('all', 
        array(
         'fields' => array('Project.id', 'Project.project_name','Project.modified','Project.status','Project.publicurl'),
         'conditions' => array('Project.publicurl'=>'1','Project.client_id'=>$client_id),
         'order' => array('Project.modified' => 'desc')	
         ));
   }
   $this->set('projectData',$projects);
}

	#____________________________________________________________________________________#
	/**
		* @Date: Nov,2016     
		* @Method : clientlogout    
		* @Purpose: This function is used to show client dashborad.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/
     function clientlogout(){
      $this->Session->delete("SESSION_CLIENT");
      $this->redirect(array('controller'=>'contacts','action' => 'clientlogin'));

  }

    public function getresults(){
      $session = $this->request->session();  
      $userSession = $session->read("SESSION_ADMIN");
      //print_r($_GET); die;
      if($this->request->data){
          $post = $this->request->data;
          if($post['type'] == 'saved_projects'){
             $exp = explode(',', $post['project_id']);
             $projects = $this->Projects->find('all',[
                  'conditions' => ['id IN' =>  $exp],
                  'fields' => ['id','project_name']
              ])->all();
             //print_r($projects); die;
                    $resultJ = json_encode(['status' => 'success' ,'projects' => $projects]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
          }
      } else {


         if($_GET){          
                    $post = $_GET;
                    //print_r($post); die;
                   $projects =  $this->Projects->find('all',[
                        'conditions' => [
                            'project_name LIKE' => '%'.$post['term'].'%' , 
                            'agency_id' => $userSession[3]
                        ],
                        'feilds' => ['id','project_name']
                      ])->all();
                   //print_r($projects); die;
                   $result = [];
                   if(count($projects) > 0){
                    foreach ($projects as $p) {
                        $result[] = ['Value' => $p['id'] ,'label' => $p['project_name'] ] ;
                    }
                   }

                    $resultJ = json_encode($result);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
             //}else { 
                //echo json_encode(['response'=> 'failed']); die;
              //}

      }
    }



      /*//dd($input);
      if($input['classname'] == '.tagswrapper'){
           $video = Tag::where('name', 'like', '%' . $input['term'] . '%')
                ->select('name as label', 'id as Value','thumbnail')
                ->where('is_deleted','No')
                 ->limit(10)->get();
              return response()->json($video);

      } else if($input['classname'] == '.candcwrapper'){
           $video = People::where('name', 'like', '%' . $input['term'] . '%')
                ->select('name as label', 'id as Value','thumbnail','name')
                ->where('is_deleted','No')
                 ->limit(10)->get();
            return response()->json($video);
      } else if($input['classname'] =='.ecmovieswrapper'){
        //dd("asdfasd"); 
            $video = Video::where('title', 'like', '%' . $input['term'] . '%')
                ->select('title as label', 'id as Value','thumbnail')
                 ->limit(10)->get();
            return response()->json($video);
      } */   

  }

}
