<?php
namespace App\Controller\Tickets;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Cache\Cache;
use Cake\Database\Connection;

class TicketsController extends AppController
{

#_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none
    * @Return: none
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
    	Router::parseNamedParams($this->request);

    	 $controller=$this->request->params['controller'];
		 $action=$this->request->params['action'];
		 $session = $this->request->session();
		 if($this->request->is('post') !== true){
		 	 $session->write("INTENDED", ['url' => $this->request->url]);	
		 }
		 
		  $permissions = TableRegistry::get('Permissions');
	       $data  = $permissions->find("all" , [
			          'conditions' => ['controller' => strtolower($controller), 'action' => strtolower($action)]
			])->first();
	       $is_notified = (isset($data)) ? $data['is_notified'] : 'No';
	       $this->set('is_notified',$is_notified);

    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('layout_admin');
            //$this->viewBuilder()->layout = "layout_front";
    	}

    	
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }
    	/**
	* @Date: 13-sept-2016
    * @Method : initialize
	* @Purpose: This function is called initialize function. it is like constructor
	* @Param: none
	* @Return: none
	**/


	public function initialize()
	{
		parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Payments');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Resumes');
        $this->loadModel('Entities');
        $this->loadModel('EntityAgency');
        $this->loadModel('Tags');
        $this->loadModel('TagAgency');
        $this->loadModel('Tickets');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }

    
	#_________________________________________________________________________#

    /**
    * @Date: 13-sept-2016
    * @Method : index
    * @Purpose: This is the default function of the administrator section for users
    * @Param: none
    * @Return: none
    **/

    function index($id = null, $key = null){
    	//pr($this->request->query); die;
    	if(!empty($this->request->query['key'])){
    		$key = $this->request->query['key'];
    	}
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$this->set('session_info',$session_info);
    	$userID = $session_info[0];


    	$this->viewBuilder()->layout('new_layout_admin');
    	$this->set("title_for_layout", "My Tickets");
    	
    	$condition_check = [];
	}
    
	
    /**
    * @Date: 13-sept-2016
    * @Method : index
    * @Purpose: This is the default function of the administrator section for users
    * @Param: none
    * @Return: none
    **/

    function index2($id = null, $key = null){
    	if(!empty($this->request->query['key'])){
    		$key = $this->request->query['key'];
    	}
    	$this->viewBuilder()->layout('new_layout_admin');
    	$this->set("title_for_layout", "My Tickets");
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
    	$condition_check = [];

	}

	/*===========================================================================*/

	  function getOrigin($id = null, $key = null){
    		if(isset($this->request->data['key'])){
    			$key = $this->request->data['key'];	
    		} else{
    			$key ="";
    		}
    	//pr($this->request->data); die;
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");

    	$currentuserroles = $session_info[2];

	if(isset($this->request->data['searchuserid']) && $this->request->data['searchuserid'] !=='none'){
		$userID =$this->request->data['searchuserid'];
	} else {
		$userID = $session_info[0];
	}
    	
    	$condition_check = [];
		if(isset($this->request->data['check_it']) && $this->request->data['check_it']=='1'){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			];
		}else {
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1,2]
			];
		}
		if(isset($this->request->data['tag_id']) && $this->request->data['tag_id'] !== 0){
    		$tagcond = ['Tickets.tag_id' => $this->request->data['tag_id']];
    	} else{
    		$tagcond = [];
    	}

		if(isset($this->request->data['searchkey']) && $this->request->data['searchkey'] !== 'none' && !empty($this->request->data['searchval'])){
			if($this->request->data['searchkey'] == 'to_name'){
				$searchcond = ['OR' => [ ['User_To.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ] , ['User_To.last_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ]] ,
									'from_id' => $userID
				              ] ;
			} else if($this->request->data['searchkey'] == 'from_name'){
				$searchcond = ['OR' => [ ['User_From.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ] , ['User_From.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ]] ,
									'to_id' => $userID
				              ] ;
			} else if($this->request->data['searchkey'] == 'title'){
				$searchcond = [
								'Tickets.Title Like' =>'%'.$this->request->data['searchval']['searchval'].'%'
				              ] ;
			} else if($this->request->data['searchkey'] == 'dates'){
				//pr("asdf"); die;
				$searchcond = ['deadline >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'deadline <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
			} else if($this->request->data['searchkey'] == 'created'){
				//pr("asdf"); die;
				$searchcond = ['Tickets.created >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'Tickets.created <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
			} else if($this->request->data['searchkey'] == 'modified'){
				//pr("asdf"); die;
				$searchcond = ['Tickets.modified >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'Tickets.modified <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
			}
			
    		
    	} else{
    		$searchcond = [];
    	}    	
    
	 $deadline =  $this->Tickets->find("all")
		->where([
		'OR' => [['from_id' => $userID], ['to_id' => $userID]],
		'status IN' => [1,2]
		])
	->order(['created' => 'DESC'])->first();
	
	isset($deadline)? $deadline->toArray():array();
	$oderby = ['Tickets.modified' => 'DESC'];
	$prioritycond = [];
	if(isset($this->request->data['filter']) && $this->request->data['filter'] !== 'none') {
		$result = array();
		if($this->request->data['filter'] == 'modified'){
			$oderby = ['Tickets.modified' => 'asc'];
		} else if($this->request->data['filter'] == 'created'){
			$oderby = ['Tickets.created' => 'asc'];
		} else if( $this->request->data['filter'] == 'created' ){
			$oderby = ['Tickets.deadline' => 'asc'];
		}
		
		if($this->request->data['filter'] == 'normal'){
			$prioritycond = ['Tickets.priority' => $this->request->data['filter'] ];
		} else if($this->request->data['filter'] == 'urgent'){
			$prioritycond = ['Tickets.priority' => $this->request->data['filter'] ] ;
		} else if($this->request->data['filter'] == 'medium'){
			$prioritycond = ['Tickets.priority' => $this->request->data['filter'] ];
		} else if($this->request->data['filter'] == 'emergency'){
			$prioritycond = ['Tickets.priority' => $this->request->data['filter'] ];
		}

		$oderby = ($this->request->data['filter'] == 'modified') ?  ['Tickets.modified' => 'asc'] : ($this->request->data['filter'] == 'created') ? ['Tickets.created' => 'asc'] :  ['Tickets.deadline' => 'asc'];
		if(!empty($key) && $this->request->data['check_it'] !== 1) {
		
			$today = date("Y-m-d 00:00:00");
			if($key == "open"){
				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [1],
				/*'AND' => ['deadline >' =>$today]*/
				];
			}elseif($key == "close"){

				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [0]
				] ;

			}elseif($key == "expire"){


				$cond =	[
				'OR' => [['from_id' => $userID], ['to_id' => $userID]],
				'Tickets.status IN' => [1,2],
				'AND' => ['Tickets.deadline <' =>$today]
				] ;
			}elseif($key == "inprogress"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [2]
			];
			} else if ($key == "scheduled"){
			$cond =	 [
						'OR' => [['from_id' => $userID], ['to_id' => $userID]],
						'type' => 'Scheduled'
					] ;
			} else if ($key == "mandatory"){
				$cond =	 		[
									'OR' => [['from_id' => $userID], ['to_id' => $userID]],
									'type' => 'Mandatory'
								] ;
			}
			$cond = array_merge($cond,$tagcond,$searchcond,$prioritycond);
		}
	}elseif(!empty($key)) {
		$today = date("Y-m-d 00:00:00");
		if($key == "open"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1],
			/*'AND' => ['deadline >' =>$today]*/
			] ;
		}elseif($key == "close"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [0]
			];
		}elseif($key == "expire"){
			$cond =	[
			'OR' => [['Tickets.from_id' => $userID], ['Tickets.to_id' => $userID]]/*,['deadline <' =>  $today]*/,
			'Tickets.status IN' => [1,2],
			"Tickets.deadline <" =>  $today
			] ;
		} elseif($key == "inprogress"){
			$cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [2]
			];
		} else if ($key == "scheduled"){
			$cond =	 [
						'OR' => [['from_id' => $userID], ['to_id' => $userID]],
						'type' => 'Scheduled'
					] ;
		} else if ($key == "mandatory"){
			$cond =	 		[
								'OR' => [['from_id' => $userID], ['to_id' => $userID]],
								'type' => 'Mandatory'
							] ;
			$cond = array_merge($cond,$tagcond,$searchcond,$prioritycond);
		}
	}

		if(!empty($this->request->data['ticket_id']) && $this->request->data['ticket_id'] != 'none'){
			$cond =['Tickets.id' => $this->request->data['ticket_id']];
		}
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session_info;
			$today = date("Y-m-d 00:00:00");
			
			$open_cond = [
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1],
			];
			/*[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 1
					];*/
				$inprogress_cond =	[
								'OR' => [['from_id' => $userID], ['to_id' => $userID]],
								'Tickets.status IN' => 2
								];
				$closed_cond =	[
								'OR' => [['from_id' => $userID], ['to_id' => $userID]],
								'Tickets.status IN' => 0
								];
				$expired_cond =	[
								'OR' => [['from_id' => $userID], ['to_id' => $userID]],['deadline' < '$today'],
								'Tickets.status IN' => [1,2],
								'AND' => ['deadline <' =>$today]
								] ;
				$schedule_cont = [
								'OR' => [['from_id' => $userID], ['to_id' => $userID]],
								'type' => 'Scheduled'
							] ;
				$mandatory_cont = [
							'OR' => [['from_id' => $userID], ['to_id' => $userID]],
							'type' => 'Mandatory'
						] ;
			$mandatory_cont = array_merge($mandatory_cont,$tagcond,$searchcond,$prioritycond);
			$open_cond = array_merge($open_cond,$tagcond,$searchcond,$prioritycond);
			$schedule_cont = array_merge($schedule_cont,$tagcond,$searchcond,$prioritycond);
			$expired_cond = array_merge($expired_cond,$tagcond,$searchcond,$prioritycond);
			$closed_cond = array_merge($closed_cond,$tagcond,$searchcond,$prioritycond);
			$inprogress_cond = array_merge($inprogress_cond,$tagcond,$searchcond,$prioritycond);
			// Tickets Related Code 
			/*$oderby = ['deadline' => 'asc'];*/
			$open = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($open_cond)->count();
			$inprogress = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($inprogress_cond)->count();
			$closed = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($closed_cond)->count();
			$expired = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($expired_cond)->count();
			$scheduled = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($schedule_cont)->count();
			$mandatory_count = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($mandatory_cont)->count();

			$this->set('open',$open);
			$this->set('inprogress',$inprogress);
			$this->set('closed',$closed);
			$this->set('scheduled',$scheduled);
			$this->set('mandatory_count',$mandatory_count);
		if(isset($this->request->data['pagetype']) &&  $this->request->data['pagetype'] == 'pre') {
			$this->request->data['pageno'] = $this->request->data['pageno'] - 2;
			/*why 2? because we will increase 1 at $next_page var*/
		}
		$tofetch = 20;
		$start_index = ($tofetch * $this->request->data['pageno']) - 20;
		$cond = array_merge($cond,$tagcond,$searchcond,$prioritycond);
		//pr($cond); die;
		$result = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.created','Tickets.modified','Tickets.status','Tickets.type','Tickets.modified','Tickets.tag_id','Tickets.description','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name','Replier.id'],
			'offset' => $start_index,
			'limit' => 20,
			])->contain(['User_To','User_From','Replier'])->all()->toArray();
		$paginatedarr = $result;
		$arr = [];
		$tofetch = 20;

		//pr($cond); die;

		$resultcount = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name']
			])->count();


		$totalpages = ceil($resultcount/20);
		$next_page = $this->request->data['pageno'] ;
		$projects = Cache::read("my_projects_$userSession[0]",'project');
		if ($projects !== false) {
			/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/

		    $my_projects = json_decode($projects);
		} else {
			$my_projects = $this->Common->getProjectsofuser($userID);	
			 Cache::write("my_projects_$userSession[0]", json_encode($my_projects),'project');
		}
		
		$cachedusers = Cache::read("all_users_$userSession[0]",'short');
		if ($cachedusers !== false) {
			/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
		    $all_users = json_decode($cachedusers);
		} else {
			$all_users =  $this->Common->getuser_name2($session_info[3]);
			 Cache::write("all_users_$userSession[0]", json_encode($all_users),'short');
		}


		foreach($result as $ticket){
			$ticket['idp'] = $ticket->id;
			$ticket['id'] = $this->Common->encForUrl($this->Common->ENC($ticket->id));
			$ticket['deadline'] =  date_create($ticket['deadline'])->format('d M Y h:i A');
			$ticket['created']  =  date_create($ticket['created'])->format('d M Y h:i A');
			$ticket['modified'] =  date_create($ticket['modified'])->format('d M Y h:i A');
			$arr[] = $ticket;
		}
		//pr($paginatedarr);die;
		$userroles = explode(',', $session_info[2]);
		//pr($userroles); die;
		$cachedentities = Cache::read("entities_$userSession[0]",'short');
		if ($cachedentities !== false) {
			/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
		    $entities = json_decode($cachedentities)[0];
		    $tags = json_decode($cachedentities)[1];	
		} else {
			$entitiesids = $this->EntityAgency->find('list',[
			 	'keyField' => 'id',
				'valueField' => 'entity_id',
				'conditions' => ['agency_id' => $session_info[3] ,'role_id IN' => $userroles ],
				'fields' => ['entity_id','agency_id','role_id','id']
			 ]);
			if(count($entitiesids->toArray()) > 0){
				$entities = $this->Entities->find('all',[
					'conditions' => ['id IN' => array_unique(array_values($entitiesids->toArray()) )]
				])->all();	
			} else {
				$entities = [];
			}

				$finalentities = [];
				$alltagids = [];
				$ids;
				if(count($entities) > 0){
					foreach($entities as $entity){
						$exp = 	explode(',', $entity['tag_id']);
						$alltagids = array_merge($alltagids,$exp);
					}
				}
				if(count($alltagids) > 0){
					$tagids = $this->TagAgency->find('list',[
				 	'keyField' => 'id',
					'valueField' => 'tag_id',
					'conditions' => ['agency_id' => $session_info[3] ,'role_id IN' => $userroles , 'tag_id IN' =>  array_unique($alltagids) ],
					'fields' => ['tag_id','agency_id','role_id','id']
					 ]);
				} else {
					$tagids = [];
				}
				
				if(count($tagids) > 0){
					$tags = $this->Tags->find('all',[
						'conditions' => ['is_deleted' => 'No','enable_on_ticket' => 'Yes','id IN' => array_unique(array_values($tagids->toArray()) )]
					]); 	
				} else {
					$tags = [];
				}

			 Cache::write("entities_$userSession[0]", json_encode([$entities,$tags]),'short');
		}

        $allmandatory = $this->Tickets->find('all',[
                             'conditions' => ['type' => 'Mandatory','to_id' => $session_info[0],'status IN' => [1,2]]
                         ])->all();
            $mandatory = [];
            foreach($allmandatory as $m){
                 if(count($mandatory) == 0){
                    if($m['last_replier'] !== intval($session_info[0])){
                            $mandatory = $m;
                        }      
                 }   
                
            }
    	 if(count($mandatory) > 0){
    	 		$mid = $this->Common->encForUrl($this->Common->ENC($mandatory->id));
    	 		$mandatory['eid'] = $mid;
    	 }
    	  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
    	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    	} else {
    		$ip_address = $_SERVER['REMOTE_ADDR'];
    	}
		echo json_encode(['status'=> 'success',
						'deadline'=> $deadline,
						'userID' => $userID,
						'resultData'=> $paginatedarr,
						'open' => $open, 
						'inprogress' => $inprogress,
						'my_projects'=> $my_projects,
						'all_users' => $all_users,
						'closed'=> $closed,
					    'expired' => $expired,
					    'totalrecords' => $resultcount,
					    'pageno' => $next_page,
					    'totalpages' => ($totalpages == 0 ) ? 1: $totalpages ,
					    'currentpage' => $this->request->data['pageno'],
					    'currentrecodsfrom' => $start_index + 1,
					    'tofetch' => $tofetch,
					    'today' =>  date('Y-m-d 00:00:00'),
					    'entities' => $entities,
					    'tags' => $tags,
					    'mandatory' => $mandatory,
					    'mandatory_count' => $mandatory_count,
					    'scheduled' => $scheduled,
					    'ip_address' => $ip_address,
					    'rls' => $currentuserroles
					 ]);
				die;
		//$this->set('deadline',$deadline);
		//$this->set('userID',$userID);
		//$this->set('resultData',$result);
	}

	/********************************/
	/*Who :maninder 				*/
	/*When : 6/2/16					*/
	/*Why : To render response 		*/
	/******************************	*/



	function ticketresponse($id = null, $last_page = null){
		$session = $this->request->session();
		$this->set('session',$session);
		$this->viewBuilder()->layout('new_layout_admin');
		$session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
		$this->set('userID',$userID);
		$this->set('current_user',$userID);
		if(!empty($id)){
			$id_ = /*$this->request->data['id'];*/ $this->Common->DCR($this->Common->dcrForUrl($id)) ;
			/*pr($id); die;*/
			$condition =array('Tickets.id'=>$id_);
			$result = $this->Tickets->find()
			->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
			->where(['Tickets.id'=>$id_])->contain(['User_To','User_From','Replier'])->first();
			$this->set('result', $result);
			$this->set('resultData',  $result);
			$this->set('last_page', $last_page);
			$this->set('id', $id);
			//echo json_encode(['status' => 'success','resultData' => $result, 'redirect' => 'ticketresponse']);
			//die;
		}
		if(isset($this->request->data['id']) /*$this->request->is('ajax')*/ ){  //pr($this->params); die;
		
			$modified_time = date("Y-m-d H:i:s");
			$status = $this->request->data['status'];
			$deadline= date("Y-m-d H:i:s",strtotime($this->request->data['deadline']));
			$condition =array('Tickets.id'=>$this->request->data['id']);
			$result = $this->Tickets->find()
			->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
			->where(['Tickets.id'=>$this->request->data['id']])->contain(['User_To','User_From','Replier'])->first();
			$this->set('result', $result);
			$this->set('resultData',  $result);
			$this->set('last_page', $last_page);
			$this->set('id', $id);
			if($status==1){
				$statusVal = 'Open';
			}else if($status==0){
				$statusVal = 'Closed';
			}else{
				$statusVal = 'In Progress';
			}

			if($this->request->data['last_page'] == "report"){
				$savedata = array();
				$receivername1 = $result['User_From']['first_name']." ".$result['User_From']['last_name'];
				$receiverMail1 = $result['User_From']['username'];
				$receivername2 = $result['User_To']['first_name']." ".$result['User_To']['last_name'];
				$receiverMail2 = $result['User_To']['username'];
				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id' => $userID,]
					])->first();
				$sender = isset($data)? $data->toArray():array();
				$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				$subject = ($userName." has updated Ticket (# ".$id.") at ".$now);
				// set values to be used on email template
				// set values to be used on email template
				$message2 = "Dear <span style='color:#666666'>".$receivername2."</span>,<br/><br/> ".$userName." has requested update to following ticket.<br/><br/>
				".$result['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";

				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail2)
				->subject($userName." has updated Ticket (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message2);


			}else{
				$response = "";
				/*pr($this->request->data); die;*/
				if(isset($this->request->data['description'])){
					$description= $this->request->data['description'];
					$title = $this->request->data['title'];
					$savedata = array('description'=>$description,'title'=>$title);
				}
				if(isset($this->request->data['response'])){
					$response = $this->request->data['response'];
					/*$description = $this->request->data['description'];*/
					$savedata = array('response'=>$response);
				}

				$result = $this->Tickets->find()
				->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
				->where(['Tickets.id'=>$this->request->data['id']])->contain(['User_To','User_From','Replier'])->first();
				$resultData = $result->toArray();
				
				if($userID == $resultData['User_To']['id']){
					$receivername = $resultData['User_From']['first_name']." ".$resultData['User_From']['last_name'];
					$receiverMail = $resultData['User_From']['username'];
				}else{
					$receivername = $resultData['User_To']['first_name']." ".$resultData['User_To']['last_name'];
					$receiverMail = $resultData['User_To']['username'];
				};
				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id' => $userID,]
					])->first();
				$sender = isset($data)? $data->toArray():array();
				$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				// Send mail to User
				/*$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has updated following ticket :<br/><br/>To :".$receivername."<br/><br/>From :".$userName."<br/><br/>Title :".$this->request->data['title']."<br/><br/>Description: ".nl2br($description)."<br/><br/>Response:".nl2br($response)."<br/><br/>Status :".$statusVal."<br/><br/>Deadline :".$this->request->data['deadline']."<br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
				//pr($message);die;
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail)
				->bcc( $sender['username'])
				->subject($userName." has updated Ticket (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message);*/
				
				// Email text message
				//$this->Email->send($message);
			}
			$time_for_work = (!empty($this->request->data['tasktime'])) ? $this->request->data['tasktime'] : "00:00";
			$billablehours = (!empty($this->request->data['billable_hours'])) ? $this->request->data['billable_hours'] : "00:00";
			$data = array(
				'to_id' => $this->request->data['to_id'],
				'status' => $status,
				'deadline' => $deadline,
				'type' => $this->request->data['type'],
				'priority' =>$this->request->data['priority'],
				'tasktime' => $time_for_work,
				'billable_hours' => $billablehours,
				'project_id' => $this->request->data['project_id'],
				'last_replier' => $userID,
				'modified' =>$modified_time,
				'ipaddress' =>IP_ADDRESS
				);
			$data = array_merge($savedata, $data);
			
			/* update Ticket closing Date */
			if($status == 0){
				$this->Tickets->updateAll(
					array('closedate' => date("Y-m-d H:i:s")),
					array('id' => $this->request->data['id'])
					);
			}
			$result =	$this->Tickets->updateAll($data,array('id' => $this->request->data['id']));

			/* update user status start */
			if(($status == 2 || $status == 0) && ($userID == $data['to_id'])){
				$current_status = $this->request->data['title']." - ".$result['description'];
				$res =$this->Users->updateAll(
					array('current_status' => $current_status,'status_modified' => date("Y-m-d H:i:s")),
					array('id' => $userID)
					);
			}
			echo json_encode(['status' => 'success']);
			 die;
			/*echo "1";
			$this->Flash->success(__('Ticket has been Updated successfully.'), 'default', array('id' => 'flashMessage', 'class' => 'green-left'), 'message');
			die;*/

		}
	}


	#_________________________________________________________________________#

	/**
    * @Date: 13-sept-2016
    * @Method : response
    * @Purpose: This function will perform ticket management.
    * @Param: id (ticket id)
    * @Return: none
    **/

	function response($id = null, $last_page = null){
			//	pr($this->request->data); die;
		$session = $this->request->session();
		$this->set('session',$session);
		$this->viewBuilder()->layout('new_layout_admin');
		$session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
		$this->set('userID',$userID);
		$this->set('current_user',$userID);
		$this->set('redirect', $this->referer());
		if(!empty($id)){
			$id_ = /*$this->request->data['id'];*/ $this->Common->DCR($this->Common->dcrForUrl($id)) ;
			/*pr($id); die;*/
			$condition =array('Tickets.id'=>$id_);
			$result = $this->Tickets->find()
			->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
			->where(['Tickets.id'=>$id_])->contain(['User_To','User_From','Replier'])->first();
			$this->set('result', $result);
			$this->set('resultData',  $result);
			$this->set('last_page', $last_page);
			$this->set('id', $id);
			//echo json_encode(['status' => 'success','resultData' => $result, 'redirect' => 'ticketresponse']);
			//die;
		}
		if(isset($this->request->data['id']) /*$this->request->is('ajax')*/ ){  //pr($this->params); die;
			/*pr($this->request->data); die;*/
			$id = $this->request->data['id'];
			$modified_time = date("Y-m-d H:i:s");
			$status = $this->request->data['status'];
			$deadline= date("Y-m-d H:i:s",strtotime($this->request->data['deadline']));
			$condition =array('Tickets.id'=>$this->request->data['id']);
			$result = $this->Tickets->find()
			->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
			->where(['Tickets.id'=>$this->request->data['id']])->contain(['User_To','User_From','Replier'])->first();
			$this->set('result', $result);
			$this->set('resultData',  $result);
			$this->set('last_page', $last_page);
			$this->set('id', $id);
			if($status==1){
				$statusVal = 'Open';
			}else if($status==0){
				$statusVal = 'Closed';
			}else{
				$statusVal = 'In Progress';
			}

			if($this->request->data['last_page'] == "report"){
				$savedata = array();
				$receivername1 = $result['User_From']['first_name']." ".$result['User_From']['last_name'];
				$receiverMail1 = $result['User_From']['username'];
				$receivername2 = $result['User_To']['first_name']." ".$result['User_To']['last_name'];
				$receiverMail2 = $result['User_To']['username'];
				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id' => $userID,]
					])->first();
				$sender = isset($data)? $data->toArray():array();
				$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				$subject = ($userName." has updated Ticket (# ".$id.") at ".$now);
				// set values to be used on email template

			/*	$message1 = "Dear <span style='color:#666666'>".$receivername1."</span>,<br/><br/> ".$userName." has requested update to following ticket.<br/><br/>
				 ".$result['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";

				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail1)
				->subject($subject)
               ->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message1);*/

				// set values to be used on email template
				$message2 = "Dear <span style='color:#666666'>".$receivername2."</span>,<br/><br/> ".$userName." has requested update to following ticket.<br/><br/>
				".$result['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";

				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail2)
				->subject($userName." has updated Ticket (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message2);


			}else{
				$response = "";
				$savedata = [];
				if(isset($this->request->data['description'])){
					$description= $this->request->data['description'];
					$title = $this->request->data['title'];
					$savedata = array_merge(array('description'=>$description,'title'=>$title),$savedata) ;
				}
				if(isset($this->request->data['response'])){
					$response = $this->request->data['response'];
					/*$description = $this->request->data['description'];*/
					$savedata = array_merge(array('response'=>$response),$savedata) ;
				}

				$result = $this->Tickets->find()
				->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.description','Tickets.response' ,'Tickets.tasktime','Tickets.project_id' ,'Tickets.billable_hours','User_To.id','User_To.first_name','User_To.last_name' ,'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','Replier.first_name','Replier.last_name'])
				->where(['Tickets.id'=>$this->request->data['id']])->contain(['User_To','User_From','Replier'])->first();
				$resultData = $result->toArray();
				
				if($userID == $resultData['User_To']['id']){
					$receivername = $resultData['User_From']['first_name']." ".$resultData['User_From']['last_name'];
					$receiverMail = $resultData['User_From']['username'];
				}else{
					$receivername = $resultData['User_To']['first_name']." ".$resultData['User_To']['last_name'];
					$receiverMail = $resultData['User_To']['username'];
				};
				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id' => $userID,]
					])->first();
				$sender = isset($data)? $data->toArray():array();
				$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				// Send mail to User
				/*$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has updated following ticket :<br/><br/>To :".$receivername."<br/><br/>From :".$userName."<br/><br/>Title :".$this->request->data['title']."<br/><br/>Description: ".nl2br($description)."<br/><br/>Response:".nl2br($response)."<br/><br/>Status :".$statusVal."<br/><br/>Deadline :".$this->request->data['deadline']."<br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
				//pr($message);die;
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail)
				->bcc( $sender['username'])
				->subject($userName." has updated Ticket (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message);*/
				
				// Email text message
				//$this->Email->send($message);
			}
			$time_for_work = (!empty($this->request->data['tasktime'])) ? $this->request->data['tasktime'] : "00:00";
			$billablehours = (!empty($this->request->data['billable_hours'])) ? $this->request->data['billable_hours'] : "00:00";
			//pr($userID);
			$data = array(
				'to_id' => $this->request->data['to_id'],
				'status' => (($status == 1) && ($this->request->data['to_id'] == intval($userID)) ) ? 2 : $status,
				'deadline' => $deadline,
				'type' => $this->request->data['type'],
				'priority' =>$this->request->data['priority'],
				'tasktime' => $time_for_work,
				'billable_hours' => $billablehours,
				'project_id' => $this->request->data['project_id'],
				'last_replier' => intval($userID),
				'modified' =>$modified_time,
				'ipaddress' =>IP_ADDRESS
				);
			$data = array_merge($savedata, $data);
			//pr($data); die;
			/* update Ticket closing Date */
			if($status == 0){
				$this->Tickets->updateAll(
					array('closedate' => date("Y-m-d H:i:s")),
					array('id' => $this->request->data['id'])
					);
			}
			/*pr($data); die;*/
			$result =	$this->Tickets->updateAll($data,array('id' => $this->request->data['id']));

			/* update user status start */
			if(($status == 2 || $status == 0) && ($userID == $data['to_id'])){
				$current_status = $this->request->data['title']." - ".$result['description'];
				$res =$this->Users->updateAll(
					array('current_status' => $current_status,'status_modified' => date("Y-m-d H:i:s")),
					array('id' => $userID)
					);
			}
			echo json_encode(['status' => 'success','id' => $this->request->data['id'] ]);
			 die;

		}


		
	}

       function add()
		{	
			$tag = (!empty($this->request->query['tag'])) ? $this->request->query['tag'] : "";
			$project_id = (!empty($this->request->query['project_id'])) ? $this->Common->DCR($this->Common->dcrForUrl($this->request->query['project_id'])) : "";
			$this->set('redirect', $this->referer());
			$this->set('tag',$tag);
			$this->set('project_id',$project_id);
			$this->viewBuilder()->layout("new_layout_admin");
			$session = $this->request->session();
			$this->set('session',$session);
			$session_info = $session->read("SESSION_ADMIN");
			$userID = $session_info[0];
			$this->set('current_user',$userID);
			if(isset($this->request->data) && isset($this->request->data['task_type'])){
			   //pr($this->request->data); die;
			   //pr($this->referer()); die;
			   $entity_type = (!empty($this->request->data['entity_type'])) ? $this->request->data['entity_type'] : "";
			   $entity_id = (!empty($this->request->data['entity_id'])) ? $this->Common->DCR($this->Common->dcrForUrl($this->request->data['entity_id'])) : "";
			   $task_type = (!empty($this->request->data['task_type'])) ? $this->request->data['task_type'] : "";
			   $this->set('selectedentity_type',$entity_type);
			   $this->set('selectedentity_id',$entity_id);
			   $this->set('selectedtask_type',$task_type);
			} else {
			   $this->set('selectedentity_type',1);
			   $this->set('selectedentity_id',0);
			   $this->set('selectedtask_type','To-Do');	
			}
			if( $this->request->is('ajax') || isset($this->request->data['to_id'])){
				$time_for_work = (!empty($this->request->data['tasktime'])) ? $this->request->data['tasktime'] : "00:00";
				$billablehours = (!empty($this->request->data['billable_hours'])) ? $this->request->data['billable_hours'] : "00:00";
				$to_id = $this->request->data['to_id'];
				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id IN' => [$userID, $to_id]],
					'fields' => ['Users.id', 'Users.first_name','Users.first_name','Users.last_name','Users.username']
					])->all();
				$userData = isset($data)? $data->toArray():array();
				foreach($userData as $userdata) {

					if($userID == $userdata['id'])
					{
						$receivername = $userdata['first_name']." ".$userdata['last_name'];
						$receiverMail = $userdata['username'];
					}else{
						$receivername = $userdata['first_name']." ".$userdata['last_name'];
						$receiverMail = $userdata['username'];
					}
				}

				$data  = 	$this->Users->find("all" , [
					'conditions' => ['id' => $userID],
					'fields' => ['Users.id', 'Users.first_name','Users.first_name','Users.last_name','Users.username']
					])->first();

				$sender = isset($data)? $data->toArray():array();
				$userName = $sender['first_name']." ".$sender['last_name'];
				$this->request->data['deadline'] = date_create($this->request->data['deadline'])->format('Y-m-d H:i:s');
				$this->request->data['to_id'] =  $to_id;
				$this->request->data['from_id'] =  $userID;
				$this->request->data['title'] =  $this->request->data['title'];
				$this->request->data['description'] =  $this->request->data['desc'];
				$this->request->data['priority'] =  $this->request->data['priority'];
				$this->request->data['type'] =  $this->request->data['type'];
				$this->request->data['entity_type'] =  $this->request->data['entity_type'];
				$this->request->data['entity_id'] = isset($this->request->data['entity_id']) ? $this->request->data['entity_id'] : 0;
				$this->request->data['last_replier'] =  $userID;
				$this->request->data['ipaddress'] =  IP_ADDRESS;
				$this->request->data['tasktime'] =  $time_for_work;
				$this->request->data['billable_hours'] =  $billablehours;
				$this->request->data['project_id'] =  ($this->request->data['entity_type'] == 2) ? $this->request->data['entity_id']: 0 ;
				$this->request->data['closedate'] =   date_format(date_create('0000-00-00'),"Y-m-d");
				$this->request->data['created'] =    date("Y-m-d H:i:s");
				$this->request->data['modified'] =    date("Y-m-d H:i:s");
				$this->request->data['agency_id'] =    $session_info[3];
				$now = date("F j, Y, g:i a");
					//pr($this->request->data); die;
				$arr = [];
				$arr['type'] = $this->request->data['type'];
				if($this->request->data['type'] == 'Scheduled'){
					$arr['after'] = $this->request->data['scheduletype'];
				}
				$this->request->data['ticket_parameters'] = json_encode($arr);
                    $users = $this->Tickets->newEntity($this->request->data(), ['validate' => false]);
                    $tick = $this->Tickets->save($users);

                    //echo "success";
                    echo json_encode(['status' => 'success' , 'id' => $tick->id ]); die;
				 // $this->Flash->success('Ticket has been added successfully.');
                    $this->Flash->success(__('Ticket has been added successfully.'), 'default', array('id' => 'flashMessage', 'class' => 'green-left'), 'message');
                    die;
				//$this->Session->setFlash("Ticket has been added successfully.",'layout_success');
                }
                  //$this->viewBuilder()->layout(false);
              	 // $all_users =  $this->Common->getuser_name($session_info[3]);
               
            }





            function getneededdata(){
					$session = $this->request->session();
					$session_info = $session->read("SESSION_ADMIN");
					$userID = $session_info[0];
					$my_projects = $this->Common->getProjectsofuser($userID);
					$all_users =  $this->Common->getuser_name2($session_info[3]);
						$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$today = date("Y-m-d H:i:s");
			$open_cond = [
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],
			'Tickets.status IN' => [1],
			'AND' => ['deadline >' =>$today]
			];
			/*[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 1
					];*/
			$inprogress_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 2
					];
			$closed_cond =	[
			'OR' => [['from_id' => $userSession[0]], ['to_id' => $userSession[0]]],
			'Tickets.status IN' => 0
			];
			$expired_cond =	[
			'OR' => [['from_id' => $userID], ['to_id' => $userID]],['deadline' < '$today'],
			'Tickets.status IN' => [1,2],
			'AND' => ['deadline <' =>$today]
			] ;
			$open = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($open_cond)
					->count();
			$inprogress = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($inprogress_cond)
					->count();
			$closed = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($closed_cond)
					->count();
			$expired = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($expired_cond)
					->count();

			$this->set('open',$open);
			$this->set('inprogress',$inprogress);
			$this->set('closed',$closed);
            	 echo json_encode(['status' => 'success',
            	 	'page'=> 'ticket',
            	 	'all_users' => $all_users,
            	 	'my_projects' => $my_projects,
            	 	'open' => $open, 
					'inprogress' => $inprogress,
					'closed'=> $closed,
				    'expired' => $expired,
				    'currenttime' => $today = date("Y-m-d H:i:s")
            	 	]);
            	 die;
            	 $this->set('all_users',$all_users);

            }
			/*********************************************************************/
			/* @page : modules
			/* @who : Maninder
			/* @why : For data to use on ticket response page
			/* @date:13-07-2017
			/**********************************************************************/


            function ticketdata(){
            	//Configure::write('debug', 2);
            if(isset($this->request->data['origin'])){
            	$id = $this->request->data['origin'];
            	 $id_ = /*$this->request->data['id'];*/ $this->Common->DCR($this->Common->dcrForUrl($id)) ;
				$session = $this->request->session();
				$session_info = $session->read("SESSION_ADMIN");
				$userSession = $session_info;
				$currentuserroles = $session_info[2];
				$userID = $session_info[0];
				//$my_projects = $this->Common->getProjectsofuser($userID);
				$projects = Cache::read("my_projects_$userSession[0]",'short');
				if ($projects !== false) {
					/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
					//pr("dd"); die;
				    $my_projects = json_decode($projects);
				} else {
					$my_projects = $this->Common->getProjectsofuser($userID);	
					 Cache::write("my_projects_$userSession[0]", json_encode($my_projects),'short');
				}

				$cachedusers = Cache::read("all_users_$userSession[0]",'short');
				if ($cachedusers !== false) {
					/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
				    $all_users = json_decode($cachedusers);
				} else {
					$all_users =  $this->Common->getuser_name2($session_info[3]);
					 Cache::write("all_users_$userSession[0]", json_encode($all_users),'short');
				}


				//$all_users =  $this->Common->getuser_name2($session_info[3]);
			$session = $this->request->session();
			$this->set('session',$session);
			//$userSession = $session->read("SESSION_ADMIN");
			$today = date("Y-m-d H:i:s");

			$condition =array('Tickets.id'=>$id_);
			$result = $this->Tickets->find()
			->select(['Tickets.id',
					  'Tickets.priority',
					  'Tickets.created',
					  'Tickets.modified',
					  'Tickets.to_id',
					  'Tickets.from_id',
					  'Tickets.title',
					  'Tickets.deadline',
					  'Tickets.status',
					  'Tickets.type',
					  'Tickets.description',
					  'Tickets.response' ,
					  'Tickets.tasktime',
					  'Tickets.project_id' ,
					  'Tickets.referencing_ticket' ,
					  'Tickets.billable_hours',
					  'Tickets.tag_id',
					  'Tickets.entity_id',
					  'Tickets.entity_type',
					  'Tickets.ipaddress',	
					  'User_To.id',
					  'User_To.first_name',
					  'User_To.last_name' ,
					  'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username','User_From.image_url','User_From.created','Replier.first_name','Replier.last_name','Tickets.entity_type','Tickets.entity_id' ,'Tickets.ticket_parameters'])
			->where(['Tickets.id'=>$id_])->contain(['User_To','User_From','Replier'])->first();
			$selectedtag = [];
			if($result->tag_id > 0){
				$selectedtag = $this->Tags->get($result->tag_id);
			}
			//pr($result['entity_type']); die;
			$entityvalue = $this->Common->getValueofEntity($this->Common->Enc($userID), $result['entity_type']);
			$userroles = explode(',', $session_info[2]);



			$cachedentities = Cache::read("entities_$userSession[0]",'short');
			if ($cachedentities !== false) {
				/*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
			    $entities = json_decode($cachedentities)[0];
			    $tags = json_decode($cachedentities)[1];	
			} else {
				$entitiesids = $this->EntityAgency->find('list',[
			 	'keyField' => 'id',
				'valueField' => 'entity_id',
				'conditions' => ['agency_id' => $session_info[3] ,'role_id IN' => $userroles ],
				'fields' => ['entity_id','agency_id','role_id','id']
				 ]);
				//pr($entitiesids->toArray()); die;
				if(count($entitiesids->toArray()) > 0){
					$entities = $this->Entities->find('all',[
					'conditions' => ['id IN' => array_unique(array_values( $entitiesids->toArray()) )]
					])->all();	
				} else {
					$entities = [];
				}

				$finalentities = [];
				$alltagids = [];
				$ids;
				if(count($entities) > 0){
					foreach($entities as $entity){
						$exp = 	explode(',', $entity['tag_id']);
						$alltagids = array_merge($alltagids,$exp);
					}
				}
				//pr($alltagids);
				if(count($alltagids) > 0){
					$tagids = $this->TagAgency->find('list',[
				 	'keyField' => 'id',
					'valueField' => 'tag_id',
					'conditions' => ['agency_id' => $session_info[3] ,'role_id IN' => $userroles , 'tag_id IN' =>  array_unique($alltagids) ],
					'fields' => ['tag_id','agency_id','role_id','id']
					 ]);
				} else {
					$tagids = [];
				}
				
				if(count($tagids) > 0){
					$tags = $this->Tags->find('all',[
						'conditions' => ['is_deleted' => 'No','enable_on_ticket' => 'Yes','id IN' => array_unique(array_values($tagids->toArray()) )]
					]); 	
				} else {
					$tags = [];
				}

			 Cache::write("entities_$userSession[0]", json_encode([$entities,$tags]),'short');
		}

			
			 $allmandatory = $this->Tickets->find('all',[
                             'conditions' => ['type' => 'Mandatory','to_id' => $session_info[0],'status IN' => [1,2]]
                         ])->all();
            $mandatory = [];
            foreach($allmandatory as $m){
                 if(count($mandatory) == 0){
                    if($m['last_replier'] !== intval($session_info[0])){
                            $mandatory = $m;
                        }      
                 }   
                
            }
			$currentuserinfo = $this->Users->get($userID);
			$currentuserinfo['id'] =  $this->Common->encForUrl($this->Common->ENC($currentuserinfo->id));
    	 if(count($mandatory) > 0){
    	 		$mid = $this->Common->encForUrl($this->Common->ENC($mandatory->id));
    	 		$mandatory['eid'] = $mid;
    	 }
    	 if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    		$ip_address = $_SERVER['HTTP_CLIENT_IP'];
    	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    		$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    	} else {
    		$ip_address = $_SERVER['REMOTE_ADDR'];
    	}
			echo json_encode( ['status' => 'success',
				'resultData' => $result,
				'current_user' => $userID ,
				'page' => 'ticketresponse',
				'all_users' => $all_users,
				'my_projects' => $my_projects,
				/*'open' => $open, 
				'inprogress' => $inprogress,
				'closed'=> $closed,
			    'expired' => $expired,*/
			    'currenttime' => $today = date("Y-m-d H:i:s"),
			    'entities' => $entities,
				'tags' => $tags,
			    'mandatory' => $mandatory,
			    'currentuserinfo' => $currentuserinfo,
			    'selectedtag' => $selectedtag,
			    'ip_address' => $ip_address,
			    'rls' => $currentuserroles,
			    'entityvalue' => $entityvalue
				] );
			die;
            }
          
            }
	#_________________________________________________________________________#

     
   function report($key = null,$id = null,$usr = null){
    	if(!empty($this->request->query['key'])){
    		$key = $this->request->query['key'];
    	}
    	$this->viewBuilder()->layout('new_layout_admin');
    	$this->set("title_for_layout", "My Tickets");
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
    	$condition_check = [];
	}        



	/**
    * @Date: 13-sept-2016
    * @Method : report
    * @Purpose: This function is used to generate tickets report to admin .
    * @Param: none
    * @Return: none
    **/


	function reportdata($key = null,$id = null,$usr = null){

		    if(isset($this->request->data['key'])){
    			$key = $this->request->data['key'];
    		} else{
    			$key ="";
    		}
    
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
    	$condition_check = [];
    	if(isset($this->request->data['sdate']) && $this->request->data['sdate'] !== 'none'){
    		$tofrom = ['Tickets.deadline >=' => date_create($this->request->data['sdate'])->format('Y-m-d H:i:s'), 'Tickets.deadline <=' => date_create($this->request->data['edate'])->format('Y-m-d H:i:s')];
    	} else{
    		$tofrom = [];
    	}

    	if(isset($this->request->data['tag_id']) && $this->request->data['tag_id'] !== '0'){
    		$tagcond = ['Tickets.tag_id' => $this->request->data['tag_id']];
    	} else{
    		$tagcond = [];
    	}


    	if(isset($this->request->data['userid']) && $this->request->data['userid'] !== 'none'){
    		$useridcond =[
				'OR' => [['from_id' => $this->request->data['userid'] ], ['to_id' => $this->request->data['userid']]]
				] ;
    	} else{
    		$useridcond = [];
    	}

    	

		if(isset($this->request->data['check_it']) && $this->request->data['check_it']=='1'){
			$cond =	[ 'Tickets.agency_id' => $session_info[3]];
		}else {

			$cond =	[
			/*'OR' => [['from_id' => $userID], ['to_id' => $userID]],*/
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [1,2]
			];
		}
		/*pr($cond); die;*/
	$deadline =  $this->Tickets->find("all")
	->where([
		/*'OR' => [['from_id' => $userID], ['to_id' => $userID]],*/
		'status IN' => [1,2]
		])
	->order(['created' => 'DESC'])->first();
	
	isset($deadline)? $deadline->toArray():array();
	$oderby = ['Tickets.modified' => 'DESC'];
	if(isset($this->request->data['filter']) && $this->request->data['filter'] !== 'none') {	
		$result = array();
		$oderby = ($this->request->data['filter'] == 'modified') ? ['Tickets.modified' => 'asc'] : ['Tickets.deadline' => 'asc'];
		if(!empty($key) && $this->request->data['check_it'] !== 1) {
			
			$today = date("Y-m-d 00:00:00");
			if($key == "open"){
				$cond =	[
				'Tickets.agency_id' => $session_info[3],
				'Tickets.status IN' => [1],
				'AND' => ['deadline >' =>$today]
				] ;
			}elseif($key == "close"){

				$cond =	[
				'Tickets.agency_id' => $session_info[3],
				'Tickets.status IN' => [0]
				] ;

			}elseif($key == "expire"){
				$cond =	[
				'Tickets.agency_id' => $session_info[3],
				'Tickets.status IN' => [1,2],
				'AND' => ['Tickets.deadline <' =>$today]
				] ;
			}elseif($key == "inprogress"){
			$cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [2]
			];
			}
		}
	}elseif(!empty($key)) {
		$today = date("Y-m-d 00:00:00");
		if($key == "open"){
			$cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [1],
			'AND' => ['deadline >' =>$today]
			] ;
		}elseif($key == "close"){
			$cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [0]
			];
		}elseif($key == "expire"){
			$cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [1,2],
			"Tickets.deadline <" =>  $today
			] ;
		} elseif($key == "inprogress"){
			$cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [2]
			];
		}
	}elseif(!empty($this->request->data['reminderid']) && $this->request->data['reminderid'] != 'none'){
		$info = $this->Tickets->find()
		->where([' Tickets.id'=> $this->Common->DCR($this->Common->dcrForUrl($this->request->data['reminderid'])) ])
		->select(['Tickets.title','Tickets.to_id','Tickets.from_id',
			'Tickets.modified','User_To.id','User_To.first_name','User_To.last_name',
			'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username'])->contain(['User_To','User_From'])->first();
		if($userID == $info['to_id'] || $userID == $info['from_id']){
			if($userID == $info['User_To']['id'])
			{
				$receivername = $info['User_From']['first_name']." ".$info['User_From']['last_name'];
				
				$receiverMail = $info['User_From']['username'];
			}else{
				$receivername = $info['User_To']['first_name']." ".$info['User_To']['last_name'];
				$receiverMail = $info['User_To']['username'];
			}
			$sender =  $this->Users->find()
			->where(['Users.id'=>$userID])
			->select(['Users.first_name','Users.last_name','Users.username'])->first();
			$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has requested update to following ticket.<br/><br/>
				".$info['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail)
				->bcc( $sender['username'])
				->subject("Ticket Reminder (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message);

				// set values to be used on email template
				if ($email->send($message)) {
					echo json_encode(['status' => 'success']);
				}else{
					echo json_encode(['status' => 'failed']);
				}
				die;
				/*$this->redirect(array('action' => 'index'));*/
			}else{
				
				$this->redirect(array('action' => 'index'));

			}
		} 
		if(!empty($this->request->data['ticket_id']) && $this->request->data['ticket_id'] != 'none') {
		
			$cond =['Tickets.id' => $this->request->data['ticket_id'],'Tickets.agency_id' => $session_info[3],];
		}
		/*pr($this->request->data['ticket_id']); die;*/
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$today = date("Y-m-d 00:00:00");
			$open_cond = [
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [1],
			'AND' => ['deadline >' =>$today]
			];

			$inprogress_cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => 2
					];
			$closed_cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => 0
			];
			$expired_cond =	[
			'Tickets.agency_id' => $session_info[3],
			'Tickets.status IN' => [1,2],
			'AND' => ['deadline <' =>$today]
			] ;

			$open = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($open_cond)
					->order($oderby)->count();
			$inprogress = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($inprogress_cond)
					->order($oderby)->count();
			$closed = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($closed_cond)
					->order($oderby)->count();
			$expired = $this->Tickets->find()
					->select(['Tickets.id'])
					->where($expired_cond)
					->order($oderby)->count();

			$this->set('open',$open);
			$this->set('inprogress',$inprogress);
			$this->set('closed',$closed);
			
		/*pr($cond); die;*/
		/*pr($oderby); die;*/
		if(isset($this->request->data['pagetype']) &&  $this->request->data['pagetype'] == 'pre') {
			$this->request->data['pageno'] = $this->request->data['pageno'] - 2;
			/*why 2? because we will increase 1 at $next_page var*/
		}


		$cond = array_merge($tofrom,$cond,$useridcond,$tagcond);
/*		pr($cond); die;
*/		$tofetch = 20;
		$start_index = ($tofetch * $this->request->data['pageno']) - 20;
		$result = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name'],
			'offset' => $start_index,
			'limit' => 20,
			])->contain(['User_To','User_From','Replier'])->all()->toArray();
		$paginatedarr = $result;
		$arr = [];
		$tofetch = 20;

		 /*else{
			$start_index = ($tofetch * $this->request->data['pageno']) - 20;
		}*/
	/*	$start_index = ($tofetch * $this->request->data['pageno']) - 20;*/
	
	
		$resultcount = $this->Tickets->find('all',[
			'conditions' => $cond,
			'order' => $oderby,
			'fields' => ['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.last_name','Replier.first_name','Replier.last_name']
			])->count();


		$totalpages = ceil($resultcount/20);
		$next_page = $this->request->data['pageno'] ;
		
		foreach($result as $ticket){
			$ticket['idp'] = $ticket->id;
			$ticket['id'] = $this->Common->encForUrl($this->Common->ENC($ticket->id));
			$arr[] = $ticket;
		}
		$users = $this->Common->getuser_name2($session_info[3]);
		echo json_encode(['status'=> 'success',
						'deadline'=> $deadline,
						'userID' => $userID,
						'resultData'=> $paginatedarr,
						'open' => $open, 
						'inprogress' => $inprogress,
						'closed'=> $closed,
					    'expired' => $expired,
					    'totalrecords' => $resultcount,
					    'pageno' => $next_page,
					    'totalpages' => ($totalpages == 0 ) ? 1: $totalpages ,
					    'currentpage' => $this->request->data['pageno'],
					    'currentrecodsfrom' => $start_index + 1,
					    'tofetch' => $tofetch,
					    'today' => $today = date("Y-m-d 00:00:00"),
					    'resultcount' => $resultcount,
					    'users' => $users
					 ]);
		die;
		$this->set('deadline',$deadline);
		$this->set('userID',$userID);
		$this->set('resultData',$result);
	}


	#_________________________________________________________________________#

	/**
    * @Date: 13-sept-2016
    * @Method : schedule
    * @Purpose: This function is used to generate tickets schedule to admin .
    * @Param: none
    * @Return: none
    **/
	function schedule() {
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		$this->viewBuilder()->layout('new_layout_admin');
		//$this->viewBuilder()->layout="layout_admin";
		$this->set("title_for_layout", "Tickets Schedule");
		$resultUser = $this->Users->find('all',[
			'fields' => ['Users.id','Users.first_name',
			'Users.last_name','Users.freefrom'],
			'conditions' => ['Users.status'=>'1','Users.agency_id' => $userSession[3]],
			'order' => ['Users.first_name' => 'asc']
			])->toArray();
		
		$year = date('Y');

		$paymentData = $this->Payments->find('all', [
			'fields' => ['Payments.responsible_id',
			'Payments.currency','Payments.payment_date','Payments.amount'],
			'conditions' => ['YEAR(payment_date)'=>$year,'Payments.agency_id' => $userSession[3]]
			])->toArray();
		

		$cond = "Tickets.status = '1' OR Tickets.status = '2' 'Payments.agency_id' = $userSession[3]";
		$resultTicket = $this->Tickets->find('all',[
			'conditions' => $cond,
			'fields' => ['Tickets.id','Tickets.to_id','Tickets.from_id','Tickets.deadline']
			])->toArray();
		
		$this->set('paymentData',$paymentData);
		$this->set('resultData',$resultUser);
		$this->set('resultTicket',$resultTicket);
	}
	#_______________________________________________________________________________________#
	/**
    * @Date: 13-sept-2016
    * @Method : closereport
    * @Purpose: This function is used to show user specific tickets listing .
    * @Param: $id
    * @Return: none
    **/
	function closereport($id = null,$date = null){
		
		$this->viewBuilder()->layout="layout_admin";
		$this->set("title_for_layout", "Closed Tickets");

		$session = $this->request->session();
		$session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
		$resultTicket = $this->Tickets->find('all',[
			"conditions" => ['Tickets.to_id' =>$id,'Tickets.status' =>'0','Tickets.closedate' => $date]
			])->contain(["User_To","User_From","Replier"])->all()->toArray();
		$this->set('userID',$userID);
		$this->set('resultData',$resultTicket);
	}
	#_______________________________________________________________________________________#
		/**
    * @Date: 13-sept-2016
    * @Method : ticketdetail
    * @Purpose: This function is used to show user specific tickets listing .
    * @Param: $id
    * @Return: none
    **/
function ticketdetail($id = null) {
		$this->viewBuilder()->layout(false);

		

		$resultTicket = $this->Tickets->find('all',[
			"conditions" => ['Tickets.id'=>$id]
			])->contain(["User_To","User_From","Replier"])->first();
		$this->set('resultData',$resultTicket);
		$result5 = $this->Tickets->find('all',['conditions'=>'id'>0]);
		

	}


	#_________________________________________________________________________#
 /**
 * @Date: 13-sept-2016
 * @Method : ticketreportdate
    * @Purpose: This function is to show the Reports of Win/Closed Project.
 * @Param: none
 * @Return: none
 **/

 function ticketreportdate(){
 	$this->viewBuilder()->layout(false);
 	$this->viewBuilder()->layout='layout_graph';
 	$session = $this->request->session();
	$this->set('session',$session);
	$userSession = $session->read("SESSION_ADMIN");
 	if(isset($this->request->data['user_id'])){
 		$userselect  = $this->request->data['user_id'];
 		$start_date = $this->request->data['start_date'];
 		$end_date   = $this->request->data['end_date'];
 		$leadselect = $this->request->data['select'];
 	}else{
 		$userselect  ="";
 		$start_date = "";
 		$end_date   = "";
 		$leadselect = "";
 	}

 	$this->set('fromDate',$start_date);
 	$this->set('toDate',$end_date);
 	$cond=array('Tickets.status'=>0,1,2);
 	$cur=date("Y-m-d");
 	$date = strtotime($cur);
 	$date = strtotime("previous monday", $date);
 	$week_start= date("Y-m-d",$date);
 	$date = strtotime($cur);
 	$date = strtotime("next sunday",$date);
 	$week_end=date("Y-m-d",$date);
 	$week_cond =(['Tickets.deadline >= '=> $week_start,'Tickets.deadline <='=>$week_end]);
 	$cond1 =(['Tickets.deadline >= '=> $start_date,'Tickets.deadline <='=>$end_date]);
 	if(empty($userselect)&&empty($start_date)&&empty($end_date))
 	{
 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Users.status'=>1,$week_cond,'Tickets.agency_id' => $userSession[3]])
 		->group('Tickets.to_id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = Users.id')
 			]);
 	}
 	elseif(empty($userselect)&&!empty($start_date)&&!empty($end_date))
 	{


 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Users.status'=>1,$cond1,'Tickets.agency_id' => $userSession[3]])
 		->group('Users.id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = Users.id')
 			]);


 	}
 	elseif(!empty($userselect) && $leadselect!='select')
 	{
 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Tickets.to_id'=>$userselect,$cond1,'Tickets.agency_id' => $userSession[3]])
 		->group('Tickets.to_id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = Users.id')
 			]);

 	}elseif(!empty($userselect) && $start_date==$cur)
 	{

 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Tickets.to_id'=>$userselect,$cond1,'Tickets.agency_id' => $userSession[3]])
 		->group('Tickets.to_id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = "$userselect"')
 			]);

 	}
 	elseif(!empty($userselect) &&$start_date==$cur)
 	{

 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Tickets.to_id'=>$userselect,'Tickets.agency_id' => $userSession[3]])
 		->group('Tickets.to_id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = Users.id')
 			]);

 	}
 	else{

 		$query=TableRegistry::get('Tickets');
 		$resultData=$query->find();
 		$resultData ->select(['Tickets.id','Tickets.status','Users.first_name','Users.last_name','NoOfTickets' => $resultData->func()->count('Tickets.id')])
 		->where(['Tickets.to_id'=>$userselect,$cond1,'Tickets.agency_id' => $userSession[3]])
 		->group('Tickets.to_id','Tickets.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Tickets.to_id = Users.id')
 			]);

 	}
 	$this->set('resultData',$resultData->toArray());


 }
/**********************************************************************/
/* @page : modules
/* @who : Maninder
/* @why : For Reordering Tickets Through Drag Drop
/* @date: 1-02-2017
**********************************************************************/
 function reordertickets(){
 		if(isset($this->request->data)){
 			$other_ticket = $this->Common->DCR( $this->Common->dcrForUrl( $this->request->data['other_ticket'] ) );
 			$dropped_ticket = $this->Common->DCR( $this->Common->dcrForUrl( $this->request->data['dropped_ticket'] ) ); 
 			$afterorbefore = $this->request->data['afterorbefore'];
 			$other_ticket_data = $this->Tickets->get($other_ticket);
 			$dropped_ticket_data = $this->Tickets->get($dropped_ticket);
 			if(isset($other_ticket_data) && isset($dropped_ticket_data)){
 				if($afterorbefore == 'after'){
 					$date = date_create($other_ticket_data->modified)->format('YmdHis') + 1;
 				} else{
 					if(date_create($other_ticket_data->modified)->format('s') == 00){
 						$date = date_create($other_ticket_data->modified)->format('YmdHis') - 141;
 					} else{
 						$date = date_create($other_ticket_data->modified)->format('YmdHis') - 1 ;
 					}
 					
 				}
 				//pr(date_create($other_ticket_data->modified)->format('s')); die;
 				pr(date_create($other_ticket_data->modified)->format('YmdHis'));
 				pr($date);
 				pr($other_ticket_data->modified);
 				pr(date_create($date)->format('Y-m-d H:i:s'));
 				pr($dropped_ticket_data['id']); die;
 				$credentials = TableRegistry::get('Tickets');
				$update = ['modified' => date_create($date)->format('Y-m-d H:i:s')];
				$credentials->updateAll($update,['id' =>$dropped_ticket_data['id']]);
 				echo json_encode(['status' => 'success']); die;
 			} else {
 				echo json_encode(['status' => 'failed','reason' => 'Invalid Data']); die;
 			}
 		}
 }
 /**********************************************************************/
/* @page : modules
/* @who : Maninder
/* @why : For Reordering Tickets Through Drag Drop
/* @date: 1-02-2017
**********************************************************************/

 
  function getValueofEntity(){
  	if(isset($this->request->data)){
  		$this->viewBuilder()->layout(false);	
  		$value = $this->Common->getValueofEntity($this->request->data['userid'], $this->request->data['entity_id']);

  		$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userroles = explode(',', $session_info[2]);

			$entity = $this->Entities->find('all',[
				'conditions' => ['id' => $this->request->data['entity_id'] ]
			])->first();

			$alltagids = explode(',',$entity['tag_id']);
			//print_r(array_filter($alltagids)); die;
				if(count(array_filter($alltagids)) > 0){
					$tagids = $this->TagAgency->find('list',[
				 	'keyField' => 'id',
					'valueField' => 'tag_id',
					'conditions' => ['agency_id' => $session_info[3] ,'role_id IN' => $userroles , 'tag_id IN' =>  array_unique($alltagids) ],
					'fields' => ['tag_id','agency_id','role_id','id']
					 ]);
				} else {
					$tagids = [];
				}
				
				if(count($tagids) > 0){
					$tags = $this->Tags->find('all',[
						'conditions' => ['is_deleted' => 'No','enable_on_ticket' => 'Yes','id IN' => array_unique(array_values($tagids->toArray()) )]
					]); 	
				} else {
					$tags = [];
				}
		$resultJ = json_encode(['response' => 'success', 'value' => $value ,'tags' => $tags]);
		$this->response->type('json');
		$this->response->body($resultJ);
		return $this->response;
  	}
  }
/*********************************************************************/
/* @page : modules
/* @who : Maninder
/* @why : For Reordering Tickets Through Drag Drop
/* @date:13-07-2017
**********************************************************************/
   function instantsave(){
   		if(isset($this->request->data)){
	  		$this->viewBuilder()->layout(false);
	  		$id = (is_numeric($this->request->data['ticket_id'] ) ) ?  $this->request->data['ticket_id'] : $this->Common->DCR($this->Common->dcrForUrl( $this->request->data['ticket_id'] ) );
	  		$session = $this->request->session();
	    	$session_info = $session->read("SESSION_ADMIN");
	    	$userID = $session_info[0];
	  		if($this->request->data['field'] == 'reminderid'){
	  			
	  					$info = $this->Tickets->find()
						->where([' Tickets.id'=> $id])
						->select(['Tickets.title','Tickets.to_id','Tickets.from_id',
							'Tickets.modified','User_To.id','User_To.first_name','User_To.last_name',
							'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username'])->contain(['User_To','User_From'])->first();
						if($userID == $info['to_id'] || $userID == $info['from_id']){
							if($userID == $info['User_To']['id'])
							{
								$receivername = $info['User_From']['first_name']." ".$info['User_From']['last_name'];
								
								$receiverMail = $info['User_From']['username'];
							}else{
								$receivername = $info['User_To']['first_name']." ".$info['User_To']['last_name'];
								$receiverMail = $info['User_To']['username'];
							}
							$sender =  $this->Users->find()
							->where(['Users.id'=>$userID])
							->select(['Users.first_name','Users.last_name','Users.username'])->first();
							$userName = $sender['first_name']." ".$sender['last_name'];
								$now = date("F j, Y, g:i a");
								$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has requested update to following ticket.<br/><br/>
								".$info['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
								$email = new Email();
								$email //->template('all_pass_change')
								->emailFormat('html')
								->to($receiverMail)
								->bcc( $sender['username'])
								->subject("Ticket Reminder (# ".$id.") at ".$now)
								->replyTo($sender['username'])
								->from(ADMIN_EMAIL)
								->send($message);
								pr($message); die;
								// set values to be used on email template
								if ($email->send($message)) {
									echo json_encode(['status' => 'success']);
								}else{
									echo json_encode(['status' => 'failed']);
								}
								die;
								/*$this->redirect(array('action' => 'index'));*/
							}else{
								
								 echo json_encode(['status' => 'failed']);

							}
	  		}else{
	  			$tosave = [$this->request->data['field'] => $this->request->data['fielddata'] ];
				if($this->request->data['fielddata'] == 'Scheduled' && $this->request->data['field'] == 'type'){ 
						$arr = [];
		  				$arr['type'] = $this->request->data['fielddata'];
						$arr['after'] = $this->request->data['after'];
						$arr['after'] = $this->request->data['after'];
						$this->request->data['ticket_parameters'] = json_encode($arr);
						$tosave['ticket_parameters'] =  json_encode($arr);
				}
		  		$ticket = $this->Tickets->updateAll($tosave,array('id' => $id));
		  		$resultJ = json_encode(['response' => 'success']);
				$this->response->type('json');
				$this->response->body($resultJ);
				return $this->response;
	  		}
	  		
  		}
   }

/*********************************************************************/
/* @page : modules
/* @who : Maninder
/* @why : For Reordering Tickets Through Drag Drop
/* @date:13-07-2017
**********************************************************************/

   function setreminder(){
   if(isset($this->request->data)){
   		$info = $this->Tickets->find()
		->where([' Tickets.id'=> $this->Common->DCR($this->Common->dcrForUrl($this->request->data['reminderid'])) ])
		->select(['Tickets.title','Tickets.to_id','Tickets.from_id',
			'Tickets.modified','User_To.id','User_To.first_name','User_To.last_name',
			'User_To.username','User_From.id','User_From.first_name','User_From.last_name','User_From.username'])->contain(['User_To','User_From'])->first();
		if($userID == $info['to_id'] || $userID == $info['from_id']){
			$session = $this->request->session();
	    	$session_info = $session->read("SESSION_ADMIN");
	    	$userID = $session_info[0];
			if($userID == $info['User_To']['id'])
			{
				$receivername = $info['User_From']['first_name']." ".$info['User_From']['last_name'];
				
				$receiverMail = $info['User_From']['username'];
			}else{
				$receivername = $info['User_To']['first_name']." ".$info['User_To']['last_name'];
				$receiverMail = $info['User_To']['username'];
			}
			$sender =  $this->Users->find()
			->where(['Users.id'=>$userID])
			->select(['Users.first_name','Users.last_name','Users.username'])->first();
			$userName = $sender['first_name']." ".$sender['last_name'];
				$now = date("F j, Y, g:i a");
				$message = "Dear <span style='color:#666666'>".$receivername."</span>,<br/><br/>".$userName." has requested update to following ticket.<br/><br/>
				".$info['title']." (# ".$id.") <br/><br/>Thanks, <br/>Regards,<br/> ERP Team";
				$email = new Email();
				$email //->template('all_pass_change')
				->emailFormat('html')
				->to($receiverMail)
				->bcc( $sender['username'])
				->subject("Ticket Reminder (# ".$id.") at ".$now)
				->replyTo($sender['username'])
				->from(ADMIN_EMAIL)
				->send($message);

				// set values to be used on email template
				if ($email->send($message)) {
					echo json_encode(['status' => 'success']);
				}else{
					echo json_encode(['status' => 'failed']);
				}
				die;
				/*$this->redirect(array('action' => 'index'));*/
			}else{
				
				 echo json_encode(['status' => 'failed']);

			}
   }
   	
   }

}
