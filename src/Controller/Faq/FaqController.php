<?php 

namespace App\Controller\Faq;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Routing\Router;

class FaqController  extends AppController { 
	var $name       	=  "Faq";   
     // Controller Name    
	
    /********************** START FUNCTIONS **************************/
     /**
     * @Date: Nov,2016  
     * @Method : beforeFilter    
     * @Purpose: This function is called before any other function.    
     * @Param: none   
     * @Return: none    
     * */
	function beforeFilter(Event $event){ // This  function is called first before parsing this controller file
       parent::beforeFilter($event);
       	// star to check permission
       $session = $this->request->session();
       $userSession = $session->read("SESSION_ADMIN");
       $controller=$this->request->params['controller'];
       $action=$this->request->params['action'];
       $permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
       if($permission!=1){
         $this->Flash->error("You have no permisson to access this Page.");
         return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
     }
    // end code to check permissions
     if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){
         $this->checkUserSession();
     }else{
         $this->viewBuilder()->layout('layout_admin');

     }
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }
    $this->set('common',$this->Common); 

}

 /**
     * @Date: Nov,2016     
     * @Method : initialize    
     * @Purpose: This function is called initialize  function.    
     * @Param: none   
     * @Return: none    
     * */

 public function initialize()
 {
    parent::initialize();
        $this->loadModel('Tickets'); // for importing Model
        // for importing Model
        $this->loadModel('Projects'); // for importing Model
       // for importing Model
        $this->loadModel('Users');

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Testimonials');
        $this->loadModel('Faq');

    }

	#_________________________________________________________________________#   
	/**        
	* @Date: Nov,2016     
	* @Method : index         
	* @Purpose: This function is the default function of the controller  
	* @Param: none                                     
	* @Return: none                                          
	**/    
	function index() {
     $this->render('login'); 
     if($session->read("SESSION_USER") != ""){      
      $this->redirect('dashboard');             
  }    
}

	#_________________________________________________________________________#  
	/**                       
	* @Date: Nov,2016        
	* @Method : contactslist      
    * @Purpose: This function is to show list of Credentials in the System   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  

	function faqlist(){   
$this->viewBuilder()->layout('new_layout_admin');
		$session = $this->request->session();
    $userSession = $session->read("SESSION_ADMIN");
       $this->set('session',$session);  
       $this->set('title_for_layout','FAQ Listing');      
       $this->set("pageTitle","FAQ");     
       $this->set("search1", "");                           
       $this->set("search2", "");                                 
		$criteria = ['agency_id' => $userSession[3]]; //All Searching	  
		$session->delete('SESSION_SEARCH');	  
        if(isset($this->request->data['Faq']) || !empty($this->request->query)) {  
          if(!empty($this->request->data['Faq']['fieldName']) || isset($this->request->query['field'])){        
              if(trim(isset($this->request->data['Faq']['fieldName'])) != ""){			
                 $search1 = trim($this->request->data['Faq']['fieldName']);    
             }elseif(isset($this->request->query['field'])){		
                $search1 = trim($this->request->query['field']);		
            }
            $this->set("search1",$search1);      
        }		
        if(isset($this->request->data['Faq']['value1']) || isset($this->request->data['Faq']['value2']) || isset($this->request->query['value'])){	
            if(isset($this->request->data['Faq']['value1']) || isset($this->request->data['Faq']['value2'])){	
                $search2 = ($this->request->data['Faq']['fieldName'] != "Faq.status")?trim($this->request->data['Faq']['value1']):$$this->request->data['Faq']['value2'];	
            }elseif(isset($this->request->query['value'])){	
              $search2 = trim($this->request->query['value']);	
          }		
          $this->set("search2",$search2);
      }		

      /* Searching starts from here */		
      if(!empty($search1) && (!empty($search2) || $search1 == "Faq.status")){	
       // echo "fjhg";die;
        $query  =    $this->Faq->find('all' , [
          'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'agency_id' => $userSession[3]],
          ]);

        $result = isset($query)? $query->toArray():array();
        $criteria = $search1." LIKE '%".$search2."%'"; 		
        $session->write('SESSION_SEARCH', $criteria);
    }else{	
     // echo "fjhg";die;
        $query  =  $this->Faq->find("all",[
            'conditions' => ['agency_id' => $userSession[3]],
          ]); 		
        $this->set("search1","");      
        $this->set("search2","");      
    }

}
    $urlString = "/";
    if(isset($this->request->query)){                   
     $completeUrl  = array();
     if(!empty($this->request->query['page']))
        $completeUrl['page'] = $this->request->query['page'];
    if(!empty($this->request->query['sort']))	
     $completeUrl['sort'] = $this->request->query['sort']; 
    if (!empty($this->request->query['direction']))        
      $completeUrl['direction'] = $this->request->query['direction']; 	
    if(isset($search2))                                         
      $completeUrl['value'] = $search2;
             // pr($search2);
    foreach($completeUrl as $key => $value) {                      
      $urlString.= $key.":".$value."/";                           
    }
    }
    $this->set('urlString', $urlString); 
    $urlString = "/";  
    if(isset($this->request->query)){
        $completeUrl  = array();  
        if(!empty($setValue)){                            
          if(isset($this->params['form']['IDs'])){          
              $saveString = implode("','",$this->params['form']['IDs']);      
          }
      }
    }

    $this->set('urlString', $urlString);      
    $this->paginate = [           
    'fields' => [	
    'id',	
    'title',	
    'status',    
    'description',		
    'created',
    'updated'	
    ],     
    'page'=> 1,'limit' => 100,     
    'order' => ['id' => 'desc']          
    ];     

    $data = $this->paginate('Faq',['conditions' => $criteria]); 
    $arr=[];
    //pr($criteria);die;
    foreach($data as $d){      
      $d['id'] = $d->id; 
        $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));   
       $arr[] = $d; 
      }

    $this->set('resultData', $arr);
    $this->set('pagename', "list");  	
}


	#_________________________________________________________________________#  


	/**  
		* @Date: Nov,2016   
		* @Method : add    
		* @Purpose: This function is to add contacts from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
       **/  

       function faqadd() {	
       $this->viewBuilder()->layout('new_layout_admin');	
           $this->set('title_for_layout','Add FAQ');		
           $this->pageTitle = "Add FAQ";			
           $this->set("pageTitle","Add Faq");	
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
           $mode = "add";		
           if($this->request->data){	
          //  pr($this->request->data); die;
            $this->request->data['agency_id'] = $userSession[3];
            $faqs = $this->Faq->newEntity($this->request->data());
            if(empty($faqs->errors())){
             $this->Faq->save($faqs);

             $this->Flash->success_new("FAQ has been created successfully.");
             $this->redirect(array('action' => 'faqlist'));   
         }else{
          $this->set("errors", $faqs->errors());
      }
  }     
}
    /*function quickadd() {  
       $this->viewBuilder()->layout(false);  
           $this->set('title_for_layout','Add FAQ');    
           $this->pageTitle = "Add FAQ";      
           $this->set("pageTitle","Add FAQ"); 
            $session = $this->request->session();
            $userSession = $session->read("SESSION_ADMIN");
           $mode = "add";   
           if($this->request->data){  
            $this->request->data['agency_id'] = $userSession[3];
            $this->request->data['iscurrent'] = '1';
            $faqs = $this->Faq->newEntity($this->request->data());
            if(empty($faqs->errors())){
             $this->Faq>save($faqs);
             echo json_encode(['status' => 'success','id' => $this->Common->ENC($faqs->id), 'name' => $faqs->name]);
             die;
         }else{
          echo json_encode(['status' => 'failed']);
            die;
          //$this->set("errors", $contactss->errors());
      }
  }     
}*/
 
                                                            

#_____________________________________________________________________________________________#  

	/**  
		* @Date: Nov,2016  
		* @Method : edit    
		* @Purpose: This function is to edit contacts from admin section. * @Param: none 
		* @Return: none  
		* @Return: none     
       **/  


        function faqedit($id = null) { 
         // pr($id);die;
          $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Edit FAQ');     
            $this->pageTitle = "Edit FAQ";    
            $this->set("pageTitle","Edit FAQ"); 
            $this->set('id',$id);  
            if($this->request->data)
            { 
              $faq = $this->Faq->get($id);
             // pr($this->request->data);
               // $finance = $this->Finance->get($this->request->data['id']);
                $faqs = $this->Faq->patchEntity($faq,$this->request->data());
                if(empty($faq->errors())) {
                  $this->Faq->save($faq);
                  $this->Flash->success_new("FAQ has been updated successfully.");
                  $this->redirect(array('action' => 'faqlist'));  
              }else{                                    
                 $this->set("errors", $faq->errors());
             } 
         }else if(!empty($id)){ 

            $FaqData = $this->Faq->get($id);
            $this->set('FaqData',$FaqData); 
            if(!$FaqData){             
              $this->redirect(array('action' => 'faqlist'));                 
          }                                                      
      }                                                          
  }    


		#_____________________________________________________________________________________________# 

		/**  
		* @Date: Nov,2016 
		* @Method : delete    
		* @Purpose: This function is to delete contacts from admin section.   
		* @Param: none 
		* @Return: none  
		* @Return: none     
       **/  
function faqdelete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
     $faq = $this->Faq->get($id);
      if ($this->Faq->delete($faq)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }


/**  
        * @Date: Nov,2016 
        * @Method : changeStatus    
        * @Purpose: This function is to changeStatus from admin section.   
        * @Param: none 
        * @Return: none  
        * @Return: none     
    **/  
function faqchangeStatus() {
  
 

 if($this->request->is('ajax')) {

       $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {

      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $updateCredentials = 1;
      $setValue = 0;
      $messageStr = 'deactivate';
      }
      if ($updateCredentials == 1) {

        $credentials = TableRegistry::get('Credentials');

        $update = ['user_id' => 0 , 'status' => 0];
        $credentials->updateAll($update,['user_id IN' => $id]);
      }
     $users = TableRegistry::get('Faq');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;

      }
     $this->setAction('faqlist'); 


}




	#_______________________________________________________________________________________#
	/**nction clientlogout(){
      $this->Sessio
		* @Date: Nov,2016 
		* @Method : clientlogin    
		* @Purpose: This function is used for client login.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/  
     function faqclientlogin(){
      $this->layout = "layout_client_withoutlogin";
      if($this->Session->read("SESSION_CLIENT") != ""){
       $this->redirect(array('controller'=>'faq','action'=>'clientdashboard'));
   }
   if($this->data){

       $this->Faq->set($this->data['Faq']);
       $isValidated = $this->Faq->validates();
       if($isValidated){ 	
        $cpassword    = $this->data['Faq']['accesspass'];
        $condition = "cpassword='".$cpassword."'";
        $client_details = $this->Faq->find('first', array("conditions" => $condition, "fields" => array("id","name")));

        if(is_array($client_details) && count($client_details) > 0){
         $this->Session->write("SESSION_CLIENT", array($client_details['Faq']['id'],$client_details['Faq']['name']));
         $this->redirect(array('controller'=>'faq','action'=>'faqclientdashboard'));
     }else{
         $this->Session->setFlash("<div class='error-message flash notice'>The access code you entered is incorrect.</div>");

     }
 }

}
}

	#____________________________________________________________________________________#
	/**
		* @Date: Nov,2016     
		* @Method : clientdashboard    
		* @Purpose: This function is used to show client dashborad.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/  
     function faqclientdashboard(){
      $this->layout = "layout_clientadmin";
      $this->set('title_for_layout','Dashboard');		
      $clientSession = $this->Session->read("SESSION_CLIENT"); 
      $client_id  = $clientSession[0];
      if($clientSession == ""){
       $this->redirect(array('controller'=>'faq','action'=>'faqclientlogin'));
   }else{
       $projects = $this->Project->find('all', 
        array(
         'fields' => array('Project.id', 'Project.project_name','Project.modified','Project.status','Project.publicurl'),
         'conditions' => array('Project.publicurl'=>'1','Project.client_id'=>$client_id),
         'order' => array('Project.modified' => 'desc')	
         ));
   }
   $this->set('projectData',$projects);
}

	#____________________________________________________________________________________#
	/**
		* @Date: Nov,2016     
		* @Method : clientlogout    
		* @Purpose: This function is used to show client dashborad.
		* @Param: $id                                       
		* @author: Neelam Thakur
		* @Return: none
       **/
     function faqclientlogout(){
      $this->Session->delete("SESSION_CLIENT");
      $this->redirect(array('controller'=>'faq','action' => 'faqclientlogin'));

  }
  function faqdetails($id=null){
    $id = $id;
    //pr($id); die;
       $this->viewBuilder()->layout('new_layout_admin');
        $this->set('title_for_layout','Detail of  FAQ');     
            $this->pageTitle = "Detail FAQ";    
            $this->set("pageTitle","Detail FAQ"); 
            $this->set('id',$id);  
            $faq = $this->Faq->get($id);
            $this->set('faq', $faq);
             if($this->request->data)
            { 
              $faq = $this->Faq->get($id);
              //pr($this->request->data);
               //$faq = $this->Faq->get($this->request->data['id']);

             }
          }
        
          function faqpage(){
             
            $this->viewBuilder()->layout('new_layout_admin');
               $this->set('title_for_layout','Detail of  FAQ');     
            $this->pageTitle = "Detail FAQ";    
            $this->set("pageTitle","Detail FAQ"); 
             $faq = $this->Faq->find('all');
             $this->set('faq', $faq);
      
            // pr($faq);die;
            //$this->redirect(array('action' => 'faqpage'));

           
        }
  


}


