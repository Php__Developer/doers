<?php

namespace App\Controller\Employees;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Utility\Text;
use Cake\Utility\Inflector;
use Cake\Auth\DefaultPasswordHasher;


class EmployeesController extends AppController {

    
    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : beforeFilter
     * @Purpose: This function is called before any other function.
     * @Param: none
     * @Return: none 
     * */
    function beforeFilter(Event $event) {
        
      parent::beforeFilter($event);
    // star to check permission
      $session = $this->request->session();
      $userSession = $session->read("SESSION_ADMIN");
 
    // end code to check permissions
     Router::parseNamedParams($this->request);
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     
     $session = $this->request->session();
     $userSession = $session->read("SESSION_ADMIN");
     if($userSession==''){
        return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    }

    if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {
     
     
        $this->checkUserSession();
    } else {
        $this->viewBuilder()->layout('layout_admin');
        
    }
    $this->set('common', $this->Common);
}

 /**
     * @Date: 26-sep-2016  
     * @Method : initialize
     * @Purpose: This function is called initialize  function.
     * @Param: none
     * @Return: none 
     * */

 public function initialize()
 {
    parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
            $this->loadModel('Agencies');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $this->loadComponent('Upload');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
        $this->loadModel('Settings');

    }



    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : index
     * @Purpose: This page will render home page
     * @Param: none
     * @Return: none 
     * */
    function index() {
        $this->set("title_for_layout", "ERP : Vlogic Labs - Relying on Commitment");
    }

    

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : employeelist
     * @Purpose: This function is to show employeelist of users in system.
     * @Param: none
     * @Return: none 
     * */
    function employeelist() {
       $this->viewBuilder()->layout('new_layout_admin');
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
        /*pr($userSession); die;*/
        $agencyid = $userSession[3];
        $this->set('session',$session);
        $this->set("title_for_layout", "Employees listing");
        $this->set("pageTitle", "Employees listing");
        $this->set("search1", "");
        $this->set("search2", "");
        $criteria = "1"; //All Searching
if (isset($this->request->data['User']) || !empty($this->request->query)) {
 if (!empty($this->request->data['User']['fieldName']) || isset($this->request->query['field'])) {
             if (trim(isset($this->request->data['User']['fieldName'])) != "") {
                    $this->set("search1", $this->request->data['User']['fieldName']);
                    
                    if (trim($this->request->data['User']['fieldName']) == "Users.employeecode") {
                        $search1 = "Users.employee_code";
                    } elseif (trim($this->request->data['User']['fieldName']) == "Users.firstname") {
                        $search1 = "Users.first_name";
                    } elseif (trim($this->request->data['User']['fieldName']) == "Users.currentsalary") {
                        $search1 = "Users.current_salary";
                    } else {
                        $search1 = trim($this->request->data['User']['fieldName']);
                    }
                } elseif (isset($this->request->query['field'])) {
                    $search1 = trim($this->request->query['field']);
                    $this->set("search1", $search1);
                }
                
            }

            if (isset($this->request->data['User']['value1']) || isset($this->request->data['User']['value2']) || isset($this->request->query['value'])) {
                if (isset($this->request->data['User']['value1']) || isset($this->request->data['User']['value2'])) {
                    $search2 = ($this->request->data['User']['fieldName'] != "Users.status") ? trim($this->request->data['User']['value1']) : $this->request->data['User']['value2'];
                } elseif (isset($this->request->query['value'])) {
                    $search2 = trim($this->request->query['value']);
                }
                $this->set("search2", $search2);
            }
            /* Searching starts from here */

            if (!empty($search1) && (!empty($search2) || $search1 == "Users.status")) {
                if($agencyid == 0){
                    $searchcond = [$search1 .' Like' =>'%'.$search2.'%' ];
                } else {
                    $searchcond = [$search1 .' Like' =>'%'.$search2.'%' ,'Users.agency_id' => $agencyid];
                }
                $query  =    $this->Users->find("all" , [
                  'conditions' => $searchcond,
                  'order' => ['Users.status asc'],
                  ])/*->contain(['Appraisals'])*/;
                
                $session->write('SESSION_SEARCH', $criteria);
            } else {
                if($agencyid == 0){
                    $searchcond = [];
                } else {
                    $searchcond = ['Users.agency_id' => $agencyid];
                }
                $query  =  $this->Users->find('all',[
                'order' => ['status asc'],
                'conditions' => $searchcond,
                ])/*->contain(['Appraisals'])*/;
                $this->set("search1", "");
                $this->set("search2", "");
            }
         
        } else {
            // if action is accessed without searching or sorting
            if($agencyid == 0){
                    $searchcond = [];
                } else {
                    $searchcond = ['Users.agency_id' => $agencyid];
                }
          $query  =  $this->Users->find('all',[
             'order' => ['Users.status asc'],
             'conditions' => $searchcond,
             //'order' => ['id'=>'desc'],
            ])/*->contain(['Appraisals'])*/;
         // pr($query);die;
          $result = isset($query)? $query->toArray():array();
        // pr($result);die;
          $this->set('resultset' ,$result);
      }

      $urlString = "/";
      if (isset($this->request->query) && count($this->request->query) > 0) {
        $completeUrl = array();
        if (!empty($this->request->query['page']))
            $completeUrl['page'] = $this->request->query['page'];
        if (!empty($this->request->query['sort']))
            $completeUrl['sort'] = $this->request->query['sort'];
        if (!empty($this->request->query['direction']))
            $completeUrl['direction'] = $this->request->query['direction'];
        if (!empty($search1))
            $completeUrl['field'] = $search1;
        if (isset($search2))
            $completeUrl['value'] = $search2;
        foreach ($completeUrl as $key => $value) {
            $urlString.= $key . ":" . $value . "/";
        }
    }
    $this->set('urlString', $urlString);
    
 
$paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/100);
$this->set('paginatecount',$paginate);
    $data = $this->paginate($query,[
      'page' => 1, 'limit' => 100,
      'order' => ['created'=>'desc'],
      'paramType' => 'querystring'
      ]);

$arr=array();
  foreach($data as $d){      
     $d['id'] = $d->id; 
     $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));
    /* pr($d->id); die;*/
     $roles = TableRegistry::get('Appraisals');
       $appraisalsdata  =  $roles->find("all" , [
                  'conditions' => ['user_id' => $d->id,'status'=>1],
                  'fields' => ['appraised_amount'],
                  'order' => ['effective_from'=>'desc']
               ])->first();
      // pr($appraisalsdata) ; die;
        $result = isset($appraisalsdata)? $appraisalsdata->toArray():array();
       

         if(count($result)>0){
            $amount=$result['appraised_amount'];
         }else{
            $amount=0;
         }
       $d['Appraisals']  = $result;
       $d['salary']=$amount;
       $arr[] = $d; 
  }
    // resultset


       $this->set('resultData', $arr);
     if($this->request->is('ajax')){
          if(isset($this->request->query['page'])){
             $pageing=$this->request->query['page'];
          }else{
            $pageing="";
          }
        echo json_encode(['resultData'=>$arr,'page'=>$pageing,'paginatecount'=>$paginate]);
         die;
      }

}

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016        
     * @Method : delete    
     * @Purpose: This function is used to delete users from admin section.
     * @Param: $id                                       
     * @Return: none
     * @Return: none
     * */
    function delete($id = null) {
        $id = $id;
        $this->viewBuilder()->layout('new_layout_admin');
        $employee = $this->Employees->get($id);
       $employeeId=$employee->id;
      $modules = TableRegistry::get('Modules');
      $query = $modules->find()
      ->where([
        'OR' => [['FIND_IN_SET(\''. $employeeId .'\',Modules.except_user_id)'],['FIND_IN_SET(\''. $employeeId .'\',Modules.including_user_id)']],
        ]);
      $results=isset($query)? $query->toArray():array();
      if(count($results)>0){
        foreach($results as $v){
            // for including user id 
            $explede=explode(",",$v->including_user_id);
            $a=array_search($employeeId, $explede); // search that id in all session array
            unset($explede[$a]);
            $count =count($explede);
            $combineagency= implode(",", $explede); 
// for except user id 
            $except_user_id=explode(",",$v->except_user_id);
            $a=array_search($employeeId, $except_user_id); // search that id in all session array
            unset($except_user_id[$a]);
            $count =count($except_user_id);
            $exceot= implode(",", $except_user_id); 
             // again insert data into table 
            $update = ['including_user_id' => $combineagency,'except_user_id'=>$exceot];
            $credentials = TableRegistry::get('Modules');
            $result=$credentials->updateAll($update,['id'=>$v->id]);
            if ($this->Employees->delete($employee)) {
                    $credentials = TableRegistry::get('Credentials');
                    $update = ['user_id' => 0 , 'status' => 0];
                    $credentials->updateAll($update,['user_id IN' => $id]);
                      echo json_encode(['status'=> 'success','page'=>'Appraisals']); die;
                     }else { 
                     echo json_encode(['status'=> 'failed']); die;
             }
          }
       }else{
        if ($this->Employees->delete($employee)) {
            $credentials = TableRegistry::get('Credentials');
            $update = ['user_id' => 0 , 'status' => 0];
            $credentials->updateAll($update,['user_id IN' => $id]);
              echo json_encode(['status'=> 'success','page'=>'Appraisals']); die;
        }else { 
             echo json_encode(['status'=> 'failed']); die;
         }
      }



       




    }



    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016         
     * @Method : changeStatus    
     * @Purpose: This function is used to changeStatus users from admin section.
     * @Param: $id                                       
     * @Return: none
     * @Return: none
     * */
    function changeStatus() {

    if($this->request->is('ajax')) {

     $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $updateCredentials = 0;
      $setValue = 1;
      $messageStr = 'activate';
                $users = TableRegistry::get('Employees');
                $update = ['status' => $setValue];
                $record= $users->updateAll($update,['id IN' => $id]);
                if($record == 1){
                $status = json_encode(['status' => 'success','msg'=>$messageStr]);
                }else{
                $status = json_encode(['status' => 'failed']);
                }

      }elseif ($this->request->data['eORd'] =='No') {
       $updateCredentials = 1;
       $setValue = 0;
       $messageStr = 'deactivate';

           $employeeId=$id;
      $modules = TableRegistry::get('Modules');
      $query = $modules->find()
      ->where([
        'OR' => [['FIND_IN_SET(\''. $employeeId .'\',Modules.except_user_id)'],['FIND_IN_SET(\''. $employeeId .'\',Modules.including_user_id)']],
        ]);
      $results=isset($query)? $query->toArray():array();
      if(count($results)>0){
        foreach($results as $v){
            // for including user id 
            $explede=explode(",",$v->including_user_id);
            $a=array_search($employeeId, $explede); // search that id in all session array
            unset($explede[$a]);
            $count =count($explede);
            $combineagency= implode(",", $explede); 
// for except user id 
            $except_user_id=explode(",",$v->except_user_id);
            $a=array_search($employeeId, $except_user_id); // search that id in all session array
            unset($except_user_id[$a]);
            $count =count($except_user_id);
            $exceot= implode(",", $except_user_id); 
             // again insert data into table 
            $update = ['including_user_id' => $combineagency,'except_user_id'=>$exceot];
            $credentials = TableRegistry::get('Modules');
            $result=$credentials->updateAll($update,['id'=>$v->id]);

           }
         }
            $users = TableRegistry::get('Employees');
                $update = ['status' => $setValue];
                $record= $users->updateAll($update,['id IN' => $id]);
                if($record == 1){
                $status = json_encode(['status' => 'success','msg'=>$messageStr]);
                }else{
                $status = json_encode(['status' => 'failed']);
                }

                
          }
          if ($updateCredentials == 1) {
                $credentials = TableRegistry::get('Credentials');
                $update = ['user_id' => 0 , 'status' => 0];
                $credentials->updateAll($update,['user_id IN' => $id]);
            }

        echo $status;
        die;


      }



 }




    #_________________________________________________________________________#
 
    /**
     * @Date: 26-sep-2016       
     * @Method : add
     * @Purpose: This function is to add user from admin section.
     * @Param: none
     * @Return: none 
     * @Return: none 
     * */
    function add() {
     Configure::write('debug', 2);
    $this->viewBuilder()->layout('new_layout_admin');
        $this->set("title_for_layout", "Add Employee");
        $today = date("Y-m-d");
        $session = $this->request->session();
        $this->set('session',$session);
        $customErrors = [];
         $userSession = $session->read("SESSION_ADMIN");
         $this->set('userSession',$userSession);
         $agencyid = $userSession[3];
        if ($this->request->data) {
           $this->request->data['dob'] = date_create($this->request->data['dob_temp'])->format('Y-m-d');
            $this->request->data['doj'] = date_create($this->request->data['doj_temp'])->format('Y-m-d');
            $this->request->data['dor'] = date_create($this->request->data['dor_temp'])->format('Y-m-d');
             if(isset($this->request->data['role_id']) ) {
              $this->request->data['role_id'] = implode(",", $this->request->data['role_id']);
              }
           if(!empty($this->request->data['username']) ){
                $usernamecheck = $this->Employees->find('all',[
                        'conditions'=> ['status' => 1 , 'username' => $this->request->data['username'],'agency_id' => $userSession[3]]
                    ])->all();
                
                if(count($usernamecheck) > 0 ){
                    $customErrors['username'] = "Username is already taken";
                    $this->set('customErrors',$customErrors);
                }
             }
             $this->request->data['agency_id'] = $agencyid;
            $users = $this->Employees->newEntity($this->request->data());
           if (empty($users->errors()) && empty($customErrors)){

                //$this->request->data['role_id'] = !empty($this->request->data['role_id']) ? implode(",", $this->request->data['role_id']): '';
                $this->Employees->save($users);
                $last_insert_id  = $users->id;
                $apraisal = $this->Appraisals->newEntity();
                $apraisal->supervisor_id = !empty($this->request->data['report_to']) ? $this->request->data['report_to'] : 0 ;
                $apraisal->user_id = $last_insert_id;
                $apraisal->appraised_amount = $this->request->data['current_salary'];
                $apraisal->evaluation_from = $today;
                $apraisal->next_review_date = $today;
                $apraisal->effective_from = $today;
                $apraisal->appraisal_date = $today;
                $apraisal->new_designaiton = $this->request->data['designation'];
                $apraisal->status = 1;

                $this->Appraisals->save($apraisal);
                if (!empty($this->request->data)){
                    $image=  $this->Upload->send($this->request->data['upload']);
                    if(isset($image)){
                        $file=$image;
                        $image = $this->request->data['upload'];
                        $articlesTable = TableRegistry::get('Users');
                        $article = $articlesTable->get($last_insert_id); 
                        $article->image_url = md5($image['name']);
                        $a=$this->Users->save($article);
                    }
                }
                if (!empty($this->request->data['dropbox'])){
                    $credential = $this->Credentials->find('all')->where(['type' => 'dropbox','username'=> $this->request->data['dropbox']])->first();

                    $credential->user_id = $last_insert_id;
                    $credential->status = 1;
                    $this->Credentials->save($credential);
                    // saving user's credentials into users table
                    $user = $this->Employees->get($last_insert_id);
                    $user->dropbox= $credential->id;
                    $this->Employees->save($user);
                }
                if (!empty($this->request->data['username'])){
                    $credential = $this->Credentials->find('all')->where(['type' => 'webmail','username'=> $this->request->data['username']])->first();

                    $credential->user_id = $last_insert_id;
                    $credential->status = 1;
                    $this->Credentials->save($credential);
                    // saving user's credentials into users table
                    $user = $this->Employees->get($last_insert_id);
                    $user->username= $credential->username;
                    $this->Employees->save($user);
                }
                if (!empty($this->request->data['skype'])){
                    $credential = $this->Credentials->find('all')->where(['type' => 'skype','username'=> $this->request->data['skype']])->first();

                    $credential->user_id = $last_insert_id;
                    $credential->status = 1;
                    $this->Credentials->save($credential);
                    // saving user's credentials into users table
                    $user = $this->Employees->get($last_insert_id);
                    $user->skype= $credential->id;
                    $this->Employees->save($user);

                }
                if (!empty($this->request->data['gmail'])){
                    $credential = $this->Credentials->find('all')->where(['type' => 'gmail','username'=> $this->request->data['gmail']])->first();
                    
                    $credential->user_id = $last_insert_id;
                    $credential->status = 1;
                    $this->Credentials->save($credential);
                      // saving user's credentials into users table
                    $user = $this->Employees->get($last_insert_id);
                    $user->gmail= $credential->id;
                    $this->Employees->save($user);
                }

                $this->Flash->success_new("Employee has been created successfully.");
                return $this->redirect( ['action' => 'employeelist']);
            } else {
               //pr($users->errors()); die;
                $this->set("errors", $users->errors());
            }
        }
    }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016       
     * @Method : edit
     * @Purpose: This function is to edit user from admin section.
     * @Param: $id
     * @Return: none 
     * @Return: none 
     * */
    function edit($id = null) {
      $session = $this->request->session();
       $this->set('session',$session);
       $userSession = $session->read("SESSION_ADMIN");
       $this->set('userSession',$userSession);
        $this->viewBuilder()->layout('new_layout_admin');
        $this->set("title_for_layout", "Edit Employee");
        $customErrors =[];
        if ($this->request->data) {
            $this->request->data['dob'] = date_create($this->request->data['dob_temp'])->format('Y-m-d');
            $this->request->data['doj'] = date_create($this->request->data['doj_temp'])->format('Y-m-d');
            $this->request->data['dor'] = date_create($this->request->data['dor_temp'])->format('Y-m-d');
            $pass = $this->request->data['password'];        
            if (empty($pass)) {
                unset($this->request->data['password']); 
            }
            
            if(isset($this->request->data['role_id']) && count($this->request->data['role_id']) > 0 ) {
               $this->request->data['role_id'] = implode(",", $this->request->data['role_id']);
         }


         $user = $this->Employees->get($this->request->data['id']);
         $old_username = $user->username;
        if($this->request->data['username'] == $user->username){
            $old_username ="";
         }
        if(!empty($this->request->data['username']) ){
                $usernamecheck = $this->Employees->find('all',[
                        'conditions'=> ['status' => 1 , 'username' => $this->request->data['username'],'id !=' => $this->request->data['id'],'agency_id'=>$userSession[3]]
                    ])->all();
               // pr($usernamecheck); die;
                if(count($usernamecheck) > 0 ){
                    $customErrors['username'] = "Username is already taken";
                    $this->set('customErrors',$customErrors);
                }
           }
         $users = $this->Employees->patchEntity($user,$this->request->data());
         if (empty($users->errors()) && empty($customErrors)) {
            if (empty($pass)) {
                unset($this->request->data['password']); 
            }
            $this->Employees->save($user);
            $last_insert_id = $user->id;

            if(!empty($this->request->data['upload']['name'])){
                //pr($this->request->data['upload']); die;
                $image=  $this->Upload->send($this->request->data['upload'],$id);
                if(isset($image)){
                    $file=$image;
                    //$image = $this->request->data['upload'];
                    $id = $this->request->data['id'];
                    $articlesTable = TableRegistry::get('Users');
                    $article = $articlesTable->get($id); 
                    $article->image_url = $image;
                    $a=$this->Users->save($article);
                }
            }

            if (!empty($this->request->data['dropbox'])){
                $credential = $this->Credentials->find('all')->where(['type' => 'dropbox','username'=> $this->request->data['dropbox']])->first();
                $data['user_id'] = $last_insert_id;
                $data['status'] = 1;
                $this->Credentials->updateAll($data,['id' => $credential->id]);
                $user = $this->Employees->get($last_insert_id);
                $userdata['dropbox']= $credential->id;
                $this->Employees->patchEntity($user,$userdata, ['validate' => false]);
                $this->Employees->save($user);
            }
            if (!empty($this->request->data['username'])){
                $credential = $this->Credentials->find('all')->where(['type' => 'webmail','username'=> $this->request->data['username']])->first();
                $data['user_id'] = $last_insert_id;
                $data['status'] = 1;
                $this->Credentials->updateAll($data,['id' => $credential->id]);
                $user = $this->Employees->get($last_insert_id);
                 if(!empty($old_username)){
                $credential = $this->Credentials->find('all')->where(['type' => 'webmail','username'=> $old_username])->first();
                $data['user_id'] = 0;
                $data['status'] = 0;
                $this->Credentials->updateAll($data,['id' => $credential->id]);
                 } 
            }

            if (!empty($this->request->data['skype'])){
                $credential = $this->Credentials->find('all')->where(['type' => 'skype','username'=> $this->request->data['skype']])->first();
                $data['user_id'] = $last_insert_id;
                $data['status'] = 1;
                $this->Credentials->updateAll($data,['id' => $credential->id]);
                $user = $this->Employees->get($last_insert_id);
                $userdata['skype']= $credential->id;
                $this->Employees->patchEntity($user,$userdata, ['validate' => false]);
                $this->Employees->save($user);

            }
            if (!empty($this->request->data['gmail'])){
                $credential = $this->Credentials->find('all')->where(['type' => 'gmail','username'=> $this->request->data['gmail']])->first();
                $data['user_id'] = $last_insert_id;
                $data['status'] = 1;
                $this->Credentials->updateAll($data,['id' => $credential->id]);
                      // saving user's credentials into users table
                $user = $this->Employees->get($last_insert_id);
                $userdata['gmail']= $credential->id;
                $this->Employees->patchEntity($user,$userdata, ['validate' => false]);
                $this->Employees->save($user);


            }
            $this->Flash->success_new("Employee's information has been updated successfully.");
            
            $this->redirect(array('action' => 'employeelist')); 
            
        } else {
            $id = $this->request->data['id'];
           $EmployeeData = $this->Employees->get($id);
    $this->set('EmployeeData',$EmployeeData);
    $employee = $this->Employees->find("all" , [
      'conditions' => ['Employees.id' => $id ],
      ])->contain(['MyAppraisals'])->first()->toArray();
    $this->set('employee',$employee);
    
    $id = $employee['id'];
    $this->set('id',$id);
    
    $employee['dor'] = ($employee['dor'] == "0000-00-00") ? "" : $employee['dor'];
    $employee['doj'] = ($employee['doj'] == "0000-00-00") ? "" : $employee['doj'];
    $employee['dob'] = ($employee['dob'] == "0000-00-00") ? "" : $employee['dob'];
    $employee['password'] = "";    
    if (!empty($employee['role_id'])) {
        $employee['role_id'] = explode(",", $employee['role_id']);
    }
    $this->set('resultData', $employee);
         $this->set("errors", $users->errors());
     }
 } else if (!empty($id)) {

    $EmployeeData = $this->Employees->get($id);
    $this->set('EmployeeData',$EmployeeData);
    $employee = $this->Employees->find("all" , [
      'conditions' => ['Employees.id' => $id ],
      ])->contain(['MyAppraisals'])->first()->toArray();
    $this->set('employee',$employee);
    $id = $employee['id'];
    $this->set('id',$id);
    $employee['dor'] = ($employee['dor'] == "0000-00-00") ? "" : $employee['dor'];
    $employee['doj'] = ($employee['doj'] == "0000-00-00") ? "" : $employee['doj'];
    $employee['dob'] = ($employee['dob'] == "0000-00-00") ? "" : $employee['dob'];
    $employee['password'] = "";
    if (!empty($employee['role_id'])) {
        $employee['role_id'] = explode(",", $employee['role_id']);
    }
    $this->set('resultData', $employee);
    if (!$employee) {
      $this->redirect(array('action' => 'employeelist'));

    }
  }
}


    #__________________________________________________________________________________________________________________#
    /**
     * @Date: 26-sep-2016  
     * @Method : credentialDetail
     * @Purpose: Displays the credential allotted to any particular user.
     * @Param: none
     * @Return: none 
     * */

    function credentialDetail($id = null) { 
      
       $this->viewBuilder()->layout(false);
       $this->viewBuilder()->layout = "layout_graph";
       $session = $this->request->session();
       $this->set('session',$session);
       $user = $session->read("SESSION_ADMIN");
       if(isset($id)){
           
        $employee = $this->Employees->get($id)->toArray();
        
    }else{
        $employee = $this->Employees->find("all")->first()->toArray();
        $id = $employee['id'];
    }
    

    $users = TableRegistry::get('Users');
    $data  =  $users->find("list" , [
        'keyField' => 'id',
        'valueField' => 'first_name',
        'conditions' => ['status' => 1],
        'fields' => ['first_name','id'],
        'order' => ['first_name'=>'ASC']
        ]);
    
    $results = isset($data)? $data->toArray():array();
    
    $this->set('UserDetail', $results);
    
    $data  =  $this->Credentials->find("all" , [
      'conditions' => ['user_id' => $id]
      ])->all();
    
    $primary_cerdentail = isset($data)? $data->toArray():array();

    $this->set('primary_cerdentail', $primary_cerdentail);
    $data  =  $this->Credentials->find("all" , [
      'conditions' => ['FIND_IN_SET(\'' . $id . '\',Credentials.other_alloted_user)']
      ])->all();
    $secon_cerdentail = isset($data)? $data->toArray():array();

    $this->set('secon_cerdentail', $secon_cerdentail);

    $data  =  $this->SaleProfiles->find("all" , [
      'conditions' => ['FIND_IN_SET(\'' . $id . '\',SaleProfiles.alloteduserid)'],
      'fields' => ['alloteduserid', 'username', 'password', 'type', 'forworded_email', 'id']
      ])->all();
    $profile_cre = isset($data)? $data->toArray():array();



    $this->set('pro_data', $profile_cre);
    /* Credential Logs from credential_logs table */

    $Credential_for_log1 = $this->Credentials->query("SELECT credentials.username,credentials.forworder_email,credentials.password,credentials.type,credentials.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM credentials left join credential_logs on credentials.id=credential_logs.credential_id and credential_logs.type='credential' and credential_logs.user_id='".$id."'")->toArray();

    $this->set('Credential_for_log2', $Credential_for_log1);

    $sale_profile_cre1 = $this->SaleProfiles->query("SELECT sale_profiles.username,sale_profiles.forworded_email,sale_profiles.password,sale_profiles.type,sale_profiles.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM sale_profiles left join credential_logs on sale_profiles.id=credential_logs.credential_id and credential_logs.type='saleProfile' and credential_logs.user_id='".$id."'")->toArray();

    $this->set('sale_profile_cre_logs', $sale_profile_cre1);

    if(isset($_GET['search'])){
      $id=$_GET['search'];
      $this->set('searchkword',$id);
      $primary_user = $this->Users->get($id)->toArray();

      $users = TableRegistry::get('Users');
      $data  =  $users->find("list" , [
        'keyField' => 'id',
        'valueField' => 'first_name',
        'conditions' => ['status' => 1],
        'fields' => ['first_name','id'],
        'order' => ['first_name'=>'ASC']
        ]);
      $results = isset($data)? $data->toArray():array();

      $this->set('UserDetail', $results);

      $this->set('primary_user', $primary_user);

      $data  =  $this->Credentials->find("all" , [
          'conditions' => ['user_id' => $id]
          ])->all();
      $primary_cerdentail = isset($data)? $data->toArray():array();
      
      $this->set('primary_cerdentail', $primary_cerdentail);
      $secon_cerdentail = $this->Credentials->find('all', array('conditions' => array('FIND_IN_SET(\'' . $id . '\',Credentials.other_alloted_user)')));

      $this->set('secon_cerdentail', $secon_cerdentail);

      $profile_cre = $this->SaleProfiles->find('all', array('fields' => array('SaleProfiles.alloteduserid', 'SaleProfiles.username', 'SaleProfiles.password', 'SaleProfiles.type', 'SaleProfiles.forworded_email', 'SaleProfiles.id'),
        'conditions' => array('FIND_IN_SET(\'' . $id . '\',SaleProfiles.alloteduserid)')));
      $this->set('pro_data', $profile_cre);
      /* Credential Logs from credential_logs table */


      $Credential_for_log1 = $this->Credentials->query("SELECT credentials.username,credentials.forworder_email,credentials.password,credentials.type,credentials.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM credentials left join credential_logs on credentials.id=credential_logs.credential_id and credential_logs.type='credential' and credential_logs.user_id='".$id."'");

      $this->set('Credential_for_log2', $Credential_for_log1);

      

      $sale_profile_cre1 = $this->SaleProfiles->query("SELECT sale_profiles.username,sale_profiles.forworded_email,sale_profiles.password,sale_profiles.type,sale_profiles.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM sale_profiles left join credential_logs on sale_profiles.id=credential_logs.credential_id and credential_logs.type='saleProfile' and credential_logs.user_id='".$id."'");


      $this->set('sale_profile_cre_logs', $sale_profile_cre1);
  } else {

      $primary_user = $this->Users->find('all', array('conditions' => array('id' => $id)))->first()->toArray();


      $this->set('primary_user', $primary_user);

      $primary_cerdentail = $this->Credentials->find('all', array('conditions' => array('Credentials.user_id' => $id)));


      $this->set('primary_cerdentail', $primary_cerdentail);

      $secon_cerdentail = $this->Credentials->find('all', array('conditions' => array('FIND_IN_SET(\'' . $id . '\',Credentials.other_alloted_user)')));

      $this->set('secon_cerdentail', $secon_cerdentail);

      $profile_cre = $this->SaleProfiles->find('all', array('fields' => array('SaleProfiles.alloteduserid', 'SaleProfiles.username', 'SaleProfiles.password', 'SaleProfiles.type', 'SaleProfiles.forworded_email', 'SaleProfiles.id'),
        'conditions' => array('FIND_IN_SET(\'' . $id . '\',SaleProfiles.alloteduserid)')));
      $this->set('pro_data', $profile_cre);
      /* Credential Logs from credential_logs table */


      $Credential_for_log1 = $this->Credentials->query("SELECT credentials.username,credentials.forworder_email,credentials.password,credentials.type,credentials.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM credentials left join credential_logs on credentials.id=credential_logs.credential_id and credential_logs.type='credential' and credential_logs.user_id='".$id."'");

      $this->set('Credential_for_log2', $Credential_for_log1);

        //saleProfile Log  

      $sale_profile_cre1 = $this->SaleProfiles->query("SELECT sale_profiles.username,sale_profiles.forworded_email,sale_profiles.password,sale_profiles.type,sale_profiles.username, credential_logs.allocatedon,credential_logs.deallocatedon,credential_logs.user_id,credential_logs.type FROM sale_profiles left join credential_logs on sale_profiles.id=credential_logs.credential_id and credential_logs.type='saleProfile' and credential_logs.user_id='".$id."'");


      $this->set('sale_profile_cre_logs', $sale_profile_cre1);  

  }
}
    #_________________________________________________________________________#

    /**
     * @method      : admin_download
     * @description : Used to download Employee record
     * @param       : type
     * return       : none
     * */
    function admin_download() {

        if (empty($this->params['pass'][0])) {
            $search = $this->Session->read('SESSION_SEARCH');
            $crData = $this->User->find('all', array('conditions' => $search)); //fetching all data
        } else {
            $crData = $this->User->find('all', array('conditions' => array('type' => $this->params['pass'][0]), 'order' => 'first_name ASC')); //fetching all related data                                           
        }
        $this->set('crData', $crData); //Setting Employee Data                             
        $this->viewBuilder()->layout = 'pdf'; //this will use the pdf.ctp layout
        $this->render();
    }

// end of download function
    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : admin_exportci
     * @Purpose: This function is used to show Employee Report in Excel format
     * @Param: none
     * @Return: none
     * */
    function admin_exportci() {
        if (empty($this->params['pass'][0])) {
            $search = $this->Session->read('SESSION_SEARCH');
            $crData = $this->User->find('all', array('conditions' => $search)); //fetching all data
        } else {
            $crData = $this->User->find('all', array('conditions' => array('type' => $this->params['pass'][0]), 'order' => 'first_name ASC')); //fetching all related data                                           
        }
        //setting excel parametres
        $this->set('filename', "UserReport_" . date("Ymd"));
        $this->set('crData', $crData); //Setting Employee Data
        $this->viewBuilder()->layout = "export_xls";
    }
    function admin_credential_detail1() {
     print_r($this->request->data); 

 }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : uploadPic
     * @Purpose: This function is used to upload employee pic
     * @Param: $id
     * @Return: none
     * */
    function uploadPic($id = null) {
        if (!empty($_FILES)) {
            $destination = realpath('../../app/webroot/img/employee_image') . DS;
            $file = $_FILES['uploadfile'];
            $filename = $id . '_' . time() . '.' . strtolower($this->Common->file_extension($file['name']));
            $size = $_FILES['uploadfile']['size'];
            if ($size > 0) {
                $files = $this->Common->get_files($destination, "/^" . $id . "_/i");
                if (!empty($files)) {
                    foreach ($files as $x) {
                        @unlink($destination . $x);
                    }
                }
                if (preg_match("/gif|jpg|jpeg|png/i", $this->Common->file_extension($file['name'])) > 0) {
                    $result = $this->Upload->upload($file, $destination, $filename);
                    echo "success";
                    die;
                }
            }
        }
    }
    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : admin_change_password
     * @Purpose: This function is used to change password
     * @Param: $id
     * @Return: none
     * */


    function admin_change_password() {
        if (!empty($this->request->data)) {
            $user = $this->Session->read('User');
            $this->User->id = $user['id'];
            $someone = $this->User->findById($this->User->id);
            if (md5($this->request->data['change']['oldpassword']) !=
                $someone['User']['password']) {
                $this->User->invalidate('oldpassword', 'Your old password is invalid');
        }
        if ($this->request->data['change']['newpassword'] !=
            $this->request->data['change']['confirmpassword']) {
            $this->User->invalidate('newpassword', 'Your passwords do not match');
        $this->User->invalidate('confirmpassword', 'Your passwords do not match');
    } else {
        $encryptedPassword = md5($this->request->data['change']['newpassword']);
        $this->request->data['User']['password'] = $encryptedPassword;
        $this->request->data['User']['id'] = $this->User->id;
    }
    if ($this->User->save($this->request->data)) {
        $this->flash('Password updated, please login again.', '/users/logout');
    } else {
        $this->render();
    }
}
}

#_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : admin_userid
     * @Purpose: This page will render getuserid page
     * @Param: none
     * @Return: none 
     * */
    function admin_userid() {

        $this->set("title_for_layout", "Get All Users");
        $data1 = $this->Role->find('all');
        $data2 = $this->Permission->find('all');
        $this->set('resultData', $data1);
        $this->set('result', $data2);
    }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : birthday
     * @Purpose: This page will render birthday page
     * @Param: none
     * @Return: none 
     * */
    function birthday() {
        $this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
        if ($this->request->data) {
            $curMonth = $this->request->data['month'];
        }
        echo $currentMonth = (isset($curMonth)) ? $curMonth : date("m");
        $data = $this->Users->find('all', array(
            'fields' => array('id', 'first_name', 'last_name', 'dob', 'status', 'role_id'),
            'conditions' => ["MONTH( dob ) = $currentMonth AND status = 1",'agency_id' => $userSession[3]]
            ));
        $data1 = isset($data)? $data->toArray():array();
        $roles = $this->Roles->find('list',[
            'keyField' => 'id',
            'valueField' => 'role',
            'fields' => ['id', 'role']]);
        $data2 = isset($roles)? $roles->toArray():array();
        $this->set('resultData', $data1);
        $this->set('rolesData', $data2);
    }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : admin_getallusers
     * @Purpose: This page will retreive all users for specific roles
     * @Param: none
     * @Return: none 
     * */
    function admin_getallusers() {
        $users = array();
        if ($this->RequestHandler->isAjax() == true) {
            if (!empty($this->params['form'])) {

                $userid = $this->params['form']['id'];
                $usersall = rtrim($userid, ",");
                $users = explode(",", $usersall);
                $cond = "";

                for ($i = 0; $i < count($users); $i++) {
                    if ($i == (count($users) - 1)) {
                        $char = "";
                    } else {
                        $char = "OR ";
                    }
                    $cond.="FIND_IN_SET ($users[$i],role_id) " . $char;
                }
                $res = $this->User->find('all', array(
                    'fields' => array('User.id', 'User.employee_code', 'User.first_name', 'User.last_name', 'User.username', 'User.status', 'User.role_id'),
                    'conditions' => $cond,
                    'recursive' => 0
                    ));


                if (count($res) > 0) {
                    $html = '<table><tr><th> Name </th><th> Email </th><th> Action </th><tr/>';
                    foreach ($res as $users) {
                        $html .= '<tr><td>' . $users['User']['first_name'] . " " . $users['User']['last_name'] . ' </td>';
                        $html .= '<td> <a href="#" >' . $users['User']['username'] . '</a> </td></tr>';
                        $html .= '<td></td></tr>';
                    }
                    $html .="</table>";
                    echo $html;
                    die;
                } else {
                    echo 'Sorry no user found in this role';
                    die;
                }
            }
        }
    }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : admin_getactions
     * @Purpose: This ajax function will retreive all actions for specific controller .
     * @Param: none
     * @Return: none 
     * */
    function admin_getactions() {
        if ($this->RequestHandler->isAjax() == true) {

            $control = $this->params['form']['controller'];
            if (!empty($this->params['form'])) {
                $cond = "";
                $cond = "controller LIKE '" . $control . "'";
                $res = $this->Permission->find('all', array(
                    'conditions' => $cond,
                    'recursive' => 0
                    ));
                
                if (count($res) > 0) {
                    $html = '<select class="new_styledselect_form">';
                    foreach ($res as $act) {
                        $html .= '<option value=' . $act['Permission']['id'] . '>' . $act['Permission']['action'] . '</option>';
                    }
                    $html .="</select>";
                    echo $html;
                    die;
                } else {
                    echo 'Sorry. No action found for this controller!';
                    die;
                }
            }
        }
    }

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : settings
     * @Purpose: These are hrm settings.
     * @Param: none
     * @Return: none 
     * @who:Gurpreet Kaur
     * */
    function settings() {
        //pr("asdf"); die;
         $this->viewBuilder()->layout('new_layout_admin');
        $this->set("title_for_layout", "HRM Settings");
        $this->set("pageTitle", "HRM Settings");
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
        $credentials = TableRegistry::get('Settings');
        $getSettings = $credentials->find()->where(['Settings.key IN' => [3,4,5,6]])->toArray();
        $p_head = $credentials->find()->where(['Settings.key' => 'process_head','agency_id' => $userSession[3]])->first();
        $h_head = $credentials->find()->where(['Settings.key' => 'hr_head','agency_id' => $userSession[3]])->first();
        $o_head = $credentials->find()->where(['Settings.key' => 'operation_head','agency_id' => $userSession[3]])->first();
        $desg = $credentials->find()->where(['Settings.key' => 'designations','agency_id' => $userSession[3]])->first();
        //pr($p_head); die;
        $this->set("p_head", $p_head);
        $this->set("h_head", $h_head);
        $this->set("o_head", $o_head);
        $this->set("desg", $desg);
        $result = array();
        foreach ($getSettings as $setting) {
         if ($setting->id == 3){
          $result['designations'] = $setting->value; 
          }if ($setting->id == 4){
        $result['process_head'] = $setting->value;
         }
        if ($setting->id == 5){
            $result['hr_head'] = $setting->value;
        }
    if ($setting->id == 6){
     $result['operation_head']= $setting->value;
 }
}
$this->set("resultData", $result);
if ($this->request->data) { 
    if (isset($this->request->data['generateEval']) && !empty($this->request->data['generateEval'])) {
        $credentials = TableRegistry::get('Users');
        $Userdata = $credentials->find('all', [
         'fields'=>['id','report_to','agency_id'],
         'conditions' =>['Users.status' => '1' ,'agency_id' => $userSession[3] ]
         ]);
        $datatosave = array();
        $previousMonth = date("m", strtotime("-1 month"));
        /*pr($previousMonth); die;*/
        $curYear = date("Y");
        $credentials_eva = TableRegistry::get('Evaluations');

        $existedData = $credentials_eva->find()->where(['month' => $previousMonth,'year'=>$curYear,'agency_id' => $userSession[3]])->first();
      //  pr($existedData); die;

        if (count($existedData)> 0) {
            $message="Previous month data exists,so evaluations cannot be enabled";
            $this->Flash->success_new($message);
        } else {
          foreach ($Userdata as $k => $v) {
            $this->request->data['user_id'] = $v->id;
            $this->request->data['agency_id'] = $v->agency_id;
            $this->request->data['senior_id'] = ($v->report_to == '') ? 0 : $v->report_to ;
            $this->request->data['status'] = '0';
            $this->request->data['month'] = $previousMonth;
            $this->request->data['year'] = $curYear;
            $this->request->data['senior_comment'] = "";
            $users = $this->Evaluations->newEntity($this->request->data(), ['validate' => false]);
            $savedData= $this->Evaluations->save($users);
        }

        if ($savedData) {
         $message="Evaluations has been enabled for last month.";
         $this->Flash->success_new($message);
     }
 }
} else {
    //pr($this->request->data); die;
    $process_head = $this->request->data['process_head'];
    $hr_head = $this->request->data['hr_head'];
    $operation_head = $this->request->data['operation_head'];
    $designations = $this->request->data['designations'];
    $credentials = $this->Settings;
    if (!empty($process_head)) {
     $update = ['value' => $process_head];
     $result=$credentials->updateAll($update,["`key`" => 'process_head','agency_id' => $userSession[3]]);
 }
 if (!empty($hr_head)) {
   
  $update = ['value' => $hr_head];
  
  $result=$credentials->updateAll($update,["`key`" => 'hr_head','agency_id' => $userSession[3]]);
  
}
if (!empty($operation_head)) {
   
 $update = ['value' => $operation_head];
 
 $result=$credentials->updateAll($update,["`key`" => 'operation_head','agency_id' => $userSession[3]]);

}

if (!empty($designations)) {

  $update = ['value' => $designations];

  $result=$credentials->updateAll($update,["`key`" => 'designations','agency_id' => $userSession[3]]);

  
}
$this->set("resultData", $result);

if ($result) {
   
  $message="Setting(s) has been updated successfully.";
  $this->Flash->success_new($message);
  
  $this->redirect(array("controller" => "Employees", 
      "action" => "settings",
      ));

}
}
}
}

    #_________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : financereport
     * @Purpose: Display finance report.
     * @Param: none
     * @Return: none 
     * */
    
    function financereport() {
       $this->viewBuilder()->layout(false);
       $users= TableRegistry::get('Users');
       $query = $users->find();
       $query->select([

        'id'=>'Users.id',
        'first_name'=>'Users.first_name',
        'last_name'=>'Users.last_name',
        'pan'=>'Users.pan',
        'account_number'=>'Users.account_number',
        'bankName'=>'Users.bankName',
        'branch'=>'Users.branch',
        'ifsc'=>'Users.ifsc',
        
        'appraised_amount' => 'Appraisals.appraised_amount']) 
       
       ->join([
          'table' => 'appraisals',
          'alias' => 'Appraisals',
          'type'=>'INNER',
          'foreignKey' => 'user_id',
          'conditions' => array ('Appraisals.user_id = Users.id')
          ]);
       
       $this->set('query', $query->toArray());
   }
    #________________________________________________________________________________________________________________________________#

    /**
     * @Date: 26-sep-2016  
     * @Method : employeefinancexal
     * @Purpose: This function is used to show Credential Report in Excel format
     * @Param: none
     * @Return: none
     * */
    
    function employeefinancexal() {
        $this->viewBuilder()->layout(false);
        if (empty($this->params['pass'][0])) {
          
          $session = $this->request->session();
          $this->set('session',$session);
          $search = $session->read("SESSION_ADMIN");

          $users= TableRegistry::get('Users');
          $data = $users->find();
          $data->select([
              'id'=>'Users.id',
              'first_name'=>'Users.first_name',
              'last_name'=>'Users.last_name',
              'pan'=>'Users.pan',
              'account_number'=>'Users.account_number',
              'bankName'=>'Users.bankName',
              'branch'=>'Users.branch',
              'ifsc'=>'Users.ifsc',
              
              'appraised_amount' => 'Appraisals.appraised_amount']) 
          
          ->join([
              'table' => 'appraisals',
              'alias' => 'Appraisals',
              'type'=>'INNER',
              'foreignKey' => 'user_id',
              'conditions' => array ('Appraisals.user_id = Users.id')
              ]);
          
      } else {
       $users= TableRegistry::get('Users');
       $data = $users->find();
       $data->select([
         'id'=>'Users.id',
         'first_name'=>'Users.first_name',
         'last_name'=>'Users.last_name',
         'pan'=>'Users.pan',
         'account_number'=>'Users.account_number',
         'bankName'=>'Users.bankName',
         'branch'=>'Users.branch',
         'ifsc'=>'Users.ifsc',
         
         'appraised_amount' => 'Appraisals.appraised_amount']) 
       
       ->join([
          'table' => 'appraisals',
          'alias' => 'Appraisals',
          'type'=>'INNER',
          'foreignKey' => 'user_id',
          'conditions' => array ('Appraisals.user_id = Users.id')
          ]);
       $this->response->download("employeefinancexal.xls");                                          
   }
   
        //setting excel parametres
   $this->set('filename', "employeefinancexal_" . date("Ymd"));
        $this->set('crData', $data->toArray()); //Setting Credential Data
        $this->viewBuilder()->layout('employeefinancexal_xls');

    }

    #_________________________________________________________________________#

    /**
     * @method      : employeefinancpdf
     * @description : Used to download Employee Financial Report in PDF
     * @param       : type
     * return       : none
     * */
    function employeefinancpdf() {
       $users= TableRegistry::get('Users');
       $data = $users->find();
       $data->select([

         
        'id'=>'Users.id',
        'first_name'=>'Users.first_name',
        'last_name'=>'Users.last_name',
        'pan'=>'Users.pan',
        'account_number'=>'Users.account_number',
        'bankName'=>'Users.bankName',
        'branch'=>'Users.branch',
        'ifsc'=>'Users.ifsc',
        
        'appraised_amount' => 'Appraisals.appraised_amount']) 
       
       ->join([
          'table' => 'appraisals',
          'alias' => 'Appraisals',
          'type'=>'INNER',
          'foreignKey' => 'user_id',
          'conditions' => array ('Appraisals.user_id = Users.id')
          ]);
       
        $this->set('crData', $data->toArray()); //Setting Employee Data                             
        $this->viewBuilder()->layout = 'pdf'; //this will use the pdf.ctp layout
        $this->render();
    }

// end of download function
    #__________________________________________________________________________________________________________________#
    /**
     * @Date: 26-sep-2016  
     * @Method : seatreport
     * @Purpose: Display Seat report.
     * @Param: none
     * @Return: none 
     * */


    function seatreport($key = null) {
        $this->viewBuilder()->layout('new_layout_admin');
        $this->set("title_for_layout", "Seat Report");
        $this->set("pageTitle", "Seat Report");
        $session = $this->request->session();
         $this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];

   $Agencydata = $this->Agencies->find("all" , [
                    'conditions' => ['id' => $userAgency]
                    ])->first();
$agencynmae=$Agencydata->agency_name;
  $this->set('agency',$agencynmae);

        if (!empty($key)) {
          $this->viewBuilder()->layout(false);

          $query=TableRegistry::get('Users');
          $desData12=$query->find();
          $desData12 ->select(['Users.first_name', 'Users.last_name', 'Users.username', 'Users.phone', 'Users.dropbox', 'Users.seat_number', 'Hardwares.item', 'Hardwares.id', 'Hardwares.unique_id'])
          ->where(['Users.status' => '1', 'Users.id' => $key,'Users.agency_id'=>$userAgency])
          ->join([
              'table' => 'hardwares',
              'alias' => 'Hardwares',
              'type'=>'LEFT',
              'foreignKey' => false,
              'conditions' => array ('Users.id = Hardwares.alloted_to')
              ]);
          $desData=$desData12->toArray();
          $desData1 = [];
          $i = 0;
          foreach ($desData as $k => $v) {
            $desData1['first_name'] = $v['first_name'];
            $desData1['last_name'] = $v['last_name'];
            $desData1['dropbox'] = $v['dropbox'];
            $desData1['username'] = $v['username'];
            $desData1['phone'] = $v['phone'];
            $desData1['seat_number'] = $v['seat_number'];
            if ($i == 0) {
                $desData1['Hardwares'] .= ucfirst($v['Hardwares']['item']);
                $desData1['unique_id'] .= $v['Hardwares']['unique_id'];
            } else {
                $desData1['harwares'] .= "," . ucfirst($v['Hardwares']['item']);
                $desData1['unique_id'] .= "," . $v['Hardwares']['unique_id'];
            }
            $i++;
        }
        $this->set('desData', $desData1);
        $this->render('seatdetails');
    }

    $minmax = $this->Users->find()
    ->select(['seat_number'])
    ->where(['Users.status' => '1', 'Users.seat_number >' => '0','Users.agency_id'=>$userAgency])
    ->min('seat_number');
    
    $manmax = $this->Users->find()
    ->select(['seat_number'])
    ->where(['Users.status' => '1', 'Users.seat_number >' => '0','Users.agency_id'=>$userAgency])
    ->max('seat_number');
    
    $minSeatNo = $minmax;
    
    $maxSeatNo = $manmax;


    $oderby =[ 'seat_number' => 'asc' ];
    $cond = ['seat_number <=' => $maxSeatNo['seat_number'], 'seat_number >=' => $minSeatNo['seat_number'],'status'=>1];
    $userData = $this->Users->find()
    ->where($cond)
    ->order($oderby)->toArray();
    if (count($userData) > 0) {
        $a = 0;
        $finalData = [];
        foreach ($userData as $k1 => $v1) {
            $userID = $v1['id'];
            $credentials = TableRegistry::get('Projects');
            $projectData = $credentials->find("all")
            ->where([
                'pm_id' => $userID,
                'OR' => [['engineeringuser' => $userID], ['salesbackenduser' => $userID]  , ['engineeringuser' => $userID] ,
                ['salesfrontuser' => $userID]   ,["FIND_IN_SET($userID,team_id)"]     ],
                'AND'=>[['status'=>'current']],
                
                ]);  
                // not working after this how 
            $projectsInfo = [];
            $j = 0;
            foreach ($projectData as $k => $v) {

                $projectsInfo[$j] = $v['project_name'];
                $j++;
            }
            
            $projectss = implode(",", $projectsInfo);
            $v1['projects'] = $projectss;
            $finalData[$a] = $v1;
            $a++;
        }
        
    }

    $this->set('resultData', $finalData);
}
    /*
     
    /**
     * @Date: 21-sep-2016
     * @Method : credentialself
     * @Purpose: Display Credential.
     * @Param: none
     * @Return: none 
     *@who:Priyanka Sharma
     * */
    
    
    
  function credentialself($id = null) {
      $this->viewBuilder()->layout(false);
      $id = $this->Common->mc_decrypt($id, ENCRYPTION_KEY);

      $primary_user  =  $this->Users->find('all', array(
        'conditions' => array('Users.id'=>$id),
        'fields' => array('Users.username','Users.password')
        ));
      $record=isset($primary_user)? $primary_user->toArray():array();

      $primary_cerdentail  =  $this->Credentials->find('all', array(
        'conditions' => array('Credentials.user_id'=>$id),
        'fields' => array('Credentials.username','Credentials.id','Credentials.password','Credentials.type','Credentials.forworder_email')
        ));

      $ab=isset($primary_cerdentail)? $primary_cerdentail->toArray():array();
      
      $this->set('pri_user', $primary_user);
      $this->set('primary_cerdentail', $primary_cerdentail);
      $profile_cre =$this->SaleProfiles->find('all', array(
       'conditions' => array( 'FIND_IN_SET(\'' . $id . '\',SaleProfiles.alloteduserid)'),
       'fields' => array('SaleProfiles.username','SaleProfiles.id','SaleProfiles.password','SaleProfiles.type','SaleProfiles.forworded_email')
       ));

      $sales=isset($profile_cre)? $profile_cre->toArray():array();
      
      $this->set('pro_data', $profile_cre);
      
      $data  =  $this->Credentials->find("all" , [
          'conditions' => ['FIND_IN_SET(\'' . $id . '\',Credentials.other_alloted_user)']
          ])->all();
      $secon_cerdentail = isset($data)? $data->toArray():array();

      $this->set('secon_cerdentail', $secon_cerdentail);

      $Credential_for_log1 =$this->CredentialLogs->find('all', array(
        'conditions' => array('CredentialLogs.user_id'=>$id,'CredentialLogs.type' => 'credential'),
        'fields' => array('Credentials.username','Credentials.password','Credentials.type','Credentials.forworder_email','CredentialLogs.allocatedon','CredentialLogs.deallocatedon')
        ))->contain(['Credentials']);
      

      $c_log=isset($Credential_for_log1)? $Credential_for_log1->toArray():array();
      
      $this->set('Credential_for_log2', $Credential_for_log1);

      $sale_profile_cre1 =$this->SaleProfiles->find('all', array(
       
        'conditions' => ['CredentialLogs.user_id'=>$id,
        'CredentialLogs.type'=>'saleprofile'
        ],
        'fields' => array('SaleProfiles.username','SaleProfiles.password','SaleProfiles.type','SaleProfiles.forworded_email','SaleProfiles.type','CredentialLogs.allocatedon','CredentialLogs.deallocatedon','CredentialLogs.user_id','CredentialLogs.type') )
      )->contain(['CredentialLogs'])->toArray();
      $this->set('sale_profile_cre_logs', $sale_profile_cre1);
  }



function startup(){
       // Configure::write('debug', 2);
        $session = $this->request->session();
         $this->set('session',$session);
        $session_info = $session->read("SESSION_ADMIN");
       $userAgency=$session_info[3]; 
    if($this->request->data){
        $this->viewBuilder()->layout(false);
         $user  =  $this->Users->find('all', array(
                         'conditions' => ['Users.employee_code'=>$this->request->data['origin'] ]
                     ))->first();

        if(count($user) > 0) {
            $cond=array('Tickets.status'=>1,2);
            $scheduled = $this->Tickets->find('all',[
                             'conditions' => ['type' => 'Scheduled','to_id' => $user->id,'status IN' => [1,2]]
                         ])->all();
            $allmandatory = $this->Tickets->find('all',[
                             'conditions' => ['type' => 'Mandatory','to_id' => $session_info[0],'status IN' => [1,2]]
                         ])->all();
            $mandatory = [];
           // pr($allmandatory); 
            foreach($allmandatory as $m){
                 if(count($mandatory) == 0){
                    if($m['last_replier'] !== intval($session_info[0])){
                            $mandatory = $m;
                        }      
                 }   
                
            }
           //pr($scheduled); die;
            if(count($scheduled) > 0 ){
                $datatopost = [];
                $counter = 0;
                foreach($scheduled->toArray() as $ticket){
                    if(!empty($ticket['ticket_parameters'])){
                          $arr  = json_decode($ticket['ticket_parameters']);
                        $after =  intval($arr->after);

                        $date = new \DateTime();
                        $currentdate = new \DateTime();
                        $newdeadline = new \DateTime();
                        $startdate =  $date->modify("-$after hours");
                        $newdeadline->modify("+$after hours");

                         $time = date_create($ticket['deadline'])->format('H:i:s');
                        if($after > 4){
                          $ddline = date_create($newdeadline->format('Y-m-d H:i:s'))->format('Y-m-d '.$time.'');
                            $stdate = date_create($startdate->format('Y-m-d H:i:s'))->format('Y-m-d '.$time.'');
                          $crted = date('Y-m-d '.$time.'');
                        } else {
                          $ddline = date_create($newdeadline->format('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
                        $stdate =  $startdate;
                          $crted = date('Y-m-d H:i:s');
                        }

                $last_ticket = $this->Tickets->find('all',[
                        'conditions' => ['referencing_ticket' => $ticket->id, 'created >' => $stdate, 'created <=' => $crted]
                ])->first();

                $mainticket = $this->Tickets->find('all',[
                        'conditions' => ['id' => $ticket->id, 'created >' => $stdate, 'created <=' => $crted]
                ])->first();
               // pr($mainticket);
              //  pr($last_ticket); die;

                    if(count($last_ticket) == 0 && count($mainticket) == 0){
                       
                          $ticket['type'] = 'To-Do'; 
                          $ticket['status'] = 1; 
                          $ticket['referencing_ticket'] = $ticket->id;
                          $ticket['deadline'] = $ddline;
                          $ticket['title'] = $ticket['title'] .' ['.$ticket['deadline'].']'; 
                          $ticket['created'] = $crted;
                          $ticket['modified'] = date('Y-m-d H:i:s');
                         // unset($ticket['modified']);
                          unset($ticket['id']);
                          //unset($ticket['created']);
                          $ticket['closedate'] =   date_format(date_create('0000-00-00'),"Y-m-d"); 
                          $datatopost[] = $ticket;
                          $tic = $this->Tickets->newEntity($ticket->toArray(),['validate' =>false]);
                          $saved = $this->Tickets->save($tic);
                         // pr($saved->id); die;
                    } 
                    
                    //pr($counter);
                    //pr(count($scheduled)); die;
                      
                    }
                     if($counter == count($scheduled->toArray())-1 ){

                                 if(count($mandatory) > 0){
                                        $mid = $this->Common->encForUrl($this->Common->ENC($mandatory->id));
                                        $mandatory['eid'] = $mid;
                                        if($mandatory['last_replier'] !== $session_info[0]){
                                            $mandatory['redirect'] = 'yes';
                                        } else {
                                            $mandatory['redirect'] = 'no';
                                        }
                                 }
                                 //pr($mandatory); die;
                                $resultJ = json_encode(['response' => 'success','mandatory' => $mandatory,'currentuser' => $session_info[0] ]);
                                $this->response->type('json');
                                $this->response->body($resultJ);
                                return $this->response;
                        $tcounter = 0;
                     }
                    $counter++;
                }
            } else if (count($mandatory) > 0){
                $mid = $this->Common->encForUrl($this->Common->ENC($mandatory->id));
                $mandatory['eid'] = $mid;
                if($mandatory['last_replier'] !== $session_info[0]){
                    $mandatory['redirect'] = 'yes';
                } else {
                    $mandatory['redirect'] = 'no';
                }
                    $resultJ = json_encode(['response' => 'success','mandatory' => $mandatory,'currentuser' => $session_info[0] ]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;   
            } else {
                    $resultJ = json_encode(['response' => 'success','mandatory' => [],'currentuser' => $session_info[0] ]);
                    $this->response->type('json');
                    $this->response->body($resultJ);
                    return $this->response;
            }
        }
    }

}











} //end