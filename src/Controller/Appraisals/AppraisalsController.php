<?php

namespace App\Controller\Appraisals;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Component\CommonComponent;


class AppraisalsController extends AppController {

	var $name = "Appraisals";
	var $paginate = array();
    var $uses = array('Appraisal', 'User'); // For Default Model

    /*     * ***************************** START FUNCTIONS ************************* */

    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016  
     * @Method : beforeFilter    
     * @Purpose: This function is called before any other function.    
     * @Param: none   
     * @Return: none    
     * */
    function beforeFilter(Event $event) {

    	parent::beforeFilter($event);
            // star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    // end code to check permissions
    	$this->viewBuilder()->layout('layout_admin');
    	$this->set('common', $this->Common);
    	Router::parseNamedParams($this->request);
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}

    }

 /**
     * @Date: Nov,2016 
     * @Method : initialize    
     * @Purpose: This function is called initialize  function.    
     * @Param: none   
     * @Return: none    
     * */
 public function initialize()
 {
 	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
    }



    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016  
     * @Method : index   
     * @Purpose: This function is the default function of the controller    
     * @Param: none    
     * @Return: none     
     * */
    function index() {
    	$this->render('login');
    	if ($this->Session->read("SESSION_USER") != "") {
    		$this->redirect('dashboard');
    	}
    }

    

    #_________________________________________________________________________#

 /**
     * @Date: Nov,2016   
     * @Method : appraisalslist    
     * @Purpose: This function is to show list of salaries given to the employee.   
     * @Param: none   
     * @Return: none     
     * */
 function appraisalslist() {
    $this->viewBuilder()->layout('new_layout_admin');
    $this->viewBuilder()->layout('new_layout_admin');
 	$session = $this->request->session();
    $session_info = $session->read("SESSION_ADMIN");
     $userAgency=$session_info[3];
    $this->set('session_info',$session_info);
 	$this->set('session',$session);
 	$this->set('title_for_layout', 'Appraisals Listing');
 	$this->set("pageTitle", "Appraisals Listing");
 	$this->set("search1", "");
 	$this->set("search2", "");
 	$criteria = "1";
 	$lastSearch = "";
 	if (isset($this->request->data['User']) || !empty($this->request->query)) {
 		if (!empty($this->request->data['User']['fieldName']) || isset($this->request->query['field'])) {
 			if (trim(isset($this->request->data['User']['fieldName'])) != "") {
 				$this->set("search1", $this->request->data['User']['fieldName']);
 				if (trim($this->request->data['User']['fieldName']) == "Users.first_name") {
 					$search1 = "Users.first_name";
 				}else {
 					$se = trim($this->request->data['User']['fieldName']);
 					$search1='"'.$se.'"';
 				}
 			} elseif (isset($this->request->query['field'])) {
 				$search1 = trim($this->request->query['field']);
 				$this->set("search1", $search1);
 			}
 		}
 		if (isset($this->request->data['User']['value1'])  || isset($this->request->query['value'])) {
 			if (isset($this->request->data['User']['value1']) ) {
 				$search2 = ($this->request->data['User']['fieldName'] != "User.status") ? trim($this->request->data['User']['value1']) : $this->request->data['User']['value2'];
 			} elseif (isset($this->request->query['value'])) {
 				$search2 = trim($this->request->query['value']);
 			}
 			$this->set("search2", $search2);
 		}

 		if (!empty($search1) && (!empty($search2) || $search1 == "Users.status")) {
 			$query  =    $this->Appraisals->find("all" , [
 				'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'Appraisals.agency_id'=>$userAgency],
 				])->contain(['Users','Supervisor']);
 		} else {
 			 $query  =    $this->Appraisals->find("all" , [
                'conditions' => ['Appraisals.agency_id'=>$userAgency],
                ])->contain(['Users','Supervisor']);
 			$this->set("search1", "");
 			$this->set("search2", "");
 		}

 	} else {

    $query  =    $this->Appraisals->find("all" , [
                'conditions' => ['Appraisals.agency_id'=>$userAgency],
                ])->contain(['Users','Supervisor']);
                        // if action isaccessed without searching or sorting
 		/*$query  = $this->Appraisals->find('all')->contain(['Users','Supervisor']);  */

 	}
 	$urlString = "/";
 	if (isset($this->request->query)) {

 		$completeUrl = array();

 		if (!empty($this->request->query['page']))
 			$completeUrl['page'] = $this->request->query['page'];

 		if (!empty($this->request->query['sort']))
 			$completeUrl['sort'] = $this->request->query['sort'];

 		if (!empty($this->request->query['direction']))
 			$completeUrl['direction'] = $this->request->query['direction'];

 		if (!empty($search1))
 			$completeUrl['field'] = $search1;

 		if (isset($search2))
 			$completeUrl['value'] = $search2;

 		foreach ($completeUrl as $key => $value) {
 			$urlString.= $key . ":" . $value . "/";
 		}
 	}
 	$this->set('urlString', $urlString);
 	if(isset($this->request->data['publish'])){      
 		$setValue = 1;
 		$messageStr = "Selected appraisal(s) have been activated.";              
 	}elseif(isset($this->request->data['unpublish'])){
 		$setValue = 0;           
 		$messageStr = "Selected appraisal(s) have been deactivated.";            
 	}                     
 	if(isset($this->request->data['publish']) || isset($this->request->data['unpublish'])) {
 		if(isset($this->request->data['IDs'])){          
 			$saveString = implode("','",$this->request->data['IDs']);      
 		}
 		if($saveString != ""){   
 			$credentials = TableRegistry::get('Appraisals');
 			$update = ['status' => $setValue];
 			$credentials->updateAll($update,['id IN' => $this->request->data['IDs']]);
 			$this->Flash->success_new($messageStr,'layout_success');
 			return $this->redirect( ['prefix'=>'appraisals' ,'controller' => 'appraisals', 'action' => 'appraisalslist']);
 		}
 	}

 if(isset($query)){
  $query=$query;
}else{
  //$query=$this->Appraisals->find("all");
}
$paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/100);
$this->set('paginatecount',$paginate);
$data = $this->paginate($query,[
 		'page' => 1, 'limit' => 100,
        'order' => ['Appraisals.appraisal_date' => 'desc','Appraisals.appraised_amount'=>'desc'],
 		'paramType' => 'querystring'
 		]);

foreach($data as $d){      
  $d['id'] = $d->id; 
    $d['encryptedid'] = $this->Common->encForUrl($this->Common->ENC($d->id));   
   $arr[] = $d; 
  }
 $this->set('resultData', $arr);
    if($this->request->is('ajax')){
          if(isset($this->request->query['page'])){
             $pageing=$this->request->query['page'];
          }else{
            $pageing="";
          }
        echo json_encode(['resultData'=>$arr,'page'=>$pageing,'paginatecount'=>$paginate]);
         die;
      }
 }


    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016   
     * @Method : add    
     * @Purpose: This function is used for activate and deactivate  
     * @Param: $id   
     * @Return: none    
     * @Return: none 
     * @Author: Gurpreet  
     * */
 function appstatus(){
  if($this->request->is('ajax')) {
     $id=$this->Common->DCR($this->Common->dcrForUrl($this->request->data['menuOrigin']));
      $data['status'] = $this->request->data['eORd']; // status
      if($this->request->data['eORd'] =='Yes') {
      $setValue = 1;
      $messageStr = 'activate';
      }elseif ($this->request->data['eORd'] =='No') {
      $setValue = 0;
      $messageStr = 'deactivate';
      }
     $users = TableRegistry::get('Appraisals');
     $update = ['status' => $setValue];
     $record= $users->updateAll($update,['id IN' => $id]);
      if($record == 1){
          $status = json_encode(['status' => 'success','msg'=>$messageStr]);
        }else{
          $status = json_encode(['status' => 'failed']);
        }
        echo $status;
        die;
      }
  }

    #_________________________________________________________________________#

    /**
     * @Date: Nov,2016   
     * @Method : add    
     * @Purpose: This function is to add/edit Appraisal from admin section.   
     * @Param: $id   
     * @Return: none    
     * @Return: none 
     * @Author: Gurpreet  
     * */
    function add() {

    $this->viewBuilder()->layout('new_layout_admin');
    	$this->set('title_for_layout', 'Add Appraisal');
    	$this->pageTitle = "Add Appraisal";
    	$this->set("pageTitle", "Add Appraisal");
    	$mode = "add";
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
        $userAgency = $session_info[3];
        $this->set('userAgency',$userAgency);
    	if ($this->request->data) {
    		$credentials= TableRegistry::get('Appraisals');
    		$res =  $credentials->find("all")
    		->where(['user_id' => $this->request->data['user_id']])
    		->order(['appraisal_date' => 'DESC'])->first();
    		$User_cred= TableRegistry::get('Users');
    		$existedData = $User_cred->find()->where(['id' =>  $this->request->data['user_id']])->first();
    		if (empty($res)){
    			$User_cred= TableRegistry::get('Users');
    			$res1 = $User_cred->find()->where(['id' =>  $this->request->data['user_id']])->first();
    		}
    		$entities = $this->Appraisals->newEntity($this->request->data());
    		if(empty($entities->errors())){
    			$last_appr_date = (empty($res['appraisal_date'])) ? $res1['doj'] : $res['appraisal_date'];
    			$this->request->data['evaluation_from'] = $last_appr_date;
    			$this->request->data['evaluation_to'] = date("Y-m-d", strtotime($this->request->data['evaluation_to1']));
    			$this->request->data['next_review_date'] = date("Y-m-d", strtotime($this->request->data['next_review_date1']));
    			$this->request->data['effective_from'] = date("Y-m-d", strtotime($this->request->data['effective_from1']));
    			$this->request->data['appraisal_date'] = date("Y-m-d", strtotime($this->request->data['appraisal_date1']));
    			$this->request->data['last_updated'] = $userID;
                $this->request->data['agency_id']=$userAgency;
    			$users = $this->Appraisals->newEntity($this->request->data(), ['validate' => true]);
    			$a=$this->Appraisals->save($users);

    			$this->Flash->success_new("Appraisal has been created successfully.");
    			return $this->redirect( ['action' => 'appraisalslist']);
    		}else{
    			$this->set("errors", $entities->errors());
    		}
    	}
    }

#_________________________________________________________________________#

    /**
     * @Date: NOv,2016   
     * @Method : edit   
     * @Purpose: This function is to add/edit Appraisal from admin section.   
     * @Param: $id   
     * @Return: none    
     * @Return: none  
     * */

    function edit($id = null) {

       $this->viewBuilder()->layout('new_layout_admin');
    	$this->set('title_for_layout', 'Edit Appraisal');
    	$this->pageTitle = "Edit Appraisal";
    	$this->set("pageTitle", "Edit Appraisal");
    	$mode = "edit";
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$readData = $session->read("SESSION_ADMIN");
    	$userID = $readData[0];
          $this->set('userAgency',$readData[3]);
    	$AppraisalsData = $this->Appraisals->get($id);
    	$this->set('AppraisalsData',$AppraisalsData);
    	$entity = $this->Appraisals->get($id);
    	$credentials = TableRegistry::get('Appraisals');
    	$FindDAta = $credentials ->find()->where(['id' => $entity['id']])->first();
    	$this->set('data',$FindDAta);
    	if ($this->request->data) { 
             $this->request->data['evaluation_to'] = date("Y-m-d", strtotime($this->request->data['evaluation_to1']));
                $this->request->data['next_review_date'] = date("Y-m-d", strtotime($this->request->data['next_review_date1']));
                $this->request->data['effective_from'] = date("Y-m-d", strtotime($this->request->data['effective_from1']));
                $this->request->data['appraisal_date'] = date("Y-m-d", strtotime($this->request->data['appraisal_date1']));           
             $this->request->data['last_updated'] = $userID;
             $entity = $this->Appraisals->get($id);
    		$credentials = TableRegistry::get('Appraisals');
    		$entities = $credentials->patchEntity($entity,$this->request->data());
    		if(empty($entities->errors())){
    			$credentials = TableRegistry::get('Appraisals');
    			$this->Appraisals->save($entities);
    			$this->Flash->success_new("Appraisal has been Updated successfully.");
    			return $this->redirect(
    				['prefix'=>'appraisals' ,'controller' => 'appraisals', 'action' => 'appraisalslist']
    				);
    		}else{
    			$this->set("errors", $entities->errors());
    		}
    	}
    	$this->set("id", $id);
    }

    #_____________________________________________________________________________________________# 

    /**
     * @Date: NOv,2016  
     * @Method : delete    
     * @Purpose: This function is to delete apprasials from admin section. * @Param: none 
     * @Return: none  
     * @Return: none     
     * */


 function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
       $project = $this->Appraisals->get($id);
      if ($this->Appraisals->delete($project)) {
                echo json_encode(['status'=> 'success','page'=>'Appraisals']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 
  

    #_____________________________________________________________________________________________# 

    /**
     * @Date: Nov,2016   
     * @Method : userappraisallist    
     * @Purpose: This function is to list employee specific apprasials .   
     * @Param: none 
     * @author: Neelam Thakur  
     * @Return: none     
     * */
    function userappraisallist() {
        $this->viewBuilder()->layout('new_layout_admin');
    	$this->set('title_for_layout', 'My Appraisal');
    	$this->pageTitle = "My Appraisal";
    	$this->set("pageTitle", "My Appraisal");
    	$session = $this->request->session();
    	$this->set('session',$session);
    	$session_info = $session->read("SESSION_ADMIN");
    	$userID = $session_info[0];
    	$data = $this->Appraisals->find("all", array(
    		'conditions' => array('user_id' => $userID),
    		'order' => array('Appraisals.effective_from' => 'desc')
    		))->contain(['Supervisor']);
    	$results = isset($data)? $data->toArray():array();
    	$this->set('resultData', $results);
    }

    #_____________________________________________________________________________________________# 

    /**
     * @Date: Nov,2016
     * @Method : appraisaldetails
     * @Purpose: This function is to list specific appraisal deatils .   
     * @Param: appraisal id 
     * @author: Neelam Thakur  
     * @Return: none     
     * */
    function details($id = null, $agreebox = null) {

    	$this->viewBuilder()->layout(false);
    	if (!empty($agreebox)) {
    		$this->set('id', $id);
    		$this->render('agreebox');
    	}
    	$data=$this->Appraisals->find('all')
    	->where(['Appraisals.id' => $id])
    	->contain(['Users','Supervisor'])->toArray();
    	$this->set('resultData', $data);

        if ($this->request->is('ajax')) {  
            if ($this->params['form']['choice'] == "agree") {
                $user_comment = $this->params['form']['user_comment'];
                $result = $this->Appraisals->updateAll(
                    array('status' => "'1'", 'user_comment' => "$user_comment"), array('id' => $this->params['form']['id'])
                    );
            }
            if ($result) {
                $this->Flash->success('Thanks for making your choice');
                echo "1";
                die;
            }
        }

    }
     /**
     * @Date: Nov,2016
     * @Method : comment
     * @Purpose:    
     * @author: Neelam Thakur  
     * @Return: none     
     * */
     function comment() {
     	if ($this->request->is('ajax')) {  
              // pr($this->request->data); die;
     		if ($this->request->data['choice'] == "agree") {
                
     			$user_comment = $this->request->data['user_comment'];
     			$result = $this->Appraisals->updateAll(
     				array('status' => 1, 'user_comment' => "'$user_comment'"), array('id' => $this->request->data['id'])
     				);
     		}
     		if ($result) {
     			$this->Flash->success("Thanks for making your choice.");
     			echo "1";
     			die;
     		}
     	}
     }
 /**
     * @Date: Nov,2016
     * @Method : agree
     * @Purpose:    
     * @author: Neelam Thakur  
     * @Return: none     
     * */
 function agree($id = null) {
 	$this->viewBuilder()->layout(false);
 	if (!empty($id)) {
 		$this->set('id', $id);
 		$this->render('agreebox');
 	}
 	if ($this->request->is('ajax')) { 

 		if ($this->params['form']['choice'] == "agree") {
            
 			$user_comment = $this->params['form']['user_comment'];
 			$result = $this->Appraisals->updateAll(
 				array('status' => "'1'", 'user_comment' => "'$user_comment'"), array('id' => $this->params['form']['id'])
 				);
 		}
 		if ($result) {
 			$this->Flash->success('Thanks for making your choice');
 			echo "1";
 			die;
 		}
 	}

 	$data=$this->Appraisals->find('all')
 	->where(['Appraisals.id' => $id])
 	->contain(['Users','Supervisor'])->toArray();
 	$this->set('resultData', $data);
 }
    #_______________________________________________________________________________________________________# 

    /**
     * @Date: Nov,2016    
     * @Method : pending
     * @Purpose: This function is to list all pending appraisals .   
     * @Param: none
     * @author: Neelam Thakur  
     * @Return: none     
     * */
    function pending() {
        $session = $this->request->session();
        $this->set('session',$session);
        $readData = $session->read("SESSION_ADMIN");
    	$this->viewBuilder()->layout(false);
    	$data = $this->Appraisals->find("all", array(
    		'conditions' => array('Appraisals.status' => '1','Appraisals.agency_id' => $readData[3]),
    		'order' => array('Appraisals.created' => 'desc')
    		))->contain(['Users','Supervisor']);
    	$uID = '0';
    	$finalArray = array();
    	foreach ($data as $appData) {

    		if ($uID != $appData['user_id']) {
    			$uID = $appData['user_id'];
    			$finalArray[$uID] = $appData;
    			$finalArray[$uID]['user'] = $appData['user'];
    			$finalArray[$uID]['supervisor'] = $appData['supervisor'];
    		}
    	}
    	$this->set('resultData', $finalArray);
    }

    #_________________________________________________________________________#  
    /**
     * @Date: NOv,2016      
     * @Method : incoming    
     * @Purpose: This function is to show the incoming appraisal.   
     * @Param: none                                                       
     * @Return: none                                                  
     * */

    function incoming() {
        $session = $this->request->session();
        $this->set('session',$session);
        $readData = $session->read("SESSION_ADMIN");
    	$this->viewBuilder()->layout(false);

    	if($this->request->data){
    		$start_month = $this->request->data['month1'];
    		$start_year = $this->request->data['year1'];
    		if (empty($start_month)) {
    			$month = date("m", strtotime("+1 months"));
    		} else {
    			$month=$start_month;
    		}
    		if (empty($start_year)) {
    			if (date('m') == 12) {
    				$year = date("Y", strtotime("+1 years"));
    			} else {
    				$year = date('Y');
    			}
    		} else {
    			$year = $start_year;
    		}
    		$result = $year . "-" . $month . "-" . "01";
    		$result23 = $year . "-" . $month . "-" . "01" . " 00:00:00";
    		$result2 = date('Y-m-d', strtotime('+1 month -1 second', strtotime($result23)));
    		$this->set('start_month', $start_month);
    		$this->set('start_year', $start_year);
    		$resultmonth = $year . "-" . "01" . "-" . "01";
    		$resultyear = $year . "-" . "12" . "-" . "31" . " 00:00:00";
    		if (empty($start_month) && empty($start_year)) {
    			$resultData = $this->Appraisals->find("all",[
    				'conditions' => ['Users.status' => 1, 'Appraisals.next_review_date  >=' => $result, 'Appraisals.next_review_date  <=' => $result2 ,'Appraisals.agency_id' => $readData[3]],
    				'order' => 'Appraisals.next_review_date DESC'
    				])->contain('Users','Appraisals');
    		} elseif (!empty($start_month) && !empty($start_year)) {
    			$resultData = $this->Appraisals->find("all",[
    				'conditions' => ['Users.status' => 1, 'Appraisals.next_review_date  >=' => $result, 'Appraisals.next_review_date  <=' => $result2 ,'Appraisals.agency_id' => $readData[3]],
    				'order' => 'Appraisals.next_review_date DESC'
    				])->contain('Users','Appraisals');
    		} elseif (!empty($start_month) && empty($start_year)) {
    			$resultData = $this->Appraisals->find("all",[
    				'conditions' => ['Users.status' => 1, 'Appraisals.next_review_date  >=' => $result, 'Appraisals.next_review_date  <=' => $result2 ,'Appraisals.agency_id' => $readData[3]],
    				'order' => 'Appraisals.next_review_date DESC'
    				])->contain('Users','Appraisals');
    		} elseif (empty($start_month) && !empty($start_year)) {
    			$resultData = $this->Appraisals->find("all",[
    				'conditions' => ['Users.status' => 1, 'Appraisals.next_review_date  >=' => $resultmonth, 'Appraisals.next_review_date  <=' => $resultyear ,'Appraisals.agency_id' => $readData[3]],
    				'order' => 'Appraisals.next_review_date DESC'
    				])->contain('Users','Appraisals');
    		}
    		$this->set('resultData', $resultData);
    	}

    }

    #_______________________________________________________________________________________________________# 

    /**
     * @Date: Nov,2016  
     * @Method : salaryreport
     * @Purpose: This function is to salary report .   
     * @Param: none  
     * @Return: none     
     * */

    function salaryreport() {
    	$this->viewBuilder()->layout(false);
        $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session->read("SESSION_ADMIN");
    	if(isset($this->request->data['user_id'])){ 
    		$selectedUser=$this->request->data['user_id'];
    		$start_date=$this->request->data['start_date'];
    		$end_date=$this->request->data['end_date'];
    		$dateRange=$this->request->data['select'];
    		$current=date('Y-m-d');
    	}else{
    		$selectedUser="";
    		$start_date="";
    		$end_date="";
    		$dateRange="";
    		$current="";
    	}
    	$this->set('fromDate',$start_date);
    	$this->set('toDate',$end_date);
    	$this->loadComponent('Common');
    	$options = $this->Common->getDateRange();
    	$this->set('options', $options);
    	if(empty($selectedUser) && empty($start_date)&&empty ($end_date))
    	{
    		$data=$this->Appraisals->find() 
    		->select(['Appraisals.appraised_amount','Users.first_name','Users.last_name']) 
    		->group('Appraisals.user_id')
    		->where(['Appraisals.status' => '1','Appraisals.agency_id' => $userSession[3]]) 
    		->join([
    			'table' => 'users',
    			'alias' => 'Users',
    			'type'=>'INNER',
    			'foreignKey' => false,
    			'conditions' => array ('Appraisals.user_id = Users.id')
    			])->toArray();
    	} elseif (empty($selectedUser) && !empty($start_date) && !empty($end_date)) {
    		$data=$this->Appraisals->find() 
    		->select(['Appraisals.appraised_amount','Users.first_name','Users.last_name']) 
    		->group('Appraisals.user_id')
    		->where(['Appraisals.effective_from >= '=> $start_date,'Appraisals.effective_from <='=>$end_date,'Appraisals.status' => '1','Appraisals.agency_id' => $userSession[3]]) 
    		->join([
    			'table' => 'users',
    			'alias' => 'Users',
    			'type'=>'INNER',
    			'foreignKey' => false,
    			'conditions' => array ('Appraisals.user_id = Users.id')
    			])->toArray();
    	} elseif (!empty($selectedUser) && $dateRange != 'select') {
    		$data=$this->Appraisals->find() 
    		->select(['Appraisals.appraised_amount','Appraisals.effective_from','Users.first_name','Users.last_name']) 
    		->group('Appraisals.user_id')
    		->where(['Appraisals.effective_from >= '=> $start_date,'Appraisals.effective_from <='=>$end_date,'Appraisals.status' => '1','Users.id' => $selectedUser,'Appraisals.agency_id' => $userSession[3]]) 
    		->join([
    			'table' => 'users',
    			'alias' => 'Users',
    			'type'=>'INNER',
    			'foreignKey' => false,
    			'conditions' => array ('Appraisals.user_id = Users.id')
    			])->toArray();
    	} elseif (!empty($selectedUser) && $start_date == $current) {
    		$data=$this->Appraisals->find() 
    		->select(['Appraisals.appraised_amount','Appraisals.effective_from','Users.first_name','Users.last_name']) 
    		->group('Appraisals.user_id')
    		->where(['Appraisals.status' => '1', 'Appraisals.user_id' => $selectedUser,'Appraisals.agency_id' => $userSession[3]]) 
    		->join([
    			'table' => 'users',
    			'alias' => 'Users',
    			'type'=>'INNER',
    			'foreignKey' => false,
    			'conditions' => array ('Appraisals.user_id = Users.id')
    			])->toArray();
    	} else {
    		$data=$this->Appraisals->find() 
    		->select(['Appraisals.appraised_amount','Users.first_name','Users.last_name']) 
    		->group('Appraisals.user_id')
    		->where(['Appraisals.effective_from >= '=> $start_date,'Appraisals.effective_from <='=>$end_date,'Appraisals.status' => '1','Users.id'=>$selectedUser,'Appraisals.agency_id' => $userSession[3]]) 
    		->join([
    			'table' => 'users',
    			'alias' => 'Users',
    			'type'=>'INNER',
    			'foreignKey' => false,
    			'conditions' => array ('Appraisals.user_id = Users.id')
    			])->toArray();
    	}
    	$this->set('resultData', $data);
    }
}
