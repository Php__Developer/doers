<?php
namespace App\Controller\payments;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
	class PaymentsController extends AppController
	{	
		//var $name       	=  "Payments";	
		/**	
		* Specifies helpers classes used in the view pages	
		* @access public	
		*/	
	//	var $helpers    	=  array('Html', 'Form', 'Javascript', 'Session','General');
		
		/**
		* Specifies components classes used
		* @access public
		**/	
	//	var $components 	=  array('RequestHandler','Email','Common');	
		
	//	var $paginate		=  array();	
	//	
	//	var $uses       	=  array('Payment','Contact','User','Project');  // For Default Model
		
		/******************************* START FUNCTIONS **************************/

		/**	* @Date: 28-Dec-2011	
		* @Method : beforeFilter	
		* @Purpose: This function is called before any other function.	
		* @Param: none   
		* @Return: none 
		**/	
	function beforeFilter(Event $event){	
		parent::beforeFilter($event);	
			// star to check permission
			$session = $this->request->session();
			$userSession = $session->read("SESSION_ADMIN");
			$controller=$this->request->params['controller'];
			$action=$this->request->params['action'];
			$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
	if($permission!=1){
            $this->Flash->error("You have no permisson to access this Page.");

    return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
	}
    // end code to check permissions
			if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){
				$this->checkUserSession();
			}else{
			$this->viewBuilder()->layout="layout_front";
			}
			Router::parseNamedParams($this->request);
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		
		if($userSession==''){
		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
		}    
	}				
		#_________________________________________________________________________#
		
		/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_index	
		* @Purpose: This is the default function of the administrator section for users	
		* @Param: none
		* @Return: none 	
		**/
		public function initialize()
    {
        parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); 
        $this->loadModel('Evaluations'); 
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
  		$this->loadModel('Leads'); 
  		$this->loadModel('Payments');
  		$this->loadModel('Projects');
    }
		function admin_index() {	
			$this->render('admin_login');	
			if($this->Session->read("SESSION_ADMIN") != ""){	
			$this->redirect('dashboard');		
			}
		}
		
		#_________________________________________________________________________#
		/**	
		* @Date: 28-Dec-2011
		* @Method : admin_list	
		* @Purpose: This function is to show list of Payments in system.	
		* @Param: none
		* @Return: none 	
		**/	
		//by anamika

	function paymentslist(){     
 
 	 	$this->viewBuilder()->layout('new_layout_admin');
		$this->set('title_for_layout','Payments Listing'); 
		$this->set("pageTitle","Payments Listing");     
		$this->set("search1", "");                          
		$this->set("search2", "");
		$criteria = "1"; //All Searching	
		$session = $this->request->session();
        $this->set('session',$session);  
		$userSession = $session->read("SESSION_ADMIN");
		$session->delete('SESSION_SEARCH');	  
		if(isset($this->request->data)) {
			//pr($this->request->data); die;
				$search2 = ['project_id !=""','Payments.agency_id' => $userSession[3]];
		if(!empty($this->request->data['project_name'])){
				$cond = ['Projects.agency_id' => $userSession[3],'Projects.project_name Like' => '%'.$this->Common->sanitizeInput($this->request->data['project_name']).'%'];
				$search2 = array_merge($search2, $cond);
			}
		if(!empty($this->request->data['note'])){        
		$cond = ['Payments.agency_id' => $userSession[3],'Payments.note' => '%'.$this->Common->sanitizeInput($this->request->data['note']).'%'];
					$search2 = array_merge($search2, $cond);
				}
				if( !empty($this->request->data['sdate']) &&  !empty($this->request->data['edate']) ){
					$create=date_create($this->request->data['sdate']);
					$to = $create->format('Y-m-d');
					$create=date_create($this->request->data['edate']);
					$from = $create->format('Y-m-d');
					$cond = ['Payments.agency_id' => $userSession[3],'Payments.payment_date >=' => $to, 'Payments.payment_date <=' => $from];
					$search2 = array_merge($search2, $cond);
				}
				if(!empty($this->request->data['start_amount']) &&  !empty($this->request->data['last_amount'])){
					$cond = ['Payments.agency_id' => $userSession[3],'Payments.amount >=' => $this->Common->sanitizeInput($this->request->data['start_amount']), 'Payments.amount <=' => $this->Common->sanitizeInput($this->request->data['last_amount'])];
					$search2 = array_merge($search2, $cond);
				}
				if(!empty($this->request->data['tds_stramount']) &&  !empty($this->request->data['tds_lastamount'])){
					
					$cond = ['Payments.agency_id' => $userSession[3], 'Payments.tds_amount >=' => $this->Common->sanitizeInput($this->request->data['tds_stramount']), 'Payments.tds_amount <=' => $this->Common->sanitizeInput($this->request->data['tds_lastamount'])];
					$search2 = array_merge($search2, $cond);
				}
					$this->set("search2",$search2);      
		}	
	//	pr($search2); die;
		if((!empty($search2))){
			$query  =    $this->Payments->find("all" , [
                  'conditions' => $search2,
               ])->contain(['Projects']);
           
            $result = isset($query)? $query->toArray():array();
			$criteria = $search2; 		
			$session->write('SESSION_SEARCH', $criteria);	
		}else{
			$query  =  $this->Payments->find("all",[
					['agency_id' => $userSession[3]]
				])->contain(['Projects']);
			$this->set("search2","");
		}	
     
 		$urlString = "/";
	      	if (isset($this->request->query)) {
	        $completeUrl = array();
	        if (!empty($this->request->query['page']))
	            $completeUrl['page'] = $this->request->query['page'];

	        if (!empty($this->request->query['sort']))
	            $completeUrl['sort'] = $this->request->query['sort'];

	        if (!empty($this->request->query['direction']))
	            $completeUrl['direction'] = $this->request->query['direction'];

	        foreach ($completeUrl as $key => $value) {
	            $urlString.= $key . ":" . $value . "/";
        }
    } 
		$this->set('urlString',$urlString);        
		  
		if(!empty($setValue)){                            
	    
	    if(isset($this->request->query['IDs'])){          
	      $saveString = implode("','",$this->request->query['IDs']);      
	      }
	    
	    if($saveString != ""){                                          
	      
	      $this->Payment->updateAll($setValue,"Payment.id in ('".$saveString."')");                                                                 
	      $session->setFlash($messageStr, 'layout_success');                          
	     	 }
	      }

      	$this->set('urlString', $urlString);
      	 $completedata = isset($query)? $query->toArray():array();

        $data = $this->paginate($query,[
              'page' => 1, 'limit' => 100,
              'order' => ['Payments.payment_date' => 'desc','Projects.project_name' => 'asc'],
            ]);
        $this->set('completedata', $completedata);
        $this->set('resultData', $data);
        $this->set('pagename', 'paymentslist');
	}                                                                      
	
		#_________________________________________________________________________#

		/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_add	
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		
	function add() {		
		 	$this->viewBuilder()->layout('new_layout_admin');
			$session = $this->request->session();
		    $userSession = $session->read("SESSION_ADMIN");
		    $this->set('session',$session);	
			$this->set('title_for_layout','Add Payment');
			$this->pageTitle = "Add Payment";		
			$this->set("pageTitle","Add Payment");
			$mode = "add";
		if($this->request->data){
            if(!empty($this->request->data['payment_date'])){
            $date =  date_format(date_create($this->request->data['payment_date']),"Y-m-d");    
            } else{
            $date =  date("Y-m-d") ;    
            }
            $this->request->data['payment_date'] = $date;
            $this->request->data['agency_id'] = $userSession[3];
            $Paymentss = $this->Payments->newEntity($this->request->data());
			if(empty($Paymentss->errors())){
 				$this->Payments->save($Paymentss);
				$this->Flash->success_new("Payments has been created successfully.",'layout_success');
				$this->redirect(array('controller' => 'Payments', 'action' => 'paymentslist'));
        }else{
          $this->set("errors", $Paymentss->errors());
        }
      }     
 	 }                                                        


		#_________________________________________________________________________#
		
			/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_quickadd	
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	function quickadd($id = null) {	
		//Configure::write('debug', 2);
		$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));
		$session = $this->request->session();
    	$this->set('session',$session);	
		$this->viewBuilder()->layout(false);
        $userSession = $session->read("SESSION_ADMIN");
			if(!empty($id)){  
				//pr($userSession); die;
				$res = $this->Projects->find('all',['conditions'=>['Projects.id'=>$id] ])->first();
				$this->request->data['Payment']['project_name'] =$res['project_name']; 
				$this->request->data['Payment']['project_id'] = $id ;
				$isallowed = $this->Common->verifyprojectpermission($id,$userSession,'quickadd_payment');
				//$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
					if($isallowed!=1){
			            return $this->redirect(
							        ['controller' => 'StaticPages', 'action' => 'customerror','prefix'=> 'static_pages']
							    );
					}
				}

			if($this->request->is('ajax')==true){
				//$sessionAry  = $session->read("SESSION_ADMIN");
				$data['Payment']['entry_id']         = $userSession['0']; 
				$data['Payment']['project_id']       = (is_numeric($this->request->data['project_id']) ) ? $this->request->data['project_id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['project_id'])); 
				$data['Payment']['amount']           =$this->request->data['amount']; 
				$data['Payment']['responsible_id']   =$this->request->data['responsible_id']; 
				$data['Payment']['currency']         =$this->request->data['currency']; 
				$data['Payment']['payment_date']     = date_create($this->request->data['payment_date'])->format('Y-m-d');
				$data['Payment']['tds_amount']       =$this->request->data['tds_amount']; 
				$data['Payment']['note']             =$this->request->data['note']; 
				$data['Payment']['billing_id'] = 0;
				$data['Payment']['agency_id']= $userSession[3];
				//pr($data['Payment']); die;
				$Payments  = $this->Payments->newEntity($data['Payment']); 
			if($this->Payments->save($Payments)){
				$this->Flash->success_new("Payment has been created successfully.");		  
				$resultJ = json_encode(['response' => 'success']);
				
			}else{
				$this->Flash->error("Data Not Saved Please Try Again.");
				$resultJ = json_encode(['response' => 'failed']);
			} 
				$this->response->type('json');
				$this->response->body($resultJ);
				return $this->response;   	
			}	                                             
		}    

		#_________________________________________________________________________#
			/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_quickadd	
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	function quickview($id = null) {	
		$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
		$session = $this->request->session();
    	$this->set('session',$session);	
		$this->viewBuilder()->layout(false);
		//pr($id); die;
			if(!empty($id) && is_numeric($id)) {
		$this->Payments->find('all')->contain(['Projects','Users']);
		$project = $this->Projects->get($id);
		$project_name = $project->project_name;
		$result = $this->Payments->find('all',[
						'conditions'=>['Projects.id'=> $id] 
						])
				        ->order(['payment_date' => 'desc'])
				        ->contain(['Projects','Users'])->toArray();
	
		foreach($result as $res){
					$project_name = $res['Projects']['project_name'];
 				}
		$currency =array(
						'usd'    =>  'USD', 
						'aud'    => 'AUD',
						'cad'    => 'CAD',
						'inr'    => 'INR'
                      );
		$resource = $this->Common->getuser(); 
			} 
		if($this->request->is('ajax')==true){
			
		if(isset($this->request->data['key']) && $this->request->data['key'] == 'delete'){
		$res = $this->Payments->deleteAll(["id IN" => [(is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])) ]  ]);
					if($res){
						echo json_encode(['status' => 'success']); 
						die;
					} else {
						echo json_encode(['status' => 'failed']); 
						die;
					}
					/*echo 1;
					else
					echo 0;
					exit;*/
		 } else if(isset($this->request->data['responsible_id']) ) { 
					$sessionAry  = $session->read("SESSION_ADMIN");
					$result = $this->Payments->updateAll( [
							'entry_id'       =>  $sessionAry['0'],
							'payment_date'   =>  (date("Y-m-d",strtotime($this->request->data['payment_date']))),
							'amount'         =>  ($this->request->data['amount']),
							'responsible_id' =>  ($this->request->data['responsible_id']),
							'tds_amount'     =>  ($this->request->data['tds_amount']),
							'note'           =>  ($this->request->data['note']),
							'currency'       =>  ($this->request->data['currency']),
							'modified'       =>   (date("Y-m-d H:i:s")),
						],
						['id ' => ((is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])) ) ]
						);

						if($result){
							$responsble = $this->Users->find('all',[
								'conditions' => ['id' =>  $this->request->data['responsible_id'] ],
								'fields' => ['first_name','last_name']
								])->first();
							//$responsble = $this->Users->get($this->request->data['responsible_id']);
							echo json_encode(['status' => 'success','responsble' => $responsble , 'currency' => $this->request->data['currency']]); 
							die;
						}
						else{
							echo 0; 
							die;
						}
							
				}
			}
			$this->set('project_name',$project_name);
			$this->set('currencySelect',$currency);
			$this->set('resourceSelect',$resource);
			$this->set('resultData',$result);         
		}    

		#_________________________________________________________________________#
		/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_edit
		* @Purpose: This function is to add/edit projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	


	function edit($id = null) { 
	  	
		  	$this->viewBuilder()->layout('new_layout_admin');
	       	$this->set('title_for_layout','Edit Payment');     
			$this->pageTitle = "Edit Payment";		
			$this->set("pageTitle","Edit Payment");	
      
      	if($this->request->data){
          
        $Payment = $this->Payments->get($this->request->data['id']);

        $Payments = $this->Payments->patchEntity($Payment,$this->request->data());

      	if(empty($Payments->errors())) {
          
          $this->Payments->save($Payment);
          $this->Flash->success_new("Payment has been updated successfully.");
          $this->redirect(array('controller' => 'Payments', 'action' => 'paymentslist'));  
        }else{                                    
          $this->set("errors", $Payments->errors());
        } 
        }else if(!empty($id)){ 

        $PaymentsData = $this->Payments->get($id);
        
        $date= $PaymentsData['payment_date']->format('Y-m-d');
         
            $this->set('PaymentsData',$PaymentsData); 
            $this->set('date',$date); 
        
        if(!$PaymentsData){        
         $this->redirect(array('action' => 'paymentslist'));              
        }                                                      
      }
        $this->set("id", $id);                                                             
    }  
		
				#_________________________________________________________________________#
		/**	
		* @Date: 06-Feb-2013	
		* @Method : admin_delete
		* @Purpose: This function is to delete projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	



     function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
     $Payment = $this->Payments->get($id);
   	if ($this->Payments->delete($Payment)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 


		#_____________________________________________________________________________________________#
		
			/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_paymentdetails	
		* @Purpose: This function is to show payment details.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	function adminPaymentdetails($id = null,$key = null) {	
	
			$this->viewBuilder()->layout(false);
			if(!empty($id)){
			
			$result = $this->Payments->find('all',[
				'conditions' => ['responsible_id'=>$id],
				'order' => ['payment_date' => 'desc']
			])->contain('Users','Projects')->toArray();
		}
			$this->set('key',$key);   
			$this->set('resultData',$result);         
		}    

		#_________________________________________________________________________#
		/**	
		* @Date: 28-Dec-2011
		* @Method : admin_reportlist	
		* @Purpose: This function is to show the report list of Salary in system.	
		* @Param: none
		* @Return: none 	
		**/	
	function admin_salaryreport(){    
				
				$this->layout= 'layout_graph';	
				$this->set('title_for_layout','Payments Listing');      
				$this->set("pageTitle","Payments Listing");     
				$this->set("search1", "");                           
				$this->set("search2", "");                                 
				
				$criteria = "1"; //All Searching	  
				$this->Session->delete('SESSION_SEARCH');	  

		if(isset($this->data['Payment'])) {
		$this->layout= 'layout_graph';
				$search2 = 'Payment.project_id!=""';
				if(!empty($this->data['Payment']['project_name'])){
					$search2 .=" AND Project.project_name LIKE '%".Sanitize::escape($this->data['Payment']['project_name'])."%' ";	
				}
				if(!empty($this->data['Payment']['note'])){        
					$search2 .=" AND Payment.note LIKE '%".Sanitize::escape($this->data['Payment']['note'])."%' ";	
				}
				if( !empty($this->data['Payment']['payment_startdate']) &&  !empty($this->data['Payment']['payment_enddate']) ){
					$search2 .=" AND (`Payment`.`payment_date` between '".date("Y-m-d",strtotime($this->data['Payment']['payment_startdate']))."' and '".date("Y-m-d",strtotime($this->data['Payment']['payment_enddate']))."') ";
				}
				if(!empty($this->data['Payment']['start_amount']) &&  !empty($this->data['Payment']['last_amount'])){
					$search2 .=" AND (`Payment`.`amount` between '".$this->data['Payment']['start_amount']."' and '".$this->data['Payment']['last_amount']."')";
				}
				if(!empty($this->data['Payment']['tds_stramount']) &&  !empty($this->data['Payment']['tds_lastamount'])){
					$search2 .=" AND (`Payment`.`tds_amount` between '".$this->data['Payment']['tds_stramount']."' and '".$this->data['Payment']['tds_lastamount']."')";
				}
					$this->set("search2",$search2);      
			}		
		if((!empty($search2))){	
			$criteria = $search2; 		
			$this->Session->write('SESSION_SEARCH', $criteria);	
		}else{			
			$this->set("search2","");	
		}	
		
		if(isset($this->params['named'])){	
				$urlString = "/";		
				$completeUrl  = array();	
			if(!empty($this->params['named']['page']))
				$completeUrl['page'] = $this->params['named']['page'];	
			if(!empty($this->params['named']['sort']))		
				$completeUrl['sort'] = $this->params['named']['sort'];		
			if(!empty($this->params['named']['direction']))		
				$completeUrl['direction'] = $this->params['named']['direction'];	
			foreach($completeUrl as $key=>$value){		
				$urlString.= $key.":".$value."/";	
			}	
		}	      
		$this->set('urlString',$urlString);        
		  
		$this->paginate = array(           
		'fields' => array(	
		'Payment.id',	
		'Project.project_name',	
		'Payment.payment_date', 	
		'Payment.tds_amount',
		'Payment.amount',	
		'Payment.note',	
		'Payment.created',
		'Payment.modified'
		
		),     
		'page'=> 1,'limit' => 100,     
		'order' => array('Payment.payment_date' => 'desc','Project.project_name' => 'asc')          
		);    
	
		$this->Payment->expects(array("Project"));
		$data = $this->paginate('Payment',$criteria);   
		$this->set('resultData', $data);	
	}                 	
#_________________________________________________________________________#  
 /**                       
 * @Date: 12-Sep-2014       
 * @Method : admin_paymentreport_week      
    * @Purpose: This function is to show the report of payments week wise  
 * @Param: none                                                       
 * @Return: none                                                  
 **/  
 
 	function paymentreportweek(){
 	
		 	 $this->viewBuilder()->layout(false);
		 	 $this->viewBuilder()->layout = "layout_graph";
			 
			 if(isset($this->request->data['start_date'])){ 
	
            $start_date = $this->request->data['start_date'];
          
            $date 		= strtotime($start_date);
          
            $date 		= strtotime("next sunday", $date);
        
            $end_date	= date('Y-m-d',$date);
        
            $todate     = str_replace("/","-",$this->request->data['start_date']);
        
            $create     = date_create($todate);
          
            $start_date = $create->format('Y-m-d');
        
            $create=date_create($end_date);
         
            $end_date = $create->format('Y-m-d');
         	}else{
            $start_date = date('Y-m-d', time() + ( 1 - date('w')) * 24 * 3600);
        	
        	if (date('N') == 7) {
            $start_date = date('Y-m-d', strtotime("last monday"));
        }
         	$this->set('toDate',$toDate);
        	$curr_sun = strtotime($start_date);
       		$curr_sun = strtotime("next sunday", $curr_sun);
        	$end_date = date('Y-m-d', $curr_sun);
        }
           $this->set('toDate',$toDate);
       	   $cond = ['Payments.payment_date >=' => $start_date, 'Payments.payment_date <=' => $end_date];
           $this->set('fromDate',$start_date);
		   
		   $resultData = $this->Payments->find()
			->select(['Projects.project_name',
					'Payments.payment_date',
					'Payments.amount',
					'Payments.tds_amount',
					'Payments.currency',
					'Payments.note',
					'Users.first_name'])
					 ->group('Payments.project_id')
					 ->where($cond)->contain(['Users','Projects'])->all();
       
        $this->set('resultData', $resultData);
	 }
  }
