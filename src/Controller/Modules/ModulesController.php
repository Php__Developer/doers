<?php

namespace App\Controller\Modules;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Admin\UsersController;
use App\Controller\Skills\SkillsController;
use App\Controller\Employees\EmployeesController;
use App\Controller\Evaluations\EvaluationsController;
use App\Controller\Generics\GenericsController;
use App\Controller\Hardwares\HardwaresController;
use App\Controller\Milestones\MilestonesController;
use App\Controller\Component\ComponentController;
use App\Controller\CredentialAudits\CredentialAuditsController;
use App\Controller\Billings\BillingsController;
use App\Controller\Contacts\ContactsController;
use App\Controller\Resumes\ResumesController;
use App\Controller\StaticPages\Static_pagesController;
use App\Controller\Testimonials\TestimonialsController;
use App\Controller\Tickets\TicketsController;
use App\Controller\Permissions\PermissionsController;
use App\Controller\PaymentMethods\Payment_methodsController;
use App\Controller\Payments\PaymentsController;
use App\Controller\Reports\ReportsController;
use App\Controller\Profileaudits\ProfileauditsController;
use App\Controller\Sales\SalesController;
use App\Controller\LearningCenters\Learning_centersController;
use App\Controller\Leads\LeadsController;
use App\Controller\Credentials\CredentialsController;
use App\Controller\Attendances\AttendancesController;
use App\Controller\Appraisals\AppraisalsController;
use App\Controller\Projects\ProjectsController;
use App\Controller\Roles\RolesController;
use App\Controller\Agencies\AgenciesController;
use App\Controller\Tags\TagsController;
use App\Controller\Entities\EntitiesController;
use App\Controller\Finance\FinanceController;
use App\Controller\Faq\FaqController;
class ModulesController extends AppController {

var $paginate		   =  array(); //pagination

    var $uses       	 =  array('Modules'); // For Default Model


    /******************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
            	  	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$controller=$this->request->params['controller'];
    	$action=$this->request->params['action'];
    	/*$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
    	if($permission!=1){
    		$this->Flash->error("You have no permisson to access this Page.");

    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}*/
    // end code to check permissions
    	Router::parseNamedParams($this->request);
    	if (!empty($this->params['prefix']) && $this->params['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('new_layout_admin');

    	}

    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize  function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Skills');
        $this->loadModel('Modules');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Permissions');
        $this->loadModel('Agencies');
        $this->loadModel('Roles');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    /**                         
    * @Date: 20-sep-2016        
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function moduleslist(){ 
    	$this->viewBuilder()->layout('new_layout_admin');    
    	$this->set('title_for_layout','Skills Listing');      
    	$this->set("pageTitle","Skills Listing");     
    	$this->set("search1", "");                                 
    	$this->set("search2", "");                                 
		$criteria = "1"; //All Searching
		 if (isset($this->request->data['User']) || !empty($this->request->query)) {

            if (!empty($this->request->data['User']['fieldName']) || isset($this->request->query['field'])) {
             
                if (trim(isset($this->request->data['User']['fieldName'])) != "") {
                    $this->set("search1", $this->request->data['User']['fieldName']);
                    
                    if (trim($this->request->data['User']['fieldName']) == "Users.employeecode") {
                        $search1 = "Users.employee_code";
                    } elseif (trim($this->request->data['User']['fieldName']) == "Users.firstname") {
                        $search1 = "Users.first_name";
                    } elseif (trim($this->request->data['User']['fieldName']) == "Users.currentsalary") {
                        $search1 = "Users.current_salary";
                    } else {
                        $search1 = trim($this->request->data['User']['fieldName']);
                    }
                } elseif (isset($this->request->query['field'])) {
                    $search1 = trim($this->request->query['field']);
                    $this->set("search1", $search1);
                }
                
            }

            if (isset($this->request->data['User']['value1']) || isset($this->request->data['User']['value2']) || isset($this->request->query['value'])) {
                if (isset($this->request->data['User']['value1']) || isset($this->request->data['User']['value2'])) {
                    $search2 = ($this->request->data['User']['fieldName'] != "Users.status") ? trim($this->request->data['User']['value1']) : $this->request->data['User']['value2'];
                } elseif (isset($this->request->query['value'])) {
                    $search2 = trim($this->request->query['value']);
                }
                $this->set("search2", $search2);
            }
            /* Searching starts from here */
            if (!empty($search1) && (!empty($search2) || $search1 == "Users.status")) {
                $query  =    $this->Users->find("all" , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
                  ])->contain(['Appraisals']);
                
                $session->write('SESSION_SEARCH', $criteria);
            } else {
                $this->set("search1", "");
                $this->set("search2", "");
            }
            
        } else {
            // if action is accessed without searching or sorting
          $query  =  $this->Modules->find('all',[
          		'conditions'=> ['Modules.parent_module' => 0]
          	]
          	);
         //pr($query);die;
          $result = isset($query)? $query->toArray():array();
          $this->set('resultset' ,$result);
      }
      $urlString = "/";
      if (isset($this->request->query)) {
        $completeUrl = array();

        if (!empty($this->request->query['page']))
            $completeUrl['page'] = $this->request->query['page'];

        if (!empty($this->request->query['sort']))
            $completeUrl['sort'] = $this->request->query['sort'];

        if (!empty($this->request->query['direction']))
            $completeUrl['direction'] = $this->request->query['direction'];

        if (!empty($search1))
            $completeUrl['field'] = $search1;

        if (isset($search2))
            $completeUrl['value'] = $search2;

        foreach ($completeUrl as $key => $value) {
            $urlString.= $key . ":" . $value . "/";
        }
    }
    $this->set('urlString', $urlString);
    
    $data = $this->paginate($query,[
      'page' => 1, 'limit' => 50,
      'order' => ['Users.status' => 'asc', 'Users.first_name' => 'asc'],
      'paramType' => 'querystring'
      ]);
    
    $this->set('resultData', $data);
	
	}                                                                      
	
		/**	
		* @Date: 20-sep-2016
		* @Method : add	
		* @Purpose: This function is to add Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	


		function addmodule() { 
			$this->viewBuilder()->layout('new_layout_admin');       
			$this->set('title_for_layout','Add Skill');
			$this->pageTitle = "Add Skill";
			$this->set("pageTitle","Add Skill");
			$agencies = $this->Agencies->find('list',[
					'keyField' => 'id',
					'valueField' => 'agency_name',
			'conditions' => []
			])->toArray();
			$agencies = $this->Agencies->find('list',[
					'keyField' => 'id',
					'valueField' => 'agency_name',
			'conditions' => []
			])->toArray();

			$arr = ['Tags'=> 'Tags','Agencies' => 'Agencies',"Resumes"=> "Resumes",'StaticPages'=> 'StaticPages','Testimonials'=>'Testimonials','Tickets'=>'Tickets','Permissions'=>'Permissions','PaymentMethods'=>'PaymentMethods','LearningCenters'=>'LearningCenters','Leads'=>'Leads','Credentials'=>'Credentials','Attendances'=>'Attendances','Appraisals'=>'Appraisals','Admin'=>'Admin','Employees'=>'Employees','Billings'=>'Billings','Contacts'=>'Contacts','Component'=>'Component','CredentialAudits'=>'CredentialAudits','Evaluations'=>'Evaluations','Hardwares'=>'Hardwares','Milestones'=>'Milestones','Payments'=>'Payments','Profileaudits'=>'Profileaudits','Projects'=>'Projects','Reports'=>'Reports','Sales'=>'Sales','Modules' => 'Modules','Roles' => 'Roles','Entities' => 'Entities','Finance' => 'Finance','Faq' => 'Faq'];
            asort($arr);
			$mode = "add";
			if($this->request->data){
				if(in_array($this->request->data['controller'], array_keys($arr))){
					$controller = $this->request->data['controller'];
					$action = $this->request->data['action'];
					$thedata = $this->Common->getprefix($this->request->data['controller']);
					$this->request->data['controller'] = $thedata['controller'];
					$this->request->data['prefix'] = $thedata['prefix'];
					$validateca = $this->Permissions->find('all',[
						'conditions' => ['controller' => $this->request->data['controller'] , 'action' => $this->request->data['action'] ]
					])->first();
				} else{
					$this->request->data['controller'] = "";
					$this->request->data['prefix'] = "";	
				}
				$this->request->data['parent_module'] = 0;
					//if($this->request->data['parent_module'] !== 0){
				
				$modules = $this->Permissions->newEntity($this->request->data());
				if(count($validateca) > 0){
						$errors =[];
						$error['controller'] = ['_combination' => "Combination of This controller and action aleardy exists"];
						$errorarr = array_merge($modules->errors(),$error);
					}
				if(empty($errorarr)){
					$this->Permissions->save($modules);
					$this->Flash->success_new("Skill has been created successfully.",'layout_success');
					$this->setAction('moduleslist');
				}else{
					$this->set("errors", $errorarr);
					$this->set('controller',$controller);
				}
			}
			$this->set('controllers',$arr);
			$this->set('agencies',$agencies);
		}

	/**	
		* @Date: 20-sep-2016
		* @Method : add	
		* @Purpose: This function is to add Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	


		function addsubmodule() { 
			$this->viewBuilder()->layout('new_layout_admin');      
			$this->set('title_for_layout','Add Skill');
			$this->pageTitle = "Add Skill";
			$this->set("pageTitle","Add Skill");
			$this->set('selectedcontroller','');
			$this->set('selectedaction','');
			$this->set('mode','add');
			$mode = "add";
			$arr = ['Tags'=> 'Tags','Agencies' => 'Agencies',"Resumes"=> "Resumes",'StaticPages'=> 'StaticPages','Testimonials'=>'Testimonials','Tickets'=>'Tickets','Permissions'=>'Permissions','PaymentMethods'=>'PaymentMethods','LearningCenters'=>'LearningCenters','Leads'=>'Leads','Credentials'=>'Credentials','Attendances'=>'Attendances','Appraisals'=>'Appraisals','Admin'=>'Admin','Employees'=>'Employees','Billings'=>'Billings','Contacts'=>'Contacts','Component'=>'Component','CredentialAudits'=>'CredentialAudits','Evaluations'=>'Evaluations','Hardwares'=>'Hardwares','Milestones'=>'Milestones','Payments'=>'Payments','Profileaudits'=>'Profileaudits','Projects'=>'Projects','Reports'=>'Reports','Sales'=>'Sales','Modules' => 'Modules','Roles' => 'Roles','Finance' => 'Finance','Faq' => 'Faq'];
			if($this->request->data){
				if(empty($this->request->data['parent_module'])){
					$this->request->data['parent_module'] = 0;
				}
				if($this->request->data['type'] == 'module'){
					unset($this->request->data['default_alias']);
					unset($this->request->data['other_aliases']);
				} else {
					$exp = explode(',', $this->request->data['other_aliases']);
					$aliases = $this->Permissions->find('list',[
						'keyField' => 'id',
						'valueField' => 'alias',
						'conditions' => ['alias IN ' => $exp]
					])->toArray();
					$this->request->data['other_aliases'] = implode(',', array_keys($aliases));
				} 
				if(isset($this->request->data['role_id']) && count($this->request->data['role_id']) > 0) {
					$this->request->data['role_id'] = implode(',', $this->request->data['role_id'] );
				}else{
					$this->request->data['role_id'] = 0;
				}
				if(isset($this->request->data['agency_id']) && count($this->request->data['agency_id']) > 0) {
					$this->request->data['agency_id'] = implode(',', $this->request->data['agency_id'] );
				} else {
					$this->request->data['agency_id'] = 0;
				}
			
				/*pr($this->request->data); die;*/
				unset($this->request->data['actiontypebox']);
				unset($this->request->data['type']);
				/*pr($this->request->data()); die;*/
				$skills = $this->Modules->newEntity($this->request->data());
				if(empty($skills->errors())){
					$this->Modules->save($skills);
					if($this->request->is('ajax')) {
						echo json_encode(['status' => 'success']);
						die;
					}
					$this->Flash->success_new("Sub-Module has been created successfully.");
					$this->setAction('moduleslist');
				}else{
					//pr($skills->errors()); die;
					$this->set("errors", $skills->errors());
					if($this->request->is('ajax')) {
						/*pr($skills->errors()); die;*/
						echo json_encode(['status' => 'failed','errors' => $skills->errors() ]);
						die;
					}
					$selectedcontroller = isset($this->request->data['controller'])? $this->request->data['controller']: '' ;
					$selectedaction = isset($this->request->data['action'])? $this->request->data['action']: '' ;
					$this->set('selectedcontroller',ucfirst($selectedcontroller));
					$this->set('selectedaction',$selectedaction);
					
					//pr($selectedcontroller);die;
				}
			}
			
			$modules = $this->Modules->find('list',[
					'keyField' => 'id',
					'valueField' => 'display_name',
			'conditions' => ['parent_module' => 0,'is_enabled' =>'Yes','is_displayed' => 'Yes']
			])->toArray();

			$agencies = $this->Agencies->find('list',[
					'keyField' => 'id',
					'valueField' => 'agency_name',
			'conditions' => []
			])->toArray();

			$roles = $this->Roles->find('list',[
					'keyField' => 'id',
					'valueField' => 'role',
					'conditions' => []
			])->toArray();

			$aliases = $this->Permissions->find('list',[
					'keyField' => 'id',
					'valueField' => 'alias',
					'conditions' => []
			])->toArray();
			
		$this->set('aliases',$aliases);
		//pr($modules); die;
			$this->set('controllers',$arr);
			$this->set('agencies',$agencies);
			$this->set('roles',$roles);
			$this->set('modules',$modules);
		}

	/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function editmodules($id = null) {
			$id = $this->Common->DCR($this->Common->dcrForUrl($id));
			$this->viewBuilder()->layout('new_layout_admin'); 
			$this->set('title_for_layout','Edit Skill');     
			$this->pageTitle = "Edit Skill";		
			$this->set("pageTitle","Edit Skill");		
			if($this->request->data){	
				$skill = $this->Modules->get($this->request->data['id']);
				$skills = $this->Modules->patchEntity($skill,$this->request->data());
				if(empty($skills->errors())) {
					$this->Modules->save($skill);
					$this->Flash->success_new("Module has been updated successfully.");
					//$this->setAction('moduleslist');
					return $this->redirect(['controller' => 'modules', 'action' => 'moduleslist']);
				}else{                                    
					$this->set("errors", $skills->errors());
				}	
			}else if(!empty($id)){ 
				$modules = $this->Modules->get($id);
				$this->set('modules',$modules); 
				if(!$modules){             
					$this->redirect(array('action' => 'moduleslist'));                 
				}                                                      
			}                                                          
		} 

		/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function deletemodule($id = null) {
			$id = $this->Common->DCR($this->Common->dcrForUrl($id));
			$this->viewBuilder()->layout('new_layout_admin'); 
			//pr($id); die;
			$module = $this->Modules->get($id);
			if(count($module->toArray()) > 0){	
				$data = ['is_deleted' => 'Yes'];
				//$skill = $this->Skills->get($this->request->data['id']);
				if($module->is_core !== 'Yes'){
                    $modules = $this->Modules->patchEntity($module,$data,['validate' => false]);
                    if(empty($modules->errors())) {
                        $this->Modules->save($module);
                        $modules = $this->Modules->find('all',[
                            'fields'=> ['id'],
                            'conditions' => ['parent_module'=> $id]
                            ])->all();
                        foreach($modules as $module){
                            $sub_module = $this->Modules->get($module->id);
                            $sub_modules = $this->Modules->patchEntity($sub_module,$data,['validate' => false]);
                            $this->Modules->save($sub_module);
                        }
                        echo json_encode(['status'=> 'success']); die;
                        //$this->Flash->success("Skill has been updated successfully.");
                        //$this->setAction('list');

                    }else{                                    
                        //$this->set("errors", $skills->errors());
                    }   
                } else {
                   echo json_encode(['status'=> 'failed','reason' => 'Core Module Can not Be deleted!']); die; 
                }
			}else { 
				echo json_encode(['status'=> 'failed']); die;
			}                                                          
		} 

		
			/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function editsubmodules($id = null) {
			$this->viewBuilder()->layout('new_layout_admin'); 
			$id = $this->Common->DCR($this->Common->dcrForUrl($id));
			$this->set('title_for_layout','Edit Skill');     
			$this->pageTitle = "Edit Skill";		
			$this->set("pageTitle","Edit Skill");	
			$this->set('selectedcontroller','');
			$this->set('selectedaction','');
			$this->set('mode','edit');
			$arr = ['Tags'=> 'Tags','Agencies' => 'Agencies',"Resumes"=> "Resumes",'StaticPages'=> 'StaticPages','Testimonials'=>'Testimonials','Tickets'=>'Tickets','Permissions'=>'Permissions','PaymentMethods'=>'PaymentMethods','LearningCenters'=>'LearningCenters','Leads'=>'Leads','Credentials'=>'Credentials','Attendances'=>'Attendances','Appraisals'=>'Appraisals','Admin'=>'Admin','Employees'=>'Employees','Billings'=>'Billings','Contacts'=>'Contacts','Component'=>'Component','CredentialAudits'=>'CredentialAudits','Evaluations'=>'Evaluations','Hardwares'=>'Hardwares','Milestones'=>'Milestones','Payments'=>'Payments','Profileaudits'=>'Profileaudits','Projects'=>'Projects','Reports'=>'Reports','Sales'=>'Sales','Modules' => 'Modules','Roles' => 'Roles','Finance' => 'Finance','Faq' => 'Faq'
            ];	
			if($this->request->data){
				/*pr($this->request->data); die;*/
				$module_id = $this->Common->DCR($this->request->data['id']);
			if(empty($this->request->data['parent_module'])){
					$this->request->data['parent_module'] = 0;
				}
				if($this->request->data['type'] == 'module'){
					unset($this->request->data['default_alias']);
					unset($this->request->data['other_aliases']);
				} else {
					$exp = explode(',', $this->request->data['other_aliases']);
					$aliases = $this->Permissions->find('list',[
						'keyField' => 'id',
						'valueField' => 'alias',
						'conditions' => ['alias IN ' => $exp]
					])->toArray();
					$this->request->data['other_aliases'] = implode(',', array_keys($aliases));
				} 
				if(isset($this->request->data['role_id']) && count($this->request->data['role_id']) > 0) {
					//array_push($this->request->data['role_id'], 8);
					$this->request->data['role_id'] = implode(',', $this->request->data['role_id'] );
				}else{
					$this->request->data['role_id'] = 0;
				}
				if(isset($this->request->data['agency_id']) && count($this->request->data['agency_id']) > 0) {
					$this->request->data['agency_id'] = implode(',', $this->request->data['agency_id'] );
				} else {
					$this->request->data['agency_id'] = 0;
				}
				
				unset($this->request->data['actiontypebox']);
				unset($this->request->data['type']);
				/*pr($this->request->data()); die;*/
				//$skills = $this->Modules->newEntity($this->request->data());
				$submodule = $this->Modules->get($module_id);
				//pr($submodule->toArray()); die;
				$this->request->data['id'] = $module_id;
				$module = $this->Modules->patchEntity($submodule,$this->request->data());
				if(empty($module->errors())){
					$this->Modules->save($submodule);
					if($this->request->is('ajax')) {
						echo json_encode(['status' => 'success']);
						die;
					}
					$this->Flash->success_new("Sub-Module has been created successfully.");
					$this->setAction('moduleslist');
				}else{
					//pr($skills->errors()); die;
					$this->set("errors", $module->errors());
					if($this->request->is('ajax')) {
						echo json_encode(['status' => 'failed','errors' => $module->errors() ]);
						die;
					}
					$selectedcontroller = isset($this->request->data['controller'])? $this->request->data['controller']: '' ;
					$selectedaction = isset($this->request->data['action'])? $this->request->data['action']: '' ;
					$this->set('selectedcontroller',ucfirst($selectedcontroller));
					$this->set('selectedaction',$selectedaction);
					
					//pr($selectedcontroller);die;
				}	
			}else if(!empty($id)){ 
				$submodules = $this->Modules->get($id);
				$this->set('submodules',$submodules); 
				$submodule = $this->Modules->find('all',[
				'conditions' => ['Modules.id' => $id]
				])->contain(['Perms'])->first();
				/*pr($submodule->toArray()); die;*/
				$this->set('submodule',$submodule); 
				$modules = $this->Modules->find('list',[
				'keyField' => 'id',
				'valueField' => 'display_name',
				'conditions' => ['parent_module' => 0,'is_enabled' =>'Yes','is_displayed' => 'Yes']
				])->toArray();
				$agencies = $this->Agencies->find('list',[
						'keyField' => 'id',
						'valueField' => 'agency_name',
				'conditions' => []
				])->toArray();

				$roles = $this->Roles->find('list',[
						'keyField' => 'id',
						'valueField' => 'role',
				'conditions' => [/*'role !=' => 'Super Admin'*/]
				])->toArray();

				$aliases = $this->Permissions->find('list',[
						'keyField' => 'id',
						'valueField' => 'alias',
						'conditions' => []
				])->toArray();
			
				$this->set('aliases',$aliases);
				//pr($modules); die;
				$this->set('controllers',$arr);
				$this->set('agencies',$agencies);
				$this->set('roles',$roles);
				$this->set('selectedcontroller',ucfirst($submodules->controller));
				$this->set('selectedaction',$submodules->action);
				$this->set('modules',$modules);
				if(!$submodules){             
					$this->redirect(array('action' => 'moduleslist'));                 
				}                                                      
			}                                                           
		} 

			/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/
		function modify(){
			if($this->request->is('ajax')) {
				$id = $this->Common->DCR($this->request->data['menuOrigin']);
				$data['is_enabled'] = $this->request->data['eORd'];
				$record = $this->Modules->get($id);
                if($record->is_enabled == 'Yes' &&  $record->is_core == 'Yes'){
                    $status = json_encode(['status' => 'failed','reason' => 'This Module is Core Module']);
                } else{
				$this->Modules->patchEntity($record,$data);
				if($this->Modules->save($record)){
					if($this->request->data['eORd'] == 'No'){
						$list = $this->Modules->find('all',[
							'conditions' => ['parent_module' => $id]
						])->toArray();
						foreach ($list as $entity) {
						$record = $this->Modules->get($entity->id);
						$this->Modules->patchEntity($record,$data);
						$this->Modules->save($record);
						}	
					}
					$status = json_encode(['status' => 'success']);
				} else{
					$status = json_encode(['status' => 'failed']);
				}
             }
				echo $status; die;
			}
		}

			/**	
		* @Date: 20-sep-2016	
		* @Method : edit
		* @Purpose: This function is to add/edit Skills from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/
		function modifysub(){
			if($this->request->is('ajax')) {
				$id = $this->Common->DCR($this->request->data['menuOrigin']);
				$data['is_enabled'] = $this->request->data['eORd'];
				$record = $this->Modules->get($id);
                if($record->is_enabled == 'Yes' &&  $record->is_core == 'Yes'){
                    $status = json_encode(['status' => 'failed','reason' => 'This Module is Core Module']);
                } else{
                      $this->Modules->patchEntity($record,$data);
                    if($this->Modules->save($record)){
                        $status = json_encode(['status' => 'success']);
                    } else{
                        $status = json_encode(['status' => 'failed','reason' => 'Unknown']);
                    }  
                }
				
				echo $status; die;
			}
		}


	/**	
	* @Date: 20-sep-2016	
	* @Method : edit
	* @Purpose: This function is to add/edit Skills from admin section.	
	* @Param: $id	
	* @Return: none 	
	* @Return: none 
	**/
	function getactions(){
		 
		  	if($this->request->is('ajax')) {
 		$folder_name = $this->request->data['cname'];
 		 $arr = ["Resumes",'StaticPages','Testimonials','Tickets','Permissions','PaymentMethods','LearningCenters','Leads','Credentials','Attendances','Appraisals','Admin','Employees','Billings','Contacts','Component','CredentialAudits','Evaluations','Hardwares','Milestones','Payments','Profileaudits','Projects','Reports','Sales','Modules','Roles','Agencies','Tags','Entities','Finance','Faq' => 'Faq'];
    	if( in_array($folder_name, $arr)){	
    	if($folder_name=="Skills" ){
                $class = new SkillsController();
            }elseif($folder_name=="Resumes"){
                $class = new ResumesController();
            }elseif($folder_name=="StaticPages"){
                $class = new Static_pagesController();
            }elseif($folder_name=="Testimonials"){
                $class = new TestimonialsController();
            }elseif($folder_name=="Tickets"){
                $class = new TicketsController();
            }elseif($folder_name=="Permissions"){
                $class = new PermissionsController();
            }elseif($folder_name=="PaymentMethods"){
                $class = new Payment_methodsController();
            }elseif($folder_name=="LearningCenters"){
                $class = new Learning_centersController();
            }elseif($folder_name=="Leads"){
                $class = new LeadsController();
            }elseif($folder_name=="Credentials"){
                $class = new CredentialsController();
            }elseif($folder_name=="Attendances"){
                $class = new AttendancesController();
            }elseif($folder_name=="Appraisals"){
                $class = new AppraisalsController();
            }elseif($folder_name=="Admin"){
                $class = new UsersController();

            }elseif($folder_name=="Employees"){
                $class = new EmployeesController();
            }elseif($folder_name=="Billings"){
                $class = new BillingsController();
            }elseif($folder_name=="Contacts"){
                $class = new ContactsController();
            }/*elseif($folder_name=="Component"){
                $class = new ComponentController();
            }*/elseif($folder_name=="CredentialAudits"){
                $class = new CredentialAuditsController();
            }elseif($folder_name=="Evaluations"){
                $class = new EvaluationsController();
            }elseif($folder_name=="Generics"){
                $class = new GenericsController();
            }elseif($folder_name=="Hardwares"){
                $class = new HardwaresController();
            }elseif($folder_name=="Milestones"){
                $class = new MilestonesController();
            }elseif($folder_name=="Payments"){
                $class = new PaymentsController();
            }elseif($folder_name=="Profileaudits"){
                $class = new ProfileauditsController();
            }elseif($folder_name=="Projects"){
                $class = new ProjectsController();
            }elseif($folder_name=="Reports"){
                $class = new ReportsController();
            }elseif($folder_name=="Sales"){
                $class = new SalesController();
            }elseif($folder_name=="Modules"){
                $class = new ModulesController();
            }elseif($folder_name=="Roles"){
                $class = new RolesController();
            }elseif($folder_name=="Agencies"){
                $class = new AgenciesController();
            }elseif($folder_name=="Tags"){
                $class = new TagsController();
            }elseif($folder_name=="Entities"){
                $class = new EntitiesController();
            }elseif($folder_name=="Finance"){
                $class = new FinanceController();
            }elseif($folder_name=="Faq"){
                $class = new FaqController();
            }
            /*pr($folder_name); die;*/
             if(isset($class)){
                $class_methods = get_class_methods($class); 
                unset($class_methods[0]);
                unset($class_methods[1]);
                $selected=$class_methods;
                foreach ($selected as $key =>$value) {
                  if ($value == 'viewOptions' || $value == 'createView' || $value == 'set' || $value == 'viewBuilder' || $value == 'requestAction' || $value == 'modelType' || $value == 'modelFactory' || $value == 'loadModel' || $value == '_setModelClass'|| $value == '_mergePropertyData' || $value == '_mergeVars' || $value == 'log' || $value == 'tableLocator' || $value == 'dispatchEvent'|| $value == 'eventManager'|| $value == 'afterFilter' || $value == 'beforeRedirect' || $value == 'isAction' || $value == 'paginate' || $value == 'referer' || $value == '_viewPath' || $value == 'render' || $value == 'setAction' || $value == 'redirect' || $value == 'shutdownProcess' || $value == 'startupProcess'  || $value == '_loadComponents' || $value == 'implementedEvents' || $value == '_mergeControllerVars' || $value == 'invokeAction' || $value == 'setRequest' || $value == '__set' || $value == '__get' || $value == 'components' || $value == '__construct' || $value == 'beforeRender'  || $value == '_mergeProperty'  || $value == 'loadComponent'){
                   unset($selected[$key]);
                  }
                }
                echo json_encode(['status' => 'success','data' => $selected]);
                       exit;  
                   }else{
                    echo json_encode(['status' => 'failed','data' => 'empty']);
                       exit;  
                   }

        	} else{
        		// passed controller is not out of the list
        			echo json_encode(['status' => 'failed','data' => 'empty']);
                       exit;  
        	}
   	
 	  	}
	}
	


 	/**********************************************************************/
	/* @page :Modules
	/* @who : Maninder
	/* @why : For enabling disabling modules & submodules
	/* @date: 12-01-2017
	**********************************************************************/

	/*function extrafields(){
		$arr = ["Resumes"=> "Resumes",'StaticPages'=> 'StaticPages','Testimonials'=>'Testimonials','Tickets'=>'Tickets','Permissions'=>'Permissions','PaymentMethods'=>'PaymentMethods','LearningCenters'=>'LearningCenters','Leads'=>'Leads','Credentials'=>'Credentials','Attendances'=>'Attendances','Appraisals'=>'Appraisals','Admin'=>'Admin','Employees'=>'Employees','Billings'=>'Billings','Contacts'=>'Contacts','Component'=>'Component','CredentialAudits'=>'CredentialAudits','Evaluations'=>'Evaluations','Hardwares'=>'Hardwares','Milestones'=>'Milestones','Payments'=>'Payments','Profileaudits'=>'Profileaudits','Projects'=>'Projects','Reports'=>'Reports','Sales'=>'Sales'];
		$this->viewBuilder()->layout(false);
		$this->set('selectedcontroller','');
		$this->set('selectedaction','');
		$this->set('mode','add');
		$this->set('controllers',$arr);
	}*/

	function getaliases(){
		$aliases = $this->Permissions->find('list',[
					'keyField' => 'id',
					'valueField' => 'alias',
			'conditions' => []
			])->toArray();
		$this->set('aliases',$aliases);
		echo json_encode(['status'=> 'success','aliases' => $aliases]);
		die;
	}

    /**********************************************************************/
    /* @page :Modules
    /* @who : Maninder
    /* @why : For enabling disabling modules & submodules
    /* @date: 3-4-2017
    **********************************************************************/

    function getallclasses(){
        $path = explode('/', __DIR__);
        array_pop($path);
        $imp_path = implode('/', $path);
        $files = array_slice(scandir($imp_path), 2) ;
        $classArr = array();
        $finalarr =[];
        if($this->request->data['type'] == 'permissions' || $this->request->data['type'] == 'addall'){
        foreach ($files as $file) {

            if(is_dir($imp_path.'/'.$file)){
                if($file !== 'Component' && $file !== 'AppController.php'){
                                $thedata = $this->Common->getprefix($file);
                                $data['controller'] = $thedata['controller'];
                                $data['prefix'] = $thedata['prefix'];
                                $controllers = array_slice(scandir($imp_path.'/'.$file), 2); 
                    foreach($controllers as $controller){
                        $haystack  = $controller;
                        $needle    = '.php';
                        $needle2 =  'Controller.php';
                        $completecontrollername = strstr($haystack, $needle, true);//strtolower(strstr($haystack, $needle, true));
                        $controllernameinlower = strtolower(strstr($haystack, $needle2, true)) ;//strtolower(strstr($haystack, $needle, 
                        $cont = "App\\Controller\\".$file.'\\'.$completecontrollername;
                        $class = new $cont();
                        $selected= get_class_methods($class);
                        $cont_act_nottracked = [];
                        $aliases_nottracked = [];
                        foreach ($selected as $key =>$value) {
                          if ($value == 'viewOptions' || $value == 'createView' || $value == 'set' || $value == 'viewBuilder' || $value == 'requestAction' || $value == 'modelType' || $value == 'modelFactory' || $value == 'loadModel' || $value == '_setModelClass'|| $value == '_mergePropertyData' || $value == '_mergeVars' || $value == 'log' || $value == 'tableLocator' || $value == 'dispatchEvent'|| $value == 'eventManager'|| $value == 'afterFilter' || $value == 'beforeRedirect' || $value == 'isAction' || $value == 'paginate' || $value == 'referer' || $value == '_viewPath' || $value == 'render' || $value == 'setAction' || $value == 'redirect' || $value == 'shutdownProcess' || $value == 'startupProcess'  || $value == '_loadComponents' || $value == 'implementedEvents' || $value == '_mergeControllerVars' || $value == 'invokeAction' || $value == 'setRequest' || $value == '__set' || $value == '__get' || $value == 'components' || $value == '__construct' || $value == 'beforeRender'  || $value == '_mergeProperty'  || $value == 'loadComponent' || $value == 'beforeFilter' ||$value == 'initialize' || $value == 'login' || $value == 'logout' || $value == 'register' || $value == 'index'){
                          } else{
                            
                                $modulesdata = $this->Permissions->find('all',[
                                'conditions' => ['controller' => $data['controller'] ,'action' => $value ]
                                ])->all();
                                if(count($modulesdata) ==  0){
                                     $cont_act_nottracked[] = $value;   
                                }
                            // } 
                            
                          }
                        }
                     
                        $finalarr[$file] = $cont_act_nottracked;
                    }
                }
                
            }
        }
      }else if ($this->request->data['type'] == 'modules'){
                                $unusedpermissions =[];
                                $permissions = $this->Permissions->find('all',[
                                'conditions' => []
                                ])->all();
                                    foreach($permissions as $permission){
                                            $modulesdata  = $this->Modules->find("all" , [
                                                  'conditions' => [ 
                                                  'is_enabled' => 'Yes','is_deleted' => 'No',
                                                  'OR' => [['default_alias' => $permission['id'] ], ['FIND_IN_SET(\''. $permission['id'] .'\',other_aliases)']]
                                                  ]
                                               ])->all();
                                            /*pr($modulesdata); die;*/
                                           if(count($modulesdata) > 0){
                                             $ever_used = "no";
                                                $is_once = "yes";
                                                foreach($modulesdata as $module){
                                                    if($module['default_alias'] == $permission['id'] || in_array( $permission['id'] , explode(',', $module['other_aliases']) ) ) {
                                                        
                                                    } else{
                                                        if($is_once == 'yes'){
                                                            $unusedpermissions[] = $permission;
                                                            $is_once ="no";
                                                            $ever_used = "yes";
                                                        }
                                                    }
                                                }
                                           } else {
                                                 $unusedpermissions[] = $permission;
                                           }
                                    }
                           $modules= $this->Modules->find("all", [/*
                            'keyField' => 'id',
                            'valueField' => 'display_name',*/
                            'conditions'=> ['Modules.parent_module !=' => 0],
                            'fields' => ['id','display_name']
                          ]);
                            $modulesData = isset($modules)? $modules->toArray():array();   
                           /*  pr($modulesData); die;*/
                               // }
                           /*pr($unusedpermissions); die;*/
                                echo json_encode(['status' => 'success','data' => $unusedpermissions,'type' => $this->request->data['type'] , 'modules' => $modulesData ]);
                                die;           

           } else if($this->request->data['type'] == 'addpermtomodule'){
                $module_id = $this->request->data['module_id'];
                $permission_id = $this->request->data['permission_id'];
                $moduledata = $this->Modules->get($this->request->data['module_id']);
                $other_aliases = explode(',', $moduledata['other_aliases']);
                $added = array_push($other_aliases, $permission_id);
                $imp = implode(',', array_unique($other_aliases));
                $data= ['other_aliases' => $imp];
                $skills = $this->Modules->patchEntity($moduledata,$data,['validate' => false]);
                if($this->Modules->save($moduledata)){
                     echo json_encode(['status' => 'success','type' => $this->request->data['type'] ]);
                    die;
                }

           }

        if($this->request->is('ajax')) {
            if($this->request->data['type'] == 'addall'){
               if(isset($this->request->data['controller']) && isset($this->request->data['action']) ) {
                                 $thedata = $this->Common->getprefix($this->request->data['controller']);
                                $data['controller'] = $thedata['controller'];
                                $data['prefix'] = $thedata['prefix'];
                                $data['action'] = $this->request->data['action'];
                                $data['alias'] =  ucfirst($thedata['controller']).' '.ucfirst($this->request->data['action']);
                                $permission = $this->Permissions->newEntity($data);
                                if(empty($permission->errors())) {
                                    $this->Permissions->save($permission);
                                }else{  
                                     echo json_encode(['status' => 'failed','errors' => $permission->errors(),'controller' => $this->request->data['controller'],'action' => $this->request->data['action']]);

                                    die;
                                }             

               } else{
                    foreach($finalarr as $controller => $actionsarr){
                    if(count($actionsarr) > 0) {
                                 foreach($actionsarr as $key => $action){
                                $thedata = $this->Common->getprefix($controller);
                                $data['controller'] = $thedata['controller'];
                                $data['prefix'] = $thedata['prefix'];
                                $data['action'] = $action;
                                $data['alias'] =  ucfirst($thedata['controller']).' '.ucfirst($action);
                                $permission = $this->Permissions->newEntity($data);
                                if(empty($permission->errors())) {
                                    $this->Permissions->save($permission);
                                }else{  
                                     echo json_encode(['status' => 'failed','errors' => $permission->errors(),'controller' => $controller,'action' => $action]);

                                    die;
                                }   
                           }

                        }
                  
                           
                    }
               }
                
                echo json_encode(['status' => 'success','type' => $this->request->data['type']]);
                die;
            }

            echo json_encode(['status' => 'success', 'data' => $finalarr,'type' => $this->request->data['type'] ]);
            die;
          }
        
      //  pr($finalarr);
      
    }




} // class ending brace