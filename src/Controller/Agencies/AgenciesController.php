<?php

namespace App\Controller\Agencies;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use App\Controller\Admin\UsersController;
use App\Controller\Skills\SkillsController;
use App\Controller\Employees\EmployeesController;
use App\Controller\Evaluations\EvaluationsController;
use App\Controller\Generics\GenericsController;
use App\Controller\Hardwares\HardwaresController;
use App\Controller\Milestones\MilestonesController;
use App\Controller\Component\ComponentController;
use App\Controller\CredentialAudits\CredentialAuditsController;
use App\Controller\Billings\BillingsController;
use App\Controller\Contacts\ContactsController;
use App\Controller\Resumes\ResumesController;
use App\Controller\StaticPages\Static_pagesController;
use App\Controller\Testimonials\TestimonialsController;
use App\Controller\Tickets\TicketsController;
use App\Controller\Permissions\PermissionsController;
use App\Controller\PaymentMethods\Payment_methodsController;
use App\Controller\Payments\PaymentsController;
use App\Controller\Reports\ReportsController;
use App\Controller\Profileaudits\ProfileauditsController;
use App\Controller\Sales\SalesController;
use App\Controller\LearningCenters\Learning_centersController;
use App\Controller\Leads\LeadsController;
use App\Controller\Credentials\CredentialsController;
use App\Controller\Attendances\AttendancesController;
use App\Controller\Appraisals\AppraisalsController;
use App\Controller\Projects\ProjectsController;
use Cake\Cache\Cache;



class AgenciesController extends AppController {

var $paginate		   =  array(); //pagination

    var $uses       	 =  array('Modules'); // For Default Model


    /******************* START FUNCTIONS **************************/


    /**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {
    	parent::beforeFilter($event);
            	  	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);

  

    }

/**                                                           
    * @Date: 20-sep-2016                                          
    * @Method : initialize
    * @Purpose: This function is called initialize  function.
    * @Param: none                                                
    * @Return: none                                               
    **/
public function initialize()
{
	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Agenciess');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('Skills');
        $this->loadModel('Modules');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Permissions');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');

    }


    /**                         
    * @Date: 24-feb -2017     
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/
    public function register(){

      $this->viewBuilder()->layout('beforelogin');
      if($this->request->data){
         $agency = $this->Agencies->newEntity($this->request->data());
           if (empty($agency->errors()) ){
              //$this->Agencies->save($agency);
           } else {
             $this->set("errors", $agency->errors());
           }
      }
    }


   /**                         
    * @Date: 24-feb -2017     
    * @Method : list      
    * @Purpose: This function is to show list of Skills in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/
    public function aslogin($id=null,$UsersId=null){
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
      $superAdminID=$userSession[0];
      $agencyId= $id;
      $Agencydetails = $this->Agencies->get($agencyId);
         $data=$this->Users->find('all')
      ->where(['Users.username' => $Agencydetails['username'],'agency_id' => $Agencydetails['id']])->first();
     // print_r($data); die;
    $id = $data['id']; // id in users table of that person whose is agency owner 
  if(!empty($id)){
        $a = 1;
        if($id)
        {
          $user = $this->Users->find("all" , [
            'conditions' => [
                    'id' => $id
                  ],
            ])->first();
          if($user==""){
                   $this->Flash->error_new("The username or password you entered is incorrect");
          }else{
            $usermail=$user->username;
           if ($a ==1){
                $this->Auth->setUser($user);
              $session = $this->request->session();
              $this->set('session',$session);
              $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);



             if($session->read('adminlogin')){
        $result = $session->read('adminlogin'); // get current logged in session
        $a=count($result);
        if($a>3){
           // return redirect()->back()->with('message',"You can not access more than 3 users"); // if user logged in more that three than display error message
           $this->Flash->error_new("You can not access more than 3 users");
            }else {
            if(in_array($id, $result)){
              //  return redirect()->back()->with('message',"User is already in your logged in List!"); // if already login then display error message
              $this->Flash->error_new("ser is already in your logged in List!");
                }else {
                $data = $session->read('adminlogin');
                //$data = [];
                $data[] = $id;
              $session->write('adminlogin', $data);
            }
        }
        }else{
          $session->write('adminlogin',array($superAdminID,$id));
           // put that id in session
         }





              $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
          }else{
              $this->Flash->error_new("The username or password you entered is incorrect");
            }
           }
          }

        }else{
          $this->Flash->error_new("Please enter the username and password");
        }

         $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
 
    }


function loginid($id=null){

 if(!empty($id)){
        $a = 1;
        if($id)
        {
          $user = $this->Users->find("all" , [
            'conditions' => ['id' => $id
          ],
            ])->first();
          if($user==""){
                   $this->Flash->error_new("The username or password you entered is incorrect");
          }else{
            $usermail=$user->username;
           if ($a ==1){
                      $this->Auth->setUser($user);
              $session = $this->request->session();
              $this->set('session',$session);
              $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);
              $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
          }else{
              $this->Flash->error_new("The username or password you entered is incorrect");
            }
           }
          }

        }else{
          $this->Flash->error_new("Please enter the username and password");
        }
}


    function list(){
        $session = $this->request->session();
        $userSession = $session->read("SESSION_ADMIN");
      $this->viewBuilder()->layout('new_layout_admin');
      $session = $this->request->session();
      $this->set('session',$session);  
      $this->set('title_for_layout','User Agencies Listing');
      $this->set("pageTitle","User Agencies Listing");   
      $this->set("search1", "");    
      $this->set("search2", "");  
      $criteria = "1";

    //pr($criteria); die;
    // Delete user and its licences and orders(single/multiple)

    if(isset($this->request->data) || !empty($this->request->query)) {
        if(!empty($this->request->data['Agencies']['fieldName']) || isset($this->request->query['field'])){

            if(trim(isset($this->request->data['Agencies']['fieldName'])) != ""){
                $search1 = trim($this->request->data['Agencies']['fieldName']);
            }elseif(isset($this->request->query['field'])){
                $search1 = trim($this->request->query['field']);
            }
            $this->set("search1",$search1);
        }

        if(isset($this->request->data['Agencies']['value1']) || isset($this->request->data['Agencies']['value2']) || isset($this->request->query['value'])){
            if(isset($this->request->data['Agencies']['value1']) || isset($this->request->data['Agencies']['value2'])){
                $search2 = ($this->request->data['Agencies']['fieldName'] != "Agencies.status")?trim($this->request->data['Agencies']['value1']):$this->request->data['Agencies']['value2'];
            }elseif(isset($this->request->query['value'])){
                $search2 = trim($this->request->query['value']);
            }
            $this->set("search2",$search2);
        }
        //echo $search1."------".$search2;
        /* Searching starts from here */
     



        if(!empty($search1) && (!empty($search2) || $search1 == "Agencies.status")){

 $query  =    $this->Agencies->find('all' , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ],
               ]);
             $result = isset($query)? $query->toArray():array();
      $criteria = $search1." LIKE '%".$search2."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
     
 }else{  
       $query  =  $this->Agencies->find('all');   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }

  
 $urlString = "/";
      if(isset($this->request->query)){                   
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 
          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }

  $this->set('urlString', $urlString);
      $urlString = "/";  
      if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
    $this->set('urlString', $urlString);  

$this->paginate = [           
/*    'fields' => [ 
    'id',
    'agency_name',
    'first_name',
    'last_name',
    'username'
    ''
],*/     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     

$data = $this->paginate('Agencies',['conditions' => $criteria]);  
$this->set('resultData', $data);  
$this->set('pagename',"Agencieslist");  

    }


     function add() {   
      $this->viewBuilder()->layout('new_layout_admin');
           $this->set('title_for_layout','Add Role');    
           $this->pageTitle = "Add Role";      
           $this->set("pageTitle","Add Role"); 
           $mode = "add";   
           if($this->request->data){  
            $roless = $this->Agencies->newEntity($this->request->data());
            if(empty($roless->errors())){

           $user = $this->Agencies->find("all" , [
                'conditions' => ['username' => $this->request->data['username']]
              ])->first();
                 
            if(count($user)>0){
              $this->Flash->error_new("this username has already taken. please try with another username");
              $this->setAction('register');
            }

             $data = $this->Agencies->save($roless);
              //$data =$this->Agencies->save($agency);
             $AgencyId=$data['id'];
             $UserName=$data['username'];
            $this->request->data['username']=$this->request->data['username'];
            $this->request->data['password']=$this->request->data['password'];
            $this->request->data['agency_id']=$AgencyId;
            $this->request->data['role_id']='1';
            $this->request->data['first_name']=$this->request->data['first_name'];
            $this->request->data['last_name']=$this->request->data['last_name'];
            $this->request->data['status']='0';
            $empCOde=$this->request->data['first_name']."".rand(100,100000000);
            $this->request->data['employee_code']=$empCOde;   
              $user = $this->Users->find("all" , [
                  'conditions' => ['username' => $this->request->data['username']]
                  ])->first();
       if(count($user)>0){
        $this->Flash->error_new("this username has already taken. please try with another username");
        $this->setAction('register');
       }else{
        $users = $this->Users->newEntity($this->request->data(), ['validate' => false]);
        $this->Users->save($users);
       }

       $connection = ConnectionManager::get('default'); // Database object for custom query
        $query ="INSERT INTO `settings` ( `key`, `value`, `agency_id`, `created`, `modified`) VALUES
      ( 'designations', 'Trainee, Software Engineer, Senior Software Engineer, Team Lead, Project Manager, Business Delivery Head, System Analyst, Sales Lead, Senior System Analyst, HR Head, Process Head, QA,QA Lead, Designer, Operation Head, System Administrator, Office Boy, CEO, Vice President, HR Executive, Junior Accountant, Chairman, President, Finance Head',  $AgencyId, '2013-06-21 00:00:00', '2013-06-21 00:00:00'),
      ( 'process_head', '', $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
      ( 'hr_head', '',  $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
      ( 'operation_head', '',  $AgencyId, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
      ( 'ip_address', '',  $AgencyId, '2014-12-16 00:00:00', '2014-12-16 00:00:00'),
      ( 'notification', '<strong><font color=red>Cab Facility is only on mentioned timings. Get more details from Operation Head!</font></strong>,\r\nRead Knowledge base on Regular basis for recent Company Policy update.,\r\nRead & Share your skill in Learning Center.,\r\nUpdate [from when you are free] on dashboard of Company ERP.,\r\nUpdate your dropbox for current projects.,\r\nView Company ERP Dashboard for recent announcements.,\r\nPlease update your TICKET STATUS daily.,\r\nPlease update Attendance on daily basis.,\r\nPlease update your PROJECT DEADLINE.,\r\nAttach your ticket(s) to respective project.,\r\nSwitch off your Computer before leaving.,\r\nSwitch off AC / Fan / Light when not needed.',$AgencyId, '2014-12-16 00:00:00', '2014-12-16 00:00:00'),
      ( 'email_address', '$UserName',  $AgencyId, '2014-12-22 00:00:00', '0000-00-00 00:00:00'),
      ('dashboard_content', 'welcome',  $AgencyId, '2017-03-24 00:00:00', '2017-03-24 00:00:00')";

        $openTicketUsers = $connection->execute($query);


        $query  =  $this->Modules->find('all',[
                    'conditions'=> ['Modules.is_core' => 'Yes']
                  ]
                  );
      $datamodules=$query->toArray();
      foreach($datamodules as $v){
            $takenArray=array();
              $allagencies=$v->agency_id;
               $explede=explode(" ",$allagencies);
              $takenArray[]=$explede[0];
              $takenArray[]=$AgencyId;
              $combineagency= implode(",", $takenArray); 
              $update = ['agency_id' => $combineagency];
          $credentials = TableRegistry::get('Modules');
              $result=$credentials->updateAll($update,['id'=>$v->id]);

      }

             $this->Flash->success_new("Agencies has been created successfully.");
             $this->redirect(array('action' => 'list'));   
         }else{
          $this->set("errors", $roless->errors());
      }
  }     
}    








    function edit($id = null) {
       $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Edit Role');     
            $this->pageTitle = "Edit Role";    
            $this->set("pageTitle","Edit Role");   
            if($this->request->data){ 
                 $role = $this->Agencies->get($this->request->data['id']);
                $roles = $this->Agencies->patchEntity($role,$this->request->data()); 
                if(empty($roles->errors())) {
                  $this->Agencies->save($role);
                  $this->Flash->success_new("Agency has been updated successfully.");
                  $this->redirect(array('action' => 'list'));  
              }else{                                    
                 $this->set("errors", $roles->errors());
             } 
         }else if(!empty($id)){ 
            $RolesData = $this->Agencies->get($id);
            $this->set('RolesData',$RolesData); 
            if(!$RolesData){             
              $this->redirect(array('action' => 'list'));                 
          }                                                      
      }                                                          
  }  






   function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('agency_layout_admin'); 
         $role = $this->Agencies->get($id);
        if ($this->Agencies->delete($role)) {
              //$credentials->updateAll($update,['user_id IN' => $id]);
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 



function dashboard(){
  $this->viewBuilder()->layout('new_layout_admin');
            $this->set('title_for_layout','Agencies ');     
           $query  =  $this->Agencies->find('all');   
$this->paginate = [           
/*    'fields' => [ 
    'id',
    'agency_name',
    'first_name',
    'last_name',
    'username'
    ''
],*/     
'page'=> 1,'limit' => 100,     
'order' => ['id' => 'desc']          
];     

$data = $this->paginate('Agencies');  
$this->set('resultData', $data);  
$this->set('pagename',"Agencieslist"); 
              
}

function userlogout($id){
    $session = $this->request->session();
    $result = $session->read("adminlogin");
    $auth=$result[0]; // on zero index we store the current user person
    $a=array_search($id, $result); // search that id in all session array
    unset($result[$a]);
    $count =count($result);
    if($count==1){
       $user = $this->Users->find("all" , [
                'conditions' => ['id' => $auth
               ],
              ])->first();
       $this->Auth->setUser($user);
       $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);
        $session->delete('adminlogin');
        }else{
        $session->write('adminlogin',array($result));
        $user = $this->Users->find("all" , [
                'conditions' => ['id' => $auth
               ],
              ])->first();
       $this->Auth->setUser($user);
       $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);
    }

     $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));

}

function switchlogin($id){
    $session = $this->request->session();
    $result = $session->read("adminlogin");
    $auth=$result[0]; // on zero index we store the current user person
    /*$a=array_search($id, $result); // search that id in all session array
    pr($a); die;
    unset($result[$a]);
    $count =count($result);*/
   
    if(in_array($id, $result)){
       $user = $this->Users->find("all" , [
                'conditions' => ['id' => $id
               ],
              ])->first();
       $this->Auth->setUser($user);
       $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);
       // $session->delete('adminlogin');
        }else{
         $this->Flash->error_new("Invalid Attempt!");
    }

     $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));

}
function destroyallsessions($id){
    $session = $this->request->session();
    $result = $session->read("adminlogin");
    $auth=$result[0]; // on zero index we store the current user person
    /*$a=array_search($id, $result); // search that id in all session array
    pr($a); die;
    unset($result[$a]);
    $count =count($result);*/
    if($auth == $id){
       $user = $this->Users->find("all" , [
                'conditions' => ['id' => $id
               ],
              ])->first();
       $this->Auth->setUser($user);
       $session->write("SESSION_ADMIN", [$this->Auth->user('id'), $this->Auth->user('first_name') , $this->Auth->user('role_id'),$this->Auth->user('agency_id'),
                ]);
        $session->delete('adminlogin');
        }else{
         $this->Flash->error_new("Invalid Attempt!");
    }

     $this->redirect(array('controller'=>'users','action'=>'dashboard','prefix'=>'admin'));
}

 public function clearcache(){      
        Cache::clear(false);
        $this->Flash->success_new("Cache Have been Cleared!");
        $this->redirect(array('action' => 'list'));   
        
  }


} // class ending brace