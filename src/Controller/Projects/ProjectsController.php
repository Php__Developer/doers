<?php	
	/**	
	* Projects Controller class	
	* PHP versions 5.3.5	
	* @date 28-Dec-2011	
	* @Purpose:This controller handles all the functionalities regarding Projects management.	
	* @filesource	
	* @author  VLL Solutions	
	* @revision	
	* @version 1.3.12	
	**/
	//App::import('Sanitize');
	namespace App\Controller\Projects;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Cache\Cache;

	class ProjectsController extends AppController
	{	
		//var $name       	=  "Projects";	
		/**	
		* Specifies helpers classes used in the view pages	
		* @access public	
		*/	
		//var $helpers    	=  array('Html', 'Form', 'Javascript', 'Session','General','Csv');
		
		/**
		* Specifies components classes used
		* @access public
		**/	
		//var $components 	=  array('RequestHandler','Email','Common');	
		
	//	var $paginate		=  array();	
		
	//	var $uses       	=  array('Projects','Contact','User','Ticket','Payment','Billing','saleProfile','Milestone');  // For Default Model
		
		
		/******************************* START FUNCTIONS **************************/

		/**	* @Date: 28-Dec-2011	
		* @Method : beforeFilter	
		* @Purpose: This function is called before any other function.	
		* @Param: none   
		* @Return: none 
		**/	
		function beforeFilter(Event $event){
			// star to check permission
	$session = $this->request->session();
	$userSession = $session->read("SESSION_ADMIN");
	$controller=$this->request->params['controller'];
	$action=$this->request->params['action'];
	if($userSession==''){
			return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
	  }
	$permission=$this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
	if($permission!=1){
            $this->Flash->error("You have no permisson to access this Page.");
 	if($this->request->is('ajax') == true){		            
        echo "You have no permisson to access this Page.";
            die;
      }  else {
      		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);	
      }
      
	}
    // end code to check permissions	
			 parent::beforeFilter($event);	
  Router::parseNamedParams($this->request);
		if(!empty($this->params['prefix']) && $this->params['prefix'] == "admin"){
			$this->checkUserSession();
		}else{

		$this->viewBuilder()->layout("new_layout_admin");
		}
	
/*$session = $this->request->session();
$userSession = $session->read("SESSION_ADMIN");
if($userSession==''){
return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
}*/
		$this->set('common',$this->Common);
		}				
		#_________________________________________________________________________#
		
		/**	
		* @Date: 28-Dec-2011
		* @Method : index
		* @Purpose: This function is the default function of the controller
		* @Param: none	
		* @Return: none 	
		**/	
		public function initialize()
    {
        parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); 
        $this->loadModel('Tickets'); 
        $this->loadModel('Billings'); 
        $this->loadModel('Evaluations'); 
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Notes');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadModel('Documents');
        $this->loadModel('Contacts');
        $this->loadModel('Agencies');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); 
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
  		$this->loadModel('Leads'); 
  		$this->loadModel('Projects');
  		$this->loadModel('Payments');
    }
		function index() {		
				$this->render('login');	
					if($this->Session->read("SESSION_USER") != "") {	
					$this->redirect('dashboard');
				}
		}
		
		#_________________________________________________________________________#	
		
		/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_index	
		* @Purpose: This is the default function of the administrator section for users	
		* @Param: none
		* @Return: none 	
		**/
		
		function admin_index() {	
			$this->render('admin_login');	
			if($this->Session->read("SESSION_ADMIN") != ""){	
			$this->redirect('dashboard');		
			}
		}
		
	
		#_________________________________________________________________________#
		/**	
		* @Date: 28-Dec-2011
		* @Method : admin_list	
		* @Purpose: This function is to show list of Projects in system.	
		* @Param: none
		* @Return: none 	
		**/	
		 function projectslist(){
        $this->viewBuilder()->layout('new_layout_admin');
        //ini_set('memory_limit', '512M');
         ini_set('memory_limit', '-1');
         $session = $this->request->session();
         $userSession = $session->read("SESSION_ADMIN");
         $agencyid = $userSession[3];
          $role = explode(",",$userSession['2']);
			//$checkadmin=;
			if(in_array('1',$role) || in_array('8',$role)){
				$admin="yes";
			} else {
				$admin="no";	
			}
		$this->set('admin',$admin); 
		$this->set('session',$session);  
		$this->set('userSession',$userSession); 
        $this->set('title_for_layout','Projects Listing');    
        $this->set("pageTitle","Projects Listing");   
        $this->set("search1", "");    
        $this->set("search2", "");  
      $criteria = "1";  
if(isset($this->request->data['Project']) || !empty($this->request->query)) {   
   if(!empty($this->request->data['Project']['fieldName']) || isset($this->request->query['field'])){                                                                
        if(trim(isset($this->request->data['Project']['fieldName'])) != ""){                
        $search1 = trim($this->request->data['Project']['fieldName']);   
         }elseif(isset($this->request->query['field'])){                              
          $search1 = trim($this->request->query['field']);
        }
        $this->set("search1",$search1);                                               
      }
      if(isset($this->request->data['Project']['value1']) || isset($this->request->data['Project']['value2']) || isset($this->request->query['value'])){                  
      if(isset($this->request->data['Project']['value1']) || isset($this->request->data['Project']['value2'])){                                                            
      $search2 = ($this->request->data['Project']['fieldName'] != "Project.publish")?trim($this->request->data['Project']['value1']):$this->request->data['Project']['value2'];
     }elseif(isset($this->request->query['value'])){         
      $search2 = trim($this->request->query['value']);     
      }                                                      
      $this->set("search2",$search2);                       
      }
      /* Searching starts from here */                                               
      if(!empty($search1) && (!empty($search2) || $search1 == "Project.publish")){      
          $query  =    $this->Projects->find('all' , [
                  'conditions' => [$search1 .' Like' =>'%'.$search2.'%' ,'Projects.agency_id' => $agencyid],
               ])->contain(['Contact']);
             $result = isset($query)? $query->toArray():array();
       $criteria = $search1." LIKE '%".$search2."%'"; 
      $session->write('SESSION_SEARCH', $criteria);
      }else{  
       $query  =  $this->Projects->find('all',[
       	'conditions' => ['Projects.agency_id' => $agencyid]
       	])->contain(['Contact']);   
      $this->set("search1","");      
      $this->set("search2","");      
      }
      }else{
          $query  = $this->Projects->find('all',[
       	'conditions' => ['Projects.agency_id' => $agencyid]
       	])->contain(['Contact']);
      }


      
       $urlString = "/";
      if(isset($this->request->query)){ 
         $completeUrl  = array();
          if(!empty($this->request->query['page']))      
          $completeUrl['page'] = $this->request->query['page'];
        if(!empty($this->request->query['sort']))
          $completeUrl['sort'] = $this->request->query['sort'];   
        if (!empty($this->request->query['direction']))        
          $completeUrl['direction'] = $this->request->query['direction']; 

          if(!empty($search1))                                        
          $completeUrl['field'] = $search1;
          if(isset($search2))                                         
          $completeUrl['value'] = $search2;
          foreach($completeUrl as $key => $value) {                      
          $urlString.= $key.":".$value."/";                           
          }
        }
       $this->set('urlString', $urlString);
      $urlString = "/"; 
    if(isset($this->request->query)){
        $completeUrl  = array();  
      if(!empty($setValue)){                            
      if(isset($this->params['form']['IDs'])){          
      $saveString = implode("','",$this->params['form']['IDs']);      
        }
      }
    }
  $this->set('urlString', $urlString);
 $paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/50);
  $this->set('paginatecount',$paginate);
$datas = $this->paginate($query,[
              'page' => 1, 'limit' => 50,
              'order' => ['Projects.modified' => 'desc','Projects.id' => 'desc'],
             'paramType' => 'querystring'
            ]);
      $this->set('resultData', $datas);
    $data = $datas->toArray();
      $counter = 0;
      $currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600); 


      foreach($data as $k=>$v){
        $ProjectsID = $v['id'];
        $paymentResults = $this->Payments->find('list',[
          'keyField' => 'id',
          'valueField' => 'amount',
          'fields' => ['amount','id'],
          'conditions' =>['Project_id' => $ProjectsID]
        ])->toArray();
        /*pr($v['project_name']);
        pr($paymentResults);*/
      $verify = $this->Billings->find()
        ->where(
        ['verified'=>'0',
        'Project_id'=>$ProjectsID,
        ['week_start <=' => $currentWeekMonday] ])->count();
        if($verify==0){
        $data[$counter]['verify'] =1;
        }
        else{
        $data[$counter]['verify'] =0;
        }
        $data[$counter]['total_payment'] = array_sum(array_values($paymentResults));
        $data[$counter]['eid'] = $this->Common->encForUrl($this->Common->ENC($data[$counter]['id']));
        $data[$counter]['eclient_id'] = $this->Common->encForUrl($this->Common->ENC($data[$counter]['client_id']));
        $counter++;
      }  


     /* 	pr($data); die;*/
          $this->set('searching',"yes");
           $this->set('page',"projectslist"); 
      $this->set('resultData', $data);
     
   if($this->request->is('ajax')){
          if(isset($this->request->query['page'])){
             $pageing=$this->request->query['page'];
          }else{
            $pageing="";
          }
         
         echo json_encode(['resultData'=>$data,'page'=>$pageing,'paginatecount'=>$paginate,'admin'=>$admin]);
          die;
      }

  }












	

		/**	
		* @Date: 02-Oct-2016	
		* @Method : admin_add	
		* @Purpose: This function is to add Projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	

 function add() {	
            $this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Add Project');		
			$this->pageTitle = "Add Project";			
			$this->set("pageTitle","Add Project");	
			$session = $this->request->session();
      	    $this->set('session',$session);  
		
			$mode = "add";		
			if($this->request->data){	
			$sessionAdmin = $session->read("SESSION_ADMIN");
			
			$last_id = $sessionAdmin['0'];
			$this->request->data['last_id'] = $last_id;
			$this->request->data['agency_id'] = $sessionAdmin[3];
				$projects = $this->Projects->newEntity($this->request->data());
				if(empty($projects->errors())){
 				$this->Projects->save($projects);

				$this->Flash->success_new("Project has been created successfully.");
				 return $this->redirect( ['action' => 'projectslist']);
       		}else{
          $this->set("errors", $projects->errors());
        }
      }     
  	}                
      
		#_________________________________________________________________________#
		/**	
		* @Date: 28-Dec-2011	
		* @Method : admin_edit
		* @Purpose: This function is to edit Projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function admin_edit($id = null) {	
				$this->set('title_for_layout','Edit Projects');     
				$this->pageTitle = "Edit Projects";		
				$this->set("pageTitle","Edit Projects");		
			
			if($this->request->data){	
				$this->request->data['Projects']['nextpayment'] = date("Y-m-d",strtotime($this->request->data['Projects']['nextpayment'])); //date to sql format
				$this->request->data['Projects']['nextrelease'] = date("Y-m-d",strtotime($this->request->data['Projects']['nextrelease']));
				$this->request->data['Projects']['finishdate'] = date("Y-m-d",strtotime($this->request->data['Projects']['finishdate']));
				$this->Projects->set($this->request->data['Projects']);		
				$isValidated=$this->Projects->validates();		 
			
			if($isValidated){				
					$data=$this->request->data['Projects'];   //post	
					if($data['clientUpdate']=="deact"){
								$this->Contact->updateAll(
								array('Contact.iscurrent' => "'0'"),//fields
								array('Contact.id' => $data['client_id'])
							);
					}else if($data['clientUpdate']=="act"){
								$this->Contact->updateAll(
								array('Contact.iscurrent' => "'1'"),//fields
								array('Contact.id' => $data['client_id'])
							);
					}
					$this->Projects->save($this->request->data['Projects'], array('validate'=>false));          
					$this->Session->setFlash("Projects has been updated successfully.",'layout_success');		                            
					$this->redirect('projectslist');             
			}else{                                    
				$this->set("Error",$this->Projects->invalidFields());	
			}	
			}else if(!empty($id)){       	
				$this->request->data = $this->Projects->find('first', array('conditions'=>array('id'=>Sanitize::escape($id))));	 
				
				$this->request->data['Projects']['nextpayment'] = date("d-m-Y",strtotime($this->request->data['Projects']['nextpayment']));  
				$this->request->data['Projects']['nextrelease'] = date("d-m-Y",strtotime($this->request->data['Projects']['nextrelease']));  
				$this->request->data['Projects']['finishdate'] = date("d-m-Y",strtotime($this->request->data['Projects']['finishdate']));
				if(!$this->request->data){             
					$this->redirect(array('action' => 'projectslist'));                 
				}                                                      
			}                                                          
		}    
		
				#_________________________________________________________________________#
		/**	
		* @Date: 06-Feb-2013	
		* @Method : admin_delete
		* @Purpose: This function is to delete Projects from admin section.	
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
	

   function delete($id = null) {
      //$id = $id;
   	$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
      $this->viewBuilder()->layout('new_layout_admin'); 
       $project = $this->Projects->get($id);
      if ($this->Projects->delete($project)) {
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       } 
		#_____________________________________________________________________________________________#
		
		/**	
		* @Date: 13-Feb-2013	
		* @Method : admin_processaudit
		* @Purpose: This function is to update Projects process from admin section.	
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		* @Return: none 
		**/	
		function processaudit() {
		 $this->viewBuilder()->layout('new_layout_admin');	
		$session = $this->request->session();
      	$this->set('session',$session);
      	 $userSession = $session->read("SESSION_ADMIN");
          $role = explode(",",$userSession['2']);
			$checkadmin=in_array('1',$role);
			if($checkadmin ==1){
				$admin="yes";
			} else{
				$admin="no";
			}
        $this->set('admin',$admin); 
		$this->set('title_for_layout','Process Audit'); 

		$this->Projects->find('all')->contain(['Engineer_User'])->toArray();
	
			$userSession = $session ->read("SESSION_ADMIN");
			$userID = $userSession[0];
			
		$this->Projects->find('all')->contain(['Last_Updatedby'])->toArray();
		
		$res = $this->Projects->find() 
				//->select([ 'Projects.id', 'Projects.project_name','Projects.dailyupdate','Projects.dropbox','Projects.vooap','Projects.onalert','Projects.processnotes','Projects.created','Projects.modified','Projects.finishdate','Projects.nextrelease','Projects.totalamount','Engineer_User.first_name','Engineer_User.last_name','Last_Updatedby.first_name','Last_Updatedby.last_name'])

					->where([ 
						'OR' => [ ['FIND_IN_SET(\''. $userID .'\',Projects.pm_id)'],['FIND_IN_SET(\''. $userID .'\',Projects.engineeringuser)'],['FIND_IN_SET(\''. $userID .'\',Projects.salesbackenduser)'],['FIND_IN_SET(\''. $userID .'\',Projects.salesfrontuser)'], 'FIND_IN_SET(\''. $userID .'\',Projects.team_id)'],
						['Projects.status' => 'current'] ])
					->order(['Projects.modified' => 'DESC'])->contain(['Engineer_User','Last_Updatedby','Contact'])->all()->toArray();
					
			$result= array();
			$i =0;
			foreach($res as $Projects){
				$ProjectsID = $Projects['id'];
				$paymentResults = $this->Payments->find('list',[
					'keyField' => 'amount',
					'valueField' => 'amount',
					'fields' => ['amount'],
					'conditions' =>['Project_id' => $ProjectsID]
				])->toArray();

				$diff = $this->Common->expiredTime($Projects['modified']);
				$hours = $diff['hours'];
				$Projects['hours'] = $hours;
				$result[$i] = $Projects;
				$result[$i]['total_payment'] = array_sum(array_keys($paymentResults));
				$i++;

			}

			$this->set('searching',"not");
			$this->set('page',"processaudit");  
			$this->set('resultData',$result); 
			$this->render('projectslist');
			if($this->request->data){ 
			$sessionAdmin = $session->read("SESSION_ADMIN");
			$last_id = $sessionAdmin['0'];
			$this->request->data['last_id'] = $last_id;
				if(isset($this->request->data['submit']) && ($this->request->data['submit'] == "Save")){
					$this->request->data['Projects']['name']=$this->request->data['name'];
					$this->request->data['Projects']['dailyupdate']=$this->request->data['dailyupdate'];
					$this->request->data['Projects']['dropbox']=$this->request->data['dropbox'];
					$this->request->data['Projects']['vooap']=$this->request->data['vooap'];
					$this->request->data['Projects']['onalert']=$this->request->data['onalert'];
					foreach($this->request->data['Projects'] as $fieldname=>$Projects){
						foreach($Projects as $key=>$value){
							$process[$key][$fieldname]= $value;	
						}
					} 
						/*if($this->request->data['submit'] == "Save & Email"){
							$html = "<table width='100%' style='border:1px solid black;border-collapse:collapse;' >
								<tr>
									<th width='22%' style='border:1px solid black;border-collapse:collapse;'>Projects</th>
									<th width='12%' style='border:1px solid black;border-collapse:collapse;'>Daily Update</th>
									<th width='12%' style='border:1px solid black;border-collapse:collapse;'>Dropbox</th>
									<th width='12%' style='border:1px solid black;border-collapse:collapse;'>Vooap</th>
									<th width='12%' style='border:1px solid black;border-collapse:collapse;'>On Alert</th>
									<th width='30%' style='border:1px solid black;border-collapse:collapse;'>Process Notes</th> 
								</tr>"; 
						}*/
						//foreach to update Projects in a loop
						foreach($process as $id=>$value){ 
								$result =  $this->Projects->updateAll( [
								'dailyupdate' => ($value['dailyupdate']),
								'dropbox' => ($value['dropbox']),
								'vooap' =>  ($value['vooap']),
								'onalert' =>  ($value['onalert']),
								'modified' => (date("Y-m-d")),
								'last_id'=>$last_id ],
								['id ' => ($id)]
							);
						}
							if($result="1"){
								$this->Flash->success_new("Projects has been updated successfully.");	
								$this->redirect(array('controller'=>'projects','action'=>'processaudit'));
							}
						}
					}
				}
		

	#_____________________________________________________________________________________________#
		/**	
		* @Date: 13-Feb-2013	
		* @Method : admin_processreport
		* @Purpose: This function is to update Projects process from admin section.	
		* @Param: none	
		* @author: Neelam Thakur 	
		* @Return: none 
		* @Return: none 
		**/	

		
		function processreport() {	
			 $this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Process Report');  
			$session = $this->request->session();
      	    $this->set('session',$session);
			 $userSession = $session->read("SESSION_ADMIN");
             $role = explode(",",$userSession['2']);
			 $checkadmin=in_array('1',$role);
			 if($checkadmin ==1){
				$admin="yes";
			 }
        $this->set('admin',$admin); 
			$res = $this->Projects->find('all', 
				array(
					'conditions' => array('Projects.status'=>'current','Projects.agency_id' => $userSession[3]),
					'order' => ['Projects.modified' => 'DESC']
				))->contain(['Last_Updatedby','Contact']);
				 $process=isset($res)? $res->toArray():array();

       		$this->set('resultData', $process);
		
			$result= array();
			$i =0;
			$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);	
			
			foreach($res as $Projects){
				$ProjectsID=$Projects['id'];
				//pr($ProjectsID); die;
			    $query=TableRegistry::get('Payments');
			    $paymentResult=$query->find();
			    $paymentResult->select(['total_sum' => $paymentResult->func()->sum('Payments.amount')])  //count as status a
			      ->where([
			        'Payments.Project_id' => $ProjectsID
			     ]);
			      //pr($paymentResult->toArray()); 
			     $diff = $this->Common->expiredTime($Projects['modified']);
				$hours = $diff['hours'];
				$Projects['hours'] = $hours;
			//pr($currentWeekMonday); die;
			$verify=$this->Billings->find('all',
				array('conditions' =>array('Billings.verified'=>'0','Billings.project_id'=>$ProjectsID,'AND' => array('Billings.week_start <=' => $currentWeekMonday))
					)
				);
		//pr($verify->toArray()); die;
				if(count($verify->toArray())==0){
				$Projects['verify'] =1;
				}
				else{
			   $Projects['verify'] =0;
				}
				//pr($Projects['verify']); die;
             $data=$paymentResult->toArray();
             
             $result[$i] = $Projects;
				$result[$i]['total_payment'] = $data[0]['total_sum'];
				$i++;
        }
			   $this->set('searching',"not");
    $this->set('page',"processreport");  
    $this->set('resultData',$result); 
        $this->render('projectslist');
  	}
		
		#_____________________________________________________________________________________________#
		
		/**	
		* @Date: 14-Feb-2013	
		* @Method : admin_Projectsreport
		* @Purpose: This function is to generate Projects report from admin section.	
		* @Param: none	
		* @author: Neelam Thakur 		
		* @Return: none 
		* @Return: none 
		**/	
		function projectreport() {
			ini_set('memory_limit', '-1');
		 $this->viewBuilder()->layout('new_layout_admin');	
		$this->set('title_for_layout','Projects Report');
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
          $role = explode(",",$userSession['2']);
			$checkadmin=in_array('1',$role);
			if($checkadmin ==1){
				$admin="yes";
			}
        $this->set('admin',$admin);  

		$res = $this->Projects->find('all', 
				array( 	
					'fields' => array('Projects.id', 'Projects.onalert','Projects.project_name','Projects.engagagment','Projects.payment','Projects.notes','Projects.status','Projects.platform','Projects.client_id',
						'Projects.engineeringuser','Projects.nextpayment','Projects.nextrelease','Projects.finishdate','Projects.totalamount','Contact.name','Engineer_User.first_name','Engineer_User.last_name','Last_Updatedby.first_name','Last_Updatedby.last_name'),
					'conditions' => array('Projects.status'=>'current','Projects.agency_id' => $userSession[3] ),
					'order' => array('Projects.modified' => 'desc')
				))->contain(["Engineer_User","Contact","Salefront_User","Saleback_User","Payments","Last_Updatedby","Billings"])->toArray();

			$paid =0 ; $totalamount=0; $left=0; $counter = 0;
			$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
			foreach($res as $k=>$v){
				$ProjectsID = $v['id'];
				
				$totalamount += $v['totalamount'];
             	$query=TableRegistry::get('Payments');
			   
			    $paymentResult2=$query->find();
			     $paymentResult2->select(['total_sum' => $paymentResult2->func()->sum('Payments.amount')])
			      ->where([
			        'Payments.Project_id' => $ProjectsID ]);

            $paymentResult=$paymentResult2->toArray();

            $res[$counter]['Projects']['total_payment'] = $paymentResult[0]['total_sum'];

				$paymentResults = $this->Payments->find('list',[
					'keyField' => 'id',
					'valueField' => 'amount',
					'fields' => ['amount','id'],
					'conditions' =>['Project_id' => $ProjectsID]
				])->toArray();
				//($result['id'] == 1207) ? pr($result): "";
				//($result['id'] == 1207) ? die : "";
			// verify billing 
     	$verify = $this->Billings->find()
				->where(
				['verified'=>'0',
				'Project_id'=>$ProjectsID,
				['week_start <=' => $currentWeekMonday] ])->count();
				if($verify == 0){
				$res[$counter]['verify'] =1;
				} else {
				$res[$counter]['verify'] =0;
				}

				$res[$counter]['total_payment'] = array_sum(array_values($paymentResults));
				$counter++;
				$paid += $paymentResult[0]['total_sum'];
			}
            $left  = $totalamount - $paid;
			$this->set('currentWeekMonday',$currentWeekMonday);
			$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
			if(date('N')==7){
			$currentWeekMonday=date('Y/m/d',strtotime("last monday"));
		      }
			/*pr($userSession[3]); die;*/
			$hourlyProjects = $this->Projects->find('list',[
				'conditions' => ['engagagment' => 'hourly','agency_id' => $userSession[3]],
				]);
			/*pr($hourlyProjects->toArray()); die;*/
if(count($hourlyProjects->toArray())>0){
		$ids = array_values($hourlyProjects->toArray());
			$hoursum = $this->Billings->find("all" , [
		          'conditions' => ['project_id IN' => $ids,'week_start >='=>$currentWeekMonday],
		           'groupField' => 'project_id'
		       ])->all();
}
		
			$this->set('hoursum',$hoursum);	
			$this->set('left',$left); 
			$this->set('searching',"not");
			$this->set('page',"projectreport");  
			$this->set('resultData',$res); 
			$this->render('projectslist'); 
		}
		
		#_____________________________________________________________________________________________#
		/**	
		* @Date: 15-Feb-2013	
		* @Method : admin_quickedit
		* @Purpose: This function is to generate Projects report from admin section.	
		* @Param: none	
		* @author: Neelam Thakur 	
		* @Return: none 
		* @Return: none 
		**/	
	
		function quickedit($id = null) {
		$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
        $this->set('title_for_layout','Edit Project');     
        $this->pageTitle = "Edit Project";    
        $this->set("pageTitle","Edit Project");   
        $this->viewBuilder()->layout(false);
			 if($this->request->data){ 
			 	/*pr($this->request->data); die;*/
        $project = $this->Projects->get($this->request->data['id']);

        if(isset($this->request->data['team_id']) ){
        	$team_id=implode(",",$this->request->data['team_id']);
        $this->request->data['team_id'] = $team_id;
   		 }else{
    	$this->request->data['team_id'] = "";
    	}
        $projects = $this->Projects->patchEntity($project,$this->request->data());
      	if(empty($projects->errors())) {
          $this->Projects->save($project);
          $this->Flash->success_new("Project has been updated successfully.");	
                  echo 1;
                  die;
                 exit;
        }else{ 
      		echo json_encode(['status'=>'failed','errors'=>$projects->errors()]);
      		die;
      		}        
      	}else if(!empty($id)){

      	 $pass = $this->SaleProfiles->find('all',[
				'fields' => ['SaleProfiles.password','SaleProfiles.username','SaleProfiles.nameonprofile','Projects.id'],
			 'conditions' =>['Projects.id'=>$id]
			])->contain(['Projects'])->toArray();
      	 //pr($pass); die;
 	   	$pasword = (count($pass)) ? $pass[0]['password']: '';
 	   	$nameonprofile = (count($pass)) ? $pass[0]['password']: '';
			$this->set('pass',$pasword);
			$this->set('nameofprofile',$nameonprofile);
        
        $ProjectsData = $this->Projects->get($id);
        $resultData = $ProjectsData->toArray();
            

              $this->set('ProjectsData',$ProjectsData); 
              $this->set('resultData',$resultData); 

	if(isset($ProjectsData['nextpayment'])){
        $nextpayment= $ProjectsData['nextpayment']->format('Y-m-d');
    }else{
        $nextpayment= '';
    }
	if(isset($ProjectsData['nextrelease'])){
        $nextrelease= $ProjectsData['nextrelease']->format('Y-m-d');
    }else{
    	$nextrelease= '';
    }
    if(isset($ProjectsData['finishdate'])){
        $finishdate= $ProjectsData['finishdate']->format('Y-m-d');
    }else{
    	$finishdate= '';
    }
                   $this->set('nextpayment',$nextpayment);
                   $this->set('nextrelease',$nextrelease);
                   $this->set('finishdate',$finishdate);
                                                  
      }                                                          
    }       
				#________________________________________________________________________________________________#
		
		/**	
		* @Date: 13-April-2013	
		* @Method : admin_editprocessaudit
		* @Purpose: This function is to generate Projects report from admin section.	
		* @Param: none	
		* @Return: none 	
		* @Author: Neelam Thakur 
		**/	
	

		function editprocessaudit($id = null) { 
		$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));	
        $this->set('title_for_layout','Process Audit');     
        $this->pageTitle = "Process Audit";    
        $this->set("pageTitle","Process Audit");   
     	$this->viewBuilder()->layout(false);
     	$session = $this->request->session();
      	$this->set('session',$session);
		$userSession = $session ->read("SESSION_ADMIN");
      if($this->request->data){
		//pr($this->request->data); die;
		$this->request->data['id'] = (is_numeric($this->request->data['id']) ) ? $this->request->data['id'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['id'])); 
		$this->request->data['last_id'] = $userSession[0];
        $project = $this->Projects->get($this->request->data['id']);
       if(isset($this->request->data['team_id']) ){
        $team_id=implode(",",$this->request->data['team_id']);
        $this->request->data['team_id'] = $team_id;
    	}else{
    	$this->request->data['team_id'] = "";
    	}
    	if(isset($this->request->data['pm_id']) ){
        $pm_id=implode(",",$this->request->data['pm_id']);
        $this->request->data['pm_id'] = $pm_id;
    	}else{
    	$this->request->data['pm_id'] = "";
    	}

    	if(isset($this->request->data['engineeringuser']) ){
        $engineeringuser=implode(",",$this->request->data['engineeringuser']);
        $this->request->data['engineeringuser'] = $engineeringuser;
    	}else{
    	$this->request->data['engineeringuser'] = "";
    	}

    	if(isset($this->request->data['salesfrontuser']) ){
        $salesfrontuser=implode(",",$this->request->data['salesfrontuser']);
        $this->request->data['salesfrontuser'] = $salesfrontuser;
    	}else{
    	$this->request->data['salesfrontuser'] = "";
    	}
    	if(isset($this->request->data['salesbackenduser']) ){
        $salesbackenduser=implode(",",$this->request->data['salesbackenduser']);
        $this->request->data['salesbackenduser'] = $salesbackenduser;
    	}else{
    	$this->request->data['salesbackenduser'] = "";
    	}

    	//pr($this->request->data); die;
        $projects = $this->Projects->patchEntity($project,$this->request->data());
        if(empty($projects->errors())) {
          $this->Projects->save($project);
          $this->Flash->success_new("Process has been updated successfully.");
                  echo 1;
           		  die;
        }else{                                    
         echo json_encode(['status'=>'failed','errors'=>$projects->errors()]);
      		die;
        } 
        }else if(!empty($id)){ 
        $ProjectsDataa = $this->Projects->get($id);
        $ProjectsDataa['id'] =  $this->Common->encForUrl($this->Common->ENC($ProjectsDataa['id']));
        $resultData = $ProjectsDataa->toArray();
              $this->set('ProjectsDataa',$ProjectsDataa); 
              $this->set('resultData',$resultData);

	    if(isset($ProjectsDataa['nextrelease'])){
        $nextrelease= $ProjectsDataa['nextrelease']->format('Y-m-d');
        }else{
    	 $nextrelease= '';
    	}
   		if(isset($ProjectsDataa['finishdate'])){
        $finishdate= $ProjectsDataa['finishdate']->format('Y-m-d');
    	}else{
    	 $finishdate= '';
   		}
    		$this->set('nextrelease',$nextrelease);
            $this->set('finishdate',$finishdate);

			$pass = $this->SaleProfiles->find('all',[
				'fields' => ['SaleProfiles.password','SaleProfiles.username','SaleProfiles.type','Projects.id'],
			 'conditions' =>['Projects.id'=>$id]
			])->contain(['Projects'])->first();
			
		$cpass = $this->Projects->find('all',[
			 'conditions' =>['Projects.id'=>$id]
			])->contain(['Contact'])->first();

			$pasword      = $pass['password'];
			$profile_name = $pass['username'];
			$profile_type = $pass['type'];
			$this->set('pass',$pasword);
			$this->set('pro_name',$profile_name);
			$this->set('pro_type',$profile_type);
			$this->set('cpass',$cpass); 
			$this->set('currentuser',$userSession);

      }                                                          
    }     

		#________________________________________________________________________________________________#
		
		/**	
		* @Date: 15-Feb-2013	
		* @Method : admin_report
		* @Purpose: This function is to generate Projects report from admin section.	
		* @Param: none	
		* @Author: ANAMIKA
		* @Return: none 
		**/	
		 
		function report($key = null) {	
			$this->viewBuilder()->layout(false);
			$session = $this->request->session();
			$this->set('session',$session);
			$userSession = $session->read("SESSION_ADMIN");
			$today = date("Y-m-d");
			if($key == "delayed"){
					$res = $this->Projects->find('all', array(
					'fields' => array('id', 'project_name','nextrelease'),
					'conditions' => array('status'=>'current','nextrelease <' => "$today",'agency_id' => $userSession[3]),
					'order' => array('modified' => 'desc')
					))->all()->toArray();

				$this->set('resultData',$res); 
				$this->render('delayreport'); 
			
			}elseif($key == "payment"){
				$res = $this->Projects->find('all', array(
					'fields' => array('id', 'project_name','nextpayment'),
					'conditions' => array('status'=>'current','nextpayment <' => "$today",'agency_id' => $userSession[3]),
					'order' => array('modified' => 'desc')
				))->all()->toArray();
				$this->set('resultData',$res); 
				$this->render('paymentreport'); 
			
			}elseif($key == "alert"){
					$res = $this->Projects->find('all', array(
					'fields' => array('id', 'project_name','notes','processnotes','onalert','agency_id' => $userSession[3]),
					'conditions' => array('status'=>'current','onalert' => '1'),
					'order' => array('modified' => 'desc')
				))->all()->toArray();
				$this->set('resultData',$res); 
				$this->render('onalertreport'); 
				
			}elseif($key == "hourly" || isset($this->request->data['type'])){			 
				
				if(isset($this->request->data['start_date'])){
				
						$start_date=$this->request->data['start_date'];
						$date = strtotime($start_date);
						$date = strtotime("next sunday", $date);
						$end_date=date('Y-m-d',$date);
						$todate = str_replace("/","-",$this->request->data['start_date']);
						$create=date_create($todate);
						$to = $create->format('Y-m-d H:i:s');
						$create=date_create($end_date);
						$from = $create->format('Y-m-d H:i:s');
					
					$cond = ['Projects.engagagment' => 'hourly','Billings.week_start <=' => $from, 'Billings.week_start >=' => $to,'Projects.agency_id' => $userSession[3]];	
				}else {
					$start_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
					$date = strtotime($start_date);
						if(date('N')==7){

					$start_date=date('Y/m/d',strtotime("last monday"));	
					}
					$cond = ['Projects.engagagment' => 'hourly','Billings.week_start >='=>$start_date,'Projects.agency_id' => $userSession[3]];
				}
				
				$this->set('fromDate',$start_date);
						$result =   $this->Projects->find()
				->select(['Billings.id',
				'Billings.week_start',
				'Billings.project_id',
				'Billings.max_limit',
				'Billings.todo_hours',
				'Billings.mon_hour',
				'Billings.tue_hour',
				'Billings.wed_hour',
				'Billings.thur_hour',
				'Billings.fri_hour',
				'Billings.sat_hour',
				'Billings.sun_hour',
				'Billings.notes',
				'Projects.project_name',
				'Projects.id',
				'Projects.platform'
				])
				->where($cond)
				->order(['Projects.modified' => 'desc'])
				->join([
			       'table' => 'billings',
				 'alias' => 'Billings',
				 'type'=>'INNER',
				 'foreignKey' => false,
				 'conditions' => array ('Billings.project_id = Projects.id')
			    ])->toArray();
	
				$this->set('resultData',$result); 
				$this->render('hourlycontract'); 
				
			}else if($key == "onhold"){
				$res = $this->Projects->find()
					->select(['Projects.id', 'Projects.Project_name', 'Projects.client_id','Projects.notes','Projects.processnotes','Projects.totalamount','Projects.status','Contact.name','Engineer_User.first_name','Engineer_User.last_name','Salefront_User.first_name','Salefront_User.last_name','Saleback_User.first_name','Saleback_User.last_name'])
					->where([ 'Projects.status' => $key,'Projects.agency_id' => $userSession[3]])
					->order(['Projects.modified' => 'desc'])->contain(["Engineer_User","Contact","Salefront_User","Saleback_User","Payments"])->all()->toArray();
				$counter = 0;
				
				$this->set('resultData',$res); 
				$this->render('onhold'); 
			}
		 
		}
		
	#____________________________________________________________________________________________________#
		
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_Projectsdetails
		* @Purpose: This function is to generate Projects detailed report from admin section.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		function projectdetails($id = null){
			$this->viewBuilder()->layout('new_layout_admin');
		    $session = $this->request->session();
      	    $this->set('session',$session);
			$this->set('title_for_layout','Projects Details');		
			$this->set("pageTitle","Projects Details");
			$controller=$this->request->params['controller'];
			$action=$this->request->params['action'];
				
			$userSession = $session ->read("SESSION_ADMIN");
			$role = explode(",",$userSession['2']);
			$orderby = "";
       		$today = date("Y-m-d H:i:s");
       		$id = (is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));
       		//pr($id); die;
            $docs = $this->Documents->find('all',[
                'conditions' => ['Documents.agency_id' => $userSession[3] ,'Documents.entity_id' => 2, 'Documents.attached_to' => $id, ],
                'order' => ['Documents.created desc'],
                ])->contain(['Modifier','Creater']);
            $notes = $this->Notes->find('all',[
                'conditions' => ['Notes.agency_id' => $userSession[3] ,'Notes.entity_id' => 2, 'Notes.attached_to' => $id, ],
                'fields' => ['Notes.id','Notes.title','Notes.content', 'Notes.attached_to', 'Notes.description','Notes.created', 'Notes.last_modifiedby','Modifier.first_name','Modifier.last_name','Creater.first_name','Creater.last_name'],
                'order' => ['Notes.created desc'],
                ])->contain(['Modifier','Creater']);
             if($this->request->is('ajax') == true){
             	$final=[];
             	$finalnotes =[];
             	if(isset($this->request->data['task']) && $this->request->data['task'] == 'reloaddocs'){
             			foreach($docs as $doc){
		             		$doc['id'] = $this->Common->ENC($doc['id']);
			             		$final[] = $doc;
			             	}
			             	 echo json_encode(['status' => 'success', 'docs' => $final ]);
                			 die;
             	} else if(isset($this->request->data['task']) && $this->request->data['task'] == 'getpayments'){
             				
			             	 
             	} else {
             		foreach($notes as $note){
	             		$note['id'] = $this->Common->ENC($note['id']);
	             		$finalnotes[] = $note;
             		}
	                echo json_encode(['status' => 'success', 'docs' => $finalnotes]);
	                die;
             	}
             	
             	
             }
             $this->set('docs',$docs);
             $this->set('notes',$notes);
             if(isset($this->request->data['orderby']) && $this->request->data['orderby']=='asc'){

				$orderby = "asc";
			}else{
				$orderby = "desc";
			}
			$cond = ['Tickets.project_id'=>$id];
			$condarr = [];
			//pr($this->request->data); die;
			if(isset($this->request->data['showopen']) && $this->request->data['showopen']=='no'){
				$opencond = [];
				$showopen = 'no';
			}else{
				//$opencond = ['Tickets.status'=>1];
				$condarr[] = 1;
				$showopen = 'yes';
			}

			if(isset($this->request->data['showclosed']) && $this->request->data['showclosed']=='no'){
				$closecond = [];
				$showclosed ='no';
			}else{
				//$closecond = ['Tickets.status'=>0];
				$condarr[] = 0;
				$showclosed ='yes';
			}

			if(isset($this->request->data['showinprogress']) && $this->request->data['showinprogress']=='no'){
				$inprogresscond = [];
				$showinprogress = 'no';
			}else{
				//$inprogresscond = ['Tickets.status'=>2];
				$condarr[] = 2;
				$showinprogress = 'yes';
			}


			$this->set('orderby',$orderby);
			$this->set('showopen',$showopen);
			$this->set('showclosed',$showclosed);
			$this->set('showinprogress',$showinprogress);
			/*pr($inprogresscond);
			pr($opencond);
			pr($closecond); die;*/
			if(count($condarr) > 0){
				$cond1 = ['Tickets.status IN'=>$condarr ];
			} else {
				$cond1 =[];
			}
			$cond = array_merge($cond,$cond1);
			//pr($cond); die;
			$userID = $userSession[0]; 
			$permission= $this->Common->menu_permissionscontroller($controller,$action,$userSession[0]);
			$condition = ["id" => $id ,'OR' => [ ['pm_id' => $userID ],['engineeringuser' =>
						$userID ],['salesbackenduser' =>$userID ],['salesfrontuser' => $userID ], 'FIND_IN_SET(\''. $userID .'\',Projects.team_id)'] ];
			$records = $this->Projects->find('all', [
				'conditions' => $condition
			])->Count();
			//pr($records); die;
			$prcessHeadID =$this->Common->getprocessheadid();
		
			if(($records > 0) || $userID==ADMIN_ID || $userID==$prcessHeadID || in_array("1", $role) || $permission == 1){
				$result1 = $this->Projects->find('all',[
					"conditions" => ['Projects.id'=>$id]
				])->contain(["Engineer_User","Salefront_User","Saleback_User","ProjectManager",'Contact'])->all()->toArray();
				//pr($result1); die;
				$this->set("projectData",$result1);
     	//pr($cond); die;
     	$this->paginate = [           
		'fields' => [	
		'id',	
		'to_id',	
		'from_id',	
		'title',	
		'description',
		'response',	
		'type',		
		'priority',
		'status',
		'deadline',	
		'tasktime',
		'billable_hours',
		'closedate',
		'ipaddress',
		'last_replier',
		'project_id',
		'created',
		'modified'
		],     
		'page'=> 1,'limit' => 50, 
		'conditions' =>$cond,    
		'order' => ['Tickets.deadline' => $orderby]          
		];     
		$data = $this->paginate('Tickets'); 

           $this->set('ticketData', $data);
			}else{
				$this->Flash->error("Sorry, You are not authorized to see this Projects details.");
				$this->redirect(array('controller'=>'Users','action'=>'dashboard'));
		}
}
		#____________________________________________________________________________________________________#
		
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_csvexport
		* @Purpose: This function is to generate milestone from admin section in xls format.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		
	public function export($id=null) {
		
		$this->response->download('export.csv');

		$data = $this->Tickets->find('all')->where(['Tickets.project_id' => $id]);
		$_serialize = 'data';
   		
   		$this->set(compact('data', '_serialize'));
		$this->viewBuilder()->className('CsvView.Csv');
 		
 		$_header = ['Title', 'Task', 'Time','Billable Hours','Type','Priority','Status','Deadline'];
  		 
  		 $this->set(compact('data', '_serialize', '_header'));

		return;
	}
		
		#________________________________________________________________________________________________________#
		
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_csvimport
		* @Purpose: This function is to read milestone from admin section in csv format.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		
		function admin_import($id=null){
			
			$Projectsid  = $this->params['pass'][0]; 
			$sessionAdmin = $this->Session->read("SESSION_ADMIN");
			$last_id = $sessionAdmin['0']; //logged user id
			$this->Projects->expects(array("Engineer_User","Salefront_User","Saleback_User","ProjectsManager"));
			$result1 = $this->Projects->find("all",array(
				"fields" => array('Projects.*', 'Engineer_User.first_name', 'Engineer_User.last_name','Salefront_User.first_name', 'Salefront_User.last_name', 'Saleback_User.first_name', 'Saleback_User.last_name', 'ProjectsManager.first_name', 'ProjectsManager.last_name'),
				"conditions" => array('Projects.id'=>$Projectsid)
			));
			
			$responsible = $result1[0]['Projects']['pm_id'];
			$accountable = $result1[0]['Projects']['engineeringuser'];
			$ProjectsName = $result1[0]['Projects']['Projects_name'];
			$now = date("Y-m-d H:i:s");
			$file = $_FILES['file_to_upload']['tmp_name'];
			$handle = fopen($file,"r");
			$show = array(); $status = "";
			$readData = array();
			$error = array();
			//loop through the csv file and insert into database
			$i=0;
			$k=0;
			while (($data = fgetcsv($handle)) !== FALSE){
			
				if($i==0){
					if($data[0]!= "Title") $error['0']="error";
					if($data[1]!= "Task") $error['1']="error";
					if($data[2]!= "Time") $error['2']="error";
					if($data[3]!= "Billable Hours") $error['3']="error";
					if($data[4]!= "Type") $error['4']="error";
					if($data[5]!= "Priority") $error['5']="error";
				    if($data[6]!= "Status") $error['6']="error";
					if($data[7]!= "Deadline") $error['7']="error";
				}
				if($i>0){
						if(($data['6']=="Open") || ($data['6']=="open")) $status='1';
						elseif(($data['6']=="Closed") || ($data['6']=="closed")) $status='0';
						else $status='2';
						$deadlineDate  = str_replace("/","-",$data['7']);
						$readData[$k]['Projects_id']= $Projectsid;
						$readData[$k]['title'] = $data['0'];
						$readData[$k]['description'] = $data['1'];
						$readData[$k]['to_id'] = $accountable;
						$readData[$k]['from_id'] = $responsible;
						$readData[$k]['tasktime'] = $data['2'];
						$readData[$k]['billable_hours'] = $data['3'];
						$readData[$k]['type'] = $data['4'];
						$readData[$k]['priority'] = $data['5'];
						$readData[$k]['status'] = $status;
						$readData[$k]['ipaddress'] = $_SERVER['REMOTE_ADDR'];
						$readData[$k]['last_replier'] = $last_id;
						$readData[$k]['deadline'] = date("Y-m-d H:i:s",strtotime($deadlineDate));
						$readData[$k]['created'] = $now;
						$readData[$k]['modified'] = $now; 
						$k++;
				}
				if(count($error)>0){
					$this->Session->setFlash("Sorry, This csv format is incorrect.",'layout_error');		
					echo "ERROR"; die;
				}

				$i++;
			}
			$this->Session->write("CSV_DATA", array($readData));
			$numberoftasks = $i-1;
			if($numberoftasks<200){
				$j=0;
				$handle = fopen($file,"r");
				while (($data = fgetcsv($handle)) !== FALSE){
					if($j>=1){
						if(($data['6']=="Open") || ($data['6']=="open")) $status='1';
						elseif(($data['6']=="Closed") || ($data['6']=="closed")) $status='0';
						else $status='2';
						$deadlineDate  = str_replace("/","-",$data['7']);
						$this->Ticket->id = null;
						$this->request->data['Ticket']['Projects_id'] = $Projectsid;
						$this->request->data['Ticket']['title'] = $data['0'];
						$this->request->data['Ticket']['description'] = $data['1'];
						$this->request->data['Ticket']['to_id'] = $accountable;
						$this->request->data['Ticket']['from_id'] = $responsible;
						$this->request->data['Ticket']['tasktime'] = $data['2'];
						$this->request->data['Ticket']['billable_hours'] = $data['3'];
						$this->request->data['Ticket']['type'] = $data['4'];
						$this->request->data['Ticket']['priority'] = $data['5'];
						$this->request->data['Ticket']['status'] = $status;
						$this->request->data['Ticket']['ipaddress'] = $_SERVER['REMOTE_ADDR'];
						$this->request->data['Ticket']['last_replier'] = $last_id;
						$this->request->data['Ticket']['deadline'] = date("Y-m-d H:i:s",strtotime($deadlineDate));
						$this->request->data['Ticket']['created'] = $now;
						$this->request->data['Ticket']['modified'] = $now;
						$result=$this->Ticket->save($this->request->data);
						
					}
				$j++;
				}
				$this->Session->setFlash("Your files has been imported successfully and database has modified.",'layout_success');		
				echo "SUCCESS";
			}else{
				$show = array('numberoftaks'=>$numberoftasks,'ProjectsName'=>$ProjectsName);
				exit(json_encode($show));
			}
			die;
		}
		#___________________________________________________________________________________________________#
		
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_readcsv
		* @Purpose: This function is to read milestone from admin section in csv format.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		function admin_readcsv(){
			if($this->params['form']['mode']==1){
				$csv = $this->Session->read('CSV_DATA');
				foreach($csv[0] as $read){
					$this->Ticket->id = null;
					$this->request->data['Ticket']['Projects_id'] = $read['Projects_id'];
					$this->request->data['Ticket']['title'] = $read['title'];
					$this->request->data['Ticket']['description'] = $read['description'];
					$this->request->data['Ticket']['to_id'] = $read['to_id'];
					$this->request->data['Ticket']['from_id'] = $read['from_id'];
					$this->request->data['Ticket']['tasktime'] = $read['tasktime'];
					$this->request->data['Ticket']['billable_hours'] = $read['billable_hours'];
					$this->request->data['Ticket']['type'] = $read['type'];
					$this->request->data['Ticket']['priority'] = $read['priority'];
					$this->request->data['Ticket']['status'] = $read['status'];
					$this->request->data['Ticket']['ipaddress'] = $read['ipaddress'];
					$this->request->data['Ticket']['last_replier'] = $read['last_replier'];
					$this->request->data['Ticket']['deadline'] = $read['deadline'];
					$this->request->data['Ticket']['created'] = $read['created'];
					$this->request->data['Ticket']['modified'] = $read['modified'];
					$result=$this->Ticket->save($this->request->data);
				}
				if($result){
					$this->Session->setFlash("Your csv has been imported successfully.",'layout_success');		
					echo "1"; 
				}
			}
			die;
		}
		#___________________________________________________________________________________________________#
		
		/**	
		* @Date: 22-May-2013	
		* @Method : ProjectstatusChangebyAjax
		* @Purpose: This function is to change Projects status by ajax.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		function ProjectstatusChangebyAjax(){

		 	if($this->request->is('ajax'))
			{  
			  if($this->request->data['client_update']=="act"){
					$this->Contacts->updateAll(
								array('iscurrent' => "'1'"),//fields
								array('id' =>$this->request->data['client_id']));
					}
				$update = ['status' => $this->request->data['status']]; 
				$result = 	$this->Projects->updateAll($update,array('id '  => $this->request->data['id']));
				if($result){  
					echo "1"; die;
				}else{
					echo "0"; die;
				}  	
			} 
		}
		#____________________________________________________________________________________________________________#
		
		
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_Projectsdetails
		* @Purpose: This function is to generate Projects detailed report from admin section.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		function closedproject(){
		$this->viewBuilder()->layout('new_layout_admin');
		$this->set('title_for_layout','Closed Projects');		
		$this->set("pageTitle","Closed Projects");
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		$cond = [];
			if($this->request->data){ 
				if(isset($this->request->data['submit']) && ($this->request->data['submit'] == "Save")){
					$this->request->data['Project']['name']=$this->request->data['name'];
					$this->request->data['Project']['is_dropboxback']=$this->request->data['is_dropboxback'];
					$this->request->data['Project']['is_codeback']=$this->request->data['is_codeback'];
					$this->request->data['Project']['operation_notes']=$this->request->data['operation_notes'];
					foreach($this->request->data['Project'] as $fieldname=>$project){
						foreach($project as $key=>$value){
							$process[$key][$fieldname]= $value;	
				}
			} 
					foreach($process as $id=>$value){ 
        $result = $this->Projects->updateAll( [
							'is_dropboxback'  => ($value['is_dropboxback']),
						'is_codeback'  => ($value['is_codeback']),
							'operation_notes'  => ($value['operation_notes']),
						  'modified'   => (date("Y-m-d H:i:s")),
						],
						['id ' => ($id)] );
						}
							if($result="1"){
					$this->Flash->success_new("Project has been updated successfully.",'layout_success');
			        
			        return $this->redirect(
            ['controller' => 'Projects', 'action' => 'closedproject']
        );	
			}
			}else{ 
				$serachName = $this->request->data['project_name'];
				$cond = ['project_name'.' Like' =>'%'.$serachName.'%','Projects.agency_id' => $userSession[3]];
				}
			}else{
				$cond= ['Projects.agency_id' => $userSession[3]]; 
			}
			
			$res = $this->Projects->find('all', [
				'conditions'=>array_merge($cond ,[
        		 'OR'=>[['Projects.is_dropboxback'=>0,'Projects.status' => 'closed'],['Projects.is_codeback'=>0,'Projects.status' => 'closed']]
        		  ])
				])
			->order(['Projects.modified' => 'desc'])
		   ->contain(["Engineer_User","Salefront_User","Saleback_User","ProjectManager","Contact"]);
            $totalcost=isset($res)? $res->toArray():array();
            $this->set('resultData', $totalcost);
		}
		
		
		#_____________________________________________________________________________________#
			
		/**	
		* @Date: 22-May-2013	
		* @Method : admin_Projectsdetails
		* @Purpose: This function is to generate Projects detailed report from admin section.	
		* @Param: none	
		* @Author: Neelam Thakur 
		* @Return: none 
		**/	
		function clientProjectsdetails($id = null){
			$this->layout = "layout_clientadmin";
			
			$this->set('title_for_layout','Projects Details');		
			$this->set("pageTitle","Projects Details");
			
			$userSession = $this->Session->read("SESSION_CLIENT");
			$role = explode(",",$userSession['0']);
	
			$orderby = "";
			if(isset($this->params['named']['orderby']) && $this->params['named']['orderby']=='asc'){
				$orderby = "asc";
			}else{
				$orderby = "desc";
			}
			$userID = $userSession[0];  
			$condition = "`id` = $id and (`client_id` = $userID)";
			$records = $this->Projects->find('count', array(
				'conditions' => $condition
			));
			if($records > 0){
				$this->Projects->expects(array("Engineer_User","Salefront_User","Saleback_User","ProjectsManager"));
				$result1 = $this->Projects->find("all",array(
					"fields" => array('Projects.*', 'Engineer_User.first_name', 'Engineer_User.last_name','Salefront_User.first_name', 'Salefront_User.last_name', 'Saleback_User.first_name', 'Saleback_User.last_name', 'ProjectsManager.first_name', 'ProjectsManager.last_name'),
					"conditions" => array('Projects.id'=>$id)
				));
				
				//get Tickets data
				$this->set("ProjectsData",$result1);
				$this->paginate = array('fields' => array(	
					'Ticket.*'
					),	
					'page'=> 1,
					'limit' => 50,	
					"conditions" => array('Ticket.Projects_id'=>$id),
					'order' => array('Ticket.deadline' => $orderby),
				);			
				$data = $this->paginate('Ticket');	
				$this->set('ticketData', $data);	
			}else{
				
				$this->Session->setFlash("Sorry, You are not authorized to see this Projects details.",'layout_error');
				$this->redirect(array('controller'=>'contacts','action'=>'clientdashboard'));
			}
		}
		#_________________________________________________________________________#
		/**	
		* @Date: 05-March-2013
		* @Method : admin_activelead	
		* @Purpose: This function is to list active leads details .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
		function holdprojects(){
		$this->viewBuilder()->layout(false);
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
        $this->set('title_for_layout','On Hold Projects');	

				$res = $this->Projects->find('all', [
					//'fields' => ['Projects.id', 'Projects.Project_name', 'Projects.client_id','Projects.notes','Projects.processnotes','Projects.totalamount','Contact.name','Engineer_User.first_name','Engineer_User.last_name','Salefront_User.first_name','Salefront_User.last_name','Saleback_User.first_name','Saleback_User.last_name'],
					'conditions' => ['Projects.status'=>'onhold','Projects.agency_id' => $userSession[3]],
					'order' => ['Projects.modified' => 'desc']
				])->contain(["Engineer_User","Contact","Salefront_User","Saleback_User"])->toArray();
				$this->set('resultData',$res); 
		}

			function hold_projects(){
		$this->viewBuilder()->layout(false);
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
        $this->set('title_for_layout','On Hold Projects');	

				$res = $this->Projects->find('all', [
					//'fields' => ['Projects.id', 'Projects.Project_name', 'Projects.client_id','Projects.notes','Projects.processnotes','Projects.totalamount','Contact.name','Engineer_User.first_name','Engineer_User.last_name','Salefront_User.first_name','Salefront_User.last_name','Saleback_User.first_name','Saleback_User.last_name'],
					'conditions' => ['Projects.status'=>'onhold','Projects.agency_id' => $userSession[3]],
					'order' => ['Projects.modified' => 'desc']
				])->contain(["Engineer_User","Contact","Salefront_User","Saleback_User"])->toArray();
				$this->set('resultData',$res); 
		}
		
		
		#_________________________________________________________________________#
		/**	
		* @Date: 19-April-2016
		* @Method : admin_description	
		* @Purpose: shows description in popup box. .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
		
  function description($id) {
	  $this->viewBuilder()->layout(false);
	 		
			 $res = $this->Projects->find('all', [
            'conditions'=> ['id'=>$id],
            'fields' => ['id','processnotes'] ])->first()->toArray();
		
			$this->set('resultData',$res);	
		} 
     /**	
		* @Date: 12-April-2016
		* @Method : addjob	
		* @Purpose: shows description in popup box. .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
	 public function addjob(){
	 	if($this->request->data){
	 	/*	pr($this->request->data);die;*/
	 		$session = $this->request->session();
			$userSession = $session->read("SESSION_ADMIN");
	 		$formdata = $this->request->data;
	 		$data =[];
	 		//foreach($formdata as $fdata){
	 			/*r : pm_id
	 			a : engineeringuser
	 			c : salesfrontuser
	 			i : salesbackenduser*/
	 			foreach($formdata['clientinfo'] as $client){
	 				$data['client_id'] = $this->Common->DCR($client['id']);
	 			}
	 			foreach($formdata['leadinfo'] as $lead){
	 				//pr($this->Common->DCR($lead['id'])); die;
	 				$data['lead_id'] = $this->Common->DCR($lead['id']);	
	 			}	
	 			$resids=[];
	 			foreach($formdata['responsibles'] as $res){
	 				//pr($res); die;
	 				$resids[] = $this->Common->DCR($res['id']);
	 			}
	 			//pr($resids); die;
	 			$data['pm_id'] = implode(',', $resids);
	 			$accids=[];
	 			foreach($formdata['accountables'] as $res){
	 				$accids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['engineeringuser'] = implode(',', $accids);
	 			$infoids=[];
	 			foreach($formdata['informers'] as $res){
	 				$infoids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['salesbackenduser'] = implode(',', $infoids);
	 			$conids=[];
	 			foreach($formdata['consultants'] as $res){
	 				$conids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['salesfrontuser'] = implode(',', $conids);
	 			$data['project_name'] = $formdata['project_name'];
	 			$data['technology'] = $formdata['technology'];
	 			$data['notes'] = $formdata['description'];
	 			$data['status'] = $formdata['status'];
	 			$data['source'] = $formdata['source'];
	 			$data['profile_id'] = $formdata['sale_profile'];
	 			$data['credentials'] = $formdata['credentials'];
	 			$data['engagagment'] = strtolower($formdata['engagagment']);
	 			$data['totalamount'] = $formdata['prospectiveamount'];
	 			$data['payment'] = $formdata['payment'];
	 			$data['agency_id'] = $userSession[3];
	 			$data['hourly_rate'] = (isset($formdata['hourlyrate']) && ($formdata['hourlyrate'] !== '' ) ) ? $formdata['hourlyrate'] : 0 ; 
	 			//pr($data); die;
	 			$projects = $this->Projects->newEntity($data);
				if(empty($projects->errors())){
 				$this->Projects->save($projects);
 				Cache::clear(false,'short');
				$this->Flash->success_new("Project has been created successfully.");
				$id =  $this->Common->encForUrl($this->Common->ENC($projects->id));
				echo json_encode([ 'status' => 'success' , 'id' => $id ]); die;
	       		}else{
			          //$this->set("errors", $projects->errors());
	       			echo json_encode(['status' => 'failed','errors' =>  $projects->errors() ]); die;
			        }

	 	}

	 }
	  public function getherdata() {
	  	Configure::write('debug', 2);
	 	if($this->request->is('ajax')){ 
	 				$session = $this->request->session();
					$userSession = $session->read("SESSION_ADMIN"); 
					if(in_array('1', explode(',', $userSession[2])) || in_array('8', explode(',', $userSession[2])) ){
						$leadcond = ['agency_id' => $userSession[3],'status' => 'active'];
					} else {
						$leadcond = ['agency_id' => $userSession[3],'status' => 'active' ,'salesfront_id' => $userSession[0]];
					}
					$clients = $this->Contacts->find('all',[
							'conditions' => ['iscurrent' => 1,'agency_id' => $userSession[3]],
							'fields' => ['id' ,'name']
						]);
					$leads = $this->Leads->find('all',[
							'conditions' => $leadcond,
							'fields' => ['id','url_title']
						]);
					$users = $this->Users->find('all',[
							'conditions' => ['agency_id' => $userSession[3],'status' => 1],
							'fields' => ['id','first_name' ,'last_name','username','role_id']
						]);
					$admin = $this->Agencies->find('all',[
							'conditions' => ['id' => $userSession[3]],
							'fields' => ['id','first_name' ,'last_name','username']
					])->first();
					$allclients =[];
					$allleads = [];
					$allusers = [];
					$adminarr =[];
					if($this->request->data['key'] == 'editprojectcredentials'){
						$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));	
	 				    $project = $this->Projects->get($id);
	 				    echo json_encode(['status' => 'success', 'project' => $project ]);
				    	die;
					}
					foreach($clients as $client){
							$client['value'] = $this->Common->ENC($client['id']);
			             	$client['label'] = $client['name'];
			             	unset($client['id']);
			             	unset($client['name']);
			             	$allclients[] = $client;
					}
					foreach ($leads as $lead) {
						$lead['Value'] = $this->Common->ENC($lead['id']);
						$lead['label'] = $lead['url_title'];
		             	unset($lead['id']);
		             	unset($lead['url_title']);
		             	$allleads[] = $lead;
					}
					if($this->request->data['key'] == 'editprojectlead'){
	 					$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));	
	 				    $project = $this->Projects->get($id);

	 				    $project['id'] = $this->Common->ENC($project['id']);

	 				    if($project->lead_id > 0){
	 				    	$leaddata = $this->Leads->get($project->lead_id);
	 				        $leaddata['id'] = $this->Common->ENC($leaddata['id']);	
	 				    } else {
	 				    	$leaddata = [];
	 				    }
	 				    //$leaddata = $this->Leads->get($project->lead_id);
	 				       // $leaddata['id'] = $this->Common->ENC($leaddata['id']);	
	 				    ///pr($leaddata); die;
	 				    echo json_encode(['status' => 'success','leaddata' => $leaddata, 'project' => $project , 'leads' => $allleads]);
				    	die;
	 				} else {
	 					$leaddata = [];
	 					$project = [];
	 				}
	 				$finaladminarr = [];
					foreach ($users as $user) {
						$user['Value'] = $this->Common->ENC($user['id']);
						$user['label'] = $user['first_name'].' '.$user['last_name'];
		             	unset($user['id']);
		             	unset($user['url_title']);
		             	$allusers[] = $user;
		             	$exp = explode(',', $user['role_id'] );
		             	
		             	if(in_array( '1' , $exp)) {
		             			//pr($user['label']);
		             			$adminarr =[];	
			             		$adminarr['Value'] = $user['Value'];
								$adminarr['label'] = $user['label'];
							//	pr($adminarr);
								$finaladminarr[] = $adminarr;
			             }
					}
					
					if($this->request->data['key'] == 'editprojecteam'){

						$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));	
	 				    $project = $this->Projects->get($id);
	 				    //pr($project); die;
						 $project['id'] = $this->Common->ENC($project['id']);
						$responsibles = $this->Users->find('all',[
		        		'conditions' => ['id IN' => explode(',', $project['pm_id'])]
		        		]);
			        	$responsiblesarr = [];
			        	foreach ($responsibles as $user) {

							$user['Value'] = $this->Common->ENC($user['id']);
							$user['label'] = $user['first_name'].' '.$user['last_name'];
			             	unset($user['id']);
			             	unset($user['url_title']);
			             	$responsiblesarr[] = $user;
			             	
			             	
						}	

						$accountables = $this->Users->find('all',[
		        		'conditions' => ['id IN' => explode(',', $project['engineeringuser'])]
		        		]);
			        	$accountablesarr = [];
			        	foreach ($accountables as $user) {
							$user['Value'] = $this->Common->ENC($user['id']);
							$user['label'] = $user['first_name'].' '.$user['last_name'];
			             	unset($user['id']);
			             	unset($user['url_title']);
			             	$accountablesarr[] = $user;
						}

						$consultants = $this->Users->find('all',[
		        		'conditions' => ['id IN' => explode(',', $project['salesfrontuser'])]
		        		]);
			        	$consultantsarr = [];
			        	foreach ($consultants as $user) {
							$user['Value'] = $this->Common->ENC($user['id']);
							$user['label'] = $user['first_name'].' '.$user['last_name'];
			             	unset($user['id']);
			             	unset($user['url_title']);
			             	$consultantsarr[] = $user;
						}

						$informers = $this->Users->find('all',[
		        		'conditions' => ['id IN' => explode(',', $project['salesbackenduser'])]
		        		]);
			        	$informersarr = [];
			        	foreach ($informers as $user) {
							$user['Value'] = $this->Common->ENC($user['id']);
							$user['label'] = $user['first_name'].' '.$user['last_name'];
			             	unset($user['id']);
			             	unset($user['url_title']);
			             	$informersarr[] = $user;
						}
						//r($finaladminarr); die;	
						$saleprofiles = $this->Common->getprofile($userSession[3]);
						echo json_encode(['status' => 'success','responsibles' => $responsiblesarr , 'accountables' => $accountablesarr , 'consultants' => $consultantsarr ,'informers' => $informersarr ,'saleprofiles' => $saleprofiles, 'users' => $allusers,'admin' => $finaladminarr , 'project' => $project]);
				    	die;
					}

					
					$saleprofiles = $this->Common->getprofile($userSession[3]);
					echo json_encode(['status' => 'success', 'leads' => $allleads , 'saleprofiles' => $saleprofiles, 'users' => $allusers,'admin' => $finaladminarr ,'leaddata' => $leaddata, 'project' => $project,'clients' => $allclients]);
				    die;
			}
	 	}

	 public function getdocs(){
	 	            if($this->request->is('ajax') == true){
	 	            	$session = $this->request->session();
	 	            	$id = (is_numeric($this->request->data['pid']) ) ? $this->request->data['pid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['pid']));	
						$userSession = $session->read("SESSION_ADMIN");
						if($this->request->data['task'] == 'docs'){
								 $docs = $this->Documents->find('all',[
				                'conditions' => ['Documents.agency_id' => $userSession[3] ,'Documents.entity_id' => 2, 'Documents.attached_to' => $id, ],
				                'order' => ['Documents.created desc'],
				                ])->contain(['Modifier','Creater']);
				         $notes = $this->Notes->find('all',[
				                'conditions' => ['Notes.agency_id' => $userSession[3] ,'Notes.entity_id' => 2, 'Notes.attached_to' => $id, ],
				                'fields' => ['Notes.id','Notes.title','Notes.content', 'Notes.attached_to', 'Notes.description','Notes.created', 'Notes.last_modifiedby','Modifier.first_name','Modifier.last_name','Creater.first_name','Creater.last_name'],
				                'order' => ['Notes.created desc'],
				                ])->contain(['Modifier','Creater']);
		             	$final=[];
		             	$finalnotes =[];
		             	if(isset($this->request->data['task']) && $this->request->data['task'] == 'docs'){
		             			foreach($docs as $doc){
				             		$doc['id'] = $this->Common->ENC($doc['id']);
					             		$final[] = $doc;
					             	}
					             	foreach($notes as $note){
					             		$note['id'] = $this->Common->ENC($note['id']);
					             		$finalnotes[] = $note;
				             		}
					             	 echo json_encode(['status' => 'success', 'docs' => $final,'notes' => $finalnotes , 'pid' => $this->request->data['pid'] ]);
	                				 die;
		             	} else {
		             		foreach($notes as $note){
			             		$note['id'] = $this->Common->ENC($note['id']);
			             		$finalnotes[] = $note;
		             		}
			                echo json_encode(['status' => 'success', 'docs' => $finalnotes]);
			                die;
		             	}
		             	
		             	}  else if($this->request->data['task'] == 'getteam'){
		             		$project = $this->Projects->get($id);
		             		if(count($project) > 0){
		             			$r = $this->Common->getusersfromids(explode(',', $project['pm_id']));
		             			$a = $this->Common->getusersfromids(explode(',', $project['engineeringuser']));
		             			$c = $this->Common->getusersfromids(explode(',', $project['salesfrontuser']));
		             			$i = $this->Common->getusersfromids(explode(',', $project['salesbackenduser']));
		             			echo json_encode(['status' => 'success', 'r' => $r , 'a' => $a, 'c' => $c, 'i' => $i]);
			                	die;
		             		}

		             	} else if ($this->request->data['task'] == 'payments') {
		             		 $project = $this->Projects->get($id);		             		 
								$project['id'] =  $this->Common->ENC($project['id']);
		             		 $result = $this->Payments->find('all',[
											'conditions'=>['Projects.id'=> $id] 
											])
									        ->order(['payment_date' => 'desc'])
									        ->contain(['Projects','Users'])->toArray();	
								$finalpayments=[];
							foreach($result as $payment){
								$payment['id'] =  $this->Common->ENC($payment['id']);
								$payment['payment_date'] = date_create($payment['payment_date'])->format('Y-m-d');
								$finalpayments[] = $payment;
							}
							if(isset($this->request->data['renderview']) && $this->request->data['renderview'] == 'yes'){
									$permission=$this->Common->menu_permissionscontroller('Payments','quickview',$userSession[0]);
									if($permission != 1){
										//IF USER DOES NOT HAS PERMISSION TO VIEW PAYMENTS										
										echo json_encode(['status' => 'success', 'payments' => [] , 'project' => $project ,'pid' => $this->request->data['pid'] ]);
			               	   			die;	
									} else {
										//IF USER HAS PEMISSIONS 
										//GO AHEAD VIEW PAYMENTS
										return $this->redirect(
										        ['controller' => 'Payments', 'action' => 'quickview','prefix'=> 'payments','id' => $this->request->data['pid']]
										    );				
									}
								
							} else {
								echo json_encode(['status' => 'success', 'payments' => $finalpayments , 'project' => $project ,'pid' => $this->request->data['pid'] ]);
			               	   die;	
							}
									        

		             	} else if ($this->request->data['task'] == 'notes') {
		             		 $result = $this->Projects->get($id);
								$finalprojectdata=[];
								$result['id'] =  $this->Common->ENC($result['id']);
							//}
							echo json_encode(['status' => 'success', 'project' => $result ]);
			                die;			        

		             	} else if ($this->request->data['task'] == 'notes') {
							$result = $this->Projects->get($id);
							$finalprojectdata=[];
							$result['id'] =  $this->Common->ENC($result['id']);
							echo json_encode(['status' => 'success', 'project' => $result ]);
			                die;			        		             			

		             	} else if ($this->request->data['task'] == 'milestones') {
							$project = $this->Projects->get($id);
							//pr($project->engagagment); die;
							if($project->engagagment == 'hourly'){
								return $this->redirect(
							        ['controller' => 'Billings', 'action' => 'quickview','prefix'=> 'billings','id' => $this->request->data['pid']]
							    );
							} else {
								return $this->redirect(
							        ['controller' => 'Milestones', 'action' => 'milestoneslist','prefix'=> 'milestones','id' => $this->request->data['pid']]
							    );
							}
			                
		             	} 
		             	
		             }
		 }


		 public function editcname(){
		 		 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");
		        if($this->request->is('ajax') == true){
		            $this->Contacts->updateAll(['name' => $this->request->data['value']],['id' => $this->Common->DCR($this->request->data['pk']) ]);
		            echo json_encode(['status' => 'success','id' => $this->request->data['pk']]);
		                die;
		            } 
				 }
		 public function putonalert(){
				 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");

		        if($this->request->is('ajax') == true){
		        	$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));		  
		        	$onalert = ($this->request->data['putonalert'] == 'yes') ? 1 :  0;
		            $this->Projects->updateAll(['onalert' => $onalert ],['id' => $id ]);
		            echo json_encode(['status' => 'success']);
		             die;
		            } 
			}
		public function togglestatus($id = null){
				 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");
		        if($this->request->is('ajax') == true){
		        	$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));		  
		        	$projectdata = $this->Projects->get($id);
		        	$oldstatus =$projectdata['status'] ;
		            $this->Projects->updateAll(['status' => $this->request->data['status'] ],['id' => $id ]);
		            echo json_encode(['status' => 'success','oldstatus' => $oldstatus]);
		             die;
		            } 
			}
		public function projectlead($id = null){
			$this->viewBuilder()->layout(false);
			 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");
		       if($this->request->is('ajax') == true){
		       	$this->autoRender = false; // avoid to render view
		       	$formdata = $this->request->data;
		       	$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));	
		       		foreach($formdata['leadinfo'] as $lead){
		       				$data = [];
	 						$data['lead_id'] = $this->Common->DCR($lead['id']);	
	 						$this->Projects->updateAll($data,['id' => $id ]);
	 						
	 					 	$resultJ = json_encode(['response' => 'success']);
			             	$this->response->type('json');
			             	$this->response->body($resultJ);
			             	return $this->response;
	 				}

		     	}
		         	$this->set('projectid',$id);
		        	$id = ( is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));		  
		        	$projectdata = $this->Projects->get($id);
		        	$this->set('project',$projectdata);
		        	$this->set('agency_id',$userSession[3]);
			}

			public function projectcredentials($id = null){
			$this->viewBuilder()->layout(false);
			 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");
		       if($this->request->is('ajax') == true){
		       	$this->autoRender = false; // avoid to render view
		       	$formdata = $this->request->data;
		       	$id = (is_numeric($this->request->data['pk']) ) ? $this->request->data['pk'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['pk']));	
		       		//foreach($formdata['leadinfo'] as $lead){
		       				$data = [];
	 						$data['credentials'] = $this->request->data['value'];
	 						$this->Projects->updateAll($data,['id' => $id ]);
	 					 	$resultJ = json_encode(['response' => 'success']);
			             	$this->response->type('json');
			             	$this->response->body($resultJ);
			             	return $this->response;
	 				//}

		     	}
		         	$this->set('projectid',$id);
		        	$id = ( is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));		  
		        	$projectdata = $this->Projects->get($id);
		        	$this->set('project',$projectdata);
		        	$this->set('agency_id',$userSession[3]);
			}

			public function editprojecteam($id = null){
			$this->viewBuilder()->layout(false);
			 $session = $this->request->session();
		         $userSession = $session->read("SESSION_ADMIN");
		       if($this->request->is('ajax') == true){
		       	$this->autoRender = false; // avoid to render view
		       	$formdata = $this->request->data;
		       	//pr($formdata); die;
		       	$id = (is_numeric($this->request->data['projectid']) ) ? $this->request->data['projectid'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['projectid']));	
		       		//foreach($formdata['leadinfo'] as $lead){
		       	$data = [];
		       	$resids=[];
	 			foreach($formdata['responsibles'] as $res){
	 				//pr($res); die;
	 				$resids[] = $this->Common->DCR($res['id']);
	 			}
	 			//pr($resids); die;
	 			$data['pm_id'] = implode(',', array_unique($resids));
	 			$accids=[];
	 			foreach($formdata['accountables'] as $res){
	 				$accids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['engineeringuser'] = implode(',', array_unique($accids));
	 			
	 			$conids=[];
	 			foreach($formdata['consultants'] as $res){
	 				//pr($this->Common->DCR($res['id']));
	 				$conids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['salesfrontuser'] = implode(',', array_unique($conids));

	 			$infoids=[];
	 			foreach($formdata['informers'] as $res){
	 				$infoids[] = $this->Common->DCR($res['id']);
	 			}
	 			$data['salesbackenduser'] = implode(',', array_unique($infoids));
	 			//pr($data); die;
	 						$this->Projects->updateAll($data,['id' => $id ]);
	 						Cache::clear(false,'project');
	 					 	$resultJ = json_encode(['response' => 'success']);
			             	$this->response->type('json');
			             	$this->response->body($resultJ);
			             	return $this->response;
		     	}
		         	$this->set('projectid',$id);
		        	$id = ( is_numeric($id) ) ? $id : $this->Common->DCR($this->Common->dcrForUrl($id));		  
		        	$projectdata = $this->Projects->get($id);
		        	//pr($projectdata->toArray());

		        	$this->set('project',$projectdata);
		        	$this->set('agency_id',$userSession[3]);
			}

		public function cngengagement()	{
			 if($this->request->is('ajax') == true){
		       	$this->autoRender = false; // avoid to render view
		       	$formdata = $this->request->data;
		       	$id = (is_numeric($this->request->data['pk']) ) ? $this->request->data['pk'] : $this->Common->DCR($this->Common->dcrForUrl($this->request->data['pk']));	
		       		//foreach($formdata['leadinfo'] as $lead){
		       				$data = [];
	 						$data['engagagment'] = $this->request->data['value'];
	 						$this->Projects->updateAll($data,['id' => $id ]);
	 					 	$resultJ = json_encode(['response' => 'success']);
			             	$this->response->type('json');
			             	$this->response->body($resultJ);
			             	return $this->response;
	 				//}

		     	}
		}

	}	
		


	 			