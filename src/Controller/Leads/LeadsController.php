<?php 


namespace App\Controller\Leads;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use App\Controller\AppController;
use Cake\Error\Debugger;
use Cake\Utility\Sanitize;
use Cake\Utility\Security;
use Cake\Utility\Hash;
use Cake\Auth\AbstractPasswordHasher;
use Cake\View\Helper\SessionHelper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Validation\Validator;
use Cake\Routing\Router;
use Cake\Cache\Cache;
class LeadsController extends AppController {

	/*************************** START FUNCTIONS **************************/

#_________________________________________________________________________#

    /**                                                           
    * @Date: Nov,2016                                        
    * @Method : beforeFilter
    * @Purpose: This function is called before any other function.
    * @Param: none                                                
    * @Return: none                                               
    **/

    function beforeFilter(Event $event) {


    	ini_set('memory_limit', '256M');
   	// star to check permission
    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");

    // end code to check permissions
    	parent::beforeFilter($event);
    	Router::parseNamedParams($this->request);

    	if (!empty($this->request->query['prefix']) && $this->request->query['prefix'] == "admin") {

    		$this->checkUserSession();
    	} else {
    		$this->viewBuilder()->layout('new_layout_admin');

    	}

    	$session = $this->request->session();
    	$userSession = $session->read("SESSION_ADMIN");
    	if($userSession==''){
    		return $this->redirect( ['prefix'=>'admin' ,'controller' => 'users', 'action' => 'login']);
    	}
    	$this->set('common', $this->Common);
    }



    #_________________________________________________________________________#

    /**      
    * @Date: Nov,2016
    * @Method : index           
    * @Purpose: This function is the default function of the controller
    * @Param: none                                                     
    * @Return: none                                                    
    **/
    
    public function initialize()
    {
    	parent::initialize();
        $this->loadModel('StaticPages'); // for importing Model
        $this->loadModel('Attendances'); // for importing Model
        $this->loadModel('Tickets'); // for importing Model
        $this->loadModel('Evaluations'); // for importing Model
        $this->loadModel('Users');
        $this->loadModel('AlternateLogin');
        $this->loadModel('Roles');
        $this->loadModel('Appraisals');
        $this->loadModel('Credentials');
        $this->loadModel('SaleProfiles');
        $this->loadModel('CredentialLogs');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Common');
        $connection = ConnectionManager::get('default'); // for custom queries
        $session = $this->request->session();
        $this->set('session',$session);
        $this->loadComponent('Paginator');
  		$this->loadModel('Leads'); // for importing Model
  	}
  	function index() {
  		$this->render('login');
  		if($session->read("SESSION_USER") != ""){      
  			$this->redirect('dashboard');                        
  		}    
  	}



	#_________________________________________________________________________#

    /**                         
    * @Date: Nov,2016        
    * @Method : leadslist      
    * @Purpose: This function is to show list of Leads in the System
    * @Param: none                                                        
    * @Return: none                                                       
    **/

    function leadslist(){

    	$this->viewBuilder()->layout('new_layout_admin');
    	$user = $this->Auth->user();
    	ini_set('memory_limit', '256M');
    	$this->set('title_for_layout','Leads Listing');      
    	$this->set("pageTitle","Leads Listing");             
    	$this->set("search1", "");                      
    	$this->set("search2", "");                       
      $criteria = "1"; //All Searching
   $session = $this->request->session();
		$this->set('session',$session);
       $session_info = $session->read("SESSION_ADMIN");
        $userAgency=$session_info[3];
      $session->delete('SESSION_SEARCH');

      ini_set('memory_limit', '256M');

      $userData = $session->read("SESSION_ADMIN");

      $sessionID = $userData[0];
      $todaylead=$this->Leads->find('all',['conditions' => ['salesfront_id' => $sessionID,'bidding_date' => date('y-m-d')]
      	])->count();


      $this->set('todayleadcount',$todaylead);
      if($user != null)
      {  

      	if(isset($this->request->data)) {

      		$search2 = ['salesfront_id'=>$sessionID,'Leads.agency_id'=>$userAgency];

      		if(!empty($this->request->data['status'])){

      			$cond = ['Leads.status Like' => '%'.$this->Common->sanitizeInput($this->request->data['status']).'%'];
      			$search2 = array_merge($search2, $cond);
      		}
      		if(!empty($this->request->data['url'])){

      			$cond = ['Leads.url_title LIKE' => '%'.$this->Common->sanitizeInput($this->request->data['url']).'%'];
      			$search2 = array_merge($search2, $cond);
      		}
      		if(!empty($this->request->data['user_id'])){


      			$cond = ['Leads.salesfront_id' => $this->Common->sanitizeInput($this->request->data['user_id'])];
      			$search2 = array_merge($search2, $cond);	
      		}
      		if(!empty($this->request->data['notes'])){        

      			$cond = ['Leads.notes LIKE' => '%'.$this->Common->sanitizeInput($this->request->data['notes']).'%'];
      			$search2 = array_merge($search2, $cond);	
      		}
      		if( !empty($this->request->data['sdate']) &&  !empty($this->request->data['edate']) ){

      			$create=date_create($this->request->data['sdate']);

      			$to = $create->format('Y-m-d');
      			$create=date_create($this->request->data['edate']);
      			$from = $create->format('Y-m-d');

      			$cond = ['Leads.bidding_date >=' => $to, 'Leads.bidding_date <=' => $from];
      			$search2 = array_merge($search2, $cond);

      		}
      		if(!empty($this->request->data['start_amount']) &&  !empty($this->request->data['last_amount'])){
      			$cond = ['Leads.perspective_amount >=' => $this->Common->sanitizeInput($this->request->data['start_amount']), 'Leads.perspective_amount <=' => $this->Common->sanitizeInput($this->request->data['last_amount'])];
      			$search2 = array_merge($search2, $cond);

      		}
      		$this->set("search2",$search2);      
      	}

      	if((!empty($search2))){

      		$query  =    $this->Leads->find("all" , [
      			'conditions' => $search2,
      			])->contain(['Users']);
      		$result = isset($query)? $query:array();
      		$criteria = $search2; 		
      		$session->write('SESSION_SEARCH', $criteria);	
      	}else{
      		$query  =    $this->Leads->find("all" , [
      			'conditions' => ['Leads.agency_id'=>$userAgency],
      			])->contain(['Users']);
      		$this->set("search2","");	
      	}	
      	$urlString = "/";          
      	if(isset($this->request->query)){ 
      		$completeUrl  = array();
      		if(!empty($this->request->query['page']))      
      			$completeUrl['page'] = $this->request->query['page'];

      		if(!empty($this->request->query['sort']))                  
      			$completeUrl['sort'] = $this->request->query['sort'];

      		if(!empty($this->request->query['direction']))             
      			$completeUrl['direction'] = $this->request->query['direction'];

      		foreach($completeUrl as $key=>$value){                      
      			$urlString.= $key.":".$value."/";                           
      		}
      	}

      	$this->set('urlString',$urlString);      

      	if(!empty($setValue)){                            
      		if(isset($this->request->query['IDs'])){          
      			$saveString = implode("','",$this->request->query['IDs']);      
      		}

      		if($saveString != ""){                                          
      			$this->Lead->updateAll($setValue,"Leads.id in ('".$saveString."')");                                                                 
      			$session->setFlash($messageStr, 'layout_success');                          
      		}

      	}
      }

      $this->set('urlString', $urlString);

$this->set('pagename','evalist');
$paginate_count=isset($query)? $query->toArray():array();
$paginate=ceil(count($paginate_count)/100);
$this->set('paginatecount',$paginate);

      $data = $this->paginate($query,[
      	'page' => 1, 'limit' => 100,
      	'order' => ['Leads.id' => 'desc'],
      	'paramType' => 'querystring'
      	]);

      $this->set('resultData', $data);
      $this->set("pagename","leadlist");
      
  }                                                       

function newList(){
     $session = $this->request->session();
      $this->set('session',$session);
      $session_info = $session->read("SESSION_ADMIN");
      //pr($session_info); die;
      $this->set('session_info',$session_info);
      $userID = $session_info[0];
      $this->viewBuilder()->layout('new_layout_admin');
      $this->set("title_for_layout", "Leads Listing");

}


    function getOrigin($id = null, $key = null){
        if(isset($this->request->data['key'])){
          $key = $this->request->data['key']; 
        } else{
          $key ="";
        }
      //pr($this->request->data); die;
      $session = $this->request->session();
      $this->set('session',$session);
      $session_info = $session->read("SESSION_ADMIN");
      $currentuserroles = $session_info[2];

  if(isset($this->request->data['searchuserid']) && $this->request->data['searchuserid'] !=='none'){
    $userID =$this->request->data['searchuserid'];
  } else {
    $userID = $session_info[0];
  }
      
      $condition_check = [];
    if(isset($this->request->data['check_it']) && $this->request->data['check_it']=='1'){
      $cond = [];
      if(in_array(1 , explode(',', $session_info[2])) || in_array(8 , explode(',', $session_info[2]))) {
          $cond = [];
       } else {
          $cond = ['salesfront_id' => $userID];
       }
    }else {
      $cond = [];
    }
    if(isset($this->request->data['tag_id']) && $this->request->data['tag_id'] !== 0){
        $tagcond = ['Tickets.tag_id' => $this->request->data['tag_id']];
      } else{
        $tagcond = [];
      }

    if(isset($this->request->data['searchkey']) && $this->request->data['searchkey'] !== 'none' && !empty($this->request->data['searchval'])){
      if($this->request->data['searchkey'] == 'to_name'){
        $searchcond = ['OR' => [ ['Users.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ] , ['Users.last_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ]] ] ;
      } else if($this->request->data['searchkey'] == 'from_name'){
        $searchcond = ['OR' => [ ['Users.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ] , ['Users.first_name Like' =>'%'.$this->request->data['searchval']['searchval'].'%' ]] ,
                  'to_id' => $userID
                      ] ;
      } else if($this->request->data['searchkey'] == 'title'){
        $searchcond = [
                'Leads.url_title Like' =>'%'.$this->request->data['searchval']['searchval'].'%'
                      ] ;
      } else if($this->request->data['searchkey'] == 'dates'){
        //pr("asdf"); die;
        $searchcond = ['bidding_date >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'bidding_date <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
      } else if($this->request->data['searchkey'] == 'created'){
        //pr("asdf"); die;
        $searchcond = ['Leads.created >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'Leads.created <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
      } else if($this->request->data['searchkey'] == 'modified'){
        //pr("asdf"); die;
        $searchcond = ['Leads.modified >' => date_create($this->request->data['searchval']['startdate'])->format('Y-m-d H:i:s'), 'Leads.modified <' => date_create($this->request->data['searchval']['enddate'])->format('Y-m-d H:i:s') ] ;
      }
      
        
      } else{
        $searchcond = [];
      }     
    
   $deadline =  $this->Tickets->find("all")
    ->where([
    'OR' => [['from_id' => $userID], ['to_id' => $userID]],
    'status IN' => [1,2]
    ])
  ->order(['created' => 'DESC'])->first();
  
  isset($deadline)? $deadline->toArray():array();
  $oderby = ['Leads.modified' => 'DESC'];
  $prioritycond = [];
  if(isset($this->request->data['filter']) && $this->request->data['filter'] !== 'none') {
    $result = array();
    if($this->request->data['filter'] == 'modified'){
      $oderby = ['Leads.modified' => 'asc'];
    } else if($this->request->data['filter'] == 'created'){
      $oderby = ['Leads.created' => 'asc'];
    } else if( $this->request->data['filter'] == 'created' ){
      $oderby = ['Leads.deadline' => 'asc'];
    }
    
    if($this->request->data['filter'] == 'normal'){
      $prioritycond = ['Leads.priority' => $this->request->data['filter'] ];
    } else if($this->request->data['filter'] == 'urgent'){
      $prioritycond = ['Leads.priority' => $this->request->data['filter'] ] ;
    } else if($this->request->data['filter'] == 'medium'){
      $prioritycond = ['Leads.priority' => $this->request->data['filter'] ];
    } else if($this->request->data['filter'] == 'emergency'){
      $prioritycond = ['Leads.priority' => $this->request->data['filter'] ];
    }

    $oderby = ($this->request->data['filter'] == 'modified') ?  ['Leads.modified' => 'asc'] : ($this->request->data['filter'] == 'created') ? ['Leads.created' => 'asc'] :  ['Leads.deadline' => 'asc'];
    if(!empty($key) && $this->request->data['check_it'] !== 1) {
    
      $today = date("Y-m-d 00:00:00");
      if($key == "open"){
        $cond = ['Leads.status' => 'cold'];
      }elseif($key == "close"){
         $cond = ['Leads.status' => 'closed'];
      }elseif($key == "expire"){
        $cond = ['Leads.status' => 'hold'];
      }elseif($key == "inprogress"){
         $cond = ['Leads.status' => 'active'];
      } else if ($key == "scheduled"){
         $cond = ['Leads.status' => 'deny'];
      } else if ($key == "mandatory"){
         $cond = ['Leads.status' => 'success'];
      }
      $cond = array_merge($cond,$tagcond,$searchcond,$prioritycond);
    }
  }elseif(!empty($key)) {
    $today = date("Y-m-d 00:00:00");
   if($key == "open"){
        $cond = ['Leads.status' => 'cold'];
      }elseif($key == "close"){
         $cond = ['Leads.status' => 'closed'];
      }elseif($key == "expire"){
        $cond = ['Leads.status' => 'hold'];
      }elseif($key == "inprogress"){
         $cond = ['Leads.status' => 'active'];
      } else if ($key == "scheduled"){
         $cond = ['Leads.status' => 'deny'];
      } else if ($key == "mandatory"){
         $cond = ['Leads.status' => 'success'];
      }
  }

    if(!empty($this->request->data['ticket_id']) && $this->request->data['ticket_id'] != 'none'){
      $cond =['Leads.id' => $this->request->data['ticket_id']];
    }
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session_info;
      $today = date("Y-m-d 00:00:00");
       if(in_array(1 , explode(',', $userSession[2])) || in_array(8 , explode(',', $userSession[2]))) {
          $cond = $cond;
       } else {
          $cond = array_merge($cond,['salesfront_id' => $userID]);
       }
       $c = $cond;
       $open_cond = array_merge($c, ['Leads.status' => 'cold']);
     
        $inprogress_cond = array_merge($c, ['Leads.status' => 'active']);
        $closed_cond =  array_merge($c, ['Leads.status' => 'closed']);
        $expired_cond = array_merge($c, ['Leads.status' => 'hold']);
        $schedule_cont = array_merge($c, ['Leads.status' => 'deny']);
        $mandatory_cont = array_merge($c, ['Leads.status' => 'success']);

      $mandatory_cont = array_merge($mandatory_cont,$tagcond,$searchcond,$prioritycond);
      $open_cond = array_merge($open_cond,$tagcond,$searchcond,$prioritycond);
      $schedule_cont = array_merge($schedule_cont,$tagcond,$searchcond,$prioritycond);
      $expired_cond = array_merge($expired_cond,$tagcond,$searchcond,$prioritycond);
      $closed_cond = array_merge($closed_cond,$tagcond,$searchcond,$prioritycond);
      $inprogress_cond = array_merge($inprogress_cond,$tagcond,$searchcond,$prioritycond);
      // Tickets Related Code 
      /*$oderby = ['deadline' => 'asc'];*/
      //pr($cond); die;
      $open = $this->Leads->find('all',[
            'conditions' => $open_cond,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();
      $inprogress = $this->Leads->find('all',[
            'conditions' => $inprogress_cond,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();
      $closed = $this->Leads->find('all',[
            'conditions' => $closed_cond,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();
      $expired =$this->Leads->find('all',[
            'conditions' => $expired_cond,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();
      $scheduled = $this->Leads->find('all',[
            'conditions' => $schedule_cont,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();
      $mandatory_count = $this->Leads->find('all',[
            'conditions' => $mandatory_cont,
            'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
            ])->contain(['Users'])->count();

      $this->set('open',$open);
      $this->set('inprogress',$inprogress);
      $this->set('closed',$closed);
      $this->set('scheduled',$scheduled);
      $this->set('mandatory_count',$mandatory_count);
    if(isset($this->request->data['pagetype']) &&  $this->request->data['pagetype'] == 'pre') {
      $this->request->data['pageno'] = $this->request->data['pageno'] - 2;
      /*why 2? because we will increase 1 at $next_page var*/
    }
    $tofetch = 20;
    $start_index = ($tofetch * $this->request->data['pageno']) - 20;
    $cond = array_merge($cond,$tagcond,$searchcond,$prioritycond);
    //pr($cond); die;
    $result = $this->Leads->find('all',[
      'conditions' => $cond,
      'order' => $oderby,
      'fields' => ['Leads.id','Leads.bidding_date','Leads.url_title','Leads.technology','Leads.salesfront_id','Leads.source','Leads.category','Leads.perspective_amount','Leads.status','Leads.time_spent','Users.id','Users.first_name','Users.last_name','Leads.created'],
      'offset' => $start_index,
      'limit' => 20,
      ])->contain(['Users'])->all()->toArray();
    $paginatedarr = $result;
    $arr = [];
    $tofetch = 20;

    //pr($cond); die;

    $resultcount = $this->Leads->find('all',[
      'conditions' => $cond,
      'fields' => ['Leads.id','Users.id','Users.first_name','Users.last_name']
      ])->contain(['Users'])->count();


    $totalpages = ceil($resultcount/20);
    $next_page = $this->request->data['pageno'] ;
    $projects = Cache::read("my_projects_$userSession[0]",'project');
    if ($projects !== false) {
      /*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/

        $my_projects = json_decode($projects);
    } else {
      $my_projects = $this->Common->getProjectsofuser($userID); 
       Cache::write("my_projects_$userSession[0]", json_encode($my_projects),'project');
    }
    
    $cachedusers = Cache::read("all_users_$userSession[0]",'short');
    if ($cachedusers !== false) {
      /*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
        $all_users = json_decode($cachedusers);
    } else {
      $all_users =  $this->Common->getuser_name2($session_info[3]);
       Cache::write("all_users_$userSession[0]", json_encode($all_users),'short');
    }


    foreach($result as $ticket){
      $ticket['idp'] = $ticket->id;
      $ticket['id'] = $this->Common->encForUrl($this->Common->ENC($ticket->id));
      $ticket['bidding_date'] =  date_create($ticket['deadline'])->format('d M Y');
      $ticket['created']  =  date_create($ticket['created'])->format('d M Y h:i A');
      $ticket['modified'] =  date_create($ticket['modified'])->format('d M Y h:i A');
      $arr[] = $ticket;
    }
    //pr($paginatedarr);die;
    $userroles = explode(',', $session_info[2]);
    //pr($userroles); die;
    $cachedentities = Cache::read("entities_$userSession[0]",'short');
    if ($cachedentities !== false) {
      /*IF WE FIND DATA STORED IN CACHES QUERIES WILL NOT BE REPEATED*/
        $entities = json_decode($cachedentities)[0];
        $tags = json_decode($cachedentities)[1];  
    } else {
      $entitiesids = [];
      //pr($entitiesids->toArray()); die;
      if(count($entitiesids) > 0){
        $entities =[];
      } else {
        $entities = [];
      }

        $finalentities = [];
        $alltagids = [];
        $ids;
        if(count($entities) > 0){
          foreach($entities as $entity){
            $exp =  explode(',', $entity['tag_id']);
            $alltagids = array_merge($alltagids,$exp);
          }
        }
        //pr($alltagids);
        if(count($alltagids) > 0){
          $tagids = [];
        } else {
          $tagids = [];
        }
        
        if(count($tagids) > 0){
          $tags = $this->Tags->find('all',[
            'conditions' => ['is_deleted' => 'No','enable_on_ticket' => 'Yes','id IN' => array_unique(array_values($tagids->toArray()) )]
          ]);   
        } else {
          $tags = [];
        }

       Cache::write("entities_$userSession[0]", json_encode([$entities,$tags]),'short');
    }

        $allmandatory = $this->Tickets->find('all',[
                             'conditions' => ['type' => 'Mandatory','to_id' => $session_info[0],'status IN' => [1,2]]
                         ])->all();
            $mandatory = [];
            foreach($allmandatory as $m){
                 if(count($mandatory) == 0){
                    if($m['last_replier'] !== intval($session_info[0])){
                            $mandatory = $m;
                        }      
                 }   
                
            }
    //pr($mandatory) ; die;
       if(count($mandatory) > 0){
          $mid = $this->Common->encForUrl($this->Common->ENC($mandatory->id));
          $mandatory['eid'] = $mid;
       }
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
      } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
        $ip_address = $_SERVER['REMOTE_ADDR'];
      }
      //$this->Tickets->getDatasource()->disconnect(); 
      //\Connection::disconnect(); 

    echo json_encode(['status'=> 'success',
            'deadline'=> $deadline,
            'userID' => $userID,
            'resultData'=> $paginatedarr,
            'open' => $open, 
            'inprogress' => $inprogress,
            'my_projects'=> $my_projects,
            'all_users' => $all_users,
            'closed'=> $closed,
              'expired' => $expired,
              'totalrecords' => $resultcount,
              'pageno' => $next_page,
              'totalpages' => ($totalpages == 0 ) ? 1: $totalpages ,
              'currentpage' => $this->request->data['pageno'],
              'currentrecodsfrom' => $start_index + 1,
              'tofetch' => $tofetch,
              'today' =>  date('Y-m-d 00:00:00'),
              'entities' => $entities,
              'tags' => $tags,
              'mandatory' => $mandatory,
              'mandatory_count' => $mandatory_count,
              'scheduled' => $scheduled,
              'ip_address' => $ip_address,
              'rls' => $currentuserroles
           ]);
    die;
    //$this->set('deadline',$deadline);
    //$this->set('userID',$userID);
    //$this->set('resultData',$result);
  }







	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : add	
		* @Purpose: This function is to add leads details .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/	
  function add() {  
    $this->viewBuilder()->layout('new_layout_admin');   
      $this->set('title_for_layout','Add Lead');
      $this->pageTitle = "Add Lead";
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
      $useragency = $userSession[3];
      $this->set('agency_id',$useragency);
      if($this->request->data){
        //pr($this->request->data); die;
        $bidding = date_create($this->request->data['bidding_date']);
        $bidding_date = $bidding->format('y-m-d');
        $next = date_create($this->request->data['nextfollow_date']);
        $nextfollow_date = $next->format('y-m-d');
        $this->request->data['perspective_amount']=(!empty($this->request->data['perspective_amount'])) ? $this->request->data['perspective_amount'] : $this->request->data['perspective_amount']='0.00' ;

        $this->request->data['bidding_date'] = $bidding_date;
        $this->request->data['nextfollow_date'] =  $nextfollow_date;

        $this->request->data['salesfront_id'] = $userSession[0];
        
        $res=$this->Leads->find('all',array('conditions'=>array('salesfront_id'=>$userSession[0],'url_title'=>$this->request->data['url_title'])));

        if(count($res->toArray())>0){
          $this->Flash->error("Lead for same URL exists.");
          $this->setAction('leadslist');

        }
        $this->request->data['agency_id'] =(int) $userSession[3];
        /*pr($this->request->data); die;*/
         /*var_dump($this->request->data['agency_id']); die;*/
        
        $leads = $this->Leads->newEntity($this->request->data());/*
        $projects = $this->Projects->newEntity($this->request->data);*/

        if(empty($leads->errors())){
          $this->Leads->save($leads);
          $this->Flash->success_new("Lead has been created successfully.");
          return $this->redirect( ['action' => 'leadslist']); 

        }else{ 

          $this->set("errors", $leads->errors());
        }


      }   
    }

    function quickadd(){
        $this->viewBuilder()->layout(false);
         $session = $this->request->session();
        $this->set('session',$session);
        $userSession = $session->read("SESSION_ADMIN");
        $useragency = $userSession[3];
        $this->set('agency_id',$useragency);
        if($this->request->data){
        $bidding = date_create($this->request->data['bidding_date']);
        $bidding_date = $bidding->format('y-m-d');
        $next = date_create($this->request->data['nextfollow_date']);
        $nextfollow_date = $next->format('y-m-d');
        $this->request->data['perspective_amount']=(!empty($this->request->data['perspective_amount'])) ? $this->request->data['perspective_amount'] : $this->request->data['perspective_amount']='0.00' ;

        $this->request->data['bidding_date'] = $bidding_date;
        $this->request->data['nextfollow_date'] =  $nextfollow_date;

        $this->request->data['salesfront_id'] = $userSession[0];
        
        $res=$this->Leads->find('all',array('conditions'=>array('salesfront_id'=>$userSession[0],'url_title'=>$this->request->data['url_title'])));

        if(count($res->toArray())>0){
          $this->Flash->error("Lead for same URL exists.");
          $this->setAction('leadslist');

        }
        $this->request->data['agency_id'] =(int) $userSession[3];
        $leads = $this->Leads->newEntity($this->request->data());
        if(empty($leads->errors())){
          $this->Leads->save($leads);
         // $this->Flash->success_new("Lead has been created successfully.");
         echo json_encode(['status' => 'success','id' => $this->Common->ENC($leads->id), 'name' => $leads->url_title]);
         die;
        }else{
          echo json_encode(['status' => 'Validationerrors', 'errors' => $leads->errors() ]);
         die;
        }


      }
    }

  	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : edit	
		* @Purpose: This function is to edit leads details .
		* @Param: $id	
		* @Return: none 	
		* @Return: none 
		**/	
		function edit($id = null){
      $id = (is_numeric($id ) ) ?  $id : $this->Common->DCR($this->Common->dcrForUrl($id) );
$this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Edit Lead');
			$session = $this->request->session();
			$this->set('session',$session);
			if($this->request->data){

				$userSession = $session->read("SESSION_ADMIN");

				$str = str_replace("/","-",$this->request->data['bidding_date']);

				$biddate = date("d-m-Y",strtotime($str));

				$strnxt = str_replace("/","-",$this->request->data['nextfollow_date']);

				$nextdate = date("d-m-Y",strtotime($strnxt));


				$lead = $this->Leads->get($this->request->data['id']);
				$leads = $this->Leads->patchEntity($lead,$this->request->data());
				if(empty($leads->errors())) {
					$this->Leads->save($lead);
					$this->Flash->success_new("Leads has been updated successfully.");
					$this->redirect(array('controller' => 'Leads', 'action' => 'leadslist'));  
				}else{                                    
					$this->set("errors", $leads->errors());
				} 
			}else if(!empty($id)){ 
				$PaymentsData = $this->Leads->get($id);
				$this->set('PaymentsData',$PaymentsData);
				$date= $PaymentsData['bidding_date']->format('Y-m-d');
				$datenxt= isset($PaymentsData['nextfollow_date']) ? $PaymentsData['nextfollow_date']->format('Y-m-d'): /*date_create('0000-00-00')->format('Y-m-d')*/'';
				$this->set('date',$date);
				$this->set('datenxt',$datenxt);


				$this->set('result',$PaymentsData->toArray());
				if(!$PaymentsData){             
					$this->redirect(array('action' => 'leadslist'));    

				}                                                      
			}
			$this->set("id", $id);                                                             
		}  


	  	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : delete	
		* @Purpose: This function is to delete leads details .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/	


function delete($id = null) {
      $id = $id;
      $this->viewBuilder()->layout('new_layout_admin'); 
     	$employee = $this->Leads->get($id);
   if ($this->Leads->delete($employee)) {
   	            $credentials = TableRegistry::get('Credentials');
				$update = ['user_id' => 0 , 'status' => 0];
				$credentials->updateAll($update,['user_id IN' => $id]);
                echo json_encode(['status'=> 'success']); die;
             }else { 
        echo json_encode(['status'=> 'failed']); die;
      } 
       }




	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : activelead	
		* @Purpose: This function is to list active leads details .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/

		function activelead(){
			$this->viewBuilder()->layout(false);
			$this->set('title_for_layout','Active Lead');
			$this->pageTitle = "Active Lead";			
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
			$activeLeads = $this->Leads->find('all',[
				'conditions' => ['Leads.status' => 'active','Leads.agency_id' => $userSession[3]],
				'order' => ['Leads.modified' => 'desc']
				])->contain('Users')->all();

			if($this->request->is('ajax')){ 
				
				$status=$this->Common->sanitizeInput($this->request->data['status']);
				$result = 	$this->Leads->updateAll(array('status'=>$status),array('id'  => $this->request->data['id']));
				
				
				if($result){
					echo "1"; die;
				}else{  
					echo "0"; die;
				}	
			}
			$this->set('resultData',$activeLeads->toArray());		
		}
		#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : admin_holdlead	
		* @Purpose: This function is to list active leads details .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
		function admin_holdlead(){
			$this->layout = false;
			$this->Lead->expects(array("User"));
			$closedLeads = $this->Lead->find('all',array(
				"fields" => array('Lead.*','User.first_name','User.last_name'),
				"conditions" => array("Lead.status" =>"hold"),
				"order" => array('Lead.modified' => 'desc'),
				));
			if($this->RequestHandler->isAjax()==true){ 
				
				$this->request->data['status'] = "'".Sanitize::escape($this->request->query['form']['status'])."'"; 
				$result = 	$this->Lead->updateAll($this->request->data,array('Lead.id '  => Sanitize::escape($this->request->query['form']['id'])));
				
				if($result){
					echo "1"; die;
				}else{  
					echo "0"; die;
				}	
			}
			$this->set('resultData',$closedLeads);
		}
	#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : adminLeadreport
		* @Purpose: This function is to list lead reports per users .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
		function leadreport(){
        $this->viewBuilder()->layout('new_layout_admin');
			$this->set('title_for_layout','Leads Report');           

			$this->set("search1", "");                      
			$this->set("search2", "");                                 
			$criteria = "1"; //All Searching
			$session = $this->request->session();
			$this->set('session',$session);
			$session->delete('SESSION_SEARCH');
      $userSession = $session->read("SESSION_ADMIN");
			if(isset($this->request->data)) {

				$search2 = ['salesfront_id !=' => 0,'Leads.agency_id' => $userSession[3]];

				if(!empty($this->request->data['status'])){
					
					$cond = ['Leads.status Like' => '%'.$this->Common->sanitizeInput($this->request->data['status']).'%'];
					$search2 = array_merge($search2, $cond);
				}
				if(!empty($this->request->data['url'])){
					
					$cond = ['Leads.url_title LIKE' => '%'.$this->Common->sanitizeInput($this->request->data['url']).'%'];
					$search2 = array_merge($search2, $cond);
				}
				if(!empty($this->request->data['user_id'])){
					

					$cond = ['Leads.salesfront_id' => $this->Common->sanitizeInput($this->request->data['user_id'])];
					$search2 = array_merge($search2, $cond);	
				}
				if(!empty($this->request->data['notes'])){        
					
					$cond = ['Leads.notes LIKE' => '%'.$this->Common->sanitizeInput($this->request->data['notes']).'%'];
					$search2 = array_merge($search2, $cond);	
				}
				if( !empty($this->request->data['sdate']) &&  !empty($this->request->data['edate']) ){

					$create=date_create($this->request->data['sdate']);

					$to = $create->format('Y-m-d');

					$create=date_create($this->request->data['edate']);

					$from = $create->format('Y-m-d');

					$cond = ['Leads.bidding_date >=' => $to, 'Leads.bidding_date <=' => $from];
					$search2 = array_merge($search2, $cond);
					
				}
				if(!empty($this->request->data['start_amount']) &&  !empty($this->request->data['last_amount'])){
					$cond = ['Leads.perspective_amount >=' => $this->Common->sanitizeInput($this->request->data['start_amount']), 'Leads.perspective_amount <=' => $this->Common->sanitizeInput($this->request->data['last_amount'])];
					$search2 = array_merge($search2, $cond);

					
				}
				$this->set("search2",$search2);      
			}	

			if((!empty($search2))){

				$query  =    $this->Leads->find("all" , [
					'conditions' => $search2,
					])->contain(['Users']);
				$result = isset($query)? $query:array();

				$criteria = $search2; 		
				$session->write('SESSION_SEARCH', $criteria);	
			}else{
				$query  =  $this->Leads->find("all",[
            'conditions' => ['Leads.agency_id' => $userSession[3]]
          ])->contain(['Users']);
				$this->set("search2","");	
			}	
			$urlString = "/";          
			if(isset($this->request->query)){ 
				$completeUrl  = array();
				if(!empty($this->request->query['page']))      
					$completeUrl['page'] = $this->request->query['page'];

				if(!empty($this->request->query['sort']))                  
					$completeUrl['sort'] = $this->request->query['sort'];

				if(!empty($this->request->query['direction']))             
					$completeUrl['direction'] = $this->request->query['direction'];

				foreach($completeUrl as $key=>$value){                      
					$urlString.= $key.":".$value."/";                           
				}
			}

			$this->set('urlString',$urlString); 
			if(!empty($setValue)){                            
				if(isset($this->request->query['IDs'])){          
					$saveString = implode("','",$this->request->query['form']['IDs']);      
				}

				if($saveString != ""){                                          
					$this->Leads->updateAll($setValue,"Leads.id in ('".$saveString."')");                                                                 
					$session->setFlash($messageStr, 'layout_success');                          
				}

			}

			$this->set('urlString', $urlString);

			$data = $this->paginate($query,[
				'page' => 1, 'limit' => 100,
				'order' => ['Leads.id' => 'desc'],
				'paramType' => 'querystring'
				]);

			$this->set('resultData', $data);



		}
		/**                                                           
    * @Date: Nov,2016                                        
    * @Method : quickedit
    * @Purpose: This function is called quickedit function for edit records.
    * @Param: none                                                
    * @Return: none                                               
    **/

		function quickedit($id = null){

			$this->set('title_for_layout','Edit Lead');     
			$this->pageTitle = "Edit Lead";    
			$this->set("pageTitle","Edit Lead");   
			$this->viewBuilder()->layout(false);
			if($this->request->data){ 

				$lead = $this->Leads->get($this->request->data['id']);

				$leads = $this->Leads->patchEntity($lead,$this->request->data());
				if(empty($leads->errors())) {
					$this->Leads->save($lead);
					$this->Flash->success_new("Lead has been updated successfully.");

					echo 1;
					die;
				}else{                                    
					$this->set("errors", $leads->errors());
				} 
			}else if(!empty($id)){ 
				$leadData = $this->Leads->get($id);

				$this->set('leadData',$leadData); 
				if(!$leadData){             
					$this->return->redirect(array('action' => 'adminLeadreport'));                 
				}                                                      
			}                                                          
		} 

		#_________________________________________________________________________#
		/**	
		* @Date: Nov,2016
		* @Method : leadreport1
		* @Purpose: This function is to list lead reports per users .
		* @Param: none	
		* @Return: none 	
		* @Return: none 
		**/
		function leadreport1(){

			$this->viewBuilder()->layout(false);
      $session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");

			if(isset($this->request->data['user_id'])){ 

				$selectedUser=$this->request->data['user_id'];

				$start_date=$this->request->data['start_date'];

				$end_date=$this->request->data['end_date'];

				$dateRange=$this->request->data['select'];

				$cur=date('Y-m-d');


			}else{

				$selectedUser="";

				$start_date="";

				$end_date="";

				$dateRange="";

				$cur="";
			}
			$this->set('fromDate',$start_date);
			$this->set('toDate',$end_date);
			$this->loadComponent('Common');

			$options = $this->Common->getDateRange();

			$this->set('options', $options);
			if(empty($selectedUser) && empty($start_date)&&empty ($end_date))
			{

				$query=TableRegistry::get('Leads');
				$data=$query->find();
				$data->select(['Users.first_name','Users.last_name','NoOfleads' => $data->func()->count('Leads.id')])  ->group('Leads.salesfront_id')
				->where(['Leads.bidding_date'=>$cur ,'Leads.agency_id' => $userSession[3]]) 

				->join([
					'table' => 'users',
					'alias' => 'Users',
					'type'=>'INNER',
					'foreignKey' => false,
					'conditions' => array ('Leads.salesfront_id = Users.id')
					]);
			}
			elseif(empty($selectedUser) && !empty($start_date)&& !empty($end_date))
			{	

				$query=TableRegistry::get('Leads');
				$data=$query->find();
				$data->select(['Users.first_name','Users.last_name','NoOfleads' => $data->func()->count('Leads.id')])  ->group('Users.id')
				->where(['Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date ,'Leads.agency_id' => $userSession[3]])
				->join([
					'table' => 'users',
					'alias' => 'Users',
					'type'=>'INNER',
					'foreignKey' => false,
					'conditions' => array ('Leads.salesfront_id = Users.id')
					]);

			}
			elseif(!empty($selectedUser) && $dateRange!='select')
			{	

				$query=TableRegistry::get('Leads');
				$data=$query->find();
				$data->select(['Users.first_name','Users.last_name','NoOfleads' => $data->func()->count('Leads.id')])->group('Users.first_name')
				->where(['Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,'Users.id'=>$selectedUser,'Leads.agency_id' => $userSession[3]]) 

				->join([
					'table' => 'users',
					'alias' => 'Users',
					'type'=>'INNER',
					'foreignKey' => false,
					'conditions' => array ('Leads.salesfront_id = Users.id')
					]);

			}
			elseif(!empty($selectedUser) && $start_date==$cur)
			{


				$query=TableRegistry::get('Leads');
				$data=$query->find();
				$data->select(['Users.first_name','Users.last_name','NoOfleads' => $data->func()->count('Leads.id')])->group('Users.first_name')
				->where(['Users.id'=>$selectedUser,'Leads.agency_id' => $userSession[3]]) 

				->join([
					'table' => 'users',
					'alias' => 'Users',
					'type'=>'INNER',
					'foreignKey' => false,
					'conditions' => array ('Leads.salesfront_id = Users.id')
					]);

			}

			else
			{

				$query=TableRegistry::get('Leads');
				$data=$query->find();
				$data->select(['Users.first_name','Users.last_name','NoOfleads' => $data->func()->count('Leads.id')])->group('Users.first_name')
				->where(['Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,'Users.id'=>$selectedUser,'Leads.agency_id' => $userSession[3]]) 
				->join([
					'table' => 'users',
					'alias' => 'Users',
					'type'=>'INNER',
					'foreignKey' => false,
					'conditions' => array ('Leads.salesfront_id = Users.id')
					]);

			}

			$this->set('resultData',$data->toArray()); 
		}
#_________________________________________________________________________#  
	/**                       
	* @Date: Nov,2016      
	* @Method : bidspiechartreport      
    * @Purpose: This function is to show the Pie Chart of no of Bids by Users.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	
	function bidspiechartreport(){


		$this->viewBuilder()->layout(false);
    $session = $this->request->session();
    $this->set('session',$session);
    $userSession = $session->read("SESSION_ADMIN");
		if(isset($this->request->data['user_id'])){ 

			$userselect=$this->request->data['user_id'];

			$start_date=$this->request->data['start_date'];

			$end_date=$this->request->data['end_date'];

			$dateRange=$this->request->data['select'];

			$cur=date('Y-m-d');


		}else{

			$userselect="";

			$start_date="";

			$end_date="";

			$dateRange="";

			$cur="";
		}
		$this->set('fromDate',$start_date);
		$this->set('toDate',$end_date);
		$this->loadComponent('Common');
		$options = $this->Common->getDateRange();
		$this->set('options', $options);
		$cond = array('Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date);

		if(empty($userselect)&&empty($start_date)&&empty ($end_date)){
			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				'Leads.bidding_date'=>$cur,'Leads.agency_id' => $userSession[3]
				])
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);

		}elseif(empty($userselect)&&!empty($start_date)&&!empty ($end_date))
		{
			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,'Leads.agency_id' => $userSession[3]
				])
			->group('Users.first_name')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);


		} elseif(!empty($userselect)) {

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,
				'Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3]
				])

			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);


		}
		elseif(!empty($userselect) && $start_date==$cur)
		{
			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				'Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3]
				])
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);

		}
		elseif(empty($userselect) &&  $start_date==$yesterday)
		{
			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				'Leads.bidding_date'=>$yesterday,'Leads.agency_id' => $userSession[3]
				])
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);

		}
		elseif(!empty($userselect) &&  $start_date==$yesterday)
		{


			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where(['Leads.bidding_date'=>$yesterday,'Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3]]) 
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);

		}
		else{
			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([
				$cond,
				'Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3]
				])
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				]);

		}


		$this->set('resultData', $resultData->toArray());
		
	}
	#_________________________________________________________________________#  
 /**                       
 * @Date: Nov,2016       
 * @Method : activeleadreport      
    * @Purpose: This function is to show the Active Lead Reports.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  

 function activeleadreport(){

 	$this->viewBuilder()->layout(false);
 	$this->viewBuilder()->layout = "layout_graph";
 	$userselect=$this->data['Lead']['user_id'];
 	$start_date=$this->data['Lead']['start_date'];
 	$end_date=$this->data['Lead']['end_date'];
 	$leadselect=$this->data['Lead']['select'];
 	$this->set('fromDate',$start_date);
 	$this->set('toDate',$end_date);
 	$cur=date("Y-m-d");
 	$current=date("Y-m-d");
 	$current = strtotime($current);
 	$current = strtotime("-1 week", $current);
 	$setdate=date('Y-m-d', $current);
 	$cond1 = array('Lead.bidding_date between ? and ?' => array($start_date, $end_date));
 	$cond2 = array('Lead.bidding_date between ? and ?' => array($setdate, $cur));
 	$cond = array('Lead.status'=>'active');
 	if(empty($userselect)&&empty($start_date)&&empty($end_date))
 	{
 		$resultData = $this->Leads->find('all',[
 			'conditions' => ['Leads.salesfront_id = Users.id'],[$cond2,$cond]
 			])
 		->contain(['Users'])->toArray();

 	}
 	elseif(empty($userselect)&&!empty($start_date)&&!empty ($end_date))
 	{
 		$resultData = $this->Leads->find('all',[
 			'conditions' => ['Leads.salesfront_id = Users.id'],[$cond,$cond1]
 			])
 		->contain(['Users'])->toArray();

 	}
 	elseif(!empty($userselect) && $leadselect!='select')
 	{
 		$resultData = $this->Leads->find('all',[
 			'conditions' => ['Leads.salesfront_id = Users.id'],['Lead.salesfront_id'=>$userselect,$cond,$cond1]
 			])
 		->contain(['Users'])->toArray();

 	}
 	elseif(!empty($userselect) && $start_date==$cur)
 	{
 		$resultData = $this->Leads->find('all',[
 			'conditions' => ['Leads.salesfront_id = Users.id'],['Lead.salesfront_id'=>$userselect,$cond]
 			])
 		->contain(['Users'])->toArray();

 	}
 	else{   
 		$resultData = $this->Leads->find('all',[
 			'conditions' => ['Leads.salesfront_id = Users.id'],['Lead.salesfront_id'=>$userselect,$cond,$cond1]
 			])
 		->contain(['Users'])->toArray();

 	}
 	$this->set('resultData', $resultData);
 }

#_________________________________________________________________________#  
 /**                       
 * @Date: Nov,2016       
 * @Method : winclosereport      
    * @Purpose: This function is to show the Reports of Win/Closed Project.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  
 
 function winclosereport(){

 	$this->viewBuilder()->layout(false);
  $session = $this->request->session();
  $this->set('session',$session);
  $userSession = $session->read("SESSION_ADMIN");

 	if(isset($this->request->data['user_id'])){ 

 		$userselect=$this->request->data['user_id'];

 		$start_date=$this->request->data['start_date'];

 		$end_date=$this->request->data['end_date'];

 		$dateRange=$this->request->data['select'];

 		$cur=date('Y-m-d');


 	}else{

 		$userselect="";

 		$start_date="";

 		$end_date="";

 		$dateRange="";

 		$cur="";
 	}
 	$this->set('fromDate',$start_date);
 	$this->set('toDate',$end_date);
 	$this->loadComponent('Common');
 	$options = $this->Common->getDateRange();
 	$this->set('options', $options);
 	$cur=date('Y/m/d');
 	$current=date("Y-m-d");
 	$current = strtotime($current);
 	$current = strtotime("-1 week", $current);
 	$setdate=date('Y-m-d', $current);

 	$cond1 = array('Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date);
 	$cond2 = array('Leads.bidding_date >= '=> $setdate,'Leads.bidding_date <='=>$cur);

 	$cond = array('Leads.status'=>'success');
 	if(empty($userselect)&&empty($start_date)&&empty($end_date))
 	{

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->where([$cond2,$cond,'Leads.agency_id' => $userSession[3]]) 
 		->group('Users.first_name')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);



 	}
 	elseif(empty($userselect)&&!empty($start_date)&&!empty($end_date))
 	{

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->where([$cond,$cond1,'Leads.agency_id' => $userSession[3]]) 
 		->group('Users.first_name')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);

 	}
 	elseif(!empty($userselect) &&$leadselect!='select')
 	{

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->where(['Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3],$cond,$cond1])

 		->group('Users.first_name')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);


 	}
 	elseif(!empty($userselect) &&$start_date==$cur)
 	{

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->where(['Leads.salesfront_id'=>$userselect,'Leads.agency_id' => $userSession[3]],$cond)

 		->group('Users.first_name')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);
 	}
 	else{   

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->where(['Leads.salesfront_id'=>$userselect,$cond,$cond1,'Leads.agency_id' => $userSession[3]]) 
 		->group('Leads.status')
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);
 	}
 	$this->set('resultData', $resultData->toArray());

 }
#_________________________________________________________________________#  
 /**                       
 * @Date: Nov,2016    
 * @Method : overleadcountuserreport    
    * @Purpose: This function is to show the Active Lead Reports.   
 * @Param: none                                                       
 * @Return: none                                                  
 **/  

 function overleadcountuserreport(){ 
 	$this->viewBuilder()->layout(false);
  $session = $this->request->session();
  $this->set('session',$session);
  $userSession = $session->read("SESSION_ADMIN");
 	if(isset($this->request->data) && count($this->request->data) > 0){ 
   /* pr($this->request->data); die;*/
 		$userselect=$this->request->data['user_id'];
 		$start_date=$this->request->data['start_date'];
 		$end_date=$this->request->data['end_date'];
 		$dateRange=$this->request->data['select'];
 		$cur=date('Y-m-d');


 	}else{

 		$userselect="";

 		$start_date="";

 		$end_date="";

 		$dateRange="";

 		$cur="";
 	}
 	$this->set('fromDate',$start_date);
 	$this->set('toDate',$end_date);
 	$this->loadComponent('Common');

 	$options = $this->Common->getDateRange();

 	$this->set('options', $options);


 	$cur=date('Y/m/d');
 	$current=date("Y-m-d");
 	$current = strtotime($current);
 	$current = strtotime("-1 week", $current);
 	$setdate=date('Y-m-d', $current);

 	$cond1 = array('Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date);
 	$cond = array('Leads.status'=>array('active','closed','cold','deny','hold','success'));
 	$cur=date("Y/m/d");

 	if(empty($userselect)&&empty($start_date)&&empty($end_date))
 	{


 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
 			'status'=>'Leads.status',

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->group('Leads.status')
 		->where([
 			'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,
 			'Leads.status IN'=>['active','closed','cold','deny','hold','success'],'Leads.agency_id' => $userSession[3]
 			])
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);

 	}
 	elseif(empty($userselect) && !empty($start_date)&& !empty($end_date))
 	{  


 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
 			'status'=>'Leads.status',

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->group('Leads.salesfront_id','Leads.status')
 		->where([
 			'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,
 			'Leads.status IN'=>['active','closed','cold','deny','hold','success'],'Leads.agency_id' => $userSession[3]
 			])

 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);


 	}
 	elseif(!empty($userselect) && $dateRange!='select')
 	{


 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
 			'status'=>'Leads.status',
 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->group('Leads.status')
 		->where([
 			'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,'Leads.salesfront_id'=>$userselect,
 			'Leads.status IN'=>['active','closed','cold','deny','hold','success'],'Leads.agency_id' => $userSession[3]
 			])
 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);

 	}
 	elseif(!empty($userselect) && $start_date==$cur)
 	{

 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
 			'status'=>'Leads.status',

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->group('Leads.status')
 		->where([
 			'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,
 			'Leads.status IN'=>['active','closed','cold','deny','hold','success'],'Leads.agency_id' => $userSession[3]
 			])

 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);

 	}
 	else{


 		$credentials= TableRegistry::get('Leads');
 		$resultData = $credentials->find();
 		$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
 			'status'=>'Leads.status',

 			'first_name'=>'Users.first_name',
 			'last_name'=>'Users.last_name']) 
 		->group('Leads.status')
 		->where([
 			'Leads.bidding_date >= '=> $start_date,'Leads.bidding_date <='=>$end_date,'Leads.salesfront_id'=>$userselect,
 			'Leads.status IN'=>['active','closed','cold','deny','hold','success'],'Leads.agency_id' => $userSession[3]
 			])

 		->join([
 			'table' => 'users',
 			'alias' => 'Users',
 			'type'=>'INNER',
 			'foreignKey' => false,
 			'conditions' => array ('Leads.salesfront_id = Users.id')
 			]);


 	}

 	$this->set('resultData', $resultData->toArray());

 }

/**                       
	* @Date: 03-Sep-2014       
	* @Method : companywideleadreport    
    * @Purpose: This function is to show the Company Wide Lead Reports.   
	* @Param: none                                                       
	* @Return: none                                                  
	**/  
	

	function companywideleadreport(){
		$this->viewBuilder()->layout(false);
     $session = $this->request->session();
  $this->set('session',$session);
  $userSession = $session->read("SESSION_ADMIN");
		if(isset($this->request->data['user_id'])){ 

			$selectedUser=$this->request->data['user_id'];

			$start_month=$this->request->data['month1'];

			$end_month=$this->request->data['month2'];
			$start_year=$this->request->data['year1'];

			$end_year=$this->request->data['year2'];

			$cur=date('Y-m-d');


		}else{

			$selectedUser="";

			$start_month="";

			$end_month="";
			$start_year="";

			$end_year="";



			$cur="";
		}

		if (empty($start_month)) 
		{
			$month = date('m');

		}
		else{
			$month=$start_month;
		}

		if (empty($start_year)) 
		{
			$year = date('Y');

		}
		else{
			$year=$start_year;
		}

		$result=$year."-".$month."-"."01";

		if (empty($end_month)) 
		{
			$month2 = date('m');

		}
		else{
			$month2=$end_month;
		}

		if (empty($end_year)) 
		{
			$year2 = date('Y');

		}
		else{

			$year2=$end_year;
		}

		$this->set('start_month', $start_month);
		$this->set('end_month', $end_month);
		$this->set('start_year', $start_year);
		$this->set('end_year', $end_year);

		$result2=$year2."/".$month2."/"."01"." 00:00:00";
		$result2 =  date('Y-m-d', strtotime('+1 month -1 second', strtotime($result2)));

		if (!empty($start_month)&& !empty($end_month)&&empty($start_year)&& empty($end_year)) 
		{
			$year = date('Y');
			$start_month_range=$year."-".$start_month."-"."01";
			$end_month_range=$year."/".$month2."/"."01"." 00:00:00";
			$end_month_range =  date('Y-m-d', strtotime('+1 month -1 second', strtotime($end_month_range)));

		}
		if (empty($start_month)&& empty($end_month)&&!empty($start_year)&& !empty($end_year)) 
		{

			$start_year_range=$start_year."-"."01"."-"."01";
			$end_year_range=$end_year."/"."12"."/"."01"." 00:00:00";
			$end_year_range =  date('Y-m-d', strtotime('+1 month -1 second', strtotime($end_year_range)));
			
		}

		$cond1 = array('Leads.bidding_date >= '=> $result,'Leads.bidding_date <='=>$result2);

		$month_cond = array('Leads.bidding_date >= '=> $start_month,'Leads.bidding_date <='=>$end_month);


		$year_cond = array('Leads.bidding_date >= '=> $start_year,'Leads.bidding_date <='=>$end_year);


		if(empty($selectedUser) && empty($start_month)&&empty ($start_year)  && empty($end_month)&&empty ($end_year))
		{


			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([$cond1,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status')

			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();


		}
		elseif(empty($selectedUser) && !empty($start_month)&&!empty ($start_year) && !empty($end_month)&&!empty ($end_year))
		{


			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([$cond1,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();


		}
		elseif(empty($selectedUser) && !empty($start_month)&&!empty ($end_month))
		{


			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([$month_cond,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();
		}
		elseif(empty($selectedUser) && !empty($start_year)&&!empty ($end_year))
		{

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where([$year_cond,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();


		}
		elseif(!empty($selectedUser) && empty($start_month)&&empty ($start_year) && empty($end_month)&& empty ($end_year))
		{

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name'])  
			->where(['Leads.salesfront_id'=>$selectedUser,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();


		}
		elseif(!empty($selectedUser) && !empty($start_year)&&!empty ($end_year))
		{	

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where(['Leads.salesfront_id'=>$selectedUser,$year_cond,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();


		}
		elseif(!empty($selectedUser) && !empty($start_month)&&!empty ($end_month))
		{

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where(['Leads.salesfront_id'=>$selectedUser,$month_cond,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();

		}
		else if(!empty($selectedUser) && !empty($start_month)&&!empty ($start_year) && !empty($end_month)&&!empty($end_year))
		{  

			$credentials= TableRegistry::get('Leads');
			$resultData = $credentials->find();
			$resultData->select(['NoOfLeads' => $resultData->func()->count('Leads.id'),
				'status'=>'Leads.status',
				'bidding_date'=>'Leads.bidding_date',
				'first_name'=>'Users.first_name',
				'last_name'=>'Users.last_name']) 
			->where(['Leads.salesfront_id'=>$selectedUser,$cond1,'Leads.agency_id' => $userSession[3]]) 
			->group('Leads.status','MONTH(bidding_date)')
			->join([
				'table' => 'users',
				'alias' => 'Users',
				'type'=>'INNER',
				'foreignKey' => false,
				'conditions' => array ('Leads.salesfront_id = Users.id')
				])->toArray();

		}
		
		$this->set('resultData', $resultData);

	}
}	