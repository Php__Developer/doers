<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\getDataSource;
/*
This Component is used for creating common function which could be reuse by other controllers

*/
define('ENCRYPTION_KEY', 'aaaaa7997b6d5fcd55f4b5c32611b87cd923e88837b63bf2941ef819dc8ca282');

class CommonComponent extends Component { //cake extends Object for creating component
	/**  @Date: 21-Aug-2010
    	*@Method : licenceCode (This 
    	*@Purpose: This function is used to generate licence code.
    **/
	function randomCode($plength='8'){

		$code="";
		$chars = 'ABCDEFGHJKLMNPQRTUVWXY346789ABCDEFGHJKLMNPQRTUVWXY346789';//string by which new code will be generated
		mt_srand(microtime() * 1000000);
		for($i = 0; $i < $plength; $i++) {
			$key = mt_rand(0,strlen($chars)-1);
			$code = $code . $chars{$key};
		}
		$code=trim($code);
		return $code ;

	}




	/** @Date: 22-Aug-2010
    	*@Method : getUserDropdown
    	*@Purpose: This function is used to generate licence code.
    **/

	function getUserDropdown(){
	
		App::import('Model','User');
		$this->User = new User();
		$this->User->unbindModel(array('hasMany' => array('Licence','Order')));
		$result = $this->User->find('all', array( 
			'fields'=>array('id','status','username'),
			'order'=>'username ASC',
			'conditions' => "User.id != '1'"
		));
		$return = array(''=>'Select User');
		foreach($result as $row){
			$return[$row['User']['id']] = ucwords($row['User']['username'])." (".(($row['User']['status'] == '1')?'Active':'Inactive').")";
		}
		return $return;
	}









	/** @Date: 08-OCT-2015
    	*@Method : encrypt_decrypt
    	*@Purpose: This function is used to generate Encrption and Decryption.
    **/
	   // Encrypt Function
	   
    function mc_encrypt($encrypt, $key){
    $encrypt = serialize($encrypt);
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
    $key = pack('H*', $key);
    $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
    $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
    $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
    $encoded = str_replace('/', 'karan', $encoded);
	$encoded = str_replace('+', 'simer', $encoded);
	$encoded = str_replace('|', 'doad', $encoded);
    return $encoded;
}
// Decrypt Function
function mc_decrypt($decrypt, $key){
	$decrypt = str_replace('simer', '+', $decrypt);
	$decrypt = str_replace('karan', '/', $decrypt);
	$decrypt = str_replace('doad', '|', $decrypt);
    $decrypt = explode('|', $decrypt.'|');
    $decoded = base64_decode($decrypt[0]);
    $iv = base64_decode($decrypt[1]);
    if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
    $key = pack('H*', $key);
    $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
    $mac = substr($decrypted, -64);
    $decrypted = substr($decrypted, 0, -64);
    $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
    if($calcmac!==$mac){ return false; }
    $decrypted = unserialize($decrypted);
    return $decrypted;
}

	/** @Date: 22-Aug-2010
    	*@Method : getUserDropdown
    	*@Purpose: This function is used to generate licence code.
    **/

	function getAdminRoleID(){
	
		App::import('Model','Role');
		$this->Role = new Role();
		$result = $this->Role->find('firsr', array( 
			'fields'=>array('id'),
			'conditions' => "role = 'Admin'"
		));
		return $result['Role']['id'];
	}
	
    /** @Date: 5-Feb-2010
    	*@Method : sendHtmlEmail
    	*@Purpose: This function is used to send email(template) using cakephp email component.
    **/
	function sendHtmlEmail($to = null,$subject = null,$bcc = array(),$template = null,$emailInfo = array())	{

		App::import('Component', 'Email'); // Import Email component
		$this->Email = new EmailComponent();
		$this->Email->to       = $to;
		$this->Email->bcc      = $bcc;
		$this->Email->subject  = $subject;
		$this->Email->replyTo  = "support@VLLsoftware.com";
		$this->Email->from     = "support@VLLsoftware.com";
		$this->Email->template = $template;
		$this->Email->sendAs   = 'html';
		$this->Email->xMailer  = "";
		if ($this->Email->send()) {
			return true;
		}else{
			return false;
		}
	}
    /**

    	* @Date: 11-Nov-2009
    	*@Method : changeDateFormat
    	*@Purpose:Gets Details of a Date Span . Called Via AJAX.
    **/

	function changeDateFormat($date = "", $format_in = "", $format_to = ""){

      $tmp_date = explode("-",$date);
      switch($format_in){
	  case "Y-m-d":
	  $t_stmp = mktime(0,0,0,$tmp_date[1],$tmp_date[2],$tmp_date[0]);
	  case "m-d-Y":
	  $t_stmp = mktime(0,0,0,$tmp_date[0],$tmp_date[1],$tmp_date[2]);
      }
      return date($format_to,$t_stmp);
         }

   /**

    * @Date: 18-Nov-2009
    *@Method : getSortLabel
    *@Purpose: Used for sorting purpose
   **/

  function getSortLabel($orderBy = "",$params = ""){

	if($orderBy!="" && is_array($params) && count($params)>0){

		if(isset($params['sort']) && $params['sort']!="" && $orderBy==$params['sort']){

			if(isset($params['direction'])){

				if($params['direction'] == "asc"){
					return "&uarr;";
				}else{
					return "&darr;";
				}
			}

		}elseif($orderBy == "Account.first_name"){
			return "&darr;";
		}

	}elseif($orderBy == "Account.first_name" && count($params)==0){
		return "&darr;";
	}
     }
 
 
  /**
    * @Date: 1-Dec-2009
    *@Method : getRandomNumber
    *@Purpose: Generates a random number
  **/

    function getRandomNumber(){

      srand ((double) microtime( )*1000000);
      $random_number = rand();
      return $random_number;
  }

   /**
    * @Date: 16-Dec-2009
    *@Method : getMonthsArray
    *@Purpose: Get an array of Months
  **/

  function getMonthsArray(){

    return array(
	""=>"-Select-",
	"01" => "January",
	"02" => "February",
	"03" => "March",
	"04" => "April",
	"05" => "May",
	"06" => "June",
	"07" => "July",
	"08" => "August",
	"09" => "September",
	"10" => "October",
	"11" => "November",
	"12" => "December"
		);
  }
  /**
    * @Date: 22-Sep-2014
    *@Method : getDateRange
    *@Purpose: Get an array of Months
  **/

  function selectforbidreport(){

    return array(
	
	'today'=>'Today',
	'yesterday'=>'Yesterday',
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }
  /**
    * @Date: 22-Sep-2014
    *@Method : getDateRange
    *@Purpose: Get an array of Months
  **/

  function getDateRange(){

    return array(
	
	'today'=>'Today',
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }
  /**
    * @Date: 22-Sep-2014
    *@Method : getMonthRange
    *@Purpose: Get an array of Months
  **/

  function getMonthRange(){

    return array(
	
	'month'=>'Month',
	'year'=>'Year',
	'today'=>'Today',	
	'week'=>'Week',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }
/**
    * @Date: 10-oct-2014
    *@Method : getWeekRange
    *@Purpose: Get an array of Months
  **/

  function getWeekRange(){

    return array(
	
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'today'=>'Today',	
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }

  
   /**
	 * @Date: 16-Dec-2009
	 *@Method : getDaysArray
	 *@Purpose: Get an array of days
   **/

   function getDaysArray(){
   
      $i 		= 1;
      $array 	= array();
      while($i<=31){
      $array[$i] = $i;
	  $i++;
      }
      return $array;
   }
   /**
    * @Date: 16-Dec-2009
    *@Method : getDaysArray
    *@Purpose: Get an array of years
   **/

   function getYearsArray(){

      $i 	= date("Y")-100;
      $array 	= array();
      while($i<=date("Y")){
	  $array[$i] = $i;
	  $i++;
      }
      return $array;
   }
   
   /**
    * @Date: 29-April-2012
    *@Method : getDaysArray
    *@Purpose: Get an array of years
   **/

   function getCustomYearsArray(){

      $i 	= 2011;
      $array 	= array();
      while($i<=date("Y")){
	  $array[$i] = $i;
	  $i++;
      }
      return $array;
   }
   
   /** @Date: 11-Jan-2009
    *@Method : getRandomString
    *@Purpose: generates random number.
  **/
	function getRandomString($length)

	{
		if($length>0) 
		{ 
		$rand_id="";
		for($i=1; $i<=$length; $i++)
		{
		mt_srand((double)microtime() * 1000000);
		$num = mt_rand(1,36);
		$rand_id .= $this->assign_rand_value($num);
		}
		}
		return $rand_id;
	}

   /** @Date: 11-Jan-2009
    *@Method : assign_rand_value
    *@Purpose: generates random number. This function is used by getRandomString function.
  **/

      function assign_rand_value($num)
	{
	// accepts 1 - 36
	switch($num)
	{
	case "1":
	$rand_value = "a";
	break;
	case "2":
	$rand_value = "b";
	break;
	case "3":
	$rand_value = "c";
	break;
	case "4":
	$rand_value = "d";
	break;
	case "5":
	$rand_value = "e";
	break;
	case "6":
	$rand_value = "f";
	break;
	case "7":
	$rand_value = "g";
	break;
	case "8":
	$rand_value = "h";
	break;
	case "9":
	$rand_value = "i";
	break;
	case "10":
	$rand_value = "j";
	break;
	case "11":
	$rand_value = "k";
	break;
	case "12":
	$rand_value = "l";
	break;
	case "13":
	$rand_value = "m";
	break;
	case "14":
	$rand_value = "n";
	break;
	case "15":
	$rand_value = "o";
	break;
	case "16":
	$rand_value = "p";
	break;
	case "17":
	$rand_value = "q";
	break;
	case "18":
	$rand_value = "r";
	break;
	case "19":
	$rand_value = "s";
	break;
	case "20":
	$rand_value = "t";
	break;
	case "21":
	$rand_value = "u";
	break;
	case "22":
	$rand_value = "v";
	break;
	case "23":
	$rand_value = "w";
	break;
	case "24":
	$rand_value = "x";
	break;
	case "25":
	$rand_value = "y";
	break;
	case "26":
	$rand_value = "z";
	break;
	case "27":
	$rand_value = "0";
	break;
	case "28":
	$rand_value = "1";
	break;
	case "29":
	$rand_value = "2";
	break;
	case "30":
	$rand_value = "3";
	break;
	case "31":
	$rand_value = "4";
	break;
	case "32":
	$rand_value = "5";
	break;
	case "33":
	$rand_value = "6";
	break;
	case "34":
	$rand_value = "7";
	break;
	case "35":
	$rand_value = "8";
	break;
	case "36":
	$rand_value = "9";
	break;
	}
	return $rand_value;
	}
    /**
    * @Date: 15-Feb-2010
    * @Method : validEmailId
    * @Purpose: Validate email Id if filled
    * @Param:  $value
    * @Return: boolean
    **/

   function validEmailId($value = null) {

         $v1 = trim($value);
         if($v1 != "" && !eregi("^[\'+\\./0-9A-Z^_\`a-z{|}~\-]+@[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+){1,3}$",$v1)){
	    return false; 
         }
      return true;
   } 

    function file_exists_in_directory($directory, $pattern=false, $filename=false) {
//echo $pattern."+++++".$directory;

        if(!isset($directory) OR !isset($filename) OR is_dir($directory) == false OR strlen($filename) < 0) return false;

        $returnval = false;
        if(false != ($handle = opendir($directory))) {

        while (false !== ($file = readdir($handle))) {

        if ($file != "." && $file != "..") {

        if($pattern != false) {

        if(preg_match("$pattern", $file) > 0 ) {
        $returnval = $file;
        break;
          }
        } else {
        if($file == $filename) {
         $returnval = $file;
          break;
                        }
                    }
                }
            }
        }
        closedir($handle);
        return $returnval;
    }
	// return an array of files in directory else false if none found

	function get_files($directory, $pattern = false) {

		if(!isset($directory) OR is_dir($directory) == false ) return false;
		$returnval = array();

		if(false != ($handle = opendir($directory))) {
		while (false !== ($file = readdir($handle))) {

	    if ($file != "." && $file != "..") {

		if($pattern != false) {

		if(preg_match("$pattern", $file) > 0 ) {
		$returnval[] = $file;
             }
			}else{
				$returnval[] = $file;
			}
			}
		}
		}
		closedir($handle);
		return $returnval;
	}

	/**
	* Makes directory, returns TRUE if exists or made
	* @param string $pathname The directory path.
	* @return boolean returns TRUE if exists or made or FALSE on failure.
	*/
	function mkdir_recursive($path, $mode = 0777)

	{	
		$basicPath = ROOT.DS."app".DS."webroot".DS."contents".DS;
		$dirs = explode(DS , $path);
		$count = count($dirs);
		$path = '';
		for ($i = 0; $i < $count; ++$i) {
	    $path .= $dirs[$i].DS;
		if (!is_dir($basicPath.rtrim($path,"/"))){
		mkdir($basicPath.$path, $mode);
			}
		}
		return true;
	}
	/**
	* Remove directory, returns TRUE if exists or made
	* @param string $pathname The directory path.
	*/
       function rmdir_recursive($dir) {

		$basicPath = ROOT.DS."app".DS."webroot".DS."contents".DS;
		if(is_dir($basicPath.$dir)){
		$files = scandir($basicPath.$dir);
		array_shift($files);    // remove '.' from array
		array_shift($files);    // remove '..' from array

		foreach ($files as $file) {
		$file = $basicPath.$dir .DS. $file;
		if (is_dir($file)) {
		rmdir_recursive($file);
		rmdir($file);
		} else {

		unlink($file);
		      }
		}
		rmdir($basicPath.$dir);
		}
	}
	
	function file_extension($filename)
	{
		$path_info = pathinfo($filename);
		return $path_info['extension'];
	}
	
	//changes sql timestamp format to compare
	function expiredTime($modified_date){
                $diff = abs(time()-strtotime($modified_date));
                 if ($diff < 0)$diff = 0;
                 $dl = floor($diff/60/60/24);
                 $hl = floor(($diff - $dl*60*60*24)/60/60);
                 $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);
                 $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));
               // OUTPUT
			   $hl = ($dl *24)+$hl;
               $return = array('hours'=>$hl, 'minutes'=>$ml, 'seconds'=>$sl);
               return $return;
	}
	
  function dateDiff($time1, $time2, $precision = 6) {
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
	$time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }
    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }
 
    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();
 
    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Set default diff to 0
      $diffs[$interval] = 0;
      // Create temp time from time1 and interval
      $ttime = strtotime("+1 " . $interval, $time1);
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
	$time1 = $ttime;
	$diffs[$interval]++;
	// Create new temp time from time1 and interval
	$ttime = strtotime("+1 " . $interval, $time1);
      }
    }
 
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
	break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
	// Add s if value is not 1
	if ($value != 1) {
	  $interval .= "s";
	}
	// Add value and interval to times array
	$times[] = $value . " " . $interval;
	$count++;
      }
    }
 
    // Return string with times
    return implode(", ", $times);
  }
  
   /** 	 @Date: 04-April-2013
    	*@Method : getuser (This 
    	*@Purpose: This function is used to active user list.
    **/
	/*function getuser(){
		 App::import("Model","User");
         $this->User= new User;
         $result= $this->User->find('list', array(
         'conditions' => array('status'=>'1'),
         'fields' => array('id','user_name'),
         'order'=>'user_name  ASC'
    ));
         $result = array('' => '--Select--') + $result;
		// pr($result);
         return $result;
		

	}*/

function getuser() {
      $users = TableRegistry::get('Users');
       
         $result= $users->find("list", [
			'keyField' => 'id',
			'valueField' => 'username',

         'conditions' => ['status'=>'1'],

         'fields' => ['id','username'],

         'order'=>['username' =>  'ASC']

  		  ]);
       

         $result = isset($result)? $result -> toArray():array();
         // pr($result);

         return $result;

    } 



	/**
    * @Date: 07-March-2013
    *@Method : getInstructionLists
    *@Purpose: Get an array of links for leads listing
  **/

  function getInstructionLists(){

    return $instLinkArray = array(
									'1' => 'Client Management',
									'2' =>'Client Sample Terms & Condition',
									'3' =>'Client Types',
									'4' =>'Current Sales Plan',
									'5' =>'Engagement Model',
									'6' =>'Freelancing Sites Policy',
									'7' =>'General Freelancing Sites',
									'8' =>'Lead Management',
									'9' =>'Local Outsourcing',
									'10' =>'Payment Reminder(s)',
									'11' =>'Project Categories',
									'12' =>'Sales Credentials',
									'13' =>'Sales Important URL',
									'14' =>'Sales Reminder Format(s)',
									'15' =>'Sales Tips',
									'85' =>'Sales Traits'
								);
  }
  // getprocessheadid 
  function getprocessheadid(){
  $settings = TableRegistry::get('Settings');
  $result= $settings->find("list", [
			'keyField' => 'key',
			'valueField' => 'value',

         'conditions' => ['id'=>'4'],

         'fields' => ['key','value']

  		  ]);
         $processhead = isset($result)? $result -> toArray():array();
         // pr($processhead);
         return $processhead;
  }
  



  	// get permissions for menus 
    function menu_permissions($controller,$action,$userID) {
		  App::import('Model','Permission');
		  $this->Permission= new Permission;
		  $cond = "WHERE controller='$controller' AND action='$action'";
		  $permissionsResult = $this->Permission->find('first',array(
		   'fields' => array('user_id'),
		   'conditions' => $cond,
		  ));
		  if(!empty($permissionsResult)){
		   $users = explode(",",$permissionsResult['Permission']['user_id']);
			if (in_array($userID,$users)) {
			return true;
		   }
		   else{
			return false;
		   }
		  }
		  return true;
		  
	}

 // Maninder 21-09-2016
	function sanitizeInput($input,$type = null){
		if(gettype($input) == 'integer'){
			$newstr = filter_var($input, FILTER_VALIDATE_INT);
		} else if(gettype($input) == 'string'){
			$newstr = filter_var($input, FILTER_SANITIZE_STRING);
		}
		return $newstr;
	}
		/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function strtohex($x)
  {
    $s='';
    foreach (str_split($x) as $c) $s.=sprintf("%02X",ord($c));
    return($s);
  }
 	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function ENC($string){
   $iv =   IVIM;
   $pass = PASSIM;
    $method = 'aes-128-cbc';    // method for encryption
    $enc = openssl_encrypt($string, $method, $this->strtohex ($pass), false ,$this->strtohex ($iv));
    return $enc;
  }
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function DCR($string){
    $iv =  IVIM;  //  getting defined constants
    $pass = PASSIM; //  getting defined constants
    $this->strtohex($iv);
    $this->strtohex($pass);
    $method = 'aes-128-cbc';    // method for encryption
    $dcr =  openssl_decrypt( $string , $method , $this->strtohex ($pass), false , $this->strtohex ($iv) );
    return $dcr;
  }
  	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions for url eligible encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function encForUrl($enc){
  $encoded = str_replace('/', 'Hxkuy9jdji877jkgha692ahuiyadyu76iuyai6', $enc);
  $encoded = str_replace('+', 'iy878ned7ehhsd87e6fd2140jdamnhgHHDysMM', $encoded);
  $encoded = str_replace('|', 'MMDU897bhhdtHHdtslsatxtas', $encoded);
  return $encoded;
 }
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl for url eligible decryptions
	/* @date: 12-01-2017
	**********************************************************************/
	function dcrForUrl($dcr){
	  $decrypt = str_replace('iy878ned7ehhsd87e6fd2140jdamnhgHHDysMM', '+', $dcr);
	  $decrypt = str_replace('Hxkuy9jdji877jkgha692ahuiyadyu76iuyai6', '/', $decrypt);
	  $decrypt = str_replace('MMDU897bhhdtHHdtslsatxtas', '|', $decrypt);
	  return $decrypt;
	}
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl for url eligible decryptions
	/* @date: 12-01-2017
	**********************************************************************/
	function modulepermissions($controller,$action){
	 	$modules = TableRegistry::get('Modules');
		$module = $modules->find('all',[
		 			'conditions' => ['controller' => $controller, 'action' => $action ,'is_enabled' => 'Yes']
		 		 ])->first();
		$related_actions = $modules->find('all',[
		 			'conditions' => ['controller' => $controller, 'FIND_IN_SET(\'' . $action . '\',related_actions)' ,'is_enabled' => 'Yes']
		 		 ])->first();
		//'conditions' => ['FIND_IN_SET(\'' . $id . '\',Credentials.other_alloted_user)']
		   return $data = isset($module)? true: isset($related_actions)? true: false;;
	}
		/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl for url eligible decryptions
	/* @date: 12-01-2017
	**********************************************************************/
	function getprefix($controller){
	 	if($controller == 'StaticPages'){
	 		$cont = 'static_pages';
	 		$prefix = $cont;
	 	} else if($controller == 'Admin'){
	 		$cont = 'users';
	 		$prefix = 'admin';
	 	}else if($controller == 'CredentialAudits'){
	 		$cont = 'credential_audit';
	 		$prefix = 'credential_audit';
	 	} else if($controller == 'PaymentMethods'){
	 		$cont = 'payment_methods';
	 		$prefix = 'payment_methods';
	 	}else if($controller == 'LearningCenters'){
	 		$cont = 'learning_centers';
	 		$prefix = 'learning_centers';
	 	}else if($controller == 'Profileaudits'){
	 		$cont = 'profileaudits';
	 		$prefix = 'profileaudits';
	 	}else{
	 		$cont = strtolower($controller);
	 		$prefix = strtolower($controller);
	 	}
		   return ['controller' => $cont, 'prefix' => $prefix];
	}

	
  function getuser_name($agency_id) {
     	$users = TableRegistry::get('Users');
         $result= $users->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            return $row['first_name'] . ' ' . $row['last_name'];
        }
    ])
         ->where(['status'=>'1','agency_id' => $agency_id])
         ->order(['first_name' =>  'ASC']);

         $result = isset($result)? $result -> toArray():array();
         
         $finalresult = $result;

         return $finalresult;

    }

    function getuser_name2($agency_id) {
      	$users = TableRegistry::get('Users');
         $result= $users->find("all", [
			'fields' => ['id','first_name','last_name']
   	 ])
         ->where(['status'=>'1','agency_id' => $agency_id])
         ->order(['first_name' =>  'ASC']);

         $result = isset($result)? $result -> toArray():array();
         
         $finalresult = $result;

         return $finalresult;

    } 


 function getProjectsofuser($userID = null){

	$projects = TableRegistry::get('Projects');



		// $condition =  "`status`='current' AND (`pm_id` = $userid OR `engineeringuser` = $userid OR `salesfrontuser` =$userid OR `salesbackenduser` = $userid OR FIND_IN_SET($userid,team_id))";
		     $result= $projects->find("all" , [
			/*'keyField' => 'id',
			'valueField' => 'project_name',*/
			/*'fields' => ['id','project_name']*/
			'conditions' =>[ 
						'OR' => [ ['FIND_IN_SET(\''. $userID .'\',Projects.pm_id)'],['FIND_IN_SET(\''. $userID .'\',Projects.engineeringuser)'],['FIND_IN_SET(\''. $userID .'\',Projects.salesbackenduser)'],['FIND_IN_SET(\''. $userID .'\',Projects.salesfrontuser)'], 'FIND_IN_SET(\''. $userID .'\',Projects.team_id)'],
						['Projects.status' => 'current'] ],
			 'fields' => ['id','project_name'],
			'order' => ['project_name' => 'ASC']
			 ]);
			
		$result = $result->toArray();
		//pr($result); die;
		 //$result = isset($result)? $result -> toArray():array();
		return $result;


	} 		
	
	/**********************************************************************/
	/* @page : new_layout_admin
	/* @who :Maninder
	/* @why : all menus will be iterated through this functions
	/* @date: 12-01-2017
	**********************************************************************/
	function get_menus($userdata){
			$modules = TableRegistry::get('Modules');
			$userinfo = $userdata;
			$modulesdata = $modules->find('all',[
			'fields' => ['id','role_id','module_name','display_name','is_core','agency_id'],
			'conditions' => ['parent_module' => 0,'is_displayed' => 'Yes','is_deleted' => 'No',
			  'OR' =>	[ ['is_enabled' =>'Yes'], ['is_core' =>'Yes']]
			]
			])->all();

			//pr($modulesdata); die;

			$modules = ($modulesdata) ? $modulesdata->toArray() : [];
			$filtered_modules = [];
			$roles = explode(',', $userinfo[0]['role_id']) ; // current user's roles
			
			$useragency = $userinfo[0]['agency_id'];
			if(count($roles) > 0 ){ // if there are any roles alloted to currentuser
				foreach($modules as $module){ // iterating all modules of current agency
					$modulesroles = explode(',', $module->role_id); // creating array of roles to which current module is alloted
					$agencies = explode(',', $module->agency_id);
					$is_once = 'yes'; // to make sure that item is pushed to final array once only
					foreach($roles as $role){ // iterating users roles
						if($module->is_core == 'Yes'){
								if((in_array($role, $modulesroles))){ // if user has role to which that module is alloted
									if($is_once == 'yes' ){
										$filtered_modules[] = $module; // push whole module to final array
										$is_once = 'no'; // stopping to push same module again	
									}
								}
						} else{
							if(((in_array($role, $modulesroles))  && in_array($useragency, $agencies)) ){ // if user has role to which that module is alloted
								if($is_once == 'yes' ){
									$filtered_modules[] = $module; // push whole module to final array
									$is_once = 'no'; // stopping to push same module again	
								}
							}	
						}
						
					}
				}	
			}
 
			return $filtered_modules; 
	}


	/**********************************************************************/
	/* @page : New layout Admin
	/* @who : Maninder
	/* @why : for rendering menus in layout
	/* @date: 12-01-2017
	**********************************************************************/
	function child_modules_fl($parent_id,$userdata){
			$modules =  $users = TableRegistry::get('Modules');
			$userinfo = $userdata;
			$modulesdata = $modules->find('all',[
				'fields' => ['Modules.id','Modules.agency_id','Modules.parent_module','Modules.is_displayed','Modules.is_deleted','Modules.role_id','Modules.including_user_id','Modules.except_user_id','Modules.is_core','Modules.display_name','Modules.is_enabled','Modules.default_alias','Modules.other_aliases','Perms.controller','Perms.action'],
				'conditions' => ['parent_module' => $parent_id, 'is_displayed' => 'Yes' ,'Modules.is_deleted' => 'No',
				 'OR' =>[ ['is_enabled' =>'Yes'], ['is_core' =>'Yes']]
						]
				])->contain(['Perms']);
			$modules = ($modulesdata) ? $modulesdata->toArray() : [];

			$filtered_modules = [];
			$roles = explode(',', $userinfo[0]['role_id']) ; // current user's roles
			$userID = $userinfo[0]['id'];
			$useragency = $userinfo[0]['agency_id'];
			if(count($roles) > 0 ){ // if there are any roles alloted to currentuser
				foreach($modules as $module){ // iterating all modules of current agency
					$modulesroles = explode(',', $module->role_id); // creating array of roles to which current module is alloted
					$agencies = explode(',', $module->agency_id);
					$alloweduserids = explode(',',$module['including_user_id']);
	    	   		$restricteduserids = explode(',',$module['except_user_id']);
					$is_once = 'yes'; // to make sure that item is pushed to final array once only
					$is_allowed = 'no';
					$is_pushed ="no";
					if((count($alloweduserids) > 0) && in_array($userID, $alloweduserids)){
						$is_allowed = 'yes';
					}
					foreach($roles as $role){ // iterating users roles
						if($module->is_core == 'Yes'){

								if((in_array($role, $modulesroles))){ // if user has role to which that module is alloted
									if($is_once == 'yes' ){
										//$filtered_modules[] = $module; // push whole module to final array
										$is_allowed = 'yes';
										if(count($restricteduserids) && in_array($userID, $restricteduserids)){
											$is_allowed = 'no';
										}
										$is_once = 'no'; // stopping to push same module again	
									}
								}
						} else{
							if(((in_array($role, $modulesroles))  && in_array($useragency, $agencies)) ){ // if user has role to which that module is alloted
								if($is_once == 'yes' ){
									//$filtered_modules[] = $module; // push whole module to final array
									$is_allowed = 'yes';
										if(count($restricteduserids) && in_array($userID, $restricteduserids)){
											$is_allowed = 'no';
										}
									$is_once = 'no'; // stopping to push same module again	
								}
							}	
						}
						if($is_allowed == 'yes' && $is_pushed == 'no'){
							unset($module['id']);
					    	unset($module['parent_module']);
					    	unset($module['is_displayed']);
					    	unset($module['is_deleted']);
					    	unset($module['role_id']);
					    	unset($module['including_user_id']);
					    	unset($module['except_user_id']);
					    	unset($module['is_core']);
					    	unset($module['default_alias']);
					    	unset($module['is_enabled']);
					    	unset($module['other_aliases']);
					    	unset($module['Perms']['id']);
					    	unset($module['Perms']['parent_module']);
					    	unset($module['Perms']['is_displayed']);
					    	unset($module['Perms']['is_deleted']);
					    	unset($module['Perms']['role_id']);
							$filtered_modules[] = $module; // push whole module to final array
							$is_pushed = 'yes';
						}

					}
				}	
			}
			
			//pr($filtered_modules); die;
			return $filtered_modules; // returning final array
		}


	   function menu_permissionscontroller($controller,$action,$userID) {
		   $usersdata = TableRegistry::get('Users');
				$user= $usersdata->find("all", [
				'conditions' => ['id'=>$userID]
				])->first();
			//pr($userID); die;
		  $permissions = TableRegistry::get('Permissions');
	       $data  = $permissions->find("all" , [
			          'conditions' => ['controller' => strtolower($controller), 'action' => strtolower($action)]
			       ])->first();
	       if(count($data) > 0) { // checking if permissions controller has entry of route hit by the userss

	       		$modules = TableRegistry::get('Modules');
	       		/*pr($data); die;*/
	    	   	$modulesdata  = $modules->find("all" , [
			          'conditions' => [ 
			          'is_enabled' => 'Yes','is_deleted' => 'No',
			          'OR' => [['default_alias' => $data['id'] ], ['FIND_IN_SET(\''. $data['id'] .'\',other_aliases)']]
			          ]
			       ])->first();	
	    	  	
	    	   	$is_allowed = false;
	    	   	if(count($modulesdata) > 0){ // checking if permission has entery in the module table.
	    	   		$modulesroles = explode(',', $modulesdata->role_id);
	    	   	    $userroles = explode(",",$user->role_id);
	    	   		$useragency = $user['agency_id'] ;
	    	   		$allowedagencies = explode(',', $modulesdata->agency_id);
	    	   		$is_once = 'yes'; // to make sure that item is pushed to final array once only
	    	   		$alloweduserids = explode(',',$modulesdata['including_user_id']);
	    	   		$restricteduserids = explode(',',$modulesdata['except_user_id']);
	    	   		if(count($alloweduserids) && in_array($userID, $alloweduserids)){
						$is_allowed = true;
					}
	    	   		foreach($userroles as $role){
					if($modulesdata->is_core == 'Yes'){
								if((in_array($role, $modulesroles))){ // if user has role to which that module is alloted
									if($is_once == 'yes' ){
										//$filtered_modules[] = $module; // push whole module to final array
										$is_allowed = true;
										if(count($restricteduserids) && in_array($userID, $restricteduserids)){
											$is_allowed = false;
										}
										$is_once = 'no'; // stopping to push same module again	
									}
								}
						} else{
							if(((in_array($role, $modulesroles))  && in_array($useragency, $allowedagencies)) ){ // if user has role to which that module is alloted
								if($is_once == 'yes' ){
									//$filtered_modules[] = $module; // push whole module to final array
									$is_allowed = true;
									if(count($restricteduserids) && in_array($userID, $restricteduserids)){
											$is_allowed = false;
									}
									$is_once = 'no'; // stopping to push same module again	
								}
							}	
						}   		
	    	   		}
	    	   		
	    	   		return $is_allowed;
	    	   	} else{
	    	   		$is_allowed = false;
	    	   		
	    	   		return $is_allowed; // for now returning true because we need to access some routes
	    	   	}
	       } else{
	       	// if there is no entery in the db
	       	$is_allowed = false;

	       	return false; // for now returning true because we need to access some routes
	       }
           
	}






		function getaliasnames($ids){
			$exp = explode(',', $ids);
			$permissions =  TableRegistry::get('Permissions');
			$modules = $permissions->find('list',[
					'keyField' => 'alias',
					'valueField' => 'alias',
					'fields'=>[ 'alias','alias'],
				'conditions' => ['id IN' => $exp ]
				])->toArray();
			return array_keys($modules);
		}


		function getusersfromrole($role_id){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		$users = TableRegistry::get('Users');
		$exp = explode(',', $role_id);

		$ids=[];
		if(count($exp) > 0){
				foreach($exp as $role){
					$result= $users->find("list", [
					'keyField' => 'id',
					'valueField' => 'username',
					'conditions' => ['status'=>'1','agency_id' => $userSession[3],'FIND_IN_SET(\''. $role .'\',role_id)'],
					'fields' => ['id','username']
		  		  ]);
		  		   $result = isset($result)? $result->toArray():array();	
		//  		   pr($result);
		  		   $idss = array_merge($ids,array_keys($result));
		  		   foreach($idss as $id){
		  		   	if(!in_array($id, $ids))
		  		   		array_push($ids, $id);
		  		   }
				}
				//pr($ids);
				if(count($ids) > 0){
					$query = $users->find('all',[
							'conditions' => ['id IN' => $ids]				
						]);
				}

		}
		 $result = isset($query)? $query->toArray():array();		
		 return $result; 
		
	}

	function getusersexceptrole($role_id,$userSession){
		$users = TableRegistry::get('Users');
		$exp = explode(',', $role_id);
		/*pr($exp); die;*/
		$ids=[];
		if(count($exp) > 0){
				//foreach($exp as $role){
					$result= $users->find("list", [
					'keyField' => 'id',
					'valueField' => 'role_id',
					'conditions' => ['status'=>'1','agency_id' => $userSession[3]/*'!FIND_IN_SET(\''. $role .'\',role_id)'*/],
					'fields' => ['id','role_id','agency_id','status']
		  		  ]);
		  		   $result = isset($result)? $result->toArray():array();	
		  		 	/*pr($result); die;*/
		  		   //$idss = array_merge($ids,array_keys($result));
		  		   foreach($result as $id => $userrole){
		  		   	$userroles = explode(',', $userrole);
		  		   	$is_once = 'yes';
		  		   		foreach($exp as $role){
		  		   			if(in_array($role, $userroles) && $is_once =='yes'){
			  		   			array_push($ids, $id);
			  		   			$is_once ='no';
			  		   		}	
		  		   		}
		  		   }
		  		 /*  pr($ids); die;*/
				if(count($ids) > 0){
					$query = $users->find("list", [
							'keyField' => 'id',
							 'valueField' => function ($row) {
				            return trim($row['first_name']);
				        }
				    ])
		         ->where(['status'=>'1','id NOT IN' => array_unique($ids),'agency_id' => $userSession[3]])
		         ->order(['first_name' =>  'ASC']);
				}

		}
		 $result = isset($query)? $query->toArray():array();		

		 return $result; 
		
	}


	function getpostfix($controller){
	 	if($controller == 'static_pages'){
	 		$cont = 'StaticPages';
	 	} else if($controller == 'users'){
	 		$cont = 'Admin';
	 	}else if($controller == 'credential_audit'){
	 		$cont = 'CredentialAudits';
	 	
	 	} else if($controller == 'payment_methods'){
	 		$cont = 'PaymentMethods';
	 	
	 	}else if($controller == 'learning_centers'){
	 		$cont = 'LearningCenters';
	 		
	 	}else if($controller == 'profileaudits'){
	 		$cont = 'Profileaudits';
	 	
	 	}else{
	 		$cont = ucfirst($controller);
	 		
	 	}
		   return ['controller' => $cont];
	}


















	function getusersasrole($role_id,$userSession){
			$users = TableRegistry::get('Users');
		$exp = explode(',', $role_id);
		$ids=[];
		if(count($exp) > 0){
				//foreach($exp as $role){
					$result= $users->find("list", [
					'keyField' => 'id',
					'valueField' => 'role_id',
					'conditions' => ['status'=>'1','agency_id' => $userSession[3]/*'!FIND_IN_SET(\''. $role .'\',role_id)'*/],
					'fields' => ['id','role_id','agency_id','status']
		  		  ]);
		  		   $result = isset($result)? $result->toArray():array();	
		  		 	/*pr($result); die;*/
		  		   //$idss = array_merge($ids,array_keys($result));
		  		   foreach($result as $id => $userrole){
		  		   	$userroles = explode(',', $userrole);
		  		   	$is_once = 'yes';
		  		   		foreach($exp as $role){
		  		   			if(in_array($role, $userroles) && $is_once =='yes'){
			  		   			array_push($ids, $id);
			  		   			$is_once ='no';
			  		   		}	
		  		   		}
		  		   }
		  		 /*  pr($ids); die;*/
				if(count($ids) > 0){
					$query = $users->find("list", [
							'keyField' => 'id',
							 'valueField' => function ($row) {
				            return trim($row['first_name']);
				        }
				    ])
		         ->where(['status'=>'1','id IN' => array_unique($ids),'agency_id' => $userSession[3]])
		         ->order(['first_name' =>  'ASC']);
				}

		}
		 $result = isset($query)? $query->toArray():array();		

		 return $result; 
		
	}
	

	function getprofile($agency_id){
		 $SaleProfiles = TableRegistry::get('SaleProfiles');
		 $result = $SaleProfiles->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['status'=>1,'agency_id' => $agency_id],
			'fields' => ['id','username'],
			'order' => 'username ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
		$finalresult =  $res;
		return $finalresult;
	}

	function getusersfromids($idsarr){
		 $SaleProfiles = TableRegistry::get('Users');
		 $result = $SaleProfiles->find('all',[
			'conditions' => ['status'=>1,'id IN' => $idsarr],
			'fields' => ['id','username','first_name','last_name','image_url'],
			'order' => 'username ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
 		$finalresult = [];
 		foreach ($res as $user) {
 			$user['id'] = $this->ENC($user['id']);
 			$finalresult[] = $user;
 		}
		//$finalresult =  $res;
		return $finalresult;
	}
	function getValueofEntity($userid , $entity_id){
		$id = $this->DCR($userid);
		 $user = TableRegistry::get('Users');
		  $userdata = $user->find('all',[
			'conditions' => ['status'=>1,'id' => $id],
		 ])->first();
		 $entities = TableRegistry::get('Entities');
		 $entity = $entities->find('all',[
			'conditions' => ['id' => $entity_id]
		 ])->first();
		 if(count($entity) > 0 && $entity->name == 'Project'){
		 	/*If Entity Project is Selected All projects of current user are returned*/
		 	return  $this->getProjectsofuser($id);
		 } else if(count($entity) > 0 && $entity->name == 'Ticket'){
		 	return  'None';
		 } else if(count($entity) > 0 &&  $entity->name == 'Leads'){
		 	 $leadsTable = TableRegistry::get('Leads');
		 	$leads = $leadsTable->find('all',[
						'conditions' => ['salesfront_id' => $id,'status' => 'active'],
						'fields' => ['id'=> 'id','name' => 'url_title']
					]);
		 	return  $leads;
		 } else {
		 	return 'none';
		 }
		 
	}		
	/*************************************\
	| @Who : Maninder
	| @Function name : lastrepier
	| @why : Verify Permissions  in a Project.
	| @when : 11-07-2017
	\*************************************/

 function verifyprojectpermission($project_id,$user_session,$permission){
 		 		/*r : pm_id
	 			a : engineeringuser
	 			c : salesfrontuser
	 			i : salesbackenduser*/	
 		 $entities = TableRegistry::get('Projects');
		 $project = $entities->find('all',[
			'conditions' => ['id' => $project_id]
		 ])->first();
		 $roles = explode(',', $user_session[2]);
		 if(in_array(1, $roles) || in_array(8, $roles)){
		 	//if Current User is Super Admin Simply Allowing Everything
		 	return true;
		 } else if($permission == 'quickadd_payment'){
		 	// Everyone can add billing accept Accountable in the Project
		 	if(in_array($user_session[0],[ $project['pm_id'], $project['salesfrontuser'],$project['salesbackenduser'] ] )) {
		 		return true;	
		 	} else if ($user_session[0] == $project['engineeringuser']){
		 		return false ; 
		 	}
		 }	

 }

}


?>