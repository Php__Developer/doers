<?php echo $this->Html->script(array('jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js')); ?>
<?php echo $this->Html->css(array("jquery.fancybox-1.3.4.css")); ?>
   <?php echo $this->Html->css(array("reports.css")); ?>	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
		<body>
		
		<div class="wrapper-ab">
            <div class="main-ab">
            <div class="heading-ab">Admin Reports - For HR Department</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/appraisals/salaryreport/', true);?>">Salary Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/leadreport1/', true);?>">Lead Report (Date Wise)</a></div></div>
			<div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/attendances/leavereport/', true);?>">Leave Report (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/evaluations/performancereport/', true);?>">Performance Report (Date Wise)</a></div></div>
            <div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/evaluations/performancereportmonth/', true);?>">Performance Report (User / Month Wise)</a></div></div>
		    <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/learning_centers/learningcenterreportuser/', true);?>">Learning Center Report (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/tickets/ticketreportdate/', true);?>">User Ticket Report (Date Wise)</a></div></div>
			<div class="cleaner"></div>
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Finance Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/billings/billingreportweek/', true);?>">Week Wise Billing Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/payments/paymentreportweek/', true);?>">Week Wise Payment Report</a></div></div>
			<div class="cleaner"></div>
            
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Sales Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/bidspiechartreport/', true);?>">Bidding Report - (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/activeleadreport/', true);?>">No of Active Leads (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/winclosereport/', true);?>">No of Win / Closed Projects (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/overleadcountuserreport/', true);?>">Overall Lead Count along with Status (User Wise)</a></div></div>
			<div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/companywideleadreport/', true);?>">Company Wide Line Graph - (Month Wise)</a></div></div>
             <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/leads/activelead/', true);?>">Current Active Leads</a></div></div>
                        <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo $this->Url->build('/projects/holdproject/', true);?>">On Hold Projects</a></div></div>
           
            <div class="cleaner"></div>
            </div>

    </div>
</body>
</html>