<?php echo $html->css(array("screen")); ?>
<?php echo $javascript->link(array("listing","jquery-1.4.1.min")); ?>
<script type="text/javascript">
     var base_url = "<?php echo BASE_URL; ?>";
	function changeStaus(){
		if($('#projectStatus').val() != "closed"){
			if(getConfirmation("Are you sure to change project's status?")){
				var status = $('#projectStatus').val();
				var id = $('#project_id').val();
				if((status =="current" || status =="onhold") && (old_status == "closed")){
					var msg = "Are you want to activate this Project client?"
					confirmAction(msg,"act");
				}
				var client_update = $('#client_update').val();
				var client_id = $('#client_id').val();
				$('#tri_'+id).remove();
				if($.active>0){
				}else{
					$.post(base_url+'admin/projects/projectStatusChangebyAjax/',{'id':id,'status':status,'client_update':client_update,'client_id':client_id},function(data){
						if(data==1){
							//window.parent.$('#close-fb').click();
							$('#showsuccess').html('Project status updated successfully.');
							$('#showsuccess').attr('style','display:block;color:green;font-size:13px;');
							setTimeout(function() { $('#showsuccess').fadeOut('slow'); }, 1000);
						}
					});
				}
			}
		}
	}
	function confirmAction(msg,key){
			var action = confirm(msg);
			if(action == true){
				$('#client_update').val(key);
			}
				return true;		
	}
</script>
<style>
div#fancybox-content {
    width: 814px !important;
}
</style>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
			<center><span style="font-weight:bold;"> Closed Projects </span>
			<div>
				<?php echo $form->create('Project',array('action'=>'report/closed','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
					<div style="margin-left: -300px;  padding-top: 26px;">
					<span style="font-size:14px;font-weight:bold;">Project Name :</span>
					<?php echo $form->input("project_name",array("type"=>'text',"style"=>"width:150px;height:24px;","label"=>false,"div"=>false))."&nbsp;&nbsp;&nbsp;";
						echo $form->button("Search", array('value'=>'Search','id'=>'search','type'=>'submit'))."&nbsp;&nbsp;&nbsp;"; 
						echo $form->button("Reset",array('type'=>'button','div'=>false,'onclick'=>"location.href='".BASE_URL."admin/projects/report/closed'"));	
					?>
					</div>
				<?php echo $form->end(); ?>
			<div>
			<div id="showsuccess" style="display:none;"></div>
				<table style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
					<tr>
						<th class="userattend" width="10%">Name</th>
						<th class="userattend" width="10%">Client</th>
						<th class="userattend" width="10%">Salesfront</th>
						<th class="userattend" width="10%">Salesback</th>
						<th class="userattend" width="10%">Resource</th>
						<th class="userattend" width="8%">Left </th>
						<th class="userattend" width="16%">Process-notes</th>
						<th class="userattend" width="16%">Notes</th>
						<th class="userattend" width="10%">Status</th>
					</tr>
				<?php $i = 1;  if(count($resultData)>0){  
				foreach($resultData as $projects): ?>
				<tr id="<?php echo "tri_".$projects['Project']['id']; ?>">
					<td><?php  $str = strip_tags($projects['Project']['project_name']);  echo $name = substr($str,0,18).((strlen($str)>18)?"...":"");   ?></td>
					<td><?php  $str = strip_tags($projects['Contact']['name']);  echo $name = substr($str,0,18).((strlen($str)>18)?"...":"");   ?></td>
					<td><?php  $str = strip_tags($projects['Salefront_User']['first_name'].' '.$projects['Salefront_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php $str = strip_tags($projects['Saleback_User']['first_name'].' '.$projects['Saleback_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php  $str = strip_tags($projects['Engineer_User']['first_name'].' '.$projects['Engineer_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php  echo $projects['Project']['left']; ?></td>
					<td><?php $str = strip_tags($projects['Project']['processnotes']);  echo $name = substr($str,0,22).((strlen($str)>22	)?"...":"");  ?></td>
					<td><?php $str = strip_tags($projects['Project']['notes']);  echo $name = substr($str,0,22).((strlen($str)>22	)?"...":"");  ?></td>
					<td><script> var old_status ='<?php echo $projects['Project']['status'];?>'; </script><?php
				   $options=array('current' => 'Current', 
								   'onhold'    => 'On Hold',
								   'closed'    => 'Closed'
                      );
				echo $form->input("status",array('type'=>'select',"default"=>"closed","id"=>"projectStatus",'options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false,"style"=>"width: 78px;","onchange"=>"javascript:changeStaus();"));   
                echo $form->hidden("clientUpdate",array('id'=>'client_update'));	
				echo $form->hidden("id",array("id"=>"project_id","value"=>$projects['Project']['id']));	$id = $projects['Project']['client_id'];		
				echo $form->hidden("client_id",array("id"=>"client_id","value"=>$projects['Project']['client_id']));				
				?>
				</td>
				</tr>
				<?php  $i++;
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="9" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->