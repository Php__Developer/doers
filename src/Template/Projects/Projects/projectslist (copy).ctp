<html>
<body>
<script type="text/javascript">
	$(document).ready(function(){ 
			 //fancybox settings
			$(".view_delayed").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
			$(".view_payment").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		
		$(".view_alert").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".view_hourly").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".payquickadd").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".billingquickadd").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".billingsquickedit").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	1100, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});  
			$(".payquickedit").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	950, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".editproject").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".milestoneimg").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	1050, 
				'height' :	700, 
				'onStart': function(){
				jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
	
	function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}
</script>
<style>
#hide_for_left
{display:none;}
</style>
<?php
$newUrl = "list".$urlString;
$urlArray = array(
	'field' 	=> $search1,
	'value' 	=> $search2
);
///$paginator->options(array('url'=>$urlArray));
$this->Paginator->options(array('url'=> array('field'   => $search1,
                                     'value'    => $search2)));
$this->Paginator->templates('number');
?>


<?php echo $this->Form->create('Project',array('url' => ['action'=>'projectslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>



<!--  start content-table-inner -->
	<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $this->Flash->render(); ?>
			<table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
		<tr>
			<td width="14%">
				<b>Search by:</b>
				<?php
				$fieldsArray = array(
				''				  => 'Select',
				'Projects.project_name'  =>  'Name',
				'Contact.name'  =>  'Client',
				'Projects.platform'  =>  'Platform ',
				'Projects.status'  =>  'Status',
				);
				echo $this->Form->select("Project.fieldName",$fieldsArray,['value'=>$search1],array("id"=>"searchBy","label"=>false,"style"=>"width:200px","class"=>"styledselect","empty"=>false),false); ?>
			
			</td>



			<td width="20%">
				<b>Search value:</b><br/>
				<?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "Project.publish"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $this->Form->input("Project.value1",array("id"=>"search_input","class"=>"top-search-inp","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
				?>
			</td>
			<td width="40%"><br/>
		  		

				<?php
				echo $this->Form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";			
				
				echo $this->Html->link("Reset",
                ['controller'=>'projects','action'=>'projectslist','prefix' => 'projects'],['class'=>"form-reset"]);  
				 $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/projects/projectslist', true)));

				?>

			</td>
		</tr>
	</table>
		<br/>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-check"><input  type="checkbox" id="toggle-all"/> </th>
					<th class="table-header-repeat line-left"  width="20%"><?php   echo $this->Paginator->sort('Projects.project_name','Name');?></th>
					<th class="table-header-repeat line-left"  width="15%"><?php   echo $this->Paginator->sort('Contact.name','Client');?></th>
					<th class="table-header-repeat line-left"  width="10%"><?php   echo $this->Paginator->sort('Projects.platform','Platform');?></th>
				   <th class="table-header-repeat line-left" width="8%">  <?php    echo $this->Paginator->sort('Projects.payment','Payment');?></th>
				   <th class="table-header-repeat line-left" width="8%">  <?php    echo $this->Paginator->sort('Projects.status','Status');?></th>
				   <th class="table-header-repeat line-left" width="10%">  <?php   echo $this->Paginator->sort('Projects.left','Left');?></th>
				<th class="table-header-repeat line-left" width="29%"><a href="#A">Options</a></th>
				</tr>
				<?php if(count($resultData)>0){
				//pr($resultData);
			$i = 1;
			foreach($resultData as $result): ?>
				<tr>
					<td><input  type="checkbox" name="IDs[]" value="<?php echo $result['id'];?>"/></td>

					<td><?php $str = strip_tags($result['project_name']); 
					echo $name = substr($str,0,25).((strlen($str)>25)? "...":""); ?>
					</td>
					<td><?php $str = strip_tags($result['Contact']['name']); 
					echo substr($str,0,25).((strlen($str)>25)? "...":"");  ?></td>
					<td><?php echo $result['platform']; ?></td>
					<td><?php echo $result['payment']; ?></td>
					<td><?php echo $result['status']; ?></td>
					<td>
					<?php $left = (($result['totalamount']) - ($result['total_payment']));
					echo number_format($left, 2, '.', ''); ?></td>
					<td class="options-width" align="center">
						<?php if($left<0){
						$left_hide="hide_for_left";
										$left_text="Negative Balance Contact Admin";
										}else
										{ $left_hide="";$left_text="";
										} 
						echo $this->Html->link("",
								array('controller'=>'payments','action'=>'quickadd','prefix'=>'payments','id'=>$result['id']),
								array('class'=>'info-tooltip payquickadd','id'=>$left_hide,'title'=>'Add Payment')
							);
						?>
						<?php
						
						
						if($result['engagagment']=="hourly" && $result['verify']=="1") {
											echo $this->Html->link("",
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=>$result['id']),
											array('class'=>'info-tooltip billingquickadd','id'=>$left_hide,'title'=>'Add Billing')
										);
								}
						elseif($result['engagagment']=="hourly"  && $result['verify']=="0") {

						echo $this->Html->link("","javascript:void(0)",
										array('class'=>'info-tooltip verifybillingimg',
										'id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()')
									);	
									
						
						}
						?>
						<?php echo $this->Html->link("",
								array('controller'=>'payments','action'=>'quickview','prefix'=>'payments','id'=>$result['id']),
								array('class'=>'info-tooltip payquickedit','id'=>$left_hide,'title'=>'View Payment')
							);
						?>
						<?php echo $this->Html->link("",
								array('controller'=>'billings','action'=>'quickview','prefix'=>'billings','id'=>$result['id']),
								array('class'=>'info-tooltip billingsquickedit','id'=>$left_hide,'title'=>'View Billings')
							);
						?>
						<?php
							echo $this->Html->link("",
								array('controller'=>'projects','action'=>'quickedit','id'=>$result['id']),
								array('class'=>'info-tooltip editproject','title'=>'Edit')
							);
						?>
						<?php
						echo $this->Html->link("",
						array('controller'=>'projects','action'=>'delete','id'=>$result['id']),
						array('class'=>'del_new info-tooltip delete','id'=>$left_hide,'title'=>'Delete')
					);
						?>
						<?php
						if($result['engagagment']=="fixed"||$result['engagagment']=="monthly") {
							echo $this->Html->link("",
								array('controller'=>'milestones','action'=>'milestoneslist','prefix'=> 'milestones','id' => $result['id']),
								array('class'=>'info-tooltip milestoneimg','id'=>$left_hide,'title'=>'Milestone')
							);
						}
						?>
						<span style="color:red"><?php echo $left_text;?></span>
					</td>
				</tr>
				<?php $i++ ;
				endforeach; ?>
				<?php } else { ?>
		<tr>
			<td colspan="10" class="no_records_found">No records found</td>
		</tr>
			<?php } ?>
				</table>
				<!--  end product-table...... --> 
			</div>
			<!--  end content-table  -->
		
			<!--  start actions-box .. -->
			<!--div id="actions-box">
				<a href="" class="action-slider"></a>
				<div id="actions-box-slider">
					
					<?php echo $this->Form->submit("Activate",array("div"=>false,"class"=>"action-activate","name"=>"publish",'onclick' => "return atleastOneChecked('Activate selected records?');")); ?>
					<?php echo $this->Form->submit("Deactivate",array("div"=>false,"class"=>"action-deactivate","name"=>"unpublish",'onclick' => "return atleastOneChecked('Deactivate selected records?');")); ?>
				</div>
				<div class="clear"></div>
			</div-->
			<!-- end actions-box........... -->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
				     <td>
				            <?php  echo $this->Paginator->prev('<span class="page-left" ></span>', ['escape' => false] );?>
                                <div id="page-info">
                                    <?php  echo $this->Paginator->counter( 'Page {{page}} / {{pages}}');
                                     echo $this->Paginator->next('<span class="page-right" ></span>', ['escape' => false] ); ?>
                                </div>
                        </td>


			
			</tr>

			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>

	</td>
	<td>

	<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Projects Management</h5>
          This section is used by Admin and PM only to Manage senstive Projects.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $this->Html->link("Add New Projects",
            array('controller'=>'projects','action'=>'add')
            );	
            ?>
            </li> 
  					
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->

</td>
</tr>
</table>
 
<div class="clear"></div>
 

</div>
<?php echo $this->Form->end(); ?>
<!--  end content-table-inner  -->