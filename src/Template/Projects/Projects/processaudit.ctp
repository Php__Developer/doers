<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){ 
		$('.tablesorter').tablesorter({
			headers: { 
						 1: {
						  sorter: false 
						  },
						 2: {
						  sorter: false 
						  },
						  3: {
						  sorter: false 
						  },
						  4: {
						  sorter: false 
						  },
						  5: {
						  sorter: false 
						  },
						  6: {
						  sorter: false 
						  }
						}
				});
		$(".editProcess").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function changeColor(id){
	var id = $("#"+id);
		if(id.is(':checked'))	{
			id.closest('tr').addClass('colorOnalert');
		}
		else{
			id.closest('tr').removeClass('colorOnalert');
		}
	}
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
</script>
<style>
#hide_for_left
{display:none;}
</style>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	

<?php echo $this->Form->create('Project',array('url'=>['action'=>'processaudit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>


	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php 
							echo $this->Form->submit('Save',array('div'=>false,"name"=>"submit","style"=>"width:45px;"));
			?>
			
			<br/><br/>
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<thead>
					<th class="table-header-repeat line-left minwidth-1" width="25%"><a href="#A">Project Name</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="14%"><a href="#A">Daily Update</a></th>
					<th  class="table-header-repeat line-left minwidth-1"  width="14%"><a href="#A">Dropbox</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="14%"><a href="#A">ERP</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="14%"><a href="#A">On Alert</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="19%"><a href="#A">Action</a></th>
				</thead>
				<tbody>
				<?php foreach($resultData as $process):
				$id = $process['id'];
				($process['onalert']=="1") ? $class = "class='colorOnalert' " : $class = "" ; 
				$finishdate = ($process['finishdate'] != '0000-00-00') ? date("d-m-Y",strtotime($process['finishdate'])) : "";
				$nextrelease = ($process['nextrelease'] != '') ? date("d-m-Y",strtotime($process['nextrelease'])) : "";
				$now = date("d-m-Y");
				$delayed = "";
				if ($nextrelease != "" && (strtotime($now) > strtotime($nextrelease))) {
					$delayed = $common->dateDiff($nextrelease ,$now);
				}else{
						$delayed = "" ;
				}
				
				?>
				<tr <?php echo $class; ?> >
					<td><?php  $str = strip_tags($process['project_name']);  $name = substr($str,0,20).((strlen($str)>20)?"...":"");  ?> 
						<?php 
						$title = "Resource :".$process['Engineer_User']['first_name'].' '.$process['Engineer_User']['last_name']."<br/> Started On :".date("d-m-Y h:i:s a",strtotime($process['created']))."<br/> Next Release :".$nextrelease."<br/> Delayed :".$delayed;
							echo $this->Html->link($name,'#A',
								array('class'=>'info-tooltip','title'=>$title)
							); 
						 echo $this->Form->hidden("name.$id",array('value'=>$process ['project_name']));
					?>
					<span class="usr_ip_1">
						  </span>
						  <br/>
						<span id="usr_name_1">
						<?php if($process['Project']['hours']>= 48) { 
							echo "<span style='color:red'>Last updated by ".$process['Last_Updatedby']['first_name']." ".$process['Last_Updatedby']['last_name']." on ".date("d/m/Y",strtotime($process['modified']))."</span>"; 
						 } else {
							echo "Last updated by ".$process['Last_Updatedby']['first_name']." ".$process['Last_Updatedby']['last_name']." on ".date("d/m/Y",strtotime($process['modified'])); 
						 } ?>
						</span>
					</td>
					<td>
					<?php ($process['dailyupdate']=="1") ? $checked = "checked" : $checked = "" ; 
						echo $this->Form->input("dailyupdate.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked));
					?>
					</td>
					<td>
					<?php ($process['dropbox']=="1") ? $checked = "checked" : $checked = "" ; 
						echo $this->Form->input("dropbox.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked));
					?>
					</td>
					<td>
					<?php ($process['vooap']=="1") ? $checked = "checked" : $checked = "" ; 
						echo $this->Form->input("vooap.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked));
					?>
					</td>
					<td>
					<?php ($process['onalert']=="1") ? $checked = "checked" : $checked = "" ; 
						echo $this->Form->input("onalert.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked,"onclick"=>"javascript :changeColor(this.id);"));
						//echo $process['client_id'];
					?>
					</td>
					<td>
					<?php $left = $process['totalamount']-$process['Project']['total_payment']; 
							if($left<0){ $left_hide="hide_for_left";
									$left_text="Negative Balance Contact Admin";
							}else{ $left_hide="";$left_text="";}
							
						echo $this->Html->link("",
								array('controller'=>'projects','action'=>'editprocessaudit','id'=>$process['id']),
								array('class'=>'info-tooltip editproject editProcess','id'=>$left_hide,'title'=>'Edit','target'=>'_blank')
							);
				
						echo $this->Html->link("",
								array('controller'=>'projects','action'=>'projectdetails','id'=>$process['id']),
								array('class'=>'info-tooltip go','title'=>'Go','id'=>$left_hide,'target'=>'_blank')
							);
					?>
					<span style="color:red"><?php echo $left_text;?></span>
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
				<table>
				<tr>
					<td>
					<?php 
							echo  $this->Form->submit('Save',array('div'=>false,"name"=>"submit","style"=>"width:45px;"));
							//echo $form->submit('Save & Email',array('div'=>false,"name"=>"submit","style"=>"width:105px;margin-left:10px;"));
							echo $this->Form->end(); ?>
					</td>
				</tr>
				</table>
				<!--  end product-table................................... --> 
					<div class="clear"></div>
	</td>
	<td>
</td>
</tr>
</table>
			</div>
			<!--  end content-table  -->
	<div class="clear"></div>
</div>