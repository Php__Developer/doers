<?php echo $html->css(array('styles')); ?>
<script type="text/javascript">
	$(document).ready(function(){ 
			 //fancybox settings
			$("#changeOrder").click(function(){
				 if ($('#changeOrder').is(':checked')){ 
					$('#changeOrder').val('ascending');
						var url = document.URL;
						var url1 = url.replace('/orderby:asc','');
						var url2 = url1.replace('/orderby:desc','');
						$('#adminreport').attr('action',url2+'/orderby:asc');
				}else{ 
						$('#changeOrder').val('descending');
						var url = document.URL;
						var url1 =url.replace('/orderby:asc','');
						var url2 = url1.replace('/orderby:desc','');
						$('#adminreport').attr('action',url2);
				}
				$('#adminreport').submit();
			});
	});
</script>
<!--  start content-table-inner -->
<?php
	$newUrl = $this->params['url'];
	$a=explode("/",$newUrl['url']);
	$id  = $a[2];
?>
	<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<?php echo $form->create('Project',array('action'=>'clientprojectdetails/'.$id,'id'=>'adminreport','method'=>'POST','onsubmit' => 'return validateForm();',"class"=>"login")); ?>
		<div class="searching_div">
		<?php $projectData = $projectData[0];  ?>
		<table class="hellStatus" width = "100%" cellspacing="2" cellpadding="4" border="0" align="center" class="top-search" >
			<tr>
				<br/><center><h2>Project : <?php echo $projectData['Project']['project_name']; ?></h2></center>
			</tr>
			<tr>
				<td><b>Next Release : </b><?php echo date("d M,Y",strtotime($projectData['Project']['nextrelease'])); ?></td>
				<td><b>Last Release : </b><?php echo date("d M,Y",strtotime($projectData['Project']['finishdate'])); ?></td>
				<td colspan="2"></td> 	
			</tr>
		
			<tr>
				<span>	<?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_blue.png', array('width' => '1%','height'=>'10%'))?>
				Open
				</span> 
				<span>   <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_green1.png', array('width' => '1%','height'=>'10%'))?>
				Close
				</span> 
				<span> <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_red1.png', array('width' => '1%','height'=>'10%'))?>
				Expired
				</span>
				&nbsp;&nbsp;
				<span>
				<?php //pr($this->params); die;
				if(isset($this->params['named']['orderby']) && $this->params['named']['orderby']=='asc'){
					$checked = "true";
					$labelTxt  = "Ascending";
				}else{
					$checked = "";
					$labelTxt  = "Descending";
				} ?>				
				<?php	echo $form->input("check_it",array('type'=>'checkbox','id'=>'changeOrder',"label"=>false,"div"=>false,"hiddenField"=>false,"value"=>"descending","checked"=>$checked,"title"=>"Change order"));
				?>
				<label for="check_it" style="color:#000000;"><?php echo $labelTxt; ?></label>
				</span>
			</tr>
		</table><br/>
		<br/>
				<div style="color:red;" id="error"></div>
		</div>
	
	

	<?php echo $form->end(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<?php $i=1; foreach($ticketData as $ticket) : 
						if($i==1){ $lastDeadlineDate = date("d-m-Y",strtotime($ticket['Ticket']['deadline'])); ?>
							<tr><I><td colspan=7 class="deadlinetd"><?php echo "Date :".$lastDeadlineDate." tickets"; ?></I></td> </tr>
							<tr>
								<td class="headtd" width="15%">Title</td>
								<td class="headtd" width="42%">Task</td>
								<td class="headtd" width="10%">Time</td>
								<td class="headtd" width="11%">Type</td>
								<td class="headtd" width="11%">Priority</td>
								<td class="headtd" width="11%">Status</td>
							</tr>
							<?php  
						}
						if($lastDeadlineDate==date("d-m-Y",strtotime($ticket['Ticket']['deadline']))) { ?>
							<?php $colorClass=""; 
							if($ticket['Ticket']['status']==0){ 
							$colorClass = "style='background-color:#E0F2CB'";
							}elseif(($ticket['Ticket']['status']==1 || $ticket['Ticket']['status']==2) && ($ticket['Ticket']['deadline'] > date("Y-m-d H:i:s")) ) {	
								$colorClass = "style='background-color:#C4D9FB'";
							}else{
								$colorClass = "style='background-color:#FBD3B1'";
							}
							?>
							<tr <?php echo $colorClass; ?>>
								<td><?php echo $ticket['Ticket']['title']; ?></td>
								<td><?php echo nl2br($ticket['Ticket']['description']); ?></td>
								<td><?php echo date("H:i",strtotime($ticket['Ticket']['tasktime'])); ?></td>
								<td><?php echo ucfirst($ticket['Ticket']['type']); ?></td>
								<td><?php echo ucfirst($ticket['Ticket']['priority']); ?></td>
								<td><?php if($ticket['Ticket']['status']==1) echo "Open";
										elseif($ticket['Ticket']['status']==0) echo "Closed";
										else echo "Inprogress";
									?>
								</td>
							</tr>
				<?php }else{ $lastDeadlineDate = date("d-m-Y",strtotime($ticket['Ticket']['deadline'])); ?>
				  <tr><td colspan=7 class="deadlinetd"><I><?php echo "Date :".$lastDeadlineDate." tickets"; ?></I></td> </tr>
					<tr>
								<td class="headtd" width="15%">Title</td>
								<td class="headtd" width="42%">Task</td>
								<td class="headtd" width="10%">Time</td>
								<td class="headtd" width="11%">Type</td>
								<td class="headtd" width="11%">Priority</td>
								<td class="headtd" width="11%">Status</td>
					</tr>
					<?php $colorClass=""; 
							if($ticket['Ticket']['status']==0){ 
							$colorClass = "style='background-color:#E0F2CB'";
							}elseif(($ticket['Ticket']['status']==1 || $ticket['Ticket']['status']==2) && ($ticket['Ticket']['deadline'] > date("Y-m-d H:i:s")) ) {	
								$colorClass = "style='background-color:#C4D9FB'";
							}else{
								$colorClass = "style='background-color:#FBD3B1'";
							}
							?>
					<tr <?php echo $colorClass; ?>>
								<td><?php echo $ticket['Ticket']['title']; ?></td>
								<td><?php echo nl2br($ticket['Ticket']['description']); ?></td>
								<td><?php echo date("H:i",strtotime($ticket['Ticket']['tasktime'])); ?></td>
								<td><?php echo ucfirst($ticket['Ticket']['type']); ?></td>
								<td><?php echo ucfirst($ticket['Ticket']['priority']); ?></td>
								<td><?php if($ticket['Ticket']['status']==1) echo "Open";
										elseif($ticket['Ticket']['status']==0) echo "Closed";
										else echo "Inprogress";
									?>
								</td>
					</tr>
				<?php }
				$i++;
				endforeach; ?>
				</table>
				<!--  end product-table................................... --> 
				<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
				<td>
				<?php echo $paginator->prev('� Previous', array('class' => 'page-left'), null, array('class' => 'page-left')); ?>
				<div id="page-info"><?php echo $this->Paginator->counter(array('format' => ' Page<strong> %page%</strong> / %pages%',"id"=>"page-info")); ?></div>
				<?php echo $paginator->next('Next �', array('class' => 'page-right'), null, array('class' => 'page-right')); ?>
				</td>
			</tr>
			</table>
			<!--  end paging................ -->
					<div class="clear"></div>
				
	</td>
	<td>
</td>
</tr>
</table>
			</div>
			<!--  end content-table  -->
	<div class="clear"></div>
	<div align="center" style="display: none;" class="loader" id="imageloader"> 
	<img src="<?php echo BASE_URL;?>app/webroot/images/ajax-loader.gif"/>  
  </div>
</div>