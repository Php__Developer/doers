
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){ 
		$('.tablesorter').tablesorter({
		/* 	headers: { 
						 1: {
						  sorter: false 
						  },
						 2: {
						  sorter: true 
						  },
						  3: {
						  sorter: false 
						  },
						  4: {
						  sorter: false 
						  },
						  5: {
						  sorter: false 
						  },
						  6: {
						  sorter: false 
						  }
						} */
				dateFormat: "uk"
				});
			$(".view_delayed").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".view_onhold").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	900, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".view_alert").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".view_hourly").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	700, 
					'height' :	550, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".billingquickadd").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".billingsquickedit").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	1100, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			}); 
			$(".editproject").fancybox({
					'type': 'iframe',
					'autoDimensions' : true,
					'width' :	650, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
			$(".description").fancybox({
					'type': 'iframe',
					'autoDimensions' : true,
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
	function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}
</script>
<style>
#hide_for_left
{display:none;}
</style>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<div id="error_msg" style="display:none;"></div>
	<div class="leadlbl"> Engineering Lead should update this Current Project Report on daily basis. </div>
	<div class="leftlbl"></div><br/>
	<div class="reportlinks" style="float:right; padding-bottom:20px;">
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Release Delayed",
						array('controller'=>'projects','action'=>'report','delayed'),
						array('class'=>'view_delayed','alt'=>'delayed','style'=>'color:red;')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("On Alert",
						array('controller'=>'projects','action'=>'report','alert'),
						array('class'=>'view_alert','alt'=>'alert','style'=>'color:red')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Hourly Contract",
						array('controller'=>'projects','action'=>'report','hourly'),
						array('class'=>'view_onhold','alt'=>'hourly','style'=>'color:red')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("On Hold",
						array('controller'=>'projects','action'=>'hold_projects'),
						array('class'=>'view_onhold','alt'=>'onhold','style'=>'color:red')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Fixed Contract",
						array('controller'=>'milestones','action'=>'fixedcontract','prefix'=>'milestones'),
						array('class'=>'view_onhold','alt'=>'onhold','style'=>'color:red')
						);
					
				?>
		</div>
		<div style="clear:both;"></div>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<thead>
					<th class="table-header-repeat line-left minwidth-1" width="26%"><a href="#A">Project</a></th>
					<th  class="table-header-repeat line-left" width="9%"><a href="#A">Next Release</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="8%"><a href="#A">Daily Update</a></th>
					<th  class="table-header-repeat line-left"  width="6%"><a href="#A">DropBox</a></th>
					<th  class="table-header-repeat line-left" width="6%"><a href="#A">ERP</a></th>
					<th  class="table-header-repeat line-left" width="7%"><a href="#A">On Alert</a></th>
					<th  class="table-header-repeat line-left minwidth-1" width="24%"><a href="#A">Description</a>
					
					</th>
					
					<th  class="table-header-repeat line-left" width="13%"><a href="#A">Action</a></th>
				</thead>
				<tbody>
				<?php  foreach($resultData as $process):
				//pr($process);  ?>
				<?php	($process['onalert']=="1") ? $class = "class='colorOnalert' " : $class = "" ; 
							
				?>
				<tr <?php echo $class; ?>>
					<?php  echo $this->Form->create('Project',array('url'=>'processaudit','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
					<td><?php echo $process ['project_name']; 
							 echo $this->Form->hidden('id',array('value'=>$process ['id']));
					?> 
					<span class="usr_ip_1">
						  </span>
						  <br/>
						<span id="usr_name_1">
						<?php if($process['Project']['hours']>= 48) { 
							echo "<span style='color:red'>Last updated by ".$process['Last_Updatedby']['first_name']." ".$process['Last_Updatedby']['last_name']." on ".date("d/m/Y",strtotime($process['Project']['modified']))."</span>"; 
						 } else {
							echo "Last updated by ".$process['Last_Updatedby']['first_name']." ".$process['Last_Updatedby']['last_name']." on ".date("d/m/Y",strtotime($process['modified'])); 
						 } ?>
						</span>
					</td>
					<td>
						<?php 
							echo date("d/m/Y",strtotime($process['nextrelease']));
						?>
					</td>
					<td>
					<?php ($process['dailyupdate']=="1") ? $val = "Yes" : $val = "No" ; 
						echo $val;
					?>
					</td>
					<td>
					<?php ($process['dropbox']=="1") ? $val = "Yes" : $val = "No" ; 
						echo $val;
					?>
					</td>
					<td>
					<?php ($process['vooap']=="1") ? $val = "Yes" : $val = "No" ; 
						echo $val;
					?>
					</td>
					<td>
					<?php ($process['onalert']=="1") ? $val = "Yes" : $val = "No" ; 
						echo $val;
					?>
					</td>
					
					
					<!--by kailash-->
					<td>
					<?php 
					
					echo $this->Html->link("show",
							array('controller'=>'projects','action'=>'description',$process['id']),
								array('class'=>'description','title'=>'Show Details','target'=>'_blank'));
							
							//echo $process['Project']['processnotes'];	 
					?>
					</td>
					<!--by kailash end-->
					<td>
					<input type="hidden" value="<?php echo $process['id'];?>" class="billing_id">
					<?php
							$left = $process['Project']['totalamount']-$process['Project']['total_payment']; 
							if($left<0){ $left_hide="hide_for_left";
										 $left_text="Negative Balance Contact Admin";
										}else
										{ $left_hide="";$left_text="";
										}
						if($process['engagagment']=="hourly" && $process['verify']=="1") {
							
											echo $this->Html->link("",
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings',$process['id']),
											array('class'=>'info-tooltip billingquickadd','id'=>$left_hide,'title'=>'Add Billing')
										);
								} elseif($process['engagagment']=="hourly"  && $process['verify']=="0") {	
								//echo "verify is 0"	;
								$id = $process['id'];
								echo $this->Html->link("","javascript:void(0)",
							array('class'=>'info-tooltip verifybillingimg','title'=>'verify Billing','onClick'=>'veryfycheck()')
									);
								//}
						}
						echo $this->Html->link("",
								array('controller'=>'billings','action'=>'quickview','prefix'=>'billings',$process['id']),
								array('class'=>'info-tooltip billingsquickedit','id'=>$left_hide,'title'=>'View Billings')
						);


						echo $this->Html->link("",
								array('controller'=>'projects','action'=>'editprocessaudit',$process['id']),
								array('class'=>'info-tooltip editproject','id'=>$left_hide,'title'=>'Edit','target'=>'_blank')
							);
						
						echo $this->Html->link("",
								array('controller'=>'projects','action'=>'projectdetails','id'=>$process['id']),
								array('class'=>'info-tooltip go','title'=>'Go','id'=>$left_hide,'target'=>'_blank')
							);
					?>
					
					<span style="color:red"><?php echo $left_text;?></span>
					</td>
					<?php echo $this->Form->end(); ?>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
				<!--  end product-table................................... --> 
					<div class="clear"></div>
	</td>
	<td>
</td>
</tr>
</table>
			</div>
			<!--  end content-table  -->
	<div class="clear"></div>
</div>