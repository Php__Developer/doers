
<?php echo  $this->fetch('scriptBottom');?>
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<?php $id  = $this->request->params['id'];?>
<div class="row">
		<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a>
	<?php echo $this->Form->create('Project',array('url' => ['action'=>'projectdetails'],'id'=>'adminreport','method'=>'POST', "class"=>"login")); ?>
		<div class="searching_div">
		<?php $ProjectsData = $projectData[0];  ?>
			<div class="row m-b-20">
				<center><h2 class="text-danger">Project : <?php echo $ProjectsData['project_name'];?></h2></center>
			</div>	
			<div class="row m-b-20 m-l-30">
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Responsible</strong> <br>
                    <p class="text-muted"><?php echo $ProjectsData['ProjectManager']['first_name']." ".$ProjectsData['ProjectManager']['last_name']; ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Accountable</strong> <br>
                    <p class="text-muted"><?php echo $ProjectsData['Engineer_User']['first_name']." ".$ProjectsData['Engineer_User']['last_name']; ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Consultant</strong> <br>
                    <p class="text-muted"><?php echo $ProjectsData['Salefront_User']['first_name']." ".$ProjectsData['Salefront_User']['last_name']; ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6"> <strong>Informer</strong> <br>
                    <p class="text-muted"><?php echo $ProjectsData['Saleback_User']['first_name']." ".$ProjectsData['Saleback_User']['last_name']; ?></p>
                  </div>
          </div>
          <div class="row m-b-20 m-l-30">
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Next Release</strong> <br>
                    <p class="text-muted"><?php echo date("d M,Y",strtotime($ProjectsData['nextrelease'])); ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Last Release</strong> <br>
                    <p class="text-muted"><?php echo date("d M,Y",strtotime($ProjectsData['finishdate'])); ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6 b-r"> <strong>Client Name</strong> <br>
                    <p class="text-muted"><?php echo $ProjectsData['Contact']['name']; ?></p>
                  </div>
                  <div class="col-md-3 col-xs-6"> <strong>Engagement</strong> <br>
                    <p class="text-muted"><?php echo ucfirst($ProjectsData['engagagment']) ;?></p>
                  </div>
          </div>

				<div class="text-danger" id="error"></div>
		</div> <!--SEARCH DIV-->	
 
</div>
<div class="row">
<input type="hidden" name="pid" value="<?php echo $id; ?>" class="attached_to">
<input type="hidden" name="entity_type1" value="2" class="entity_id">
<input type="hidden" name="entity_type" value="2" class="entity_type">
<input type="hidden" name="entity_id" value="<?php echo $id; ?>" class="entity_id_original">
<input type="hidden" name="orderby" value="<?php echo $orderby;?>" class="orderby">
<input type="hidden" name="showopen" value="<?php echo $showopen;?>" class="showopen">
<input type="hidden" name="showclosed" value="<?php echo $showclosed;?>" class="showclosed">
<input type="hidden" name="showinprogress" value="<?php echo $showinprogress;?>" class="showinprogress">
<input type="hidden" name="task_type" value="To-Do" class="task_type">

	<div class="white-box">
		<section class="pdtabs">
                  <div class="sttabs tabs-style-bar">
                    <nav>
                      <ul>
                        <li class="tab-current"><a href="#section-linemove-1" class="section-linemove-1 sticon fa fa-tasks" data-type="tasks"><span>Tasks</span></a></li>
                        <li class=""><a href="#section-linemove-2" class="section-linemove-2 sticon fa fa-list-ol milestonestab" data-type="milestones"><span>Milestones</span></a></li>
                        <li class=""><a href="#section-linemove-3" class="section-linemove-3 sticon fa fa-file-text-o" data-type="documents"><span>Documents</span></a></li>
                        <li class=""><a href="#section-linemove-4" class="section-linemove-4 sticon fa fa-money paymentstab" data-type="payments"><span>Payments</span></a></li>
                        <li class=""><a href="#section-linemove-5" class="section-linemove-5 sticon fa fa-list-ul" data-type="notes"><span>Notes</span></a></li>
                        <li class=""><a href="#section-linemove-6" class="section-linemove-6 sticon fa fa-list-ul logstab" data-type="logs"><span>Logs</span></a></li>
                      </ul>
                    </nav>
                    <div class="content-wrap ">
                      <!-- Section 1 Started --><section id="section-linemove-1" class="content-current">
                      <div id="content-table-inner">
	<?php echo $this->Form->end(); ?>
	  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-5" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<div class="row m-b-20">
				
				<span>
					<span class="text-info col-md-2">
					<?php
					if($showopen == 'yes'){
						$showopenchecked = "true";
					}else{
						$showopenchecked = "";
					} ?>				
					<?php	echo $this->Form->input("check_it",array('data-classname'=> 'showopen','type'=>'checkbox','id'=>'',"label"=>false,"div"=>false,"hiddenField"=>false,"value"=>"descending","checked"=>$showopenchecked,"title"=>"Change order","class"=>"ticcond"));
					?>
					<label for="check_it" class="m-l-20 m-t-5 text-primary font-bold">Open</label>


					</span>
					<span class="text-info col-md-2">
					<?php
					if($showclosed == 'yes'){
						$showclosedchecked = "true";
					}else{
						$showclosedchecked = "";
					} ?>				
					<?php	echo $this->Form->input("check_it",array('data-classname'=> 'showclosed','type'=>'checkbox','id'=>'',"label"=>false,"div"=>false,"hiddenField"=>false,"value"=>"descending","checked"=>$showclosedchecked,"title"=>"Change order","class"=>"ticcond"));
					?>
					<label for="check_it" class="m-l-20 m-t-5 text-primary font-bold">Closed</label>


					</span>

					<span class="text-info col-md-2">
					<?php
					if($showinprogress == 'yes'){
						$showinprogresschecked = "true";
					}else{
						$showinprogresschecked = "";
					} ?>				
					<?php	echo $this->Form->input("check_it",array('data-classname'=> 'showinprogress','type'=>'checkbox','id'=>'',"label"=>false,"div"=>false,"hiddenField"=>false,"value"=>"descending","checked"=>$showinprogresschecked,"title"=>"Change order","class"=>"ticcond"));
					?>
					<label for="check_it" class="m-l-20 m-t-5 text-primary font-bold">In Progress</label>
					</span>
				</span>




				<span class="pull-right">
					<div class="col-md-4 m-t-5">
					<button class="btn btn-info btn-circle btn-sm addtask" data-id="<?php $id ;?>" data-type="To-Do" title='Add new task'><i class='fa fa-plus'></i></button>
					<?php 
						 $this->Html->link("<i class='fa fa-plus'></i>",
							array('controller'=>'Tickets','action'=>'add','prefix'=> 'tickets'),
							array('escape' => false , 'class'=>'btn btn-info btn-circle btn-sm addtask','alt'=>'delayed','title' => 'Add new task','data-id' => $id ,'data-type' => 'To-Do')
						);	?>	
					</div>
					<div class="col-md-2">
							<?php 
						if($orderby == 'asc'){
							$checked = "true";
							$labelTxt  = "Ascending";
						}else{
							$checked = "";
							$labelTxt  = "Descending";
						} ?>				
						<?php	echo $this->Form->input("check_it",array('type'=>'checkbox','id'=>'changeOrder',"label"=>false,"div"=>false,"hiddenField"=>false,"value"=>"descending","checked"=>$checked,"title"=>"Change order","class"=>""));
						?>
						<label for="check_it" class="m-l-20 m-t-5 text-primary font-bold"><?php echo $labelTxt; ?></label>	
					</div>
				
				
				</span>		


			</div>
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">

				<?php $i=1; foreach($ticketData as $ticket) : 
						if($i==1){ $lastDeadlineDate = date("d-m-Y",strtotime($ticket['deadline'])); ?>
							<tr><I><td colspan=7 class="deadlinetd text-warning pull-left"><?php echo "Date :".$lastDeadlineDate." tickets"; ?></I></td> </tr>
							<tr>
								<td class="headtd" width="14%">Title</td>
								<td class="headtd" width="42%">Task</td>
								<td class="headtd" width="9%">Time</td>
								<td class="headtd" width="10%">Type</td>
								<td class="headtd" width="10%">Priority</td>
								<td class="headtd" width="10%">Status</td>
								<td class="headtd" width="5%">Details</td>
							</tr>
							<?php  
						}
						if($lastDeadlineDate==date("d-m-Y",strtotime($ticket['deadline']))) { ?>
							<?php $colorClass=""; 

							if($ticket['status']==0){ 
							$colorClass = "style='color:#00c292'";
							}elseif(($ticket['status']==1 || $ticket['status']==2) && (date_create($ticket['deadline'])->format("YmdHis") >= date("YmdHis")) ) {	
								$colorClass = "style='color:#03a9f3'";
							}else{
								$colorClass = "style='color:#fb9678'";
							}
							?>
							<tr <?php echo $colorClass; ?>>
								<td><?php 
									 $ticket['status'];
								 date_create($ticket['deadline'])->format("YmdHis")."<br>";
								 date("YmdHis");
								echo $ticket['title']; ?></td>

								<td><?php echo nl2br($ticket['description']); ?></td>
								<td><?php echo date("H:i",strtotime($ticket['tasktime'])); ?></td>
								<td><?php echo ucfirst($ticket['type']); ?></td>
								<td><?php echo ucfirst($ticket['priority']); ?></td>
								<td><?php if($ticket['status']==1) echo "Open";
										elseif($ticket['status']==0) echo "Closed";
										else echo "Inprogress";
									?>
								</td>
								<td><?php 
                                           echo $this->Html->link(
                                              '<i class="fa fa-cog text-muted"></i>',
                                            array('controller'=>'tickets','action'=>'ticketdetail','prefix'=>'tickets',$ticket['id']),
                                             ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit']
                                             );?>

								</td>
							</tr>
				<?php }else{ $lastDeadlineDate = date("d-m-Y",strtotime($ticket['deadline'])); ?>
				  <tr><td colspan=7 class="deadlinetd text-warning pull-left"><I><?php echo "Date :".$lastDeadlineDate." tickets"; ?></I></td> </tr>
					<tr>
								<td class="headtd" width="14%">Title</td>
								<td class="headtd" width="42%">Task</td>
								<td class="headtd" width="9%">Time</td>
								<td class="headtd" width="10%">Type</td>
								<td class="headtd" width="10%">Priority</td>
								<td class="headtd" width="10%">Status</td>
								<td class="headtd" width="5%">Details</td>
					</tr>
					<?php $colorClass=""; 
							if($ticket['status']==0){ 
							$colorClass = "style='color:#00c292'";
							}elseif(($ticket['status']==1 || $ticket['status']==2) && (date_create($ticket['deadline'])->format("YmdHis") >= date("YmdHis") ) ) {	
								$colorClass = "style='color:#03a9f3'";
							}else{
								$colorClass = "style='color:#fb9678'";
							}
							?>
					<tr <?php echo $colorClass; ?>>
								<td><?php echo $ticket['title']; ?></td>
								<td><?php echo nl2br($ticket['description']); ?></td>
								<td><?php echo date("H:i",strtotime($ticket['tasktime'])); ?></td>
								<td><?php echo ucfirst($ticket['type']); ?></td>
								<td><?php echo ucfirst($ticket['priority']); ?></td>
								<td><?php if($ticket['status']==1) echo "Open";
										elseif($ticket['status']==0) echo "Closed";
										else echo "Inprogress";
									?>
								</td>
								<td><?php 
                                           echo $this->Html->link(
                                              '<i class="fa fa-cog text-muted"></i>',
                                            array('controller'=>'tickets','action'=>'ticketdetail','prefix'=>'tickets',$ticket['id']),
                                             ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit']
                                             );?>
								</td>
					</tr>
				<?php }
				$i++;
				endforeach; ?>
				</table>
				<div class="col-md-12">
					<?php
                       echo $this->Html->link(
                      'Report a bug in this Project',
                      array('controller'=>'tickets','action'=>'add','prefix'=>'tickets',
                      		'?'=>['tag' => 'Bugs','project_id' => $id],
                      ),
                      ['escape' => false,'class' => 'btn default btn-outline manage','title'=>'Create A Bug Ticket','target'=>'_blank']
                      );
                    
                    ?>
				</div>
				<!--  end product-table................................... --> 
				<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>

				     <td>
				           <ul class="pagination paginate-data">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
                        </td>


			
			</tr>

			</table>
			<!--  end paging................ -->
					<div class="clear"></div>
				
	</td>
	<td>
</td>
</tr>
</table>
			</div> <!--content-table-inner END-->
                      </section><!-- Section 1 Ended -->
                      <!-- Section 2 Started --><section id="section-linemove-2" class="">
                      									<!-- <h2>Comming Soon.</h2> -->
                      									<div class="row">
							                      			<div class="white-box">
								                      			<div class="col-md-12 p-r-30">
								                      			<span>
								                      				<?php
								                      					if($ProjectsData->engagagment == 'hourly'){
								                      						echo $this->Html->link("<i class='fa fa-plus'></i>",
																			array('controller'=>'Billings','action'=>'quickadd','prefix'=> 'billings','id' => $id),
																			array('escape' => false , 'class'=>'popup_window btn btn-info btn-circle btn-sm pull-right m-r-10 m-l-10 billingspopup','alt'=>'delayed')
																			);			
								                      					}
								                      				 
																	?>

								                      				<?php echo $this->Html->link("<i class='fa fa-refresh'></i>",
																	array('controller'=>'Billings','action'=>'quickadd','prefix'=> 'billings','id' => $id),
																	array('escape' => false , 'class'=>'btn btn-info btn-circle btn-sm pull-right m-r-10 m-l-10 reloadmilestones','alt'=>'delayed')
																	);	
																	?>	
								                      			</span>
																
								                      				<!-- <button type="button" class="btn btn-info btn-circle btn-sm pull-right addnotesbtn m-r-30" title="Add Payment"><i class="fa fa-plus"></i> </button> -->
								                      			</div>
							                      			</div>
							                      			<div class="white-box milestoneswrapper">
							                      				<!--
							                      				HERE PAYMENTS DYNAMIC CONTENT IS LOADED THROUGH AJAX
							                      				PLEASE SEE projectdetails.js FILE FOR FURTHER INFORMATION
							                      				-->
							                      			</div>
							                      		</div>	

                      							</section>

                      <!-- Section 2 Ended -->
                      <!-- Section 3 Started --><section id="section-linemove-3" class="">
                      		<div class="row">
                      		<div class="col-md-12 p-r-30">
                      			<button type="button" class="btn btn-info btn-circle btn-sm pull-right addfiles m-r-30" title="Upload New Files"><i class="fa fa-plus"></i> </button>
                      		</div>
                      		
					     <div class="uploadwrapper default_hidden">
					     <div class="row">
					        <div class="col-sm-12">
					          <div class="white-box" >
					          <div class="upldarea dragandrophandler" id="dragandrophandler">
					          	<center><label for="input-file-now-custom-2" id="inputlebel" class="text-center p-t-30 m-b-0"> Click here or Drop a file here to upload!</label></center>
					          </div> 
					            
					          </div>
					          <input type="file" id="input-file-now-custom-2" class="brwsefile" data-height="500" />
					          </div>
					          </div>
					          <div class="row uploadprogress m-t-20">
					            <div class="white-box m-l-20 m-r-5">
					              <div class="row default_hidden prgsswrapper">
					                   <div class="col-sm-2">
					                      Upload Status : <span class="upldprgrsprcnt">0</span>%
					                  </div>
					                  <div class="col-sm-8">
					                  <div class="progress">
					                    <div class="progress progress-striped active">
					                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">  </div>
					                      </div>
					                  </div>
					                </div>
					                <div class="col-sm-2">
					                    <span class="donefiles">0</span> uploaded of total <span class="totalfiles">0</span> files
					                </div>
					              </div>
					            </div>
					          </div>
					     </div>
					          <div class="row">
					            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mail_listing"> -->
					            <a href="#" class="reloaddocs">Reload Docs</a>
					            <div class="resulssets">
					            		  <?php foreach($docs as $doc){ ;?>
					               		<div class="col-md-4 singledoc">
											<div class="white-box">
							                	<div class="col-md-12">
							                	<a href="<?php echo $doc['name'];?>" class="deldoc pull-right"><i class="fa fa-times"></i></a>
							                		<div class="col-md-12 m-t-5 text-center">
							                		<?php 
							                		$icon = "";

							                		if($doc['type'] == 'application/vnd.oasis.opendocument.spreadsheet' || $doc['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){ 
							                			$icon =  'fa-file-excel-o';
							                			$title = 'Excel';
							                		} else if(in_array($doc['type'],['image/jpg','image/png','image/jpeg'])){ 
							                			$icon =  'fa-file-image-o';
							                			$title = 'Image';
							                		  } else if( $doc['type'] == 'application/pdf' ) { 
							                		  	$icon =  ' fa-file-pdf-o';
							                		  	$title = 'Pdf';
							                		  } else if( $doc['type'] == 'application/msword' || $doc['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {  
							                		  	$icon =  'fa-file-word-o';
							                		  	$title = 'Word';
							                		  } else if( $doc['type'] == 'application/zip' ) {  
							                		  	$icon =  'fa-file-zip-o';
							                		  	$title = 'Zip';
							                		  } else if( $doc['type'] == 'application/vnd.ms-excel' || $doc['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {  
							                		  	$icon =  'fa-file-excel-o';
							                		  	$title = 'Excel';
							                		  } else if($doc['type'] == 'application/x-rar'){
							                		  	$icon =  'fa-file-zip-o';
							                		  	$title = 'Rar';
							                		  } else if($doc['type'] == 'text/plain'){
							                		  	$icon =  'fa-file-text-o';
							                		  	$title = 'Text';
							                		  };
							                		  ;?>

							                		  <i class="fa <?php echo $icon;?> bigfa" title="<?php echo $title ;?>"></i>
							                		</div>
							                		<div class="col-md-12 m-t-5 text-center">
							                		  <div class="row">
							                		  	<div class="font-bold orgnalname"><a href="https://doersdocs.s3.amazonaws.com/projects/<?php echo $doc['name'];?>" target ="_blank"><?php echo $doc['original_name'];?></a> </div>
							                		  </div>		
							                		  <div class="row">
							                		  	<div class="addedby">Added By:<?php echo $doc['Creater']['first_name'].' '.$doc['Creater']['last_name'];?> on <?php echo $doc['created'];?></div>
							                		  </div>
							                		  <div class="row">
							                		  	<div class="addcmnt">
							                		  	<a href="#" class="adddes" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($doc['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Description">
							                		  			<?php echo !empty($doc['description']) ? $doc['description']: 'Add Comment';?>
							                		  	</a>
							                		  	</div>
							                		  </div>
							                		</div>
							                	</div>
							                	<div class="col-md-6">
							                			
							                	</div>
											</div>
											<div class="editbox">
											</div>
										</div>    
										
					                <?php } ?>

					            </div>
					           
					                <div class="row">
					                 <!--  <div class="col-xs-7 m-t-20"> Showing 1 - 15 of 200 </div> -->
					                  <div class="col-xs-5 m-t-20">
					                    <div class="btn-group pull-right">
					                      <!-- <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
					                      <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button> -->
					                    </div>
					                  </div>
					                </div>
					              <!-- </div> -->
					          </div>
					      </div>
                      </section><!-- Section 3 Ended -->
                      <!-- Section 4 Started -->
                      <section id="section-linemove-4" class="">
                      		<div class="row">
                      			<div class="white-box">
	                      			<div class="col-md-12 p-r-30">
	                      			<span>
	                      				<?php echo $this->Html->link("<i class='fa fa-plus'></i>",
										array('controller'=>'Payments','action'=>'quickadd','prefix'=> 'payments','id' => $id),
										array('escape' => false , 'class'=>'popup_window btn btn-info btn-circle btn-sm pull-right m-r-10 m-l-10','alt'=>'delayed')
										);	
										?>

	                      				<?php echo $this->Html->link("<i class='fa fa-refresh'></i>",
										array('controller'=>'Payments','action'=>'quickadd','prefix'=> 'payments','id' => $id),
										array('escape' => false , 'class'=>'btn btn-info btn-circle btn-sm pull-right m-r-10 m-l-10 reloadpayments','alt'=>'delayed')
										);	
										?>	
	                      			</span>
									
	                      				<!-- <button type="button" class="btn btn-info btn-circle btn-sm pull-right addnotesbtn m-r-30" title="Add Payment"><i class="fa fa-plus"></i> </button> -->
	                      			</div>
                      			</div>
                      			<div class="white-box paymentswrapper">
                      				<!--
                      				HERE PAYMENTS DYNAMIC CONTENT IS LOADED THROUGH AJAX
                      				PLEASE SEE projectdetails.js FILE FOR FURTHER INFORMATION
                      				-->
                      			</div>
                      		</div>
                      </section><!-- Section 4 Ended -->
                      <!-- Section 5 Started --><section id="section-linemove-5" class="">
                      	<div class="row">
                      		<div class="white-box">
                      			<div class="col-md-12 p-r-30">
                      			<button type="button" class="btn btn-info btn-circle btn-sm pull-right addnotesbtn m-r-30" title="Upload New Files"><i class="fa fa-plus"></i> </button>
                      			</div>
                      		</div>
                      	</div>
                      	<div class="row noteswrapper default_hidden">
                      	<div class="col-md-12">
                      			<div class="form-group">
				                <label class="col-md-12 pull-left">Title of <span class="help">the Note</span></label>
				                <div class="col-md-12 titlewrapper">
				                  <input class="form-control notetitle" value="Untitled" type="text">
				                  <div class="originwrapper">
				                  	
				                  </div>
				                </div>
				              </div>
                      	</div>
					        <div class="col-sm-12">
					          <div class="white-box">

					            <form method="post">
					              <div class="form-group">
					                <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea>
					              </div>
					            </form>
					          </div>
					        </div>
					      </div>
					      <div class="row noteswrapper default_hidden">
					      	<div class="col-md-12">
					      		<div class="col-lg-2 col-sm-4 col-xs-12">
				                	<button class="btn btn-block btn-outline btn-info addnote">Add Note</button>
				                	<button class="btn btn-block btn-outline btn-info editnotebtn default_hidden">Edit Note</button>
				              </div>
					      	</div>
					      </div>
					      	<div class="row">
					            <a href="#" class="reloadnotes default_hidden">Reload notes</a>
					            <div class="noteresulssets">
					            		  <?php foreach($notes as $note){ ;?>
					               		<div class="col-md-4 singlenote ">
					               		<a href="<?php echo $this->General->ENC($note['id']) ;?>" class="delnote pull-right  m-l-5"><i class="fa fa-times"></i></a>
					               		<a href="<?php echo $this->General->ENC($note['id']) ;?>" class="editnote pull-right"><i class="fa fa-pencil"></i></a>
											<div class="white-box">
												<div class="notemainwrapper p-10">
							                	<div class="row">
							                		<div class="col-md-12">
							                			<h4 class="font-bold"><?php echo $note['title'] ;?></h4>
								                	</div>
								                	<div class="col-md-12 maincontent">
								                		 <?php echo $note['content'] ;?>		
								                	</div>
							                	</div>
							                	 </div>
							                	 <div class="row text-center">
						                		  	<div class="addedby">Added By:<?php echo $note['Creater']['first_name'].' '.$note['Creater']['last_name'];?> on <?php echo $note['created'];?></div>
						                		  </div>
						                		  <div class="row text-center">
						                		  	<div class="addcmnt">
						                		  	<a href="#" class="addnotcmnt" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($note['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Description">
						                		  			<?php echo !empty($note['description']) ? $note['description']: 'Add Comment';?>
						                		  	</a>
						                		  	</div>
						                		  </div>
						                		 
											</div>
										</div>    
										
					                <?php } ?>

					            </div>
					           
					                <div class="row">
					                 <!--  <div class="col-xs-7 m-t-20"> Showing 1 - 15 of 200 </div> -->
					                  <div class="col-xs-5 m-t-20">
					                    <div class="btn-group pull-right">
					                      <!-- <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
					                      <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button> -->
					                    </div>
					                  </div>
					                </div>
					              <!-- </div> -->
					          </div>




                      </section><!-- Section 5 Ended -->
                      <section id="section-linemove-6" class="">
                      	<div class="row">
                      		<div class="white-box">
                      			<div class="col-lg-12 logswrapper">
                      				
                      			</div>
                      		</div>
                      	</div>
                      </section>
                    </div><!-- /content -->
                  </div><!-- /tabs -->
                </section>
	</div>	
</div>




	
			<!--  end content-table  -->
	<div class="clear"></div>
	<input type="hidden" name="page" value="projectdetails" class="currentpage">
	<input type="hidden" name="base_loc" value="<?php echo $this->Url->build('/', true); ?>" class="base_loc">
	
	<div align="center" style="display: none;" class="loader" id="imageloader"> 
	<img src="<?php echo BASE_URL;?>app/webroot/images/ajax-loader.gif"/>  
  </div>
</div>


<script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
<script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.min.js"></script>

<!-- <script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script> -->
<script src="<?php echo BASE_URL; ?>js/projectdetails.js"></script>

<script src="<?php echo BASE_URL; ?>js/cbpFWTabs.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet"> -->
<?php $this->Html->script(array('uploader','ajaxupload.3.5')); ?>
<script type="text/javascript">
	/*function uploadFile(){
		if(getConfirmation("Are you sure you want to import file?")){
		$('[name=file_to_upload]').click();
		}
		}
	*/
	$(document).ready(function(){ 

	
		

			 //fancybox settings
			/*$(".editproject").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});*/
			$("#changeOrder").click(function(){
			 if ($('#changeOrder').is(':checked')){ 
					//$('#changeOrder').val('ascending');
					var orderval = $('.orderby').val('asc');
						var url = document.URL;
						$('#adminreport').attr('action',url);
				}else{ 
						var orderval = $('.orderby').val('desc');
						var url = document.URL;
						$('#adminreport').attr('action',url);
						
				}
				$('#adminreport').submit();
			});
	});
</script>