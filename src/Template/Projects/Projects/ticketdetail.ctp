<center>
	<table width="100%">
	<h3>Ticket Details</h3>
	<tr>
		<td width="40%"><b>Ticket Number:</b></td>
		<td width="70%"><?php 
		echo $resultData['id']; ?></td>
	 </tr>
	 <tr>
		<td width="40%"><b>Aligned To:</b></td>
		<td width="70%"><?php echo $resultData['User_To']['first_name']." ".$resultData['User_To']['last_name']; ?></td>
	 </tr>
	 <tr>
		<td width="40%"><b>Aligned From:</b></td>
		<td width="70%"><?php echo $resultData['User_From']['first_name']." ".$resultData['User_From']['last_name']; ?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>Title:</b></td>
		<td width="70%"><?php echo $resultData['title']; ?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>Type:</b></td>
		<td width="70%"><?php echo ucfirst($resultData['type']); ?></td>
	 </tr>
	 </tr>
	 <tr>
		<td width="40%"><b>Status:</b></td>
		<td width="70%"><?php if($resultData['status']==1){
				echo "Open";
			}elseif($resultData['status']==0){
				echo "Closed";
			}else{
				echo "Inprogress";
			}?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>Tasktime:</b></td>
		<td width="70%"><?php echo $resultData['tasktime']; ?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>Billable Hours:</b></td>
		<td width="70%"><?php echo $resultData['billable_hours']; ?></td>
	 </tr>
	 <tr>
		<td width="40%"><b>Deadline:</b></td>
		<td width="70%"><?php echo date_create($resultData['deadline'])->format("d/m/Y h:i a") ;?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>IP Address:</b></td>
		<td width="70%"><?php echo $resultData['ipaddress']; ?></td>
	 </tr>
	  <tr>
		<td width="40%"><b>Description:</b></td>
		<td width="70%"><?php echo $resultData['description']; ?></td>
	 </tr>
	   <tr>
		<td width="40%"><b>Response:</b></td>
		<td width="70%"><?php echo $resultData['response']; ?></td>
	 </tr>
	</table>
</center>