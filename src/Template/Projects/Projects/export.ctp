<?php
	$header = array();
	$datas = array();
	$_header = ['Title', 'Task', 'Time','Billable Hours','Type','Priority','Status','Deadline'];
$this->set(compact('data', '_serialize', '_header'));

	$csv->addRow($header);
	$i = 0;
	$line1 = array();
	foreach($ticketData as $ticket){
			//$datas[$i]['Ticket'][] = $i+1;
			$datas[$i][] = $ticket['title'];
			$datas[$i][] = $ticket['description'];
			$datas[$i][] = $ticket['tasktime'];
			$datas[$i][] = $ticket['billable_hours'];
			$datas[$i][] = $ticket['type'];
			$datas[$i][] = $ticket['priority'];
			if($ticket['status'] == '1'){
				$datas[$i][] = "Open";
			}
			elseif($ticket['status'] == '0'){
				$datas[$i][] = "Closed";
			}else{
				$datas[$i][] = "Inprogess";
			}
			$datas[$i][] = date("d-m-Y",strtotime($ticket['deadline']));
			$line1 = $datas;
			$i++;
	}
	foreach ($line1 as $data){
		$lines = $data['Ticket'];
		$csv->addRow($lines);
	}
	$projectName= str_replace(" ","",$ticketData[0]['project_name']); 
	$filename= "milestone_".$projectName."_V1.0";
	echo $csv->render($filename);
?>