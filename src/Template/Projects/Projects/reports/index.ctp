<?php echo $javascript->link(array('jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js')); ?>
<?php echo $this->Html->css(array("jquery.fancybox-1.3.4.css")); ?>
   <?php echo $this->Html->css(array("reports.css")); ?>	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
		<body>
		
		<div class="wrapper-ab">
            <div class="main-ab">
            <div class="heading-ab">Admin Reports - For HR Department</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/appraisals/salaryreport/';?>">Salary Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/leadreport1/';?>">Lead Report (Date Wise)</a></div></div>
			<div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/attendances/leave_report/';?>">Leave Report (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/evaluations/performance_report/';?>">Performance Report (Date Wise)</a></div></div>
            <div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/evaluations/performancereport_month/';?>">Performance Report (User / Month Wise)</a></div></div>
		    <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/learning_centers/learningcenterreport_user/';?>">Learning Center Report (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/tickets/ticketreport_date/';?>">User Ticket Report (Date Wise)</a></div></div>
			<div class="cleaner"></div>
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Finance Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/billings/billingreport_week/';?>">Week Wise Billing Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/payments/paymentreport_week/';?>">Week Wise Payment Report</a></div></div>
			<div class="cleaner"></div>
            
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Sales Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/bidspiechart_report/';?>">Bidding Report - (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/activelead_report/';?>">No of Active Leads (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/winclose_report/';?>">No of Win / Closed Projects (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/overleadcountuser_report/';?>">Overall Lead Count along with Status (User Wise)</a></div></div>
			<div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/companywidelead_report/';?>">Company Wide Line Graph - (Month Wise)</a></div></div>
             <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/leads/activelead/';?>">Current Active Leads</a></div></div>
                        <div class="box-ab"><div class="graf-ab"><a class="addLinks fancybox" href="<?php echo BASE_URL.'admin/projects/holdProjects/';?>">On Hold Projects</a></div></div>
           
            <div class="cleaner"></div>
            </div>

    </div>
</body>
</html>