<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>

<?php 
    echo  $this->Html->script(array('jquery-3'));
?>
<?php echo $this->Html->css(array("listing","jquery-1.4.1.min")); ?>
<?php 
    echo  $this->Html->script(array('jquery-3',"listing"));
?>
<script type="text/javascript">
$(document).ready(function(){
     var base_url = "<?php echo $this->Url->build('/', true); ?>";
     
     $('.statusselect').on('change',function(){
     	var id =$(this).closest('td').find('.project_id').val();

     	var old_status =$(this).closest('td').find('.thestatus').val();
     	if($('#projectStatus'+id).val() != "onhold"){
			if(getConfirmation("Are you sure to change project's status?")){
				var status = $('#projectStatus'+id).val();
				//var id = $('#project_id'+id).val();
				console.log(old_status);
				console.log(status);
				if(status == 'closed'){
					var msg = "Are you want to deactivate this Project client?"
					confirmAction(msg,"deact",id);
				}else if(status =="current" && (old_status == "closed")){
					var msg = "Are you want to activate this Project client?"
					confirmAction(msg,"act",id);
				}
				var client_update = $('#client_update'+id).val();
				console.log(client_update);
				var client_id =  $(this).closest('td').find('.client_id').val();
				$('#tri_'+id).remove();
				if($.active>0){
				}else{
					form_data = {'id':id,'status':status,'client_update':client_update,'client_id':client_id}
					console.log(form_data);
					$.post(base_url+'/projects/projectStatusChangebyAjax/',form_data,function(data){
						if(data==1){
							//window.parent.$('#close-fb').click();
							$(this).closest('td').find('.thestatus').val(status);


							$('#showsuccess').html('Project status updated successfully.');
							$('#showsuccess').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
							setTimeout(function() { $('#showsuccess').fadeOut('slow'); }, 1000);
						}
					});
				}
			}
		}

     });

function confirmAction(msg,key,id){
	console.log(msg);
			var action = confirm(msg);
			if(action == true){
				$('#client_update'+id).val(key);
			}
				return true;		
	}

	
});
function confirmAction(msg,key,id){
	console.log(msg);
			var action = confirm(msg);
			if(action == true){
				$('#client_update'+id).val(key);
			}
				return true;		
	}
</script>

<!--  start content-table-inner -->
	
			 <?php $this->Flash->render(); ?>
			<center><span class="font-bold text-success"> On Hold Projects </span></center>
			<div id="showsuccess" style="display:none;"></div>

				  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
					   <thead>
                <tr>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Name</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Client</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Salesfront</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">Salesback</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Resource</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Left</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Process-notes</button></th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Notes</button></th>
                     <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Status</button></th>
                 
                </tr>
              </thead>
              <tbody>
		
				<?php $i = 1; if(count($resultData)>0){ 
				foreach($resultData as $projects): 

				//($projects); die;
			?>
				<tr id="<?php echo "tri_".$projects['Project']['id']; ?>">
					<td><?php  $str = strip_tags($projects['project_name']);  echo $name = substr($str,0,18).((strlen($str)>18)?"...":"");   ?></td>
					<td><?php  $str = strip_tags($projects['Contact']['name']);  echo $name = substr($str,0,18).((strlen($str)>18)?"...":"");   ?></td>
					<td><?php  $str = strip_tags($projects['Salefront_User']['first_name'].' '.$projects['Salefront_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php $str = strip_tags($projects['Saleback_User']['first_name'].' '.$projects['Saleback_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php  $str = strip_tags($projects['Engineer_User']['first_name'].' '.$projects['Engineer_User']['last_name']);  echo $name = substr($str,0,15).((strlen($str)>15)?"...":""); ?></td>
					<td><?php  echo $left = (($projects['totalamount']) - ($projects['total_payment'])); ?></td>
					<td><?php $str = strip_tags($projects['processnotes']);  echo $name = substr($str,0,22).((strlen($str)>22	)?"...":"");  ?></td>
					<td><?php $str = strip_tags($projects['notes']);  echo $name = substr($str,0,22).((strlen($str)>22	)?"...":"");  ?></td>
					<td>
					<input type="hidden" class="thestatus" value="<?php echo $projects['status'];?>">
					<input type="hidden" class="project_id" value="<?php echo $projects['id'];?>">
					<input type="hidden" class="client_id" value="<?php echo $projects['client_id'];?>">
					<?php //echo $projects['status'];?><?php
					$pid = $projects['id'];
				   $options=array('current' => 'Current', 
								  'onhold'    => 'On Hold',
								  'closed'    => 'Closed'
                      );

				echo $this->Form->input("status",array('type'=>'select',"default"=>"onhold","id"=>"projectStatus$pid",'options'=>$options,"class"=>"form-control statusselect","label"=>false,"div"=>false,/*"onchange"=>"javascript:changeStaus($pid);"*/));   
               ?>
               <input type="hidden" id="client_update<?php echo $projects['id'];?>" value="<?php echo $projects['status'];?>">
				</td>
				</tr>
				<?php  $i++;
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="9" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->





<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
