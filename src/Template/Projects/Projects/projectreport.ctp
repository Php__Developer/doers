
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function(){ 
	//alert($('a#paylabel').attr('id'));
		$('a#paylabel').click(function(){
			if($(this).text() == "Show"){
			$('.leftlbl').hide();
			$('#paytext').show();
			$(this).text("Hide");
			}else{
			$('.leftlbl').show();
			$('#paytext').hide();
			$(this).text("Show");
			}
		});
		
		$('.tablesorter').tablesorter({dateFormat: "uk"});
			 //fancybox settings
			$(".view_delayed").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550, 
   					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },

					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
			$(".view_payment").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	550,
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   }, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		
		$(".view_alert").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	550, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".view_hourly").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	700, 
					'height' :	550, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".view_onhold").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	900, 
					'height' :	550, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".payquickadd").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
			$(".billingquickadd").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	550, 
					'height' :	650, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
		$(".billingsquickedit").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	1100, 
					'height' :	650,
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   }, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});  
			$(".payquickedit").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' : 950, 
					'height' :	550,
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   }, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".editproject").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	700, 
					padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			$(".milestoneimg").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	1050, 
				'height' :	700, 
				padding: 0,
         			 helpers: {
              		overlay: {
               			   locked: false
              				  }
         				   },
				'onStart': function(){
				jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
	function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	}
	
	
</script>
<style>
#hide_for_left
{display:none;}
</style>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<div id="error_msg" style="display:none;"></div>
	<div class="leadlbl"> Engineering Lead should update this Current Project Report on daily basis. </div>
<a id="paylabel" href="#A">Show</a>	<div style="display:none;" class="leftlbl" id="paytext">$ <?php echo $left;  ?></div><div class="leftlbl">*****</div>
	<div class="reportlinks" style="float:right; padding-bottom:20px;">
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Verify Billing",
						array('controller'=>'billings','action'=>'verifybilling','prefix' => 'billings'),
						array('class'=>'view_onhold','alt'=>'delayed','style'=>'color:red;','title'=> 'Verify Billings')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Release Delayed",
						array('controller'=>'projects','action'=>'report','key'=> 'delayed'),
						array('class'=>'view_delayed','alt'=>'delayed','style'=>'color:red;','title'=> 'Delayed')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Payment Delayed",
						array('controller'=>'projects','action'=>'report','key'=> 'payment'),
						array('class'=>'view_payment','alt'=>'payment','style'=>'color:red','title'=> 'Delayed Payments')
						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php 
			echo $this->Html->link("On Alert",
						array('controller'=>'projects','action'=>'report','key' => 'alert'),
						array('class'=>'view_alert','alt'=>'alert','style'=>'color:red','title'=> 'On Alert')
						);	
				?>
				
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php
			$total_count=[];
			if(isset($hoursum) && count($hoursum) > 0){
					foreach($hoursum as $billingsum)
					{
						//pr($billingsum)	; die;
						$total = $billingsum['0']['mon_hour'] + $billingsum['0']['tue_hour'] + $billingsum['0']['wed_hour'] + $billingsum['0']['thur_hour']+    $billingsum['0']['fri_hour'] + $billingsum['0']['sat_hour'] + $billingsum['0']['sun_hour'] ;
						$total=$billingsum['todo_hours'];
						$total_count[]=$total;
					}
			}
			
			$billingtotal=number_format(array_sum($total_count),0);
				echo $this->Html->link("Hourly Contract",
						array('controller'=>'projects','action'=>'report','key' => 'hourly'),
						array('class'=>'view_onhold','alt'=>'hourly','style'=>'color:red','title'=>'Sum :'.$billingtotal)

						);	
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("On Hold",
						array('controller'=>'projects','action'=>'hold_projects','key' => 'onhold'),
						array('class'=>'view_onhold','alt'=>'onhold','style'=>'color:red' ,'title'=> 'On Hold')
						);
					
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Fixed Contract",
						array('controller'=>'milestones','action'=>'fixedcontract', 'prefix' => 'milestones'),
						array('class'=>'view_onhold','alt'=>'onhold','style'=>'color:red','title'=> 'Fixed Conract')
						);
					
				?>
		</div>
		<div class="addLinks" style="float:left; padding-right:20px;">
			<?php echo $this->Html->link("Verify Milestone",
						array('controller'=>'milestones','action'=>'verifyallmilestone' , 'prefix' => 'milestones'),
						array('class'=>'view_onhold','alt'=>'onhold','style'=>'color:red','title'=> 'Verify Milestones')
						);
					
				?>
		</div>
		<div style="clear:both;"></div>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<thead>
					<th class="table-header-repeat line-left " width="15%"><a href="#A">Project</a></th>
					<th class="table-header-repeat line-left " width="10%"><a href="#A">Client </a></th>
					<th class="table-header-repeat line-left "  width="8%"><a href="#A">Payment</a></th>
					<th class="table-header-repeat line-left " width="10%"><a href="#A">Resource</a></th>
					<th class="table-header-repeat line-left " width="7%"><a href="#A">$ Left</a></th>
					<th class="table-header-repeat line-left " width="10%"><a href="#A">Next Payment</a></th>
					<th class="table-header-repeat line-left " width="9%"><a href="#A">Next Release</a></th>
					<th class="table-header-repeat line-left " width="9%"><a href="#A">Finish Date</a></th>
					<th class="table-header-repeat line-left" width="20%"><a href="#A">Action</a></th>
				</thead>
				<tbody>
				<?php

				foreach($resultData as $projectData): $class=""; 
					//pr($projectData);die;
				 ?>
				<tr class="<?php echo $class; ?>">
					<td><?php 

					//echo $projectData['project_name']; die;
					 $str = strip_tags($projectData['project_name']);  $name = substr($str,0,20).((strlen($str)>20)?"...":""); 
					 ?> 
						<?php echo $this->Html->link($name,'#A',
								array('class'=>'','title'=>$projectData['notes'])
							); ?>
					</td>
					<td><?php $str = strip_tags($projectData['Contact']['name']); echo substr($str,0,20).((strlen($str)>20)?"...":"");  ?>  
					</td>
					<td><?php echo $projectData['payment']; ?> </td>
					<td><?php $str = strip_tags($projectData['Engineer_User']['first_name']." ".$projectData['Engineer_User']['last_name']);  
						$engUsr = substr($str,0,20).((strlen($str)>20)?"...":"");
							$id = $projectData['engineeringuser'];
							echo $this->Html->link($engUsr,
								array('controller'=>'tickets','action'=>'report', 'prefix'=> 'tickets','id' => $id),
								array('title'=>'Project Name','target'=>'_blank')
							);
					?>		
					</td>
					<td><?php  $left = $projectData['totalamount']-$projectData['total_payment'];
					echo  number_format($left, 2, '.', '');
						?> </td>
					<?php// echo $projectData['nextpayment']; ?> 
<td><?php echo  date_format(date_create($projectData['nextpayment']),"Y-m-d"); ?> </td>
<td><?php echo  date_format(date_create($projectData['nextrelease']),"Y-m-d"); ?> </td>
<td><?php echo  date_format(date_create($projectData['finishdate']),"Y-m-d"); ?> </td>


					<?php// echo $projectData['nextrelease']; ?> 
					<?php //echo $projectData['finishdate']; ?>
				<td>
					<?php if($left<0){ $left_hide="hide_for_left";
										$left_text="Negative Balance Contact Admin";
					}else{ $left_hide="";$left_text="";}
					echo $this->Html->link("",
								array('controller'=>'payments','action'=>'quickadd','prefix'=> 'payments','id' => $projectData['id']),
								array('class'=>'payquickadd','id'=>$left_hide,'title'=>'Add Payment')
							);
						?>
						<?php

						$verify = $this->General->checkverifystatus($projectData['id'],$currentWeekMonday);


           if($projectData['engagagment']=="hourly" && count($verify)>0) {

           	echo $this->Html->link("","javascript:void(0)",
										array('class'=>'verifybillingimg',
										'id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()')
									);	
                 	}
						elseif($projectData['engagagment']=="hourly"  && count($verify)=="") {

					echo $this->Html->link("",
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=>$projectData['id']),
											array('class'=>'billingquickadd','id'=>$left_hide,'title'=>'Add Billing')
										);
									
						
						}

                     ?>
						<?php
						
						
						if($projectData['engagagment']=="hourly" && $projectData['verified']=="1") {
											echo $this->Html->link("",
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=>$result['id']),
											array('class'=>'billingquickadd','id'=>$left_hide,'title'=>'Add Billing')
										);
								}
						elseif($projectData['engagagment']=="hourly"  && $projectData['verified']=="0") {

						echo $this->Html->link("","javascript:void(0)",
										array('class'=>' verifybillingimg',
										'id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()')
									);	
									
						
						}
						?>

						
						<?php echo $this->Html->link("",
								array('controller'=>'payments','action'=>'quickview','prefix'=> 'payments','project_id' => $projectData['id']),
								array('class'=>' payquickedit','id'=>$left_hide,'title'=>'View Payment')
							);
						?>
						<?php echo $this->Html->link("",
								array('controller'=>'billings','action'=>'quickview','prefix'=> 'billings','project_id' =>$projectData['id']),
								array('class'=>' billingsquickedit','id'=>$left_hide,'title'=>'View Billings')
							);
						?>
					<?php echo $this->Html->link("",
								array('controller'=>'projects','action'=>'quickedit','project_id' => $projectData['id']),
								array('class'=>' editproject','title'=>'Edit','target'=>'_blank')
							);
							
							echo $this->Html->link("",
								array('controller'=>'projects','action'=>'projectdetails','project_id' =>$projectData['id']),
								array('class'=>' go','id'=>$left_hide,'title'=>'Go','target'=>'_blank')
							);
						?>
						<?php
						if($projectData['engagagment']=="fixed"||$projectData['engagagment']=="monthly") {
							echo $this->Html->link("",
								array('controller'=>'milestones','action'=>'milestoneslist','prefix'=> 'milestones','id' => $projectData['id']),
								array('class'=>' milestoneimg','id'=>$left_hide,'title'=>'Milestone')
							);
						}
						?>
						<span style="color:red"><?php echo $left_text;?></span>
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
				
					<div class="clear"></div>
	</td>
	<td>
</td>
</tr>
</table>
			</div>
			<!--  end content-table  -->
	<div class="clear"></div>
</div>
