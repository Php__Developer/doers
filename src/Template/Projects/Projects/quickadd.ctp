

<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>












<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox'))	//ui.core ?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//alert(base_url);
	$(document).ready(function() { 
			//calender
			$( "#week_start" ).datepicker({
				dateFormat: 'dd-mm-yy',
				firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}
			});
			$('.numericMax').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
			$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	});
	$(window).ready(function(){
	$('#ui-datepicker-div').attr('style','display:none');
	});
	function addbilling(){
			if($('#verified').val()>0){
			swal("Please verify Last week billing first");
			return false;
			}
			else{
		if(notEmpty($('#responsible').val(),'error_msg11','Enter Id of responsible person'))
		{ return false; }
		if(notEmpty($('#week_start').val(),'error_msg1','Enter week start'))
		{ return false; }
		if(notEmpty($('#max_limit').val(),'error_msg2','Enter max limit billing') || checkLimit($('#max_limit').val(),'200','error_msg2','Max hours cannot be greater than 200'))
		{  return false; }
		if(notEmpty($('#todo_hours').val(),'error_msg3','Enter to-do hours') || checkLimit($('#todo_hours').val(),'200','error_msg3','To-do hours cannot be greater than 200'))
		{ return false; }
		var msg = "Daily billing hours cannot be greater than 24";
		if(checkLimit($('#mon_hour').val(),'24','error_msg4',msg) || checkLimit($('#tue_hour').val(),'24','error_msg5',msg) || checkLimit($('#wed_hour').val(),'24','error_msg6',msg) || checkLimit($('#thur_hour').val(),'24','error_msg7',msg) || checkLimit($('#fri_hour').val(),'24','error_msg8',msg) || checkLimit($('#sat_hour').val(),'24','error_msg9',msg) || checkLimit($('#sun_hour').val(),'24','error_msg10',msg))
		{ return false; }
		var post_data = { 'project_id' : $('#project_id').val(),
									'user_id':$('#responsible').val(),
									'currency':$('#currency').val(),
									'week_start' : $('#week_start').val(),
									'max_limit' : $('#max_limit').val(),
									'todo_hours' : $('#todo_hours').val(),
									'mon_hour' : $('#mon_hour').val(),
									'tue_hour' : $('#tue_hour').val(),
									'wed_hour' : $('#wed_hour').val(),
									'thur_hour' : $('#thur_hour').val(),
									'fri_hour' : $('#fri_hour').val(),
									'sat_hour' : $('#sat_hour').val(),
									'sun_hour' : $('#sun_hour').val(),
									'notes' : $('#notes').val()
									};
									
									//alert(post_data);
		if($.active>0){
		}else{
			var base_url = "<?php echo $this->Url->build('/', true); ?>";
			//alert(base_url);
			$.post(base_url+'billings/quickadd',post_data,function(data){
				//alert(base_url);

				if(data==1){
					window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
				}else if(data=='error'){
					swal("Billing entry for this week already exists!");
				}else{
				//alert("Billing updation failed!");
					window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
				}
			});
		}
	}
	}
	// not null validation function
	function notEmpty(val,errorID,msg){
		if(val == ""){
			$('#'+errorID).html(msg);
			$('#'+errorID).attr('style','display:block;color:red;font-size:13px;');
			setTimeout(function() { $('#'+errorID).fadeOut('slow'); }, 5000);
			return true;
		}
	}
	function checkLimit(val,limit,errorID,msg){
		if(parseFloat(val) > parseFloat(limit)){
			val = '';
			$('#'+errorID).html(msg);
			$('#'+errorID).attr('style','display:block;color:red;font-size:13px;');
			setTimeout(function() { $('#'+errorID).fadeOut('slow'); }, 5000);
			return true;
		}
	}
</script>

	<!--  start content-table-inner -->
	
		 <div class="col-sm-9">
            <div class="white-box">

   <?php echo $this->Form->create('Billing',array('url' => ['action'=>'quickadd'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>

	<h2 class="font-bold text-success pull-right"> <?php echo $this->request->data['Billing']['project_name']; ?> Add Billings </h2>
			<div id="error_msg"></div>

<br><br>
			<div class="form-group">
              <label for="exampleInputpwd1">Project Name</label>
                  

        <?php 
				$projectname= $this->request->data['Billing']['project_name'];
			
				
				 echo $this->Form->input("",array("type"=>"text","readonly"=>true,"class"=>"form-control","label"=>false,"div"=>false,"value"=>$projectname));
          


				 echo $this->Form->hidden("project_id",array("class"=>"form-control","id"=>"project_id","value"=>$this->request->data['Billing']['project_id']));
            ?></div>




		
			<div class="form-group">
              <label for="exampleInputpwd1">Responsible</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

		<?php
				//pr($userID);
				$options=$this->General->getuserexceptme($userSession[0]);
				//pr($options);
				 echo $this->Form->input("pm_id",array( "type"=>'select',"id"=>"responsible","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
          ?></div></div>
         <div id="error_msg11"></div>
			





			<div class="form-group">
              <label for="exampleInputpwd1">Currency</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                 <?php
				   $options=array('usd'    =>  'USD', 
											'aud'    => 'AUD',
											'cad'    => 'CAD',
											'inr'    => 'INR'
                      );
			echo $this->Form->input("currency",array('type'=>'select',"id"=>"currency",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div></div>


     	<div class="form-group">
              <label for="exampleInputpwd1">Week start:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
			<?php
				
				 echo $this->Form->input("week_start",array("type"=>"text","readonly"=>true,"id"=>"week_start","class"=>"form-control","label"=>false,"div"=>false));
            ?></div></div>
			<div id="error_msg1"></div>
			

	     	<div class="form-group">
              <label for="exampleInputpwd1">Maximum limit:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


                        <?php
		echo $this->Form->input("max_limit",array("class"=>"form-control numericMax","id"=>"max_limit","label"=>false,"div"=>false));
		?></div></div><div id="error_msg2"></div>


		
     	<div class="form-group">
              <label for="exampleInputpwd1"> Todo Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


		
		<?php
		echo $this->Form->input("todo_hours",array("class"=>"form-control numericMax","id"=>"todo_hours","label"=>false,"div"=>false));
		?></div></div><div id="error_msg3"></div>


	<div class="form-group">
              <label for="exampleInputpwd1">Monday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>




		<?php
		echo $this->Form->input("mon_hour",array("class"=>"form-control numeric","id"=>"mon_hour","label"=>false,"div"=>false));
		?></div></div><div id="error_msg4"></div>





	<div class="form-group">
              <label for="exampleInputpwd1">Tuesday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


		<?php
		echo $this->Form->input("tue_hour",array("class"=>"form-control numeric","id"=>"tue_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg5"></div>





	<div class="form-group">
              <label for="exampleInputpwd1">Wednesday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

		<?php
		echo $this->Form->input("wed_hour",array("class"=>"form-control numeric","id"=>"wed_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg6"></div>





	<div class="form-group">
              <label for="exampleInputpwd1">Thursday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


        <?php
		echo $this->Form->input("thur_hour",array("class"=>"form-control numeric","id"=>"thur_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg7"></div>



			
<div class="form-group">
              <label for="exampleInputpwd1">Friday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


		<?php
		echo $this->Form->input("fri_hour",array("class"=>"form-control numeric","id"=>"fri_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg8"></div>



<div class="form-group">
              <label for="exampleInputpwd1">Saturday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>



		<?php
		echo $this->Form->input("sat_hour",array("class"=>"form-control numeric","id"=>"sat_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg9"></div>

<div class="form-group">
              <label for="exampleInputpwd1">Sunday Hour:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

		<?php
		echo $this->Form->input("sun_hour",array("class"=>"form-control numeric","id"=>"sun_hour","label"=>false,"div"=>false,));
		?></div></div><div id="error_msg10"></div>





<div class="form-group">
              <label for="exampleInputpwd1">Notes:</label>

		<?php
				 echo $this->Form->input("notes",array("type"=>"textarea","id"=>"notes","class"=>"form-control","label"=>false,"div"=>false,));
            ?></div>




		
	





			
<div class="form-group">

			<input type="button" class="btn btn-success" onclick="javascript:addbilling();" value="Save" style="width:80px;">
<input type="hidden" id="verified" class="btn btn-success" value=<?php echo count($verified)?> />

 </div>
 </div>





	<?php
if(count($verified)>0){?>
			<script>
				swal("Please verify Last week billing first");
			</script>
		<?php }
?>	



<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<!-- 		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 -->

<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
