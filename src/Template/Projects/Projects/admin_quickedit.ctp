<?php echo $html->css(array("screen","jquery_ui_datepicker")); ?>
<?php echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','listing')); ?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function() { 
	
		$('a#paylabel').click(function(){
		
		 $("#paytext").toggle();
		 $("#paylabel").toggle();
			
		});
		$('#paytext').click(function(){
		
		  $("#paylabel").toggle();
		 $("#paytext").toggle();
			
		});
		//calender
			$("#nextpayment").datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0
			});
		//calender 
			$("#nextrelease").datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0
			});
		//calender
			$( "#finishdate" ).datepicker({
				dateFormat: 'dd-mm-yy',
				minDate: 0
			});
	$('#vooap').checkbox_value();
	$('#is_dropboxback').checkbox_value();
	$('#is_codeback').checkbox_value();
	$('#onalert').checkbox_value();
	$('#dropbox').checkbox_value();
	$('#dailyupdate').checkbox_value();
	/*** code start : remove and append options from team drop down according to above selected users ***/
		 var detachedItem1;
		 var detachedItem2;
		 var detachedItem3;
		 var detachedItem4;
		$('#pm_id').change(function(){
			if(detachedItem1){ 
				detachedItem1.appendTo(".multiSelect option:last");
				detachedItem1 = null;
				sortDropDownListByText('team_id');
			}
			var respID = $('select#pm_id option:selected').val();
			detachedItem1 = null;
			detachedItem1 = $(".multiSelect option[value='"+respID+"']").detach();
		});
		$('#engineeringuser').change(function(){
			if(detachedItem2){ 
				detachedItem2.appendTo(".multiSelect option:last");
				detachedItem2 = null;
				sortDropDownListByText('team_id');
			}
			var acctID = $('select#engineeringuser option:selected').val();
			detachedItem2 = null;
			detachedItem2 = $(".multiSelect option[value='"+acctID+"']").detach();
		});
		$('#salesfrontuser').change(function(){
			if(detachedItem3){ 
				detachedItem3.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem3 = null;
			}
			var ContID = $('select#salesfrontuser option:selected').val();
			detachedItem3 = null;
			detachedItem3 = $(".multiSelect option[value='"+ContID+"']").detach();
		});
		$('#salesbackenduser').change(function(){
			if(detachedItem4){ 
				detachedItem4.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem4 = null;
			}
			var infID = $('select#salesbackenduser option:selected').val();
			detachedItem4 = null;
			detachedItem4 = $(".multiSelect option[value='"+infID+"']").detach();
		});
	});
	
	(function( $){
		  $.fn.checkbox_value = function() {
			$(this).click(function() {
					if($(this).is(":checked")){
						$(this).val('1');
					}else{
						$(this).val('0');
					}
				});
		  };
	})( jQuery );
	
	$(window).ready(function(){
	$('#ui-datepicker-div').attr('style','display:none');
	});
	var flag=0;
	function confirmAction(msg,key){
			var action = confirm(msg);
			if(action == true){
				$('#client_update').val(key);
			}
				return true;		
	}	
	function editproject(){
	
		
		
		if(validateTeamUsers()){
			var status = $('#projectStatus').val();
			if(status == 'closed'){
				var msg = "Are you want to deactivate this Project client?"
				confirmAction(msg,"deact");
			}else if(status =="current" && (old_status == "closed")){
				var msg = "Are you want to activate this Project client?"
				confirmAction(msg,"act");
			}
			var team_id = $('.multiSelect').val();
			if(team_id == null){
				team_id = "";
			}
			var id = $('#project_id').val();
			var project_name = $('#project_name').val();
			var client_id = $('#client_id').val();
			var pm_id = $('#pm_id').val();
			var profile_id = $('#profile_id').val();
			
			var engagagment = $('#engagagment').val();
			var hourly_rate = $('#hourly_rate').val();
			var platform = $('#platform').val();
			var credits = $('#credits').val();
			var payment = $('#payment').val();
			var technology = $('#technology').val();
			var engineeringuser = $('#engineeringuser').val();
			var salesfrontuser = $('#salesfrontuser').val();
			var salesbackenduser = $('#salesbackenduser').val();
			var totalamount = $('#totalamount').val();
			//var paid = $('#paid').val();
			//var left = $('#left').val();
			var nextpayment = $('#nextpayment').val();
			var nextrelease = $('#nextrelease').val();
			var finishdate = $('#finishdate').val();
			var notes = $('#notes').val();
			var operation_notes = $('#operation_notes').val();
			var dailyupdate = $('#dailyupdate').val();
			var dropbox_foldername = $('#dropbox_foldername').val();
			var dropbox = $('#dropbox').val();
			var vooap = $('#vooap').val();
			var is_dropboxback = $('#is_dropboxback').val();
			var is_codeback = $('#is_codeback').val();
			var onalert = $('#onalert').val();
			var processnotes = $('#processnotes').val();
			var client_update = $('#client_update').val();
			$('.ajaxloaderdiv').show();
			if($.active>0){
			}else{
				$.post(base_url+'admin/projects/quickedit',{'id':id,'project_name':project_name,'client_id':client_id,'pm_id':pm_id,'profile_id':profile_id,'engagagment':engagagment,'hourly_rate':hourly_rate,'platform':platform,'payment':payment,'technology':technology,'engineeringuser':engineeringuser,'salesfrontuser':salesfrontuser,'salesbackenduser':salesbackenduser,'totalamount':totalamount,'nextpayment':nextpayment,'nextrelease':nextrelease,'finishdate':finishdate,'notes':notes,'dailyupdate':dailyupdate,'dropbox_foldername':dropbox_foldername,'dropbox':dropbox,'vooap':vooap,'is_dropboxback':is_dropboxback,'is_codeback':is_codeback,'operation_notes':operation_notes,'onalert':onalert,'processnotes':processnotes,'status':status,'client_update':client_update,'credits':credits,'team_id':team_id},function(data){
					if(data!=1){alert(data)};
					if(data==1){
						$('.ajaxloaderdiv').hide();
						window.parent.$('#close-fb').click();
						
					}
				});
			}
		}
	}
	function validateTeamUsers(){
		var team_id = $('.multiSelect').val();
		var pm_id = $('#pm_id').val();
		var engineeringuser = $('#engineeringuser').val();
		var salesfrontuser = $('#salesfrontuser').val();
		var salesbackenduser = $('#salesbackenduser').val();
		if(team_id){
			$.each( team_id, function( key, value ) {
			//alert( key + ": " + value );
				flag=0;
				if(value == engineeringuser){
					flag=1;
				}else if(value == salesfrontuser){
					flag=2;
				}else if(value == salesbackenduser){
					flag=3;
				}else if(value == pm_id){
					flag=4;
				}
			});
			if(flag>0){
				if(flag==1){
						$('#showteamerror').html("Repetitive accountant user ID in project team.");
				}else if(flag==2){
					$('#showteamerror').html("Repetitive consultant user ID in project team.");
				}else if(flag==3){
					$('#showteamerror').html("Repetitive informer user ID in project team.");
				}else if(flag==4){
					$('#showteamerror').html("Repetitive responsible user ID in project team.");
				}
				$('#showteamerror').attr('style','display:block;color:red;font-size:13px;');
				setTimeout(function() { $('#showteamerror').fadeOut('slow'); }, 5000);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	function hourly_hide()
			{
			var e = document.getElementById("engagagment");
			var strSel = e.options[e.selectedIndex].value 
			if(strSel=="hourly"){
			document.getElementById("hourly_rate").readOnly = false;
			//.style.display='block';
			}
			else
			{
			document.getElementById("hourly_rate").readOnly = true;
			}
		}

</script>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('Project',array('action'=>'edit','method'=>'POST',"class"=>"login"));?>
	<?php $session->flash(); $session_info = $this->Session->read("SESSION_ADMIN");
		$userID = $session_info[0]; ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<center>
	
	<table border="0" cellpadding="0" cellspacing="4"  id="id-form" style="font-size:13px;">
		<tr><h1> Edit Project Details </h1>
			<th valign="top">Project name:</th>
			<td><?php //pr($this->data); die;
				 echo $form->input("project_name",array( "class"=>"inp-form","id"=>"project_name","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
				 echo $form->hidden("id",array("id"=>"project_id"));
				 echo $form->hidden("clientUpdate",array('id'=>'client_update'));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Client name:</th>
			<td><?php 
				$id = $this->data['Project']['client_id'];
				$options=$general->getclients($id);
				 echo $form->input("client_id",array( "type"=>'select',"class"=>"ourselect","id"=>"client_id",'options'=>$options,"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Responsible:</th>
			<td><?php
				$options=$general->getuserexceptme($userID);
				 echo $form->input("pm_id",array( "type"=>'select',"id"=>"pm_id","class"=>"ourselect",'options'=>$options,"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
		
			<th valign="top">Sale Profile:</th>
			<td><?php
				$options=$general->getprofile();
				echo $form->input("profile_id",array( "type"=>'select',"id"=>"profile_id","class"=>"ourselect",'options'=>$options,"label"=>false,"div"=>false));
            ?>
			<a id="paylabel" href="#A" style="color:red;">Show Details</a>	<div style="display:none;folat:left;margin-left:200px;margin-top:-19px;cursor:pointer;" class="" id="paytext"> <?php echo $nameofprofile.", ".htmlspecialchars($pass);  ?></div><!--<div class="leftlbl">*****</div>		-->
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Engagagment:</th>
			<td><?php
				   $options=array('hourly'    =>  'Hourly', 
											'fixed'    => 'Fixed',
											'monthly'    => 'Monthly'
                      );
			echo $form->input("engagagment",array('type'=>'select','options'=>$options,"class"=>"ourselect",'onChange'=>'hourly_hide()',"id"=>"engagagment","label"=>false,"div"=>false));    
            ?></td>
		</tr>
		<tr>
			<th valign="top">Hourly Rate:</th>
			<td><?php
				 echo $form->input("hourly_rate",array("class"=>"inp-form",'id'=>'hourly_rate',"style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Platform:</th>
			<td><?php
				   $options=array('upwork'    =>  'Upwork', 
											'elance'    => 'Elance',
											'freelance'    => 'Freelance',
											'other'    => 'Other'
                      );
			echo $form->input("platform",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"platform","label"=>false,"div"=>false));    
            ?></td>
		</tr>
		<tr>
			<th valign="top">Payment:</th>
			<td><?php
				   $options=array('upwork'    =>  'Upwork', 
											'elance'    => 'Elance',
											'paypal'    => 'Paypal',
											'cheque'    => 'Cheque',
											'wiretransfer'    => 'Wiretransfer',
											'freelance'    => 'Freelance',
											'other'    => 'Other'
                      );
			echo $form->input("payment",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"payment","label"=>false,"div"=>false));    
            ?></td>
		</tr>
		<tr>
			<th valign="top">Technology:</th>
			<td><?php
				   $options=array('other'    =>  'Other', 
											'php'    => 'PHP',
											'phone'    => 'Phone',
											'microsoft'    => 'Microsoft',
											'design'    => 'Design',
											'seo'    => 'Seo',
                      );
			echo $form->input("technology",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"technology","label"=>false,"div"=>false));    
            ?></td>
		</tr>
		<tr>
			<th valign="top">Accountable:</th>
			<td><?php
				$options=$general->getuser();
				 echo $form->input("engineeringuser",array( "type"=>'select',"class"=>"ourselect",'options'=>$options,'id'=>'engineeringuser',"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Consultant:</th>
			<td><?php
				$options=$general->getuser();
				 echo $form->input("salesfrontuser",array( "type"=>'select',"class"=>"ourselect",'options'=>$options,'id'=>'salesfrontuser',"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Informer:</th>
			<td><?php
				$options=$general->getuser();
				 echo $form->input("salesbackenduser",array( "type"=>'select',"class"=>"ourselect",'id'=>'salesbackenduser','options'=>$options,"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Team:</th>
			<td><?php
				$options=$general->getuserexceptraci();
			    if($this->data['Project']['team_id'] != ''){
					$cont_cats = explode(",",$this->data['Project']['team_id']);
				}else{
					$cont_cats = '';
				}  
				echo $form->input("team_id",array( "type"=>'select','size'=>'10','multiple' => true,'options'=>$options,"label"=>false,"div"=>false,"class"=>"multiSelect","id"=>"team_id",'selected' =>$cont_cats));
            ?>
			<div id="showteamerror" style="display:none;"></div>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Total Amount:</th>
			<td><?php
				 echo $form->input("totalamount",array("class"=>"inp-form",'id'=>'totalamount',"style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<!--<tr>
			<th valign="top">Paid Amount:</th>
			<td><?php
				 echo $form->input("paid",array("class"=>"inp-form",'id'=>'paid',"style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Left Amount:</th>
			<td><?php
				 echo $form->input("left",array("class"=>"inp-form",'id'=>'left',"style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>-->
		<tr>
			<th valign="top">Next Payment:</th>
			<td><?php
				 echo $form->input("nextpayment",array("type"=>"text","readonly"=>true,"class"=>"inp-form","id"=>"nextpayment","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Next Release:</th>
			<td><?php
				 echo $form->input("nextrelease",array("type"=>'text',"class"=>"inp-form","readonly"=>true,"id"=>"nextrelease","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Last Release:</th>
			<td><?php
				 echo $form->input("finishdate",array("type"=>'text',"class"=>"inp-form","readonly"=>true,"id"=>"finishdate","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Notes:</th>
			<td><?php
				 echo $form->input("notes",array("style"=>"height:131px;width:298px;","label"=>false,'id'=>'notes',"div"=>false));
				 echo $form->hidden("id");
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Operation Notes:</th>
			<td><?php
				 echo $form->input("operation_notes",array("style"=>"height:131px;width:298px;","label"=>false,"id"=>"operation_notes","div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Credentials:</th>
			<td><?php
				 echo $form->input("credentials",array("style"=>"height:131px;width:298px;",'id'=>'credits',"label"=>false,"div"=>false));
            ?></td>
			<td>
			</td>
		</tr>
		<tr>
			<th valign="top">Updates</th>
			<td>
			<div style="float:left;padding-right:10px;">
				<?php  $value = isset($this->data['Project']['dailyupdate']) ? $value=$this->data['Project']['dailyupdate'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="dailyupdate" name="dailyupdate" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl" for="dailyupdate">Daily Update </label>

			<div style="float:left;padding-right:5px;">
				<?php $value = isset($this->data['Project']['dropbox']) ? $value=$this->data['Project']['dropbox'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="dropbox" name="dropbox" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl" for="dropbox">Dropbox </label>
			</div>
			<div style="float:left;padding-right:5px;">
				<?php $value = isset($this->data['Project']['is_dropboxback']) ? $value=$this->data['Project']['is_dropboxback'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="is_dropboxback" name="is_dropboxback" title="Back up of DropBox needed" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl" for="is_dropboxback">Is_dropboxback </label>
			</div>
			<div style="float:left;padding-right:5px;">
				<?php $value = isset($this->data['Project']['is_codeback']) ? $value=$this->data['Project']['is_codeback'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="is_codeback" name="is_codeback" title="Back up of code needed" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl" for="is_codeback">Is_codeback </label>
			</div>
			<div>
				<?php $value = isset($this->data['Project']['vooap']) ? $value=$this->data['Project']['vooap'] : $value=''; ?>
					<?php $checked = ($value == '1') ? $checked= "checked=checked " : $checked =''; ?>
					<input type="checkbox" id="vooap" name="vooap" value="<?php echo $value; ?>" <?php echo $checked; ?> >
					<label class="projectlbl" for="erp">ERP</label>
			</div>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Process Notes:</th>
			<td><?php
				 echo $form->input("processnotes",array('id'=>'processnotes',"style"=>"height:131px;width:298px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Dropbox folder:</th>
			<td><?php
				 echo $form->input("dropbox_foldername",array("id"=>"dropbox_foldername","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Status:</th>
			<td><script> var old_status ='<?php echo $this->data['Project']['status'];?>'; </script><?php
				
				   $options=array('current'    =>  'Current', 
											'onhold'    => 'On Hold',
											'closed'    => 'Closed'
                      );
			echo $form->input("status",array('type'=>'select',"id"=>"projectStatus",'options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Alert :</th>
			<td>
					<?php $value = isset($this->data['Project']['onalert']) ? $value=$this->data['Project']['onalert'] : $value=''; ?>
					<?php $checked = ($value == '1') ? $checked= "checked=checked " : $checked =''; ?>
					<input type="checkbox" id="onalert" name="onalert" value="<?php echo $value; ?>" <?php echo $checked; ?> >
					<label class="projectlbl" for="onalert">On Alert </label>
			</td>
			<td></td>
		</tr>
			<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: editproject();","style"=>"width:80px;")); 
			 
			?>
			</td>
			<td></td>
		</tr>
	</table></center>
	<!-- end id-form  -->

			</td>
			<td>

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->