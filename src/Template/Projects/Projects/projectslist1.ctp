 <!-- .row -->
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="css/oribe.css" rel="stylesheet">
<?php echo $this->Html->css(array("oribe")); ?>
<?php   echo $this->Html->css(array("oribe")); ?> 
 <?php echo $this->Form->create('Project',array('url' => ['action'=>'projectslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<div class="row el-element-overlay m-b-40">

Results: <span class="counteringdata">1 of <?php echo $paginatecount;?></span>

	<input type="hidden" value="<?php echo $paginatecount;?>" class="paginate" >
   <div class="col-md-12">
       <div class="col-md-3 searchingdata">
			  <?php
	             $fieldsArray = array(
	              // ''				  => 'Search By',
					'Projects.project_name'  =>  'Search By Project Name',
					'Contact.name'  =>  'Search By Client Name',
					'Projects.platform'  =>  'Search By Platform',
					'Projects.status'  =>  'Search By Status',
	                    );
	                    echo $this->Form->select("Project.fieldName",$fieldsArray,['value'=> $search1,'class'=>'selectpicker m-b-20 m-r-10 bs-select-hidden','data-style'=>'btn-primary btn-outline'],array("id"=>"searchBy selectopt","label"=>false,"class"=>"form-control","empty"=>false),false); ?>
									
                 
			 </div>
           <div class="col-md-4">
		  
              <?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "User.status"){
				 $display1 = "display:block";
				}else{
				  $display2 = "display:block";
				}
				echo $this->Form->input("Project.value1",array("id"=>"example-input1-group2 demo-input-search2 searchval","class"=>"form-control searchval","style"=>"$display1", "div"=>false, "label"=> false,"value"=>$search2,"placeholder"=>"Search"));
				?>
     
         </div>

		<div class="col-md-4">
		  
				<?php
		echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";

			echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
            $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/projects/projectslist', true)));
				?>

			<button class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" aria-expanded="false" data-toggle="dropdown" type="button">
			Sort By
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu animated drp_list" role="menu">
		    <li>
			<a href="#">Name</a>
			</li>  
			<li>
			<a href="#">Client</a>
			</li>

			<li>
			<a href="#">Platform </a>
			</li>
			<li>
			<a href="#"> Payment</a>
			</li>
			
			<li>
			<a href="#"> Status </a>
			</li>
			<!-- <li class="divider"></li> -->
			<li>
			<a href="#">Left</a>
			</li>
			</ul>

			<?php 
			echo $this->Html->link('<i class="fa  fa-plus"></i>',
			array('controller'=>'projects','action'=>'add'),
			['escape' => false,"class"=>"btn btn-info btn-circle ","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Project"]
			);?>
		
			<?php echo $this->Form->end(); ?>
							
		 </div>
		 <div class="col-md-1 paginateproject">
		 <ul class="pagination paginate-data">
				<li class="previousdata">
				
				<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
				</li>
                 <li>
				<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
				</li></ul>
             </div>


<div class="ajax">

  <div class="parent_data">
  <?php if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $result): ?>
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
          <div class="white-box">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs firstLISt" role="tablist">
              <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
              <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
            </ul>
            <!-- Tab panes  substring($row->parent->category_name,35);  -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane ihome active" >
                 <ul class="list-group ">
                <li class="list-group-item list-group-item-danger project"><b>Project :</b> 
                  <?php $str = strip_tags($result['project_name']); 
					echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
						<li class="list-group-item list-group-item-success client"><b>Client :</b>  <?php $str = strip_tags($result['Contact']['name']); 
						echo substr($str,0,19).((strlen($str)>19)? "...":"");  ?></li>
						<li class="list-group-item list-group-item-info plateform"><b>PlateForm:</b> <?php echo $result['platform']; ?></li>
						<li class="list-group-item list-group-item-warning payment"><b>Payment:</b><?php echo $result['payment']; ?></li>
						<li class="list-group-item list-group-item-danger status"><b>Status:</b><?php echo $result['status']; ?></li>
						<li class="list-group-item list-group-item-success left"><b>Left:</b><span class="left_amount"><?php $left = (($result['totalamount']) - ($result['total_payment']));
											echo number_format($left, 2, '.', ''); ?></span></li>
						
             <ul class="nav nav-tabs list-group-item-info " role="tablist">


			<li >
                          <?php if($left<0){
						  $left_hide="hide_for_left";
										$left_text="Negative Balance Contact Admin";
										}else
										{ $left_hide="";$left_text="";
										} 

                    echo $this->Html->link(
		                  '<i class="fa fa-usd"></i>',
		                  array('controller'=>'payments','action'=>'quickadd','prefix'=>'payments','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline  popup_window ','id'=>$left_hide,'title'=>'Add Payment',]
		                  );
							?></li>
                            <li>
                        <?php
                 if($result['engagagment']=="hourly" && $result['verify']=="1") {             
						   echo $this->Html->link(
		                  '<i class="fa fa-behance-square"></i>',
		                  array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Add Billing']
		                  );
                         }elseif($result['engagagment']=="hourly"  && $result['verify']=="0") {
 					      echo $this->Html->link(
		                  '<i class="fa fa-behance"></i>',
		                  array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window ','id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()']
		                  );}
						?></li>
							<li>
							<?php
                           echo $this->Html->link(
		                  '<i class="fa fa-scribd"></i>',
		                  array('controller'=>'payments','action'=>'quickview','prefix'=>'payments','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window' ,'id'=>$left_hide,'title'=>'View Payment']
		                  );
		                  ?></li>
							<li><?php
                           echo $this->Html->link(
		                  '<i class="ti-pinterest-alt"></i>',
		                  array('controller'=>'billings','action'=>'quickview','prefix'=>'billings','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'View Billings']
		                  );
		                  ?></li>
                           <li><?php
                           echo $this->Html->link(
		                  '<i class="fa fa-cog"></i>',
		                  array('controller'=>'projects','action'=>'quickedit','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Edit']
		                  );
		                  ?></li>
						   <li>



						   <?php
                           echo $this->Html->link(
		                  '<i class="ti-close"></i>',
		                  array('controller'=>'projects','action'=>'delete','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','id'=>"$left_hide" ,'title'=>'Delete']
		                  );
		                  ?>


		                  </li>
                      

                     <li> <?php
						if($result['engagagment']=="fixed"||$result['engagagment']=="monthly") {
							 echo $this->Html->link(
		                  '<i class="fa fa-weibo"></i>',
		                  array('controller'=>'milestones','action'=>'milestoneslist','prefix'=> 'milestones','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Milestone']
		                  );
                           }
						   ?></li>
	 <li><h3 class="box-title m-b-0"  style="color:red;padding:6px"><?php echo $left_text;?></h3><li>
	</ul>
	</ul>

	 </div>
              <div role="tabpanel" class="tab-pane iprofile">
                <div class="col-md-6">
                  <h3>Lets check profile</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane imessages">
                <div class="col-md-6">
                  <h3>Come on you have a lot message</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane isettings">
                <div class="col-md-6">
                  <h3>Just do Settings</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>




                 

         


            
          </div>
        </div>
      
     <?php $i++ ;
				endforeach; ?>
				<?php } else { ?>

				<?php
				}
				?>




       </div>
				
        <!-- /.usercard-->
      </div>




 <!-- /.row -->


<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->

 <?php 
    echo  $this->Html->script(array('oribe'));
?>
<script type="text/javascript">

function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}

      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>
