<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Billings</title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="/erp/fssi/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.bind.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/common.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/listing.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jsDatePick.min.1.3.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.validate.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery-ui-datepicker.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/stuHover.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/site.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.ticker.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/pGenerator.jquery.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/rating_simple.js"></script>
<script type="text/javascript" src="http://localhost/erp/fssi/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="http://localhost/erp/fssi/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="shortcut icon" href="http://localhost/erp/fssi/img/favicon.ico">
<![if !IE 7]>

<!--  styled select box script version 1 -->

	<script type="text/javascript" src="/erp/fssi/js/jquery.selectbox-0.5.js"></script>

<![endif]>

	<script type="text/javascript" src="/erp/fssi/js/jquery.selectbox-0.5_style_2.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.filestyle.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/custom_jquery.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.tooltip.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.dimensions.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/jquery.pngFix.pack.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/functions.js"></script>
	<script type="text/javascript" src="/erp/fssi/js/tinysort.js"></script>




<link rel="stylesheet" type="text/css" href="/erp/fssi/css/jsDatePick_ltr.min.css" />
<link rel="stylesheet" type="text/css" href="/erp/fssi/css/jquery_ui_datepicker.css" />
<!-------------------------------->
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<?php echo $html->css(array("screen")); ?>
<script type="text/javascript">
	jQuery(document).ready(function(){ 
	
	  jQuery.noConflict();
	
		jQuery('.tablesorter').tablesorter({

		headers: {
            0: {
                sorter: false
            },	
		
            1: {
                sorter: false
            },
		    2: {
                sorter: false
            }    
		}		
		});
	});
	$(".viewPaydetails").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
</script>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	jQuery(function() {
	jQuery( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}	
		});
	jQuery( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
			// maxDate:new Date(),
			 firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 0,''];
				}
			 
		 });
	});
	
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
	}	
	
 function weekFrom()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(previous).setDate(previous.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("startdate").value=monthsum2;
		 }
		 function weekFrom1()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(next).setDate(next.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("enddate").value=monthsum2;
		 }
  function nextWeek()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
	var next=new Date(endDate);
	var monthsum= new Date(new Date(previous).setDate(previous.getDate()+6));
			//alert(monthsum);
			var monthsum2=monthsum.toISOString().substr(0,10);
			//alert(monthsum2);
			document.getElementById("startdate").value=monthsum2;
	
  }
 

</script>
<?php
   echo $form->create('Project',array('method'=>'POST', 'controller'=>'projects',  'action'=>'admin_report/hourly', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm();')); ?>

<!--  start content-table-inner -->
	<div id="content-table-inner">
	<center><span style="font-weight:bold;"><h4> (Current Week) Hourly Contract Projects</h4></span></center></br>
	<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
	<tr>	
	</br>
	<td width="8%">
	From Date:
		<div style="float:left;margin-top:10px;">
		</td>
		<td width="5%">
		<?php
		 echo $form->button('<<',array('type'=>'button','id'=>'weekStart','OnClick'=>'weekFrom()','class'=>'buttonnextreport1'));?>
	</td>
	<td width="1%">
	<?php 
		if($fromDate!=""){
		$st_date=date('Y/m/d',strtotime($fromDate));
		}
		else{
		if(date('N')==7){
		$st_date=date('Y/m/d',strtotime("last monday"));
		}
		else{
		$st_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		}
	}
	 echo $form->input("start_date",array("class"=>"top-search-inp","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>date('Y/m/d',strtotime($st_date))));
     ?>
	</td>
	<td width="5%">
	 <?php echo $form->button('>>',array('type'=>'button','id'=>'NextWeek','OnClick'=>'nextWeek()','class'=>'buttonnextreport1'));
     ?>
	</div></td>
		<td width="8%">
		To Date:
		</td>
		<td width="5%">
		<div style="float:left">
	<?php echo $form->button('<<',array('type'=>'button','id'=>'weekend1','OnClick'=>'weekFrom1()','class'=>'buttonnextreport1'));
     echo $form->input('key',array('type'=>'hidden','id'=>'key', 'name'=>'key' ,'value'=>'hourly'));
	?>
	</td>
			<td width="15%">
	<?php
		if($toDate!=""){
		$todate=date('Y/m/d',strtotime($toDate));
		}else{
		if(date('N')==7){
		$todate=date('Y/m/d',strtotime("sunday"));
		}else{
		$current=date("Y-m-d");
		$date = strtotime($current);
		$date = strtotime("next sunday", $date);
		$todate=date('Y/m/d',$date);
		}
	}
	echo $form->input("end_date",array("class"=>"top-search-inp","label"=>false,"div"=>false,"id"=>"enddate","readonly"=>true, "value"=>date('Y/m/d',strtotime($todate)))); ?>
	</td>
	</div>
		<td width="5%">
		<div style="float:left;margin-top:0px;">
			<?php echo $form->submit("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
			</div>
			</td>
		</tr>
	</table>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
			
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<thead>
						<th class="table-header-repeat line-left incoming" width="5%"><a href="#A">Sr. No.</th>
						<th class="table-header-repeat line-left hourlydata" width="30%"><a href="#A">Project Name</th>
						<th class="table-header-repeat line-left hourlydata" width="25%"><a href="#A">Platform</th>
						<th class="table-header-repeat line-left hourlydata" width="15%"><a href="#A">To-do</th>
						<th class="table-header-repeat line-left hourlydata" width="10%"><a href="#A">Done</th>
						<th class="table-header-repeat line-left hourlydata" width="10%"><a href="#A">Left</th>
						<th class="table-header-repeat line-left hourlydata" width="10%" nowrap><a href="#A">Weekly Average</th>
						<th class="table-header-repeat line-left hourlydata" width="10%" nowrap><a href="#A">Required Average</th>
					</thead>
					<tbody>
					
				<?php $i = 1;  if(count($resultData)>0){ 
				
				foreach($resultData as $projectData): ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php  echo $projectData['Project']['project_name']; ?></td>
					<td><?php echo $projectData['Project']['platform']; ?></td>
					<td><?php  echo $projectData['0']['todo_hours']; ?></td>
					<td><?php  $total = $projectData['0']['mon_hour'] + $projectData['0']['tue_hour'] + $projectData['0']['wed_hour'] + $projectData['0']['thur_hour']+    $projectData['0']['fri_hour'] + $projectData['0']['sat_hour'] + $projectData['0']['sun_hour'] ; echo number_format($total,2);?></td>
					<?php $billing_left = $projectData['0']['todo_hours']-$total.",";
					if(date('N')==7){ $billing_alert=$billing_left;}
					else{
					$billing_alert=number_format($billing_left /(7 - (date('N'))),2); 
					}
					if($billing_alert<10){
					?><td><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2); ?></td>
					<?php }
					else if($billing_alert==10||$billing_alert<=11){
					?><td style="background-color: #00CC00;"><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2); ?></td>
					<?php }
					else if($billing_alert==11||$billing_alert<=12){?>
					<td style="background-color: #FFFF66;"><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2);?></td>
					<?php } else if($billing_alert==12||$billing_alert<=13){ ?>
						<td style="background-color: #0099FF;"><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2); ?></td>
					<?php } else if($billing_alert==13||$billing_alert<=14){ ?>
						<td style="background-color: #FF33CC;"><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2);?></td>
					<?php } else if($billing_alert>14){ ?>
						<td style="background-color: #FF0000;"><?php  echo $left = number_format($projectData['0']['todo_hours'] - $total ,2); ?></td>
					<?php } ?>
					<td class="info-tooltip" title="Average of 7 day working"><?php  echo $avrage = number_format($projectData['0']['todo_hours']/7,2); ?></td>
					<td class="info-tooltip viewPaydetails " title="Average of 6 day working"><?php  echo $avrage1 = number_format($projectData['0']['todo_hours']/6,2); ?></td>
					
				</tr>
				<?php  $i++;
				$todo_count[]=$projectData['0']['todo_hours'];
				$total_count[]=$total;
				$left_count[]=$left;
				
				endforeach; 
							
				$todo_sum=array_sum($todo_count);
				$total_sum=array_sum($total_count);
				$left_sum=array_sum($left_count);
				
				
				?>
				
				<tbody>
				<tr>
					<td colspan="3"><center><B>Total</B></center></td>
					
					<td><?php  echo $todo_sum; ?></td>
					<td><?php   echo number_format($total_sum ,2);?></td>
					<td colspan="3"><?php  echo number_format($left_sum ,2); ?></td>
				</tr>
				
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				
				<div style="width:20%;margin-left:50px;">
				<div style="width:10px;height:10px;background-color: #00CC00;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" > Alert(Hours required between 10 to 11) </h5></div></div></br>
				<div style="width:20%;margin-left:50px;">
				<div style="width:10px;height:10px;background-color: #FFFF66;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:280px;" > Mega Alert(Hours required between 11 to 12)</h5></div></div></br>
				<div style="width:16%;margin-left:50px;">	
					<div style="width:10px;height:10px;background-color: #0099FF;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" > Urgent(Hours required between 12 to 13) </h5></div></div></br>
				<div style="width:20%;margin-left:50px;">	
				<div style="width:10px;height:10px;background-color: #FF33CC;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:280px;" >Mega Urgent (Hours required between 13 to 14)</h5></div></div></br>
				<div style="width:20%;margin-left:50px;">	
				<div style="width:10px;height:10px;background-color: #FF0000;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" >Emergency (Hours required more than 14)</h5></div></div></br>
				</div>
				
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->
<?php  exit; ?>