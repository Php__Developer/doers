<?php echo $this->Html->css(array("screen")); ?>
<style type="text/css">
#content{
max-width:100%;
min-width:100%;
}
</style>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php  ?>
			<center><span style="font-weight:bold;"> Payment Delayed Projects</span>
				<table style="margin-top:10px;font-size:12px;margin-left:50px;" border="0" width="80%" cellpadding="0" cellspacing="0" id="product-table">
					<tr>
						<th class="userattend" style="width:40%;height:20px;"><h4>Project Name</h4></th>
						<th class="userattend" style="width:40%;height:20px;"><h4>Payment Date</h4></th>
					</tr>
				<?php if(count($resultData)>0){
				foreach($resultData as $projectData): ?>
				<tr>
					<td><?php echo $projectData['project_name']; ?></td>
					<td><?php echo date("F d,Y",strtotime($projectData['nextpayment'])); ?></td>
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="2" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
			</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->