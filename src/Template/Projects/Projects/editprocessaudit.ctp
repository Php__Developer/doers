<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?><?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>



<script type="text/javascript">
	var base_url = "<?php echo $this->Url->build('/', true); ?>";
	var base_url = '<?php //echo BASE_URL; ?>';
	
	$(document).ready(function() { 
	
	$('a#paylabel').click(function(){
		
		 $("#paytext").toggle();
		 $("#paylabel").toggle();
			
		});
		$('#paytext').click(function(){
		
		  $("#paylabel").toggle();
		 $("#paytext").toggle();
			
		});
	
		//calender 
			$( "#nextrelease" ).datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0
			});
		//calender
			$( "#finishdate" ).datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0
			});
	$('#vooap').checkbox_value();
	$('#is_dropboxback').checkbox_value();
	$('#is_codeback').checkbox_value();
	$('#onalert').checkbox_value();
	$('#dropbox').checkbox_value();
	$('#dailyupdate').checkbox_value();
	/*** code start : remove and append options from team drop down according to above selected users ***/
		 var detachedItem1;
		 var detachedItem2;
		 var detachedItem3;
		 var detachedItem4;
		$('#pm_id').change(function(){
			if(detachedItem1){ 
				detachedItem1.appendTo(".multiSelect option:last");
				detachedItem1 = null;
				sortDropDownListByText('team_id');
			}
			var respID = $('select#pm_id option:selected').val();
			detachedItem1 = null;
			detachedItem1 = $(".multiSelect option[value='"+respID+"']").detach();
		});
		$('#engineeringuser').change(function(){
			if(detachedItem2){ 
				detachedItem2.appendTo(".multiSelect option:last");
				detachedItem2 = null;
				sortDropDownListByText('team_id');
			}
			var acctID = $('select#engineeringuser option:selected').val();
			detachedItem2 = null;
			detachedItem2 = $(".multiSelect option[value='"+acctID+"']").detach();
		});
		$('#salesfrontuser').change(function(){
			if(detachedItem3){ 
				detachedItem3.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem3 = null;
			}
			var ContID = $('select#salesfrontuser option:selected').val();
			detachedItem3 = null;
			detachedItem3 = $(".multiSelect option[value='"+ContID+"']").detach();
		});
		$('#salesbackenduser').change(function(){
			if(detachedItem4){ 
				detachedItem4.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem4 = null;
			}
			var infID = $('select#salesbackenduser option:selected').val();
			detachedItem4 = null;
			detachedItem4 = $(".multiSelect option[value='"+infID+"']").detach();
		});
	
	});
	
	
	$(window).ready(function(){
	$('#ui-datepicker-div').attr('style','display:none');
	});
    var flag=0;
	function editproject(){
		if(validateTeamUsers()){
			var id = $('#project_id').val();
			var pm_id = $('#pm_id').val();
			var technology = $('#technology').val();
			var engineeringuser = $('#engineeringuser').val();
			var salesfrontuser = $('#salesfrontuser').val();
			var salesbackenduser = $('#salesbackenduser').val();
			var nextrelease = $('#nextrelease').val();
			var finishdate = $('#finishdate').val();
			var processnotes = $('#processnotes').val();
			var credentials = $('#credentials').val();
			var dailyupdate = $('#dailyupdate').val();
			var dropbox_foldername = $('#dropbox_foldername').val();
			var dropbox = $('#dropbox').val();
			var vooap = $('#vooap').val();
			var is_dropboxback = $('#is_dropboxback').val();
			var is_codeback = $('#is_codeback').val();
			var onalert = $('#onalert').val();
			var team_id = $('.multiSelect').val();
			if(team_id == null){
				team_id = "";
			}
			if($.active>0){
			}else{
				var base_url = "<?php echo $this->Url->build('/', true); ?>";
				$.post(base_url+'projects/editprocessaudit',{'id':id,'pm_id':pm_id,'technology':technology,'engineeringuser':engineeringuser,'salesfrontuser':salesfrontuser,'salesbackenduser':salesbackenduser,'nextrelease':nextrelease,'finishdate':finishdate,'processnotes':processnotes,'credentials':credentials,'dailyupdate':dailyupdate,'dropbox_foldername':dropbox_foldername,'dropbox':dropbox,'vooap':vooap,'onalert':onalert,'is_dropboxback':is_dropboxback,'is_codeback':is_codeback,'team_id':team_id},function(data){
					if(data==1){
						window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
					}
							if (jQuery.parseJSON(data).status =='failed'){
							$(jQuery.parseJSON(data).errors).each(function( key, value) {
								console.log(value);
								//console.log(key);
								$(value).each(function(k,v){
									
									 /*if(typeof v.team_id !== typeof undefined){

									 	$('.team_id').html('');
									    $('.team_id').append(v.team_id._empty);
									 } */
									 if(typeof v.engineeringuser !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.engineeringuser').html('');
									 	$('.engineeringuser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');

									    $('.engineeringuser').append(v.engineeringuser._empty);
									 } 

									 if(typeof v.pm_id !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.pm_id').html('');
									 	$('.pm_id').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.pm_id').append(v.pm_id._empty);
									 } 
									 if(typeof v.salesfrontuser !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.salesfrontuser').html('');
									 	$('.salesfrontuser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.salesfrontuser').append(v.salesfrontuser._empty);
									 } 

									 if(typeof v.salesbackenduser !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.salesbackenduser').html('');
									 		$('.salesbackenduser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.salesbackenduser').append(v.salesbackenduser._empty);
									 } 

									 if(typeof v.credentials !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.credentials').html('');
									 		$('.credentials').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.credentials').append(v.credentials._empty);
									 } 

								});
								
							});
					}

				});
			}
		}
	}
	
	
	(function( $){
		  $.fn.checkbox_value = function() {
			$(this).click(function() {
					if($(this).is(":checked")){
						$(this).val('1');
					}else{
						$(this).val('0');
					}
				});
		  };
	})( jQuery );
	
		function validateTeamUsers(){
		var team_id = $('.multiSelect').val();
		var pm_id = $('#pm_id').val();
		var engineeringuser = $('#engineeringuser').val();
		var salesfrontuser = $('#salesfrontuser').val();
		var salesbackenduser = $('#salesbackenduser').val();
		if(team_id){
			$.each( team_id, function( key, value ) {
				flag=0;
				if(value == engineeringuser){
					flag=1;
				}else if(value == salesfrontuser){
					flag=2;
				}else if(value == salesbackenduser){
					flag=3;
				}else if(value == pm_id){
					flag=4;
				}
			});
			if(flag>0){
				if(flag==1){
					$('#showteamerror').html("Repetitive accountant user ID in project team.");
				}else if(flag==2){
					$('#showteamerror').html("Repetitive consultant user ID in project team.");
				}else if(flag==3){
					$('#showteamerror').html("Repetitive informer user ID in project team.");
				}else if(flag==4){
					$('#showteamerror').html("Repetitive responsible user ID in project team.");
				}
				$('#showteamerror').attr('style','display:block;color:red;font-size:13px;');
				setTimeout(function() { $('#showteamerror').fadeOut('slow'); }, 5000);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
</script>
	<!--  start content-table-inner -->
	
	
	 <div class="col-sm-9">
            <div class="white-box">

	<?php echo $this->Form->create($ProjectsDataa,array('url' => ['action'=>'editprocessaudit'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
	
	<?php 
         $session = $this->request->session();
      	$this->set('session',$session);


	     $this->Flash->render();
	     $session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0]; ?>
	
<h2 class="font-bold text-success "> Edit Details </h2>
    
			<div class="form-group">
              <label for="exampleInputpwd1">Responsible</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


                   <?php
				$options=$this->General->getuser_name();
			    if(isset($resultData['pm_id']) && $resultData['pm_id'] !== ''){
					$cont_cats = explode(",",$resultData['pm_id']);
				}else{
					$cont_cats = [];
				}  ?>
				
            	<select name="pm_id[]" multiple size='10' class="multiSelect_" id ="pm_id" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select>      

			<?php
				 echo $this->Form->hidden("id",array("id"=>"project_id"));
				 echo $this->Form->hidden("id");
            ?>
</div></div><div class="pm_id"></div>




			<div class="form-group">
              <label for="exampleInputpwd1">Technology</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


			<?php
				   $options=array('other'    =>  'Other', 
											'php'    => 'PHP',
											'phone'    => 'Phone',
											'microsoft'    => 'Microsoft',
											'design'    => 'Design',
											'seo'    => 'Seo',
                      );
			echo $this->Form->input("technology",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"technology","label"=>false,"div"=>false));    
            ?></div></div>





	<div class="form-group">
              <label for="exampleInputpwd1">Accountable</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php
				$options=$this->General->getuser_name();
			    if(isset($resultData['engineeringuser']) && $resultData['engineeringuser'] !== ''){
					$cont_cats = explode(",",$resultData['engineeringuser']);
				}else{
					$cont_cats = [];
				}  ?>
				
            	<select name="engineeringuser[]" multiple size='10' class="multiSelect_" id ="engineeringuser" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select>     
			<div class="engineeringuser"></div>
			</div>
		</div>



	<div class="form-group">
              <label for="exampleInputpwd1">Consultant</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

             <?php
				$options=$this->General->getuser_name();
			    if(isset($resultData['salesfrontuser']) && $resultData['salesfrontuser'] !== ''){
					$cont_cats = explode(",",$resultData['salesfrontuser']);
				}else{
					$cont_cats = [];
				}  ?>
				
            	<select name="salesfrontuser[]" multiple size='10' class="multiSelect_" id ="salesfrontuser" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select> 
			<div class="salesfrontuser" ></div>
	</div>
	</div>

	<div class="form-group">
              <label for="exampleInputpwd1">Informer</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

                   <?php
				$options=$this->General->getuser_name();
			    if(isset($resultData['salesbackenduser']) && $resultData['salesbackenduser'] !== ''){
					$cont_cats = explode(",",$resultData['salesbackenduser']);
				}else{
					$cont_cats = [];
				}  ?>
				
            	<select name="salesbackenduser[]" multiple size='10' class="multiSelect_" id ="salesbackenduser" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select>
			<div class="salesbackenduser" ></div>
			</div>
	</div>		
            	
          

		<?php 
		
		if($pro_name!="" || $pro_type!=""){ ?>
		
			<div class="form-group">
              <label for="exampleInputpwd1">Sale Profile</label><a id="paylabel" href="#A" class="font-bold text-danger pull-right">Password</a>	<div style="display:none;folat:left;margin-left:200px;margin-top:-19px;cursor:pointer;" class="text-danger font-bold" id="paytext"> <?php echo htmlspecialchars($pass);  ?></div>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

                        <?php
			
			$value=$pro_name."  "."(".$pro_type.")";
				
				echo $this->Form->input("profile_id",array( "type"=>'text',"id"=>"profile_id","class"=>"form-control","value"=>$value,"label"=>false,"readonly"=>true,"div"=>false));
            ?>
		
		</div></div>
		<?php }?>
	






	<div class="form-group">
              <label for="exampleInputpwd1">Team</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>



<?php
				$options=$this->General->getuserexceptraci();
			    if(isset($resultData['team_id']) && $resultData['team_id'] !== ''){
					$cont_cats = explode(",",$resultData['team_id']);
				}else{
					$cont_cats = [];
				}  
				 $this->Form->input("team_id",array( "type"=>'select','size'=>'10','multiple' => true,'options'=>$options,"label"=>false,"div"=>false,"class"=>"multiSelect","id"=>"team_id",'value' => $cont_cats));
				  $this->Form->select("team_id", ['type' => 'select','size'=>'10','multiple' => true,"class"=>"multiSelect",'default' => $cont_cats,'options'=>$options,"label"=>false,"div"=>false]) ;?>
            	
            	<select name="team_id[]" multiple size='10' class="multiSelect" id ="team_id" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select> 

           		</div>
           		<div id="showteamerror" style="display:none;"></div>
           		</div>	 
			
		




	<div class="form-group">
              <label for="exampleInputpwd1">Client Password</label>
                  
<?php
				echo $cpass = (isset($cpass['Contact']['cpassword'])) ? $cpass['Contact']['cpassword'] :"No Password ";
           
?>

			</div>








	<div class="form-group">
              <label for="exampleInputpwd1">Next Release</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
             <?php
				 echo $this->Form->input("nextrelease",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"nextrelease","label"=>false,"div"=>false,'value'=> $nextrelease ,($ProjectsDataa->pm_id == $currentuser[0] || in_array(1,explode(',',$currentuser[2])) ) ? "": "disabled" ));

            ?></div></div>





	<div class="form-group">
              <label for="exampleInputpwd1">Last Release</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

               <?php
				 echo $this->Form->input("finishdate",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"finishdate","label"=>false,"div"=>false,'value'=> $finishdate));
            ?></div></div>






	<div class="form-group">
              <label for="exampleInputpwd1">Process Notes</label>
                  
           <?php
				 echo $this->Form->input("processnotes",array('id'=>'processnotes',"label"=>false,"div"=>false,"class"=>"form-control"));
            ?></div>





		<div class="form-group">
              <label for="exampleInputpwd1">Credentials</label><?php
				 echo $this->Form->input("credentials",array('id'=>'credentials',"label"=>false,"div"=>false,"class"=>"form-control"));
            ?></div>
            <div class="credentials"></div>
            	
           




<div class="form-group">
              <label for="exampleInputpwd1">Dropbox folder:</label>


                <?php
				 echo $this->Form->input("dropbox_foldername",array("id"=>"dropbox_foldername","label"=>false,"div"=>false,"class"=>"form-control"));
            ?></div>









<div class="form-group">
              <label for="exampleInputpwd1">Updates:</label>
 <div class="input-group">

		
		
				<?php  $value = isset($ProjectsDataa['dailyupdate']) ? $value=$ProjectsDataa['dailyupdate'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="dailyupdate" name="dailyupdate" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl " for="dailyupdate">Daily Update </label>

		
				<?php $value = isset($ProjectsDataa['dropbox']) ? $value=$ProjectsDataa['dropbox'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="dropbox" name="dropbox" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl " for="dropbox">Dropbox </label>
	
		
				<?php $value = isset($ProjectsDataa['is_dropboxback']) ? $value=$ProjectsDataa['is_dropboxback'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="is_dropboxback" name="is_dropboxback" title="Back up of DropBox needed" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl " for="is_dropboxback">Is_dropboxback </label>
		
		
				<?php $value = isset($ProjectsDataa['is_codeback']) ? $value=$ProjectsDataa['is_codeback'] : $value=''; ?>
				<?php $checked = ($value=='1') ? $checked= "checked=checked " : $checked =''; ?>
				<input type="checkbox" id="is_codeback" name="is_codeback" title="Back up of code needed" value="<?php echo $value; ?>" <?php echo $checked; ?> >
				<label class="projectlbl " for="is_codeback">Is_codeback </label>
		
		
				<?php $value = isset($ProjectsDataa['vooap']) ? $value=$ProjectsDataa['vooap'] : $value=''; ?>
					<?php $checked = ($value == '1') ? $checked= "checked=checked " : $checked =''; ?>
					<input type="checkbox" id="vooap" name="vooap" value="<?php echo $value; ?>" <?php echo $checked; ?> >
					<label class="projectlbl " for="vooap">ERP </label>
	


				<?php $value = isset($ProjectsDataa['onalert']) ? $value=$ProjectsDataa['onalert'] : $value=''; ?>
					<?php $checked = ($value == '1') ? $checked= "checked=checked " : $checked =''; ?>
					<input type="checkbox" id="onalert" name="onalert" value="<?php echo $value; ?>" <?php echo $checked; ?> >
					<label class="projectlbl " for="onalert">On Alert </label>
			
</div>
</div>



			
<div class="form-group">
  	
			<?php echo $this->Form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: editproject();","style"=>"width:80px;","class"=>"btn btn-success")); 
			?>


 </div>
 </div>



<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<!-- 		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 -->

<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
