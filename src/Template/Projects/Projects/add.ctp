<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
<!--  <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script> -->
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() { 
			
			$( "#nextpayment" ).datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0
			});
	
			$( "#nextrelease" ).datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0
			});
	
			$( "#finishdate" ).datepicker({
				dateFormat: 'yy-mm-dd',
				minDate: 0
			});
			
			 $(".paid__").on("keyup", function() {
				$('.left__').val('');
				$('.left__').val($('.tamount').val() - $(this).val());
			});	
			
	});
	function hourly_hide()
			{
			var e = document.getElementById("ProjectEngagagment");
			var strSel = e.options[e.selectedIndex].value 
			if(strSel=="hourly"){
			document.getElementById("ProjectHourlyRate").readOnly = false;
			}
			else
			{ 
			document.getElementById("ProjectHourlyRate").readOnly = true;
			}
		}
	//$('.paid').on('keydown',function(){

</script>
	
		 <div class="col-sm-6">
            <div class="white-box">
	 
<?php echo $this->Form->create('Project',array('url'=>['action'=>'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>


	 <?php $this->Flash->render(); 
	 $session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];  ?>
  <div class="form-group">
              <label for="exampleInputpwd1">Project Name</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
			      	<?php echo $this->Form->input("project_name",array( "class"=>"form-control","label"=>false,"div"=>false));
                   ?></div>

                   <?php if(isset($errors['project_name'])){
							echo $this->General->errorHtml($errors['project_name']);
							} ;?></div>

                    <div class="form-group">
              <label for="exampleInputpwd1">Client Name </label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

			      	<?php
				$options = $this->General->getclients();
				echo $this->Form->input("client_id",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
                ?>
</div>
                <?php if(isset($errors['client_id'])){
							echo $this->General->errorHtml($errors['client_id']);
							} ;?>
               </div>


  <div class="form-group">
              <label for="exampleInputpwd1">Responsible</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

			   
			    	<?php
				
				$options=$this->General->getuser_name();

				 echo $this->Form->input("pm_id",array( "type"=>'select',"id"=>"responsible","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
                ?></div>

                 <?php if(isset($errors['pm_id'])){
							echo $this->General->errorHtml($errors['pm_id']);
							} ;?></div>
               

                 <div class="form-group">
              <label for="exampleInputpwd1">Sale Profile:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
			     	<?php
				$options=$this->General->getprofile();
				
				echo $this->Form->input("profile_id",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));?>	</div>	

				<?php if(isset($errors['profile_id'])){
							echo $this->General->errorHtml($errors['profile_id']);
							} ;?></div>

			

			  <div class="form-group">
              <label for="exampleInputpwd1">Engagagment:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

				<?php
				   $options=array('hourly'    =>  'Hourly', 
								  'fixed'    => 'Fixed',
								  'monthly'    => 'Monthly'
                      );
			echo $this->Form->input("engagagment",array('type'=>'select','options'=>$options,"class"=>"form-control",'onChange'=>'hourly_hide()',"label"=>false,"div"=>false,"id"=>"ProjectEngagagment"));    
            ?>
</div>
            <?php if(isset($errors['engagagment'])){
							echo $this->General->errorHtml($errors['engagagment']);
							} ;?>
</div>
        
		


			  <div class="form-group">
              <label for="exampleInputpwd1">Hourly Rate:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>









			<?php
				 echo $this->Form->input("hourly_rate",array( "class"=>"form-control","label"=>false,"div"=>false,'value'=>0,"id"=>"ProjectHourlyRate"));
            ?></div></div>





			  <div class="form-group">
              <label for="exampleInputpwd1">Platform:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

				<?php
				   $options=array('Upwork'    =>  'Upwork', 
											'elance'    => 'Elance',
											'freelance'    => 'Freelance',
											'other'    => 'Other'
                      );
			echo $this->Form->input("platform",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?>
</div>
            <?php  if(isset($errors['platform'])){
							echo $this->General->errorHtml($errors['platform']);
							} ;?></div>


	  <div class="form-group">
              <label for="exampleInputpwd1">Payment:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>



				<?php
				   $options=array('Upwork'    =>  'Upwork', 
											'elance'    => 'Elance',
											'paypal'    => 'Paypal',
											'cheque'    => 'Cheque',
											'wiretransfer'    => 'Wiretransfer',
											'freelance'    => 'Freelance',
											'other'    => 'Other'
                      );
			echo $this->Form->input("payment",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>

             <?php  if(isset($errors['payment'])){
							echo $this->General->errorHtml($errors['payment']);
							} ;?></div>

         	  <div class="form-group">
              <label for="exampleInputpwd1">Technology:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		
				<?php
				   $options=array('other'    =>  'Other', 
											'php'    => 'PHP',
											'phone'    => 'Phone',
											'microsoft'    => 'Microsoft',
											'design'    => 'Design',
											'seo'    => 'Seo',
                      );
			echo $this->Form->input("technology",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>

            <?php  if(isset($errors['technology'])){
							echo $this->General->errorHtml($errors['technology']);
							} ;?></div>


            
         	  <div class="form-group">
              <label for="exampleInputpwd1">Accountable:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		

				<?php
				

				$options=$this->General->getuser_name();
				 echo $this->Form->input("engineeringuser",array( "type"=>'select',"id"=>"accountable","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
            ?></div>

           <?php  if(isset($errors['engineeringuser'])){
							echo $this->General->errorHtml($errors['engineeringuser']);
							} ;?></div>

            


         	  <div class="form-group">
              <label for="exampleInputpwd1">Consultant:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>



			
				<?php
				
				$options=$this->General->getuser_name();

				 echo $this->Form->input("salesfrontuser",array( "type"=>'select',"id"=>"consultant","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
            ?></div>

           <?php  if(isset($errors['salesfrontuser'])){
							echo $this->General->errorHtml($errors['salesfrontuser']);
							} ;?></div>
          




         	  <div class="form-group">
              <label for="exampleInputpwd1">Informer:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				
				$options=$this->General->getuser_name();

				 echo $this->Form->input("salesbackenduser",array( "type"=>'select',"id"=>"informer","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
            ?></div>

            <?php  if(isset($errors['salesbackenduser'])){
							echo $this->General->errorHtml($errors['salesbackenduser']);
							} ;?></div>





         	  <div class="form-group">
              <label for="exampleInputpwd1">Total Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
			
				<?php
				 echo $this->Form->input("totalamount",array("class"=>"form-control tamount","label"=>false,"div"=>false));
            ?></div>
             <?php  if(isset($errors['totalamount'])){
							echo $this->General->errorHtml($errors['totalamount']);
							} ;?></div>

           
	  <div class="form-group">
              <label for="exampleInputpwd1">Paid Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

<?php
				 echo $this->Form->input("paid",array("class"=>"form-control paid__","label"=>false,"div"=>false));
            ?></div>

          <?php  if(isset($errors['paid'])){
							echo $this->General->errorHtml($errors['paid']);
							} ;?></div>
         
	  <div class="form-group">
              <label for="exampleInputpwd1">Left Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		

				<?php
				 echo $this->Form->input("left",array("class"=>"form-control left__","label"=>false,"div"=>false));
            ?></div>

            <?php  if(isset($errors['left'])){
							echo $this->General->errorHtml($errors['left']);
							} ;?></div>

                    
	  <div class="form-group">
              <label for="exampleInputpwd1">Next Payment</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				 echo $this->Form->input("nextpayment",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"nextpayment","label"=>false,"div"=>false));
            ?></div>
            <?php  if(isset($errors['nextpayment'])){
							echo $this->General->errorHtml($errors['nextpayment']);
							} ;?></div>






         
	  <div class="form-group">
              <label for="exampleInputpwd1">Next Release:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				 echo $this->Form->input("nextrelease",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"nextrelease","label"=>false,"div"=>false));
            ?></div>
            <?php  if(isset($errors['nextrelease'])){
							echo $this->General->errorHtml($errors['nextrelease']);
							} ;?></div>

                   
	  <div class="form-group">
              <label for="exampleInputpwd1">Last Release</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				 echo $this->Form->input("finishdate",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"finishdate","label"=>false,"div"=>false));
            ?></div>

            <?php  if(isset($errors['finishdate'])){
							echo $this->General->errorHtml($errors['finishdate']);
							} ;?></div>


           



         
	  <div class="form-group">
              <label for="exampleInputpwd1">Notes</label>
                  

           <?php
				 echo $this->Form->input("notes",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>

	  <div class="form-group">
              <label for="exampleInputpwd1">Operation Notes</label>
                <?php
				 echo $this->Form->input("operation_notes",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>





   
				

  <div class="form-group">
              <label for="exampleInputpwd1">Credentials</label><span class="pull-right text-danger"> 
                  <?php 
					echo $this->Html->link('View Instructions',
						array('controller'=>'static_pages','action'=>'static_view','prefix'=>'static_pages','key' => '46'),
						array('class'=>'popup_window','alt'=>'Instructions','title'=>'View Instructions')
					);	
				?>
            </span>
                      <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

				<?php
				 echo $this->Form->input("credentials",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>

            <?php if(isset($errors['credentials'])){
							echo $this->General->errorHtml($errors['credentials']);
							} ;?></div>

          







  <div class="form-group">
              <label for="exampleInputpwd1">Updates</label>
                   <div class="input-group">
                       
                     
		
				<?php
					 echo $this->Form->input("dailyupdate",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"dailyupdate",'hiddenField'=>true));
					echo "<label class='projectlbl m-l-30' for ='dailyupdate'>Daily Update </label><br/>";
				?>
		
			<?php
				 echo $this->Form->input("dropbox",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"dropbox",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-30' for ='dropbox'>Dropbox </label><br/>";
            ?>
			
			<?php
				 echo $this->Form->input("is_dropboxback",array('type'=>'checkbox','class'=>'roles',"label"=>false,"div"=>false,"id"=>"is_dropboxback",'hiddenField'=>true,"title"=>"Back up of DropBox needed"));
				echo "<label class='projectlbl m-l-30' for ='is_dropboxback'>Dropbox backup</label><br/>";
            ?>
			
			<?php
				 echo $this->Form->input("is_codeback",array('type'=>'checkbox','class'=>'roles',"label"=>false,"div"=>false,"id"=>"is_codeback",'hiddenField'=>true,"title"=>"Back up of code needed"));
				echo "<label class='projectlbl m-l-30' for ='is_codeback'>Code backup </label><br/>";
            ?>
		
				<?php
					 echo $this->Form->input("vooap",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"vooap",'hiddenField'=>true));
					echo "<label class='projectlbl m-l-30' for ='erp'>ERP </label><br/>";
				?>
			</div>
			</div>





  <div class="form-group">
              <label for="exampleInputpwd1">Process Notes:</label>
                      

                <?php
				 echo $this->Form->input("processnotes",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>





  <div class="form-group">
              <label for="exampleInputpwd1">Dropbox folder:</label>
                      
<?php
				 echo $this->Form->input("dropbox_foldername",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>



  <div class="form-group">
              <label for="exampleInputpwd1">Status:</label>
                         <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				   $options=array('current'    =>  'Current', 
											'onhold'    => 'On Hold',
											'closed'    => 'Closed'
                      );
			echo $this->Form->input("status",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>

             <?php  if(isset($errors['status'])){
							echo $this->General->errorHtml($errors['status']);
							} ;?></div>


   <div class="form-group">
              <label for="exampleInputpwd1">Alert:</label>
          	<?php
				 echo $this->Form->input("onalert",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"onalert",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-30' for ='onalert'>On Alert </label><br/>";
            ?></div></div>





<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'projects','action'=>'add','prefix' => 'projects'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/projects/add', true)));?>

 </div>
 </div>




<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Projects Management</h4>
          This section is used by Admin and PM only to Manage senstive projects.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'projects','action'=>'projectslist'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     <!-- /.right-sidebar -->
</body>
</html>

