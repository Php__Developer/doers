<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<?php echo $this->Html->css(array('oribe')); ?>
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core ?>
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
<script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
<script type="text/javascript">
$(function() {
    $("#bidding_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
    $("#nextfollow_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
<?php echo $this->Html->css(array('oribe')); ?>
	 <div class="col-sm-12">
    <input type="hidden" name="projectid" class="projectid" value="<?php echo $projectid ;?>">
    <input type="hidden" name="currentpage" class="currentpage" value="editprojecteam">
      <a href="#" class="cls_host pull-right"><i class="fa fa-times"></i></a>
      <div class="white-box p-r-0 p-l-0">
          <div class="form-group leadtagsinputwrapper">
          <div class="col-lg-12 col-md-12">
              <h3 class="box-title text-center">View/Shuffle Team For <span class="projectname"></span></h3>
          </div>
                                    <!-- <label class="col-xs-3 control-label"></label> -->
                                    <div class="col-md-12 col-xs-12 col-lg-12 m-t-30 text-center loaderdiv"><i class="fa fa-refresh fa-spin text-center m-t-30" style="font-size:25px;"></i></div>
                                    
                                   
                                     
                    </div>
                    <div class="inpwrapper default_hidden">
                    <?php echo $this->Form->create('Project',array('url'=> ['action' => 'editprojecteam'], 'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>

                        <div class="form-group restagsinputwrapper">
                                    <label class="col-xs-2 control-label">Responsible</label>
                                    <div class="col-xs-10">
                                        <?php echo $this->Form->input("responsible",array("type"=>"text","class"=>"restag51nput responsible other_aliases",'placeholder'=> "To add Resposible", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    
                                     <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-2 control-label"></label>
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div> 
                                    </div>
                                </div>

                                <div class="form-group  acctagsinputwrapper">
                                    <label class="col-xs-2 control-label">Accountable</label>
                                    <div class="col-xs-10">
                                        <?php echo $this->Form->input("accountable",array("type"=>"text","class"=>"acctag51nput accountable other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>   
                                    </div>
                                </div>
                                

                                <div class="form-group  contagsinputwrapper">
                                    <label class="col-xs-2 control-label">Contsultant</label>
                                    <div class="col-xs-10">
                                        <?php echo $this->Form->input("consultant",array("type"=>"text","class"=>"contag51nput consultant other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                   
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>   
                                    </div>
                                </div>
                                

                                <div class="form-group  inftagsinputwrapper">
                                    <label class="col-xs-2 control-label">Informer</label>
                                    <div class="col-xs-10">
                                        <?php echo $this->Form->input("informer",array("type"=>"text","class"=>"inftag51nput informer other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>   
                                    </div>
                                </div>
                                <div class="form-group">
                                <button type="submit" class="btn btn-success savteam">Submit</button>
                                <?php echo $this->Html->link("Back",  ['controller'=>'tags','action'=>'add','prefix' => 'tags'],['class'=>"btn btn-inverse waves-effect waves-light goback"]);  
                                $this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/tags/add', true)));?>
                              </div>
                    </div>
                          

      </div>
            <div class="white-box default_hidden">
<?php echo $this->Form->create('Lead',array('url'=> ['action' => 'add'], 'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php echo $this->Flash->render(); ?>
        <div class="form-group">
            <label for="exampleInputpwd1">Bidding Date*</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->form->input("bidding_date",array("type"=>"text","class"=>"form-control bidding_date","id"=>"bidding_date","label"=>false,"div"=>false));
            ?>

            </div>
            <?php if(isset($errors['bidding_date'])){
            echo $this->General->errorHtml($errors['bidding_date']);
            } ;?>
            <div class="errors"></div>
       </div>
      <div class="form-group">
          <label for="exampleInputpwd1">URL / Title*</label>
          <div class="input-group">
          <div class="input-group-addon"><i class="ti-lock"></i></div>
          <?php
          echo $this->form->input("url_title",array("type"=>"text","class"=>"form-control url_title","label"=>false,"div"=>false));
          ?>
          </div>
          <?php if(isset($errors['url_title'])){
          echo $this->General->errorHtml($errors['url_title']);
          } ;?>
          <div class="errors"></div>
      </div>
      <div class="form-group">
          <label for="exampleInputpwd1">Technology:</label>
          <div class="input-group">
          <div class="input-group-addon"><i class="ti-lock"></i></div>
          <?php
          $options=array('php' =>  'PHP', 
          'phone'    => 'Phone',
          'microsoft'    => 'Microsoft',
          'design'  => 'Design',
          'seo'   => 'SEO'
          );
          echo $this->form->input("technology",array("type"=>"select",'options'=>$options,"class"=>"form-control technology","label"=>false,"div"=>false));
          ?>
          </div>
          <div class="errors"></div>
      </div>
  <div class="form-group">
                         <label for="exampleInputpwd1">Source:</label>
                         <div class="input-group">
                          <div class="input-group-addon"><i class="ti-lock"></i></div>
                 <?php
  			$options=array('upwork' =>  'Upwork', 
  										'elance'    => 'Elance',
  										'guru'    => 'Guru',
  										'freelancer'  => 'Freelancer',
  										'phone'   => 'Phone',
  										'direct'    => 'Direct',
  										'email'    => 'Email',
  										'client reference'  => 'Client Reference',
  										'engineering'   => 'Engineering'
                        );
  				echo $this->form->input("source",array("type"=>"select",'options'=>$options,"class"=>"form-control source","label"=>false,"div"=>false));
              ?>
  </div>
  <div class="errors"></div>
  </div>
<div class="form-group">
  <label for="exampleInputpwd1">Category:</label>
  <div class="input-group">
  <div class="input-group-addon"><i class="ti-lock"></i></div>
  <?php
  $options=array('s' =>  'Silver', 
  's&m'    => 'Gold',
  'm&l'    => 'daimond',
  'I'    => 'platinum'
  );
  echo $this->form->input("category",array("type"=>"select",'options'=>$options,"class"=>"form-control category","label"=>false,"div"=>false));
  echo $this->form->hidden("agency_id",array("class"=>"form-control agency_id","label"=>false,"div"=>false,'value' => $agency_id));
  ?>
  </div>
  <div class="errors"></div>
</div>
<div class="form-group">
      <label for="exampleInputpwd1">Profile On:</label>
      <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
      <?php
      $session_info = $session->read("SESSION_ADMIN");
      $userID = $session_info[0];
      $option = $this->General->getProfilesForLead($userID);
      echo $this->form->input("profile_id",array("type"=>"select",'options'=>$option,"class"=>"form-control profile_id","label"=>false,"div"=>false));

      ?>
      </div>
      <div class="errors"></div>
</div>
  <div class="form-group">
      <label for="exampleInputpwd1">Perspective amount:</label>
      <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
      <?php
      echo $this->form->input("perspective_amount",array("class"=>"form-control perspective_amount","label"=>false,"div"=>false));
      ?>
      </div>
      <?php if(isset($errors['perspective_amount'])){
      echo $this->General->errorHtml($errors['perspective_amount']);
      } ;?>
      <div class="errors"></div>
  </div>
    <div class="form-group">
      <label for="exampleInputpwd1">Status:</label>
      <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
      <?php
      $options=array(
      'active'    => 'active',
      'cold' =>  'cold', 
      'deny'    => 'deny',
      'closed'    => 'closed',
      'success'    => 'success'
      );
      echo $this->form->input("status",array("type"=>"select",'options'=>$options,"class"=>"form-control status","label"=>false,"div"=>false,'disabled' => 'disabled'));
      ?>
      </div>
      <div class="errors"></div>
    </div>
    <div class="form-group">
      <label for="exampleInputpwd1">Next-follow Date:</label>
      <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
      <?php
      echo $this->form->input("nextfollow_date",array("type"=>"text","class"=>"form-control nextfollow_date","id"=>"nextfollow_date","label"=>false,"div"=>false));
      ?>
      </div>
      <div class="errors"></div>
    </div>
    <div class="form-group">
        <label for="exampleInputpwd1">Time Spent:</label><span style="color:red" class="pull-right"> e.g. 40:18  </span>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div>
        <?php
        echo $this->form->input("time_spent",array("type"=>"text","class"=>"form-control time_spent","label"=>false,"div"=>false));
        ?> 
        </div> 
        <?php if(isset($errors['time_spent'])){
        echo $this->General->errorHtml($errors['time_spent']);
        } ;?>
        <div class="errors"></div>
    </div>
	  <div class="form-group"> 
        <label for="inputName" class="control-label">Notes:</label>
        <?php echo $this->form->input("notes",array("type"=>"textarea","class"=>"form-control notes","label"=>false,"div"=>false));?>
        <div class="errors"></div>
    </div>
<div class="form-group">
  <button type="submit" class="btn btn-success addlead">Submit</button>
  <?php 
  echo $this->Html->link("Reset",  ['controller'=>'leads','action'=>'add','prefix' => 'leads'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
  $this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light ",'div'=>false,'onclick'=>$this->Url->build('/leads/add', true)));?>
 </div>
 </div>
</div>




     <!-- /.right-sidebar -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<!--    <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 -->

<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/lead.js"></script>
<script src="<?php echo BASE_URL; ?>js/editlead.js"></script>

      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

