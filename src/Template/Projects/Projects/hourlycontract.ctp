

 <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','function.js')); ?>







<!-- 
<?php echo $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"));
           $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"))
; ?>
<?php //echo $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'))	//ui.core
	echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox'))	//ui.core
?>



<?php echo $this->Html->script(['jquery.selectbox-0.5']); ?>


<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions','tinysort'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?> -->



<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}	
		});
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
			// maxDate:new Date(),
			 firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 0,''];
				}
			 
		 });
	});
	
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
	}	
	
 function weekFrom()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(previous).setDate(previous.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("startdate").value=monthsum2;
		 }
		 function weekFrom1()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(next).setDate(next.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("enddate").value=monthsum2;
		 }
  function nextWeek()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
	var next=new Date(endDate);
	var monthsum= new Date(new Date(previous).setDate(previous.getDate()+6));
			//alert(monthsum);
			var monthsum2=monthsum.toISOString().substr(0,10);
			//alert(monthsum2);
			document.getElementById("startdate").value=monthsum2;
	
  }
 
</script>
<?php
   echo $this->Form->create('Project',array('url' => ['action' => 'report'], 'method'=>'POST', 'controller'=>'projects',   "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm();')); ?>

<!--  start content-table-inner -->


 <div class="col-sm-12">
            <div class="white-box">
	<center><span><h2 class="font-bold text-success"> (Current Week) Hourly Contract Projects</h2></span></center></br>
	<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
	<tr>	
	</br>
	<td style="width:8%;">
		<div style="float:left;margin-top:10px;margin-left:120px;">
		Week Start :
		</div>	
	</td>
		
	<td width="1%">
	<div style="float:left;margin-left:-250px;">
	<?php 
		if($fromDate!=""){
		$st_date=date('Y/m/d',strtotime($fromDate));
		}
		else{
		if(date('N')==7){
		$st_date=date('Y/m/d',strtotime("last monday"));
		}
		else{
		$st_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		}
	}
	 echo $this->Form->input("start_date",array("class"=>"form-control","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>date('Y/m/d',strtotime($st_date))));
	 echo $this->Form->hidden("type",array("class"=>"form-control","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>'hourly'));
     ?>
	</td>
	
	</div>
		
		<td width="5%">
		<div style="float:left;margin-top:0px;">
			<?php echo $this->Form->submit("Search", array('class'=>'btn btn-danger','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
			</div>
			</td>
		</tr>
	</table>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
	  <table >
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $this->Flash->render(); ?>
			
				  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
				   <thead>
                <tr>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">S.No</button></th>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn"> Project Name</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Platform</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">To-do</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">Done</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Left</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Weekly Average</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Required Average</button></th>
                                   </tr>
              </thead>
              <tbody>


				<?php $i = 1;  if(count($resultData)>0){ 
				
				foreach($resultData as $projectData): 
                     //     pr($projectData['Billings']);die();

					?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php  echo $projectData['project_name']; ?></td>
					<td><?php echo $projectData['platform']; ?></td>
					<td><?php  echo number_format($projectData['Billings']['todo_hours'],2); ?></td>
					<td><?php  $total = $projectData['Billings']['mon_hour'] + $projectData['Billings']['tue_hour'] + $projectData['Billings']['wed_hour'] + $projectData['Billings']['thur_hour']+    $projectData['Billings']['fri_hour'] + $projectData['Billings']['sat_hour'] + $projectData['Billings']['sun_hour'] ; echo number_format($total,2);
                         // 

					?></td>
					<?php $billing_left = $projectData['Billings']['todo_hours']-$total;
					if(date('N')==7){ $billing_alert=$billing_left;}
					else{
					$billing_alert=number_format($billing_left /(7 - (date('N'))),2); 
					}
					if($billing_alert<10){
					?><td><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', ''); ?></td>
					<?php }
					else if($billing_alert==10||$billing_alert<=11){
					?><td class="bg-success "><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', ''); ?></td>
					<?php }
					else if($billing_alert==11||$billing_alert<=12){?>
					<td class="bg-info"><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', '');?></td>
					<?php } else if($billing_alert==12||$billing_alert<=13){ ?>
						<td class="bg-info"><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', ''); ?></td>
					<?php } else if($billing_alert==13||$billing_alert<=14){ ?>
						<td class="bg-primary"><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', '');?></td>
					<?php } else if($billing_alert>14){ ?>
						<td class="bg-danger"><?php  echo $left = number_format($projectData['Billings']['todo_hours'] - $total ,2, '.', ''); ?></td>
					<?php } ?>

					<?php
				//	pr($total);die();

					?>
					<td ><a href='javascript:void(0);' class="info-tooltip" title="Average of 6 day working" style="text-decoration:none;"><?php  echo $avrage = number_format($projectData['Billings']['todo_hours']/6,2); ?></a></td>
					<td><a href='javascript:void(0);' class="info-tooltip" title="Average of 7 day working" style="text-decoration:none;"><?php if(date('N')==7){ echo $running_avrage=$left;}
																																				else{ echo $running_avrage=number_format($left/(8-(date('N'))),2);  }
																																				?></a></td>
					<?php
					/*$avrage1=$projectData['todo_hours']-$total;if(date('N')==7){ echo $running_avrage=$avrage1;}
					else{	echo $running_avrage=number_format($avrage1 /(7 - (date('N'))),2);   }*/?>
																																
					
				</tr>
				<?php  $i++;
				$todo_count[]=$projectData['Billings']['todo_hours'];
				$total_count[]=$total;
				$left_count[]=$left;
				
				endforeach; 
							
				$todo_sum=array_sum($todo_count);
				$total_sum=array_sum($total_count);
				$left_sum=array_sum($left_count);
				
				
				?>
				
				<tbody>
				<tr>
					<td colspan="3"><center><B>Total</B></center></td>
					
					<td><?php  echo number_format($todo_sum,2); ?></td>
					<td><?php   echo number_format($total_sum ,2);?></td>
					<td colspan="3"><?php  echo number_format($left_sum ,2); ?></td>
				</tr>
				
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				
			<h5 class="text-success " > Alert(Hours required between 10 to 11) </h5>
				<h5 class="text-warning "> Mega Alert(Hours required between 11 to 12)</h5>
			<h5 class="text-info "> Urgent(Hours required between 12 to 13) </h5>
			<h5 class="text-primary " >Mega Urgent (Hours required between 13 to 14)</h5>
				<h5 class="text-danger ">Emergency (Hours required more than 14)</h5>
				</div>
				
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>
<script>
jQuery(document).ready(function(){
	
	jQuery('.ui-widget-content').css('border','0px');
		});

</script>
	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->

</div></div>
		<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
