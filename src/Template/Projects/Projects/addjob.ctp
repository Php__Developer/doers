<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
<link href="<?php echo BASE_URL; ?>js/tpt/highlight/styles/github.css" id="theme"  rel="stylesheet">
  <?php $url =  $this->Url->build('/', true);
    echo $this->Html->css(array('popup.css'));?>
   <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Add New Project</h3>
            <p class="text-muted m-b-30 font-13">Please fill all required details</p>
                 <div id="exampleValidator" class="wizard">
                    <ul class="wizard-steps" role="tablist">
                        <li class="active" role="tab">
                            <h4><span>1</span>Client</h4>
                        </li>
                        <li role="tab">
                            <h4><span>2</span>Lead</h4>
                        </li>
                        <li role="tab">
                            <h4><span>3</span>Project</h4>
                        </li>
                        <li role="tab">
                            <h4><span>4</span>Credentials</h4>
                        </li>
                        <li role="tab">
                            <h4><span>5</span>Team</h4>
                        </li>
                         <li role="tab">
                            <h4><span>6</span>Engagements</h4>
                        </li>
                    </ul>
                    <form id="validation" class="form-horizontal">
                        <div class="wizard-content">
                            <div class="wizard-pane active" role="tabpanel">
                                <div class="form-group tagsinputwrapper">
                                    <label class="col-xs-3 control-label">Selected Client Name</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("clientname",array("type"=>"text","class"=>"tag51nput client other_aliases",'placeholder'=> "Type in below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false,'vale' => ''));?>
                                    </div>
                                    <div class="col-xs-4">
                                         <span class="ortxt">Or</span> 
                                        <?php
                                        echo $this->Html->link(
                                        '<i class="fa fa-plus"></i>',
                                        array('controller'=>'contacts','action'=>'quickadd','prefix'=>'contacts'),
                                        ['escape' => false,'class' => 'btn btn-info btn-circle popup_window m-l-20','title'=>'Add Client']
                                        );
                                        ?>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                            <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                     <input type="hidden" name="newclientname" class="newclientname" value=""> 
                                          <input type="hidden" name="newclientid" class="newclientid" value="">
                                </div>
                               
                            </div>
                            <div class="wizard-pane" role="tabpanel">
                                <div class="form-group leadtagsinputwrapper">
                                    <label class="col-xs-3 control-label">Selected Lead</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("lead",array("type"=>"text","class"=>"leadtag51nput other_aliases lead",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4">
                                         <span class="ortxt">Or</span> 
                                            <?php
                                            echo $this->Html->link(
                                            '<i class="fa fa-plus"></i>',
                                            array('controller'=>'leads','action'=>'quickadd','prefix'=>'leads'),
                                            ['escape' => false,'class' => 'btn btn-info btn-circle popup_window m-l-20','title'=>'Add Lead']
                                            );
                                            ?>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                            <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                    <input type="hidden" name="newleadname" class="newleadname" value=""> 
                                    <input type="hidden" name="newleadid" class="newleadid" value="">
                                </div>
                               
                            </div>
                            <div class="wizard-pane" role="tabpanel">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Project name</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control project_name" name="project_name" />
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Technology</label>
                                    <div class="col-xs-5">
                                        <div class="input select technology"><select name="technology" class="form-control technology" id="technology"><option value="other">Other</option><option value="php">PHP</option><option value="phone">Phone</option><option value="microsoft">Microsoft</option><option value="design">Design</option><option value="seo">Seo</option></select></div>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Notes</label>
                                    <div class="col-xs-5">
                                        <textarea class="form-control pdes" name="Description" ></textarea>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Status</label>
                                    <div class="col-xs-5">
                                        <div class="input select"><select name="status" class="form-control project_status" id="status"><option value="current">Current</option><option value="onhold">On Hold</option><option value="closed">Closed</option></select></div>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="errors"></div>
                                </div>
                            </div>
                             <div class="wizard-pane" role="tabpanel">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Platform</label>
                                    <div class="col-xs-5">
                                        <select name="source" class="form-control source" id="source"><option value="Upwork">Upwork</option><option value="elance">Elance</option><option value="freelance">Freelance</option><option value="other">Other</option></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Sale Profiles</label>
                                    <div class="col-xs-5 saleprofileswrapper">
                                        <!-- <select name="source" class="form-control" id="source"><option value="Upwork">Upwork</option><option value="elance">Elance</option><option value="freelance">Freelance</option><option value="other">Other</option></select> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Credentials</label>
                                    <div class="col-xs-5">
                                        <textarea class="form-control credentials" name="credentials" ></textarea>
                                        <!-- <select name="source" class="form-control" id="source"><option value="Upwork">Upwork</option><option value="elance">Elance</option><option value="freelance">Freelance</option><option value="other">Other</option></select> -->
                                    </div>
                                </div>
                            </div>
                             <div class="wizard-pane" role="tabpanel">
                                <div class="form-group restagsinputwrapper">
                                    <label class="col-xs-3 control-label">Responsible</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("responsible",array("type"=>"text","class"=>"restag51nput responsible other_aliases",'placeholder'=> "To add Resposible", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4"></div>
                                     <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>

                                <div class="form-group  acctagsinputwrapper">
                                    <label class="col-xs-3 control-label">Accountable</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("accountable",array("type"=>"text","class"=>"acctag51nput accountable other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>
                                

                                <div class="form-group  contagsinputwrapper">
                                    <label class="col-xs-3 control-label">Contsultant</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("consultant",array("type"=>"text","class"=>"contag51nput consultant other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>
                                

                                <div class="form-group  inftagsinputwrapper">
                                    <label class="col-xs-3 control-label">Informer</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("informer",array("type"=>"text","class"=>"inftag51nput informer other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>
                                


                              <!--    <div class="form-group default_hidden inftagsinputwrapper">
                                    <label class="col-xs-3 control-label">Informer</label>
                                    <div class="col-xs-5">
                                        <?php echo $this->Form->input("informer",array("type"=>"text","class"=>"inftag51nput other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="errors"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-xs-3 control-label">Informer Person</label>
                                    <div class="col-xs-5">
                                         <?php  $this->Form->input("informername",array("type"=>"text","class"=>"infnames form-control",'placeholder'=> "Type Here and Select From Suggestions","label"=>false,"div"=>false));?>
                                         <input type="text" class="form-control infnames" name="informername" placeholder="Type Here and Select From Suggestions" />
                                    </div>
                                    <div class="col-xs-4">
                                       
                                    </div>
                                    <div class="errors"></div>
                                </div> -->

                            </div>
                            <div class="wizard-pane" role="tabpanel">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Engagement Type</label>
                                    <div class="col-xs-5">
                                        <select name="engagagment" class="form-control engagagment" id="engagagment"><option value="Fixed">Fixed</option><option value="Hourly">Hourly</option><option value="Monthly">Monthly</option></select>
                                    </div>
                                </div>
                                <div class="form-group default_hidden">
                                    <label class="col-xs-3 control-label">Hourly Rate</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control hourlyrate" name="hourlyrate" />
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Prospective Amount</label>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control prospectiveamount" name="prospectiveamount" />
                                    </div>
                                    <div class="col-xs-4"></div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                         <label class="col-xs-3 control-label"></label>
                                        <div class="col-xs-5"><div class="errors"></div></div>     
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-xs-3 control-label">Payment</label>
                                    <div class="col-xs-5">
                                    <select name="payment" class="form-control payment" id="payment"><option value="Upwork">Upwork</option><option value="elance">Elance</option><option value="paypal">Paypal</option><option value="cheque">Cheque</option><option value="wiretransfer">Wiretransfer</option><option value="freelance">Freelance</option><option value="other">Other</option></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
        
          </div>
        </div>
      </div>
       <?php 
      echo  $this->Html->script(array('jquery-3','jquery-ui/jquery-ui','addjob') );
      ?>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/common.js"></script>
