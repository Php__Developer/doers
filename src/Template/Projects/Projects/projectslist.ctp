
 <link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
 <link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/css-chart/css-chart.css" rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <?php echo $this->Html->css(array("oribe")); ?>
 <?php   echo $this->Html->css(array("oribe")); ?> 
 <?php echo $this->Form->create('Project',array('url' => ['action'=>'projectslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
 <div class="row el-element-overlay m-b-40">
<div id ='dialog_modal' class="dialog_modal default_hidden">
	
</div>
<div class="popupstatus"></div>
 	<?php if($page=="processreport"){?>
                <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                       <ul class="pull-right">
							<?php echo $this->Html->link("Release Delayed",
							array('controller'=>'projects','action'=>'report','delayed'),
							array('class'=>'view_delayed popup_window','alt'=>'delayed','style'=>'color:red;')
							);	
							?>	<?php echo $this->Html->link("On Alert",
							array('controller'=>'projects','action'=>'report','alert'),
							array('class'=>'view_alert popup_window','alt'=>'alert','style'=>'color:red')
							);	
							?><?php echo $this->Html->link("Hourly Contract",
							array('controller'=>'projects','action'=>'report','hourly'),
							array('class'=>'view_onhold popup_window','alt'=>'hourly','style'=>'color:red')
							);	
							?>	<?php echo $this->Html->link("On Hold",
							array('controller'=>'projects','action'=>'holdprojects'),
							array('class'=>'view_onhold popup_window','alt'=>'onhold','style'=>'color:red')
							);	
							?>	<?php echo $this->Html->link("Fixed Contract",
							array('controller'=>'milestones','action'=>'fixedcontract','prefix'=>'milestones'),
							array('class'=>'view_onhold popup_window','alt'=>'onhold','style'=>'color:red')
							);
							?>
                      </ul>
                    </nav>
               </div><!-- /tabs -->
 	<?php }elseif($page == "projectreport"){?>
 	<div class="col-md-12 uppericonslist">
 		<a id="paylabel" href="#A">Show</a>	<div style="display:none;" class="leftlbl" id="paytext">$ <?php echo $left;  ?></div><div class="leftlbl">*****</div>
 		   <div class="sttabs tabs-style-linebox erw">
				<nav class="lis">
				<ul class="pull-right">
					<?php echo $this->Html->link("Verify Billing",
						array('controller'=>'billings','action'=>'verifybilling','prefix' => 'billings'),
						array('class'=>'view_onhold popup_window','alt'=>'delayed','style'=>'color:red;')
					);?>
					<?php echo $this->Html->link("Release Delayed",
						array('controller'=>'projects','action'=>'report','key'=> 'delayed'),
						array('class'=>'view_delayed popup_window','alt'=>'delayed','style'=>'color:red;')
					);?>
					<?php 
						echo $this->Html->link("On Alert",
						array('controller'=>'projects','action'=>'report','key' => 'alert'),
						array('class'=>'view_alert popup_window','alt'=>'alert','style'=>'color:red')
					);?> 	
					<?php
					$total_count=[];
					if(isset($hoursum) && count($hoursum) > 0){
					foreach($hoursum as $billingsum)
					{
					//pr($billingsum)	; die;
					$total = $billingsum['0']['mon_hour'] + $billingsum['0']['tue_hour'] + $billingsum['0']['wed_hour'] + $billingsum['0']['thur_hour']+    $billingsum['0']['fri_hour'] + $billingsum['0']['sat_hour'] + $billingsum['0']['sun_hour'] ;
					$total=$billingsum['todo_hours'];
					$total_count[]=$total;
					}
					}
                    $billingtotal=number_format(array_sum($total_count),0);
					echo $this->Html->link("Hourly Contract",
					array('controller'=>'projects','action'=>'report','key' => 'hourly'),
					array('class'=>'view_onhold popup_window','alt'=>'hourly','style'=>'color:red','title'=>'Sum :'.$billingtotal)
					);?>	
					<?php echo $this->Html->link("On Hold",
					array('controller'=>'projects','action'=>'holdprojects','key' => 'onhold'),
					array('class'=>'view_onhold popup_window','alt'=>'onhold','style'=>'color:red')
					);?>
					<?php echo $this->Html->link("Fixed Contract",
					array('controller'=>'milestones','action'=>'fixedcontract', 'prefix' => 'milestones'),
					array('class'=>'view_onhold popup_window','alt'=>'onhold','style'=>'color:red')
					);?> 	
					<?php echo $this->Html->link("Verify Milestone",
					array('controller'=>'milestones','action'=>'verifyallmilestone' , 'prefix' => 'milestones'),
					array('class'=>'view_onhold popup_window','alt'=>'onhold','style'=>'color:red')
					);?>  
				</ul>
			</nav>
          </div><!-- /tabs -->
	<?php
	}?>
<?php if($searching=="yes"){?>

<div class="displaycount"><span class="counteringdata"> 1 of <?php echo $paginatecount;?></span>  Page(s)</div>
<input type="hidden" value="<?php echo $paginatecount;?>" class="paginate" >
<div class="col-md-12">
	<div class="col-md-3 searchingdata">
		<?php
		$fieldsArray = array(
		// ''				  => 'Search By',
		'Projects.project_name'  =>  'Search By Project Name',
		'Contact.name'  =>  'Search By Client Name',
		'Projects.platform'  =>  'Search By Platform',
		'Projects.status'  =>  'Search By Status',
		);
		echo $this->Form->select("Project.fieldName",$fieldsArray,['value'=> $search1,'class'=>'selectpicker m-b-20 m-r-10 bs-select-hidden','data-style'=>'btn-primary btn-outline'],array("id"=>"searchBy selectopt","label"=>false,"class"=>"form-control","empty"=>false),false); ?>
	</div>
	<div class="col-md-4">

		<?php
		$display1   = "display:none";
		$display2   = "display:none";
		if($search1 != "User.status"){
		$display1 = "display:block";
	}else{
	$display2 = "display:block";
}
echo $this->Form->input("Project.value1",array("id"=>"example-input1-group2 demo-input-search2 searchval","class"=>"form-control searchval","style"=>"$display1", "div"=>false, "label"=> false,"value"=>$search2,"placeholder"=>"Search"));
?>
</div>
<div class="col-md-4">
	<?php
	echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
	echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
	$this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/projects/projectslist', true)));
	?>
	<button class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" aria-expanded="false" data-toggle="dropdown" type="button">
		Sort By
		<span class="caret"></span>
	</button>
	<ul class="dropdown-menu animated drp_list" role="menu">
	    <li>
			<a href="#">Name</a>
		</li>  
		<li>
			<a href="#">Client</a>
		</li>
		<li>
			<a href="#">Platform </a>
		</li>
		<li>
			<a href="#"> Payment</a>
		</li>
		<li>
			<a href="#"> Status </a>
		</li>
		<li>
			<a href="#">Left</a>
		</li>
	</ul>

	<?php 
	echo $this->Html->link('<i class="fa  fa-plus"></i>',
	array('controller'=>'projects','action'=>'addjob'),
	['escape' => false,"class"=>"btn btn-info btn-circle ","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Project"]
	);?>
	<?php 
	echo $this->Html->link('<i class="fa  fa-refresh"></i>',
	array('controller'=>'projects','action'=>'add'),
	['escape' => false,"class"=>"btn btn-info btn-circle reloadstats","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to regenerate stats"]
	);?>
<?php echo $this->Form->end(); ?>
</div>
<div class="col-md-1 paginateproject">
	<ul class="pagination paginate-data">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
	</div>
<?php }?>
<div class="ajax">
    <div class="parent_data">
			<?php if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $result): 

			?>
			<!-- /.usercard -->

			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 alldata">
			 <input type="hidden" value="<?php echo  $this->General->encForUrl($this->General->ENC($result['id']));?>" class="pid">
			 <input type="hidden" value="<?php echo $result['engagagment'] ;?>" class="pengagement">
				<div class="white-box ">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs firstLISt" role="tablist">
						<li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false" title="Project"><span><i class="ti-home" ></i></span></a></li>
						<li role="presentation" class="iprofiletab" style=""><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false" title="Project Data"><span><i class="fa fa-paperclip"></i></span> </a></li>
						<li role="presentation" class="imessagestab" style=""><a href="#imessages" title="Team Members" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false" ><span><i class="fa fa-users"></i></span></a> </li>
						<li role="presentation" class="isettingstab" style=""><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true" title="Engagements"><span><i class="fa fa-money"></i></span></a></li>
						<li role="presentation" class="inotestab" style=""><a href="#inotes" aria-controls="notes" role="tab" data-toggle="tab" aria-expanded="true" title="Notes"><span><i class="fa fa-clipboard"></i></span></a></li>
						<li role="presentation" class="ilogstab" style=""><a href="#ilogs" aria-controls="logs" role="tab" data-toggle="tab" aria-expanded="true" title="Log"><span><i class="fa fa-list-alt"></i></span></a></li>

					</ul>
					<!-- Tab panes  substring($row->parent->category_name,35);  -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane ihome active" >
							<!-- <ul class="list-group "> -->
							<?php 
							//echo $result['client_id']; 
							$clientinfo = $this->General->getclientbyid($result['client_id']);
							$clientname = (isset($clientinfo)) ? $clientinfo['name'] : "None";
							$left = (($result['totalamount']) - ($result['total_payment']));
							if($result['status'] == 'onhold'){
								$classname = 'bg-success';
							} else if($result['status'] == 'current'){
								$classname = 'bg-info';
							} else if($result['status'] == 'closed'){
								$classname = 'bg-danger';
							} 
							if($page=="projectslist" || $page=="projectreport"){ 
							?>
						<input type="hidden" value="<?php echo  $this->General->encForUrl($this->General->ENC($result['client_id']));?>" class="cid">
						<div class="col-md-12 col-xs-12 col-lg-12 col-sm-6 p-l-0 p-r-0">
				          <div class="white-box m-b-0 <?php echo $classname;?>">
				            <h3 class="text-white box-title" title="<?php echo $result['project_name'] ;?>">
				            <?php $str = strip_tags($result['project_name']); 
							echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); 
							 ?>
							<a href="#" class="pull-right statusicon"><span class="pull-right"><?php echo ($result['onalert'] == 1) ? '<i class="fa fa-warning togglealertstatus" title="On Alert"></i>' : '<i class="fa fa-check togglealertstatus" title="Running Smooth"></i>'; ?><!-- <i class="fa fa-check"></i> --></span></a>
							</h3>
				           <div id="<?php echo str_replace('==','',$this->General->encForUrl($this->General->ENC($result['id']) ) ) ;?>__spark" class="text-center sparkline1dash">
				           <canvas ></canvas>
				           </div>
				           <p class="infotxt text-white text-center"></p>
				          </div>
				          <div class="white-box">
				            <div class="row">
				              <div class="pull-left">
				               <div class="text-muted m-t-20" title="<?php echo $clientname; ?>">
				               	<a href="#" class="editcname" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($result['client_id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Client Name">
				                	<?php $str = strip_tags($clientname); 
										echo $name = substr($str,0,18).((strlen($str)>18)? "...":""); 
									?>
							     </a>

				               </div>
				               <h2 >
				              		<?php echo  $result['technology'] ;?>
							    </h2>
				                
				              </div>
				              <?php
				              	if($result['totalamount'] == 0){
				              		$number = 0;
				              		$bar = 0;
				              	} else {
				              		$percentage =  $result['total_payment'] / $result['totalamount'] * 100 ;
					              	$ciel = ceil($percentage);
					              	if($ciel > 100){
					              		$number = 100;
					              		$bar = 100;
					              	} else{
					              		$number = $ciel;
					              		$bar =  ceil( $ciel / 5) * 5;
					              	}
				              	}
				              	

				               ;?>
				              <div data-label="<?php echo $ciel;?>%" class="css-bar css-bar-<?php echo $bar;?> css-bar-lg m-b-0  css-bar-info pull-right"></div>
				            </div>
				          </div>
				        </div>
						<?php
						 
						}elseif($page=="processaudit" || $page =="processreport"){


						?>   

                          <li class="list-group-item list-group-item-danger project"><b>Project :</b> 
									<?php $str = strip_tags($result['project_name']); 
									echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
									    <li class="list-group-item list-group-item-success client"><b>Client :</b>  <?php $str = strip_tags($result['Contact']['name']); 
										echo substr($str,0,19).((strlen($str)>19)? "...":"");  ?></li>
										<li class="list-group-item list-group-item-info plateform"><b>PlateForm:</b> <?php echo $result['platform']; ?></li>
								     <?php $left = (($result['totalamount']) - ($result['total_payment']));
											//echo number_format($left, 2, '.', ''); ?>
									<li class="list-group-item list-group-item-danger status"><b>Updated by:</b>
															<?php if($result['Project']['hours']>= 48) { 
															echo $result['Last_Updatedby']['first_name']." ".$result['Last_Updatedby']['last_name']; 
														} else {
														echo $result['Last_Updatedby']['first_name']." ".$result['Last_Updatedby']['last_name']; 
													} ?>
												</li> 
												<li  class="list-group-item list-group-item-info status">
												<b>Next Release :</b><?php echo date("d/m/Y",strtotime($result['nextrelease']));?>
												</li>
													<?php ($result['onalert']=="1") ? $class = "alert-danger" : $class = "" ;?>
										<li class="list-group-item list-group-item-warning payment <?php echo $class;?>"><b>On Alert:</b><?php
										 $alert= $result['onalert']; 
                                         if($alert==1){
                                            $alert_data="Yes";
                                         }else{
                                         $alert_data="No";
                                         }
                                         echo " ".$alert_data;
                                         ?></li>
							            <?php
            							}?>
								
						<?php if($page =="projectslist"){ ?>

								<ul class="nav nav-tabs list-group-item-info " role="tablist">
												<li>
													<?php
													if($left<0){
														$left_hide="hide_for_left";
														$left_text="Negative Balance Contact Admin";
													}else{
														$left_hide="";$left_text="";
													}
													echo $this->Html->link(
													'<i class="fa fa-cog"></i>',
													array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
													['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Edit']
													);
													?>
											     </li>
														<?php if($result['status'] != 'onhold'){ ?>
												 <li>
														<?php
														echo $this->Html->link(
														'<i class="fa fa-pause"></i>',
														array('controller'=>'projects','action'=>'togglestatus','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline togglestatus','id'=>"$left_hide" ,'title'=>'Put On Hold']
														);
														?>
												 </li>
													<?php }?>
													<?php if($result['status'] != 'current'){ ?>
												  <li>
														<?php
														echo $this->Html->link(
														'<i class="fa fa-play"></i>',
														array('controller'=>'projects','action'=>'togglestatus','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline togglestatus','id'=>"$left_hide" ,'title'=>'Resume Project']
														);
														?>
												   </li>
													<?php }?>
													<?php if($result['status'] != 'closed'){ ?>
												 <li>
														<?php
														echo $this->Html->link(
														'<i class="fa fa-stop"></i>',
														array('controller'=>'projects','action'=>'togglestatus','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline togglestatus','id'=>"$left_hide" ,'title'=>'Close']
														);
														?>
												 </li>
													<?php }?>
													
												
													                                                      
                                                       	<?php 
                                                       if($left_hide !=""){
                                                           if(isset($admin)){
                                                       	 if($admin=="yes"){
                                                       	 ?>
                                                  <li>
	                                                       	 <?php
			                                                echo $this->Html->link(
															'<i class="fa fa-cog"></i>',
															array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
															['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>'','title'=>'Negative Balance Contact Admin']
															);?>
													</li>
														<?php 
                                                       	 }
                                                       	}else{?>
                                                       	<li><h3 class="box-title m-b-0"  style="color:red;padding:6px"><?php echo $left_text;?></h3> </li>
                                                       	<?php
                                                         }
                                                       }
                                                       	?>
                                                       
                                                     <li class="pull-right"><?php

													echo $this->Html->link(
													'Go',
													array('controller'=>'projects','action'=>'projectdetails','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
													['escape' => false,'class' => 'btn default btn-outline font-bold b-all','id'=>$left_hide,'title'=>'Go','target'=>'_blank']
													);?></li>
                                                    </ul>
														<?php
													}elseif($page=="processaudit"){
													 
													?>
													<ul class="nav nav-tabs list-group-item-info" role="tablist"><li>
														<?php   if($left<0){
														$left_hide="hide_for_left";
														$left_text="Negative Balance Contact Admin";
													}else{
													$left_hide="";$left_text="";
												} 
												echo $this->Html->link(
												'<i class="fa fa-cog"></i>',
												array('controller'=>'projects','action'=>'editprocessaudit','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
												['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Edit']
												);?></li>
												<li><?php

													echo $this->Html->link(
													'Go',
													array('controller'=>'projects','action'=>'projectdetails','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
													['escape' => false,'class' => 'btn default btn-outline ','id'=>$left_hide,'title'=>'Go','target'=>'_blank']
													);?></li>
                                                    <li>                                                      
                                                       	<?php 
                                                       if($left_hide !=""){
                                                           if(isset($admin)){
                                                       	 if($admin=="yes"){
                                                       	 ?><?php
		                                                echo $this->Html->link(
														'<i class="fa fa-cog"></i>',
														array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>'','title'=>'Negative Balance Contact Admin']
														);
                                                       	 }
                                                       	}else{?>
                                                       	<h3 class="box-title m-b-0"  style="color:red;padding:6px"><?php echo $left_text;?></h3> 
                                                       	<?php
                                                         }
                                                       }
                                                       	?>
                                                       </li>														
											</ul>

											<?php }elseif($page=="processreport"){
											
											?>
											<ul class="nav nav-tabs list-group-item-info" role="tablist">
												<li>
													<input type="hidden" value="<?php echo  $this->General->encForUrl($this->General->ENC($result['id']));?>" class="billing_id">
													<?php
													$left = $result['Project']['totalamount']-$result['Project']['total_payment']; 
													if($left<0){  $left_hide="hide_for_left";
													$left_text="Negative Balance Contact Admin";
												}else
												{ $left_hide="";$left_text="";
											} 
											if($result['engagagment']=="hourly" && $result['verify']=="1") {
											echo $this->Html->link(
											'<i class="fa fa-behance-square"></i>',
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
											['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Add Billing']
											);
										}elseif($result['engagagment']=="hourly"  && $result['verify']=="0") {
										echo $this->Html->link(
										'<i class="fa fa-behance"></i>',
										array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
										['escape' => false,'class' => 'btn default btn-outline popup_window ','id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()']
										);}

										?></li>
										<li><?php
											echo $this->Html->link(
											'<i class="ti-pinterest-alt"></i>',
											array('controller'=>'billings','action'=>'quickview','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
											['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'View Billings']
											);
											?></li>
											<li><?php echo $this->Html->link(
												'<i class="fa fa-cog"></i>',
												array('controller'=>'projects','action'=>'editprocessaudit','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
												['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Edit']
												);
												?></li>	
												<li><?php 	 echo $this->Html->link('Go',
													array('controller'=>'projects','action'=>'projectdetails','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
													['escape' => false,'class' => 'btn default btn-outline ','id'=>$left_hide,'title'=>'Go','target'=>'_blank']
													);
													?></li>
                                                      <li>                                                      
                                                       	<?php 
                                                       if($left_hide !=""){
                                                           if(isset($admin)){
                                                       	 if($admin=="yes"){
                                                       	 ?><?php
		                                                echo $this->Html->link(
														'<i class="fa fa-cog"></i>',
														array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>'','title'=>'Negative Balance Contact Admin']
														);
                                                       	 }
                                                       	}else{?>
                                                       	<h3 class="box-title m-b-0"  style="color:red;padding:6px"><?php echo $left_text;?></h3> 
                                                       	<?php
                                                         }
                                                       }
                                                       	?>
                                                       </li>											
											</ul>
											<?php }elseif($page=="projectreport"){
											?>
											<ul class="nav nav-tabs list-group-item-info " role="tablist"><li> <?php if($left<0){
												$left_hide="hide_for_left";
												$left_text="Negative Balance Contact Admin";
											}else
											{ $left_hide="";$left_text="";
										} 
										echo $this->Html->link(
										'<i class="fa fa-usd"></i>',
										array('controller'=>'payments','action'=>'quickadd','prefix'=>'payments','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
										['escape' => false,'class' => 'btn default btn-outline  popup_window ','id'=>$left_hide,'title'=>'Add Payment',]
										);
										?></li>
										<li>
											<?php
											if($result['engagagment']=="hourly" && $result['verify']=="1") {             
											echo $this->Html->link(
											'<i class="fa fa-behance-square"></i>',
											array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
											['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Add Billing']
											);
										}elseif($result['engagagment']=="hourly"  && $result['verify']=="0") {
										echo $this->Html->link(
										'<i class="fa fa-behance"></i>',
										array('controller'=>'billings','action'=>'quickadd','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
										['escape' => false,'class' => 'btn default btn-outline popup_window ','id'=>$left_hide,'title'=>'verify Billing','onClick'=>'veryfycheck()']
										);}
										?></li>
										<li>
											<?php
											echo $this->Html->link(
											'<i class="fa fa-scribd"></i>',
											array('controller'=>'payments','action'=>'quickview','prefix'=>'payments','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
											['escape' => false,'class' => 'btn default btn-outline popup_window' ,'id'=>$left_hide,'title'=>'View Payment']
											);
											?></li>
											<li><?php
												echo $this->Html->link(
												'<i class="ti-pinterest-alt"></i>',
												array('controller'=>'billings','action'=>'quickview','prefix'=>'billings','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
												['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'View Billings']
												);
												?></li>
												<li><?php
													echo $this->Html->link(
													'<i class="fa fa-cog"></i>',
													array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
													['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Edit']
													);
													?></li>
													<li> <?php
														if($result['engagagment']=="fixed"||$result['engagagment']=="monthly") {
														echo $this->Html->link(
														'<i class="fa fa-weibo"></i>',
														array('controller'=>'milestones','action'=>'milestoneslist','prefix'=> 'milestones','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>$left_hide,'title'=>'Milestone']
														);
													}
													?></li>
													<li>
														<?php
														echo $this->Html->link(
														'Go',
														array('controller'=>'projects','action'=>'projectdetails','prefix'=> 'projects','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline ','id'=>$left_hide,'title'=>'Go','target'=>'_blank']
														);?>
													</li>
                                               <li>                                                      
                                                       	<?php 
                                                       if($left_hide !=""){
                                                           if(isset($admin)){
                                                       	 if($admin=="yes"){
                                                       	 ?><?php
		                                                echo $this->Html->link(
														'<i class="fa fa-cog"></i>',
														array('controller'=>'projects','action'=>'quickedit','id'=> $this->General->encForUrl($this->General->ENC($result['id']))),
														['escape' => false,'class' => 'btn default btn-outline popup_window','id'=>'','title'=>'Negative Balance Contact Admin']
														);
                                                       	 }
                                                       	}else{?>
                                                       	<h3 class="box-title m-b-0"  style="color:red;padding:6px"><?php echo $left_text;?></h3> 
                                                       	<?php
                                                         }
                                                       }
                                                       	?>
                                                       </li>														<?php }?>
                                                   </ul>
												</div>
												<div role="tabpanel" class="tab-pane iprofile">
													<div class="row">
														 <div class="col-md-12 ">
														 		<div class="docsandnots"></div>
														 </div>	
													</div>
												</div>
												<div role="tabpanel" class="tab-pane imessages">
													<div class="row">
														 <div class="col-md-12 ">
														 		<div class="teamwrapper"></div>
														 </div>	
													</div>	
													<div class="clearfix"></div>
												</div>
												<div role="tabpanel" class="tab-pane isettings">
													<div class="row">
														 <div class="col-md-12 ">
														 		<div class="paymentswrapper"></div>
														 </div>	
													</div>
													<div class="clearfix"></div>
												</div>
												<div role="tabpanel" class="tab-pane inotes">
													<div class="row">
														 <div class="col-md-12 ">
														 		<div class="noteswrapper"></div>
														 </div>	
													</div>
													<div class="clearfix"></div>
												</div>
												<div role="tabpanel" class="tab-pane ilogs">
													<div class="row">
														<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 ">
														<div class="logswrapper">
															 	
														</div>
												        </div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<?php $i++ ;
									endforeach; ?>
									<?php } else { ?>
                                      <div class="error-body text-center"><h1>404</h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30"></p>  </div>
									<?php
								}
								?>
							</div>
							<!-- /.usercard-->
						</div>
						<input type="hidden" name="page" class="currentpage" value="<?php echo $page;?>">



						<!-- /.row -->
						<?php 
						echo  $this->Html->script(array('jquery-3','jquery-ui/jquery-ui','oribe'));
						?>

				<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
				
				<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
				
				<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
				<script src="<?php echo BASE_URL; ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
				<script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

						<!-- Magnific popup JavaScript -->

						
						<script type="text/javascript">

							function veryfycheck()
							{
								alert("1) Either billing for this project is already added for this week."+'\n'+
									"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
							}

							(function() {

								[].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
									new CBPFWTabs( el );
								});

							})();
						</script>
