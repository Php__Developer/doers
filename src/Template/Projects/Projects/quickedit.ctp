<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
	<!--  start content-table-inner -->
		 <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="white-box">
 
<?php 

echo $this->Form->create($ProjectsData,array('url' => ['action'=>'quickedit'],'method'=>'POST', "class" => "login", "name" => "listForm", "id" => "mainform")); ?>

 <?php 
         $session = $this->request->session();
      	$this->set('session',$session);

	     $this->Flash->render();
	     $session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0]; ?>
	
	<h2 class="font-bold text-success text-center"> Edit Project Details </h2>
			<div class="form-group">
				<label for="exampleInputpwd1">Project Name</label>
				<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div><?php
					echo $this->Form->input("project_name",array( "class"=>"form-control","label"=>false,"div"=>false,'id' => 'project_name'));
					echo $this->Form->hidden("id",array("id"=>"project_id"));
					echo $this->Form->hidden("id");
					echo $this->Form->hidden("clientUpdate",array('id'=>'client_update'));
					?>
				</div>
				<?php if(isset($errors['project_name'])){
				echo $this->General->errorHtml($errors['project_name']);
				} ;?>
			</div>
		<!--   <div class="form-group">
          <label for="exampleInputpwd1">Client Name </label>
               <div class="input-group">
                    <div class="input-group-addon"><i class="ti-lock"></i></div>
					<?php $id = $ProjectsData['client_id'];
					$options=$this->General->getclients($id);
					echo $this->Form->input("client_id",array( "type"=>'select',"class"=>"form-control",'id'=> 'client_id','options'=>$options,"label"=>false,"div"=>false));
					?>
            </div>
            <div class="client_id" > </div>
          </div> -->
<!-- 		  <div class="form-group">
				<label for="exampleInputpwd1">Responsible</label>
				<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div>
					<?php
					$options=$this->General->getuser_name();
					echo $this->Form->input("pm_id",array( "type"=>'select',"class"=>"form-control",'id'=> 'pm_id','options'=>$options,"label"=>false,"div"=>false));
					?>
				</div>
				<?php if(isset($errors['pm_id'])){
				echo $this->General->errorHtml($errors['pm_id']);
				} ;?>
				<div class="pm_id" ></div>
		  </div> -->
			<!-- <div class="form-group">
				<label for="exampleInputpwd1">Sale Profile:</label><a id="paylabel" href="#A" class="text-danger pull-right font-bold">Show Details</a>	<div style="display:none;folat:left;margin-left:200px;margin-top:-19px;cursor:pointer;" class="text-danger font-bold" id="paytext"> <?php echo $nameofprofile.", ".htmlspecialchars($pass);  ?></div>
				<div class="input-group">
					<div class="input-group-addon"><i class="ti-lock"></i></div>
					<?php $options=$this->General->getprofileps();
					echo $this->Form->input("profile_id",array( "type"=>'select',"id"=>"profile_id","class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));?>
				</div>
				<?php if(isset($errors['profile_id'])){
				echo $this->General->errorHtml($errors['profile_id']);
				} ;?>
			</div> -->
        <!--   <div class="profile_id" ></div> -->
		 <!--  <div class="form-group">
				<label for="exampleInputpwd1">Engagagment:</label>
				<div class="input-group">
					<div class="input-group-addon"><i class="ti-lock"></i></div><?php
					$options=array('hourly'    =>  'Hourly', 
					'fixed'    => 'Fixed',
					'monthly'    => 'Monthly'
					);
					echo $this->Form->input("engagagment",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
					?>
				</div>
				<?php if(isset($errors['engagagment'])){
				echo $this->General->errorHtml($errors['engagagment']);
				} ;?>
		</div> -->
	<!-- 	<div class="form-group">
			<label for="exampleInputpwd1">Hourly Rate:</label>
			<div class="input-group">
			<div class="input-group-addon"><i class="ti-lock"></i></div><?php
			echo $this->Form->input("hourly_rate",array("class"=>"form-control",'id'=>'hourly_rate',"style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
			?></div>
		</div> -->
<!-- 		<div class="form-group">
			<label for="exampleInputpwd1">Platform:</label>
			<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				$options=array('upwork'    =>  'Upwork', 
				'elance'    => 'Elance',
				'freelance'    => 'Freelance',
				'other'    => 'Other'
				);
				echo $this->Form->input("platform",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
				?>
			</div>
			<?php  if(isset($errors['platform'])){
			echo $this->General->errorHtml($errors['platform']);
			} ;?>
		</div> -->
<!-- 		<div class="form-group">
			<label for="exampleInputpwd1">Payment:</label>
			<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				$options=array('upwork'    =>  'Upwork', 
				'elance'    => 'Elance',
				'paypal'    => 'Paypal',
				'cheque'    => 'Cheque',
				'wiretransfer'    => 'Wiretransfer',
				'freelance'    => 'Freelance',
				'other'    => 'Other'
				);
				echo $this->Form->input("payment",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
				?>
			</div>
			<?php  if(isset($errors['payment'])){
			echo $this->General->errorHtml($errors['payment']);
			} ;?>
		</div> -->
		  <div class="form-group">
				<label for="exampleInputpwd1">Technology:</label>
				<div class="input-group">
					<div class="input-group-addon"><i class="ti-lock"></i></div><?php
					$options=array('other'    =>  'Other', 
									'php'    => 'PHP',
									'phone'    => 'Phone',
									'microsoft'    => 'Microsoft',
									'design'    => 'Design',
									'seo'    => 'Seo',
					);
					echo $this->Form->input("technology",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
					?>
				</div>
				<?php  if(isset($errors['technology'])){
				echo $this->General->errorHtml($errors['technology']);
				} ;?>
			</div>
          	 <!--  <div class="form-group">
					<label for="exampleInputpwd1">Accountable:</label>
					<div class="input-group">
					<div class="input-group-addon"><i class="ti-lock"></i></div><?php

					$options=$this->General->getuser_name();

					echo $this->Form->input("engineeringuser",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false)); ?>
					</div>
					<?php  if(isset($errors['engineeringuser'])){
					echo $this->General->errorHtml($errors['engineeringuser']);
					} ;?>
				</div> -->
         <!--  <div class="engineeringuser"></div> -->
<!-- 		  <div class="form-group">
              <label for="exampleInputpwd1">Consultant:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
			
				$options=$this->General->getuser_name();
				 echo $this->Form->input("salesfrontuser",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));?>
</div>
            <?php  if(isset($errors['salesfrontuser'])){
							echo $this->General->errorHtml($errors['salesfrontuser']);
							} ;?></div>
				<div class="salesfrontuser"></div> -->	

          


			
<!--          	  <div class="form-group">
              <label for="exampleInputpwd1">Informer:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
				
				 $options=$this->General->getuser_name();

				 echo $this->Form->input("salesbackenduser",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false)); ?>
				 </div>
				 <?php  if(isset($errors['salesbackenduser'])){
							echo $this->General->errorHtml($errors['salesbackenduser']);
							} ;?></div>
					<div class="salesbackenduser" ></div>	 -->

            
<!--          	  <div class="form-group">
              <label for="exampleInputpwd1">Team:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
              

 <select name="team_id[]" multiple size='10' class="multiSelect" id ="team_id" >
                <?php foreach($options as $key => $value) {;//pr($value);die;?>

                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?>        <?php// pr((in_array($key, $cont_cats)));die; ?></option>
                 <?php };?>
                </select> 


</div>
</div> -->

<!-- <div id="showteamerror"></div> -->



				  <div class="form-group">
              <label for="exampleInputpwd1">Total Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				 echo $this->Form->input("totalamount",array("class"=>"form-control totalamount","label"=>false,"div"=>false));
            ?></div>

             <?php  if(isset($errors['totalamount'])){
							echo $this->General->errorHtml($errors['totalamount']);
							} ;?></div>
				<div class="totalamount"></div>

           










        	  <!-- <div class="form-group">
              <label for="exampleInputpwd1">Next Payment</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				echo $this->Form->input("nextpayment",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"nextpayment","style"=>"","label"=>false,"div"=>false,'value'=> $nextpayment));
            ?></div>
            	<?php  if(isset($errors['nextpayment'])){
							echo $this->General->errorHtml($errors['nextpayment']);
							} ;?></div>
				<div class="nextpayment"></div>

            






              <div class="form-group">
              <label for="exampleInputpwd1">Next Release:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				 echo $this->Form->input("nextrelease",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"nextrelease","style"=>"","label"=>false,"div"=>false,'value'=> $nextrelease));
            ?></div>
            	<?php  if(isset($errors['nextrelease'])){
							echo $this->General->errorHtml($errors['nextrelease']);
							} ;?></div>
				<div class="nextrelease"></div>











	  <div class="form-group">
              <label for="exampleInputpwd1">Last Release</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				echo $this->Form->input("finishdate",array("type"=>'text',"class"=>"form-control","readonly"=>true,"id"=>"finishdate","style"=>"","label"=>false,"div"=>false,'value'=> $finishdate));
            ?></div>
            	<?php  if(isset($errors['finishdate'])){
							echo $this->General->errorHtml($errors['finishdate']);
							} ;?></div>
				<div class="finishdate"></div> -->







	  		<div class="form-group">
              <label for="exampleInputpwd1">Notes</label><?php
				 echo $this->Form->input("notes",array("class"=>"form-control","label"=>false,"div"=>false));
				 echo $this->Form->hidden("id");
            ?></div>









 <div class="form-group">
              <label for="exampleInputpwd1">Operation Notes:</label>

		<?php
				 echo $this->Form->input("operation_notes",array("class"=>"form-control", 'id'=> 'operation_notes',"label"=>false,"div"=>false));
            ?></div>







		

 <div class="form-group">
              <label for="exampleInputpwd1">Credentials:</label>
                 

                        <?php
				 echo $this->Form->input("credentials",array("class"=>"form-control",'id'=>'credentials',"label"=>false,"div"=>false));
            ?></div>

            	<div class="credentials"></div>
            
           
<!--   <div class="form-group">
              <label for="exampleInputpwd1">Updates</label>
                   <div class="input-group">
                       
			
				<?php
					 echo $this->Form->input("dailyupdate",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"dailyupdate",'hiddenField'=>true));
					echo "<label class='projectlbl m-l-40' for ='dailyupdate'>Daily Update </label><br/>";
				?>
			
			
			<?php
				 echo $this->Form->input("dropbox",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"dropbox",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-40' for ='dropbox'>Dropbox </label><br/>";
            ?>
			
			<?php
				 echo $this->Form->input("is_dropboxback",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"is_dropboxback",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-40' for ='is_dropboxback'>Master Sheet </label><br/>";
            ?>
			
			<?php
				 echo $this->Form->input("is_codeback",array('type'=>'checkbox','class'=>'roles',"label"=>false,"div"=>false,"id"=>"is_codeback",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-40' for ='is_codeback'>Is_backup </label><br/>";
            ?>
			
				<?php
					 echo $this->Form->input("vooap",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"vooap",'hiddenField'=>true));
					echo "<label class='projectlbl m-l-40' for ='vooap'>Vooap </label><br/>";
				?>
			
			</div></div> -->








		  <div class="form-group">
              <label for="exampleInputpwd1">Process Notes:</label>
                      <?php
				 echo $this->Form->input("processnotes",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>







		
<!--   <div class="form-group">
              <label for="exampleInputpwd1">Dropbox folder:</label>
              <?php
				 echo $this->Form->input("dropbox_foldername",array("id"=>"dropbox_foldername","style"=>"height:31px;width:198px;","label"=>false,"div"=>false,"class"=>"form-control"));
            ?></div> -->







 <!--  <div class="form-group">
              <label for="exampleInputpwd1">Status:</label>
                         <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				   $options=array('current'    =>  'Current', 
											'onhold'    => 'On Hold',
											'closed'    => 'Closed'
                      );
			echo $this->Form->input("status",array('type'=>'select',"id"=>"projectStatus",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>

            <?php  if(isset($errors['status'])){
							echo $this->General->errorHtml($errors['status']);
							} ;?></div> -->

							<!-- <div class="status"></div>

 -->







<!-- 
   <div class="form-group">
              <label for="exampleInputpwd1">Alert:</label><?php
				 echo $this->Form->input("onalert",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"onalert",'hiddenField'=>true));
				echo "<label class='projectlbl m-l-40' for ='onalert'>On Alert </label><br/>";
            ?></div> -->





			
<div class="form-group">
  	
			<?php echo $this->Form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: editproject();","style"=>"width:80px;","class"=>"btn btn-success")); 
			?>


 </div>
 </div>


		
			
			


<?php 
    echo  $this->Html->script(array('jquery-3','migrate','listing','project'));
    
?>
<script type="text/javascript">
	var base_url = "<?php echo $this->Url->build('/', true); ?>";
	var base_url = '<?php //echo BASE_URL; ?>';
	
	$(document).ready(function() { 
	
	$('a#paylabel').click(function(){
		
		 $("#paytext").toggle();
		 $("#paylabel").toggle();
			
		});
		$('#paytext').click(function(){
		
		  $("#paylabel").toggle();
		 $("#paytext").toggle();
			
		});
		
	$('#vooap').checkbox_value();
	$('#is_dropboxback').checkbox_value();
	$('#is_codeback').checkbox_value();
	$('#onalert').checkbox_value();
	$('#dropbox').checkbox_value();
	$('#dailyupdate').checkbox_value();
	/*** code start : remove and append options from team drop down according to above selected users ***/
		 var detachedItem1;
		 var detachedItem2;
		 var detachedItem3;
		 var detachedItem4;
		$('#pm_id').change(function(){
			if(detachedItem1){ 
				detachedItem1.appendTo(".multiSelect option:last");
				detachedItem1 = null;
				sortDropDownListByText('team_id');
			}
			var respID = $('select#pm_id option:selected').val();
			detachedItem1 = null;
			detachedItem1 = $(".multiSelect option[value='"+respID+"']").detach();
		});
		$('#engineeringuser').change(function(){
			if(detachedItem2){ 
				detachedItem2.appendTo(".multiSelect option:last");
				detachedItem2 = null;
				sortDropDownListByText('team_id');
			}
			var acctID = $('select#engineeringuser option:selected').val();
			detachedItem2 = null;
			detachedItem2 = $(".multiSelect option[value='"+acctID+"']").detach();
		});
		$('#salesfrontuser').change(function(){
			if(detachedItem3){ 
				detachedItem3.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem3 = null;
			}
			var ContID = $('select#salesfrontuser option:selected').val();
			detachedItem3 = null;
			detachedItem3 = $(".multiSelect option[value='"+ContID+"']").detach();
		});
		$('#salesbackenduser').change(function(){
			if(detachedItem4){ 
				detachedItem4.appendTo(".multiSelect option:last");
				sortDropDownListByText('team_id');
				detachedItem4 = null;
			}
			var infID = $('select#salesbackenduser option:selected').val();
			detachedItem4 = null;
			detachedItem4 = $(".multiSelect option[value='"+infID+"']").detach();
		});
	});
	
	
	
	$(window).ready(function(){
	$('#ui-datepicker-div').attr('style','display:none');
	});
	var flag=0;
	/*function confirmAction(msg,key){
			var action = confirm(msg);
			if(action == true){
				$('#client_update').val(key);
			}
				return true;		
	}*/	
	function editproject(){

		if(validateTeamUsers()){
			var status = $('#projectStatus').val();
		//	alert('hii');
			/*if(status == 'closed'){
				var msg = "Are you want to deactivate this Project client?"
				confirmAction(msg,"deact");
			}else if(status =="current" && (old_status == "closed")){
				var msg = "Are you want to activate this Project client?"
				confirmAction(msg,"act");
			}*/
			var team_id = $('.multiSelect').val();
			if(team_id == null){
				team_id = "";
			}
			var id = $('#project_id').val();
			var project_name = $('#project_name').val();
			//var client_id = $('#client_id').val();
			//var pm_id = $('#pm_id').val();
			//var profile_id = $('#profile_id').val();
			//var engagagment = $('#engagagment').val();
			//var hourly_rate = $('#hourly_rate').val();
			//var platform = $('#platform').val();
			var credentials = $('#credentials').val();
			//var payment = $('#payment').val();
			var technology = $('#technology').val();
			//var engineeringuser = $('#engineeringuser').val();
			//var salesfrontuser = $('#salesfrontuser').val();
			//var salesbackenduser = $('#salesbackenduser').val();
			var totalamount = $('.totalamount').val();
			//var paid = $('#paid').val();
			//var left = $('#left').val();
			//var nextpayment = $('#nextpayment').val();
			//var nextrelease = $('#nextrelease').val();
			//var finishdate = $('#finishdate').val();
			var notes = $('#notes').val();
			var operation_notes = $('#operation_notes').val();
			//var dailyupdate = $('#dailyupdate').val();
			//var dropbox_foldername = $('#dropbox_foldername').val();
			//var dropbox = $('#dropbox').val();
			//var vooap = $('#vooap').val();
			//var is_dropboxback = $('#is_dropboxback').val();
			//var is_codeback = $('#is_codeback').val();
			//var onalert = $('#onalert').val();
			var processnotes = $('#processnotes').val();
//			var client_update = $('#client_update').val();
			if($.active>0){
			}else{
				var base_url = "<?php echo $this->Url->build('/', true); ?>";
				//postdata = {'id':id,'project_name':project_name,'client_id':client_id,'pm_id':pm_id,'profile_id':profile_id,'engagagment':engagagment,'hourly_rate':hourly_rate,'platform':platform,'payment':payment,'technology':technology,'engineeringuser':engineeringuser,'salesfrontuser':salesfrontuser,'salesbackenduser':salesbackenduser,'totalamount':totalamount,'nextpayment':nextpayment,'nextrelease':nextrelease,'finishdate':finishdate,'notes':notes,'dailyupdate':dailyupdate,'dropbox_foldername':dropbox_foldername,'dropbox':dropbox,'vooap':vooap,'is_dropboxback':is_dropboxback,'is_codeback':is_codeback,'operation_notes':operation_notes,'onalert':onalert,'processnotes':processnotes,'status':status,'client_update':client_update,'credentials':credentials,'team_id':team_id}
				postdata = {'id':id,'project_name':project_name,'technology':technology,'notes':notes,'operation_notes':operation_notes,'processnotes':processnotes,'credentials':credentials,'totalamount' : totalamount}
				$.post(base_url+'projects/quickedit',postdata,function(data){
						//console.log(data);

				if(data==1){
					window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
				  }
					//validation through ajax
					if (jQuery.parseJSON(data).status =='failed'){
							$(jQuery.parseJSON(data).errors).each(function( key, value) {
								console.log(value);
								//console.log(key);client_id
								$(value).each(function(k,v){
									//$('.'+v).html('');
									//$('.'+v).append(value._empty);
									 //console.log(v.team_id);client_id
									  if(typeof v.client_id !== typeof undefined){
                                          swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.client_id').html('');
										$('.client_id').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.client_id').append(v.client_id._empty);
									 } 

									 if(typeof v.profile_id !== typeof undefined){
				 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.profile_id').html('');
									 	$('.profile_id').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.profile_id').append(v.profile_id._empty);
									 } 
									/* if(typeof v.team_id !== typeof undefined){

									 	$('.team_id').html('');
									    $('.team_id').append(v.team_id._empty);
									 } */
									 if(typeof v.engineeringuser !== typeof undefined){
 			swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.engineeringuser').html('');
									 		$('.engineeringuser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.engineeringuser').append(v.engineeringuser._empty);
									 } 

									 if(typeof v.pm_id !== typeof undefined){
 					swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.pm_id').html('');
									 	$('.pm_id').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.pm_id').append(v.pm_id._empty);
									 } 
									 if(typeof v.salesfrontuser !== typeof undefined){
 									swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.salesfrontuser').html('');
									 	$('.salesfrontuser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.salesfrontuser').append(v.salesfrontuser._empty);
									 } 

									 if(typeof v.salesbackenduser !== typeof undefined){
 							swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.salesbackenduser').html('');
									 	$('.salesbackenduser').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.salesbackenduser').append(v.salesbackenduser._empty);
									 } 
									 if(typeof v.totalamount !== typeof undefined){
 							swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.totalamount').html('');
									 		$('.totalamount').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.totalamount').append(v.totalamount._empty);
									 } 
									 if(typeof v.nextpayment !== typeof undefined){
 							swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.nextpayment').html('');
									 		$('.nextpayment').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.nextpayment').append(v.nextpayment._empty);
									 } 
									 if(typeof v.nextrelease !== typeof undefined){
 							swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.nextrelease').html('');
									 	$('.nextrelease').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.nextrelease').append(v.nextrelease._empty);
									 } 

									 if(typeof v.finishdate !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.finishdate').html('');
									 	$('.finishdate').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.finishdate').append(v.finishdate._empty);
									 } 

									 if(typeof v.credentials !== typeof undefined){
 swal("", "Data Not Saved .Please check all the required Fields!", "error")
									 	$('.credentials').html('');
									 	$('.credentials').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
									    $('.credentials').append(v.credentials._empty);
									 } 

									 //console.log(k);pm_id,salesfrontuser,salesbackenduser
								});	
								//$('.'+key).html('');
								//$('.'+key).append(value._empty);
							});
					}
				});
			}
		}
	}

	(function( $){
		  $.fn.checkbox_value = function() {
			$(this).click(function() {
					if($(this).is(":checked")){
						$(this).val('1');
					}else{
						$(this).val('0');
					}
				});
		  };
	})( jQuery );
	function validateTeamUsers(){
		var team_id = $('.multiSelect').val();
		var pm_id = $('#pm_id').val();
		var engineeringuser = $('#engineeringuser').val();
		var salesfrontuser = $('#salesfrontuser').val();
		var salesbackenduser = $('#salesbackenduser').val();
		if(team_id){
			$.each( team_id, function( key, value ) {
			//alert( key + ": " + value );
				flag=0;
				if(value == engineeringuser){
					flag=1;
				}else if(value == salesfrontuser){
					flag=2;
				}else if(value == salesbackenduser){
					flag=3;
				}else if(value == pm_id){
					flag=4;
				}
			});
			if(flag>0){
				if(flag==1){
						$('#showteamerror').html("Repetitive accountant user ID in project team.");
				}else if(flag==2){
					$('#showteamerror').html("Repetitive consultant user ID in project team.");
				}else if(flag==3){
					$('#showteamerror').html("Repetitive informer user ID in project team.");
				}else if(flag==4){
					$('#showteamerror').html("Repetitive responsible user ID in project team.");
				}

				$('#showteamerror').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
				//$('#showteamerror').attr('style','display:block;color:red;font-size:13px;');
				setTimeout(function() { $('#showteamerror').fadeOut('slow'); }, 5000);
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	function hourly_hide()
			{
			var e = document.getElementById("engagagment");
			var strSel = e.options[e.selectedIndex].value 
			if(strSel=="hourly"){
			document.getElementById("hourly_rate").readOnly = false;
			//.style.display='block';
			}
			else
			{
			document.getElementById("hourly_rate").readOnly = true;
			}
		}

</script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
