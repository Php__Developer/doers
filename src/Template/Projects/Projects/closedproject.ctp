<!-- .row -->
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="css/oribe.css" rel="stylesheet">
<?php echo $this->Html->css(array("oribe")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>

<style>
.info-tooltip {
    color: black;
    font-weight: normal;
    padding: 12px;
}
.input.checkbox > input {
    margin-left: 30px;
    margin-top: -16px;
}
td {
    padding: 11px;
}
 th {
    color: black;
    font-size: 18px;
/*    font-weight: bolder;*/
    padding: 12px;
}
</style>
<?php echo $this->Html->css(array('oribe')); ?>
<script type="text/javascript">
     var base_url = "<?php echo BASE_URL; ?>";
	function changeStaus(){
		if($('#projectStatus').val() != "closed"){
			if(getConfirmation("Are you sure to change project's status?")){
				var status = $('#projectStatus').val();
				var id = $('#project_id').val();
				if((status =="current" || status =="onhold") && (old_status == "closed")){
					var msg = "Are you want to activate this Project client?"
					confirmAction(msg,"act");
				}
				var client_update = $('#client_update').val();
				var client_id = $('#client_id').val();
				$('#tri_'+id).remove();
				if($.active>0){
				}else{

					var base_url = "<?php echo $this->Url->build('/', true); ?>";
					$.post(base_url+'projects/projectStatusChangebyAjax/',{'id':id,'status':status,'client_update':client_update,'client_id':client_id},function(data){
						if(data==1){
							//window.parent.$('#close-fb').click();
							$('#showsuccess').html('Project status updated successfully.');
							$('#showsuccess').attr('style','display:block;color:green;font-size:13px;');
							setTimeout(function() { $('#showsuccess').fadeOut('slow'); }, 1000);
						}
					});
				}
			}
		}
	}
	function confirmAction(msg,key){
			var action = confirm(msg);
			if(action == true){
				$('#client_update').val(key);
			}
				return true;		
	}
</script>
<style>
div#fancybox-content {
    width: 814px !important;
}
</style>
<!--  start content-table-inner -->
	

         <?php echo $this->Form->create('Project',array('url' => ['action'=>'closedproject'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>


<div class="row el-element-overlay m-b-40">

 <div class="col-md-12">
       <div class="col-md-3 searchingdata">

               <?php //echo $this->Form->input("project_name",array("type"=>'text',"style"=>"width:150px;height:24px;","label"=>false,"div"=>false,"class"=>"form-control"))."&nbsp;&nbsp;&nbsp;";
              
 echo $this->Form->input("project_name",array("id"=>"example-input1-group2 demo-input-search2 searchval","class"=>"form-control searchval", "div"=>false, "label"=> false,"value"=>"","placeholder"=>"Search"));
?>
      
     </div>

   <div class="col-md-4">
    <?php
      echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
      echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
      $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/projects/closedproject', true)));
    ?> </div>

</div>


<br>
<br>
<br>

<div class="col-sm-12">
<div class="white-box">
            <!-- row -->
    <div class="row">
      <div class="">
	             <table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
				<table border="1" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<thead>
					<th class="table-header-repeat line-left " width="20%">Project Name</th>
					<th class="table-header-repeat line-left " width="13%">Dropbox Backup</th>
					<th class="table-header-repeat line-left " width="13%">Code Backup</th>
					<th class="table-header-repeat line-left" width="54%">Operation-notes</th>
				</thead>
				<tbody>
				<?php 
				$i = 1;
				 $class=""; 
				 if(count($resultData)>0){  
				foreach($resultData as $projects): 
				$id = $projects ['id'];
			        //spr($id);die();
				?>
				<tr class="<?php echo $class; ?>" id="<?php echo "tri_".$projects['id']; ?>">

					<td><?php  $str = strip_tags($projects['project_name']);  $name = substr($str,0,18).((strlen($str)>18)?"...":""); 

					$engineer_User = (!empty($projects['Engineer_User'])) ? $projects['first_name']." ".$projects['last_name'] : "";
					$Salefront_User = (!empty($projects['Salefront_User'])) ? $projects['first_name']." ".$projects['last_name'] : "";
					$Saleback_User = (!empty($projects['Saleback_User'])) ? $projects['first_name']." ".$projects['last_name'] : "";
					$ProjectManager = (!empty($projects['ProjectManager'])) ? $projects['first_name']." ".$projects['last_name'] : "";
					$title = "$engineer_User,$Salefront_User,$Saleback_User,$ProjectManager";
					echo $this->Html->link($name,'#A',
								array('class'=>'info-tooltip')
							); 

					echo $this->Form->hidden("name.$id",array('value'=>$projects['Project'] ['project_name']));
					?></td>
					<td> 

					<?php ($projects['is_dropboxback']=="1") ? $checked = "checked" : $checked = "" ; 
						echo $this->Form->input("is_dropboxback.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked));



					?>
					</td>
					<td>
					<?php ($projects['is_codeback']=="1") ? $checked = "checked" : $checked = "" ; 
					echo $this->Form->input("is_codeback.$id",array('type'=>'checkbox',"label"=>false,"div"=>false,'hiddenField'=>true,"checked"=>$checked));
					?>
					</td>
					<td><?php 
					echo $this->Form->input("operation_notes.$id",array('type'=>'textarea',"label"=>false,"div"=>false,'style'=>'height:70px;witdh:250px;','class'=>'form-control','value'=>$projects['operation_notes']));
					?></td>
				</tr>
				<?php  $i++;
				endforeach; ?>
				<?php } else { ?> 
                 <tr>
			<td colspan="15" class="no_records_found"><center><h3 class="text-uppercase">No Record Found !</h3></center><p class="text-muted m-t-30 m-b-30"></p> </td>
		</tr>
		<?php } ?>
				</tbody>
				</table>
				<table>
				<tr>
					<center><td>
					<br>
					<?php 
						echo $this->Form->submit('Save',array('div'=>false,"name"=>"submit",'class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon'));

     //echo $this->Form->button("Submit", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
							echo $this->Form->end(); ?>

					</td></center>
				</tr>
				</table>
			</div>
	</td>
	<td>
</td>
</tr>
</table>
                </div>
             </div>
         </div>
     </div>
 </div>