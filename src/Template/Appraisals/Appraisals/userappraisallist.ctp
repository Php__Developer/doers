
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

<html>
<body>

	<!--  start content-table-inner -->

 <div class="col-sm-12">
    <div class="white-box">
    <center><h2 class="font-bold text-success">My Appraisal</h2></center>
		
		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
							
								
							

<thead>
                <tr>
              
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn"> Supervisor</button></th>
                <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Appraised amount</a></th>
							  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Effective from</a></th>
								  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Next review</a></th>
								  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">New designation</a></th>
							  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Your Choice</a></th>
								  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Details</a></th>
					
                                   </tr>
              </thead>
              <tbody>










								<?php  foreach($resultData as $appData): 
								
								$class=""; 
								$class = ($appData['status']==0) ? 'alternate-row' : '' ;
								$id = $appData['id'];
								?>
								<tr class="<?php echo $class; ?>">
									<td><?php echo $appData->Supervisor->first_name." ".$appData->Supervisor->last_name; ?> </td>
									<td><?php echo $appData['appraised_amount']; ?> </td>
									<td><?php echo date("d-m-Y",strtotime($appData['effective_from'])); ?> </td>
									<td><?php echo date("d-m-Y",strtotime($appData['next_review_date'])); ?> </td>
									<td><?php echo $appData['new_designaiton']; ?> </td>
									<?php
									if($appData['status']==0) { ?>
									<td><?php
									echo $this->Html->link("Agree",
										array('controller'=>'appraisals','action'=>'agree','id' => $appData['id']),
										array('class'=>'btn btn-success popup_window','title'=>'Agree')
										);
										?> 
									</td>
									<?php }else{ ?>
									<td> Agree </td>
									<?php } ?>
									<td>

										<?php
										echo $this->Html->link('View Details', '/appraisals/details/' . $id,array('class'=>'popup_window','title'=>''));
										?>
									</td>
								</tr>
							<?php endforeach;    ?>
						</tbody>
					</table>
				</div>
				</div>

















	
		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>

