<?php echo $this->Html->css(array("screen")); ?>
<!--  start content-table-inner -->
<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<!--  start table-content  -->
				<div id="table-content">
					<center><div style="font-weight:bold;">Pending Appraisals  </div>
						<table style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
							<tr>
								<th class="userattend" width="18%">Name</th>
								<th class="userattend" width="15%">Designation</th>
								<th class="userattend" width="10%">Salary</th>
								<th class="userattend" width="17%">Last Appraisal</th>
								<th class="userattend" width="25%">Due Date/Next Review Date</th>
							</tr>
							<?php 
							$today = date("Y-m-d");
							if(count($resultData)>0){ 
								foreach($resultData as $appData): 
									if($appData['next_review_date'] < $today){
										?>
										<tr>
											<td><?php echo $appData['Users']['first_name']." ".$appData['Users']['last_name']; ?></td>
											<td><?php echo $appData['new_designaiton']; ?></td>
											<td><?php echo $appData['appraised_amount']; ?></td>
											<td><?php echo date("d-m-Y",strtotime($appData['appraisal_date'])); ?></td>
											<td><?php echo date("d-m-Y",strtotime($appData['next_review_date'])); ?></td>
										</tr>
										<?php  }
										endforeach; ?>
										<?php } else { ?> 
										<tr>
											<td colspan="8" class="no_records_found">No records found</td>
										</tr>
										<?php } ?>
									</table>
								</center>
								<!--  end product-table --> 
							</div>
							<!--  end content-table  -->
							<div class="clear"></div>
						</td>
					</table>
					<div class="clear"></div>
				</div>
<!--  end content-table-inner  -->