<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>
  <script type="text/javascript">
 //$(document).ready(function() {
  //alert("ok");
  //$( "#startdate" ).removeClass('hasDatepicker');
 //$( ".attendancedate" ).datetimepicker({
       // dateFormat: "dd-mm-yy",
    // }); 
  //});
    </script>
<script type="text/javascript">
$(function() {
    $("#evaluation_from1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#evaluation_to1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#next_review_date1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#effective_from1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#appraisal_date1").datepicker({
        dateFormat: 'MM dd, yy',
    });

});
</script>
<!--  start content-table-inner -->
        <div class="col-sm-6">
            <div class="white-box">

                  <?php echo $this->Flash->render(); ?>

    <?php echo $this->Form->create('Appraisal',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
        <!-- start id-form -->
              <div class="form-group">
                       <label for="exampleInputpwd1">User*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php $options =  $this->General->getuser_name(); 
                       echo $this->Form->select('user_id',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                       <?php if(isset($errors['user_id'])){
                                    echo $this->General->errorHtml($errors['user_id']);
                                } ;?>
                  </div>

  <div class="form-group">
                       <label for="exampleInputpwd1">Supervisor*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                    <?php  $options =  $this->General->getuser_name();
                      echo $this->Form->select('supervisor_id',[" "=>$options],['class' => 'form-control'] );?>
                    </div>
                     <?php if(isset($errors['supervisor_id'])){
                                        echo $this->General->errorHtml($errors['supervisor_id']);
                                    } ;?>
                 </div>
                   <div class="form-group">
                    <label for="exampleInputuname">Appraisal Amount*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("appraised_amount",array("type"=>"text","class"=>"form-control",'placeholder'=> "Appraisal Amount", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['appraised_amount'])){
                      echo $this->General->errorHtml($errors['appraised_amount']);
                      } ;?>
                  </div>
             <div class="form-group">
                    <label for="exampleInputuname">Evaluation To*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("evaluation_to1",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Evaluation To ", "label"=>false,"div"=>false,"id"=>"evaluation_to1"));?>
                       </div>
                     <?php if(isset($errors['evaluation_to1'])){
                      echo $this->General->errorHtml($errors['evaluation_to1']);
                      } ;?>
                  </div>
   <div class="form-group">
                       <label for="exampleInputpwd1">Technical Competency*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('technical_5',['1'=> '1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],['empty' => '--Select--','class' => 'form-control','required'=>'required'] );?>
                    </div>
                 </div>
   <div class="form-group">
                       <label for="exampleInputpwd1">Communication Competency*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('communication_5',['1'=> '1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],['empty' => '--Select--','class' => 'form-control','required'=>'required'] );?>
                    </div>
                 </div>
       <div class="form-group">
                       <label for="exampleInputpwd1">Interpersonal Competency*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('interpersonal_5',['1'=> '1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'],['empty' => '--Select--','class' => 'form-control','required'=>'required'] );?>
                    </div>
                 </div>
            <div class="form-group"> 
               <label for="inputName" class="control-label">Technical comment:</label>
             <?php
             echo $this->Form->input("technical_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the technical comment here .."));
                     if(isset($errors['technical_comment'])){
                                  echo $this->General->errorHtml($errors['technical_comment']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
                </div>
                <div class="form-group"> 
   <label for="inputName" class="control-label">Communication comment:</label>
             <?php
 echo $this->Form->input("communication_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the communication comment here .."));
         if(isset($errors['communication_comment'])){
                      echo $this->General->errorHtml($errors['communication_comment']);
                      } ;
       echo $this->Form->hidden("id");
          ?>
       </div>

                <div class="form-group"> 
               <label for="inputName" class="control-label">Interpersonal comment:</label>
                         <?php
             echo $this->Form->input("interpersonal_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the interpersonal comment here .."));
                     if(isset($errors['interpersonal_comment'])){
                                  echo $this->General->errorHtml($errors['interpersonal_comment']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
               </div>

                  <div class="form-group"> 
               <label for="inputName" class="control-label">Supervisor comment:</label>
                         <?php
             echo $this->Form->input("supervisor_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the supervisor comment here .."));
                     if(isset($errors['supervisor_comment'])){
                                  echo $this->General->errorHtml($errors['supervisor_comment']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
               </div>
                 <div class="form-group">
                    <label for="exampleInputuname">Next Review-Date:*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("next_review_date1",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Next Review-Date", "label"=>false,"div"=>false,"id"=>"next_review_date1"));?>
                       </div>
                     <?php if(isset($errors['next_review_date1'])){
                      echo $this->General->errorHtml($errors['next_review_date1']);
                      } ;?>
                  </div>

                    <div class="form-group">
                    <label for="exampleInputuname">Effective from:*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("effective_from1",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Effective from", "label"=>false,"div"=>false,"id"=>"effective_from1"));?>
                       </div>
                     <?php if(isset($errors['effective_from1'])){
                      echo $this->General->errorHtml($errors['effective_from1']);
                      } ;?>
                  </div>

                    <div class="form-group">
                    <label for="exampleInputuname">Appraisal date:*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("appraisal_date1",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Appraisal date", "label"=>false,"div"=>false,"id"=>"appraisal_date1"));?>
                       </div>
                     <?php if(isset($errors['appraisal_date1'])){
                      echo $this->General->errorHtml($errors['appraisal_date1']);
                      } ;?>
                  </div>
                <div class="form-group">
                       <label for="exampleInputpwd1">New designaiton*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php 
                        $options = $this->General->getalldesignations($userAgency);
                      echo $this->Form->select('new_designaiton',[""=>$options],['class' => 'form-control'] );?>
                    </div>
                     <?php if(isset($errors['new_designaiton'])){
                        echo $this->General->errorHtml($errors['new_designaiton']);
                        } ;?>
                 </div>

<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'appraisals','action'=>'add','prefix' => 'appraisals'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/testimonials/add', true)));?>

 </div>
 </div>
</div>
    <!-- end id-form  -->

    

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Appraisals Management</h4>
          Appraisal used for a number of purposes such that evaluation,planing,control. 
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'appraisals','action'=>'appraisalslist'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     
    
      <!-- /.right-sidebar -->



</body>
</html>
