<?php echo $this->Html->css(array("screen")); ?>
<script type="text/javascript">
$(document).ready(function(){ 
	$(".viewPaydetails").fancybox({
		'type': 'iframe',
		'autoDimensions' : false,
		'width' :	700, 
		'height' :	650, 
		'onStart': function(){
			jQuery("#fancybox-overlay").css({"position":"fixed"});
		}
	});
});
</script>
<?php echo $this->Form->create('Appraisal',array('url' => ['action' => 'incoming'],'method'=>'POST','onsubmit' => '',"class"=>"login" ,'id'=>'mainform')); ?>
<!--  start content-table-inner -->
<div id="content-table-inner">
	<center><div style="font-weight:bold;"><h1>Incoming Appraisals </h1></div></center></br>
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<div style="width:656px;border:1px solid #d2d2d2;margin-left:10px; border-radius: 9px 9px 9px 9px;">
		<table width ="600px" cellspacing="0" cellpadding="4" border="0" align="center">
			<tr>
				<td width="10%">
				</br>
				<div>
					<div style="float:left">
						<span style="vertical-align: middle; margin-left:10px;">
							<b>From:</b>
						</span>
					</div>
				</td>
				<td width="70%">
					<div style="float:left;width:186px;">
						Month:
						<br>
						<?php 
						$options = $common->getMonthsArray();
						if(isset($start_month)){
							if($start_month!=""){
								$currentMonth=$start_month;
							}else{
								$currentMonth=date("m",strtotime("+1 months"));
							}
						}else{
							$currentMonth="";	
						}
						
						echo $this->Form->input("month1",array("type"=>"select","class"=>"top-search-inp","options"=>$options,'selected'=>$currentMonth,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"month1"));
						?>
					</div>
					<div style="float:left">
						Year:
						<br>
						<?php
						if(isset($start_year)){
							if($start_year!=""){
								$currentYear=$start_year;
							}else{
								if(date('m')==12){
									$currentYear =  date("Y",strtotime("+1 years"));
								}
								else{
									$currentYear=date('Y');
								}
							}
						}else{

							$currentYear="";
						}
						
						$curYear 	= "2013";
						$array 	= array();
						$array[''] = "-Select-";
						while($curYear<='2025'){
							$array[$curYear] = $curYear;
							$curYear++;
						}
						
						echo $this->Form->input("year1",array("type"=>"select","class"=>"top-search-inp","options"=>$array,'selected'=>$currentYear,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"year1"));
						?>
					</div>
				</div>
			</td>
			<div style="float:left">
				<td width="10px">
					<br>
					<div style="padding-left:90px;">
						<?php echo $this->Form->submit("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
					</div>
				</div>
			</td>
		</tr>
	</table>
	<div style="color:red;" id="error"></div>
</div>
<br>
<?php  if(isset($resultData) && count($resultData)>0){?>
<div id="table-content">
	<table style="margin-top:10px;font-size:12px;" border="0" width="87%" cellpadding="0" cellspacing="0" id="product-table">
		<tr>
			<th class="userattend" width="18%" style="height:25px;"><b>Name</b></th>
			<th class="userattend" width="15%" style="height:25px;"><b>Designation</b></th>
			<th class="userattend" width="18%" style="height:25px;"><b>Current Salary</b></th>
			<th class="userattend" width="18%" style="height:25px;"><b>Last Appraised</b></th>
			<th class="userattend" width="18%" style="height:25px;"><b>Next Review Date</b></th>
		</tr>
		<?php foreach($resultData as $record)
		{ ?>
		<tr >
			<td><?php echo $record['Users']['first_name'];?> </td>
			<td><?php echo $record['new_designaiton']; ?> </td>
			<td><?php echo $record['appraised_amount']; ?> </td>
			<td><?php echo $record['appraisal_date']; ?> </td>
			<td><?php echo $record['next_review_date']; ?> </td>
		</tr>
		<?php } ?>
	</table>
	<div class="clear"></div>
</div>
<?php } else { ?> 
<div id="table-content">
	<table border="0" width="87%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
		<tbody>
			<tr>
				<td colspan="13" class="no_records_found">No records found</td>
			</tr>
		</tbody>
		<?php } ?>
	</table>
	<div class="clear"></div>
</div></div>