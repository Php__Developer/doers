
  <?php $url= $this->Url->build('/', true); ?>
<div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                      <ul class="">

  <li> <a><h2 style="color:red;">Admin Reports - For HR Department</h2></a></center> 
</li>
                 </ul>
                    </nav>
               </div><!-- /tabs -->
               <br>

               <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
             <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'appraisals/salaryreport/';?>">Salary Report</a></p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/leadreport1/';?>">Lead Report (Date Wise)</a></p>
          </div>
        </div>


                <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'attendances/leavereport/';?>">Leave Report (User Wise)</a></p>
          </div>
        </div>


       <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'evaluations/performancereportdatewise/';?>">Performance Report (Date Wise)</a></p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'evaluations/performancereportmonth/';?>">Performance Report (User / Month Wise)</a></p>
          </div>
        </div>



          <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'tickets/ticketreportdate/';?>">User Ticket Report (Date Wise)</a></p>
          </div>
        </div>

 </div>



<div class="sttabs tabs-style-linebox erw">
   <nav class="lis"> <ul class=""> <li> <a><h2 style="color:red;">Admin Reports - Finance Deparment</h2></a></center> </li></ul></nav>
               </div><!-- /tabs -->
               <br>

<div class="row">
     <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'billings/billingreportweek/';?>">Week Wise Billing Report</a></p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight ">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'payments/paymentreportweek/';?>">Week Wise Payment Report</a></p>
          </div>
        </div>

 </div>









<div class="sttabs tabs-style-linebox erw">
   <nav class="lis"> <ul class=""> <li> <a><h2 style="color:red;">Admin Reports - Sales Deparment</h2></a></center> </li></ul></nav>
               </div><!-- /tabs -->
               <br>

<div class="row">
     <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/bidspiechartreport/';?>">Bidding Report - (User Wise)</a></p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/winclosereport/';?>">No of Win / Closed Projects (User Wise)</a></p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/overleadcountuserreport/';?>">Overall Lead Count along with Status (User Wise)</a></p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/companywideleadreport/';?>">Company Wide Line Graph - (Month Wise)</a></p>
          </div>
        </div> 


       
  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/activelead/';?>">Current Active Leads</a></p>
          </div>
        </div> 


 <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><a class="addLinks fancybox popup_window" href="<?php echo $url.'projects/hold_projects';?>">On Hold Projects</a></p>
          </div>
        </div> 




 </div>
















      
       
<style>
.fancybox popup_window {
    color: red;
    font-weight: bolder;
}
.info-tooltip {
    color: springgreen;
}
</style>