<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<link href="<?php echo $this->Url->build('/', true);?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
<input id="page" class="hidden" value="<?php echo $pagename;?>">


<style>
.switchery.switchery-small {
    float: right;
    margin-left: 147px;
    margin-top: 12px;
}
.list-group-item-warning {
    padding: 11px;
}
</style>
<div id ='dialog_modal' class="dialog_modal default_hidden"></div>
<?php

 echo $this->Form->create('Entities',array('url' => ['action'=>'list'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<div class="row el-element-overlay m-b-40">
<span class="pull-right text-success"> <?= $this->Paginator->counter([
    'format' => ' {{page}} of {{pages}} Pages'
]) ?></span>

  <div class="col-md-12">
  <div class="col-md-3 searchingdata">
  <?php
        $fieldsArray = array(
          ''   => 'Select',
        'Entities.name'     => 'Entity Name'
        );
      
       echo $this->Form->input("Entities.fieldName",array("type"=>"select",'options'=>$fieldsArray,"class"=>"form-control","label"=>false,"div"=>false,"id"=>"status")); ?>

      </div>
  <div class="col-md-4">

  <?php
        $display1   = "display:none";
        $display2   = "display:none";
        if($search1 != "Role.status"){
          $display1 = "display:block";
        }else{
          $display2 = "display:block";
        }
          echo $this->Form->input("Entities.value1",array("id"=>"search_input","class"=>"form-control","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
        ?>
</div>
<div class="col-md-4">
  <?php
  echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
  echo $this->Html->link("Reset",  ['controller'=>'entities','action'=>'list','prefix' => 'entities'],['class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject']);  ?>

<div class="btn-group m-r-10">
                <button aria-expanded="true" data-toggle="dropdown" class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" type="button">Sort By <span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu animated flipInX drp_list">
                    <li><a href="#" class="1">Entity Name</a></li>  
                    <li><a href="#" class="2">Agency</a></li>
                    <li><a href="#" class="4">Created</a></li>
           
                </ul>
              </div>
  </ul>

   <?php 
            echo $this->Html->link('<i class="fa  fa-plus"></i>',
            array('controller'=>'Entities','action'=>'add'),
            ['escape' => false,"class"=>"btn btn-info btn-circle pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Add New Entity"]
            );
            ?>
<?php echo $this->Form->end(); ?>
</div>
<div class="col-md-1 paginateproject">
  <ul class="pagination paginate-data">
    <li class="previousdata">
      <?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
    </li>
    <li>
      <?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
    </li></ul>
  </div>



<br>
<br>
<br>

<script src="moment.js"></script>
<script src="moment-timezone-with-data.js"></script>
<div class="ajax">

  <div class="parent_data">
  <?php if(count($resultData)>0){
      $i = 1;
      foreach($resultData as $p):         
            if(!$p['status']%2)$class = "nav nav-tabs list-group-item-info actioning redish"; else $class = "nav nav-tabs list-group-item-info";  ?>

        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
        <input type="hidden" value="<?php echo  $this->General->encForUrl($this->General->ENC($p['id']));?>" class="eid">
          <div class="white-box">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs firstLISt" role="tablist">
              <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
              <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
            </ul>
            <!-- Tab panes  substring($row->parent->category_name,35);  -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane ihome active" >
                 <ul class="list-group ">
      <li class="list-group-item list-group-item-danger 1" title="<?php echo $p['name']; ?>" ><b>Name :</b> 
         <a href="#" class="ename" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($p['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Client Name" data-params="{'type': 'name'}" >
                          <?php $str = strip_tags($p['name']); 
                    echo $name = substr($str,0,20).((strlen($str)>20)? "...":""); 
                  ?>
                   </a>
      </li>
      <li class="list-group-item list-group-item-danger 1" title="<?php echo $p['alias']; ?>" ><b>Alias :</b> 
         <a href="#" class="ename" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($p['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Client Name" data-params="{'type': 'alias'}" >
                          <?php $str = strip_tags($p['alias']); 
                    echo $name = substr($str,0,20).((strlen($str)>20)? "...":""); 
                  ?>
         </a>
      </li>
     
      <li class="list-group-item list-group-item-warning 4"><b>Created :</b> 
      <?php echo date(DATE_FORMAT, strtotime($p['created'])); ?></li>
      
     
      <?php 
      if(isset($p['created'])){
      $created=$p['created']->format("Ymd");
      }else{
      $created="";
      }
      ?>

  <input type="hidden" class="testicons" value = "<?php echo $this->General->ENC($p->id);?>" >

            <ul class="<?php echo $class;?>" role="tablist">

              <li><?php
                         echo $this->Html->link(
                      '<i class="fa fa-exchange"></i>',
                      array('controller'=>'entities','action'=>'manage','id'=>$this->General->encForUrl($this->General->ENC($p['id']))),
                      ['escape' => false,'class' => 'btn default btn-outline manage','title'=>'Manage Agencies And Roles','target'=>'_blank']
                      );
                    ?></li>
               <li>
                         <?php
                           echo $this->Html->link(
                      '<i class="ti-close"></i>',
                      array('controller'=>'entities','action'=>'delete','id'=>$this->General->encForUrl($this->General->ENC($p['id']))),
                      ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
                      );
                      ?>
                        </li>
                       
                  </ul>
              <input id="last_update" class="hidden created" value="<?php echo $created;?>">
              </ul>

            </div>
              <div role="tabpanel" class="tab-pane iprofile">
                <div class="col-md-6">
                  <h3>Lets check profile</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane imessages">
                <div class="col-md-6">
                  <h3>Come on you have a lot message</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane isettings">
                <div class="col-md-6">
                  <h3>Just do Settings</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            </div>
        </div>
         <?php $i++ ;
        endforeach; ?>
        <?php } else { ?>

        <?php
        }
      ?>
         </div>
        <!-- /.usercard-->
      </div>
<!-- /.row -->
 <?php 
    echo  $this->Html->script(array('jquery-3','jquery-ui/jquery-ui','entity','jquery.slimscroll'));
?>
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>

<!-- Magnific popup JavaScript -->

<script type="text/javascript">
function veryfycheck()
  {
  alert("1) Either billing for this project is already added for this week."+'\n'+
  "2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
  
  }

      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
