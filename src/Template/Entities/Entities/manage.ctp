<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<?php echo $this->Html->css(array('oribe')); ?>
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core ?>
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
<script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
<script type="text/javascript">
$(function() {
    $("#bidding_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
    $("#nextfollow_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
});
</script>
<?php echo $this->Html->css(array('oribe')); ?>
  <div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-top alerttop2" style="display: none;">
<center><h4 class="error_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-success myadmin-alert-top alerttop1" style="display: none;">
<center><h4 class="success_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop ng-binding successwrapperdv"  style="">
    <span><i class="fa fa-thumbs-up "></i> <span class="successmsg"></span></span>
  </div>
  
	 <div class="col-sm-12">
    <input type="hidden" name="entityid" class="entityid" value="<?php echo $this->General->encForUrl($this->General->ENC($entity['id'])) ;?>">
    <input type="hidden" name="currentpage" class="currentpage" value="manageentities">
    <a href="#" class="cls_host pull-right"><i class="fa fa-times"></i></a>
      <div class="white-box">
      <h3 class="box-title text-center">Manage Entities</h3>
      <div class="col-lg-12 col-md-12">
            <div class="col-md-12 col-xs-12 col-lg-12 m-t-30 text-center loaderdiv"><i class="fa fa-refresh fa-spin text-center m-t-30" style="font-size:25px;"></i></div> 
            </div>
      <div class="leadtagsinputwrapper default_hidden">
         <div class="form-group ">
                <h3 class="box-title">Entity Name</h3>
                <div class="col-lg-12 col-md-12 namewrapper ">
                 <a href="#" class="ename" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($entity['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Entity Name" data-params="{'type': 'name'}" >
                    <?php $str = strip_tags($entity['name']); 
                    echo $str;
                  ?>
                  </a> 
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <label class="col-xs-3 control-label"></label>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
                </div>
          </div> 
           <div class="form-group ">
                <h3 class="box-title">Entity Alias</h3>
                <div class="col-lg-12 col-md-12 namewrapper ">
                 <a href="#" class="ename" id="description" data-type="text" data-pk="<?php echo $this->General->ENC($entity['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Client Name" data-params="{'type': 'alias'}" >
                          <?php $str = strip_tags($entity['alias']); 
                          echo $str;
                  ?>
                </a>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <label class="col-xs-3 control-label"></label>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
                </div>
          </div>     
          <div class="form-group ">
                <h3 class="box-title">Attached Agencies</h3>
                <div class="col-lg-12 col-md-12 inpwrapper ">
                <?php echo $this->Form->input("lead",array("type"=>"text","class"=>"leadtag51nput other_aliases agency ",'placeholder'=> "Type in box below to add more Agencies", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <label class="col-xs-3 control-label"></label>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
                
          </div>
          <div class="form-group ">
                <h3 class="box-title">Selet Agency to manage roles furthers</h3>
                <div class="col-lg-12 col-md-12 selectwrapper">
                
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <label class="col-xs-3 control-label"></label>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
                
          </div>
          <div class="form-group restagsinputwrapper default_hidden">
                  <h3 class="box-title">Add / Remove Roles</h3>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <?php echo $this->Form->input("tags",array("type"=>"text","class"=>"restag51nput responsible other_aliases",'placeholder'=> "To add Resposible", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                </div>
                
                 <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                     
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
          </div>
           <div class="form-group ">
                <h3 class="box-title">Notes</h3>
                <div class="col-lg-12 col-md-12 noteswrapper ">
                 <a href="#" class="ename entitynotes" id="notes" data-type="textarea" data-pk="<?php echo $this->General->ENC($entity['id']) ;?>" data-placement="right" data-placeholder="Required" data-title="Enter Client Name" data-params="{'type': 'notes'}" >
                          <?php $str = strip_tags($entity['notes']); 
                          echo $str;
                  ?>
                </a>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <label class="col-xs-3 control-label"></label>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"><div class="errors"></div></div>     
                </div>
                </div>
          </div>

          <!-- <div class="form-group">
            <button type="submit" class="btn btn-success addlead">Submit</button>
            <?php 
            echo $this->Html->link("Reset",  ['controller'=>'leads','action'=>'add','prefix' => 'leads'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
            $this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light ",'div'=>false,'onclick'=>$this->Url->build('/leads/add', true)));?>
          </div> -->

      </div>
      
      </div>
</div>




     <!-- /.right-sidebar -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/entity.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

