<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />

 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>

  <script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#created" ).datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$( "#modified" ).datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$( "#lastauditdate" ).datepicker({
				dateFormat: 'yy-mm-dd'
			});


	});
		
</script>


	<script type="text/javascript">
	$(document).ready(function() { 
		 //fancybox settings
			$(".view_instc").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	750, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
	});
	
</script>



	<?php echo $this->Form->create('saleProfile',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>

	<!--  start content-table-inner -->
  <div class="col-sm-6">
            <div class="white-box">

<div class="form-group">
                    <label for="exampleInputuname">User Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>


				<?php
				 echo $this->Form->input("username",array( "class"=>"form-control","label"=>false,"div"=>false));
				?>
</div>
				<?php if(isset($errors['username'])){
							echo $this->General->errorHtml($errors['username']);
							} ;?></div>

				   <div class="form-group">
                    <label for="exampleInputuname">Password:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>

				<?php
			 echo $this->Form->input("password",array('type'=>'text',"class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div>

			
			 <?php if(isset($errors['password'])){
							echo $this->General->errorHtml($errors['password']);
							} ;?>



</div>



			     <div class="form-group">
                    <label for="exampleInputuname">Name on Profile:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
				<?php
			 echo $this->Form->input("nameonprofile",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div>
			 <?php if(isset($errors['nameonprofile'])){
							echo $this->General->errorHtml($errors['nameonprofile']);
							} ;?></div>





		   <div class="form-group">
                    <label for="exampleInputuname">Type:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>

<?php				   $options=array(						'Upwork'    =>  'Upwork', 
                                                             //'elance'    => 'Elance'
                                                             'Freelancer' => 'Freelancer',
                                                             'PPH' => 'PPH',
                                                             'Guru' => 'Guru',
                                                             'Others' => 'Others'
								);
			echo $this->Form->input("type",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
             ?></div></div>



		   <div class="form-group">
                    <label for="exampleInputuname">Technology For:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
			<?php   $options=array('php'    =>  'PHP', 
						 'mobile'    => 'Mobile',
						 'designing'    => 'Designing',
						 '.net'    => '.Net'
						);
			echo $this->Form->input("technologyfor",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div></div>



  <div class="form-group">
                    <label for="exampleInputuname">Status:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>

	
			<?php
			 $options=array('1'    =>  'Active', 
						 '0'    => 'Deactive',
						
						);
			echo $this->Form->input("status",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div></div>





  <div class="form-group">
                    <label for="exampleInputuname">Id Verified::</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>

<?php	 $options=array(0    => 'No',
					1=>  'Yes', 
						);
			echo $this->Form->input("idverified",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
         	 ?></div></div>



		  <div class="form-group">
                    <label for="exampleInputuname">Payment Method:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>


                      <?php  
			   // $options1=array(''=>'--Select--')+$options1; 

			   $options1=$this->General->getupaymentname();
			   
 echo $this->Form->input("payment_methods_id",array("type"=>'select','options'=>$options1,"class"=>"form-control","label"=>false,"div"=>false));
            ?></div></div>


          <div class="form-group">
                    <label for="exampleInputuname">Doc In Dropbox:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    
		
	

	<?php
			 $options=array( '0'    => 'No',
						'1'    =>  'Yes', 
						);
			echo $this->Form->input("docindropbox",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"docindropbox","label"=>false,"div"=>false));    
         
			// echo $form->input("docindropbox",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>


      <div class="form-group">
                    <label for="exampleInputuname">Is Shared:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    

               
               <?php	 $options=array(
					0    => 'No',
					1    =>  'Yes', 
							);
			echo $this->Form->input("is_shared",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"is_shared","label"=>false,"div"=>false));    
         	 ?></div></div>





      <div class="form-group">
                    <label for="exampleInputuname">Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    
				<?php
			 echo $this->Form->input("email",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div>
			 	<?php if(isset($errors['email'])){
							echo $this->General->errorHtml($errors['email']);
							} ;?></div>




			
      <div class="form-group">
                    <label for="exampleInputuname">Forwarded Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    
			<?php
			 echo $this->Form->input("forworded_email",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?>
			</div></div>
		
				
      <div class="form-group">
                    <label for="exampleInputuname">Security Ques 1:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    <?php
			 echo $this->Form->input("securityque1",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>



      <div class="form-group">
                    <label for="exampleInputuname">Security Ans 1:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

		

	     	<?php
			 echo $this->Form->input("securityans1",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>


	      <div class="form-group">
                    <label for="exampleInputuname">Profile Url:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

		
           <?php
			 echo $this->Form->input("profileurl",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>



     <div class="form-group">
                    <label for="exampleInputuname">Phone No. Attached:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 



		
				<?php
			 echo $this->Form->input("phonenoattached",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div>
			 <?php if(isset($errors['phonenoattached'])){
							echo $this->General->errorHtml($errors['phonenoattached']);
							} ;?></div>



     <div class="form-group">
                    <label for="exampleInputuname">Security Ques 2:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 




         <?php
			 echo $this->Form->input("securityque2",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>



     <div class="form-group">
                    <label for="exampleInputuname">Security Ans  2:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

      <?php
			 echo $this->Form->input("securityans2",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>





     <div class="form-group">
                    <label for="exampleInputuname">Test Passed:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
        <?php
			 echo $this->Form->input("testpassed",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>




     <div class="form-group">
                    <label for="exampleInputuname">Last Audit Date:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

			<?php
			 echo $this->Form->input("lastauditdate",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"lastauditdate","label"=>false,"div"=>false));
        
			// echo $form->input("lastauditdate",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>

     <div class="form-group">
                    <label for="exampleInputuname">Sales Rep Points:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 


			<?php
			 echo $this->Form->input("salesreppoints",array("class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>


     <div class="form-group">
                    <label for="exampleInputuname">Rating:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 


			<?php
			 echo $this->Form->input("rating",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>


     <div class="form-group">
                    <label for="exampleInputuname">Other Notes:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

	      <?php
			 echo $this->Form->input("othernotes",array("class"=>"form-control","label"=>false,"div"=>false));
          	 ?></div></div>




    <div class="form-group">
                    <label for="exampleInputuname">Alloted User id:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 



		<?php
					$options = $this->General->getUsersprofile('5'); 
					//pr($options);die();
				/*  if($this->data['User']['role_id'] != ''){
					$cont_cats = explode(",",$this->data['User']['role_id']);
					}else{
					$cont_cats = '';
					}
					,'selected' =>$cont_cats
					*/ 
				echo $this->Form->input("alloteduserid",array( "type"=>'select','size'=>'10','multiple' => true,'options'=>$options,"label"=>false,"div"=>false,"class"=>"multiSelect","id"=>"team_id"));
         		 ?></div></div>


    <div class="form-group">
                    <label for="exampleInputuname">Associated Paypal:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 


		<?php
			 echo $this->Form->input("associated_paypal",array( "class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>



    <div class="form-group">
                    <label for="exampleInputuname">Payment Remark:</label>
                   
                        <?php
             echo $this->Form->input("payment_remark",array("class"=>"form-control payment_remark","label"=>false,"div"=>false,'id' => 'payment_remark'));  
             ?></div>




			    <div class="form-group">
                    <label for="exampleInputuname">Contract On Hold:</label>
               <?php
			 echo $this->Form->input("contract_on_hold",array("class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div>



    <div class="form-group">
                    <label for="exampleInputuname">Stats:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

		<?php
			 echo $this->Form->input("stats",array("class"=>"form-control","label"=>false,"div"=>false));  
			 ?></div></div>






    <div class="form-group">
	    <label for="exampleInputuname">Created:</label>
	    <div class="input-group">
	      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
				<?php echo $this->Form->input("created",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"created","label"=>false,"div"=>false));?>
		</div>
	</div>


			    <div class="form-group">
                    <label for="exampleInputuname">Modified:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
                      

	<?php
			 echo $this->Form->input("modified",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"modified","label"=>false,"div"=>false));
        
			 ?>
		<?php
			 echo $this->Form->hidden("quota",array( "class"=>"form-control","value"=>"60","label"=>false,"div"=>false));  
			 ?>
			
</div></div>

<div class="form-group">
    <label for="exampleInputuname">Process Notes:</label>
    <div class="input-group">
      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
		<?php echo $this->Form->input("process_notes",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"Enter Previous Employment."));?>
	</div>
</div>




<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'sales','action'=>'add','prefix' => 'sales'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/sales/add', true)));?>

 </div>
 </div>
</div>

	<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Evaluations Management</h4>
                       Evaluation used for a number of purposes such that evaluation,planing,control.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'sales','action'=>'saleslist'), array('style'=>'color:red;'));?>

              </li> 
          

                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>

      <!-- /.right-sidebar -->


</form>
</body>
</html>
		

