
  <?php $url= $this->Url->build('/', true); ?>
<div class="sttabs tabs-style-linebox erw">
  <nav class="lis">
    <ul class="">
      <li> 
          <a><h2 style="color:red;">Profile Reports - For Admin Department</h2></a> 
      </li>
    </ul>
  </nav>
</div><!-- /tabs -->
               <br>

               <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
             <p class="text-white m-t-40">
              <!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'appraisals/salaryreport/';?>">Salary Report</a> -->
              <?php 
                echo $this->Html->link("Profile Report by Running Projects",
                array('controller'=>'sales','action'=>'profilereportbyrunnningproject'),
                array('class'=>'runningprofile popup_window')
               );  
            ?>
             </p>
          </div>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40">
            <!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'leads/leadreport1/';?>">Lead Report (Date Wise)</a> -->
             <?php 
              echo $this->Html->link("Sales Profile Audit Report",
              array('controller'=>'sales','action'=>'salesprofileauditreport'),
              array('class'=>'runningprofile popup_window')
              );  
              ?>
              </p>
          </div>
        </div>


                <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40">
            <!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'attendances/leavereport/';?>">Leave Report (User Wise)</a> -->
              <?php 
              echo $this->Html->link("Profile Report By Alloted To",
              array('controller'=>'sales','action'=>'profilereportbyallottedto'),
              array('class'=>'auditreports popup_window')
              );  
              ?>
              </p>
          </div>
        </div>


       <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40">
            <!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'evaluations/performancereportdatewise/';?>">Performance Report (Date Wise)</a> -->
            <?php 
                echo $this->Html->link("Verified And Unverified Report",
                array('controller'=>'sales','action'=>'verifiedUnverifiedreport'),
                array('class'=>'runningprofile popup_window')
                );  
            ?>
              
            </p>
          </div>
        </div>


  <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40"><!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'evaluations/performancereportmonth/';?>">Performance Report (User / Month Wise)</a> -->
             <?php 
                echo $this->Html->link("Technology Wise Report",
                array('controller'=>'sales','action'=>'technologyreport'),
                array('class'=>'runningprofile popup_window' )
                );  
            ?>
            </p>
          </div>
        </div>



          <div class="col-md-3 col-xs-12 col-sm-6">
          <div class="white-box text-center fixedheight">
            <p class="text-white m-t-40">
              <!-- <a class="addLinks fancybox popup_window" href="<?php echo $url.'tickets/ticketreportdate/';?>">User Ticket Report (Date Wise)</a> -->
              <?php 
            echo $this->Html->link("Payment Report",
            array('controller'=>'sales','action'=>'Paymentreport'),
                array('class'=>'auditreports popup_window')
            );  
            ?>
            </p>
          </div>
        </div>
<div class="col-md-12">
  <div class="sttabs tabs-style-linebox erw">
    <nav class="lis">
      <ul class="">
        <li> 
            <a><h2 style="color:red;">Summarized Report</h2></a> 
        </li>
      </ul>
    </nav>
  </div>
</div>


        
        <div class="col-md-12">
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">All Activated Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $allactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">All deactivated Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $alldeactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Active Upwork Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $upworkactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Deactive Upwork Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $upworkdeactive ;?></span></li>
                </ul>
              </div>
            </div>
        </div>

         <div class="col-md-12">
          <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Active Elance Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $elanceactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Deactive Elance Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $elancedeactive ;?></span></li>
                </ul>
              </div>
            </div>

            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Active PPH Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $PPHactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Deactive PPH Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $PPHdeactive ;?></span></li>
                </ul>
              </div>
            </div>
        </div>

         <div class="col-md-12">
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Active Guru Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $Guruactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Deactive Guru Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $Gurudeactive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Active FreeLancer Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $Freelanceractive ;?></span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Deactive FreeLancer Profiles</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter"><?php echo $Freelancerdeactive ;?></span></li>
                </ul>
              </div>
            </div>
        </div>
          



 </div>





























      
       
<style>
.fancybox popup_window {
    color: red;
    font-weight: bolder;
}
.info-tooltip {
    color: springgreen;
}
</style>