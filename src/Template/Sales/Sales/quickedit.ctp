<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3','jquery.bind','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min'))   //ui.core ?>
<script type="text/javascript">
            var base_url = "<?php echo $this->Url->build('/', true); ?>";
 

    function editsaleprofile() {
       
        var is_shared = $('#is_shared').val();
        var id = $('#saleProfileid').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var nameonprofile = $('#nameonprofile').val();
        var type = $('#type').val();
        var technologyfor = $('#technologyfor').val();
        var status = $('#profile_status').val();
        var idverified = $('#idverified').val();
        var docindropbox = $('#docindropbox').val();
        var email = $('#email').val();
        var forworded_email = $('#forwarded_email').val();
        var securityque1 = $('#securityque1').val();
        var securityans1 = $('#securityans1').val();
        var profileurl = $('#profileurl').val();
        var phonenoattached = $('#phonenoattached').val();
        var securityque2 = $('#securityque2').val();
        var securityans2 = $('#securityans2').val();
        var testpassed = $('#testpassed').val();
        var lastauditdate = $('#lastauditdate').val();
        var salesreppoints = $('#salesreppoints').val();
        var othernotes = $('#othernotes').val();
        var alloteduserid = $('#team_id').val(); //.join(',');
        console.log(alloteduserid);
        var rating = $('#rating').val();
        var payment_remark = $('#payment_remark').val();
        var associated_paypal = $('#associated_paypal').val();
        var created = $('#created').val();
        var modified = $('#modified').val();
        var forworded_email = $('#forworded_email').val();
        var contract_on_hold = $('#contract_on_hold').val();
        var stats = $('#stats').val();
        var quota = $('#quota').val();
        var process_notes = $('.process_notes').val();
        
        if ($.active > 0) {
        } else {
            var base_url = "<?php echo $this->Url->build('/', true); ?>";
            $.post(base_url+'sales/quickedit', {'id': id, 'username': username, 'password': password, 'nameonprofile': nameonprofile, 'type': type, 'technologyfor': technologyfor, 'status': status, 'idverified': idverified, 'docindropbox': docindropbox, 'email': email, 'forworded_email': forworded_email, 'securityque1': securityque1, 'securityans1': securityans1, 'profileurl': profileurl, 'phonenoattached': phonenoattached, 'securityque2': securityque2, 'securityans2': securityans2, 'testpassed': testpassed, 'lastauditdate': lastauditdate, 'salesreppoints': salesreppoints, 'othernotes': othernotes, 'alloteduserid': alloteduserid, 'rating': rating, 'created': created, 'modified': modified, 'payment_remark': payment_remark, 'associated_paypal': associated_paypal, 'contract_on_hold': contract_on_hold, 'stats': stats, 'quota': quota,'is_shared':is_shared ,'process_notes' : process_notes}, function (data) {
                if (data == 1) {
                    window.parent.location.href = window.parent.location.href;
                }
            });
        }

    }

</script>

         <?php echo $this->Form->create($SaleProfilesData,array('url' => ['action' => 'quickedit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?> 



            <?php echo $this->Flash->render(); 

    $session_info = $session->read("SESSION_ADMIN");
        $userID = $session_info[0];  ?>
    
                <!-- start id-form -->
   <center> <h2 class="font-bold text-success"> Edit Sales Profile </h2></center>

           




    <!--  start content-table-inner -->
  <div class="col-sm-12">
            <div class="white-box">

<div class="form-group">
                    <label for="exampleInputuname">User Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
  <?php
                 echo $this->Form->input("username",array( "class"=>"form-control","id"=>"username","label"=>false,"div"=>false));
                 echo $this->Form->input("id",array("class"=>"form-control","id"=>"saleProfileid"));
           
            ?></div>
       
               <?php if(isset($errors['username'])){
                            echo $this->General->errorHtml($errors['username']);
                            } ;?> </div> 



              <div class="form-group">
                    <label for="exampleInputuname">Password:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div><?php
             echo $this->Form->input("password",array( 'type'=>'text',"class"=>"form-control","id"=>"password","label"=>false,"div"=>false));  
             ?></div>
                 <?php if(isset($errors['password'])){
                            echo $this->General->errorHtml($errors['password']);
                            } ;?></div>



            <div class="form-group">
                    <label for="exampleInputuname">Name on Profile:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div><?php
             echo $this->Form->input("nameonprofile",array( "class"=>"form-control","id"=>"nameonprofile","label"=>false,"div"=>false));  
             ?></div>
                 <?php if(isset($errors['nameonprofile'])){
                            echo $this->General->errorHtml($errors['nameonprofile']);
                            } ;?></div>


            
           <div class="form-group">
                    <label for="exampleInputuname">Type:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div><?php                  $options=array('Upwork'    =>  'Upwork', 
                                                             //'elance'    => 'Elance'
                                                             'Freelancer' => 'Freelancer',
                                                             'PPH' => 'PPH',
                                                             'Guru' => 'Guru',
                                                             'Others' => 'Others'
                                                            );
            echo $this->Form->input("type",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"type","label"=>false,"div"=>false));    
             ?></div></div>









            <div class="form-group">
                    <label for="exampleInputuname">Technology For:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div><?php   $options=array('php'    =>  'PHP', 
                         'mobile'    => 'Mobile',
                         'designing'    => 'Designing',
                         '.net'    => '.Net'
                        );
            echo $this->Form->input("technologyfor",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"technologyfor","label"=>false,"div"=>false));    
            ?></div></div>




             <div class="form-group">
                    <label for="exampleInputuname">Status:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
<?php
            $options=array('1'    =>  'Active', 
                         '0'    => 'Deactive',
                        
                        );
            echo $this->Form->input("status",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"profile_status","label"=>false,"div"=>false));    
            ?></div></div>





  <div class="form-group">
                    <label for="exampleInputuname">Id Verified::</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
<?php    $options=array(
                    0    => 'No',
                    1    =>  'Yes', 
                            );
            echo $this->Form->input("idverified",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"idverified","label"=>false,"div"=>false));    
             ?></div></div>







   <div class="form-group">
                    <label for="exampleInputuname">Payment Method:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
<?php 
 echo $this->Form->input("payment_methods_id",array("type"=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            
            ?></div></div>





            

          <div class="form-group">
                    <label for="exampleInputuname">Do we have his/her verification documents?:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    
        
    <?php
             $options=array( '0'    => 'No',
                        '1'    =>  'Yes', 
                        );
            echo $this->Form->input("docindropbox",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"docindropbox","label"=>false,"div"=>false));    
            
            // echo $form->input("docindropbox",array( "class"=>"form-control","id"=>"docindropbox","label"=>false,"div"=>false));  
             ?></div></div>






                <div class="form-group">
                    <label for="exampleInputuname">Is currently in use ?:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>    
<?php    $options=array(
                    0    => 'No',
                    1    =>  'Yes', 
                            );
            echo $this->Form->input("is_shared",array('type'=>'select','options'=>$options,"class"=>"form-control","id"=>"is_shared","label"=>false,"div"=>false));    
             ?></div></div>




              
      <div class="form-group">
                    <label for="exampleInputuname">Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>  <?php
             echo $this->Form->input("email",array( "class"=>"form-control","id"=>"email","label"=>false,"div"=>false));  
             ?></div>
            <?php if(isset($errors['email'])){
                            echo $this->General->errorHtml($errors['email']);
                            } ;?></div>


                <div class="form-group">
                    <label for="exampleInputuname">Forwarded Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>  <?php
             echo $this->Form->input("forworded_email",array( "class"=>"form-control","id"=>"forworded_email","label"=>false,"div"=>false));  
             ?></div></div>
                        <div class="error-message"><?php // echo $bug['forworded_email'];?></div>
                   







                
      <div class="form-group">
                    <label for="exampleInputuname">Security Ques 1:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("securityque1",array( "class"=>"form-control","id"=>"securityque1","label"=>false,"div"=>false));  
             ?></div></div>




                     <div class="form-group">
                    <label for="exampleInputuname">Security Ans 1:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

        

            <?php
             echo $this->Form->input("securityans1",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>


          <div class="form-group">
                    <label for="exampleInputuname">Profile Url:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

        
           <?php
             echo $this->Form->input("profileurl",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>



     <div class="form-group">
                    <label for="exampleInputuname">Phone No. Attached:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 



        
                <?php
             echo $this->Form->input("phonenoattached",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div>
             <?php if(isset($errors['phonenoattached'])){
                            echo $this->General->errorHtml($errors['phonenoattached']);
                            } ;?></div>



     <div class="form-group">
                    <label for="exampleInputuname">Security Ques 2:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 




         <?php
             echo $this->Form->input("securityque2",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>



     <div class="form-group">
                    <label for="exampleInputuname">Security Ans  2:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

      <?php
             echo $this->Form->input("securityans2",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>





     <div class="form-group">
                    <label for="exampleInputuname">Test Passed:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
        <?php
             echo $this->Form->input("testpassed",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>







     <div class="form-group">
                    <label for="exampleInputuname">Last Audit Date:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("lastauditdate",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"lastauditdate","label"=>false,"div"=>false,'value'=> $lastauditdate));
        
            // echo $form->input("lastauditdate",array( "class"=>"form-control","label"=>false,"div"=>false));  
             ?></div></div>




                   <div class="form-group">
                    <label for="exampleInputuname">Sales Rep Points:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("salesreppoints",array("class"=>"form-control","id"=>"salesreppoints","label"=>false,"div"=>false));  
             ?></div></div>









           

     <div class="form-group">
                    <label for="exampleInputuname">Rating:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("rating",array( "class"=>"form-control","id"=>"rating","label"=>false,"div"=>false));  
             ?></div></div>






     <div class="form-group">
                    <label for="exampleInputuname">Other Notes:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("othernotes",array("class"=>"form-control","id"=>"othernotes","label"=>false,"div"=>false));
             ?></div></div>






                   

    <div class="form-group">
                    <label for="exampleInputuname">Alloted User id:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 
<?php
                $options = $this->General->getUsersprofile('5'); 

                   
                if(isset($resultData['alloteduserid']) && $resultData['alloteduserid'] !== ''){
                 
                    $cont_cats = explode(",",$resultData['alloteduserid']);
                }else{
                    $cont_cats = [];
                }
                // this select is not working properly so html select was added 
                 $this->Form->select("alloteduserid",$options, ['type' => 'select','size'=>'10','multiple' => true,"class"=>"multiSelect",'default' => $cont_cats,'id'=> 'team_id']);
             ?>
                <select name="alloteduserid" multiple size='10' class="multiSelect" id ="team_id" >
                <?php foreach($options as $key => $value) {;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?></option>
                 <?php };?>
                </select>   



            </div></div>







      <div class="form-group">
                    <label for="exampleInputuname">Associated Paypal:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 


        <?php
             echo $this->Form->input("associated_paypal",array( "class"=>"form-control","label"=>false,"div"=>false ,'id' => 'associated_paypal'));  
             ?></div></div>



    <div class="form-group">
                    <label for="exampleInputuname">Payment Remark:</label>
                   
                      <?php
             echo $this->Form->input("payment_remark",array("class"=>"form-control payment_remark","label"=>false,"div"=>false,'id' => 'payment_remark'));  
             ?></div>




                <div class="form-group">
                    <label for="exampleInputuname">Contract On Hold:</label>
               <?php
             echo $this->Form->input("contract_on_hold",array("class"=>"form-control","label"=>false,"div"=>false,'id' => 'contract_on_hold'));  
             ?></div>



    <div class="form-group">
                    <label for="exampleInputuname">Stats:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> 

        <?php
             echo $this->Form->input("stats",array("class"=>"form-control","label"=>false,"div"=>false,'id' => 'stats'));  
             ?></div></div>






    <div class="form-group">
                    <label for="exampleInputuname">Created:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("created",array("type"=>"text","readonly"=>true,"class"=>"form-control","id"=>"created","label"=>false,"div"=>false,'value'=> $nextrelease));
        
             ?></div></div>





       

                <div class="form-group">
                    <label for="exampleInputuname">Modified:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div> <?php
             echo $this->Form->input("modified",array("type"=>"text","readonly"=>true,"class"=>"form-control modified","id"=>"modified","label"=>false,"div"=>false,'value'=> $finishdate));
        
                 ?></div></div>

     <div class="form-group">
        <label for="exampleInputuname">Process Notes:</label>
        <div class="input-group">
          <div class="input-group-addon"><i class="ti-wallet"></i></div> 
            <?php echo $this->Form->input("process_notes",array("type"=>"textarea","class"=>"form-control process_notes","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"Enter Previous Employment."));?>
        </div>
    </div>



<div class="form-group">
  
        <?php
         echo $this->Form->hidden("quota",array( "class"=>"form-control","value"=>"60","id"=>"quota","label"=>false,"div"=>false));  
         ?>
            
            <?php echo $this->Form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: editsaleprofile();","class"=>"btn btn-success
")); 
            ?>

 </div>
 </div>
</div>



</form>













        <!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->

<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->

<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->



      
<!--Style Switcher -->

<script>
   $(document).ready(function () {
        //calender
        $("#created").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0
        });
        //calender
        $(".modified").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0
        });
        $("#lastauditdate").datepicker({
            dateFormat: 'yy-mm-dd',
            //minDate: 0
        });//lastauditdate

        //fancybox settings

    });

</script>