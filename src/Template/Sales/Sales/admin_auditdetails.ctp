<?php
echo $this->element("sql_dump");
echo $html->css(array("screen","jquery_ui_datepicker")); ?>
<?php echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','function.js')); ?>
<?php echo $html->css(array("screen")); ?>
<script type="text/javascript">
  
    /*------------------------------Create New sales audit ----------------------------------------------*/

    function audit_report()
    {
        var auditpoint=$('#auditpoint').val();
         var profile_id=$('#profile_id').val();
        if($.active>0){
			}else{
					$('.ajaxloaderdiv').show();
				$.post(base_url+'admin/Profileaudits/auditdetails',{'auditpoint':auditpoint,'profile_id':profile_id},function(data){
					//alert(data);
					if(data==0){
						$("#error_msg").html("Audit Point can not Added");
					}else{
						document.forms["AuditReportForm"].reset();
						$('#product-table').append(data);
						$('.ajaxloaderdiv').hide();
						$("#success").html("Milestone has been added successfully!");
					}
				});
			}
    }

</script>
<?php echo $form->create('saleProfile',array('method'=>'POST', "class" => "longFieldsForm", "name" => "AuditReportForm")); ?>
<!--  start content-table-inner -->
<style>
    .add_milstone
    {
        width:150px;
        height:25px;
    }

</style>
<div id="content-table-inner">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
    `   <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr valign="top">
            <td>
                <!--  start table-content  -->
                <div id="table-content">
			<?php echo $session->flash(); ?>

                    <center><div style="font-weight:bold;">Sales Profile Audit Report</div></center>

                    <!--  end product-table................................... --> 
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                        <tr>
                            <th class="userattend" style="width:80%;height:20px;" nowrap><h4>Audit Points</h4></th>
                        <th class="userattend" style="width:20%;height:20px;" nowrap></th>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $form->hidden('profile_id',array('id'=>"profile_id","value"=>$audit_id,"label"=>false,"div"=>false));?>
                                <?php echo $form->input('auditpoint',array('id'=>'auditpoint',"label"=>false,"div"=>false));?>
                            </td>
                            <td><?php echo $form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: audit_report();","style"=>"width:150px;")); 
                                        ?>
                            </td>
                        </tr>
                    </table>
                  
                </table>
    <!--  end content-table  -->
</div>

<div class="clear"></div>


</div>
<?php echo $form->end(); ?>
<!--  end content-table-inner  --> 