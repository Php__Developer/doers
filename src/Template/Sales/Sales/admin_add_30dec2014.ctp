	<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#created" ).datepicker({
				dateFormat: 'dd-mm-yy',
				//minDate: 0
			});
		//calender
			$( "#modified" ).datepicker({
				dateFormat: 'dd-mm-yy',
				//minDate: 0
			});
			$( "#lastauditdate" ).datepicker({
				dateFormat: 'dd-mm-yy',
				//minDate: 0
			});//lastauditdate
		
		 //fancybox settings
			$(".view_instc").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	750, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			
			
	});
	
</script>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('saleProfile',array('action'=>'add','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php 
		//echo "<pre>";print_r($Error);
		$bug=array();
		if(count($Error)>0){
		foreach($Error as $k=>$error){
		$bug[$k]=$error;
		}
		
		}
		
		?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">User Name:</th>
			<td><?php
				 echo $form->input("username",array( "class"=>"inp-form","label"=>false,"div"=>false));
				?>
				<div class="error-message"><?php echo $bug['username'];?></div>
				</td>
			<td></td>
			
		</tr>
		<tr>
			<th valign="top">Password:</th>
			<td><?php
			 echo $form->input("password",array('type'=>'text',"class"=>"inp-form","label"=>false,"div"=>false));  
			 ?>
			 <div class="error-message"><?php echo $bug['password'];?></div>
			 </td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Name On Profile:</th>
			<td><?php
			 echo $form->input("nameonprofile",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Type:</th>
			<td><?php				   $options=array('odesk'    =>  'Odesk', 
								  'elance'    => 'Elance'
								);
			echo $form->input("type",array('type'=>'select','options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
             ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Technology For:</th>
			<td><?php   $options=array('php'    =>  'PHP', 
						 'mobile'    => 'Mobile',
						 'designing'    => 'Designing',
						 '.net'    => '.Net'
						);
			echo $form->input("technologyfor",array('type'=>'select','options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
            ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Status:</th>
			<td><?php
			 $options=array('1'    =>  'Active', 
						 '0'    => 'Deactive',
						
						);
			echo $form->input("status",array('type'=>'select','options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
            ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Id Verified:</th>
			<td><?php	 $options=array(0    => 'No',
					1    =>  'Yes', 
						);
			echo $form->input("idverified",array('type'=>'select','options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
         	 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Doc In Dropbox:</th>
			<td><?php
			 $options=array( '0'    => 'No',
						'1'    =>  'Yes', 
						);
			echo $form->input("docindropbox",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"docindropbox","label"=>false,"div"=>false));    
         
			// echo $form->input("docindropbox",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Email:</th>
			<td><?php
			 echo $form->input("email",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?>
			 <div class="error-message"><?php echo $bug['email'];?></div>
			 </td>
			<td></td>
		</tr><tr>
			<th valign="top">Security Ques 1:</th>
			<td><?php
			 echo $form->input("securityque1",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Security Ans 1:</th>
			<td><?php
			 echo $form->input("securityans1",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Profile URL:</th>
			<td><?php
			 echo $form->input("profileurl",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Phone No. Attached:</th>
			<td><?php
			 echo $form->input("phonenoattached",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>		
		<tr>
			<th valign="top">Security Ques 2:</th>
			<td><?php
			 echo $form->input("securityque2",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Security Ans 2:</th>
			<td><?php
			 echo $form->input("securityans2",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Test Passed:</th>
			<td><?php
			 echo $form->input("testpassed",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Last Audit Date:</th>
			<td><?php
			 echo $form->input("lastauditdate",array("type"=>"text","readonly"=>true,"class"=>"inp-form","id"=>"lastauditdate","label"=>false,"div"=>false));
        
			// echo $form->input("lastauditdate",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Auditor Points:</th>
			<td><?php
			 echo $form->input("auditorpoints",array("class"=>"form-textarea","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr><tr>
			<th valign="top">Sales Rep Points:</th>
			<td><?php
			 echo $form->input("salesreppoints",array("class"=>"form-textarea","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Quota:</th>
			<td><?php
			 echo $form->input("quota",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Rating:</th>
			<td><?php
			 echo $form->input("rating",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Other Notes:</th>
			<td><?php
			 echo $form->input("othernotes",array("class"=>"form-textarea","label"=>false,"div"=>false));
          	 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Alloted User id:</th>
			<td><?php
					$options = $general->getUsersprofile('5'); 
				/*  if($this->data['User']['role_id'] != ''){
					$cont_cats = explode(",",$this->data['User']['role_id']);
					}else{
					$cont_cats = '';
					}
					,'selected' =>$cont_cats
					*/ 
				echo $form->input("alloted_id",array( "type"=>'select','size'=>'10','multiple' => true,'options'=>$options,"label"=>false,"div"=>false,"class"=>"multiSelect","id"=>"team_id"));
         		 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Created:</th>
			<td><?php
			 echo $form->input("created",array("type"=>"text","readonly"=>true,"class"=>"inp-form","id"=>"created","label"=>false,"div"=>false));
        
			// echo $form->input("created",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Modified:</th>
			<td><?php
			 echo $form->input("modified",array("type"=>"text","readonly"=>true,"class"=>"inp-form","id"=>"modified","label"=>false,"div"=>false));
        
			 //echo $form->input("modified",array( "class"=>"inp-form","label"=>false,"div"=>false));  
			 ?></td>
			<td></td>
		</tr>
		
		
		
			<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/Sale_profiles/add'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Projects Management</h5>
          This section is used by Admin and PM only to Manage sensitive profiles.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Go To Listing",
            array('controller'=>'sale_profiles','action'=>'list')
            );	
            ?>
            </li> 
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->