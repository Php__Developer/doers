<?php 
$a1 = new ArrayIterator(array(1, 2, 3, 4, 5, 6,7,8));
$a2 = new ArrayIterator(array(11, 12, 13, 14, 15, 16,17,18));

$it = new MultipleIterator;
$it->attachIterator($a1);
$it->attachIterator($a2);

foreach($it as $e) {
  echo $e[0], ' = ', $e[1], "<br>";
}
?>