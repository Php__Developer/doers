<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>
  <script type="text/javascript">
 //$(document).ready(function() {
  //alert("ok");
  //$( "#startdate" ).removeClass('hasDatepicker');
 //$( ".attendancedate" ).datetimepicker({
       // dateFormat: "dd-mm-yy",
    // }); 
  //});
    </script>
<script type="text/javascript">
$(function() {
    $("#evaluation_from1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#evaluation_to1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#next_review_date1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#effective_from1").datepicker({
        dateFormat: 'MM dd, yy',
    });
    $("#appraisal_date1").datepicker({
        dateFormat: 'MM dd, yy',
    });

});
</script>
<!--  start content-table-inner -->
        <div class="col-sm-6">
            <div class="white-box">
                  <?php //echo $this->Flash->render(); ?>
      <?php echo $this->Form->create('Permission',array('url' => ['action' => 'settings'],'method'=>'POST','onsubmit' => '',"class"=>"login","id"=>"setPermission",'enctype'=>"multipart/form-data")); ?>
 <!-- start id-form -->
              <div class="form-group">
                       <label for="exampleInputpwd1">Process Head:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php $options =  $this->General->getuser_name(); 
                        echo $this->Form->select('process_head',[" "=>$options],['class' => 'form-control',"default"=>$p_head['value']] );?>
                      </div>
                  </div>

                   <div class="form-group">
                       <label for="exampleInputpwd1">HR Head:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                    <?php  $options =  $this->General->getuser_name();
                      echo $this->Form->select('hr_head',[" "=>$options],['class' => 'form-control',"default"=>$h_head['value']]);?>
                      </div>
                     <?php if(isset($errors['hr_head'])){
                                        echo $this->General->errorHtml($errors['hr_head']);
                                    } ;?>
                 </div>
                  
            
                 <div class="form-group">
                       <label for="exampleInputpwd1">Operation Head:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                    <?php  $options =  $this->General->getuser_name();
                      echo $this->Form->select('operation_head',[" "=>$options],['class' => 'form-control',"default"=>$o_head['value']] );?>
                </div>
                     <?php if(isset($errors['operation_head'])){
                                        echo $this->General->errorHtml($errors['operation_head']);
                                    } ;?>
                 </div>
                  
       

        
                  <div class="form-group"> 
               <label for="inputName" class="control-label">Designations:</label>
                         <?php
                     echo $this->Form->input("designations",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter designation","value"=>$desg['value']));
                     if(isset($errors['designations'])){
                                  echo $this->General->errorHtml($errors['designations']);
                                  } ;
                      ?>
               </div>
<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 </div>
 </div>
</div>
    <!-- end id-form  -->

    

 <div class="col-sm-6">
 	<div class="white-box">
 		<div class="form-group">
 			<div class="row el-element-overlay m-b-40">
 				<div class="sttabs tabs-style-linebox erw">
 					<nav class="lis">
 						<h4 class="media-heading">Settings</h4>
 						HRM Settings can be customized from here .
 						  <ul >
 							<li> 
 							</li> 
 						</ul>
 					</nav>
 				</div><!-- /tabs -->
 			</div>   
 		</div>
 	</div>

 	<div class="white-box">
 		<div class="form-group">
 			<div class="row el-element-overlay m-b-40">
 				<div class="sttabs tabs-style-linebox erw">
 					<nav class="lis">
 					<h4 class="media-heading">Enable Ratings: </h4>
 						<fieldset>
			 <div style="float:left;padding: 23px;"> <b>Enable <?php echo "<i  class='alert-danger' style='font-size:18px;'>".date("F,Y", strtotime( "-1 month" ))."</i>" ?> Ratings :</b></div>
			 <div style="float:left;padding:23px; 0 0 29px;">
			<?php echo $this->Form->hidden('set');
				echo $this->Form->submit('Submit',array('class'=>"btn btn-success","value"=>"saveme",'div'=>false,"name"=>"generateEval"))."&nbsp;&nbsp;&nbsp;<br/>";  ?>
			 </fieldset>

 					</nav>
 				</div><!-- /tabs -->

 			</div>   
 		</div>
 	</div>

  </div>   
    
      <!-- /.right-sidebar -->



</body>
</html>
