<?php
//echo $search1; die;
//$this->Url->build('/', true); die;
$newUrl = "employeelist".$urlString;
//echo $search1." ".$search2; die;
$urlArray = array(
    'field'     => $search1,
    'value'     => $search2
);
//$this->Paginator->options(array('url'=>$urlArray));
$this->Paginator->options(array('url'=> array('field'   => $search1,
                                     'value'    => $search2)));
$this->Paginator->templates('number');
?>
<script type="text/javascript">

    $(document).ready(function () {
        $(".credentialdetal").fancybox({
            'type': 'iframe',
            'autoDimensions': false,
            'width': 700,
            'height': 550,
            'onStart': function () {
                jQuery("#fancybox-overlay").css({"position": "fixed"});
            }
        });
    });         
</script>   
<?php echo $this->Form->create('Employee',array('url' => ['action' => 'employeelist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "employeelistForm", "id" => "mainform")); ?>
<!--  start content-table-inner -->
<div id="content-table-inner">
    <?php $user = $session->read("SESSION_ADMIN"); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr valign="top">
            <td>

                <!--  start table-content  -->
                <div id="table-content">
            <?php echo $this->Flash->render(); ?>
                    <table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
                        <tr>
                            <td width="14%">
                                <b>Search by:</b>
                <?php
                $fieldsArray = array(
                ''                        => '---',
                'Users.employee_code'     => 'Employee Code',
                'Users.first_name'         => 'First Name',
                'Users.username'  => 'Email',
                'Users.phone'  => 'Phone',
                'Appraisals.appraised_amount'  => 'Current Salary',
                );
                echo $this->Form->select("User.fieldName",$fieldsArray,['value'=> $search1],array("id"=>"searchBy","label"=>false,"style"=>"width:200px","class"=>"styledselect","empty"=>false),false); ?>
                            </td>
                            <td width="20%">
                                <b>Search value:</b><br/>
                <?php
                $display1   = "display:none";
                $display2   = "display:none";
                if($search1 != "User.status"){
                    $display1 = "display:block";
                }else{
                    $display2 = "display:block";
                }
                    echo $this->Form->input("User.value1",array("id"=>"search_input","class"=>"top-search-inp","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
                    
                ?>
                            </td>
                            <td width="40%"><br/>
                <?php
                echo $this->Form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
                echo $this->Html->link("Reset",
                ['controller'=>'employees','action'=>'employeelist','prefix' => 'employees'],['class'=>"form-reset"]);   
                 $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/employees/employeelist', true)));             
                ?>
                            </td>
                            <td>
            <?php  $sum=0;
            if(count($resultData)>0){
               // pr($resultData);
                foreach($resultData as $r){
                   // pr($r);
                    if($r['status'] == '1'){

                        //$UserID = $r['id'];
                        //pr( $UserID);
                        //$rt = $this->General->getInfoFromAppraisal($UserID);
                       // pr($rt);
                        $sum+=$r['appraised_amount'];
                        }
                }
            }
            echo " Sum of all salaries: $sum";
            ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <div class="addLinks">
 
                <?php echo $this->Html->link("Add New Employee",
                ['controller'=>'employees','action'=>'add','prefix' => 'employees']);   
                ?>
                    </div>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                        <tr>
                            <th class="table-header-check"><input  type="checkbox" id="toggle-all"> </th>
                            <th class="table-header-repeat line-left minwidth-1"  width="14%"><?php echo $this->Paginator->sort('employee_code', 'Employee Code');?></th>
                            <th class="table-header-repeat line-left minwidth-1"  width="14%"><?php echo $this->Paginator->sort('first_name','First Name');?></th>
                            <th class="table-header-repeat line-left minwidth-1"  width="27%"><?php echo $this->Paginator->sort( 'username','Email');?></th>
                            <th class="table-header-repeat1 line-left minwidth-1"  width="16%"><?php echo $this->Paginator->sort( 'phone','Phone');?></th>
                            <th class="table-header-repeat line-left" width="10%"><?php echo $this->Paginator->sort( 'current_salary','Salary');?></th>
                            <th class="table-header-repeat1 line-left" width="6%"><?php echo $this->Paginator->sort('status','Status');?></th>
                            <th class="table-header-options1 line-left" width="21%"><a href="#A">Options</a></th>
                        </tr>
                <?php  if(count($resultData)>0){
            $i = 1;
            foreach($resultData as $result):
                //pr($result['first_name']); die;
                //pr($resultData);
            if(!$result['status']%2)$class = "alternate-row"; else $class = "";  ?>
                        <tr class="<?php echo $class; ?>">
                            <td><input  type="checkbox" name="IDs[]" value="<?php echo $result['id'];?>"/></td>
                            <td><?php echo $result['employee_code']; ?></td>
                            <td><?php echo $result['first_name']; ?></td>

                            <td><?php echo $this->Html->link($result['username'],"mailto:".$result['username']); ?></td>
                            <td><?php echo nl2br($result['phone']); ?></td>
                            <td><?php //echo number_format($result['User']['current_salary']); 
                       // $UserID = $result['id'];
                      //  $resultData = $this->General->getInfoFromAppraisal($UserID);
                       //pr($resultData);
                        //$salary = (isset($resultData[0]['appraised_amount'])) ? $resultData[0]['appraised_amount'] : "0";
                       // echo $salary;
                            echo $result['appraised_amount']; 
                    ?>
                            </td>

                            <td style="text-align:center">
                        <?php echo ($result['status'] == '1')?$this->Html->image(BASE_URL."images/table/green.png", array("alt"=>"Activated")):$this->Html->image(BASE_URL."images/table/red.png", array("alt"=>"Deactivated")); ?>
                            </td>
                            <td class="options-width" align="center">
                        <?php
                        echo $this->Html->link("",
                        array('controller'=>'employees','action'=>'edit','id' => $result['id']),
                        array('class'=>'icon-1 info-tooltip','title'=>'Edit')
                    );
                        ?>
                        <?php
                        echo $this->Html->link("",
                        array('controller'=>'employees','action'=>'delete','id' => $result['id']),
                        array('class'=>'icon-2 info-tooltip delete','title'=>'Delete')
                    );
                        ?>
                        <?php
                        echo $this->Html->link("",
                        array('controller'=>'employees','action'=>'credential_detail','id' => $result['id']),
                        array('class'=>'info-tooltip credentialdetal','title'=>'Credential Detail')
                    );
                        ?>
                        
                            </td>
                        </tr>
                <?php $i++ ;
                endforeach; ?>
                <?php } else { ?>
                        <tr>
                            <td colspan="10" class="no_records_found">No records found</td>
                        </tr>
            <?php } ?>
                    </table>
                    <!--  end product-table................................... --> 
                </div>
                <!--  end content-table  -->

                <!--  start actions-box ............................................... -->
                <div id="actions-box">
                    <a href="" class="action-slider"></a>
                    <div id="actions-box-slider">
                    <?php echo $this->Form->submit("Activate",array("div"=>false,"class"=>"action-activate","name"=>"publish",'onclick' => "return atleastOneChecked('Activate selected records?','employees/changeStatus');")); ?>
                    <?php echo $this->Form->submit("Deactivate",array("div"=>false,"class"=>"action-deactivate","name"=>"unpublish",'onclick' => "return atleastOneChecked('Deactivate selected records?','employees/changeStatus');")); ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <!-- end actions-box........... -->

                <!--  start paging..................................................... -->
                <table border="0" cellpadding="0" cellspacing="0" id="paging-table">
                    <tr>
                        <td>
                            <?php  echo $this->Paginator->prev('<span class="page-left" ></span>', ['escape' => false] );?>
                                <div id="page-info">
                                    <?php  echo $this->Paginator->counter( 'Page {{page}} / {{pages}}');
                                     echo $this->Paginator->next('<span class="page-right" ></span>', ['escape' => false] ); ?>
                                </div>
                        </td>
                    </tr>
                </table>
                <!--  end paging................ -->

                <div class="clear"></div>

            </td>
            <td>

    <?php echo $this->element('user_sidebar'); ?>

            </td>
        </tr>
    </table>

    <div class="clear"></div>


</div>
<?php echo $this->Form->end(); ?>
<!--  end content-table-inner  -->