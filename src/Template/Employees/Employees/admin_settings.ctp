<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('Employee',array('action'=>'settings','method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
	<?php echo $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
		<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<?php  //pr($resultData) ; die; ?>
		<tr>
			<td valign="top" colspan="2" >
			<fieldset>
			  <legend><h3> Enable Ratings: </h3></legend>
			 <div style="float:left;padding: 23px;"> <b>Enable <?php echo "<i style='background-color:#EADD64;font-size:18px;'>".date("F,Y", strtotime( "-1 month" ))."</i>" ?> Ratings :</b></div>
			 <div style="float:left;padding:23px; 0 0 29px;">
			<?php echo $form->hidden('set');
				echo $form->submit('Saveme',array('class'=>"form-submit","value"=>"saveme",'div'=>false,"name"=>"generateEval"))."&nbsp;&nbsp;&nbsp;<br/>";  ?>
			 </fieldset>
			
			</td>
			
		</tr>
		<tr>
		<th valign="top">Process Head:</th>
      <td>
		<?php    $options = $general->getuser();
				 echo $form->input("process_head",array("type"=>"select","class"=>"ourselect","options"=>$options,"label"=>false,"div"=>false,"selected"=>$resultData['process_head']));
            ?></td>
           <td> </td>
		</tr>
		<tr>
		<th valign="top">HR Head:</th>
      <td>
		<?php    $options = $general->getuser();
				 echo $form->input("hr_head",array("type"=>"select","class"=>"ourselect","options"=>$options,"label"=>false,"div"=>false,"selected"=>$resultData['hr_head']));
            ?></td>
           <td> </td>
		</tr>
		<tr>
		<th valign="top">Operation Head:</th>
      <td>
		<?php    $options = $general->getuser();
				 echo $form->input("operation_head",array("type"=>"select","class"=>"ourselect","options"=>$options,"label"=>false,"div"=>false,"selected"=>$resultData['operation_head']));
            ?></td>
           <td> </td>
		</tr>
		<tr>
			<th valign="top">Designations:</th>
			<td><?php 
				 echo $form->input("designations",array("type"=>"textarea","class"=>"form-textarea","label"=>false,"div"=>false,"value"=>$resultData['designations'] ));
				 echo (!empty($Error['domain1'])?"<div class='error-message'>".$Error['domain1']."</div>":''); 
            ?></td>
		</tr>
	
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
		<?php echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; ?>
		</td>
		<td></td>
	</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Settings</h5>
           HRM Settings can be customized from here .
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->