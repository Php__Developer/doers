<?php echo $this->Html->css(array("screen")); ?>
<?php echo $this->Html->script(array('jquery-1.4.1.min')) ?>
<script type="text/javascript">
	$(document).ready(function() { 
		$('.ourselect').change(function(){
			$('#bdayForm').submit();
		});
	});
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $this->Form->create('Employee',array('id'=>'bdayForm','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
		<div style="padding-left:1em;padding-bottom:2em;">
			<span><b>Select Month :</b></span>
			<?php
						$options = $common->getMonthsArray();
						$default = date("m");
						echo $this->Form->input("month",array("type"=>"select","class"=>"ourselect","options"=>$options,"default"=>$default,"label"=>false,"div"=>false));
				?>
		</div>
		</div>
	<?php echo $this->Form->end(); ?>
	<!--  start table-content  -->
			<div id="table-content">
			<center><span style="font-weight:bold;"> Monthly Birthday List </span></center>
				<table style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
					<tr>
						<th class="userattend" width="8%">Sr No.</th>
						<th class="userattend" width="25%">Name</th>
						<th class="userattend" width="20%">Birth Date</th>
						<th class="userattend" width="47%">Roles</th>
					</tr>
				<?php if(count($resultData)>0){
				$i = 1;
				foreach($resultData as $bdayData): ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php $str = strip_tags($bdayData['first_name'].' '.$bdayData['last_name']); echo substr($str,0,22).((strlen($str)>22)? "...":""); ?></td>
					<td><?php echo date("d, F Y",strtotime($bdayData['dob'])); ?></td>
					<td><?php
					$UserRole = ""; $rolesAry = explode(",",$bdayData['role_id']); 
							foreach($rolesAry as $roles){
								if(in_array($roles,array_keys($rolesData)))
								{
									$UserRole .= ", ".$rolesData[$roles];
								}
							}  
						echo ltrim($UserRole,",");	?>
					</td>
				</tr>
				<?php 
				$i ++;
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="4" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->