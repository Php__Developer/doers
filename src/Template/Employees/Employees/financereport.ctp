<?php

echo  $this->Html->css(array("screen")); ?>
<?php echo $this->Html->script(array('jquery-1.4.1.min')) ?>
<!--  start content-table-inner -->
<div id="content-table-inner">
    <!--  start table-content  -->

    <div id="table-content">
        <center><span style="font-weight:bold;"> Employee Financial Report</span></center>
        <div style="float:left;margin-left:840px;">
			<?php echo $this->Html->link("",
            array('controller'=>'Employees','action'=>'employeefinancexal'),
			array('class'=>'icon-2 info-tooltip financeexcel','title'=>'Download Finance Report in Excel')
            );?>
            <?php echo $this->Html->link("",
            array('controller'=>'Employees','action'=>'employeefinancpdf'),
            array('class'=>'icon-2 info-tooltip financepdf','title'=>'Download Finance Report in PDF')
            );?></div>
			
        <table style="margin-top:15px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
            <tr>
                <th class="userattend" width="22%">Name</th>
                <th class="userattend" width="17%">PAN No.</th>
                <th class="userattend" width="16%">Account No.</th>
                <th class="userattend" width="15%">Bank Name</th>
                <th class="userattend" width="23%">Bank Address</th>
                <th class="userattend" width="19%">IFSC</th>
                <th class="userattend" width="23%">Current Salary</th>
            </tr>
            <?php

  //   pr($query);die();
            ?>
				<?php 
				//echo "<pre>";print_r($resultData);exit;
				if(count($query)>0){
				$i = 1; 
				foreach($query as $panData):
            //    pr($panData);die(); ?>
            <tr>
                <td><?php echo $panData['first_name']." ".$panData['last_name']; ?></td>
                <td><?php echo $panData['pan']; ?></td>
                <td><?php echo $panData['account_number']; ?></td>
                <td><?php echo $panData['bankName']; ?></td>
                <td><?php echo $panData['branch']; ?></td>
                <td><?php echo $panData['ifsc']; ?></td>
                   <td><?php echo $panData['appraised_amount']; ?></td>
                                       
            </tr>
				<?php 
				$i ++;
				endforeach; ?>
				<?php } else { ?> 
            <tr>
                <td colspan="4" class="no_records_found">No records found</td>
            </tr>
		<?php } ?>
        </table>
        <!--  end product-table --> 
    </div>
    <!--  end content-table  -->
    <div class="clear"></div>

</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->