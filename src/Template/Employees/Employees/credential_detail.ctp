<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">













<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->css(array("style_new")); ?>
<style>
    .notification{
     color:red !important;
   font-size:20px; 
   font-weight:bold;
   margin-top:17px;
   line-height:51px;
  } 
  .notificationAllotedto{
    color:red !important;
   font-size:20px; 
   font-weight:bold;
   margin-top:17px;
   line-height:51px;
  }
  
#product-table td.alternate-row {
    background: #ff6544 none repeat scroll 0 0;
}
</style>

<?php echo $this->Html->css(array("jquery.fancybox-1.3.4.css")); ?>
</style>
<script type="text/javascript">

    $(document).ready(function () {
        $(".credentialdetal1").fancybox({
            'type': 'iframe',
            'autoDimensions': false,
            'width': 700,
            'height': 550,
            'onStart': function () {
                jQuery("#fancybox-overlay").css({"position": "fixed"});
            }
        });
      $(".credentialdetal2").fancybox({
            'type': 'iframe',
            'autoDimensions': false,
            'width': 700,
            'height': 550,
            'onStart': function () {
                jQuery("#fancybox-overlay").css({"position": "fixed"});
            }
        });
    
    
    });
  function display(id) {
    var temp = id.split("_");

    $('#paytext_' + temp[1]).toggle();
    $('#showpass_' + temp[1]).toggle();
}
function displaysec(id) {
    var temp = id.split("_");

    $('#paysectext_' + temp[1]).toggle();
    $('#showsecpass_' + temp[1]).toggle();
}
function displays(id) {
    var temp = id.split("_");

    $('#paytexts_' + temp[1]).toggle();
    $('#showpasss_' + temp[1]).toggle();
}
function display4(id) {
    var temp = id.split("_");

    $('#paytext4_' + temp[1]).toggle();
    $('#showpass4_' + temp[1]).toggle();
}
</script>

<!--  start content-table-inner -->
<?php echo $this->Form->create('', array('type' => 'get')); ?><table border="0" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:59px;">
<tr>
    <td width="14%">
        <b>Search By Name:</b>
        <?php 

        $UserDetail 


        ?>

        <?php
        echo $this->Form->input('search',array("options"=>$UserDetail,"label"=>false,"style"=>"width:200px","class"=>"form-control","empty"=>false /*,'value'=> $searchkword */));

        ?>
    </td>
    <td width="40%"><br/>
      <?php
      echo $this->Form->button('Search', array('class'=>'btn btn-success','name'=>'submit','class'=>'btn btn-success','id'=>'search','onclick'=>'setSubmitMode(this.id)'));
 

      ?>
  </td>
</tr>

<?php

?>
</table>
<br/>

    <a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
    <?php $user = $session->read("SESSION_ADMIN");	                 
    ?>
    <table border="0" width="98%" cellpadding="0" cellspacing="0">
        <tr valign="top">
            <td>
               <?php   
               ?>
               <!--  start table-content  -->
               <div id="table-content">
                   <?php //echo $session->flash(); ?>
                   <?php  $full_name=$primary_user['first_name']." ".$primary_user['last_name'];

                   ?>
                   <center><span style="font-weight:bold;"><?php echo strtoupper($full_name).", ";?>ALLOTTED CREDENTIALS</span></center>
                   <br><center><span style="font-weight:bold;">Primary Allotted Credential(s)</span></center>
                    <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-10" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
       
                    <tr>
                        <th class="userattend" nowrap><h4>User Name</h4></th>
                        <th class="userattend"  nowrap><h4>Password</h4></th>
                        <th class="userattend" nowrap><h4>Type</h4></th>    
                        <th class="userattend"  nowrap><h4>Forwarded Email</h4></th>
                    </tr>
                    <?php
                    
                    if(count($primary_cerdentail)>0){
                        foreach($primary_cerdentail as $val){
                           ?>
                           <tr>
                            <td><?php  echo $val['username']; ?></td>

<td><a id="showpass4_<?php echo $val->id; ?>" href="#A" onClick='display4(this.id);'>Show</a> <div style="display:none;" class="leftlbl_<?php echo $val->id;  ?>" id="paytext4_<?php echo $val->id; ?>" onClick='display4(this.id);'><?php  echo htmlspecialchars($val->password); ?></div></td>

  <?php if($val['type']=='other'){
                                $type=$val['other'];
                            }else {
                             $type=$val['type'];
                         }?>
                         <td><?php  echo $type; ?></td>
                         <td><?php  echo $val['forworder_email']; ?></td>
                     </tr>
                     <?php }} else{?>
                     <tr>
                        <td colspan="4" class="no_records_found">No records found</td>
                    </tr>
                    <?php } ?>
                </table>

                <!----------------Table to show Secondary credential records---------------->

                <br><center><span style="font-weight:bold;">Secondary Allotted Credential(s)</span></center>
               <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-10" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
                    <tr>
                        <th class="userattend" nowrap><h4>User Name</h4></th>
                        <th class="userattend"  nowrap><h4>Password</h4></th>
                        <th class="userattend" nowrap><h4>Type</h4></th>    
                        <th class="userattend"  nowrap><h4>Forwarded Email</h4></th>
                    </tr>
                    <?php
                    if(count($secon_cerdentail)>0){
                        foreach($secon_cerdentail as $val){
                           ?>
                           <tr>
                            <td><?php  echo $val['username']; ?></td>
                            <td><a id="showsecpass_<?php echo $val['id']; ?>" href="#A" onClick='displaysec(this.id);'>Show</a>	<div style="display:none;" class="leftseclbl_<?php  echo $val['id']; ?>" id="paysectext_<?php  echo $val['id']; ?>" onClick='displaysec(this.id);'><?php  echo htmlspecialchars($val['password']); ?></div>
                            </td>
                            <?php if($val['type']=='other'){
                                $sec_type=$val['other'];
                            }else {
                             $sec_type=$val['type'];
                         }?>
                         <td><?php  echo $sec_type; ?></td>
                         <td><?php  echo $val['forworder_email']; ?></td>
                     </tr>
                     <?php }} else{?>
                     <tr>
                        <td colspan="4" class="no_records_found">No records found</td>
                    </tr>
                    <?php } ?>
                </table>

                <!----------------Table to show sale Profile records---------------->

                <br><center><span style="font-weight:bold;">Sales Profile Credential(s)</span></center>
               <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-10" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
                    <tr>

                        <th class="userattend" nowrap><h4>User Name</h4></th>
                        <th class="userattend" nowrap><h4>Password</h4></th>
                        <th class="userattend"  nowrap><h4>Type</h4></th>    
                        
                    </tr>
                    <?php
                    if(count($pro_data)>0){
                        foreach($pro_data as $pro_val){
                           ?>
                           <tr>
                            <td><?php  echo $pro_val['username']; ?></td>
                            <td><a id="showpass_<?php echo $pro_val['id']; ?>" href="#A" onClick='display(this.id);'>Show</a>	<div style="display:none;" class="leftlbl_<?php  echo $pro_val['id']; ?>" id="paytext_<?php  echo $pro_val['id']; ?>" onClick='display(this.id);'><?php  echo htmlspecialchars($pro_val['password']); ?></div>
                            </td>
                            <td><?php  echo $pro_val['type']; ?></td>
                            
                        </tr>
                        <?php }} else{?>
                        <tr>
                            <td colspan="4" class="no_records_found">No records found</td>
                        </tr>
                        <?php } ?>
                    </table>

                    <br><center><span style="font-weight:bold;">Credential Log Detail</span></center>
                    <br> <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-10" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
                    <tr>

                        <th class="userattend"  nowrap><h4>User Name</h4></th>
                        <th class="userattend"  nowrap><h4>Password</h4></th>
                        <th class="userattend"  nowrap><h4>Type</h4></th>    
                        <th class="userattend"  nowrap><h4>Forwarded Email</h4></th>
                        <th class="userattend"  nowrap><h4>Allocated On</h4></th>
                        <th class="userattend"  nowrap><h4>Deallocated On</h4></th>
                        
                    </tr>
                    <?php 
                    $counterTag = 1;
                    if(count($Credential_for_log2)>0){
                      foreach($Credential_for_log2 as $val1){ 
                        if($val1['allocatedon']=="" or $val1['type']=='saleprofile'){
                        }else{
                            ?>

                            <tr>
                             <td><?php  echo $val1['username']; ?></td>   
                             <td><a id="showpasss_<?php echo $counterTag; ?>" href="#A" onClick='displays(this.id);'>Show</a>	<div style="display:none;" class="leftlbl_<?php echo $counterTag; ?>" id="paytexts_<?php echo $counterTag++; ?>" onClick='displays(this.id);'><?php  echo htmlspecialchars($val1['password']); ?></div>
                              <td><?php  echo $val1['type']; ?></td>
                              <td><?php  echo $val1['forworder_email']; ?></td>
                              <td><?php  echo $val1['allocatedon']; ?></td>
                              <td><?php  echo $val1['deallocatedon']; ?></td>	
                          </tr>
                          <?php } 
                      }
                  } else{?>
                  <tr>
                    <td colspan="6" class="no_records_found">No records found</td>
                </tr>
                <?php } ?>
            </table>
            <br><center><span style="font-weight:bold;">SaleProfile Log Detail</span></center>
            <br> <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-10" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
            <tr>

                <th class="userattend" nowrap><h4>User Name</h4></th>
                <th class="userattend"  nowrap><h4>Password</h4></th>
                <th class="userattend"  nowrap><h4>Type</h4></th>    
                <th class="userattend"  nowrap><h4>Forwarded Email</h4></th>
                <th class="userattend"  nowrap><h4>Allocated On</h4></th>
                <th class="userattend"  nowrap><h4>Deallocated On</h4></th>           					
            </tr>
            <?php    
            $counterTags = 1;					
            if(count($sale_profile_cre_logs)>0){
              foreach($sale_profile_cre_logs as $val3){ 
                if($val3['allocatedon']=="" or $val3['type']=='credential'){
                }else{
                    ?>

                    <tr>
                     <td><?php  echo $val3['username']; ?></td>   
                     <td><a id="showpass4_<?php echo $counterTags; ?>" href="#A" onClick='display4(this.id);'>Show</a>	<div style="display:none;" class="leftlbl_<?php echo $counterTags; ?>" id="paytext4_<?php echo $counterTags++; ?>" onClick='display4(this.id);'><?php  echo htmlspecialchars($val3['password']); ?></div>	
                      <td><?php  echo $val3['type']; ?></td>
                      <td><?php  echo $val3['forworder_email']; ?></td>
                      <td><?php  echo $val3['allocatedon']; ?></td>
                      <td><?php  echo $val3['deallocatedon']; ?></td>	
                  </tr>
                  <?php } }
              } 

              else{?>
              <tr>
                <td colspan="6" class="no_records_found">No records found</td>
            </tr>
            <?php }  ?>
        </table>
        <table>
            <h2 class="text-danger "> Other Important Notes:</h2>
            <h5 class="text-danger font-bold">A) Employee May have Client(s) Credentials for which project he / she is working with.</br>
                B) Employee May have Sales Profile Credentials for which project he / she is working with.</br>
                C) Employee May have Company's Domain Credentials for which project he / she is working with.</br>
                D) Employee May have Other Credentials for which project / profile he / she is working with.</h5>
          
<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>