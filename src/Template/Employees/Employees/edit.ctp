<html>
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup','oribe')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>
  <script type="text/javascript">
 //$(document).ready(function() {
  //alert("ok");
  //$( "#startdate" ).removeClass('hasDatepicker');
 //$( ".attendancedate" ).datetimepicker({
       // dateFormat: "dd-mm-yy",
    // }); 
  //});09-02-2017
    </script>
    <style type="text/css">
    	.input.checkbox > input {
    opacity: 23;
    background-color: #ab8ce4;
}
    .input-group > label {
    margin-left: 40px;
    margin-top: 4px;
    
}	
#animationSandbox > img {
    height: 159px;
    width: 256px;
}

    </style>
    <!-- <?php echo $this->Html->script(array('swfupload/swfupload.js','swfupload/fileprogress.js','swfupload/handlers.js'));?> -->


<script type="text/javascript">
        $(document).ready(function() { 
            //calender
            $( "#doj_temp" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });
            $( "#dor_temp" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $( "#dob_temp" ).datepicker({
                dateFormat: 'dd-mm-yy'
            });

        $('body div#ui-datepicker-div').css({'display':'none'});
        $('#reportto option[value="1"]').html('Management Cell');
    });
 </script>
<!-- <script type="text/javascript">
	$(document).ready(function(){
	   new JsDatePick({
		   useMode:2,
		   target:"doj_temp",
		   dateFormat:"%d/%m/%Y",
		   yearsRange:[1910,2020],
		   //selectedDate:,
		   limitToToday:true
		});
	  new JsDatePick({
		   useMode:2,
		   target:"dor_temp",
		   dateFormat:"%d/%m/%Y",
		   yearsRange:[1910,2020],
		   //selectedDate:,
		   limitToToday:true
		});
	 /* new JsDatePick({
		   useMode:2,
		   target:"dob_temp",
		   dateFormat:"%d-%m-%Y",
		   yearsRange:[1910,2020],
		   //selectedDate:,
		   //limitToToday:true
		});*/
		$( "#dob_temp" ).datepicker({
		dateFormat: 'dd-mm-yy',
		selectOtherMonths: true,
		changeYear: true,
		yearRange: "1910:2020"
		});
	$('#reportto option[value="1"]').html('Management Cell');
  });
	</script> -->
<!--  start content-table-inner -->
  

	<?php echo $this->Form->create($EmployeeData,array('method'=>'POST','onsubmit' => '',"class"=>"login" ,"type"=>"file")); ?>
  <div class="col-sm-6">
            <div class="white-box">

<?php //echo $this->Html->image($wellington['Beef']['picture'], array('alt' => 'story image'));
	$gmailVal ="";
	$skypeVal ="";
	$dropboxVal ="";  ?>
	<?php echo $this->Flash->render(); $session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
		

		 ?>

		<?php 
			$resultData['password'] = '';
		
				$IDs = []; //pr($resultData); 
	
				(!empty($resultData['gmail'])) ? $IDs[]=$resultData['gmail']:''; 
				(!empty($resultData['skype'])) ? $IDs[]=$resultData['skype']:'';
				(!empty($resultData['dropbox'])) ? $IDs[]=$resultData['dropbox']:'';
				
			 ($IDs)? $resData = $this->General->getCredentialsforUser($IDs): $resData = []; ;
			
			 if(count($resData)>0){
			 
			 foreach($resData as $v2){
			
			 	 if($v2['type']=="gmail"){
                $gmailVal = $v2['username']; 
            }elseif($v2['type']=="skype"){
              $skypeVal = $v2['username']; 
              }elseif($v2['type']=="dropbox"){
              	 $dropboxVal = $v2['username']; 
              	}
    
	
	 }
	
			} else {
				$gmailVal ="";
				 $skypeVal ="";
				$dropboxVal ="";	
			} 
			

	?>
		      <div class="form-group">
                    <label for="exampleInputuname">Employee Code:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div><?php 
				 echo $this->Form->input("employee_code",array("class"=>"form-control","label"=>false,"div"=>false));
                   ?>  </div>
                     <?php if(isset($errors['employee_code'])){
                      echo $this->General->errorHtml($errors['employee_code']);
                      } ;?>
                  </div>
		
		
<div class="form-group">
                    <label for="exampleInputuname">Assign Role:</label>
                    <div class="input-group">
                 <?php
				foreach($this->General->getUserRoles() as $key=>$role){
					$checked = array();
					if(!empty($resultData['role_id']) && in_array($key, $resultData['role_id'])){
						$checked = array('checked'=>true);
					}
					echo $this->Form->input("role_id.$key",array_merge($checked ,array('type'=>'checkbox',"label"=>false,"div"=>false,"value"=>$key,"id"=>'role_'.$key,"hiddenField"=>false)));
					echo "<label for = 'role_".$key."'>".$role."</label><br/>";
				  }
			    ?>
               </div>
            </div>

     

       <div class="form-group">
                       <label for="exampleInputpwd1">Reporting To:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php   $options = $this->General->getuserexceptme($userID);
                       echo $this->Form->select('report_to',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                        <?php if(isset($errors['report_to'])){
                       echo $this->General->errorHtml($errors['report_to']);
                      } ;?>
                  </div>
              <div class="form-group">
                    <label for="exampleInputuname">First Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("first_name",array("type"=>"text","class"=>"form-control",'placeholder'=> "First Name", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['first_name'])){
                      echo $this->General->errorHtml($errors['first_name']);
                      } ;?>
                  </div>


     <div class="form-group">
                    <label for="exampleInputuname">Last Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("last_name",array("type"=>"text","class"=>"form-control",'placeholder'=> "Last Name", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['last_name'])){
                      echo $this->General->errorHtml($errors['last_name']);
                      } ;?>
                  </div>

	



   <div class="form-group">
                    <label for="exampleInputuname">Father's Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("father_name",array("type"=>"text","class"=>"form-control",'placeholder'=> "Father's Name", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['father_name'])){
                      echo $this->General->errorHtml($errors['father_name']);
                      } ;?>
                  </div>



                    <div class="form-group">
                    <label for="exampleInputuname">Mother's Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("mother_name",array("type"=>"text","class"=>"form-control",'placeholder'=> "Mother's Name", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['mother_name'])){
                      echo $this->General->errorHtml($errors['mother_name']);
                      } ;?>
                  </div>

       


              <div class="form-group">
                       <label for="exampleInputpwd1">Designation:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php 
        $designation = (isset($employee['MyAppraisals']['new_designaiton'])) ? $employee['MyAppraisals']['new_designaiton'] : "None";
        $options = $this->General->getalldesignations($userSession[3]);
                       echo $this->Form->select('designation',[" "=>$options],['class' => 'form-control',"value"=>$designation,] );?>
                       </div>
                       <?php if(isset($errors['designation'])){
                                    echo $this->General->errorHtml($errors['designation']);
                                } ;?>
                  </div>

  <div class="form-group">
                    <label for="exampleInputuname">Dropbox:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("dropbox",array("type"=>"text","class"=>"form-control",'placeholder'=> "Dropbox", "label"=>false,"div"=>false,"value"=>$dropboxVal,"readonly"=>true));?>
                        </div>
                     <?php if(isset($errors['dropbox'])){
                      echo $this->General->errorHtml($errors['dropbox']);
                      } ;?>

                      <?php
            // newer
             echo  $this->Html->link("Assign Dropbox",
                    array('controller'=>'credentials','action'=>'freecredits','prefix'=>'credentials','key'=> 'dropbox'),
                    array('style'=>'color:red','class'=>'fancybox popup_window')
                    );  
            ?>

                  </div>


<div class="form-group">
                    <label for="exampleInputuname">Gmail:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("gmail",array("type"=>"text","class"=>"form-control",'placeholder'=> "Gmail", "label"=>false,"div"=>false,"value"=>$gmailVal,"readonly"=>true));?>
                        </div>
                     <?php if(isset($errors['gmail'])){
                      echo $this->General->errorHtml($errors['gmail']);
                      } ;?>

                       <?php echo $this->Html->link("Assign Gmail",
                    array('controller'=>'credentials','action'=>'freecredits','prefix'=>'credentials','key'=> 'gmail'),
                    array('style'=>'color:red','class'=>'fancybox popup_window')
                    );  
            ?>

                  </div>

                   <div class="form-group">
                    <label for="exampleInputuname">Skype:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("skype",array("type"=>"text","class"=>"form-control",'placeholder'=> "Skype", "label"=>false,"div"=>false,"value"=>$skypeVal,"readonly"=>true));?>
                        </div>
                     <?php if(isset($errors['skype'])){
                      echo $this->General->errorHtml($errors['skype']);
                      } ;?>


                   <?php echo $this->Html->link("Assign Skype",
                    array('controller'=>'credentials','action'=>'freecredits','prefix'=>'credentials','key'=> 'skype'),
                    array('style'=>'color:red','class'=>'fancybox popup_window ')
                    );  
                   ?>

                  </div>

 <div class="form-group">
                    <label for="exampleInputuname">Seat Number:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("seat_number",array("type"=>"text","class"=>"form-control",'placeholder'=> "Seat Number", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['seat_number'])){
                      echo $this->General->errorHtml($errors['seat_number']);
                      } ;?>
                  </div>
 <div class="form-group">
                    <label for="exampleInputuname">Experience:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("experience",array("type"=>"text","class"=>"form-control",'placeholder'=> "Experience", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['experience'])){
                      echo $this->General->errorHtml($errors['experience']);
                      } ;?>
                  </div>

         <div class="form-group"> 
               <label for="inputName" class="control-label">Previous Employment:</label>
             <?php
             echo $this->Form->input("previous_employment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"Enter Previous Employment."));
                     if(isset($errors['previous_employment'])){
                                  echo $this->General->errorHtml($errors['previous_employment']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
                </div>
	


        <div class="form-group">
                    <label for="exampleInputuname">Date of Joining:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
            <?php 
           
           			 $val1 = (!empty($EmployeeData['doj'])?date('d-m-Y',strtotime($EmployeeData['doj'])):"");	

            ?>
                      <?php echo $this->Form->input("doj_temp",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Date of Joining", "label"=>false,"div"=>false,"id"=>"next_review_date1","value"=>$val1,"id"=>"doj_temp"));?>
                       </div>
                  </div>


   <div class="form-group">
                    <label for="exampleInputuname">Date of Relieving:</label>
                    <div class="input-group">
                       <?php 
				$val = (!empty($resultData['dor'])?date('d-m-Y',strtotime($resultData['dor'])):"");
                    ?>
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("dor_temp",array("type"=>"text","class"=>"form-control attendancedate",'placeholder'=> "Date of Relieving", "label"=>false,"div"=>false,"id"=>"dor_temp","value"=>$val));?>
                       </div>
                   
                  </div>

<h2 class="text-primary">Login Information</h2>     

              <div class="form-group">
                    <label for="exampleInputuname">Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("username",array("type"=>"text","class"=>"form-control inp-form",'placeholder'=> "Email", "label"=>false,"div"=>false,"id"=>"webmail"));?>
                        </div>
                     <?php if(isset($errors['username'])){
                      echo $this->General->errorHtml($errors['username']);
                      } ;?>

                      <?php echo $this->Html->link("Assign Webmail",
                    array('controller'=>'credentials','action'=>'freecredits','prefix'=>'credentials','key'=> 'webmail'),
                    array('style'=>'color:red','class'=>'fancybox popup_window')
                    );  
                  ?>

                  </div>
                    <div class="form-group">
                    <label for="exampleInputuname">Password:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php 
                       echo $this->Form->input("password",array("type"=>"text","class"=>"form-control pwd","id"=>"crpass",'placeholder'=> "Password", "label"=>false,"div"=>false,"value"=>""));?>
                        </div>
                     <?php if(isset($errors['password'])){
                      echo $this->General->errorHtml($errors['password']);
                      } ;?>


                  </div> 

<h2 class="text-primary">Contact Information</h2>  

      <div class="form-group">
                    <label for="exampleInputuname">Phone:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("phone",array("type"=>"text","class"=>"form-control",'placeholder'=> "Phone", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['phone'])){
                      echo $this->General->errorHtml($errors['phone']);
                      } ;?>
                  </div> 


         <div class="form-group"> 
               <label for="inputName" class="control-label">Address:</label>
             <?php
             echo $this->Form->input("full_address",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"Enter Address."));
                     if(isset($errors['full_address'])){
                                  echo $this->General->errorHtml($errors['full_address']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
                </div>

         <div class="form-group">
                    <label for="exampleInputuname">Personal Email:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("personal_email",array("type"=>"text","class"=>"form-control",'placeholder'=> "Personal Email", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['personal_email'])){
                      echo $this->General->errorHtml($errors['personal_email']);
                      } ;?>
                  </div> 




<h2 class="text-primary">Personal Information</h2>  

 <div class="form-group">
                    <label for="exampleInputuname">Place of birth:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("place_of_birth",array("type"=>"text","class"=>"form-control",'placeholder'=> "Place of birth", "label"=>false,"div"=>false));?>
                        </div>
                    
                  </div> 


 <div class="form-group">
                    <label for="exampleInputuname">DOB:</label>
                    <div class="input-group">
           <?php

			 if(isset($resultData['dob']) && $resultData['dob']!="") {
				$val2 = $resultData['dob']->format('d-m-Y');
			}else {
				$val2 = "";
			}
			?>
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("dob_temp",array("type"=>"text","class"=>"form-control",'placeholder'=> "Date of Birth", "label"=>false,"div"=>false,"value"=>$val2,"id"=>"dob_temp"));?>
                        </div>
                     <?php if(isset($errors['dob'])){
                      echo $this->General->errorHtml($errors['dob']);
                      } ;?>
                  </div> 
                       <div class="form-group">
                       <label for="exampleInputpwd1">Gender:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php $options=array('Male'    =>  'Male', 
                                'Female'    => 'Female'
                             );
                       echo $this->Form->select('sex',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                       <?php if(isset($errors['sex'])){
                                    echo $this->General->errorHtml($errors['sex']);
                                } ;?>
                  </div>



		<div class="form-group">
                       <label for="exampleInputpwd1">Urban/Rural:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php $options=array('urban'    =>  'Urban', 
                      'rural'    => 'Rural'
                      );
                       echo $this->Form->select('urban_rural',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                       <?php if(isset($errors['urban_rural'])){
                                    echo $this->General->errorHtml($errors['urban_rural']);
                                } ;?>
                  </div>


              <div class="form-group">
                       <label for="exampleInputpwd1">Martial Status:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php    $options=array('single'    =>  'Single', 
                      'married'    => 'Married',
                       'divorced'    => 'Divorced'
                      );
                       echo $this->Form->select('marital_status',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                       <?php if(isset($errors['marital_status'])){
                                    echo $this->General->errorHtml($errors['marital_status']);
                                } ;?>
                  </div>




<div class="form-group">
                    <label for="exampleInputuname">Caste:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("caste",array("type"=>"text","class"=>"form-control",'placeholder'=> "Caste", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['caste'])){
                      echo $this->General->errorHtml($errors['caste']);
                      } ;?>
                  </div> 
<div class="form-group">
                    <label for="exampleInputuname">Religion:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("religion",array("type"=>"text","class"=>"form-control",'placeholder'=> "Religion", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['religion'])){
                      echo $this->General->errorHtml($errors['religion']);
                      } ;?>
                  </div> 

     <div class="form-group"> 
               <label for="inputName" class="control-label">Language Known:</label>
             <?php
             echo $this->Form->input("language_known",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter here.."));
                     if(isset($errors['language_known'])){
                                  echo $this->General->errorHtml($errors['language_known']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
                </div>
	
<div class="form-group"> 
               <label for="inputName" class="control-label">Qualification:</label>
             <?php
             echo $this->Form->input("qualification",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter here.."));
                     if(isset($errors['qualification'])){
                                  echo $this->General->errorHtml($errors['qualification']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
                </div>
       <div class="form-group"> 
               <label for="inputName" class="control-label">Skills:</label>
             <?php
             echo $this->Form->input("skills",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter here.."));
                                         ?>
                </div>


         <div class="form-group"> 
               <label for="inputName" class="control-label">Responsibilities:</label>
             <?php
             echo $this->Form->input("responsibility",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter here.."));
                    
                  
                      ?>
                </div>


<h2 class="text-primary">Bank Account Information</h2>  

<div class="form-group">
                    <label for="exampleInputuname">Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("bankName",array("type"=>"text","class"=>"form-control",'placeholder'=> "BankName", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['accountName'])){
                      echo $this->General->errorHtml($errors['accountName']);
                      } ;?>
                  </div>


		   <div class="form-group">
                    <label for="exampleInputuname">PAN Number:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("pan",array("type"=>"text","class"=>"form-control",'placeholder'=> "PAN Number", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['pan'])){
                      echo $this->General->errorHtml($errors['pan']);
                      } ;?>
                  </div>
	<div class="form-group">
                    <label for="exampleInputuname">Account Number:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("account_number",array("type"=>"text","class"=>"form-control",'placeholder'=> "Account Number", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['account_number'])){
                      echo $this->General->errorHtml($errors['account_number']);
                      } ;?>
                  </div>

<div class="form-group">
                    <label for="exampleInputuname">Bank Name:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("bankName",array("type"=>"text","class"=>"form-control",'placeholder'=> "Account Number", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['bankName'])){
                      echo $this->General->errorHtml($errors['bankName']);
                      } ;?>
                  </div>


    <div class="form-group"> 
               <label for="inputName" class="control-label">Branch Address:</label>
             <?php
             echo $this->Form->input("branch",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","placeholder"=>"enter here .."));
                     
                      ?>
                </div>


<div class="form-group">
                    <label for="exampleInputuname">IFSC:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php echo $this->Form->input("ifsc",array("type"=>"text","class"=>"form-control",'placeholder'=> "Account Number", "label"=>false,"div"=>false));?>
                        </div>
                     <?php if(isset($errors['ifsc'])){
                      echo $this->General->errorHtml($errors['ifsc']);
                      } ;?>
                  </div>


		     <div class="form-group">
                    <label for="exampleInputuname">Current Salary:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <?php 
                  $UserID = $resultData['id'];
				$resultData = $this->General->getInfoFromAppraisal($UserID);
				//pr($resultData);die;
				$salary = (isset($resultData['appraised_amount'])) ? $resultData['appraised_amount'] : "0";
		
                      echo $this->Form->input("current_salary",array("type"=>"text","class"=>"form-control",'placeholder'=> "Current Salary", "label"=>false,"div"=>false,"value"=>$salary,"readonly"=>true));?>
                 </div>
                  
                  </div>

	
<h2 class="text-primary">Other Information</h2>  


    <div class="form-group"> 
               <label for="inputName" class="control-label">Notes:</label>
             <?php
             echo $this->Form->input("notes",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter  here .."));
                
                      ?>
                </div>


    <div class="form-group"> 
               <label for="inputName" class="control-label">Documents:</label>
             <?php
             echo $this->Form->input("document",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter here .."));
                     
                      ?>
                </div>





<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  //echo $this->Html->link("Reset",  ['controller'=>'employees','action'=>'add','prefix' => 'employees'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
//$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/employees/add', true)));?>

 </div>
 </div>
</div>


 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading"></h4>
                    <ul >
                 <li> <?php // echo $this->Html->link("Go To Listing",array('controller'=>'appraisals','action'=>'appraisalslist'), array('style'=>'color:red;'));?>
            </li> 
         <?php
			if($title_for_layout == "Add Employee"){
				echo $this->element('user_sidebar', array('data'=>array('mode'=>'add')));
			}else{
				$ele = array('id'=>$id,'mode'=>'edit','type'=>'employees');
				echo $this->element('user_sidebar', array('data'=>$ele));
				//echo $this->element('document_sidebar', array('data'=>$ele));
			}
			
			?>

                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>

      <!-- /.right-sidebar -->


</form>
</body>
</html>
