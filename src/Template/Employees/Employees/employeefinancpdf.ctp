<?php
//App::import('Vendor','xtcpdf');  
//App::import('Vendor','xtcpdf');  
//require_once('/var/www/html/erp/plugins/tcpdf/src/TCPDF/src/examples/tcpdf_include.php');

// create new PDF document
//$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
mb_internal_encoding('UTF-8');


$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

$pdf->AddPage();
       // set JPEG quality
$pdf->setJPEGQuality(75);
$htmltable1 = '<table cellpadding="1" cellspacing="1" border="0" style="text-align:center;margin-left:180px;">
<tr><td align="center"><h4 >Employee Financial Report </h4></td></tr>
<tr><td height="5px"></td></tr>
</table>'; 
 // set core font
$pdf->SetFont('helvetica', '', 9); 
// output the HTML content
$pdf->writeHTML($htmltable1, true, 0, true, true);


$htmltable4 = '<br><table align="left" border="1" cellspacing="0" cellpadding="4" >' ;
$htmltable4  .=	'<tr bgcolor="#4d9ffa">
<td align="center" style="color:black ; font-weight:bold;">Name</td>
<td align="center" style="color:black ; font-weight:bold;">PAN No.</td>
<td align="center" style="color:black;width:100px;font-weight:bold;">Account No.</td>
<td align="center" style="color:black ; font-weight:bold;">Bank Name</td>
<td align="center" style="color:black ; font-weight:bold;">Bank Address</td>
<td align="center" style="color:black ; font-weight:bold;">IFSC</td>
<td align="center" style="color:black ; font-weight:bold;">Current Salary</td>
</tr>' ;

$bgclr  = '';
foreach($crData as $val) {
	//pr($crData);die();


$htmltable4  .=	'<tr bgcolor="'. $bgclr .'">
<td align="left" style="color:black ; font-weight:bold;">'.$val['first_name']." ".$val['last_name'].'</td>
<td align="left" style="color:black ; font-weight:bold;">'. $val['pan'].'</td>
<td align="left" style="color:black;width:100px;  font-weight:bold;">'. $val['account_number'].'</td>
<td align="left" style="color:black ; font-weight:bold;">'. $val['bankName'].'</td>
<td align="left" style="color:black ; font-weight:bold;">'. $val['branch'].'</td>
<td align="left" style="color:black ; font-weight:bold;">'. $val['ifsc'].'</td>
<td align="left" style="color:black ; font-weight:bold;">'. $val['appraised_amount'].'</td>
</tr>' ;
}

$htmltable4 .='</table>';
                                                 
// set core font
$pdf->SetFont('helvetica', '', 9);
// output the HTML content
$pdf->writeHTML($htmltable4, true, 0, true, true);

// reset pointer to the last page
$pdf->lastPage();


//Close and output PDF document
$pdf->Output('Employee Financial Report.pdf', 'D');
?>