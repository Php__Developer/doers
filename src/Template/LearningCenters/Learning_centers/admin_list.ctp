<script type="text/javascript">
	$(document).ready(function() 
	{
		//fancybox settings
		$(".view_template").fancybox({
				//'titlePosition'	: 'outside',
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	960, 
				'height' :	1000, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
	});
	function submit_myform(){
		$("#mainform").submit();
	}
</script>

<?php
$newUrl = "list".$urlString;
$urlArray = array(
	'field' 	=> $search1,
	'value' 	=> $search2
);
$paginator->options(array('url'=>$urlArray));
?>
<?php echo $form->create('LearningCenter',array('action'=>$newUrl,'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
			<table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
		<tr>
			<td width="14%">
				<b>Search by:</b>
				<?php
				$fieldsArray = array(
				''				          => 'Select',
				'LearningCenter.title'     => 'Title',
				'LearningCenter.content'     => 'Content'
				
				);
				echo $form->select("LearningCenter.fieldName",$fieldsArray,$search1,array("id"=>"searchBy","label"=>false,"style"=>"width:200px","class"=>"styledselect","empty"=>false),false); ?>
			</td>
			<td width="20%">
				<b>Search value:</b><br/>
				<?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "LearningCenter.status"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $form->input("LearningCenter.value1",array("id"=>"search_input","class"=>"top-search-inp","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
				?>
			</td>
			<td width="40%"><br/>
		  		<?php
				echo $form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
				echo $form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/learning_centers/list'"));				
				?>
			</td>
		</tr>
	</table>
		<br/>
				<table border="0" widthicon-5 info-tooltip view_template="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-check"><input  type="checkbox" id="toggle-all"> </th>
					<th class="table-header-repeat line-left minwidth-1" ><?php echo $paginator->sort('Title', 'LearningCenter.title');?> </th>
					<th class="table-header-repeat line-left minwidth-1"  width="50%"><?php echo $paginator->sort('Content', 'LearningCenter.content');?></th>
					
					<th class="table-header-repeat line-left" width="22%"><?php echo $paginator->sort('Skill', 'LearningCenter.skill');?></th>
					<th class="table-header-options line-left" width="12%"><a href="#A">Options</a></th>
				</tr>
				<?php if(count($resultData)>0){
			$i = 1; 
			foreach($resultData as $result):
			if(!$result['LearningCenter']['status']%2){
			$class = "alternate-row";
			}elseif($result['LearningCenter']['status']==2){
				$class = "alternatependingblog";
			}else{
			$class = ""; } ?>
				<tr class="<?php echo $class; ?>">
					<td><input  type="checkbox" name="IDs[]" value="<?php echo $result['LearningCenter']['id'];?>"/></td>
					 <td>
					 <?php
						$nameByUser = "Contributed By: ".$result['User']['first_name']." ".$result['User']['last_name'];
						echo $html->link($result['LearningCenter']['title'],
						array('controller'=>'learning_centers','action'=>'view',$result['LearningCenter']['id']),
						array('class'=>'view_template','title'=>$nameByUser)
					   );
						?>
					<td class="type-view">
					<?php  $str = strip_tags($result['LearningCenter']['content']);
					echo substr($str,0,100).((strlen($str)>100)? "...":"");
					?>
					</td>
					<td><?php
						$options=$general->getSkills();
						echo $options[$result['LearningCenter']['skill']];
					?>
					</td>
					<td class="options-width" align="center">
						<?php
						echo $html->link("",
						array('controller'=>'learning_centers','action'=>'view',$result['LearningCenter']['id']),
						array('class'=>'icon-5 view_template','title'=>$result['LearningCenter']['title'],'alt'=>'view')
					   );
						?>
						
						<?php
						echo $html->link("",
						array('controller'=>'learning_centers','action'=>'edit',$result['LearningCenter']['id']),
						array('class'=>'icon-1 info-tooltip','title'=>'Edit')
				   	);
						?>
						<?php
						echo $html->link("",
						array('controller'=>'learning_centers','action'=>'delete',$result['LearningCenter']['id']),
						array('class'=>'icon-2 info-tooltip delete','title'=>'Delete')
					   );
						?>
					</td>
				</tr>
				<?php $i++ ;
				endforeach; ?>
				<?php } else { ?>
		<tr>
			<td colspan="10" class="no_records_found">No records found</td>
		</tr>
			<?php } ?>
				</table>
				<!--  end product-table................................... --> 
			</div>
			<!--  end content-table  -->
		
			<!--  start actions-box ............................................... -->
			<div id="actions-box">
				<div style="float:left">
				<a href="" class="action-slider"></a>
				<div id="actions-box-slider" >
				 
				 <?php echo $form->submit("Activate",array("div"=>false,"class"=>"action-activate","name"=>"publish","value"=>"publish",'onclick' => "return atleastOneChecked('Activate selected records?','learning_centers/changeStatus');")); ?>
				 <?php echo $form->submit("Deactivate",array("div"=>false,"class"=>"action-deactivate","name"=>"unpublish","value"=>"unpublish",'onclick' => "return atleastOneChecked('Deactivate selected records?','learning_centers/changeStatus'); ")); ?>
				 
				</div>
				</div>
				<!--<div id="listpage_users">
				 <?php
				  $options=$general->getuser(); 
				  echo $form->input("user_id",array( "type"=>'select',"class"=>"ourselect",'options'=>$options,"label"=>false,"div"=>false,"onchange"=>"if(atleastOneChecked('Assign permissions for pages.','learning_centers/setPermission')) submit_myform() ; " ));
				 ?>
				</div>-->
				<div class="clear"></div>
			</div>
			<!-- end actions-box........... -->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
			<td>
				<?php echo $paginator->prev('� Previous', array('class' => 'page-left'), null, array('class' => 'page-left')); ?>
				<div id="page-info"><?php echo $this->Paginator->counter(array('format' => ' Page<strong> %page%</strong> / %pages%',"id"=>"page-info")); ?></div>
				<?php echo $paginator->next('Next �', array('class' => 'page-right'), null, array('class' => 'page-right')); ?>
			</td>
			</tr>
			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>

	</td>
	<td>

	<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Manage Knowledge Base</h5>
          This section is used by Admin only to manage senstive Information.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Add New Template",
            array('controller'=>'learning_centers','action'=>'add')
            );	
            ?>
            </li> 
  			</ul>
				</div>
					<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
		</div>
		<!-- end related-act-bottom -->
	</div>
	<!-- end related-activities -->

</td>
</tr>
</table>
 </div>
<?php echo $form->end(); ?>
<!--  end content-table-inner  -->