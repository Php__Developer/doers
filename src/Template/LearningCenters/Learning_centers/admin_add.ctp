<?php echo $javascript->link('ckeditor/ckeditor.js'); ?>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('LearningCenter',array('action'=>'add','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top"> Skill: </th>
			<td>
			<?php
				$options=$general->getSkills();
				echo $form->input('skill', array("type"=>'select',"class"=>"ourselect",'options' => $options,"label"=>false,"div"=>false)); 
            ?>	
			</td>
		</tr>
		<tr>
			<th valign="top">Title:</th>
			<td><?php
				 echo $form->input("title",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
				<tr>
			<th valign="top">Content</th>
			<td>
			<?php //pr($Error); die;
				 echo $form->textarea("content",array("class"=>"inp-form","label"=>false,"div"=>false));
				echo (!empty($Error['content'])?"<div class='error-message '>".$Error['content']."</div>":''); 
            ?>
			
			</td>
			<script>
			CKEDITOR.replace('data[LearningCenter][content]', { "width": "750" }); 
			</script></tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			
			<?php 
			echo $form->hidden("id");
			echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/learning_centers/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Instructions:</h5>
						- General standard is Verdana font with font size 11.</br/> 
						- For Images / Video use direct website link or embed url.</br/> 
						- Format it properly before submitting to the Admin.</br/> 
						- Make sure same knowledge has been never shared by anyone in Learning Center.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Go To Listing",
            array('controller'=>'learning_centers','action'=>'list')
            );	
            ?>
            </li> 
						
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
	