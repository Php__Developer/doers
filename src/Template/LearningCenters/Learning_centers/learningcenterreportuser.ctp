<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->css(array("screen")); ?>
<?php echo $this->Html->script('highcharts'); ?>	
<?php  echo $this->Html->script('modules/exporting');?>

<script type="text/javascript">
var base_url = '<?php  echo $this->Url->build('/', true); ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			  
		 });
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
		 });
	});
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
		
	}
	/*
  *---------------Java script code for date calculation----------------*
  */
  function date_calculation()
  {
  var selectedType=document.getElementById("selectdate");
  var selecteddate = selectedType.options[selectedType.selectedIndex].value;
  var today=new Date();
  var today2=today.toISOString().substr(0,10);
  if(selecteddate=="month")
  {
   var monthsum= new Date(new Date(today).setMonth(today.getMonth()-1));
   var monthsum2=monthsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=monthsum2;
   document.getElementById("enddate").value=today2;
   
  }
  else if(selecteddate=="year")
  {
   var yearsum= new Date(new Date(today).setMonth(today.getMonth()-12));
   var yearsum2=yearsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=yearsum2;
   document.getElementById("enddate").value=today2;
  }
   else if(selecteddate=="week")
  {
   var yearsum= new Date(new Date(today).setDate(today.getDate()-6));
   var yearsum2=yearsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=yearsum2;
   document.getElementById("enddate").value=today2;
  }
  else if(selecteddate=="today")
  {
   document.getElementById("startdate").value=today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="select")
  {
   document.getElementById("startdate").value =today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="custom")
  {
   document.getElementById("startdate").value ='';
   document.getElementById("enddate").value ='';
  }
  }
  //------------------------------------------Implementing chart---------------------------------------//
	$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Learning Center Report (User Wise)'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color:'black'
						//color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
//---------------------------Fetching data for chart------------------------------//
		<?php





		$result=array();
				
		foreach($usercount as $record)
		{	
			pr($record); die;
		foreach($record as $records)
			{
		foreach($records as $key)
					{
			$result[]=$key;
				
				}
			}
		}
$resultname=array();
$resultvalue=array();
	for($i=0;$i<count($result);$i++)
		{
		if($i==0 || $i%2==0)
			{
			$resultname[]=$result[$i];
			}
		else
			{
			$resultvalue[]=$result[$i];
			}
		}	
?>
        series: [{
            type: 'pie',
            name: 'Active',
			data: [										//passing data here
			<?php for($i=0;$i<count($resultname);$i++){ ?>
                [<?php echo "'$resultvalue[$i]'";?>,<?php echo "$resultname[$i]"; ?>],
			<?php }	?>
            ] 
        }]
    });
});
</script>
<!--  start content-table-inner -->
<h1 align="center">Learning Center Report (User Wise)</h1>
<div id="content-table-inner">

	

<?php echo $this->Form->create('LearningCenter',array('url' => ['action'=>'learningcenterreportuser'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm()')); ?>




<div id="content-table-inner">
<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
				<tr>
				<td width="15%">
		<div style="float:left;">
			 <?php

 $options = $this->General->getDateRange();
 
 echo $this->Form->input("select",array("type"=>"select","class"=>"top-search-inp","options"=>$options,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"selectdate",'onChange'=>'date_calculation();'));
 ?>
  </td>
	<th align="left" ><b>From:&nbsp;</b></th>
	<td>
		<div style="float:left;">
		<?php 
  if($fromDate!=""){
  $st_date=date('Y/m/d',strtotime($fromDate));
  }else{
   $st_date=date('Y/m/d');
  }
  if($toDate!=""){
  $todate=date('Y/m/d',strtotime($toDate));
  }else{
  $todate=date('Y/m/d');
  }
  ?>



<?php 
	

 echo $this->Form->input("start_date",array("type"=>"text","id"=>"startdate","class"=>"top-search-inp","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($st_date)))); ?>	 </td>
<th align="left" ><b>To:&nbsp;</b></th>
<td width="30%">
<?php  echo $this->Form->input("end_date",array("type"=>"text","id"=>"enddate","class"=>"top-search-inp","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($todate)))); ?>	 </td>
	</div>
<div style="clear:both"></div>
	</div>
	<th><b>User:&nbsp;</b></th>
	<td width="30%">
				<div style="float:left;">
				<div style="float:left;">
				<?php
					$options =$this->General->getUser(); 
					echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"top-search-inp","id"=>"usersid",'options'=>$options,"label"=>false,"div"=>false,"style"=>"width:150px;"));		
					?>
				</div><div style="clear:both"></div>
			</div>
		</td>
	<br/>
<td width="40%">
<div style="">
	<?php echo $this->Form->submit("Submit ", array('class'=>'form-search','id'=>'search','method'=>'POST',"id" => "mainform3","name" => "listForm3"));  ?>
	</div>
			</td>
		</tr>
	</table>
<br/>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
	<div class ="lead_usr">
	<div style="clear:both"></div>
</div></div>
	<?php echo $this->Form->end(); 


	?>

<?php if(isset($resultData) && count($resultData)) {

 ?>		

<!--Inserting graph in form here-->	
	<div class="contentbox">
<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</table>
<!--  end Graph-................................... --> 
<div id="table-content">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
<thead>
<tr>
			<th class="heading1"><b>User</b></th>
			<th class="heading1"><b>Topic</b></th>
			<th class="heading1"><b>Content</b></th>
			<th class="heading1"><b>Date</b></th>
	</tr>
</thead>
		<?php 
		foreach($resultData as $record)
		{ 
		 $dateWithTime=$record['created'];
		$date=substr("$dateWithTime",0,-8);
		 ?>
		 <tbody>
		<tr class>
		<td class="wrapping"><?php echo $record[0]['first_name'];  ?></td>
		<td class="wrapping"><?php echo $record['LearningCenter']['title'];?></td>
		<?php ?>
		<?php if (strlen($record['LearningCenter']['content'])>250){ ?>  
		<td class="wrapping"><?php echo substr($record['LearningCenter']['content'],0,250)."......";?> <?php echo $html->link("View More",
								array('controller'=>'learning_centers','action'=>'reportmore',$record['LearningCenter']['id'],'detail'),
								array('class'=>'info-tooltip viewPaydetails ','title'=>'Details')
							); ?>
							<?php } else { ?></td>
		<td class="wrapping"><?php echo $record['LearningCenter']['content'];  ?>
							<?php } ?>
			</td>
		<td class="wrapping" nowrap><?php echo $date;  ?></td>
</td>
</tr>
</tbody>
		<?php }?>
<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">			
	

			<div class="clear"></div>
<?php		}
		else {


		?>

			<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<tbody>
					<tr>
					<td colspan="13" class="no_records_found">No records found</td>
	
	</tr>
		<?php } ?>
				</tbody>
	<div class="clear"></div>
</div>
		 	<?php 

die;
?>