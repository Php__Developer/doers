<?php echo $html->css(array("screen")); ?>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
			<center><span style="font-weight:bold;">Content</span></center>
				<table style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
					
				<?php if(count($resultData)>0){
				foreach($resultData as $result): ?>
				<tr>
					<td><?php echo $result['LearningCenter']['content'];  ?></td>
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				 <?php echo $html->link("Goto Listing",
								array('controller'=>'learning_centers','action'=>'learningcenterreport_user'),
								array('class'=>'info-tooltip viewPaydetails ','title'=>'Goto Listing')
							); ?><!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->