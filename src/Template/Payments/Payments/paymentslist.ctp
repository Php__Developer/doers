<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php //echo $this->Html->css(array("oribe")); ?>
<input id="pe" class="pe hidden" value="<?php echo $pagename;?>">
<script type="text/javascript">
    $(document).ready(function() { 
      $( "#payment_startdate" ).datepicker({
        dateFormat: 'MM dd, yy',
      });
      $( "#payment_enddate" ).datepicker({
        dateFormat: 'MM dd, yy',
      });

     
  });
  </script>
<script>
function validateForm(){
		if($('#payment_enddate').val()!="" && $('#payment_startdate').val()=="" || $('#payment_enddate').val()=="" && $('#payment_startdate').val()!=""){
			$ (".amountdates").html("");
			$(".amountdates").append('Enter both start and end date');
			$(".amountdates").show();
			return false;
		}else if($('#payment_enddate').val()!="" && $('#payment_startdate').val()!=""){
			var str_date = changeDate($('#payment_startdate').val());
			var end_date = changeDate($('#payment_enddate').val());
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				$ (".amountdates").html("");
				$(".amountdates").append('Start date should be less than end date');
				$(".amountdates").show();
				return false;
			}
		}
		if($('#start_amount').val()!="" && $('#last_amount').val()=="" || $('#start_amount').val()=="" && $('#last_amount').val()!=""){
				$ (".contamount").html("");
				$(".contamount").append('Enter both min and max amount');
				$(".contamount").show();
				return false;
		}else if(($('#start_amount').val()!="" && $('#last_amount').val()!="")){
			if (parseFloat($('#start_amount').val()) > parseFloat($('#last_amount').val())) {
				$ (".contamount").html("");
				$(".contamount").append('Min amount must be less than max amount');
				$(".contamount").show();
				return false;
			}
		}
		if($('#tds_stramount').val()!="" && $('#tds_lastamount').val()=="" || $('#tds_stramount').val()=="" && $('#tds_lastamount').val()!=""){
				$ (".tdsamount").html("");
				$(".tdsamount").append('Enter both min and max TDS amount');
				$(".tdsamount").show();
				return false;
		}else if($('#tds_stramount').val()!="" && $('#tds_lastamount').val()!=""){
			if (parseFloat($('#tds_stramount').val()) > parseFloat($('#tds_lastamount').val())) {
				$ (".tdsamount").html("");
				$(".tdsamount").append('Min amount must be less than max amount');
				$(".tdsamount").show();
			}
		}
	}
	function changeDate(date){
		date = date.split("-");
		new_date = date[1]+ "/"+date[0]+"/"+date[2];
		new_date = new Date(new_date);
		return new_date;
	}
</script>
<style>
.dropdown-menu.animated.drp_list {
    margin-left: 6px;
    z-index:2;
}
.btn.btn-info.btn-circle {

    margin-top: 21px;
}
</style>

<?php echo $this->Form->create('Payment',array('url' => ['action'=>'paymentslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform","onsubmit"=>"return validateForm();")); ?>
<div class="row el-element-overlay m-b-40">
<span class="pull-right text-success"> <?= $this->Paginator->counter([
    'format' => ' {{page}} of {{pages}} Pages'
]) ?></span>

      <div class="row">
      <div class="col-md-3">
             <b>Project Name</b>
            <?php  echo $this->form->input("project_name",array("type"=>"text","id"=>"url","class"=>"form-control","style"=>"width:229px;height:32px","div"=>false,"label"=> false)); ?>
        
            </div>
                <div class="col-md-3">
            <b> Notes:</b>
          <?php  echo $this->form->input("note",array("type"=>"text","id"=>"notes","class"=>"form-control","style"=>"width:229px;height:32px","div"=>false,"label"=> false)); ?>

       
            </div>
              <div class="col-md-2">
               <b>Branch:</b>
              					<?php echo $this->Form->input("Branch",array('options'=> array('cdg'=>'Chandigarh','lko'=>'Lucknow'),'empty'=>'select', "class"=>"form-control","style"=>"width:100px;","div"=>false, "label"=> false)); 	?>

    
           
            </div>
     <div class="col-md-2">
      <b class="unk"></b>
            <?php 
      echo $this->Html->link('<i class="fa  fa-plus"></i>',
         array('controller'=>'payments','action'=>'add','prefix' => 'payments'),
          ['escape' => false,"class"=>"btn btn-info btn-circle ","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Payment"]
          );
          ?>
        
            </div>
            <div class="col-md-2 m-t-0">
              <ul class="pagination paginate-data">
             <li class="previousdata">

                <?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
            </li>
            <li>
                <?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
            </li>
                </ul>

            </div>
        </div>




        <div class="row">
          <div class="col-md-4">
            <div class="col-md-6">
              <b>Min Amount:</b>

              <?php echo $this->Form->input("start_amount",array("id"=>"start_amount","class"=>"form-control","style"=>"", "div"=>false, "label"=> false)); ?>
            </div>
            <div class="col-md-6">
              <b>Max Amount:</b>
              <?php echo $this->Form->input("last_amount",array("id"=>"last_amount","class"=>"form-control","style"=>"", "div"=>false, "label"=> false)); ?>
            </div>
             
          </div>
  
          <div class="col-md-4">
            <div class="col-md-6">
              <b>From Date:</b>
              <?php  echo $this->Form->input("sdate",array("type"=>"text","id"=>"payment_startdate","class"=>"form-control","style"=>"","div"=>false,"label"=> false,"readonly"=>"readonly")); ?></div>
              <div class="col-md-6">
                <b>To Date:</b>
                <?php  echo $this->Form->input("edate",array("type"=>"text","id"=>"payment_enddate","class"=>"form-control","style"=>"","div"=>false,"label"=> false,"readonly"=>"readonly")); ?>
              </div>
            </div>

               <div class="col-md-4">
            <div class="col-md-6">
              <b> TDS Min Amount:</b>
              <?php  echo $this->Form->input("tds_stramount",array("type"=>"text","id"=>"tds_stramount","class"=>"form-control","style"=>"","div"=>false,"label"=> false)); ?></div>
              <div class="col-md-6">
                <b>TDS Max Amount:</b>
                <?php  echo $this->Form->input("tds_lastamount",array("type"=>"text","id"=>"tds_lastamount","class"=>"form-control","style"=>"","div"=>false,"label"=> false)); ?>
              </div>
            </div>
          </div>
     
<span class="m-r-40  text-danger contamount"></span>
<span class="m-r-40 text-danger amountdates"></span>     
<span class="m-r-40 pull-right text-danger tdsamount"></span>  
<br>
  <div class="row">
          <div class="col-md-6">
              <div class="col-md-3">
          <button class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" aria-expanded="false" data-toggle="dropdown" type="button">
              Sort By
              <span class="caret"></span>
          </button>
          <ul class="dropdown-menu animated drp_list" role="menu">
            <li>
              <a href="#" class="1">Name</a>
          </li>  
          <li>
              <a href="#" class="2">Amount</a>
          </li>

          <li>
              <a href="#" class="3">TDS Amount</a>
          </li>
          <li>
              <a href="#" class="4">Branch</a>
          </li>

          <li>
              <a href="#" class="5">Payment Date</a>
          </li>

      </ul>
                  </div>
          </div>
          <div class="col-md-6">

          <?php
            echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
            echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
            $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/payments/paymentslist', true)));
            if(count($completedata) > 0){
              $total = 0;
              foreach($completedata as $data){
                $total += $data['amount'];
              }
            }else{
            $total = 0;
            }

            ?> 
          </div>
          <a href="#" class="pull-right m-r-40">Total Amount : <?php echo $total;?></a>
   </div>

<br>

<!-- <div class="row">
  <div class="col-md-12">
  <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                      <ul class="pull-right">
 <?php  $instLinkArray = $this->General->getInstructionLists();
        foreach($instLinkArray as $key => $value) { ?>
        <li>
        <?php 
          echo $this->Html->link($value,
            array('controller'=>'static_pages','action'=>'static_view','prefix'=>'static_pages','key' => $key),
            array('class'=>'view_inst popup_window','alt'=>'Instructions','title'=>$value,'style'=>'color:red')
          );  
        ?>
        </li>
      <?php } ?>
      </ul>
      </nav>
      </div>
    </div>
</div> -->






    <script src="moment.js"></script>
    <script src="moment-timezone-with-data.js"></script>
    <div class="ajax">

      <div class="parent_data">
   <?php if(count($resultData)>0){
      $i = 1;
      $sum=0;
      foreach($resultData as $result): ?>
              <!-- /.usercard -->
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
                  <div class="white-box">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs firstLISt" role="tablist">
                          <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
                      </ul>
                      <!-- Tab panes  substring($row->parent->category_name,35);  -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane ihome active" >
                             <ul class="list-group ">
                              <li class="list-group-item list-group-item-danger 1"><b>Project Name:</b> 
                             <?php $str= $result['Projects']['project_name']; 
                               echo substr($str,0,15).((strlen($str)>15)? "...":""); ?> 
                               </li>
                               <li class="list-group-item list-group-item-success 2"><b>Amount:</b> 
                            
                               <?php echo $result['amount']; ?>
                      </li>
                                  <li class="list-group-item list-group-item-info 3"><b>TDS Amount :</b> 
                                     <?php echo $result['tds_amount']; ?>

                                     </li>
                                     <li class="list-group-item list-group-item-warning 4"><b>Branch :</b> 
                                 <?php echo $result['Branch']; ?>
                                         <li class="list-group-item list-group-item-danger 5"><b>Payment date:</b>
                                        <?php echo  date_format(date_create($result['payment_date']),"Y-m-d");?>
                                      </li>



 <?php  $paydate= date_format(date_create($result['payment_date']),"Ymdhis");?>
<input value = "<?php echo $paydate;?>" class="hidden paydate" />
                       <ul class="nav nav-tabs list-group-item-info" role="tablist">
						<li><?php 
						echo $this->Html->link(
						'<i class="fa fa-cog"></i>',
						array('controller'=>'payments','action'=>'edit',$result['id']),
						['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
						);?></li>
						<li>
						<?php
						echo $this->Html->link(
						'<i class="ti-close"></i>',
						array('controller'=>'payments','action'=>'delete','id'=>$result['id']),
						['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
						);
						?> </li>

                          </ul>
                           </ul>
                             </div>
                             <div role="tabpanel" class="tab-pane iprofile">
                                <div class="col-md-6">
                                  <h3>Lets check profile</h3>
                                  <h4>you can use it with the small code</h4>
                              </div>
                              <div class="col-md-5 pull-right">
                                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                              </div>
                              <div class="clearfix"></div>
                          </div>
                          <div role="tabpanel" class="tab-pane imessages">
                            <div class="col-md-6">
                              <h3>Come on you have a lot message</h3>
                              <h4>you can use it with the small code</h4>
                          </div>
                          <div class="col-md-5 pull-right">
                              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div role="tabpanel" class="tab-pane isettings">
                        <div class="col-md-6">
                          <h3>Just do Settings</h3>
                          <h4>you can use it with the small code</h4>
                      </div>
                      <div class="col-md-5 pull-right">
                          <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
      <?php $i++ ;
      endforeach; ?>
      <?php } else { ?>
<div class="white-box leading">
<center class="ld"><h3 class="text-uppercase ">No Record Found !</h3></center><p class="text-muted m-t-30 m-b-30"></p> 
</div>
        <?php
      }
    ?>
     </div>
<!-- /.usercard-->
</div>
<!-- /.row -->
<?php 
echo  $this->Html->script(array('pdetail'));
?>

<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>

<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
<?php 
echo  $this->Html->script(array('credlist','moment'));
?>
<script type="text/javascript">
    function veryfycheck()
    {
      alert("1) Either billing for this project is already added for this week."+'\n'+
          "2) Please verify all billing till previous week for this project and then reload this page again to add billing.");

  }

  (function() {

    [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();
</script>


