<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
 <script src="<?php echo BASE_URL; ?>js/common.js"></script>

<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#payment_date" ).datepicker({
				dateFormat: 'yy-mm-dd'
			});
		$('body div#ui-datepicker-div').css({'display':'none'});
	});
		
</script>
	<!--  start content-table-inner -->
	 <div class="col-sm-6">
            <div class="white-box">

	<?php echo $this->Form->create('Payment',array('url' => ['action'=>'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
			<?php echo $this->Flash->render(); ?>
	<div class="form-group">
              <label for="exampleInputpwd1">Project Name:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
			
				<?php
			   	$options = $this->General->getProjects();
				 echo $this->Form->input("project_id",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false));
            ?></div>
  <?php if(isset($errors['project_id'])){
							echo $this->General->errorHtml($errors['project_id']);
				} ;?></div>
      

	<div class="form-group">
              <label for="exampleInputpwd1">Payment Date:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				 echo $this->Form->input("payment_date",array("type"=>"text","class"=>"form-control","id"=>"payment_date","label"=>false,"div"=>false));
            ?></div>
              <?php if(isset($errors['payment_date'])){
							echo $this->General->errorHtml($errors['payment_date']);
				} ;?></div>
        	<div class="form-group">
              <label for="exampleInputpwd1">Resource:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

				<?php
				//$options = $this->General->getuser();
				$options=$this->General->getuser_name();

				 echo $this->Form->input("responsible_id",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false));
            ?></div>
<?php if(isset($errors['responsible_id'])){
							echo $this->General->errorHtml($errors['responsible_id']);
				} ;?></div>
       


	<div class="form-group">
              <label for="exampleInputpwd1">Branch:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>



				<?php
				   $options=array('lko'    =>  'Lucknow', 
											'cdg'    => 'Chandigarh'
                      );
			echo $this->Form->input("branch",array('type'=>'select','options'=>$options,"id"=>"branch","class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>
<?php if(isset($errors['branch'])){
							echo $this->General->errorHtml($errors['branch']);
				} ;?></div>
        </td>
		</tr>		
		
		
			<div class="form-group">
              <label for="exampleInputpwd1">Currency:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

				<?php
				   $options=array('usd'    =>  'USD', 
											'aud'    => 'AUD',
											'cad'    => 'CAD',
											'inr'    => 'INR'
                      );
			echo $this->Form->input("currency",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>
  <?php if(isset($errors['currency'])){
							echo $this->General->errorHtml($errors['currency']);
				} ;?></div>
     		
			<div class="form-group">
              <label for="exampleInputpwd1">Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				 echo $this->Form->input("amount",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
<?php if(isset($errors['amount'])){
							echo $this->General->errorHtml($errors['amount']);
				} ;?></div>
        		
			<div class="form-group">
              <label for="exampleInputpwd1">TDS Amount:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
				 echo $this->Form->input("tds_amount",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
<?php if(isset($errors['tds_amount'])){
							echo $this->General->errorHtml($errors['tds_amount']);
				} ;?></div>



       <div class="form-group"> 
               <label for="inputName" class="control-label">Notes:</label>
				<?php
					echo $this->Form->input("note",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
            ?>
<?php if(isset($errors['note'])){
							echo $this->General->errorHtml($errors['note']);
				} ;?>
       </div>


<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'payments','action'=>'add','prefix' => 'payments'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/payments/add', true)));?>

 </div>
 </div>
</div>
<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Payments Management</h4>
                 Payments Management is used to keep track of project payments. 
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'payments','action'=>'paymentslist'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     <!-- /.right-sidebar -->
</body>
</html>
