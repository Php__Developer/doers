
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>

<?php //echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','function.js')); ?>

<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>



<script type= "text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	var base = $('.base').val();

	/*function editRecord(id){
		spantoInput('amount_'+id);
		spantoInput('tdsamount_'+id);
		spantoInput('payDate_'+id);
		$('#payDate_'+id).datepicker({
				dateFormat: 'dd-mm-yy'
			});
			$('body div#ui-datepicker-div').css({'display':'none'});
		data_span = $('#note_'+id).html();
		$('#note_'+id).replaceWith('<textarea id="note_'+id+'" rows="2" cols="20">'+data_span+'</textarea>');
		changeLink('edit_'+id);
		$('#currency_'+id).hide();
		$('#resource_'+id).hide();
		$('#currency_'+id).siblings('select').show();
		$('#resource_'+id).siblings('select').show();
	}*/
	
	/*function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" style="width:100px;height:30px;" class="form-control">');
	}*/
	
/*	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}*/

	/*function verifyRecord(id){
	if($.active>0){
		}else{
			$.post(base_url+'admin/payments/quickview',{'id':id,'key':'verify'},function(data){
				if(data==1){
					$('#amount_'+id).closest("tr").remove();
						$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Payment has been varified!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					get_total();
				}
			});
		}
	}*/


/*	function editData(id){
			var postdata = {	'id':id,
										'payment_date':$('#payDate_'+id).val(),
										'responsible_id':$('#res_'+id).val(),
										'tds_amount':$('#tdsamount_'+id).val(),
										'amount':$('#amount_'+id).val(),
										'note':$('#note_'+id).val(),
										'currency' : $('#cur_'+id).val(),
									};
									var amount = $('#amount_'+id).val();
								
				if(!amount.match(/^-?\d*(\.\d+)?$/) || amount==""){
					var error_msg = "Enter a valid amount";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if($('#tdsamount_'+id).val() !="" && !$('#tdsamount_'+id).val().match(/^-?\d*(\.\d+)?$/)){
					var error_msg = "Enter a valid tds amount";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if($('#res_'+id).val() ==""){
					var error_msg = "Select resource";
					$('#error_msg').html(error_msg);
						$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			var base_url = "<?php echo $this->Url->build('/', true); ?>";

			$.post(base_url+'payments/quickview',postdata,function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Payment has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					updateRecord(id);
					get_total();
				}
			});
		}
	}*/
	

	

	$(document).ready(function(){
		get_total();
		
	});
	function get_total()
	{
		var total  = 0;
		$('[id^="amount_"]').each(function(){
		
			total = parseFloat(total)+parseFloat($(this).html());
			
		});
		$('#total_amount').html(total.toFixed(2));
	}
function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
		}
function is_verify(){
			if(confirm("Are you sure you want to verify?") == true){
			return true;
		}
		return false;
		}
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<center><div class="font-bold text-success"><?php echo $project_name; ?> Payment Details  </div>
			
				<div id="success"></div>
				<div id="error_msg"></div>
			  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-5" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr>
                    <!--<th class="userattend" width="10%">Branch</th>-->
						<th class="userattend" width="10%">Amount</th>
						<th class="userattend" width="15%">TDS Amount</th>
						<th class="userattend" width="10%">Payment Date</th>
						<th class="userattend" width="15%">Resource</th>
						<th class="userattend" width="10%">Currency</th>
						<th class="userattend" width="25%">Notes</th>
						<th class="userattend" width="15%">Options</th>
					</tr>
				<?php if(count($resultData)>0){ 
				foreach($resultData as $paymentData): 
					//pr($paymentData['Users']);
					?>
				<tr>
					<?php $id = $this->General->encForUrl($this->General->ENC($paymentData['id']) ) ; ?>
                <!--    <td><span id="branch_<?php echo $id; ?>"><?php // echo $paymentData['Payment']['Branch']; ?></span></td>-->
					<td><span id="amount_" class="amount_"><?php  echo $payment_sum=number_format((float)$paymentData['amount'],2,".",""); ?></span></td>
					<td><span id="tdsamount_" class="tdsamount_"><?php echo $paymentData['tds_amount']; ?></span></td>
					<td><span id="payDate_" class="payDate_"><?php echo date("d-m-Y",strtotime($paymentData['payment_date'])); ?></span></td>
					<td><span id="resource_" class="resource_"><?php  echo $paymentData['Users']['first_name'].' '.$paymentData['Users']['last_name']; ?></span>
					<?php echo $this->Form->input("resource",array('type'=>'select','options'=>$resourceSelect,'selected'=>$paymentData['responsible_id'],"style"=>"display:none;height:24px;width:94px;","id"=>"res_","label"=>false,"div"=>false,'class' => 'responsibleid'));  ?>
					</td>
					<td><span id="currency_" class="currency_"><?php  echo strtoupper($paymentData['currency']); ?></span>
					<?php echo $this->Form->input("currency",array('type'=>'select','options'=>$currencySelect,'selected'=>$paymentData['currency'],"style"=>"display:none;height:24px;width:94px;","id"=>"cur_","label"=>false,"div"=>false,'class' => 'currencyval'));  ?>
					</td>
					<td><span id="note_" class="note_"><?php  echo $paymentData['note']; ?></span></td>
					<td>
					<input type="hidden" name="paymentid" class="paymentid" value="<?php echo $id ;?>">
					
					<?php
					 
						if($paymentData['billing_id']==0){ 

						echo $this->html->link("<i class='fa fa-gear facontrol' ></i>","javascript:void(0)",
								array('class'=>'info-tooltip editRecord','title'=>'Edit','id'=>'edit_','onClick'=>"",'escape' => false)
							 ); 
					}
					echo $this->html->link("<i class='fa fa-check-square facontrol' ></i>","javascript:void(0)",
						array('class'=>'info-tooltip editData default_hidden','title'=>'Edit','id'=>'edit_','onClick'=>"",'escape' => false)
					);

					?>
					<?php  echo $this->Html->link("<i class='fa fa-times facontrol' ></i>","javascript:void(0)",
						array('class'=>'info-tooltip deleterec','title'=>'Delete','id'=>'del_','onClick'=>"",'escape' => false)
					); ?>
					
					</td>
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				<div  class="pull-right font-bold text-danger"><b>Total Amount : <span id="total_amount"></span></b> </div>
				</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>

<div class="clear"></div>
</div>
<!--  end content-table-inner  -->

<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>
<script src="<?php echo BASE_URL; ?>js/paymentsquickview.js"></script>
<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
		<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>