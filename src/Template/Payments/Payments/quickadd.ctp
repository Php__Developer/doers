<html>
<body>
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<!-- <link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet"> -->
	<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox'))	//ui.core ?>


	<script type="text/javascript">
			var base = '<?php echo BASE_URL; ?>';
			function addPayment(){
				var project_id = $('#project_id').val();
				var responsible_id = $('#responsible_id').val();
				var amount = $('#amount').val();
				var payment_date = $('#payment_date').val();
				var tds_amount = $('#tds_amount').val();
				var currency= $('#currency').val();
				
				if(payment_date==""){
					var error_msg = "Enter payment date";
					$('#error_msg').html(error_msg);
					$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 3000);
					return false;
				}
				if(responsible_id==""){
					var error_msg = "Select Resource.";
					$('#error_msg3').html(error_msg);
					$('#error_msg3').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg3').fadeOut('slow'); }, 3000);
					return false;
				}
				if(!amount.match(/^-?\d*(\.\d+)?$/) || amount==""){
					var error_msg = "Enter a valid amount";
					$('#error_msg1').html(error_msg);
					$('#error_msg1').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg1').fadeOut('slow'); }, 3000);
					return false;
				}
				if(tds_amount !="" && !tds_amount.match(/^-?\d*(\.\d+)?$/)){
					var error_msg = "Enter a valid tds amount";
					$('#error_msg2').html(error_msg);
					$('#error_msg2').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					setTimeout(function() { $('#error_msg2').fadeOut('slow'); }, 3000);
					return false;
				}
				var note = $('#note').val();
				$('.ajaxloaderdiv').show();
				if($.active>0){
				}else{
				 var base = "<?php echo $this->Url->build('/', true); ?>";
					$('.ajaxloaderdiv').hide();
					$.post(base+'payments/quickadd',{'project_id':project_id,'amount':amount,'note':note,'payment_date':payment_date,'tds_amount':tds_amount,'currency':currency,'responsible_id':responsible_id},function(data){
						var currentparent = window.parent.$('.currentpage').val();
						console.log(currentparent)
						console.log(data);
						if(data.response == 'success'){
							if(currentparent == 'projectdetails'){
								console.log("IF");
								window.parent.parent.$(document).find('.paymentstab').click();
								window.parent.$('.mfp-close').click();
							}
						} else {
								window.parent.parent.$('.paymentstab').click();
								window.parent.$('.mfp-close').click();
						}

			/*				if(data==1){
					//window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
				}else if(data=='error'){
					alert("Payment already exists!");
				}else if(data==0){
					window.parent.$('.mfp-close').click();
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
				}else{
				//alert("Billing updation failed!");
					window.parent.$('#close-fb').click();
				}*/
					});
				}
			}
			$(document).ready(function(){
				$( "#payment_date" ).datepicker({
					dateFormat: 'yy-mm-dd'
				});
				$('body div#ui-datepicker-div').css({'display':'none'});
			});
	</script>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $this->Form->create('Payment',array('url' => ['action'=>'quickadd'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>




	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<center> <h2 class="font-bold text-success"><?php echo $this->request->data['Payment']['project_name']; ?> Add Payment </h2><br/>
	


	<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style ="font-size:13px;" >
		<tr>
			<th valign="top">Project Name:</th>
			<td><b><?php 
				echo $this->request->data['Payment']['project_name'];
				echo $this->Form->hidden("project_id",array("id"=>"project_id","value"=>  $this->General->encForUrl($this->General->ENC($this->request->data['Payment']['project_id']) ) ) )
            ?></b></td>
			<td></td>
		</tr>
		<tr>
          <th valign="top">Payment Date:</th>
			<td><?php
				 echo $this->Form->input("payment_date",array("type"=>"text","class"=>"form-control","id"=>"payment_date","label"=>false,"div"=>false,"style" =>"height:32px;width:198px;"));
            ?><div id="error_msg"></div></td>
		</tr>
		<tr>
          <th valign="top">Resource:</th>
			<td><?php
				//$options = $this->General->getuser();
				$options=$this->General->getuser_name();
				
				 echo $this->Form->input("responsible_id",array("type"=>"select","class"=>"form-control","id"=>"responsible_id","options"=>$options,"label"=>false,"div"=>false));
            ?><div id="error_msg3"></div></td>
		</tr>

            <!--    <tr>
			<th valign="top">Branch:</th>
			<td><?php
				  /* $options=array('lko'    =>  'Lucknow', 
											'cdg'    => 'Chandigarh'
                      );
			echo $this->Form->input("branch",array('type'=>'select','options'=>$options,"id"=>"branch","class"=>"form-control","label"=>false,"div"=>false)); */
            ?></td>
		</tr>	-->	


		<tr>
			<th valign="top">Currency:</th>
			<td><?php
				   $options=array('usd'    =>  'USD', 
											'aud'    => 'AUD',
											'cad'    => 'CAD',
											'inr'    => 'INR'
                      );
			echo $this->Form->input("currency",array('type'=>'select','options'=>$options,"id"=>"currency","class"=>"form-control","label"=>false,"div"=>false));    
            ?></td>
		</tr>
		<tr>
          <th valign="top">Amount:</th>
			<td><?php
				 echo $this->Form->input("amount",array("class"=>"form-control numeric","id"=>"amount","label"=>false,"div"=>false, "style" =>"height:32px;width:198px;"));
            ?>
			<div id="error_msg1"></div>
			</td>
		</tr>
		<tr>
          <th valign="top">TDS Amount:</th>
			<td><?php
				 echo $this->Form->input("tds_amount",array("id"=>"tds_amount","class"=>"form-control","label"=>false,"div"=>false,"style" =>"height:32px;width:198px;"));
            ?><div id="error_msg2"></div></td>
		</tr>
		<tr>
          <th valign="top">Notes:</th>
			<td><?php
				 echo $this->Form->input("note",array("class"=>"form-textarea","label"=>false,"id"=>"note","div"=>false,"style" =>"height:103px;width:390px;"));
            ?></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<input type="button" onclick="javascript: addPayment();" value="Save" class="btn btn-success">
			</td>
			<td></td>
		</tr>
	</table></center>
	<!-- end id-form  -->

			</td>
			<td>

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->