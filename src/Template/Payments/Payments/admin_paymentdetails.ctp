<?php echo  $this->Html->css(array("screen")); ?>
<?php echo $this->Html->script(array('jquery-1.4.1.min','jquery-ui.min')); ?>
<script type="text/javascript">
	$(document).ready(function(){
		get_total();		
	});

	function get_total()
	{ 
		var total  = 0;
		$('[id^="amount_"]').each(function(){
			var idAmountSpan = $(this).attr('id');
			var id = idAmountSpan.replace("amount_","");
			//var total  = 0;
			var Cur_for_id = $('#currency_'+id).html();
				if(Cur_for_id=='INR') {
					var divider= 50;
				}else{
					var divider= 1;
				}
			total = (parseFloat(total)+(parseFloat($('#'+idAmountSpan).html())/divider)/10);
			//alert(total);
		});
		$('#total_amount').html(total.toFixed(2));
	}
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<center><div style="font-weight:bold;">Payment Details  </div>
				<div id="success" style="color:green;font-size:13px;float:left; padding-bottom:5px;"></div>
				<div id="error_msg"></div>
				<table style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
					<tr>
						<th class="userattend" width="15%">Amount</th>
						<th class="userattend" width="15%">TDS Amount</th>
						<th class="userattend" width="15%">Payment Date</th>
						<th class="userattend" width="15%">Currency</th>
						<th class="userattend" width="25%">Notes</th>
					
					</tr>
				<?php  $lastmonth1 = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));  
					  
				if(count($resultData)>0){


				
					foreach($resultData as $paymentData):
				//	pr($paymentData['id']);die();
					if($key == "cur"){
						//pr($key);die();
						$firstendDates =  $this->General->getFirstLastDates(date('m'),date('Y')) ;
						//pr($firstendDates);die();
					}else{
						$firstendDates =  $this->General->getFirstLastDates(date("m",$lastmonth1),date('Y')) ; // pr($lastMonth);
					}
					if(($paymentData['payment_date'] >= $firstendDates['firstdate']) && ($paymentData['payment_date'] <= $firstendDates['lastdate'])){ ?>
					<tr>
						<?php $id = $paymentData['id'];
						pr($id);die(); ?>
						<td><span id="amount_<?php echo $id; ?>"><?php  echo $paymentData['amount']; ?></span></td>
						<td><span id="tdsamount_<?php echo $id; ?>"><?php echo $paymentData['tds_amount']; ?></span></td>
						<td><span id="payDate_<?php echo $id; ?>"><?php echo date("d-m-Y",strtotime($paymentData['payment_date'])); ?></span></td>
						<td><span id="currency_<?php echo $id; ?>"><?php  echo strtoupper($paymentData['currency']); ?></span>
						</td>
						<td><span id="note_<?php echo $id; ?>"><?php  echo $paymentData['note']; ?></span></td>
					</tr>
					<?php }
					endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				<div style="float:right;color:red;"><b>Total Amount : <span id="total_amount"></span></b> </div>
				</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->