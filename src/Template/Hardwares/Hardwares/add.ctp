<!--  start content-table-inner -->
	<?php echo $this->Html->css(array('oribe')); ?>
	<!--  start content-table-inner -->
	 <div class="col-sm-6">
            <div class="white-box">



		
	<?php echo $this->Form->create('Hardware',array('method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
	<?php //$session->flash(); ?>
	


  <div class="form-group">
              <label for="exampleInputpwd1">Item Name:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				 echo $this->Form->input("item",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
         <?php if(isset($errors['item'])){
							echo $this->General->errorHtml($errors['item']);
				} ;?></div>



  <div class="form-group">
              <label for="exampleInputpwd1">Alloted To:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


				<?php
				//$options = $this->General->getuser();
				$options=$this->General->getuser_name();


				 echo $this->Form->input("alloted_to",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false));
            ?></div>
         <?php if(isset($errors['alloted_to'])){
							echo $this->General->errorHtml($errors['alloted_to']);
				} ;?></div>



  <div class="form-group">
              <label for="exampleInputpwd1">Unique Id :</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
               <?php
				 echo $this->Form->input("unique_id",array("type"=>"text","class"=>"form-control","label"=>false,"div"=>false));
				 //echo $form->hidden("id");
            ?></div>
         <?php if(isset($errors['unique_id'])){
							echo $this->General->errorHtml($errors['unique_id']);
				} ;?></div>


  <div class="form-group">
              <label for="exampleInputpwd1">Others:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
      <?php
				echo $this->Form->input("others",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
         <?php if(isset($errors['others'])){
							echo $this->General->errorHtml($errors['others']);
				} ;?>
</div>





  <div class="form-group">
              <label for="exampleInputpwd1">Purchase Price:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
				 echo $this->Form->input("purchase_price",array("class"=>"form-control","label"=>false,"div"=>false));
            ?>
            </div>
         <?php if(isset($errors['purchase_price'])){
							echo $this->General->errorHtml($errors['purchase_price']);
				} ;?></div>



  <div class="form-group">
              <label for="exampleInputpwd1">Current Price:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>     

     <?php
				echo $this->Form->input("current_price",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
         <?php if(isset($errors['current_price'])){
							echo $this->General->errorHtml($errors['current_price']);
				} ;?>
</div>



      <div class="form-group"> 
               <label for="inputName" class="control-label">Current Location:</label>
	
				<?php
				echo $this->Form->input("current_location",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
				 
            ?>
         <?php if(isset($errors['current_location'])){
							echo $this->General->errorHtml($errors['current_location']);
				} ;?>

</div>


 <div class="form-group"> 
               <label for="inputName" class="control-label">Description:</label>

     <?php
				 echo $this->Form->input("description",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
            ?>
         <?php if(isset($errors['description'])){
							echo $this->General->errorHtml($errors['description']);
				} ;?>
</div>





  <div class="form-group">
              <label for="exampleInputpwd1">Legends:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>

     <?php
			$options = array(
								'0'=>'-Select-',
								'1'=>'Computer peripherals',
								'2'=>'Electric Items',
								'3'=>'Furniture',
								'4'=>'Software & Scripts',
								'5'=>'Others',
							);
				 echo $this->Form->input("legends",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false));
       ?></div>
    <?php if(isset($errors['legends'])){
							echo $this->General->errorHtml($errors['legends']);
				} ;?></div>



  <div class="form-group">
              <label for="exampleInputpwd1">Status:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
     <?php
			$optionss = array(
								'1'=>'Active',
								'2'=>'Lost',
								'3'=>'Damaged',
							);
				echo $this->Form->input("status",array("type"=>"select","class"=>"form-control","options"=>$optionss,"label"=>false,"div"=>false));
       ?>
</div></div>


<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'hardwares','action'=>'add','prefix' => 'hardwares'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/hardwares/add', true)));?>

 </div>
 </div>
</div>
<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Hardware Management</h4>
           Hardware Management keep track of all assets information.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'hardwares','action'=>'hardwareslist'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     <!-- /.right-sidebar -->
</body>
</html>



