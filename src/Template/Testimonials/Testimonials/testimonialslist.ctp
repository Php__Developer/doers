
 <!-- .row -->
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="css/oribe.css" rel="stylesheet">
<?php echo $this->Html->css(array("oribe")); ?>
<?php echo $this->Form->create('Testimonial',array('url' => ['action'=>'testimonialslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>

<style>
.switchery.switchery-small {
    float: right;
    margin-left: 147px;
    margin-top: 12px;
}
.list-group-item-warning {
    padding: 11px;
}
</style>
<div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                      <ul class="pull-right">
		
            <?php echo $this->Html->link("Download Odesk : Excel",
							array('controller'=>'testimonials','action'=>'exportciodesk'),
							array('class'=>'view_delayed','style'=>'color:red;')
							);	
							?>
 Invite friends who 
          
            <?php echo $this->Html->link("Download Odesk : PDF",
            array('controller'=>'testimonials','action'=>'download'),
            array('class'=>'view_delayed','style'=>'color:red;')
            );	
            ?>
         
            <?php echo $this->Html->link("Download Elance : Excel",
            array('controller'=>'testimonials','action'=>'exportcielance'),
            array('class'=>'view_delayed','style'=>'color:red;')
            );	
            ?>
      
            <?php echo $this->Html->link("Download Elance : PDF",
            array('controller'=>'testimonials','action'=>'elance'),
            array('class'=>'view_delayed','style'=>'color:red;')
            );	
            ?>
          
            <?php echo $this->Html->link("Download Other : Excel",
            array('controller'=>'testimonials','action'=>'exportciother'),
            array('class'=>'view_delayed','style'=>'color:red;')
            );	
            ?>
          
            <?php echo $this->Html->link("Download Other : PDF",
            array('controller'=>'testimonials','action'=>'other'),
            array('class'=>'view_delayed','style'=>'color:red;')
            );	
            ?>
                 </ul>
                    </nav>
               </div><!-- /tabs -->
<div class="displaycount"><span class="counteringdata"> 1 of <?php echo $paginatecount;?></span>  Page(s)</div>
<input type="hidden" value="<?php echo $paginatecount;?>" class="paginate" >
   <div class="col-md-12">
       <div class="col-md-3 searchingdata">
                 <?php
				$fieldsArray = array(
				//''				  => 'Search By Select',
				'Testimonials.author' => 'Search By Author',
				'Testimonials.description' => 'Search By Description',
				'Testimonials.type'  => 'Search By Type'
				);
			 
			  echo $this->Form->select("Testimonial.fieldName",$fieldsArray,['value'=> $search1,'class'=>'selectpicker m-b-20 m-r-10 bs-select-hidden','data-style'=>'btn-primary btn-outline'],array("id"=>"searchBy selectopt","label"=>false,"class"=>"form-control","empty"=>false),false); ?>
               </div>
           <div class="col-md-4">
		   <?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "Testimonial.status"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $this->Form->input("Testimonial.value1",array("id"=>"example-input1-group2 demo-input-search2 searchval","class"=>"form-control searchval","style"=>"$display1", "div"=>false, "label"=> false,"value"=>$search2,"placeholder"=>"Search"));
				?>
			
				

         </div>

		<div class="col-md-4">
		  
				<?php
		echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";

			echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
            $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/testimonials/testimonialslist', true)));
				?>

			<button class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" aria-expanded="false" data-toggle="dropdown" type="button">
			Sort By
			<span class="caret"></span>
			</button>
			<ul class="dropdown-menu animated drp_list" role="menu">
		    <li>
			<a href="#" class="1">Author</a>
			</li>  
			<li>
			<a href="#" class="2">Description</a>
			</li>

			<li>
			<a href="#" class="3">Type </a>
			</li>
			<li>
			<a href="#" class="4"> Keyword</a>
			</li>
			
			<li>
			<a href="#" class="5"> Modified </a>
			</li>
			<!-- <li class="divider"></li> -->
			
			</ul>

			<?php 
			echo $this->Html->link('<i class="fa  fa-plus"></i>',
			array('controller'=>'testimonials','action'=>'add'),
			['escape' => false,"class"=>"btn btn-info btn-circle ","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Testimonial"]
			);
			?>
			<?php echo $this->Form->end(); ?>
			
		 </div>
		 <div class="col-md-1 paginateproject">
		     <ul class="pagination paginate-data">
				<li class="previousdata">
				
				<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
				</li>
                 <li>
				<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
				</li>





				</ul>
				
             </div>

<script src="moment.js"></script>
<script src="moment-timezone-with-data.js"></script>
<div class="ajax">

  <div class="parent_data">
  <?php if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $result):
 	$testi_is_enabled = ($result['status'] == 1) ? 'checked': '';?>
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
          <div class="white-box">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs firstLISt" role="tablist">
              <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
              <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
            </ul>
            <!-- Tab panes  substring($row->parent->category_name,35);  -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane ihome active" >
                 <ul class="list-group ">
			<li class="list-group-item list-group-item-danger 1"><b>Author :</b> 
			<?php $str = strip_tags($result['author']); 
			echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
			<li class="list-group-item list-group-item-success 2"><b>Description :</b> 
			<?php echo substr(strip_tags($result['description']),0,10)."..."; ?></li>
			<li class="list-group-item list-group-item-info 3"><b>Type :</b> 
			<?php echo $result['type']; ?></li>
			<li class="list-group-item list-group-item-warning 4"><b>Keyword :</b> 
			<?php echo substr(strip_tags($result['keyword']),0,19)."..."; ?></li>
			<li class="list-group-item list-group-item-danger 5"><b>Modified :</b> 
			<?php echo date(DATE_FORMAT, strtotime($result['modified'])); ?></li>
		   
                 <?php 
 if(isset($result['modified'])){
 $last_update=$result['modified']->format("Ymd");
 }else{
 $last_update="";
 }
         ?>

		     <input type="hidden" value = "<?php echo $this->General->ENC($result->id);?>" class="testicons" />

            <ul class="nav nav-tabs list-group-item-info " role="tablist">
              <li><?php
                         echo $this->Html->link(
		                  '<i class="fa fa-cog"></i>',
		                  array('controller'=>'testimonials','action'=>'edit','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
		                  );
                    ?></li>
						   <li>
                         <?php
                           echo $this->Html->link(
		                  '<i class="ti-close"></i>',
		                  array('controller'=>'testimonials','action'=>'delete','id'=>$result['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
		                  );
		                  ?>
                        </li>
                        <li>
                     <input type="checkbox" <?php echo $testi_is_enabled;?> class="js-switch pull-right textChekb0x"  data-color="#f96262" data-size="small" />
                        </li>
	                </ul>
	                 <input id="last_update" class="last_update" value="<?php echo $last_update;?>">
	            </ul>

            </div>
              <div role="tabpanel" class="tab-pane iprofile">
                <div class="col-md-6">
                  <h3>Lets check profile</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane imessages">
                <div class="col-md-6">
                  <h3>Come on you have a lot message</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane isettings">
                <div class="col-md-6">
                  <h3>Just do Settings</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            </div>
        </div>
         <?php $i++ ;
				endforeach; ?>
				<?php } else { ?>

				<?php
				}
			?>
         </div>
        <!-- /.usercard-->
      </div>
<!-- /.row -->
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
 <?php 
    echo  $this->Html->script(array('commonall','moment'));
?>
<script type="text/javascript">
function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}

      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>


