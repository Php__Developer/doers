

<!--  start content-table-inner -->
        <div class="col-sm-6">
            <div class="white-box">

                  <?php echo $this->Flash->render(); ?>

<?php echo $this->Form->create($TestimonialsData,array('url'=>['action'=>'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
        <!-- start id-form -->

    <div class="form-group">
                    <label for="exampleInputuname">Author*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputuname" placeholder="Sub-Module Name" name="sub_module"> -->
                                       <?php echo $this->Form->input("author",array("type"=>"text","class"=>"form-control",'placeholder'=> "Author Name", "label"=>false,"div"=>false));?>

		               </div>
                     <?php if(isset($errors['author'])){
                      echo $this->General->errorHtml($errors['author']);
                      } ;?>
                      
                  </div>
                  <div class="form-group">
                       <label for="exampleInputpwd1">Type*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('type',['upwork'=> 'Upwork','elance'=>'Elance','other'=>'Other'],['empty' => '--Select--','class' => 'form-control'] );?>
                    </div>
                 </div>
<div class="form-group"> 
   <label for="inputName" class="control-label">Description</label>
             <?php
 echo $this->Form->input("description",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the description here.."));
		 if(isset($errors['description'])){
                      echo $this->General->errorHtml($errors['description']);
                      } ;
       echo $this->Form->hidden("id");

            ?>
</div>
  <div class="form-group">
                    <label for="exampleInputuname">Keyword*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                             <?php
    echo $this->Form->input("keyword",array("class"=>"form-control","label"=>false,"div"=>false, "maxlength"=>"2000","required"=>"required","placeholder"=>"Keyword")); ?>
        </div>
                         <?php if(isset($errors['keyword'])){
                      echo $this->General->errorHtml($errors['keyword']);
                      } ;?>
                  </div>

<div class="form-group">
 <button type="submit" class="btn btn-success">Submit</button>
 <?php //echo $this->Form->submit('Submit',array('class'=>"btn btn-success",'div'=>false)); 
  echo $this->Html->link("Reset",
                ['controller'=>'testimonials','action'=>'add','prefix' => 'testimonials'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
				 $this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/testimonials/add', true)));?>
 </div>
 </div>
</div>
    <!-- end id-form  -->

    

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Testimonial Management</h4>
          This Section is used by Admin and Sales only to Manage Testimonials.
         
					<ul >
  					<li>
            <?php 
            echo $this->Html->link("Go To Listing",
            array('controller'=>'testimonials','action'=>'testimonialslist'),
              array('style'=>'color:red;')
            );	
            ?>
            </li> 
  					<li>
            <?php echo $this->Html->link("Download Odesk : Excel",
            array('controller'=>'testimonials','action'=>'exportciodesk'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Odesk : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'pdf'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Elance : Excel",
            array('controller'=>'testimonials','action'=>'exportcielance'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Elance : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'elance'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
  					
  					<li>
            <?php echo $this->Html->link("Download Other : Excel",
            array('controller'=>'testimonials','action'=>'exportciother'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Other : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'other'),
              array('style'=>'color:red;')
            );	
            ?>
            </li>
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     
      <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
          <div class="r-panel-body">
            <ul>
              <li><b>Layout Options</b></li>
              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
              
            </ul>
            <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 chatonline">
              <li><b>Chat option</b></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /.right-sidebar -->



</body>
</html>
