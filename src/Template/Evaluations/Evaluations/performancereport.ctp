<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){ 
		$('.tablesorter').tablesorter({dateFormat: "uk"});
		
		$(".viewPaydetails").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
		$(".viewattendance").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	900, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
	function validateForm(){
			var syear = $('#year1').val();
			var eyear = $('#year2').val();
			var smonth = $('#month1').val();
			var emonth = $('#month2').val();
			//alert(syear);
			//alert(eyear);
			//alert(smonth);
			//alert(emonth);
		if(($('#month1').val()!="" && $('#year1').val()!="") && ($('#month2').val()!="" && $('#year2').val()!="")){
			//alert(syear);
			if (syear>eyear) {
				var message = "<div>Search date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}else if(syear == eyear){
				if(smonth > emonth){
					var message = "<div>Search date should be less than end date</div>";
					//alert(message);
					$('#error').html(message);
					return false;
				}
			}
		}else if($('#month1').val()!="" && $('#year1').val()==""){
			var message = "<div>Select year.</div>";
				$('#error').html(message);
				return false;
		}else if($('#month2').val()!="" && $('#year2').val()==""){
			var message = "<div>Select year.</div>";
				$('#error2').html(message);
				return false;
		}
	
	}
</script>
<?php
$urlString = "";
$newUrl = "performancereport".$urlString;
/* $urlArray = array(
	'field' 	=> $search1,
	'value' 	=> $search2
); */
//$paginator->options(array('url'=>$urlArray));
?>
<?php echo $this->Form->create('Evaluation',array('url' => ['action'=>'performancereport'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform","onsubmit"=>"return validateForm();")); ?>
<!--  start content-table-inner -->
	

	<div class="white-box">
         
	<ul class="pagination pull-right">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
	



	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<div class="searching_div">
		<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
			<tr>
				<td width="50%">
				<div style="padding-left:4px;">
				<div style="float:left;">
				<div style="float:left;margin-right:50px">	<b> Username:</b></div>
			<?php  echo $this->Form->input("user_id",array("type"=>"text","class"=>"form-control","style"=>"width:100px","div"=>false,"label"=> false)); ?>
				</div>
				<div style="clear:both"></div>
				</div>
				</td><br/>
				<td width="50%">
				<div style="float:left;margin-right:77px;">	
				<b> Senior:</b></div>
				<?php  echo$this->Form->input("senior_id",array("type"=>"text","class"=>"form-control","style"=>"width:100px","div"=>false,"label"=> false)); ?>
				
				</td>
			</tr>
			</table><br/>
				<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
				<tr>
				<td width="10%">
					<div>
					<div style="float:left">
					<span style="vertical-align: middle;">
					<b>To:</b>
					</span>
					</div>
				</td>
				<td width="40%">
					<div style="float:left">
					<b>Month:</b>
					<br>
				<?php  
						echo $this->Form->month('mob1', array('monthNames' => true,'class'=>'form-control','type'=>'select','div'=>false,'style'=>'width:100px;','id'=>'month1'));
					?>
					</div>
					<div style="float:left">
					<b>Year:</b>
					<br>
					<?php 
						  $curYear 	= "2013";
						  $array 	= array();
						  $array[''] = "-Select-";
						  while($curYear<='2025'){
						  $array[$curYear] = $curYear;
						  $curYear++;
						  }
						  echo $this->Form->input("year1",array("type"=>"select","class"=>"form-control","options"=>$array,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"year1"));
					?>
					</div>
					</div>
				</td>
				<td width="10%">
				<div>
				<div style="float:left">
					<span style="vertical-align: middle;">
					<b>From:</b>
					</span>
					</div>
				</td>
				<td width="40%">
					<div style="float:left">
					<b>Month:</b><br/>
					<?php  

					echo $this->Form->month('mob', array('monthNames' => true,'class'=>'form-control','type'=>'select','div'=>false,'style'=>'width:100px;','id'=>'month2'));

					?>
					</div>
					<div style="float:left">
					<span style="">
					<b>Year:</b></span><br/>
					<?php 
						  $curYear 	= "2013";
						  $array 	= array();
						  $array[''] = "-Select-";
						  while($curYear<='2025'){
						  $array[$curYear] = $curYear;
						  $curYear++;
						  }
						  echo $this->Form->input("year2",array("type"=>"select","class"=>"form-control","options"=>$array,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"year2"));
					?>
					</div>
					</div>
				</td>
				</tr>
				<tr>
				<td colspan="2">
					<div style="color:red;padding-left:117px" id="error"></div>
					<div style="color:red; float: right;margin-right: -178px;" id="error2"></div>
				</td>
				</tr>
			</table><br/>
			<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
			<tr>
				<td width="50%" colspan="2">
				<div style="padding-left:90px;">
	<?php
	echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
	echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
	$this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/evaluations/performancereport', true)));
	?>
                    </div>
				</td>
			</tr>
		</table>
		<br/>
				
		</div>
	
 <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="stack" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="" >
              <thead>
                <tr>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">User</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Senior</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">User Comment</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Senior Comment</button></th>
                      <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Ratings</button></th>
                       <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Tickets</button></th>
                        <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">ROI</button></th>
                           <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Other</button></th>
                                </tr>
              </thead>
              <tbody>
				<?php 
				//$userData = $session->read("SESSION_ADMIN");
			//	$sessionID = $userData[0];
				//$rolesAry = explode(",",$userData[2]);
				if(count($evalData)>0){
					//pr($evalData);
				foreach($evalData as $userData):  $class=""; 
				             
				                 $user_id    = $userData['user_id'];
                                $UserStatus = $userData['Users']['status'];
                              // pr($user_id);
                               // pr($UserStatus);
                                if($UserStatus >0){
				?>
				<tr class="<?php echo $class; ?>">
					<td><?php echo $userData['Users']['first_name']." ".$userData['Users'] ['last_name']; ?> </td>
					
					<td><?php echo $userData['Seniors'] ['first_name']." ".$userData['Seniors'] ['last_name']; ?> </td>
					<td><?php $str = strip_tags($userData['user_comment']);
						echo substr($str,0,50).((strlen($str)>50)? "...":""); ?>
						</td>
					<td>
					<?php $str = strip_tags($userData['senior_comment']);
						echo substr($str,0,50).((strlen($str)>50)? "...":"");
						?> </td>
					<td><?php echo "Performance: ".$userData['performance_5']."<br/>";
						echo "Process: ".$userData['process_5']."<br/>";
						echo "Knowledge: ".$userData['knowledge_5']."<br/>";
						echo "Discipline: ".$userData['discipline_5']."<br/>";
						echo "HR activities: ".$userData['hractivities_5'];
					?> </td>
					<?php $aligned = 0; $received = 0; $closedTic =0 ; 
						$resultTicket = array();
						//pr($resultTicket);

						$resultTicket = ( !empty($userPerformanceData['Tickets'][$user_id])) ? $userPerformanceData['Tickets'][$user_id] : array();
							//pr($resultTicket);
						//pr($resultTicket);
					if(count($resultTicket)>0){
						foreach($resultTicket as $k1=>$v1){
							//pr($k1);
							if($v1['Ticket']['to_id']==$user_id){
								$received++;
							}elseif($v1['Ticket']['from_id']==$user_id){
								$aligned++;
							}
							if($v1['Ticket']['status']==0 &&  $v1['Ticket']['last_replier']==$user_id){
								$closedTic++;
							}
						}
					}?>
				    <td><?php echo "Ticket Aligned: ".$aligned."<br/>";
						echo "Ticket Received: ".$received."<br/>";
						echo "Ticket Closed: ".$closedTic;
					?></td>
					<!--Calculate specified months ROI for a responsible user -->
					<?php $roiSum = "";
					//$paymentData = array();
					$paymentData = ( !empty($userPerformanceData['Payment'][$user_id])) ? $userPerformanceData['Payment'][$user_id] : array();
					//pr($paymentData);
					if(count($paymentData)>0){
					foreach ($paymentData as $k=>$v) { 
						$roiSum += ($v['Payment']['currency']=='inr') ?  $v['Payment']['amount']/50 : $v['Payment']['amount'];
						}
					}
					?>
					<td><?php $amount1 = ($roiSum/10) ; 
						if($amount1 < 120) {
						$styleColor = "lessAmount"; } else{
						$styleColor = '';
						} 
						echo "ROI :".$amount1;
					 	/*echo $this->Html->link($amount1,
								array('controller'=>'payments','action'=>'paymentdetails','prefix' =>'payments','id'=>$userData['user_id'],'last'=>'last'),array('class'=>'info-tooltip viewPaydetails '.$styleColor,'title'=>'Details','target'=>'_blank')
							); */
						//pr($rolesAry);
						//pr($biddingCount);
						$rolesAry = array();
						
						$rolesAry  = $userPerformanceData['Roles'];
						//pr($rolesAry);
						if(in_array('5',$rolesAry)){
							$biddingCount = array();
							$biddingCount  = $userPerformanceData['Biddings'][$user_id];

							//pr($biddingCount);
							echo "<br/>";

							//echo "Bidding Count :".count($biddingCount); 
						}
						echo "<br/>";
						echo "Bidding Count :".count($biddingCount); 

					?></td>
					<td><?php if($userData['status']==1){
						echo $this->Html->link("Respond",
								array('controller'=>'evaluations','action'=>'userresponse','id'=>$userData['id'],'response'=>'response'),
								array('class'=>'info-tooltip viewPaydetails popup_window ','title'=>'Respond')
							);
							
					}elseif($userData['status']==2){
						echo $this->Html->link("View Details",
								array('controller'=>'evaluations','action'=>'userresponse','id'=>$userData['id'],'detail'=>'detail'),array('class'=>'info-tooltip viewPaydetails popup_window','title'=>'Details'));
					}
					echo "<br/>";
					//pr($userData);
					echo $this->Html->link("View Attendance",
								array('controller'=>'attendances','action'=>'monthlyattendance','prefix' =>'attendances','id'=>$userData['user_id'],'month'=>$userData ['month'],'year'=>$userData['year']),
								array('class'=>'info-tooltip viewattendance popup_window','title'=>'Attendance Report')
							);
					?></td>
				</tr>
				<?php } endforeach; }else{  ?>
				<tr>
					<td colspan="15"> No Records! </td>
				</tr>
				<?php } ?>
				</tbody>
				</table>
</div>

<!-- /#wrapper -->
<!-- jQuery -->
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery peity -->
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<!--Style Switcher -->
