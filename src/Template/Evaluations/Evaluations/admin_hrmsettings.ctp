<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('Evaluation',array('action'=>'hrmsettings','method'=>'POST','onsubmit' => '',"class"=>"login"));echo $session->flash();  ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<div style="float:left"><h2> Enable <?php echo "<i>".date("F,Y", strtotime( "-1 month" ))."</i>" ?> Ratings :</h2></div>
			<div style="float:right; padding-left:10px;margin-top:-6px;">
			<?php 
			echo $form->hidden('set');
			echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/users/dashboard'")); 
				echo $form->end();
			?>
			</div>
			<div class="clear"></div>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>HRM Settings</h5>
         HRM Settings used by HR to enable evaluations for previous month.
          <div class="lines-dotted-short"></div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->