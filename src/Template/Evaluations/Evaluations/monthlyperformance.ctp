<!-- .row -->
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="css/oribe.css" rel="stylesheet">
<?php echo $this->Html->css(array("oribe")); ?>
<input id="pe" class="pe" value="evalist">
<?php echo $this->Form->create('Evaluation',array('url' => ['action'=>'evaluationslist'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<?php $user = $session->read("SESSION_ADMIN"); ?>

<style>
    .switchery.switchery-small {
        margin-top: 12px;
        margin-left:90px;
    }
    .list-group-item-warning {
        padding: 11px;
    }
      .list-group-item-danger {
        padding: 11px;
    }
    .dropdown-menu.animated.drp_list {
    margin-left: 5px;
}
   .nav.nav-tabs.list-group-item-info.actioning.green {
    background-color: #ab8ce4;
}
   .nav.nav-tabs.list-group-item-info.actioning.redish {
    background-color: #fb9678;
}
.fcbtn.btn.btn-warning.btn-outline.btn-1b.dropdown-toggle.waves-effect.waves-light {
    margin-left: 80px;
}
</style>
<div class="row el-element-overlay m-b-40">
<div class="displaycount"><span class="counteringdata"> <?= $this->Paginator->counter([
    'format' => ' {{page}} of {{pages}}'
]) ?></span> Pages</div>
      <input type="hidden" value="<?php echo $paginatecount;?>" class="paginate" >
      <div class="col-md-12">
       <div class="col-md-3 searchingdata first">
            </div>

     <div class="col-md-4">
               


          </div>
          <div class="col-md-4">
            
            

     
         
      </div>
      <div class="col-md-1 paginateproject">
         <ul class="pagination paginate-data">
            <li class="previousdata">

                <?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
            </li>
            <li>
                <?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
            </li>
</ul>

    </div>
</div>

     <div class="col-md-12">
 <div class="col-md-3 searchingdata 2nd">
                    

        </div>
        <div class="col-sm-4">
       
        </div>

</div>
   
    <script src="moment.js"></script>
    <script src="moment-timezone-with-data.js"></script>
    <div class="ajax">

      <div class="parent_data">
          <?php
           if(count($resultData)>0){

              $i = 1;
              foreach($resultData as $result):
              $status=$result['status'];
              if($status==2){
                 $class="nav nav-tabs list-group-item-info actioning green";
               }elseif($status == 0){
                 $class ="nav nav-tabs list-group-item-info actioning redish";
                }elseif($status==1){
                $class = "nav nav-tabs list-group-item-info actioning"; 
               }

            $testi_is_enabled = ($result['status'] == 1) ? 'checked': '';?>
              <!-- /.usercard -->
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
                  <div class="white-box">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs firstLISt" role="tablist">
                          <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
                      </ul>
                      <!-- Tab panes  substring($row->parent->category_name,35);  -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane ihome active" >
                             <ul class="list-group ">
                              <li class="list-group-item list-group-item-danger 1"><b>Name:</b> 
                               <?php 
                               $str= $result['Users']['first_name']." ".$result['Users']['last_name'];
                               echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?> </li>
                               <li class="list-group-item list-group-item-success 2"><b>Senior:</b> 
                                  <?php $str=  $result['Seniors']['first_name']." ".$result['Seniors']['last_name'];
                                  echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
                                  <li class="list-group-item list-group-item-info 3"><b>User Comment :</b> 
                                     <?php $str= $result['user_comment'];  
                                      echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?>

                                     </li>
                                     <li class="list-group-item list-group-item-warning 4"><b>Senior Comment :</b> 
                                          <?php $str= $result['senior_comment'];  
                                      echo $name = substr($str,0,8).((strlen($str)>8)? "...":""); ?>

                                         <li class="list-group-item list-group-item-danger 5"><b>Month & Year:</b> <?php 
											$monthNum = $result['month'];
											$monthName = date("F", mktime(0, 0, 0,(int)$monthNum,10));
											echo $monthName.",".$result['year']; ?>
                                      </li>
                                      <input type="hidden" value = "<?php echo $result->encryptedid;?>" class="appicons" />
                                      <input type="hidden" value = "<?php echo $result['month'].$result['year'];?>" class="monthicons" />
                                      <ul class="<?php echo $class;?>" role="tablist">
                                          <li> <?php
                                           echo $this->Html->link(
                                              '<i class="fa fa-cog"></i>',
                                            array('controller'=>'evaluations','action'=>'giveratings','id'=>$result['id']),
                                             ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
                                             );?>
                                         </li>
                                      </ul>
                                 </ul>

                             </div>
                             <div role="tabpanel" class="tab-pane iprofile">
                                <div class="col-md-6">
                                  <h3>Lets check profile</h3>
                                  <h4>you can use it with the small code</h4>
                              </div>
                              <div class="col-md-5 pull-right">
                                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                              </div>
                              <div class="clearfix"></div>
                          </div>
                          <div role="tabpanel" class="tab-pane imessages">
                            <div class="col-md-6">
                              <h3>Come on you have a lot message</h3>
                              <h4>you can use it with the small code</h4>
                          </div>
                          <div class="col-md-5 pull-right">
                              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div role="tabpanel" class="tab-pane isettings">
                        <div class="col-md-6">
                          <h3>Just do Settings</h3>
                          <h4>you can use it with the small code</h4>
                      </div>
                      <div class="col-md-5 pull-right">
                          <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
      <?php $i++ ;
      endforeach; ?>
      <?php } else {
           ?>
            <div class="error-body text-center"><h1></h1><h3 class="text-uppercase">No Record Found !</h3><p class="text-muted m-t-30 m-b-30"></p>  </div>
           <?php
      
    }
    ?>
</div>
<!-- /.usercard-->
</div>
<!-- /.row -->
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
<?php 
echo  $this->Html->script(array('moment'));
?>
<script type="text/javascript">
    function veryfycheck()
    {
      alert("1) Either billing for this project is already added for this week."+'\n'+
          "2) Please verify all billing till previous week for this project and then reload this page again to add billing.");

  }

  (function() {

    [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();
</script>


