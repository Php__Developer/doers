<!--  start content-table-inner -->
  <div class="col-sm-6">
            <div class="white-box">



	<?php echo $this->Form->create($EvaluationsData,array('url'=>['action'=>'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $this->Flash->render(); 
		$session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
	?>

   <div class="form-group">
                    <label for="exampleInputuname">User:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                     <?php
                    $options =  $this->General->getuser_name(); 
				 echo $this->Form->input("user_id",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false));
                      ?>
                    </div>
               <?php if(isset($errors['user_id'])){
							echo $this->General->errorHtml($errors['user_id']);
							} ;?>
                 </div>


		<tr>
		
  <div class="form-group">
                    <label for="exampleInputuname">Senior:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
			<?php
				 $options =  $this->General->getuser_name(); 
				 echo $this->Form->input("senior_id",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false));
				  echo $this->Form->hidden("id");
            ?>

            </div>
 <?php if(isset($errors['senior_id'])){
							echo $this->General->errorHtml($errors['senior_id']);
							} ;?>

    </div>
		
			  <div class="form-group">
                    <label for="exampleInputuname">User Comment:</label>
              <?php
             echo $this->Form->input("user_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","placeholder"=>"Enter here..."));
                     if(isset($errors['user_comment'])){
                                  echo $this->General->errorHtml($errors['user_comment']);
                                  } ;
               
                      ?>  </div>	
  

<div class="form-group"> 
               <label for="inputName" class="control-label">Senior Comment:</label>
				<?php
				 echo $this->Form->textarea("senior_comment",array("class"=>"form-control","label"=>false,"div"=>false)); 
            ?>

            <?php if(isset($errors['senior_comment'])){
							echo $this->General->errorHtml($errors['senior_comment']);
							} ;?>
 </div>

       <div class="form-group">
                       <label for="exampleInputpwd1">Performance_5:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php $options = array(
							'1'=>'1',
							'2'=>'2',
							'3'=>'3',
							'4'=>'4',
							'5'=>'5',
				     );
                       echo $this->Form->select('performance_5',[" "=>$options],['class' => 'form-control'] );?>
                       </div>
                       <?php if(isset($errors['sex'])){
                                    echo $this->General->errorHtml($errors['sex']);
                                } ;?>
                        </div>


			 <div class="form-group">
                       <label for="exampleInputpwd1">Process_5:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                  <?php
				$options = array(
							'1'=>'1',
							'2'=>'2',
							'3'=>'3',
							'4'=>'4',
							'5'=>'5',
				);
				echo $this->Form->select('process_5',[" "=>$options],['class' => 'form-control'] );?>
		 </div></div>


	 <div class="form-group">
                       <label for="exampleInputpwd1">Knowledge_5:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		    <?php
				$options = array(
							'1'=>'1',
							'2'=>'2',
							'3'=>'3',
							'4'=>'4',
							'5'=>'5',
				);
				echo $this->Form->select('knowledge_5',[" "=>$options],['class' => 'form-control'] );?>
</div></div>

      <div class="form-group">
                       <label for="exampleInputpwd1">Discipline_5:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
				$options = array(
							'1'=>'1',
							'2'=>'2',
							'3'=>'3',
							'4'=>'4',
							'5'=>'5',
				);
				echo $this->Form->select('discipline_5',[" "=>$options],['class' => 'form-control'] );?>
          </div>
        </div>


		 
		 <div class="form-group">
                       <label for="exampleInputpwd1">HR activities_5:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				$options = array(
							'1'=>'1',
							'2'=>'2',
							'3'=>'3',
							'4'=>'4',
							'5'=>'5',
				);
			
        	echo $this->Form->select('hractivities_5',[" "=>$options],['class' => 'form-control'] );?>

            </div>
        </div>


        <div class="form-group">
        	<label for="exampleInputpwd1">Month:</label>
        	<div class="input-group">
        		<div class="input-group-addon"><i class="ti-lock"></i></div>
        		<?php
        		$options = $this->General->getMonthsArray();
        		echo $this->Form->select('month',[" "=>$options],['class' => 'form-control'] );?>
        	
        	</div>
        	<?php if(isset($errors['month'])){
        	echo $this->General->errorHtml($errors['month']);
        } ;?>
    </div>




		
		   <div class="form-group">
        	<label for="exampleInputpwd1">Year:</label>
        	<div class="input-group">
        		<div class="input-group-addon"><i class="ti-lock"></i></div>
        		<?php   $curYear 	= date("Y");
						  $options 	= array();
						  $options[''] = "-Select-";
						  while($curYear<='2025'){
						  $options[$curYear] = $curYear;
						  $curYear++;
						  }
        		echo $this->Form->select('year',[" "=>$options],['class' => 'form-control'] );?>
                </div>
					 <?php if(isset($errors['year'])){
							echo $this->General->errorHtml($errors['year']);
							} ;?>
		       </div>



<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'employees','action'=>'add','prefix' => 'employees'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/evaluations/add', true)));?>

 </div>
 </div>
</div>

	<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Evaluations Management</h4>
                       Evaluation used for a number of purposes such that evaluation,planing,control.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'evaluations','action'=>'evaluationslist'), array('style'=>'color:red;'));?>

              </li> 
          

                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>

      <!-- /.right-sidebar -->


</form>
</body>
</html>
		
