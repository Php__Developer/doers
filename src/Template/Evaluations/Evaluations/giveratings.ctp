<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->css(array("oribe")); ?>
<script language="javascript" type="text/javascript">
$(function() {
	$("#performance_5").webwidget_rating_simple({
	rating_star_length: '5',
	rating_initial_value: '<?php echo $EvaluationsData['performance_5']; ?>',
	rating_function_name: '',
	directory: baseurl+'images'
	});
	$("#process_5").webwidget_rating_simple({
	rating_star_length: '5',
	rating_initial_value: '<?php echo $EvaluationsData['process_5']; ?>',
	rating_function_name: '',
	directory: baseurl+'images'
	});
	$("#knowledge_5").webwidget_rating_simple({
	rating_star_length: '5',
	rating_initial_value: '<?php echo $EvaluationsData['knowledge_5']; ?>',
	rating_function_name: '',
	directory: baseurl+'images'
	});
	$("#discipline_5").webwidget_rating_simple({
	rating_star_length: '5',
	rating_initial_value: '<?php echo $EvaluationsData['discipline_5']; ?>',
	rating_function_name: '',
	directory: baseurl+'images'
	});
	$("#hractivities_5").webwidget_rating_simple({
	rating_star_length: '5',
	rating_initial_value: '<?php echo $EvaluationsData['hractivities_5']; ?>',
	rating_function_name: '',
	directory: baseurl+'images'
	});
});
</script> 
<!--  start content-table-inner -->
        <div class="col-sm-6">
            <div class="white-box">

                  <?php echo $this->Flash->render(); ?>

   <?php echo $this->Form->create($EvaluationsData,array('url'=>['action'=>'giveratings'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $this->Flash->render(); 
		$session_info = $session->read("SESSION_ADMIN");
		$userID = $session_info[0];
	?>
        <!-- start id-form -->
              <div class="form-group">
                       <label for="exampleInputpwd1">User*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
    <?php 
				if(!isset($EvaluationsData['Users']['first_name'])){
					$result1 =$this->General->getusername($EvaluationsData['user_id']);
					//pr($result1);
					$username= $result1['Users']['username'];
					//pr($username);
				}else{
				$username=$EvaluationsData['Users']['first_name']." ".$EvaluationsData['Users']['last_name']; 
				}
				echo $this->Form->input("user",array("type"=>"text","id"=>"user_id","label"=>false,"div"=>false,"readonly"=>true,"class"=>"form-control","value"=>$username));
				echo $this->Form->hidden('id');
				echo $this->Form->hidden('user_id');
            ?>
                  </div>
</div>


  <div class="form-group">
                       <label for="exampleInputpwd1">Senior*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                  
                 
            <?php
				if(!isset($EvaluationsData['Seniors']['first_name'])){
					$result2 = $this->General->getusername($EvaluationsData['senior_id']);
					//pr($result2);
					$seniorname= $result2['Users']['user_name'];
				}else{
				$seniorname = $EvaluationsData['Seniors']['first_name']." ".$EvaluationsData['Seniors']['last_name']; 
				}
				
				echo $this->Form->input("senior",array("type"=>"text","id"=>"senior_id","label"=>false,"div"=>false,"readonly"=>true,"class"=>"form-control","value"=>$seniorname));
				echo $this->Form->hidden('senior_id');
			
            ?>

        </div>
    </div>
           <div class="form-group"> 
               <label for="inputName" class="control-label">Senior comment:</label> 
                  
             <?php
             echo $this->Form->input("senior_comment",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","required"=>"required","placeholder"=>"enter the technical comment here .."));
                     if(isset($errors['senior_comment'])){
                                  echo $this->General->errorHtml($errors['senior_comment']);
                                  } ;
                   echo $this->Form->hidden("id");
                      ?>
            </div>
 <div class="form-group">
                       <label for="exampleInputpwd1">Performance_5:</label>
                       <div class="input-group">
            <?php
				echo $this->Form->input("performance_5",array("type"=>"hidden","id"=>"performance_5","label"=>false,"div"=>false));
            ?>
		</div>
		</div>
 <div class="form-group">
                       <label for="exampleInputpwd1">Process_5:</label>
                       <div class="input-group">
			
			<?php
				echo $this->Form->input("process_5",array("type"=>"hidden","id"=>"process_5","label"=>false,"div"=>false));
            ?>
            </div>
            </div>
		 <div class="form-group">
                       <label for="exampleInputpwd1">Knowledge_5:</label>
                       <div class="input-group">
			<?php
				echo $this->Form->input("knowledge_5",array("type"=>"hidden","id"=>"knowledge_5","label"=>false,"div"=>false));
            ?>
		</div>
		</div>
			 <div class="form-group">
                       <label for="exampleInputpwd1">Discipline_5:</label>
                       <div class="input-group">
			<?php
				echo $this->Form->input("discipline_5",array("type"=>"hidden","id"=>"discipline_5","label"=>false,"div"=>false));
            ?>
		</div>
		</div>
	 <div class="form-group">
                       <label for="exampleInputpwd1">HR activities_5::</label>
                       <div class="input-group">
			<?php
				echo $this->Form->input("hractivities_5",array("type"=>"hidden","id"=>"hractivities_5","label"=>false,"div"=>false));
            ?>
	
	</div>
	</div>

             
<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>

 </div>
 </div>
</div>
    <!-- end id-form  -->

    

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Evaluations Management</h4>
                     You can give ratings to user here.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'evaluations','action'=>'monthlyperformance'), array('style'=>'color:red;'));?>

            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     
    
      <!-- /.right-sidebar -->



</body>
</html>
 