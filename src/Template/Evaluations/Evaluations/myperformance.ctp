<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<!--  start content-table-inner -->

<div class="white-box">
          
         



            <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
              <thead>
                <tr>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Month & Year</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">User Comment</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Senior Comment</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">Tickets</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">ROI</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Other</button></th>
                </tr>
              </thead>
              <tbody>
                
        


				<?php 
				$userData = $session->read("SESSION_ADMIN");
				//pr($userData);die;
				$sessionID = $userData[0];
				//pr($sessionID);die;
				$rolesAry = explode(",",$userData[2]);
				if(count($evalData)>0){
				foreach($evalData as $userData):  $class="";  
				?>
				<tr>
					<td><?php echo date("F", mktime(0, 0, 0, $userData ['month'], 10)).",".$userData ['year']; ?> </td>
					<td><?php $str = strip_tags($userData['user_comment']);
						echo substr($str,0,50).((strlen($str)>50)? "...":""); ?> </td>
					<td><?php $str = strip_tags($userData['senior_comment']);
						//echo substr($str,0,50).((strlen($str)>50)? "...":""); 
                                                  echo $str;
						?> <br/>
						<span id="usr_name_1"><b><?php echo $userData['first_name']." ".$userData ['last_name']; ?></b></span>
					</td>
					<td><?php echo "Performance: ".$userData['performance_5']."<br/>";
						echo "Process: ".$userData['process_5']."<br/>";
						echo "Knowledge: ".$userData['knowledge_5']."<br/>";
						echo "Discipline: ".$userData['discipline_5']."<br/>";
						echo "HR activities: ".$userData['hractivities_5'];
					?> </td>
					<?php $aligned = 0; $received = 0; $closedTic =0 ; 
					//pr($resultTicket);die;
					if(count($resultTicket)>0){
					//	pr($resultTicket);die;
						
						foreach($resultTicket as $k=>$v){
							//pr($v);die;
							if($v['to_id']==$sessionID){

								$received++;
								//pr($received);die;
							}elseif($v['from_id']==$sessionID){
								$aligned++;
								//pr($aligned);die;
							}
							if($v['status']==0 &&  $v['last_replier']==$sessionID){
								$closedTic++;
								//pr($aligned);die;
							}
						}
					}?>
				    <td><?php echo "Ticket Aligned: ".$aligned."<br/>";
						echo "Ticket Received: ".$received."<br/>";
						echo "Ticket Closed: ".$closedTic;
					?></td>
					<!--Calculate specified months ROI for a responsible user -->
					<?php $roiSum = "";
					if(count($paymentData)>0){
					foreach ($paymentData as $k=>$v) { 
						$roiSum += ($v['currency']=='inr') ?  $v['amount']/50 : $v['amount'];
						}
					}
					?>
					<td><?php $amount1 = ($roiSum/10) ; 
						if($amount1 < 120) {
						$styleColor = "lessAmount"; } else{
						$styleColor = '';
						} 
						echo "ROI :".$amount1;
					/* 	echo $html->link($amount1,
								array('controller'=>'payments','action'=>'paymentdetails',$userData['Evaluation']['user_id'],'last'),
								array('class'=>'info-tooltip viewPaydetails '.$styleColor,'title'=>'Details','target'=>'_blank')
							); */
						if(in_array('5',$rolesAry)){
							echo "<br/>";
							echo "Bidding Count :".count($biddingCount); 
						}
					?></td>
					<td><?php if($userData['status']==1){
						echo $this->Html->link("Respond",
								array('controller'=>'evaluations','action'=>'userresponse',$userData['id'],'response'),
								array('class'=>'info-tooltip popup_window ','title'=>'Respond')
							);
					}elseif($userData['status']==2){
						echo $this->Html->link("View Details",
								array('controller'=>'evaluations','action'=>'userresponse',$userData['id'],'detail'),
								array('class'=>'info-tooltip popup_window ','title'=>'Details')
							);
					}
					echo "<br/>";
					echo $this->Html->link("View Attendance",
								array('controller'=>'attendances','action'=>'monthlyattendance','prefix' =>'attendances','id'=>$userData['user_id'],'month'=>$userData ['month'],'year'=>$userData['year']),
								array('class'=>'info-tooltip popup_window','title'=>'Attendance Report')
							);
					?></td>
				</tr>
				<?php endforeach; }else{   ?>
				<tr>
					<td colspan="15"> No Records! </td>
				</tr>
				<?php } ?>
				      </tbody>
            </table>
          </div>
