<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $this->Form->create($SkillsData,array('method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php echo $this->Flash->render(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
          <th valign="top">Skills :</th>
			<td>
				<?php echo $this->Form->input("skills",array("type"=>"text","class"=>"inp-form","id"=>"skills","label"=>false,"div"=>false));?>
				<?php if(isset($errors['skills'])){
							echo $this->General->errorHtml($errors['skills']);
				} ;?>
			</td>
		</tr>
		<tr>
          <th valign="top">Tools :</th>
			<td><?php
				 echo $this->Form->input("tools",array("type"=>"text","class"=>"inp-form","id"=>"tools","label"=>false,"div"=>false));
				 echo $this->Form->hidden('id');
            ?>
		  </td>
		</tr>
		<tr>
          <th valign="top">Query :</th>
			<td><?php
				 echo $this->Form->input("query",array("class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
		</tr>
		<tr>
          <th valign="top">Notes:</th>
			<td><?php
				 echo $this->Form->input("notes",array("class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
		</tr>
		<tr>
          <th valign="top">Technical solution :</th>
			<td><?php
				 echo $this->Form->input("technicalsolution",array("class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
		</tr>
		<tr>
          <th valign="top">Comparative study :</th>
			<td><?php
				 echo $this->Form->input("comparativestudy",array("class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $this->Form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $this->Form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/skills/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Skill Management</h5>
         Skill Management . 
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $this->Html->link("Go To Listing",
            array('controller'=>'testimonials','action'=>'list')
            );	
            ?>
            </li> 
  					<li>
            <?php echo $this->Html->link("Download Odesk : Excel",
            array('controller'=>'testimonials','action'=>'exportci','type'=>'odesk')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Odesk : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'pdf')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Elance : Excel",
            array('controller'=>'testimonials','action'=>'exportci','type'=>'elance')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Elance : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'elance')
            );	
            ?>
            </li>
  					
  					<li>
            <?php echo $this->Html->link("Download Other : Excel",
            array('controller'=>'testimonials','action'=>'exportci','type'=>'other')
            );	
            ?>
            </li>
  					<li>
            <?php echo $this->Html->link("Download Other : PDF",
            array('controller'=>'testimonials','action'=>'download','type'=>'other')
            );	
            ?>
            </li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->