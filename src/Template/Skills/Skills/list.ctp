<?php
$newUrl = "list".$urlString;
$urlArray = array(
	'field' 	=> $search1,
	'value' 	=> $search2
);
$this->Paginator->options(array('url'=>$urlArray));
?>
<?php echo $this->Form->create('Skill',array('url' => ['action' => 'list'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $this->Flash->render(); ?>
			<table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
		<tr>
			<td width="14%">
				<b>Search by:</b>
				<?php
				$fieldsArray = array(
				''				  => 'Select',
				'Skills.skills' => 'Skill',
				'Skills.tools' => 'Tools',
				'Skills.notes'  => 'Notes',
				'Skills.query'  => 'Query',
				'Skills.technicalsolution'  => 'Solution',
				);
				echo $this->Form->select("Skill.fieldName",$fieldsArray,['value'=> $search1],array("id"=>"searchBy","label"=>false,"style"=>"width:200px","class"=>"styledselect","empty"=>false),false); ?>
			</td>
			<td width="20%">
				<b>Search value:</b><br/>
				<?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "Skill.status"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $this->Form->input("Skill.value1",array("id"=>"search_input","class"=>"top-search-inp","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
				?>
			</td>
			<td width="40%"><br/>
		  		<?php
				echo $this->Form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
				 echo $this->Html->link("Reset",
                ['controller'=>'skills','action'=>'list','prefix' => 'skills'],['class'=>"form-reset"]); 
				 $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/skills/list', true)));				
				?>
			</td>
		</tr>
	</table>
		<br/>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-repeat line-left "  width="15%"><?php echo $this->Paginator->sort('skills','Skills');?></th>
					<th class="table-header-repeat line-left "  width="15%"><?php echo $this->Paginator->sort('tools', 'Tools');?></th>		
					<th class="table-header-repeat line-left "  width="20%"><?php echo $this->Paginator->sort('notes', 'Notes');?></th>					
					<th class="table-header-repeat line-left" width="20%"><?php echo $this->Paginator->sort('query', 'Query');?></th>
					<th class="table-header-repeat line-left "  width="20%"><?php echo $this->Paginator->sort('technicalsolution', 'Technical Solution');?></th>			
					<th class="table-header-repeat line-left" width="10%"><a href="#A">Options</a></th>
				</tr>
				<?php if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $result): ?>
				<tr>
					<td class="type-view"><?php $str = strip_tags($result['skills']); echo substr($str,0,15).((strlen($str)>15)? "...":"");?></td>
					<td><?php $str = strip_tags($result['tools']); echo substr($str,0,15).((strlen($str)>15)? "...":"");  ?></td>
					<td class="type-view"><?php $str = strip_tags($result['notes']);  echo substr($str,0,22).((strlen($str)>22)?"...":""); ?></td>
					<td><?php  $str = strip_tags($result['query']);  echo substr($str,0,22).((strlen($str)>22)?"...":"");  ?></td>
					<td><?php  $str = strip_tags($result['technicalsolution']);  echo substr($str,0,22).((strlen($str)>22)?"...":"");  ?></td>
					<td class="options-width" align="center">
						<?php
						echo $this->Html->link("",
						array('controller'=>'skills','action'=>'edit',$result['id']),
						array('class'=>'icon-1 info-tooltip','title'=>'Edit')
					);
						?>
						<?php
							echo $this->Html->link("",
						array('controller'=>'skills','action'=>'delete',$result['id']),
						array('class'=>'icon-2 info-tooltip delete','title'=>'Delete')
					);
						?>
					</td>
				</tr>
				<?php $i++ ;
				endforeach; ?>
				<?php } else { ?>
		<tr>
			<td colspan="6" class="no_records_found">No records found</td>
		</tr>
			<?php } ?>
				</table>
				<!--  end product-table................................... --> 
			</div>
			<!--  end content-table  -->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>

				     <td>
				            <?php  echo $this->Paginator->prev('<span class="page-left" ></span>', ['escape' => false] );?>
                                <div id="page-info">
                                    <?php  echo $this->Paginator->counter( 'Page {{page}} / {{pages}}');
                                     echo $this->Paginator->next('<span class="page-right" ></span>', ['escape' => false] ); ?>
                                </div>
                        </td>


			
			</tr>
			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>

	</td>
	<td>

	<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Skill Management</h5>
          Skills Management _____________________________. 
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $this->Html->link("Add New Skill",
            array('controller'=>'skills','action'=>'add')
            );	
            ?>
            </li> 
  					
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->

</td>
</tr>
</table>
 
<div class="clear"></div>
 

</div>
<?php echo $this->Form->end(); ?>
<!--  end content-table-inner  -->