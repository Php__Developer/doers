<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<?php echo $this->Html->css(array('oribe')); ?>
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core ?>
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
<script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
<script type="text/javascript">

</script>
<?php echo $this->Html->css(array('oribe')); ?>
  <div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-top alerttop2" style="display: none;">
<center><h4 class="error_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-success myadmin-alert-top alerttop1" style="display: none;">
<center><h4 class="success_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop ng-binding successwrapperdv"  style="">
    <span><i class="fa fa-thumbs-up "></i> <span class="successmsg"></span></span>
  </div>
  
	 <div class="col-sm-12">
    <input type="hidden" name="tagid" class="tagid" value="<?php echo $this->General->encForUrl($this->General->ENC($tag['id'])) ;?>">
    <input type="hidden" name="currentpage" class="currentpage" value="tagscontent">
    <a href="#" class="cls_host pull-right"><i class="fa fa-times"></i></a>
      <div class="white-box">
      <h3 class="box-title text-center">Manage Tag Content For <?php echo $tag['name'];?></h3>
      <div class="col-md-12">
           <div class="col-md-6">
            <h4 class="">Content Keywords</h4>
          </div>
          <div class="col-md-6">
            <span class="col-md-6 pull-right contentdetailsbtn"><span class="whattodo">View</span> Details</span>
          </div>
         <div class="contentdetails default_hidden">
              <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <h3 class="box-title">Keyword</h3>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <h3 class="box-title">OutPut</h3>
          </div>
          <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{date,FieldName, yes/no}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Date Input</small>
          </div>
          <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{date-time,FieldName,yes/no}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Date And Time</small>
          </div>
          <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{nput,FieldName,yes/no}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Text Input</small>
          </div>
          <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{textarea,FieldName,yes/no}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Big Text-Box</small>
          </div>
          <div class="col-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{userslist,FieldName,yes/no}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>All Users-List</small>
          </div>
          <div class="ccol-md-7 col-sm-7 col-lg-7 col-xs-7">
            <small>{{current-user}}</small>
          </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Print Current User-name</small>
          </div>
      <div class="ccol-md-7 col-sm-7 col-lg-7 col-xs-7">
        <small>{{radio,FieldName,Yes,[Field1|Field2]}}</small>
      </div>
      <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
        <small>Radio Buttons</small>
      </div>
    <div class="ccol-md-7 col-sm-7 col-lg-7 col-xs-7">
        <small>{{checkbox,FieldName,no,labelvalue}}</small>
      </div>
          <div class="col-md-5 col-sm-5 col-lg-5 col-xs-5">
            <small>Checkbox</small>
          </div>

         </div> 
      	

      </div>
      
       <div class="form-group m-t-20">
       		<h3 class="box-title">Tag Content</h3>
                  <!-- <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea> -->
                <?php
                $content = json_decode($tag['on_ticket']);
                 echo $this->Form->textarea('description', array("type"=>'text',"class"=>"tagscontent form-control ", 'id' => 'desc',"label"=>false,"div"=>false,'placeholder' => "",'rows'=>"20",'value' => $content->content)); 
                ?>
                <div class="errors">
                	
                </div>
       </div>
       <button type="submit" class="btn btn-primary addcontent"><i class="fa fa-envelope-o"></i> Submit</button>
       <button class="btn btn-default goback"><i class="fa fa-mail-reply"  ></i> Cancel</button>
      </div>
</div>



     <!-- /.right-sidebar -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Menu Plugin JavaScript -->
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/tagnew.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

