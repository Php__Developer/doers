  <div class="col-sm-6">
            <div class="white-box">
              <?php echo $this->Form->create('entities',array('url' => ['action'=>'add'],'method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
              <?php echo $this->Flash->render(); ?>
              <div class="form-group">
                <label for="exampleInputuname">Tag Name*:</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="ti-wallet"></i></div>
                  <?php
                  echo $this->Form->input("name",array("class"=>"form-control","label"=>false,"div"=>false));
                  ?>
                </div>
                <?php if(isset($errors['name'])){
                echo $this->General->errorHtml($errors['name']);
                } ;?>
              </div>
              <div class="form-group">
                <label for="exampleInputuname">Alias*:</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="ti-wallet"></i></div>
                  <?php
                  echo $this->Form->input("alias",array("class"=>"form-control","label"=>false,"div"=>false));
                  ?>
                </div>
                <?php if(isset($errors['alias'])){
                echo $this->General->errorHtml($errors['alias']);
                } ;?>
              </div>
              <div class="form-group">
                       <label for="exampleInputpwd1">Agencies*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('agency_id',$agencies,['class' => 'form-control agency_id' , 'multiple' => true] );?>
                      </div>
                      <span class="help-block">Please select Agency under which you want to implement this Module/Sub-module</span>
                      <?php if(isset($errors['agency_id'])){
                      echo $this->General->errorHtml($errors['agency_id']);
                      } ;?>
                      <div class="agency_id_error"></div>
              </div>
              <div class="form-group">
                       <label for="exampleInputpwd1">Roles*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('role_id',$roles,['class' => 'form-control role_id','id' => '', 'multiple' => true] );?>
                      </div>
                      <span class="help-block">Please select Roles that can access this Module/Sub-module</span>
                      <?php if(isset($errors['role_id'])){
                      echo $this->General->errorHtml($errors['role_id']);
                      } ;?>
                      <div class="role_id_error"></div>
              </div>
              <div class="form-group">
                       <label for="exampleInputpwd1">Entities</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('entity_id',$entities,['class' => 'form-control entity_id' , 'multiple' => true] );?>
                      </div>
                      <span class="help-block">Please select entities to which that tag will be attached</span>
                      <?php if(isset($errors['entity_id'])){
                      echo $this->General->errorHtml($errors['entity_id']);
                      } ;?>
                      <div class="agency_id_error"></div>
              </div>
              <div class="form-group">
                <label for="exampleInputuname">Notes:</label>
                <?php echo $this->Form->textarea("notes",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['notes'])){
                echo $this->General->errorHtml($errors['notes']);
                } ;?>
              </div>
               <!-- <div class="form-group">
                <label for="exampleInputuname">Entity Id:</label>
                <?php   $this->Form->select('entity_id',[" "=>$entities],['class' => 'form-control'] );?>
                <?php if(isset($errors['entity_id'])){
                echo $this->General->errorHtml($errors['entity_id']);
                } ;?>
              </div> -->
              <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
                <?php echo $this->Html->link("Reset",  ['controller'=>'tags','action'=>'add','prefix' => 'tags'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
                $this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/tags/list', true)));?>
              </div>
           </div>
  </div>

	<div class="col-sm-6">
      <div class="white-box">
           <div class="form-group">
              <div class="row el-element-overlay m-b-40">
                  <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                      <h4 class="media-heading">Tags Management</h4>
                      This section is used by Admin  to Manage Tags.
                      <ul >
                        <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'tags','action'=>'list'), array('style'=>'color:red;'));?></li> 
                      </ul>
                    </nav>
                  </div><!-- /tabs -->
              </div>   
            </div>
      </div>
</div>
      <!-- /.right-sidebar -->
<!-- </form>
</body>
</html> -->
		

	