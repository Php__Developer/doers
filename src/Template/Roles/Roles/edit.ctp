	<!--  start content-table-inner -->
  <div class="col-sm-6">
            <div class="white-box">

	<?php echo $this->Form->create($RolesData,array('url' => ['action'=>'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
<?php echo $this->Flash->render(); ?>
	
   <div class="form-group">
                    <label for="exampleInputuname">Role:</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
			<?php
				 echo $this->Form->input("role",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
  <?php if(isset($errors['role'])){
							echo $this->General->errorHtml($errors['role']);
							} ;?>
            </div>
		


		 <div class="form-group">
                    <label for="exampleInputuname">Description:</label>

			<?php
				 echo $this->Form->input("description",array("class"=>"form-control","label"=>false,"div"=>false));
				 echo $this->Form->hidden("id");
            ?>
  <?php if(isset($errors['description'])){
							echo $this->General->errorHtml($errors['description']);
							} ;?>
         </div>

  
			<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'roles','action'=>'edit','prefix' => 'roles','id'=>$RolesData['id']],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/roles/edit', true)));?>

 </div>
 </div>
</div>

	<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Roles Management</h4>
                            This section is used by Admin and PM only to Manage senstive roles.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'roles','action'=>'list'), array('style'=>'color:red;'));?>

              </li> 
          

                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>

      <!-- /.right-sidebar -->


</form>
</body>
</html>
		

	