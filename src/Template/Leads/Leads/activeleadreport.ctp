<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->css(array("screen")); ?>
<?php echo $this->Html->script('highcharts'); ?>	
<?php  echo $this->Html->script('modules/exporting');?>
<script type="text/javascript">
var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			  
		 });
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
		 });
	});
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
		
	}
	/*
  *---------------Java script code for date calculation----------------*
  */
 function date_calculation()
  {
  var selectedType=document.getElementById("selectdate");
  var selecteddate = selectedType.options[selectedType.selectedIndex].value;
  var today=new Date();
  var today2=today.toISOString().substr(0,10);
  if(selecteddate=="month")
  {
   var monthsum= new Date(new Date(today).setMonth(today.getMonth()-1));
   var monthsum2=monthsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=monthsum2;
   document.getElementById("enddate").value=today2;
   
  }
  else if(selecteddate=="year")
  {
   var yearsum= new Date(new Date(today).setMonth(today.getMonth()-12));
   var yearsum2=yearsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=yearsum2;
   document.getElementById("enddate").value=today2;
  }
   else if(selecteddate=="week")
  {
   var yearsum= new Date(new Date(today).setDate(today.getDate()-6));
   var yearsum2=yearsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=yearsum2;
   document.getElementById("enddate").value=today2;
  }
  else if(selecteddate=="today")
  {
   document.getElementById("startdate").value=today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="select")
  {
   document.getElementById("startdate").value =today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="custom")
  {
   document.getElementById("startdate").value ='';
   document.getElementById("enddate").value ='';
  }
  }

 <!------------------Implementing chart----------------------> 
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Number of Active Leads by users(in %)'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color:'black'
//						(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
	
//---------------------------Fetching data for chart------------------------------//
	<?php
		$result=array();
				
		foreach($resultData as $record)
		{	
		foreach($record as $records)
			{
		foreach($records as $key)
					{
			$result[]=$key;
				
				}
			}
		}
$resultname=array();
$resultvalue=array();
	for($i=0;$i<count($result);$i++)
		{
		if($i==0 || $i%2==0)
			{
			$resultname[]=$result[$i];
			}
		else
			{
			$resultvalue[]=$result[$i];
			}
		}	
?>		
        series: [{
            type: 'pie',
            name: 'Leads',
			data: [										//passing data here
			<?php for($i=0;$i<count($resultname);$i++){ ?>
                [<?php echo "'$resultname[$i]'";?>,<?php echo "$resultvalue[$i]"; ?>],
			<?php }	?>
				
                
                
            ] 
            
        }]
    });
});
    

		</script>
<h1 align="center">No of Active Leads (User Wise)</h1>
<?php echo $this->Form->create('Lead',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm();')); ?>
<div id="content-table-inner">
<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
				<tr>
			<td width="15%">
		<div style="float:left;">
			 <?php
 //  App::import('Component', 'common');
  // $this->Common= new CommonComponent;
 $options = $this->General->getWeekRange();
echo $this->Form->input("select",array("type"=>"select","class"=>"top-search-inp","options"=>$options,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"selectdate",'onChange'=>'date_calculation();'));
 ?>
  </td>
	<th align="left" ><b>From:&nbsp;</b></th>
	<td>
		<div style="float:left;">
		<?php 
  if($fromDate!=""){
  $setdate=date('Y/m/d',strtotime($fromDate));
  }else{
   $current=date("Y-m-d");
			$current = strtotime($current);
			$current = strtotime("-1 week", $current);
			$setdate=date('Y-m-d', $current);
  }
  if($toDate!=""){
  $todate=date('Y/m/d',strtotime($toDate));
  }else{
  $todate=date('Y/m/d');
  }
  ?>
  <?php	
	?>
<?php  echo $this->Form->input("start_date",array("type"=>"text","id"=>"startdate","class"=>"top-search-inp","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($setdate)))); ?>	 </td>
<th align="left" ><b>To:&nbsp;</b></th>
<td width="30%">
<?php  echo $this->Form->input("end_date",array("type"=>"text","id"=>"enddate","class"=>"top-search-inp","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($todate)))); ?>	 </td>
	</div>
<div style="clear:both"></div>
	</div>
	</td>
	<th><b>User:&nbsp;</b></th>
	<td width="30%">
				<div style="float:left;">
				<div style="float:left;">
				<?php
					$options = $this->General->getUsersByRole('5'); 
					echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"top-search-inp","id"=>"usersid",'options'=>$options,"label"=>false,"div"=>false,"style"=>"width:150px;"));		
					?>
				</div><div style="clear:both"></div>
			</div>
		</td>
	<br/>
<td width="40%">
<div style="">
	<?php echo $this->Form->submit("Submit", array('class'=>'form-search','id'=>'search','method'=>'POST',"id" => "mainform3","name" => "listForm3"));  ?>
	</div>
			</td>
		</tr>
	</table>
<br/>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
	<div class ="lead_usr">
	<div style="clear:both"></div>
</div>
	<?php echo $this->Form->end();?>
	<!--------------------------------Showing Data------------------------------------->
	<?php
		$result=array();
			foreach($resultData as $record)
				{	
				foreach($record as $records)
					{
					foreach($records as $val)
						{
			$result[]=$val;
				}
			}
		}
		$resultname=array();
		$resultvalue=array();
			for($i=0;$i<count($result);$i++)
				{
					if($i==0 || $i%2==0)
						{
			$resultname[]=$result[$i];
				}
			else
				{
			$resultvalue[]=$result[$i];
			}
		}
?>
<div>
<?php  if(isset($resultData) && count($resultData)>0){?>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<div id="table-content">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
<thead>
<tr>
<th class="heading1" style="color:white;"><h4><b>Name</b></h4></th>
<th class="heading1" style="color:white;"><h4><b>No.of Leads</b></h4></th>
</tr>
</thead>
 <?php for($i=0;$i<count($resultname);$i++){ ?>
    <tbody>
		<tr class>
		<td class="wrapping">
		<?php echo "$resultname[$i]";?>
	</td>
		<td class="wrapping">
		<?php   echo "$resultvalue[$i]"; ?>
	</td>
</tr>
</tbody>
		<?php } ?>
</table>
<!-----------------------------------Inserting graph in form here------------------>
		<?php if(empty($this->request->data['user_id'])){ ?> 
		<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<div class="contentbox">
					<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
				</div>
			</div>
		</table>
		<?php } else { ?>
		<div class="contentbox">
				</div>
				<?php } ?>
<!--  end Graph-................................... --> 
	<div class="clear"></div>
</div>
				<?php } else { ?> 
		<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<tbody>
					<tr>
						<td colspan="13" class="no_records_found">No records found</td>
					</tr>
		<?php } ?>
			</table>
	<div class="clear"></div>
</div></div></div>