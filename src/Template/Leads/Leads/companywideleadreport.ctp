
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">


<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>

<?php echo $this->Html->script('highcharts'); ?>	
<?php  echo $this->Html->script('modules/exporting');?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy-mm-dd',
			  //minDate: 0,
			  
		 });
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy-mm-dd',
			 // minDate: 0,
		 });
	});
 function validateForm(){
		
		if(($('#year2').val()!="" && $('#year1').val()=="") || ($('#year2').val()=="" && $('#year1').val()!="")){
			var message = "<div>Enter both start and end year</div>";
			$('#error').html(message);
			return false;
		}else if(($('#year2').val()!="" && $('#year1').val()!="")){
			var str_date = $('#year1').val();
			var end_date = $('#year2').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start year should be less than end year</div>";
				$('#error').html(message);
				return false;
			}
		}
		
	}
	</script>


	<?php		$month_name=array();
				$lead_status=array();
				$status_count=array();
				$totalLeads=array();
				$graphdata =[];
				foreach($resultData as $record)
				{
					$month_name[]=$record['bidding_date']->format("M");
					$lead_status[]=$record['status'];
					$status_count[$record['bidding_date']->format("M")][$record['status']]=$record['NoOfLeads'];
					$totalLeads[$record['bidding_date']->format("M")]=   $totalLeads[$record['bidding_date']->format("M")]+$record['NoOfLeads'];
				}
			
				$month_name=array_values(array_unique($month_name));
				$lead_status=array_values(array_unique($lead_status));
				foreach($month_name as $month){
				foreach($lead_status as $status){
				if(!empty($status_count[$month][$status])){
					$graphdata[$status][$month]=	$status_count[$month][$status];
				}else{
					$graphdata[$status][$month]	=0;
				}

	
				
				}
				}
				
				$databcv="";
				foreach($graphdata as $key=>$val){
				
						$databcv .= "{
						name: '".$key."',
						data: [".implode(',', $val)."]
					},";
				}
				

		$month_name2=array();
		foreach($resultData as $record)
		{	if(!in_array(date("M Y",strtotime($record['bidding_date'])),$month_name2))
		
		$month_name2[]=date("M Y",strtotime($record['bidding_date']));
		}
	
?>	



<script type="text/javascript">
$(function () {
        $('#container').highcharts({
             title: {
                text: 'Company Wide Line Graph - (Month Wise) ',
                x: 0 //center
            },
            
		
            xAxis: {
				
                categories: [<?php for($name=0;$name<count($month_name2);$name++){
									echo "'$month_name2[$name]',";
								}?>]
				  },
             yAxis: {
                title: {
                    text: 'NO. Of Lead(s)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ' Leads'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
				   },
			series:[<?php echo "$databcv";?>
					{
                name: 'Total Leads',
				 data: [<?php foreach($totalLeads as $totalleads)
						{	
						echo "$totalleads,";
						}
				 ?>]}
			
			]
			
			
        });
    });
    

</script>	
<h1 align="center">Company Wide Line Graph - Month Wise</h1>
<?php echo $this->Form->create('Lead',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform","onsubmit"=>"return validateForm();")); ?>
<div id="content-table-inner">
	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<div class="searching_div">
		<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
			<tr>
				<td width="7%">
				<div style="padding-left;">
				<div style="float:left;">
				<div style="float:left;margin-right:50px">	<b> Employee Name:</b></div></td>
				<td width="40%">
							<?php
					 $options = $this->General->getUsersByRole('5'); 
					echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"form-control","id"=>"usersid",'options'=>$options,"label"=>false,"div"=>false,"style"=>"width:150px;"));		
					?>	</div>
				<div style="clear:both"></div>
				</div>
				</td><br/>
				</tr>
			</table><br/>
				<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
				<tr>
				<td width="7%">
				<br>
					<div>
					<div style="float:left">
					<span style="vertical-align: middle;">
					<b>From:</b>
					</span>
					</div>
				</td>
				<td width="30%">
					<div style="float:left">
					<b>Month:</b>
					<br>
							<?php // App::import('Component', 'common');
						//$this->Common= new CommonComponent;
						$options = $this->General->getMonthsArray();
						if($start_month!=""){
						  $currentMonth=$start_month;
						  }else{
						   $currentMonth=date('m');
							}
						echo $this->Form->input("month1",array("type"=>"select","class"=>"form-control","options"=>$options,'selected'=>$currentMonth,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"month1"));
					?>
					</div>
					<div style="float:left">
					<b>Year:</b>
					<br>
					<?php
						
						if($start_year!=""){
						  $currentYear=$start_year;
						  }else{
						   $currentYear=date('Y');
							}
							$curYear 	= "2013";
						  $array 	= array();
						  $array[''] = "-Select-";
						  while($curYear<='2025'){
						  $array[$curYear] = $curYear;
						  $curYear++;
						  }
						 
						  echo $this->Form->input("year1",array("type"=>"select","class"=>"form-control","options"=>$array,'selected'=>$currentYear,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"year1"));
					?>
					</div>
					</div>
				</td>
				<td width="5%">
				<br>
				<div>
				<div style="float:left">
					<span style="vertical-align: middle;">
					<b>To:</b>
					</span>
					</div>
				</td>
				<td width="	30%">
					<div style="float:left">
					<b>Month:</b><br/>
					<?php // App::import('Component', 'common');
						//$this->Common= new CommonComponent;
						$options = $this->General->getMonthsArray();
					if($end_month!=""){
						  $currentMonth=$end_month;
						  }else{
						   $currentMonth=date('m');
							}
						echo $this->Form->input("month2",array("type"=>"select","class"=>"form-control","options"=>$options,'selected'=>$currentMonth,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"month2"));
					?>
					</div>
					<div style="float:left">
					<span style="">
					<b>Year:</b></span><br/>
					<?php 
							if($end_year!=""){
						  $currentYear=$end_year;
						  }else{
						   $currentYear=date('Y');
							}
						  $curYear 	= "2013";
						  $array 	= array();
						  $array[''] = "-Select-";
						  while($curYear<='2025'){
						  $array[$curYear] = $curYear;
						  $curYear++;
						  }
						  echo $this->Form->input("year2",array("type"=>"select","class"=>"form-control","options"=>$array,'selected'=>$currentYear,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"year2"));
					?>
					</div>
					</div>
					</td>
<td width="10px">
				
									<div style="padding-left:90px;">
						<?php echo $this->Form->submit("Search", array('class'=>'btn btn-success','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
						</div>
				</td>
			</tr>
		</table>
		<br/>
	
			<div style="color:red;" id="error"></div>
		</div>
				<div class ="lead_usr">
				<div style="clear:both"></div>
		</div>
		</div>
				<?php echo $this->Form->end(); ?>
	
<div>
<?php

 if(isset($resultData) && count($resultData)>0){ 



 	?>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<div id="table-content">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
<thead>
<tr>
<th class="heading1" style="color:black;"><h4><b>Status</b></h4></th>
<th class="heading1" style="color:black;"><h4><b>No.of Leads</b></h4></th>
<th class="heading1" style="color:black;"><h4><b>Date </b></h4></th>
</tr>
</thead>
 <?php foreach($resultData as $record)
				{ ?>
        <tbody>
		<tr >
		<td class="wrapping"><?php echo $lead_status[]=$record['status'];?> </td>
		<td class="wrapping"><?php echo $record['NoOfLeads']; ?> </td>
		<td class="wrapping"><?php echo $record['bidding_date']->format("M Y"); ?> 
            </td></tr>
		
		<?php } ?>

<tbody>
	<td class="wrapping">
	<font color="#FF6347"><b>Total Bids</b></font></td>

	<td colspan="3"class="wrapping">
		<?php echo $add=array_sum($totalLeads);?>
		</td>
	</tr>
	</tbody>
</table>
<!-----------------------------------Inserting graph in form here-------------------->
		<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<div class="contentbox">
					<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
				</div>
			</div>
		</table>
<!--  end Graph-................................... --> 
	<div class="clear"></div>
</div>
				<?php



				$month_name=array();
				$lead_status=array();
				$status_count=array();
				$totalLeads=array();
				$graphdata=array();
				foreach($resultData as $record)
				{

				$month_name[]=$record['bidding_date']->format("M");
              $lead_status[]=$record['status'];
				$status_count[$record['bidding_date']->format("M")][$record['status']]=$record['NoOfLeads'];

             // $totalLeads[$record['bidding_date']->format("M")] = $totalLeads[$record['bidding_date']->format("M")]+$record['NoOfLeads'];

             }

			$month_name=array_values(array_unique($month_name));
				$lead_status=array_values(array_unique($lead_status));
				
				foreach($month_name as $month){
				foreach($lead_status as $status){


if(!empty($status_count[$month][$status])){
      $graphdata[$status][$month]=	$status_count[$month][$status];

}else{
    $graphdata[$status][$month]	=0;
}
	
				}
				}
				
			
				$databcv="";
			
							foreach($graphdata as $key=>$val){
				
						$databcv .= "{
						name: '".$key."',
						data: [".implode(',', $val)."]
					},";
				}
			//echo $add=array_sum($totalLeads);	
			
	

				} else { ?> 
		<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<tbody>
					<tr>
						<td colspan="13" class="no_records_found">No records found</td>
	
					</tr>
		<?php } ?>
			</table>
	<div class="clear"></div>
</div></div></div>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

    <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
