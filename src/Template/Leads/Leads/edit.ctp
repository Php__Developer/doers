<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>

<script type="text/javascript">
$(function() {
    $("#bidding_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
    $("#nextfollow_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
   

});
</script>
<?php echo $this->Html->css(array('oribe')); ?>
	<!--  start content-table-inner -->
	 <div class="col-sm-6">
            <div class="white-box">


	<?php echo $this->Form->create($PaymentsData,array('url' => ['action' => 'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php  echo $this->Flash->render(); ?>
	<div class="form-group">
              <label for="exampleInputpwd1">Bidding Date*</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php  
				$bid= date_create($result['bidding_date'])->format('d-m-y');
				echo $this->Form->input("bidding_date",array("type"=>"text","class"=>"form-control","id"=>"bidding_date","label"=>false,"div"=>false,'value' => $date));
            ?>
            </div>
             <?php if(isset($errors['bidding_date'])){
							echo $this->General->errorHtml($errors['bidding_date']);
				} ;?>
            </div>






		
                 <div class="form-group">
                       <label for="exampleInputpwd1">URL / Title*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
           <?php
				echo $this->Form->input("url_title",array("type"=>"text","class"=>"form-control","label"=>false,"div"=>false));
            ?> </div>
            <?php if(isset($errors['url_title'])){
							echo $this->General->errorHtml($errors['url_title']);
				} ;?>
            </div>



	<div class="form-group">
                       <label for="exampleInputpwd1">Technology:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
			$options=array('php' =>  'PHP', 
										'phone'    => 'Phone',
										'microsoft'    => 'Microsoft',
										'design'  => 'Design',
										'seo'   => 'SEO'
                      );
				echo $this->Form->input("technology",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            </div>



         <div class="form-group">
                       <label for="exampleInputpwd1">Source:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
			$options=array('upwork' =>  'Upwork', 
										'elance'    => 'Elance',
										'guru'    => 'Guru',
										'freelancer'  => 'Freelancer',
										'phone'   => 'Phone',
										'direct'    => 'Direct',
										'email'    => 'Email',
										'client reference'  => 'Client Reference',
										'engineering'   => 'Engineering'
                      );
				echo $this->Form->input("source",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            </div>



      <div class="form-group">
                       <label for="exampleInputpwd1">Category:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
             <?php
			$options=array('s' =>  'S', 
										's&m'    => 'S&M',
										'm&l'    => 'M&I',
										'I'    => 'I'
                      );
				echo $this->Form->input("category",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            ?>
</div></div>



	<div class="form-group">
                       <label for="exampleInputpwd1">Profile On:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
           <?php
			$session_info = $session->read("SESSION_ADMIN");
				$userID = $session_info[0];
				//pr($userID); die;
				$option = $this->General->getProfilesForLead($userID);
				//pr($option);die();
			echo $this->Form->input("profile_id",array("type"=>"select",'options'=>$option,"id"=>"profile_id","class"=>"form-control","label"=>false,"div"=>false));
            	
		?>
       </div>
   </div>





<div class="form-group">
                       <label for="exampleInputpwd1">Perspective amount:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
				echo $this->Form->input("perspective_amount",array("class"=>"form-control","label"=>false,"div"=>false));
		?>
		</div>
		<?php if(isset($errors['perspective_amount'])){
							echo $this->General->errorHtml($errors['perspective_amount']);
				} ;?>
	
</div>







	  <div class="form-group">
                       <label for="exampleInputpwd1">Status:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
			$options=array('cold' =>  'cold', 
										'active'    => 'active',
										'deny'    => 'deny',
										'closed'    => 'closed',
										'success'    => 'success'
                      );
				echo $this->Form->input("status",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            ?>
</div>
</div>










	 <div class="form-group">
                       <label for="exampleInputpwd1">Next-follow Date:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
 
         <?php
				//$next= date_create($result['nextfollow_date'])->format('Y-m-d');
				echo $this->Form->input("nextfollow_date",array("type"=>"text","class"=>"form-control","id"=>"nextfollow_date","label"=>false,"div"=>false,'value'=> $datenxt));
            ?>
</div>
</div>










 <div class="form-group">
                       <label for="exampleInputpwd1">Time Spent:</label><span style="color:red" class="pull-right"> e.g. 40:18  </span>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
				echo $this->Form->input("time_spent",array("type"=>"text","class"=>"form-control","label"=>false,"div"=>false));
            ?>
            </div>
            <?php if(isset($errors['time_spent'])){
							echo $this->General->errorHtml($errors['time_spent']);
				} ;?>

           </div>




	  <div class="form-group"> 
               <label for="inputName" class="control-label">Notes:</label>
<?php
		 echo $this->Form->input("notes",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
				 echo $this->Form->hidden("id");
		?>
			
</div>
			
        <div class="form-group">
          <label for="exampleInputpwd1">Is Invalid:</label>
             <div class="input-group">
           <?php
				echo $this->Form->input("is_invalid",array("type"=>"checkbox","label"=>false,"div"=>false));
            ?>
           </div>
         </div>

	<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'leads','action'=>'edit','prefix' => 'leads'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/leads/edit', true)));?>

 </div>
 </div>
</div>



<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Leads Management</h4>
          This section is used by User only to Manage senstive leads.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'leads','action'=>'leadslist'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     <!-- /.right-sidebar -->
</body>
</html>
