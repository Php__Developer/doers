 <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">



<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>

<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function() { 
			
			$( "#nextfollow_date" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
			
				$( "#bidding_date" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$('#is_invalid').checkbox_value();
		$('#ui-datepicker-div').attr('style','display:none');
	});
	
	(function( $){
		  $.fn.checkbox_value = function() {
			$(this).click(function() {
					if($(this).is(":checked")){
						$(this).val('1');
					}else{
						$(this).val('0');
					}
				});
		  };
	})( jQuery );
	function postData(){
		if(notEmpty($('#bidding_date').val(),'error_msg1','Enter bidding date'))
		{ return false; }
		if(notEmpty($('.url_title').val(),'error_msg2','Enter url title'))
		{ return false; }
		if(!($('#p_amount').val().match(/^(?:\d+|\d*\.\d+)$/)) && $('#p_amount').val() !=""){
					$('#error_msg3').html("Enter a perspective amount");
					$('#error_msg3').attr('style','display:block;color:red;font-size:13px;');
					setTimeout(function() { $('#error_msg3').fadeOut('slow'); }, 3000);
					return false;
				}
		if(!$('#time_spent').val().match(/^[0-9]{1,2}(:[0-9]{2})?$/i) && $('#time_spent').val() !=""){
					$('#error_msg4').html("Enter valid time spent");
					$('#error_msg4').attr('style','display:block;color:red;font-size:13px;');
					setTimeout(function() { $('#error_msg4').fadeOut('slow'); }, 3000);
					return false;
				}
		var post_data = { 'id' : $('#lead_id').val(),
									'bidding_date' : $('#bidding_date').val(),
									'url_title' : $('.url_title').val(),
									'technology' : $('#technology').val(),
									'source' : $('#source').val(),
									'category' : $('#category').val(),
									'salesfront_id' : $('#salesfront_id').val(),
									'perspective_amount' : $('#p_amount').val(),
									'status' : $('#statuss').val(),
									'nextfollow_date' : $('#nextfollow_date').val(),
									'time_spent' : $('#time_spent').val(),
									'notes' : $('#notes').val(),
									'is_invalid' : $('#is_invalid').val(),
									'profile_id':$('#profile_id').val()
								};
		if($.active>0){
		}else{

			var base_url = "<?php echo $this->Url->build('/', true); ?>";
			$.post(base_url+'leads/quickedit',post_data,function(data){
				//console.log(data);
				if(data==1){
					///window.parent.$('#close-fb').click();
					 window.parent.location.href = window.parent.location.href;
				}
			});
		}
	}
	// not null validation function
	function notEmpty(val,errorID,msg){
		if(val == ""){
			$('#'+errorID).html(msg);
			$('#'+errorID).attr('style','display:block;color:red;font-size:13px;');
			setTimeout(function() { $('#'+errorID).fadeOut('slow'); }, 5000);
			return true;
		}
	}
	function deleteRecord(id){

		var base_url = "<?php echo $this->Url->build('/', true); ?>";
		$.post(base_url+'leads/quickview',{'id':id,'key':'delete'},function(data){
			if(data==1){
				$('#bidding_date'+id).closest("tr").remove();
			}
		});
	}
	function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}
</script>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	

<?php echo $this->Form->create($leadData,array('url'=>['action'=>'quickedit'],'method'=>'POST','onsubmit' => '',"class"=>"login"));
 ?>




	<?php $this->Flash->render(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<center>
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form" style="font-size:13px;">
		<tr><h1> Edit Lead  </h1>
			<th valign="top">Bidding Date:</th>
			<td><?php
				echo $this->Form->input("bidding_date",array("type"=>"text","class"=>"form-control","id"=>"bidding_date","style"=>"height:31px;width:198px;","label"=>false,"readonly"=>true,"div"=>false));
            ?><div id="error_msg1"></div></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">URL Title:</th>
			<td><?php
				echo  $this->Form->input("url_title",array("type"=>"text","class"=>"form-control url_title","label"=>false,"style"=>"height:31px;width:198px;","div"=>false));
            ?>	<div id="error_msg2"></div></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top"> Technology: </th>
			<td><?php
			$options=array('php' =>  'PHP', 
										'phone'    => 'Phone',
										'microsoft'    => 'Microsoft',
										'design'  => 'Design',
										'seo'   => 'SEO'
                      );
				echo  $this->Form->input("technology",array("type"=>"select",'options'=>$options,"class"=>"form-control","id"=>"technology","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top"> Source: </th>
			<td><?php
			$options=array('odesk' =>  'Odesk', 
										'elance'    => 'Elance',
										'guru'    => 'Guru',
										'freelancer'  => 'Freelancer',
										'phone'   => 'Phone',
										'direct'    => 'Direct',
										'email'    => 'Email',
										'client reference'  => 'Client Reference',
										'engineering'   => 'Engineering'
                      );
				echo $this->Form->input("source",array("type"=>"select",'options'=>$options,"class"=>"form-control","id"=>"source","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top"> Category: </th>
			<td><?php
			$options=array('s' =>  'S', 
										's&m'    => 'S&M',
										'm&l'    => 'M&I',
										'I'    => 'I'
                      );
				echo  $this->Form->input("category",array("type"=>"select",'options'=>$options,"class"=>"form-control","id"=>"category","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
	<tr>
			<th valign="top"> Sales-front: </th>
			<td><?php
				$options= $this->General->getUsersByRole('5');
				echo  $this->Form->input("salesfront_id",array("type"=>"select",'options'=>$options,"class"=>"form-control","id"=>"salesfront_id","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Profile On:</th>
		<td><?php

			$session = $this->request->session();
			$this->set('session',$session);


		        $session_info = $session->read("SESSION_ADMIN");
				$userID = $session_info[0];
				//pr($userID);
				$option = $this->General->getProfilesForLead($userID);
				//pr($option);
			echo  $this->Form->input("profile_id",array("type"=>"select",'options'=>$option,"id"=>"profile_id","class"=>"form-control","label"=>false,"div"=>false));
            
			///	echo $form->input("profile_id",array("class"=>"form-control","label"=>false,"div"=>false));
		?></td>
			<td></td>
	</tr>	
		
	<tr>
		<th valign="top">Perspective amount:</th>
		<td><?php
				echo  $this->Form->input("perspective_amount",array("class"=>"form-control","style"=>"height:31px;width:198px;","id"=>"p_amount","label"=>false,"div"=>false));
		?><div id="error_msg3"></div></td>
			
			<td></td>
	</tr>
	<tr>
			<th valign="top"> Status: </th>
			<td><?php
			$options=array('cold' =>  'cold', 
										'active'    => 'active',
										'deny'    => 'deny',
										'closed'    => 'closed',
										'success'    => 'success'
                      );
				echo  $this->Form->input("status",array("type"=>"select",'options'=>$options,"class"=>"form-control","id"=>"statuss","label"=>false,"div"=>false));
            ?></td>
			<td></td>
	</tr>
	<tr>
			<th valign="top">Next-follow Date:</th>
			<td><?php
				echo  $this->Form->input("nextfollow_date",array("type"=>"text","class"=>"form-control","id"=>"nextfollow_date","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?></td>
			<td></td>
	</tr>
	<tr>
			<th valign="top">Time Spent :</th>
			<td><?php
				echo  $this->Form->input("time_spent",array("type"=>"text","class"=>"form-control","id"=>"time_spent","style"=>"height:31px;width:198px;","label"=>false,"div"=>false));
            ?>	<span style="color:red"> e.g. 40:18  </span> <div id="error_msg4"></div></td>
			<td></td>
	</tr>
	</tr>
	<tr>
		<th valign="top"> Notes :</th>
		<td><?php
		 echo  $this->Form->input("notes",array("label"=>false,"div"=>false,"style"=>"height:100px;width:298px;","id"=>"notes"));
				 echo  $this->Form->hidden("id",array("type"=>"text","id"=>"lead_id"));
		?></td>
			<td></td>
	</tr>
	<tr>
			<th valign="top"> Is Invalid: </th>
			<td>
				<?php $value = isset($this->request->data['is_invalid']) ? $value=$this->data['is_invalid'] : $value=''; ?>
					<?php $checked = ($value == '1') ? $checked= "checked=checked " : $checked =''; ?>
					<input type="checkbox" id="is_invalid" name="is_invalid" value="<?php echo $value; ?>" <?php echo $checked; ?> >
           </td>
			<td></td>
	</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $this->Form->button('Save',array('type'=>'button','onClick'=>'javascript: postData();','div'=>false,'id'=>'savebtn',"class"=>"
btn btn-success"));
			?>
			</td>
			<td></td>
		</tr>
	</table></center>
	<!-- end id-form  -->

			</td>
			<td>

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
		<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
