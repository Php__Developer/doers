<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<script type="text/javascript">
	$(document).ready(function() {
	if($('#usersid').val()==""){
		$('#user_biddetails').hide();
	}
		$( "#sdate" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$( "#edate" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$('#ui-datepicker-div').attr('style','display:none');
		$(".icon-1").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	});
		function validateForm(){
		if(($('#edate').val()!="" && $('#sdate').val()=="") || ($('#edate').val()=="" && $('#sdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#edate').val()!="" && $('#sdate').val()!="")){
			var str_date = changeDate($('#sdate').val());
			var end_date = changeDate($('#edate').val());
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
		if(($('#start_amount').val()!="" && $('#last_amount').val()=="") || ($('#start_amount').val()=="" && $('#last_amount').val()!="")){
				var message = "<div>Enter both min and max amount</div>";
				$('#error').html(message);
				return false;
		}else if(($('#start_amount').val()!="" && $('#last_amount').val()!="")){
			if (parseFloat($('#start_amount').val()) > parseFloat($('#last_amount').val())) {
				var message = "<div>Min amount must be less than max amount</div>";
				$('#error').html(message);
				return false;
			}
		}
	}
	function changeDate(date){
		date = date.split("-");
		new_date = date[1]+ "/"+date[0]+"/"+date[2];
		new_date = new Date(new_date);
		return new_date;
	}
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
</script>
<!--  start content-table-inner -->
	


	<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
	<?php echo $this->Form->create('Lead',array( 'url' => ['action' => 'admin_leadreport'] ,'id'=>'adminreport','method'=>'POST','onsubmit' => 'return validateForm();',"class"=>"login")); ?>


<div class="white-box">
         
	<ul class="pagination pull-right">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
	

		<div class="searching_div">
		<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
			<tr>
				<td width="40%">
				<div style="float:left;">
				<div style="float:left;margin-right:10px"><b>Select User :</b></div>
				<div style="float:left;">
				<?php
				$options =  $this->General->getuser_name();
				//	$options = $this->General->getUsersByRole('5');
					//pr($options); 
					echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"form-control","id"=>"usersid",'options'=>$options,"label"=>false,"div"=>false,"style"=>"width:150px;"));		
					?>
				</div><div style="clear:both"></div>
			</div>
				<div style="float:left;margin-left:10px">
					<div style="float:left;margin-right:10px"><b>Status :</b></div>
					<div style="float:left;">
					<?php 			$options=array( '' =>  '-Select-',
										'cold' =>  'cold', 
										'active'    => 'active',
										'deny'    => 'deny',
										'closed'    => 'closed',
										'success'    => 'success'
                      );
				  echo $this->Form->input("status",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false,"style"=>"width:150px;")); ?>
				 	</div><div style="clear:both"></div>
				</div>
				</td><br/>
				<td width="63%">
					<div style="float:left;">
					<b> URL / Title:</b>
						<?php  echo $this->Form->input("url",array("type"=>"text","class"=>"form-control","style"=>"width:150px;height:30px","div"=>false,"label"=> false)); ?>
					</div>
					<div style="float:left; padding-left: 129px;">
					<b> Notes:</b>
						<?php  echo $this->Form->input("notes",array("type"=>"text","class"=>"form-control","style"=>"width:229px;height:32px","div"=>false,"label"=> false)); ?>
					</div>
				</td>
			</tr>
			</table><br/>
				<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
				<tr>
				<td width="10%">
					<div>
					<div style="float:left">
					<span style="vertical-align: middle;">
					<b>Amount:</b>
					</span>
					</div>
				</td>
				<td width="30%">
					<div style="float:left">
					<b>Min Amount:</b>
					<br>
					<?php echo $this->Form->input("start_amount",array("id"=>"start_amount","class"=>"form-control","style"=>"width:100px;", "div"=>false, "label"=> false));	?>
					</div>
					<div style="float:left">
					<b>Max Amount:</b>
					<br>
					<?php echo $this->Form->input("last_amount",array("id"=>"last_amount","class"=>"form-control","style"=>"width:100px;", "div"=>false, "label"=> false));	?>
					</div>
					</div>
				</td>
				<td width="10%">
				<div>
				<div style="float:left">
					<span style="vertical-align: middle;">
					<b>Date:</b>
					</span>
					</div>
				</td>
				<td width="30%">
					<div style="float:left">
					<b>From Date:</b><br/>
					<?php  echo $this->Form->input("sdate",array("type"=>"text","id"=>"sdate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly")); ?>
					</div>
					<div style="float:left">
					<span style="">
					<b>To Date:</b></span><br/>
					<?php  echo $this->Form->input("edate",array("type"=>"text","id"=>"edate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly")); ?>
					</div>
					</div>
				</td>
				<td width="20%">
				<div style="">
			
					<?php
	echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
	echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
	$this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/leads/leadreport', true)));
	?>





					</div>
				</td>
				</tr>
			</table>
		<br/>
				<div style="color:red;" id="error"></div>
		</div>
	

     

	<div class ="lead_usr">
			<div id="user_biddetails" style="float:left; padding: 10px 0 0 0;font-size:12px;font-weight:bold">
				<?php $today = date("Y-m-d"); 
						   $yesterday = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-1,date("Y")));  
						   $week = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y"))); 
						   $olderthanoneweek = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-8,date("Y"))); 
						   
						   $month = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-30,date("Y")));  
						   $olderthanonemonth = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-31,date("Y")));
						   
					$todayBid = 0; $yesterdayBid = 0;  $weekBid = 0;  $monthBid = 0; 
					
					
					foreach($resultData as $leadData){
					
					   //echo strtotime($leadData['bidding_date']).'==>'.strtotime($month).'<br />';
						if(strtotime($leadData['bidding_date']) == strtotime($today)){
							$todayBid++;
							$weekBid++;
							$monthBid++;
						}else if(strtotime($leadData['bidding_date']) == strtotime($yesterday)){
							$yesterdayBid++;
							$weekBid++;
							$monthBid++;
						}else if((strtotime($leadData['bidding_date']) >= strtotime($week))){
							$weekBid++;
							$monthBid++;
						}elseif((strtotime($leadData['bidding_date']) >= strtotime($olderthanonemonth))){
							$monthBid++;
						}
					}
					
					
					
				?>
				<span> Today Bid : <?php echo $todayBid ; ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
				<span> Yesterday Bid : <?php echo $yesterdayBid ; ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
				<span> Weekly Bids : <?php echo $weekBid ; ?></span> &nbsp;&nbsp;&nbsp;&nbsp;
				<span> Monthly Bids : <?php echo $monthBid ; ?></span>
			</div>
			<div style="clear:both"></div>
	</div>
	<?php echo $this->Form->end(); ?>
	 <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="stack" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
              <thead>
                <tr>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Bid Date</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">URL Title</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Resource</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Category</button></th>
                      <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Amount</button></th>
                        <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Status</button></th>
                           <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Options</button></th>
                                </tr>
              </thead>
              <tbody>
				<?php  if(isset($resultData) && count($resultData)>0){
					//pr($resultData); die;
					foreach($resultData as $leadData): $class=""; 
				//	pr($resultData);die(); ?>
				<tr class="<?php echo $class; ?>">
					<td><?php 
						echo  date_format(date_create($leadData['bidding_date']),"d-m-y");


					?> </td>
					<td class="wraping"><?php  $url_title = $leadData['url_title']; 
							echo $this->Html->link($url_title,'#A',
								array('class'=>'info-tooltip','title'=>$leadData['notes'])
							); ?>
					</td>
					<td><?php $str = strip_tags($leadData['Users']['first_name']." ".$leadData['User']['last_name']);  echo substr($str,0,15).((strlen($str)>15)?"...":"");?> </td>
					<td><?php echo strtoupper($leadData['category']); ?> </td>
					<td><?php echo $leadData['perspective_amount']; ?> </td>
					<td><?php echo ucfirst($leadData['status']); ?> </td>
					<td>
					<?php  echo $this->Html->link(
                                              '<i class="fa fa-cog"></i>',
                                            array('controller'=>'leads','action'=>'quickedit',$leadData['id']),
                                             ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit','target'=>'_blank']
                                             );?>
					  <?php
                           echo $this->Html->link(
                      '<i class="ti-close"></i>',
                      array('controller'=>'leads','action'=>'delete','id'=>$leadData['id']),
                      ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
                      );
                      ?>

					</td>
				</tr>
				<?php endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="13" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
									      </tbody>
    
		   </table>
			
				  <?php 
   echo  $this->Html->script(array('leadreport'));
  ?>
			<!--  start paging..................................................... -->

			<!--  end paging................ -->
			
		</div>
		<!-- jQuery -->

<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<!--Style Switcher -->
