<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','popup','oribe')); ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
<?php echo $this->Html->css(array("oribe")); ?>
<input id="pe" class="pe" value="<?php echo $pagename;?>">

<script type="text/javascript">
    $(document).ready(function() { 
      //calender
      $( "#sdate" ).datepicker({
        dateFormat: 'MM dd, yy',
      });
      $( "#edate" ).datepicker({
        dateFormat: 'MM dd, yy',
      });

     
  });


  </script>
<style>
.dropdown-menu.animated.drp_list {
    margin-left: 6px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		//alert('hiii');
	/*		$(".view_inst").fancybox({
			type : 'iframe',
			autoDimensions : false,
			width		: '960',
			height		: '550',
			autoSize	: false,
			onStart     : function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
			});*/
		/*$( "#sdate" ).datepicker({
				 dateFormat: 'MM dd, yy',
			});
		$( "#edate" ).datepicker({
				 dateFormat: 'MM dd, yy',
			});*/
		$('#ui-datepicker-div').attr('style','display:none');
	});
		function validateForm(){
		if(($('#edate').val()!="" && $('#sdate').val()=="") || ($('#edate').val()=="" && $('#sdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#edate').val()!="" && $('#sdate').val()!="")){
			var str_date = changeDate($('#sdate').val());
			var end_date = changeDate($('#edate').val());
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
		if(($('#start_amount').val()!="" && $('#last_amount').val()=="") || ($('#start_amount').val()=="" && $('#last_amount').val()!="")){
				var message = "<div>Enter both min and max amount</div>";
				$('#error').html(message);
				return false;
		}else if(($('#start_amount').val()!="" && $('#last_amount').val()!="")){
			if (parseFloat($('#start_amount').val()) > parseFloat($('#last_amount').val())) {
				var message = "<div>Min amount must be less than max amount</div>";
				$('#error').html(message);
				return false;
			}
		}
	}
	function changeDate(date){
		date = date.split("-");
		new_date = date[1]+ "/"+date[0]+"/"+date[2];
		new_date = new Date(new_date);
		return new_date;
	}
</script>
<?php echo $this->Form->create('Lead',array('url' => ['action' => 'leadslist'], 'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform","onsubmit"=>"return validateForm();")); ?>



<div class="row el-element-overlay m-b-40">


















      <div class="displaycount"><span class="counteringdata"> <?= $this->Paginator->counter([
    'format' => ' {{page}} of {{pages}}'
]) ?></span> Pages</div>
      <input type="hidden" value="<?php echo $paginatecount;?>" class="paginate" >

      <div class="row">
      <div class="col-md-3">
             <b>URL / Title:</b>
						<?php  echo $this->form->input("url",array("type"=>"text","class"=>"form-control","style"=>"width:229px;height:32px","div"=>false,"label"=> false)); ?>
				
            </div>
                <div class="col-md-3">
            <b> Notes:</b>
					<?php  echo $this->form->input("notes",array("type"=>"text","class"=>"form-control","style"=>"width:229px;height:32px","div"=>false,"label"=> false)); ?>
				
            </div>
              <div class="col-md-2">
            <button class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" aria-expanded="false" data-toggle="dropdown" type="button">
              Sort By
              <span class="caret"></span>
          </button>
          <ul class="dropdown-menu animated drp_list" role="menu">
            <li>
              <a href="#" class="1">Name</a>
          </li>  
          <li>
              <a href="#" class="2">Senior</a>
          </li>

          <li>
              <a href="#" class="3">User Comment</a>
          </li>
          <li>
              <a href="#" class="4">Senior Comment</a>
          </li>

          <li>
              <a href="#" class="5">Month & Year</a>
          </li>

      </ul>
				
            </div>
     <div class="col-md-2">
            <?php 
      echo $this->Html->link('<i class="fa  fa-plus"></i>',
         array('controller'=>'leads','action'=>'add','prefix' => 'leads'),
          ['escape' => false,"class"=>"btn btn-info btn-circle ","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Leads"]
          );
          ?>
				
            </div>
            <div class="col-md-2 paginateproject">
            	
            		<ul class="pagination paginate-data">
             <li class="previousdata">

                <?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
            </li>
            <li>
                <?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
            </li>
            		</ul>

            </div>
        </div>




        <div class="row">
        	<div class="col-md-6">
        		<div class="col-md-3">
        			<b>Min Amount:</b>

        			<?php echo $this->Form->input("start_amount",array("id"=>"start_amount","class"=>"form-control","style"=>"width:100px;", "div"=>false, "label"=> false));	?>
        		</div>
        		<div class="col-md-3">
        			<b>Max Amount:</b>

        			<?php echo $this->Form->input("last_amount",array("id"=>"last_amount","class"=>"form-control","style"=>"width:100px;", "div"=>false, "label"=> false));	?>
        		</div>
        	</div>

        	<div class="col-md-6">
        		<div class="col-md-3">
        			<b>From Date:</b>
        			<?php  echo $this->Form->input("sdate",array("type"=>"text","id"=>"sdate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly")); ?></div>
        			<div class="col-md-3">
        				<b>To Date:</b>
        				<?php  echo $this->Form->input("edate",array("type"=>"text","id"=>"edate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly")); ?>
        			</div>
        		</div>
        	</div>

				

<br>
  <div class="row">
        	<div class="col-md-6">
        	   	<div class="col-md-3">
        	<?php 			$options=array( '' =>  '-Select-',
										'cold' =>  'cold', 
										'active'    => 'active',
										'deny'    => 'deny',
										'closed'    => 'closed',
										'success'    => 'success'
                      );
				  echo $this->Form->input("status",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false)); ?>
        	</div>
        	</div>
        	<div class="col-md-6">

        	<?php
            echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
            echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject'));
            $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>$this->Url->build('/leads/leadslist', true)));
            ?> 
        	</div>
   </div>

<br>
 <div class="row">
        	<div class="col-md-6">
        	 <p style="color:red;" ><b>Today's Total Bids : <?php echo $todayleadcount;?></b></p>
        	</div>
        	<div class="col-md-6">
        	<div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                      <ul class="pull-right">
        	    <?php echo $this->Html->link("Active Leads",
              array('controller'=>'leads','action'=>'activelead','prefix'=>'leads'),
              array('class'=>'view_inst popup_window','alt'=>'onhold','style'=>'color:red')
              );
              ?> 

              
                  <?php echo $this->Html->link("On Hold Projects",
              array('controller'=>'projects','action'=>'holdprojects','prefix'=>'projects'),
              array('class'=>'view_inst popup_window','alt'=>'onhold','style'=>'color:red')
              );
              ?>
              </ul>
              </nav>
              </div>
</div>
        	</div>







   
    <div class="ajax">

      <div class="parent_data">
   <?php if(count($resultData)>0){

              $i = 1;
              foreach($resultData as $result):
              $status=$result['status'];
              if($status==2){
                 $class="nav nav-tabs list-group-item-info actioning green";
               }elseif($status == 0){
                 $class ="nav nav-tabs list-group-item-info actioning redish";
                }elseif($status==1){
                $class = "nav nav-tabs list-group-item-info actioning"; 
               }

            $testi_is_enabled = ($result['status'] == 1) ? 'checked': '';?>
              <!-- /.usercard -->
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
                  <div class="white-box">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs firstLISt" role="tablist">
                          <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
                      </ul>
                      <!-- Tab panes  substring($row->parent->category_name,35);  -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane ihome active" >
                             <ul class="list-group ">
                              <li class="list-group-item list-group-item-danger 1"><b>Bidding Date:</b> 
                              <?php  echo date_format(date_create($result['bidding_date']),"d-m-Y");
                              ?>
                               <!-- <?php 
                               $str= $result['Users']['first_name']." ".$result['Users']['last_name'];
                               echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?> --> </li>
                               <li class="list-group-item list-group-item-success 2"><b>URL/Title:</b> 
                            
                                <?php $title =  $result['url_title']; 
                              $url_title= substr($title,0,19).((strlen($title)>19)? "...":"");
								echo $this->Html->link($url_title,'#A',
								array('class'=>'info-tooltip','title'=>$result['notes'])
							    ); ?>
							  </li>
                                  <li class="list-group-item list-group-item-info 3"><b>User Amount :</b> 
                                     <?php echo $result['perspective_amount']; ?>

                                     </li>
                                     <li class="list-group-item list-group-item-warning 4"><b>Status :</b> 
                                     <?php echo ucwords($result['status']); ?></li>

                                         <li class="list-group-item list-group-item-danger 5"><b>Next-follow date:</b>
                                         <?php echo  date_format(date_create($result['nextfollow_date']),"d-m-Y");?>
                                      </li>
                                      <input type="hidden" value = "<?php echo $result->encryptedid;?>" class="appicons" />
                                      <input type="hidden" value = "<?php echo $result['month'].$result['year'];?>" class="monthicons" />
                                      <ul class="<?php echo $class;?>" role="tablist">
                                     <li> <?php
                                           echo $this->Html->link(
                                              '<i class="fa fa-cog"></i>',
                                            array('controller'=>'leads','action'=>'edit','id'=>$result['id']),
                                             ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
                                             );?>
                                         </li>
                                    </ul>
                                 </ul>

                             </div>
                             <div role="tabpanel" class="tab-pane iprofile">
                                <div class="col-md-6">
                                  <h3>Lets check profile</h3>
                                  <h4>you can use it with the small code</h4>
                              </div>
                              <div class="col-md-5 pull-right">
                                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                              </div>
                              <div class="clearfix"></div>
                          </div>
                          <div role="tabpanel" class="tab-pane imessages">
                            <div class="col-md-6">
                              <h3>Come on you have a lot message</h3>
                              <h4>you can use it with the small code</h4>
                          </div>
                          <div class="col-md-5 pull-right">
                              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div role="tabpanel" class="tab-pane isettings">
                        <div class="col-md-6">
                          <h3>Just do Settings</h3>
                          <h4>you can use it with the small code</h4>
                      </div>
                      <div class="col-md-5 pull-right">
                          <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
      <?php $i++ ;
      endforeach; ?>
      <?php } else { ?>

        <?php
    }
    ?>
</div>
<!-- /.usercard-->
</div>
<!-- /.row -->
<!-- <?php echo $this->Html->script(array('cbpFWTabs.js')) ?> -->
<!-- <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script> -->
<!-- Magnific popup JavaScript -->

<script type="text/javascript">
    function veryfycheck()
    {
      alert("1) Either billing for this project is already added for this week."+'\n'+
          "2) Please verify all billing till previous week for this project and then reload this page again to add billing.");

  }

/*  (function() {
if($(document).find('div').hasClass('sttabs')){
  [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });
}
    

})();*/
</script>


