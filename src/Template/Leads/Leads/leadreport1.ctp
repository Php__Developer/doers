

 <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">


<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core ?>



<script type="text/javascript">
var base_url = '<?php echo BASE_URL; ?>';
  //-------------------------Calender-------------------------//
  $(function() {
  $( "#startdate" ).datepicker({
        ampm: true,
        dateFormat: 'yy/mm/dd',
        //minDate: 0,
        
     });
  $( "#enddate" ).datepicker({
        ampm: true,
        dateFormat: 'yy/mm/dd',
       // minDate: 0,
     });
  });

function validateForm(){
    
    if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
      var message = "<div>Enter both start and end date</div>";
      $('#error').html(message);
      return false;
    }else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
      var str_date = $('#startdate').val();
      var end_date = $('#enddate').val();
      if ((Date.parse(str_date)) > (Date.parse(end_date))) {
        var message = "<div>Start date should be less than end date</div>";
        $('#error').html(message);
        return false;
      }
    }
    
  }
  /*
  *---------------Java script code for date calculation----------------*
  */
  function date_calculation()
  {
  var selectedType=document.getElementById("selectdate");
  var selecteddate = selectedType.options[selectedType.selectedIndex].value;
  var today=new Date();
  var today2=today.toISOString().substr(0,10);
  if(selecteddate=="month")
  {
   var monthsum= new Date(new Date(today).setMonth(today.getMonth()-1));
   var monthsum2=monthsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=monthsum2;
   document.getElementById("enddate").value=today2;
   
  }
  else if(selecteddate=="year")
  {
   var yearsum= new Date(new Date(today).setMonth(today.getMonth()-12));
   var yearsum2=yearsum.toISOString().substr(0,10);
   document.getElementById("startdate").value=yearsum2;
   document.getElementById("enddate").value=today2;
  }
  else if(selecteddate=="today")
  {
   document.getElementById("startdate").value=today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="week")
   {
     var yearsum= new Date(new Date(today).setDate(today.getDate()-6));
     var yearsum2=yearsum.toISOString().substr(0,10);
     document.getElementById("startdate").value=yearsum2;
     document.getElementById("enddate").value=today2;
    }
  else if(selecteddate=="select")
  {
   document.getElementById("startdate").value =today2;
   document.getElementById("enddate").value =today2;
  }
  else if(selecteddate=="custom")
  {
   document.getElementById("startdate").value ='';
   document.getElementById("enddate").value ='';
  }
  }
</script>
<?php echo $this->Form->create('Lead',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm",'onSubmit'=>'return validateForm();',"id" =>"mainform")); ?>
<center><h1>Lead Report (Date Wise)</h1></center>
<div id="content-table-inner">
<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
        <tr>
        <td width="15%">
      <div style="float:left;">
       <?php
   //App::import('Component', 'common');
   //$this->Common= new CommonComponent;
// $options = $this->Common->getDateRange();
 echo $this->Form->input("select",array("type"=>"select","class"=>"form-control","options"=>$options,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"selectdate",'onChange'=>'date_calculation();'));
 ?>
 </td>
  <th align="left" ><b>From:&nbsp;</b></th>
  
  <td>
    <div style="float:left;">
    <?php 
  if($fromDate!=""){
  $st_date=date('Y/m/d',strtotime($fromDate));
  }else{
   $st_date=date('Y/m/d');
  }
  if($toDate!=""){
  $todate=date('Y/m/d',strtotime($toDate));
  }else{
  $todate=date('Y/m/d');
  }
  ?>
<?php  echo $this->Form->input("start_date",array("type"=>"text","id"=>"startdate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($st_date)))); ?>  </td>
<th align="left" ><b>To:&nbsp;</b></th>
<td width="30%">
<?php  echo $this->Form->input("end_date",array("type"=>"text","id"=>"enddate","class"=>"form-control","style"=>"width:100px;","div"=>false,"label"=> false,"readonly"=>"readonly","value"=>date('Y/m/d',strtotime($todate)))); ?>   </td>
  </div>
<div style="clear:both"></div>
  </div>
  <th><b>User:&nbsp;</b></th>
  <td width="30%">
        <div style="float:left;">
        <div style="float:left;">
        <?php
        $options =  $this->General->getuser_name();
        //  $options = $this->General->getUsersByRole('5'); 
      //    pr($options);
          echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"form-control","id"=>"usersid",'options'=>$options,"label"=>false,"div"=>false,"style"=>"width:150px;"));    
          ?>
        </div><div style="clear:both"></div>
      </div>
    </td>
  <br/>
<td width="40%">
<div style="">
  <?php echo $this->Form->submit("Submit", array('class'=>'btn btn-success','id'=>'search','method'=>'POST',"id" => "mainform3","name" => "listForm3"));  ?>
  </div>
      </td>
    </tr>
  </table>
<br/>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
  <div class ="lead_usr">
  <div style="clear:both"></div>

  


</div>
  <?php echo $this->Form->end(); ?>
  <div id="table-content">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
        <tbody>
  <?php if(isset($resultData) && count($resultData)>0) { 


    $a=array();
    
    $records=array();
  
  foreach($resultData as $record)
    {     
      $a[]=$record['NoOfleads'];
        $a[]=$record['Users']['first_name'];
      
  
    }



    $aa=array();
    $aaa=array();
    for($i=0;$i<count($a);$i++)
    {
      if($i==0 || $i%2==0)
      {
        $aa[]=$a[$i];
      }
      else{
      $aaa[]=$a[$i];
      }
    }

?>




  
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Lead Report (Date Wise)'
            },
            subtitle: {
                text: ' '
            },
  
      
            xAxis: {
                categories: [ <?php for($name=0;$name<count($aaa);$name++)
                  echo "'$aaa[$name]',";
                ?>],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
         
                title: {
                    text: 'No. of Leads',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' Leads'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
      
            series: [{
                name: 'Leads',
                data: [<?php for($name=0;$name<count($aa);$name++){       
        echo "$aa[$name],";}?>]
            },
      
      ]
        });
    });
    

  </script>
  
<?php echo $this->Html->script('highcharts'); ?>  
<?php  echo $this->Html->script('modules/exporting');?>

  <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
  <div class="clear"></div>
<?php 
        }
    else {
    ?>
    <tr>
      <td colspan="13" class="no_records_found">No records found</td>
        </tr>
    <?php
  } 
    ?>
    </tbody>
    

</table>
    <div class="clear"></div>
</td>
</tr>
</table>
</div>
      <!--  end content-table  -->
  <div class="clear"></div>
</div>


<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

    <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
