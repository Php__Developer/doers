<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'listing','project'));
 ?>
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
<input id="pe" class="hidden pe" value="<?php echo $pagename;?>">
  
<?php 
$totalcount = 0;
?>
<div class="row el-element-overlay m-b-40">
  <div class="col-md-12 ">
    <div class="col-md-4 ">
      <div class="btn-group m-r-10 ">
  <button aria-expanded="true" data-toggle="dropdown" class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" type="button">Sort By <span class="caret"></span></button>
    <ul role="menu" class="dropdown-menu animated flipInX drp_list">
      <li><a href="#" class="1">User Name</a></li>  
      <li><a href="#" class="2">Type</a></li>
      <li><a href="#" class="3">Email Id</a></li>
      <li><a href="#" class="4">Profile Name</a></li>
      <li><a href="#" class="5">Stats</a></li>
    </ul>
  </div>
</div>









                 <div class="col-md-4">
                 <?php 
            echo $this->Html->link('<i class="fa  fa-plus"></i>',
            array('controller'=>'sales','action'=>'add'),
            ['escape' => false,"class"=>"btn btn-info btn-circle pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Add New Sales Profile"]
            );
            ?>
                </div>
       </div>

       <div class="col-md-12 ">
        <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                       <ul class="">
                         <?php 
            echo $this->Html->link("Profile Report by Running Projects",
            array('controller'=>'sales','action'=>'profilereportbyrunnningproject'),
            array('class'=>'runningprofile popup_window')
            );  
            ?>
                                   
            <?php 
            echo $this->Html->link("Sales Profile Audit Report",
            array('controller'=>'sales','action'=>'salesprofileauditreport'),
            array('class'=>'runningprofile popup_window')
            );  
            ?>
                                  
            <?php 
            echo $this->Html->link("Profile Report By Alloted To",
            array('controller'=>'sales','action'=>'profilereportbyallottedto'),
            array('class'=>'auditreports popup_window')
            );  
            ?>
                                   
            <?php 
                echo $this->Html->link("Verified And Unverified Report",
                array('controller'=>'sales','action'=>'verifiedUnverifiedreport'),
                array('class'=>'runningprofile popup_window')
                );  
            ?>
                                   
            <?php 
                echo $this->Html->link("Technology Wise Report",
                array('controller'=>'sales','action'=>'technologyreport'),
                array('class'=>'runningprofile popup_window' )
                );  
            ?>
                                    
            <?php 
            echo $this->Html->link("Payment Report",
            array('controller'=>'sales','action'=>'Paymentreport'),
                array('class'=>'auditreports popup_window')
            );  
            ?>
                      </ul>
                    </nav>
               </div><!-- /tabs -->
       </div>
<br>
<br>
 
    <div class="ajax">

      <div class="parent_data">
      <span class="mytooltip tooltip-effect-5">
                <?php

                $i=1;
                $loopValue=0;
                $columncolor="";
                $idmatch=array();
                $uniqu_id=array();      
            if(count($resultData)>0){
                foreach($resultData as $saleProfileData){
                        $testi_is_enabled = ($saleProfileData['status'] == 1) ? 'checked': '';
                $id=$saleProfileData['id'];
            
                if($saleProfileData['status']==0)
                    {
                    $class="nav nav-tabs list-group-item-info actioning redish";                               
                    }
                    else{
                    $class="nav nav-tabs list-group-item-info";
                    }       
                      ?>   


         <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
                  <div class="white-box">
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs firstLISt" role="tablist">
                          <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
                          <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
                      </ul>
                      <!-- Tab panes  substring($row->parent->category_name,35);  -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane ihome active" >
                             <ul class="list-group ">
                              <li class="list-group-item list-group-item-danger 1"><b>UserName:</b> 
 <?php 
  $str= $saleProfileData['username'];
                               echo $name = substr($str,0,15).((strlen($str)>15)? "...":"");
                                ?>
                                <?php if($saleProfileData['idverified']==1){
                                    echo $this->Html->image("".BASE_URL."images/tick.png", array('title' => 'verified','style'=>'float:right;'));
                                }
                                ;?></li>

                          <li class="list-group-item list-group-item-success 2"><b>Type:</b>
                                          <?php  echo $saleProfileData['type']; ?>  </li>

                                  <li class="list-group-item list-group-item-info 3"><b>Email Id :</b> 
                       
                              <a class="popover-primary" type="button" title="" data-toggle="popover" data-placement="top" data-content="<?php echo $saleProfileData['email'];?>" data-original-title="Email Id" aria-describedby="popover507663"> <?php 
                               $str= $saleProfileData['email'];
                               echo $name = substr($str,0,15).((strlen($str)>15)? "...":""); ?>
                           </a>

                                    </li>

                                     <li class="list-group-item list-group-item-warning 4"><b>Profile Name :</b> 
                          <?php 
                          $str= $saleProfileData['nameonprofile'];
                               echo $name = substr($str,0,15).((strlen($str)>15)? "...":""); ?>
                               </li>

                                         <li class="list-group-item list-group-item-danger 5"><b>Stats:</b>
                              <?php  echo $saleProfileData['stats']; ?>    </li>


     <input type="hidden" value = "<?php echo $this->General->ENC($saleProfileData['id']);?>" class="salelisticons" />

                    <ul class="<?php echo $class;?>" role="tablist">
                    

                     <li>   <?php echo $this->Html->link('<i class="fa fa-eye"></i>',
                                array('controller'=>'sales','action'=>'view', 'prefix' => 'sales','id' => $saleProfileData['id'],'encrption' => $saleProfileData['encrption']),
                               ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'View profile']

                            );?></li>

                    
                         <li>   <?php echo $this->Html->link('<i class="fa fa-cog"></i>',
                                array('controller'=>'sales','action'=>'quickedit','id' => $saleProfileData['id']),
                              ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit']

                            );?></li>
                           <li> 
                            <?php
                                 // AllotedBy nofication counter
                                if(empty($saleProfileData['alloteduserid'])){
                                 //   pr($saleProfileData);die();
                                    $result = 0; 
                                }else{
                                    $totalAlloted= $saleProfileData['alloteduserid'];
                                  //  pr($totalAlloted);die();
                                    $totalAlloted = explode(",", $totalAlloted);
                                 //   pr($totalAlloted);die();
                                    $result = count($totalAlloted);
                                   // pr($result);die();    
                                }
                                echo $this->Html->link("<span class='notification'>$result</span><i class='fa fa-cog icon-people'></i>",
                                array('controller'=>'sales','action'=>'allotedto','id' => $saleProfileData['id']),
                    ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'alloted to ']

                                );   
                            ?></li>
                           <li>
                        <?php 
                             $count =0;
                            foreach($options as $option){
                  //  pr($options->toArray());die;
                                if($option['status']==1){
                                 $ids=$option['profile_id'];
                               
                                 if($ids==$id){ 
                                 //   pr($ids);die();
                                    $count++;                                          
                                 } 
                            } }
                            //pr($ids);die();
                             $totalcount = $totalcount+$count;
                           //  pr($totalcount);die();

                         echo $this->Html->link("<span class='notification'>$count</span><i class='fa fa-search'></i>", '/profileaudits/auditdetails/' . $id, ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'']);
                         
                         ?></li>
                       <li> <?php
                                 if(empty($counterForRuniningProject)){
                                      $counterForRuniningProject=0;
                                  }else{
                                       $counterForRuniningProject = explode(",", $counterForRuniningProject);
                                  $counterForRuniningProject = count($counterForRuniningProject);
                                  }
                                echo $this->Html->link("<span class='notification'>$counterForRuniningProject</span><i class='fa fa-bolt'></i>",
                                array('controller'=>'sales','action'=>'runningprojects', 'prefix' => 'sales','id' => $saleProfileData['id']),
                                                    ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Running Projects ']

                                );
                                $loopValue++;
                            ?></li>




                            <li>
                            <?php
                            if($saleProfileData['is_shared']==0 && $saleProfileData['status']==1){
                       
                           echo $this->Html->link('<i class="fa fa-mail-forward sharedfa"></i>',
                                array('controller'=>'sales','action'=>'share','id' => $saleProfileData['id']),
                              ['escape' => false,'class' => 'btn default btn-outline shared','title'=>'Share ']
                                ); 
                                }
                                else if($saleProfileData['is_shared']==1 && $saleProfileData['status']==1){
                                 echo $this->Html->link('<i class="fa fa-mail-reply unfashared"></i>',
                                array('controller'=>'sales','action'=>'unshare','id' => $saleProfileData['id']),
                              ['escape' => false,'class' => 'btn default btn-outline unshared','title'=>'UnShare ']
                                ); 


                                }
                            ?></li>



                            <li>
                            <?php
                                echo $this->Html->link('<i class="ti-close"></i>',
                                array('controller'=>'sales','action'=>'delete','id' => $saleProfileData['id']),
                              ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete ']
                                ); ?></li>

                                  <li>
<input type="checkbox" <?php echo $testi_is_enabled;?> class="js-switch pull-right textChekb0x"  data-color="#f96262" data-size="small" />
</li>
          

                                             
                                       </ul>
                                 </ul>

                             </div>
                             <div role="tabpanel" class="tab-pane iprofile">
                                <div class="col-md-6">
                                  <h3>Lets check profile</h3>
                                  <h4>you can use it with the small code</h4>
                              </div>
                              <div class="col-md-5 pull-right">
                                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                              </div>
                              <div class="clearfix"></div>
                          </div>
                          <div role="tabpanel" class="tab-pane imessages">
                            <div class="col-md-6">
                              <h3>Come on you have a lot message</h3>
                              <h4>you can use it with the small code</h4>
                          </div>
                          <div class="col-md-5 pull-right">
                              <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div role="tabpanel" class="tab-pane isettings">
                        <div class="col-md-6">
                          <h3>Just do Settings</h3>
                          <h4>you can use it with the small code</h4>
                      </div>
                      <div class="col-md-5 pull-right">
                          <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
      </div>
      <?php }
        }else{ ?>
<div class="white-box leading">
<center class="ld"><h3 class="text-uppercase ">No Record Found !</h3></center><p class="text-muted m-t-30 m-b-30"></p> 
</div>
        <?php
      }
    ?>
    
                

</div>
<!-- /.usercard-->
</div>
<div class="row pull-right">
          <div class="col-md-6">
           <p style="color:red;" ><b>Total Open Audit Points: <?php echo $totalcount;?></b></p>
          </div>
   </div>

<!-- /.row -->
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>

<!-- Magnific popup JavaScript -->
<?php 
echo  $this->Html->script(array('pdetail'));
?>
  <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
  <script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
  <!-- Magnific popup JavaScript -->
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>

  <script>
 jQuery(document).ready(function() {
    // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
    // For select 2
         
 });

 </script>

  <script type="text/javascript">

  function veryfycheck()
  {
  alert("1) Either billing for this project is already added for this week."+'\n'+
  "2) Please verify all billing till previous week for this project and then reload this page again to add billing.");

  }

  (function() {

  [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
  new CBPFWTabs( el );
  });

  })();
  </script>



