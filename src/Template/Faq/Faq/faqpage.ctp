<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<!-- Bootstrap Core CSS -->
<link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="http://www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
    <div class="container-fluid">
     
      <!-- /.row -->
       <div class="row">
        <div class="col-md-12">
          <h4 class="box-title m-b-20">Basic FAQs</h4>
          <div class="panel-group"  role="tablist" aria-multiselectable="true">
       
     
          <?php 
          $counter = 0;
          foreach($faq as $q){ ;?>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo<?php echo $counter ;?>">
                <h4 class="panel-title"> <a class="collapsed font-bold" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo<?php echo $counter ;?>" aria-expanded="false" aria-controls="collapseTwo" > <?php echo $q['title'] ;?>  </a> </h4>
              </div>
              <div id="collapseTwo<?php echo $counter ;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo<?php echo $counter ;?>">
                <div class="panel-body"> <?php echo $q['description'] ;?> </div>
              </div>
            </div>
           <?php
           $counter++;
            }?>

          </div>
        </div>
  
      </div>

</body>
</html>