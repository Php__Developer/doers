<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){ 
		$('.tablesorter').tablesorter({dateFormat: "uk"});
	}); 
</script>
<div id="content-table-inner">
	<?php echo $form->create('Contact',array('action'=>'faqadd','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php echo $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
		<td>
	<!-- start id-form -->
	<div id="table-content">
	<div style="padding-bottom:5px;">
		<span><?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_green1.png', array('width' => '1%','height'=>'10%'))?>
		Current
		</span> 
		<span>	<?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_blue.png', array('width' => '1%','height'=>'10%'))?>
		Closed
		</span> 
		<span> <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_red1.png', array('width' => '1%','height'=>'10%'))?>
		On Hold
		</span>
	</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
	<thead>
		<th class="table-header-repeat line-left minwidth-1" width="80%"><a href="#A">Project Name</a></th>
		<th class="table-header-repeat line-left minwidth-1" width="20%"><a href="#A">Go For Details</a></th>
	</thead>
	<tbody>
		<?php if(count($projectData)>0){ 
		foreach($projectData as $project){ 
		if($project['Project']['status']=='current'){
			$trcolor = "background-color:#E0F2CB";
		}elseif($project['Project']['status']=='closed'){
			$trcolor = "background-color:#C4D9FB";
		}else{
			$trcolor = "background-color:#FAD2B0";
		}
		?>
	<tr style="<?php echo $trcolor; ?> ">
		<td><?php echo "<span style='font-size:15px;font-weight:bold'>".$project['Project']['project_name']."</span>"; ?> </td>
		<td><?php
						echo $html->link("",
								array('controller'=>'projects','action'=>'clientprojectdetails',$project['Project']['id']),
								array('class'=>'info-tooltip go','title'=>'Go','target'=>'_blank','style'=>'  margin: 0 2px -9px 36px;')
							);
		?>
		</td>
	</tr>
		
	<?php	} }else{ ?>
	<tr>
		<td colspan="2"><span style="font-size:15px;"><center>No Current Projects!</center></span> </td>
	</tr>
	<?php } ?>
	</tbody>
	</table>
	</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->