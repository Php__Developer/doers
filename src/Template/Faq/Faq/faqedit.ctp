	<!--  start content-table-inner -->
<?php  $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'));?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
<?php echo $this->Html->css(array('colors/blue','animate')); ?>
<script src="http://www.w3schools.com/lib/w3data.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.min.js"></script>


<?php echo $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min')); ?>
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions')); ?>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>

<?php echo $this->Form->create($FaqData,array('url'=>['action'=>'faqedit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>



	<?php echo $this->Html->css(array('oribe')); ?>
    <!--  start content-table-inner -->
	  <div class="col-sm-6">
            <div class="white-box">

	<?php $this->Flash->render(); ?>
	 <div class="form-group">
                       <label for="exampleInputpwd1">Title</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
				 echo $this->Form->input("title",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['title'])){
							echo $this->General->errorHtml($errors['title']);
							} ;?></div>

                 <div class="form-group">
                       <label for="exampleInputpwd1">Description</label>
                       <div class="input-group">
                        <?php
				 echo $this->Form->textarea("description",array("class"=>"form-control","label"=>false,"div"=>false,'id' => 'mymce'));
			
            ?>
</div>
            <?php if(isset($errors['description'])){
							echo $this->General->errorHtml($errors['description']);
							} ;?></div>
      

         
        
	
<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'faq','action'=>'faqedit','prefix' => 'faq','id'=>$id],['class'=>"btn btn-inverse waves-effect waves-light"]);?>

 </div>
 </div>
</div>
   <!-- end id-form  -->

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Finance Management</h4>
         Finance Management is used to keep track of title, description, amount, and other information. 
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'faq','action'=>'faqlist'), array('style'=>'color:red;'));?>
               </li> 
             
						
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
 </div>
 </div>












 
<script>


   
    $(document).ready(function(){

        $('#mymce').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        $('.inline-editor').summernote({
            airMode: true            
        });

   });
   
  window.edit = function() {
          $(".click2edit").summernote()
      }, 
  window.save = function() {
          $(".click2edit").destroy()
      }
</script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

     
   
</body>
</html>


