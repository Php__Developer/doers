	<!--  start content-table-inner -->
<?php  $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'));?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions')); ?>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>

<?php echo $this->Form->create($FinanceData,array('url'=>['action'=>'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>



	<?php echo $this->Html->css(array('oribe')); ?>
    <!--  start content-table-inner -->
	  <div class="col-sm-6">
            <div class="white-box">

	<?php $this->Flash->render(); ?>
	 <div class="form-group">
                       <label for="exampleInputpwd1">Title</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
				 echo $this->Form->input("title",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['title'])){
							echo $this->General->errorHtml($errors['title']);
							} ;?></div>

                 <div class="form-group">
                       <label for="exampleInputpwd1">Description</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				 echo $this->Form->input("description",array("class"=>"form-control","label"=>false,"div"=>false));
				// echo $this->form->hidden("id");
            ?>
</div>
            <?php if(isset($errors['description'])){
							echo $this->General->errorHtml($errors['description']);
							} ;?></div>
        <div class="form-group">
                       <label for="exampleInputpwd1">Amount</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				 echo $this->Form->input("amount",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
		
	 




 
			 

                   <div class="form-group">
                       <label for="exampleInputpwd1">Card Type</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div> 


           <?php
				    $options=array( 'credit'    =>  'Credit', 
									'debit'    =>  'Debit', 
									
                      );
			echo $this->Form->input("cardtype",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div>
            </div>

         
        
	
<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'finance','action'=>'edit','prefix' => 'finance','id'=>$id],['class'=>"btn btn-inverse waves-effect waves-light"]);?>

 </div>
 </div>
</div>
</div>
   <!-- end id-form  -->

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Finance Management</h4>
         Finance Management is used to keep track of title, description, amount, and other information. 
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'finance','action'=>'list'), array('style'=>'color:red;'));?>
               </li> 
             
						
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
 </div>
 </div>

     
   
</body>
</html>


