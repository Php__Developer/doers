<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<?php echo $this->Html->css(array('oribe')); ?>
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core ?>
    <!--  start content-table-inner -->
	  <div class="col-sm-6">
        <div class="white-box">
        <?php echo $this->Form->create('Finance',array('url'=>['action'=>'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
        <?php $this->Flash->render(); ?>
        <div class="form-group">
          <label for="exampleInputpwd1">Name</label>
          <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("name",array("class"=>"form-control name","label"=>false,"div"=>false));
            ?>
          </div>
          <?php if(isset($errors['name'])){
          echo $this->General->errorHtml($errors['name']);
          } ;?>
        </div>

        <div class="form-group">
        <label for="exampleInputpwd1">Primary Email</label>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div>

        <?php
        echo $this->Form->input("primaryemail",array("class"=>"form-control primaryemail","label"=>false,"div"=>false));
        //echo $form->hidden("id");
        ?></div>
        <?php if(isset($errors['primaryemail'])){
        echo $this->General->errorHtml($errors['primaryemail']);
        } ;?></div>
        <div class="form-group">
        <label for="exampleInputpwd1">Primary Phone</label>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div>

        <?php
        echo $this->Form->input("primaryphone",array("class"=>"form-control primaryphone","label"=>false,"div"=>false));
        ?>

        </div></div>



        <div class="form-group">
        <label for="exampleInputpwd1">Client Password</label>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div>

        <?php
        echo $this->Form->input("cpassword",array("type"=>"text","id"=>"cpassword","class"=>"form-control","label"=>false,"div"=>false));
        ?></div>


        <?php if(isset($errors['cpassword'])){
        echo $this->General->errorHtml($errors['cpassword']);
        } ;?></div>

        <span class="genpass" >Generate Password</span>




        <div class="form-group">
        <label for="exampleInputpwd1">Skyup</label>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div>
        <?php
        echo $this->Form->input("skype",array("class"=>"form-control skype","label"=>false,"div"=>false));
        ?></div>

        <?php if(isset($errors['skype'])){
        echo $this->General->errorHtml($errors['skype']);
        } ;?></div>
        <div class="form-group"> 
        <label for="inputName" class="control-label">Other Finance:</label>
        <?php
        echo $this->Form->input("otherfinance",array("class"=>"form-control otherfinance","label"=>false,"div"=>false));
        ?></div>





        <div class="form-group">
        <label for="exampleInputpwd1">Type</label>
        <div class="input-group">
        <div class="input-group-addon"><i class="ti-lock"></i></div> 

        <?php
        $options=array( 'general'    =>  'General', 
        'lead'    =>  'Lead', 
        'recurring'    => 'Recurring',
        'silver'    => 'Silver',
        'gold'     => 'Gold',
        'platinum'     => 'Platinum',
        );
        echo $this->Form->input("type",array('type'=>'select','options'=>$options,"class"=>"form-control type","label"=>false,"div"=>false));    
        ?></div></div>


        <div class="form-group"> 
        <label for="inputName" class="control-label">Notes:</label>


        <?php
        echo $this->Form->input("notes",array("class"=>"form-control notes","label"=>false,"div"=>false));
        //echo $form->hidden("id");
        ?></div>
<!-- 
        <div class="form-group">
        <label for="exampleInputpwd1">Current Client?:</label>
        <div class="input-group">



        <?php
         $this->Form->input("iscurrent",array('type'=>'checkbox','class'=> 'roles iscurrent',"label"=>false,"div"=>false,"id"=>"iscurrent",'hiddenField'=>true));
        //echo "<label class='projectlbl' for ='iscurrent'>Daily Update </label><br/>";
        ?></div></div> -->



        <div class="form-group">
        <button type="submit" class="btn btn-success addfinance">Submit</button>
        <?php 
        echo $this->Html->link("Reset",  ['controller'=>'finance','action'=>'quickadd','prefix' => 'finance'],['class'=>"btn btn-inverse waves-effect waves-light"]);?>
        </div>
        </div>
</div>
    <!-- end id-form  -->

     
   
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<!--    <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 -->

<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/finance.js"></script>

      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
