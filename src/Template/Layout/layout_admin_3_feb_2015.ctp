<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout;?></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<?php print $html->charset('UTF-8') ?>
<script>
var base_url = "<?php echo BASE_URL; ?>";
</script>
<?php echo $html->css(array("screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple")); ?>
<?php echo $javascript->link(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'))	//ui.core
?>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript"> 
    $(function () {
        //$('#js-news').ticker();
	//	$('.ticker-title').append("<span>Hello</span>");
    });
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<![if !IE 7]>

<!--  styled select box script version 1 -->
<?php echo $javascript->link(array('jquery.selectbox-0.5')); ?>

<![endif]>
<?php echo $javascript->link(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?>
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL."/css/ie.css"?>" />
<![endif]-->
</head>
<?php $user = $this->Session->read("SESSION_ADMIN"); ?>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">
	<!-- start logo -->
	<div id="logo">
	<?php echo $html->link(
			$html->image(BASE_URL."images/erp_logo.png",array("alt" => SITE_NAME,"height"=>'90px')),
			array('controller'=>'users','action'=>'login'),
			array('escape'=>false,'title'=>SITE_NAME)
			);			
	?>
	</div>
	<!-- end logo -->
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer">
		<!-- start nav-right -->
		<div id="nav-right">
			<div class="nav-divider">&nbsp;</div>
			<?php 
			  echo $html->link(
			    $html->image(BASE_URL."images/nav_performancet.png",array("alt" => 'My Performance')),
			    array('controller'=>'evaluations','action'=>'myperformance'),
			    array('escape'=>false,'title'=>'','id'=>'myperformance','class'=>'menuLinks')
			    );
			?>
			<div class="nav-divider">&nbsp;</div>
			<?php 
			  echo $html->link(
			    $html->image(BASE_URL."images/shared/nav/nav_myaccount.gif",array("alt" => 'MyAccount')),
			    array('controller'=>'users','action'=>'profile'),
			    array('escape'=>false,'title'=>'My Account','id'=>'profile','class'=>'menuLinks')
			    );
			?>
			<div class="nav-divider">&nbsp;</div>
			<?php 
			echo $html->link(
				$html->image(BASE_URL."images/shared/nav/nav_logout.gif",array("alt" => 'Logout')),
				array('controller'=>'users','action'=>'logout'),
				array('escape'=>false,'title'=>'Logout','id'=>'logout')
				);	
			?>
			<div class="clear">&nbsp;</div>
			
		</div>
		<!-- end nav-right -->
		<!--  start nav -->
		<div class="nav">
		<div class="table">
		<ul id="nav">
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">HRM</span></a>
			<ul class="sub">
				<?php if($this->General->menu_permissions('employees','admin_list',$user[0]) == "1"){ ?>
					<li>
					<?php
					echo $html->link("Employees",
					array('controller'=>'employees','action'=>'list')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('skills','admin_list',$user[0]) == "1"){ ?>
					<li><?php echo $html->link("Skill Management",
					array('controller'=>'skills','action'=>'admin_list')
					);	
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('resumes','admin_list',$user[0]) == "1"){ ?>
					<li>
					<?php
						echo $html->link("Resume Management",
						array('controller'=>'resumes','action'=>'list')
					);
					?></li>
				<?php } ?>
					<li>
					<?php echo $html->link("Add Attendance",
					array('controller'=>'attendances','action'=>'admin_add')
					);	
					?></li>
		
				<?php if($this->General->menu_permissions('appraisals','admin_list',$user[0]) == "1"){ ?>
					<li>
					<?php
					echo $html->link("Appraisals",
					array('controller'=>'appraisals','action'=>'list')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('employees','admin_list',$user[0]) == "1"){ ?>
					<li>
					<?php
					echo $html->link("HRM Settings",
					array('controller'=>'employees','action'=>'settings')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('static_pages','admin_dashboardeditor',$user[0]) == "1"){ ?>
					<li>
					<?php
						echo $html->link("Dashboard Editor",
						array('controller'=>'static_pages','action'=>'dashboardeditor')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('evaluations','admin_list',$user[0]) == "1"){ ?>
					<li>
					<?php
						echo $html->link("Performance Manager",
						array('controller'=>'evaluations','action'=>'list')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('evaluations','admin_monthlyperformance',$user[0]) == "1"){ ?>
					<li>
					<?php
						echo $html->link("Monthly Performance",
						array('controller'=>'evaluations','action'=>'monthlyperformance')
					);
					?></li>
				<?php } ?>
				
								
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Tools</span></a>
			<ul class="sub">
				<li><?php echo $html->link("Knowledge Base",
				array('controller'=>'static_pages','action'=>'list')
				);	
				?></li>
				<li><?php echo $html->link("Learning Center",
				array('controller'=>'learning_centers','action'=>'list')
				);	
				?></li>
				<li><?php echo $html->link("Tickets",
				array('controller'=>'tickets','action'=>'index')
				);	
				?></li>
				<li><?php echo $html->link("Blog",
				array('controller'=>'learning_centers','action'=>'add')
				);	
				?></li>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Engineering</span></a>
			<ul class="sub">
				<li><?php echo $html->link("Billings", array('controller'=>'billings','action'=>'list'));	
				?></li>
				<?php if($this->General->menu_permissions('projects','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Project Management",
				array('controller'=>'projects','action'=>'admin_list')
				);	
				?></li>
			<?php } ?>
			
			<?php if($this->General->menu_permissions('projects','admin_processaudit',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Process Audit",
				array('controller'=>'projects','action'=>'admin_processaudit')
				);	
				?></li>
			<?php } ?>
			<li><?php echo $html->link("Process Report",
				array('controller'=>'projects','action'=>'admin_processreport')
				);	
				?>
			</li>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Sales</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('testimonials','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Testimonials",
				array('controller'=>'testimonials','action'=>'list')
				);	
				?></li>
			<?php } ?>
			
				<?php if($this->General->menu_permissions('projects_categories','admin_list',$user[0]) == "1"){ ?>
    <li><?php echo $html->link("Project Category",
    array('controller'=>'project_categories','action'=>'admin_list')
    ); 
    ?></li>
    <?php } ?>
			
			<?php if($this->General->menu_permissions('Technologies','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Technology",
				array('controller'=>'technologies','action'=>'admin_list')
				);	
				?></li>
				<?php } ?>
			
			
			<?php if($this->General->menu_permissions('leads','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Lead Management",
				array('controller'=>'leads','action'=>'admin_list')
				);	
				?></li>
			<?php } ?>
				<?php if($this->General->menu_permissions('portfolios','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Portfolios", array('controller'=>'portfolios','action'=>'list')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('portfolios','admin_setting',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Settings",
				array('controller'=>'portfolios','action'=>'admin_setting')
				);	?>
				</li>
			<?php	} ?>
			<?php if($this->General->menu_permissions('portfolios','admin_tagcloud',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Tag Cloud",
				array('controller'=>'portfolios','action'=>'admin_tagcloud')
				);	?>
				</li>
			<?php	} ?>
			
			<?php if($this->General->menu_permissions('sale_profiles','admin_profiledetail',$user[0]) == "1"){ ?>
			<li><?php echo $html->link("Profile Details",
				array('controller'=>'sale_profiles','action'=>'profiledetail')
				);	?>
				</li>
			<?php	} ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Operations</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('hardwares','admin_list',$user[0]) == "1"){ ?>
				<li>
				<?php
				echo $html->link("Assests Management",
				array('controller'=>'hardwares','action'=>'list')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('employees','admin_seatreport',$user[0]) == "1"){ ?>
				<li>
				<?php
				echo $html->link("Seat Report",
				array('controller'=>'employees','action'=>'admin_seatreport')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('projects','admin_closedproject',$user[0]) == "1"){ ?>
				<li>
				<?php
				echo $html->link("Closed Project",
				array('controller'=>'projects','action'=>'admin_closedproject')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('credentials','admin_list',$user[0]) == "1"){ ?>
				<li>
				<?php
				echo $html->link("Credentials Management",
				array('controller'=>'credentials','action'=>'list')
				);
				?>
				</li>
			<?php } ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Admin</span></a>
			<ul class="sub">
				<?php if($this->General->menu_permissions('contacts','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Contacts", array('controller'=>'contacts','action'=>'list')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('payments','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Payments", array('controller'=>'payments','action'=>'list')
				);	
				?></li>
			<?php } ?>
			
			<?php if($this->General->menu_permissions('permissions','admin_setting',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Settings",
				array('controller'=>'permissions','action'=>'setting')
				);	
				?></li>
			<?php } ?>		
			
			<?php if($this->General->menu_permissions('permissions','admin_index',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Allot Permissions",
				array('controller'=>'permissions','action'=>'index')
				);	
				?></li>
			<?php } ?>
			
			<?php if($this->General->menu_permissions('permissions','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Permission Management",
				array('controller'=>'permissions','action'=>'admin_list')
				);	
				?></li>
			<?php } ?>
			
			<?php if($this->General->menu_permissions('sale_profiles','admin_list',$user[0]) == "1"){ ?>
			<li><?php echo $html->link("Profile Management",
				array('controller'=>'sale_profiles','action'=>'list')
				);	?>
				</li>
			<?php	} ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Reports</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('attendances','admin_report',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Attendance Report",
				array('controller'=>'attendances','action'=>'report')
				);	
				?></li>
			<?php } ?>
				<?php if($this->General->menu_permissions('users','admin_report',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Company Snapshot",
				array('controller'=>'users','action'=>'report')
				);	
				?></li>
				
			<?php } ?>
			<?php if($this->General->menu_permissions('leads','admin_leadreport',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Lead Report",
				array('controller'=>'leads','action'=>'admin_leadreport')
				);	
				?></li>
			<?php } ?>
				<?php if($this->General->menu_permissions('projects','admin_projectreport',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Project Report",
				array('controller'=>'projects','action'=>'projectreport')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('tickets','admin_report',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Ticket Report",
				array('controller'=>'tickets','action'=>'report')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('report','admin_list',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Other Reports", array('controller'=>'reports','action'=>'index')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('tickets','admin_schedule',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Ticket Schedule",
				array('controller'=>'tickets','action'=>'schedule')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('evaluations','admin_performancereport',$user[0]) == "1"){ ?>
				<li><?php echo $html->link("Performance Report",
				array('controller'=>'evaluations','action'=>'performancereport')
				);	
				?></li>
			<?php } ?>
			         <li><?php echo $html->link("Salary Calculation ",
				array('controller'=>'users','action'=>'employee_salary')
				);	
				?></li> 

                               <?php  $role_array=explode(',', $user[2]);
                            $d=count($role_array);
    
                                if($this->General->menu_permissions('evaluations','admin_performancereport',$user[0]) == "1"){ ?>  ?>    
                            <li><?php echo $html->link("Leave Calculation ",
				array('controller'=>'users','action'=>'employee_leave_other')
				);	
				?></li>
                            <?php } ?>






			</ul>
		</li>
		</ul>
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>

		<!--  start nav -->
 
</div>
<div class="clear"></div>
<!--  start nav-outer -->

</div>
<!--  start nav-outer-repeat................................................... END -->
 
 <div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer">

<!-- start content -->
<div id="content">

<?php echo $session->flash();  ?>

 <div id="newsFlash">
 <span>Notifications :</span>
 <ul id="js-news" class="js-hidden">
		<?php $options = $general->dynamic_notification();
				$notes=array();
			foreach($options as $val){ $notice=$val['Setting']['value'];}
			$notes=explode(',',$notice);
		foreach($notes as $note){
		?>
	<li class="news-item"><a href="#"><?php echo $note; ?></a></li><?php } ?>
	
</ul>
 </div>
<h4 style="float:right">If anyone facing any issue using this system, please email at <a href="mailTo:admin@vlogiclabs.com">admin@vlogiclabs.com</a> or call me at 9044002044</h4>

<div id="page-heading"><h1><?php echo $title_for_layout; ?></h1></div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowleft.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowright.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td class="view_text" valign="top">
		<?php echo $content_for_layout; ?>
	</td>
	<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
<div style="float: right; margin-top: 6px; margin-right: 20px;" id="social_button">
<a target="_blank" href="https://www.facebook.com/Vlogiclabs"><img src="/images/facebook-logo.png"></a> &nbsp;&nbsp;
<a target="_blank" href="http://youtu.be/k_UnkonOtO4 "><img height="30px" src="/images/youtube-logo.png"></a>  &nbsp;
<a target="_blank" href="https://twitter.com/vlogiclabs"><img height="30px" src="/images/twitter-icon.jpg"></a>  &nbsp;
<a target="_blank" href="https://www.linkedin.com/in/vlogiclabs"><img height="30px" src="/images/linkedin-icon.jpg"></a> 
</div>
	<!--  start footer-left -->
	<div id="footer-left">
	 &copy; Copyright <?php echo SITE_NAME; ?> <?php echo $html->link(BASE_URL,BASE_URL);?>. All Rights Reserved.
	</div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script type="text/javascript">
	$(document).ready(function(){
	$('#ui-datepicker-div').hide();
	});
</script>
</body>
</html>
