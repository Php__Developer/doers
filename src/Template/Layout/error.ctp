
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout;?></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<?php print $this->Html->charset('UTF-8') ?>
<script>
var base_url = "<?php echo $this->Url->build('/', true); ?>";

//console.log(base_url); 
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"));
           $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"))
; ?>
<?php //echo $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'))   //ui.core
    echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core
?>


<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">

<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions','tinysort'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?>
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL."/css/ie.css"?>" />
<![endif]-->
</head>

<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">
    <!-- start logo -->
    <div id="logo">
    <?php echo $this->Html->link(
            $this->Html->image(BASE_URL."images/erp_logo.png",array("alt" => SITE_NAME,"height"=>'90px')),
            array('controller'=>'users','action'=>'login' , 'prefix' =>'admin' ),
            array('escape'=>false,'title'=>SITE_NAME)
            );          
    ?>
    </div>
    <!-- end logo -->
    
    <div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
    
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat START -->
<div class="nav-outer-repeat"> 


<!--  start nav-outer -->

</div>
<!--  start nav-outer-repeat................................................... END -->
 
 <div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer">

<!-- start content -->
<div id="content">



 <div id="newsFlash">
 <span></span>
 <ul id="js-news" class="js-hidden">
  <style>
  .error_logo {
    margin-left: 533px;
    margin-top: 47px;
}
.er_txt {
    margin-left: 520px;
    margin-top: 12px;
}
.exception_txt{
    margin-left: 450px;
    margin-top: -29px;
}
.foo_loink > a {
    margin-left: 544px;
    margin-top: -34px;
    position: absolute;
}
  </style>     

  
    
</ul>
 </div>
<h4 style="float:right"></h4>

<div id="page-heading"><h1></h1></div>

<div id="container">
        <div id="header" >
          
        <img src="<?php echo BASE_URL?>images/sorry.png" class="error_logo"></img>
            <h1 class="er_txt"><?= __("We're sorry ! ") ?></h1>
        </div>
        <div id="content">
            <?= $this->Flash->render() ?>
           <?= $this->fetch('content') ?>
        </div>
        <div id="" class="foo_loink">
            <?= $this->Html->link(__('Go to Back'), 'javascript:history.back()') ?>
        </div>
    </div>

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">

<?php /* ?>
<div style="float: right; margin-top: 6px; margin-right: 20px;" id="social_button">
<a target="_blank" href="https://www.facebook.com/Vlogiclabs"><img src="/images/facebook-logo.png"></a> &nbsp;&nbsp;
<a target="_blank" href="http://youtu.be/k_UnkonOtO4 "><img height="30px" src="/images/youtube-logo.png"></a>  &nbsp;
<a target="_blank" href="https://twitter.com/vlogiclabs"><img height="30px" src="/images/twitter-icon.jpg"></a>  &nbsp;
<a target="_blank" href="https://www.linkedin.com/in/vlogiclabs"><img height="30px" src="/images/linkedin-icon.jpg"></a> 

<?php */ ?>
</div>
    <!--  start footer-left -->
    <div id="footer-left">
     &copy; Copyright <?php //echo SITE_NAME; ?> <?php //echo $this->Html->link(BASE_URL,BASE_URL);?>. All Rights Reserved.
    </div>
    <!--  end footer-left -->
    <div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script type="text/javascript">
    $(document).ready(function(){
    $('#ui-datepicker-div').hide();
    });
</script>
</body>
</html>























