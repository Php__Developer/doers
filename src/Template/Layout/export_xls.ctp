<?php
header ("Expires: Mon, 28 Oct 2008 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/vnd.ms-excel");
header ("Content-Disposition: attachment; filename=\"$filename.xls" );
header ("Content-Description: Generated Report" );
?>
<table border="1">
	<tr> 
		<td><b>Credential(s) Report<b></td> 
	</tr>
	<tr>		
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
	</tr>
	<tr>		
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
	</tr>  		
	<tr>
	<th class="tableTd">Username</th>
	<th class="tableTd">Password</th>
	<th class="tableTd">Description</th>
	<th class="tableTd">Type</th>
	<th class="tableTd">Keyword</th>
	</tr> 
	<?php 
	foreach($crData as $val) { 
	$bgclr  = $val->status ?'#ffffff' : '#ff6544'; 
	?>
		<tr>
		<td bgcolor="<?php echo $bgclr;?>" align="right"><?php echo $val->username; ?></td>
		<td bgcolor="<?php echo $bgclr;?>"  align="left"><?php echo $val->password; ?></td>
		<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo nl2br($val->description); ?></td>
		<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo nl2br($val->type); ?></td>	
		<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo $val->keyword; ?></td>	
		</tr>
	<?php
	}//end for
	?>
	</tr>		
</table>

