<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout;?></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<?php print $this->Html->charset('UTF-8') ?>
<script>
var base_url = "<?php echo $this->Url->build('/', true); ?>";

</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->css(array("admin-reports","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple")); ?>
<?php  echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox'));	//ui.core

// $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'))	//ui.core
?>

<script type="text/javascript"> 
    $(function () {
        $('#js-news').ticker();
	//	$('.ticker-title').append("<span>Hello</span>");
    });
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.css" media="admin-reports" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<![if !IE 7]>

<!--  styled select box script version 1 -->
<?php echo $this->Html->script(array('jquery.selectbox-0.5')); ?>

<![endif]>
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions','tinysort'));
//$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?>
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL."/css/ie.css"?>" />
<![endif]-->
</head>
<?php $user = $session->read("SESSION_ADMIN"); ?>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">
	<!-- start logo -->
	<div id="logo">




	<?php echo $this->Html->link(
			$this->Html->image(BASE_URL."images/erp_logo.png",array("alt" => SITE_NAME,"height"=>'90px')),
			array('controller'=>'users','action'=>'login','prefix' =>'admin'),
			array('escape'=>false,'title'=>SITE_NAME)
			);			
	?>
	</div>
	<!-- end logo -->
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer">
		<!-- start nav-right -->
		<div id="nav-right">
			<div class="nav-divider">&nbsp;</div>

<?php 
			  echo $this->Html->link(
			    $this->Html->image(BASE_URL."images/nav_performancet.png",array("alt" => 'My Performance')),
			    array('controller'=>'evaluations','action'=>'myperformance' ,'prefix' => 'evaluations'),
			    array('escape'=>false,'title'=>'','id'=>'myperformance','class'=>'menuLinks')
			    );
			?>
<div class="nav-divider">&nbsp;</div>
			<?php 
			 echo $this->Html->link(
			    $this->Html->image(BASE_URL."images/shared/nav/nav_myaccount.gif",array("alt" => 'MyAccount')),
			   array('controller'=>'users','action'=>'profile','prefix'=> 'admin'),
			    array('escape'=>false,'title'=>'My Account','id'=>'profile','class'=>'menuLinks')
			    );
			?>
			<div class="nav-divider">&nbsp;</div>
			<?php 
			echo $this->Html->link(
				$this->Html->image(BASE_URL."images/shared/nav/nav_logout.gif",array("alt" => 'Logout')),
				array('controller'=>'users','action'=>'logout','prefix'=> 'admin'),
				array('escape'=>false,'title'=>'Logout','id'=>'logout')
				);	
			?>
			<div class="clear">&nbsp;</div>
			
		</div>
		<!-- end nav-right -->
		<!--  start nav -->
		<div class="nav">
		<div class="table">
		<ul id="nav">
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">HRM</span></a>
			<ul class="sub">
				<?php 
	          $data=$this->General->getUsersession();
				if($this->General->menu_permissions('employees','admin_list',$data[0]) == "1"){ ?>
					<li>
					<?php
					echo $this->Html->link("Employees",
					array('controller'=>'employees','action'=>'employeelist','prefix' => 'employees')
					);
					
					?></li>
				<?php } ?>
				
					<li>
					<?php echo $this->Html->link("Add Attendance",
					array('controller'=>'attendances','action'=>'add','prefix' => 'attendances')
					);	
					?></li>
		
				<?php if($this->General->menu_permissions('appraisals','appraisalslist',$data[0]) == "1"){ ?>
					<li>
					<?php
					echo $this->Html->link("Appraisals",
					array('controller'=>'appraisals','action'=>'appraisalslist','prefix' => 'appraisals')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('employees','employeelist',$data[0]) == "1"){ ?>
					<li>
					<?php
					echo $this->Html->link("HRM Settings",
					array('controller'=>'employees','action'=>'settings','prefix' => 'employees')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('staticpages','dashboardeditor',$data[0]) == "1"){ ?>
					<li>
					<?php
						echo $this->Html->link("Dashboard Editor",
						array('controller'=>'static_pages','action'=>'dashboardeditor','prefix' => 'static_pages')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('evaluations','evaluationslist',$data[0]) == "1"){ ?>
					<li>
					<?php
						echo $this->Html->link("Performance Manager",
						array('controller'=>'evaluations','action'=>'evaluationslist','prefix' => 'evaluations')
					);
					?></li>
				<?php } ?>
				<?php if($this->General->menu_permissions('evaluations','monthlyperformance',$data[0]) == "1"){ ?>
					<li>
					<?php
						echo $this->Html->link("Monthly Performance",
						array('controller'=>'evaluations','action'=>'monthlyperformance','prefix' => 'evaluations')
					);
					?></li>
				<?php } ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Tools</span></a>
			<ul class="sub">
				
				<li><?php echo $this->Html->link("Tickets",
				array('controller'=>'tickets','action'=>'index','prefix' => 'tickets')
				);	
				?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Engineering</span></a>
			<ul class="sub">
				<li><?php echo $this->Html->link("Billings", array('controller'=>'billings','action'=>'billingslist','prefix' => 'billings'));	
				?></li>
				<?php if($this->General->menu_permissions('projects','projectslist',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Project Management",
				array('controller'=>'projects','action'=>'projectslist','prefix' => 'projects')
				);	
				?></li>
			<?php } ?>
			
			<?php if($this->General->menu_permissions('projects','processaudit',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Process Audit",
				array('controller'=>'projects','action'=>'processaudit','prefix' => 'projects')
				);	
				?></li>
			<?php } ?>
			<li><?php echo $this->Html->link("Process Report",
				array('controller'=>'projects','action'=>'processreport','prefix' => 'projects')
				);	
				?>
			</li>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Sales</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('testimonials','testimonialslist',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Testimonials",
				array('controller'=>'testimonials','action'=>'testimonialslist','prefix' => 'testimonials')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('leads','leadslist',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Lead Management",
				array('controller'=>'leads','action'=>'leadslist','prefix' => 'leads')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('Generics','admin_setting',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Settings",
				array('controller'=>'generics','action'=>'admin_setting','prefix' => 'generics')
				);	?>
				</li>
			<?php	} ?>
			
			
					<?php if($this->General->menu_permissions('sales','profiledetail',$data[0]) == "1"){ ?>
			<li><?php echo $this->Html->link("Profile Details",
				array('controller'=>'sales','action'=>'profiledetail','prefix' => 'sales')
				);	?>
				</li>
			<?php	} ?>
			
			
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Operations</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('hardwares','hardwareslist',$data[0]) == "1"){ ?>
				<li>
				<?php
				echo $this->Html->link("Assests Management",
				array('controller'=>'hardwares','action'=>'hardwareslist','prefix' => 'hardwares')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('employees','seatreport',$data[0]) == "1"){ ?>
				<li>
				<?php
				echo $this->Html->link("Seat Report",
				array('controller'=>'employees','action'=>'seatreport','prefix' => 'employees')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('projects','closedproject',$data[0]) == "1"){ ?>
				<li>
				<?php
			echo $this->Html->link("Closed Project",
				array('controller'=>'projects','action'=>'closedproject','prefix' => 'projects')
				);
				?>
				</li>
			<?php } ?>
			<?php if($this->General->menu_permissions('credentials','credentialslist',$data[0]) == "1"){ ?>
				<li>
				<?php
				echo $this->Html->link("Credentials Management",
				array('controller'=>'credentials','action'=>'credentialslist','prefix' => 'credentials')
				);
				?>
				</li>
			<?php } ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Admin</span></a>
			<ul class="sub">
				<?php if($this->General->menu_permissions('contacts','contactslist',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Contacts", array('controller'=>'contacts','action'=>'contactslist','prefix' => 'contacts')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('payments','paymentslist',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Payments", array('controller'=>'payments','action'=>'paymentslist','prefix' => 'payments')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('reports','index',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Other Reports", array('controller'=>'reports','action'=>'index','prefix' => 'reports')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('permissions','admin_list',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Permission Management",
				array('controller'=>'permissions','action'=>'admin_list','prefix' => 'permissions')
				);	
				?></li>
			<?php } ?>
			</ul>
		</li>
		<li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Reports</span></a>
			<ul class="sub">
			<?php if($this->General->menu_permissions('attendances','report',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Attendance Report",
				array('controller'=>'attendances','action'=>'report','prefix' => 'attendances')
				);	
				?></li>
			<?php } ?>
				<?php if($this->General->menu_permissions('users','report',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Company Snapshot",
				array('controller'=>'users','action'=>'report','prefix' => 'admin')
				);	
				?></li>
				
			<?php } ?>
			<?php if($this->General->menu_permissions('leads','leadreport',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Lead Report",
				array('controller'=>'leads','action'=>'admin_leadreport','prefix' => 'leads')
				);	
				?></li>
			<?php } ?>
				<?php if($this->General->menu_permissions('projects','projectreport',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Project Report",
				array('controller'=>'projects','action'=>'projectreport','prefix' => 'projects')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('tickets','report',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Ticket Report",
				array('controller'=>'tickets','action'=>'report','prefix' => 'tickets')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('tickets','schedule',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Ticket Schedule",
				array('controller'=>'tickets','action'=>'schedule','prefix' => 'tickets')
				);	
				?></li>
			<?php } ?>
			<?php if($this->General->menu_permissions('evaluations','performancereport',$data[0]) == "1"){ ?>
				<li><?php echo $this->Html->link("Performance Report",
				array('controller'=>'evaluations','action'=>'performancereport','prefix' => 'evaluations')
				);	
				?></li>
			<?php } ?>
			      


                               <?php  $role_array=explode(',', $user[2]);
                            $d=count($role_array);
    
                                if($this->General->menu_permissions('evaluations','admin_performancereport',$data[0]) == "1"){ ?>      
                          
                            <?php } ?>






			</ul>
		</li>
		</ul>
		<div class="clear"></div>
		</div>
		<div class="clear"></div>
		</div>

		<!--  start nav -->
 
</div>
<div class="clear"></div>
<!--  start nav-outer -->

</div>
<!--  start nav-outer-repeat................................................... END -->
 
 <div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer">

<!-- start content -->
<div id="content">

<?php echo $this->Flash->render();  ?>

 <div id="newsFlash">
 <span>Notifications :</span>
 <ul id="js-news" class="js-hidden">
 
    <li class="news-item"><a href="#">Read Knowledgebase on Regualar basis for recent Company Policy update.</a></li>
    <li class="news-item"><a href="#">Read & Share your skill in Learning Center.</a></li>
    <li class="news-item"><a href="#">Update [from when you are free] on dashboard of Company ERP.</a></li>
    <li class="news-item"><a href="#">Update your dropbox for current projects.</a></li>
	<li class="news-item"><a href="#">View Company ERP Dashboard for recent announcements.</a></li>
    <li class="news-item"><a href="#">Please update your TICKET STATUS daily.</a></li>
    <li class="news-item"><a href="#">Please update Attendance on daily basis.</a></li>
    <li class="news-item"><a href="#">Please update your PROJECT DEADLINE.</a></li>
	<li class="news-item"><a href="#">Attach your ticket(s) to respective project.</a></li>
    <li class="news-item"><a href="#">Switch off your Computer before leaving.</a></li>
    <li class="news-item"><a href="#">Switch off AC / Fan / Light when not needed.</a></li>
</ul>
 </div>
<h4 style="float:right">If anyone facing any issue using this system, please email at <a href="mailTo:admin@vlogiclabs.com">admin@vlogiclabs.com</a></h4>

<div id="page-heading"><h1><?php echo $title_for_layout; ?></h1></div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowleft.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowright.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td class="view_text" valign="top">
		<?php $content_for_layout; ?>
		<?php echo $this->fetch('content'); ?>
	</td>
	<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
<!--<div style="float: right; margin-top: 6px; margin-right: 20px;" id="social_button">
<a target="_blank" href="http://www.facebook.com/vlogiclabs"><img src="/images/facebook-logo.png"></a> &nbsp;&nbsp;
<a target="_blank" href="http://www.youtube.com/user/vlogiclabs"><img height="30px" src="/images/youtube-logo.png"></a>  &nbsp;
<a target="_blank" href="http://twitter.com/vlogiclabs"><img height="30px" src="/images/twitter-icon.jpg"></a>  &nbsp;
<a target="_blank" href="http://www.linkedin.com/company/vlogiclabs"><img height="30px" src="/images/linkedin-icon.jpg"></a> -->
</div>
	<!--  start footer-left -->
	<div id="footer-left">
	 &copy; Copyright <?php //echo SITE_NAME; ?> <?php //echo $html->link(BASE_URL,BASE_URL);?>. All Rights Reserved.
	</div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script type="text/javascript">
	$(document).ready(function(){
	$('#ui-datepicker-div').hide();
	});
</script>
</body>
</html>
