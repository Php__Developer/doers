<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>plugins/images/afterlogin_logo.png">
<title>Doers are online for all your needs</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">

<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">

 <div class="login-box login-sidebar">
    <div class="white-box">

<?php echo $this->fetch('content'); ?>


    </div>
  </div>

</section>
<!-- jQuery -->
<script src="<?php echo BASE_URL;?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
