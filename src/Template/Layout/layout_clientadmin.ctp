<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout;?></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<?php print $html->charset('UTF-8') ?>
<script>
var base_url = "<?php echo BASE_URL; ?>";
</script>
<?php echo $html->css(array("screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style")); ?>
<?php echo $javascript->link(array('jquery-1.5.1.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery'))	//ui.core
?>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript"> 
    $(function () {
        $('#js-news').ticker();
	//	$('.ticker-title').append("<span>Hello</span>");
    });
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<![if !IE 7]>

<!--  styled select box script version 1 -->
<?php echo $javascript->link(array('jquery.selectbox-0.5')); ?>

<![endif]>
<?php echo $javascript->link(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?>
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL."/css/ie.css"?>" />
<![endif]-->
</head>
<?php $user = $this->Session->read("SESSION_ADMIN"); ?>
<body> 
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">
<?php echo $html->image(BASE_URL."images/beta.jpg",array('style'=>'float:right')); ?>
	<!-- start logo -->
	<div id="logo">
	</div>
	<!-- end logo -->
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat START -->
<div class="nav-outer-repeat"> 
<!--  start nav-outer -->
<div class="nav-outer">

	
		<!-- start nav-right -->
		<div id="nav-right">
		
			<div class="nav-divider">&nbsp;</div>
			<?php 
					$clientSession = $this->General->getClientSession(); 
					$clientName = $clientSession[1]; 
				?>
					<span id="clientName"><?php echo "Hello <i>".$clientName; ?></i></span>
			<div class="nav-divider">&nbsp;</div>
			<?php 
				echo $html->link(
				$html->image(BASE_URL."images/shared/nav/nav_logout.gif",array("alt" => 'Logout')),
				array('controller'=>'contacts','action'=>'clientlogout'),
				array('escape'=>false,'title'=>'Logout','id'=>'logout')
				);	
			
			?>
			<div class="clear">&nbsp;</div>
			
		</div>
		<!-- end nav-right -->
		
	
 
</div>
<div class="clear"></div>
<!--  start nav-outer -->

</div>
<!--  start nav-outer-repeat................................................... END -->
 
 <div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer">

<!-- start content -->
<div id="content">

<?php echo $session->flash();  ?>

<div id="page-heading"><h1><?php echo $title_for_layout; ?></h1></div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowleft.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"><?php //echo $html->image(BASE_URL."images/shared/side_shadowright.jpg", array("alt"=>"Activated","width"=>"20", "height"=>"300"))?></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td class="view_text" valign="top">
		<?php echo $content_for_layout; ?>
	</td>
	<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
<div style="float: right; margin-top: 6px; margin-right: 20px;" id="social_button">
</div>
	<!--  start footer-left -->
	<div id="footer-left">
	 &copy; Copyright <?php echo "Vooap.com ::" ?>  All Rights Reserved.
	</div>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
 
</body>
</html>
<?php echo $this->element("sql_dump"); ?>