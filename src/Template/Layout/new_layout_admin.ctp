<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>plugins/images/afterlogin_logo.png">
<title>Doers are online for all your needs</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- <link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet"> -->

<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<script>
var base_url = "<?php echo $this->Url->build('/', true); ?>";
//console.log(base_url); 
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
/*  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-19175540-9', 'auto');
  ga('send', 'pageview');*/

</script>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<?php if(isset($customErrors)){?>
<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-top alerttop2" style="display: block;">
<center><h4 class="error_text"> 
       <?php  echo $customErrors['username'];?>
    </h4></center>
<a class="closed" href="#">×</a>
</div><?php 
  }?>

<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-top alerttop2" style="display: none;">
<center><h4 class="error_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-success myadmin-alert-top alerttop1" style="display: none;">
<center><h4 class="success_text"></h4></center>
<a class="closed" href="#">×</a>
</div>
<div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop ng-binding successwrapperdv"  style="">
    <span><i class="fa fa-thumbs-up "></i> <span class="successmsg"></span></span>
  </div>

<div id="wrapper">
<div class="alert alert-loading myadmin-alert-top alerttop succesfontwrapper"> <span class="succesfont"></span></div>
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part">
<?php echo $this->Html->link(
      $this->Html->image(BASE_URL."plugins/images/afterlogin_logo.png",array("alt" => "home",'class'=>'logo p-l-20',"title"=>"Doers are online for all your needs")),
      array('controller'=>'users','action'=>'login' , 'prefix' =>'admin','class'=>'logo' ),
      array('escape'=>false,'title'=>SITE_NAME,'class'=>'logo')
      );
  ?>
      <span class="hidden-xs">
        </span>
      </div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        <li>
        <!--
          <form role="search" class="app-search hidden-xs">
            <input type="text" placeholder="Search..." class="form-control">
            <a href=""><i class="fa fa-search"></i></a>
          </form>
          -->
        </li>
      </ul>

      <ul class="nav navbar-top-links navbar-right pull-right">
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
          <div class="notify"><span class=""></span><span class=""></span></div>
          </a>
          <ul class="dropdown-menu mailbox animated bounceInDown">
            <li>
              <!--<div class="drop-title">You have 4 new messages</div> -->

                  <div class="drop-title"><?php    $data=$this->General->mytickets();
                 echo   "You have ".count($data['open'])." opened ticket(s)" ;?> </div>
                 </li>
            <li>
              <div class="message-center">
              <?php
                $data=$this->General->mytickets();
                $tickets = $data['tickets'];
                $counter = 0;
                foreach($tickets as $ticket){
                   if($ticket['status'] == 1 && $counter !== 4){
                 ?>
              <a href="#">
                <div class="user-img">
                <!--<img src="<?php echo BASE_URL; ?>plugins/images/users/pawandeep.jpg" alt="user" class="img-circle">  -->
                <?php $image=$ticket['User_From']['image_url'];
                if(!empty($image)){
                echo $this->Html->image("employee_image/".$image ,array("title" => 'Profile Pic',"alt" => 'Profile Pic',"class"=>"img-circle"));
                }else{
                echo $this->Html->image(BASE_URL."images/shared/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"class"=>"img-circle"));
                };?>
                <span class="profile-status online pull-right"></span>
              </div>
                <div class="mail-contnet">
                  <h5><?php echo $ticket['User_From']['first_name'].' '.$ticket['User_From']['last_name'] ;?></h5>
                  <span class="mail-desc"><?php echo $ticket['title'] ;?></span> <span class="time"><?php echo isset($ticket['modified']) ?  $ticket['modified']->format('d:m:Y g:i A'): "" ;?></span>
                </div>
                </a>
                <?php
                    $counter++;
                   }
                 }?>
                <!--<a href="#">
                <div class="user-img"> <img src="<?php echo BASE_URL; ?>plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Sonu Nigam</h5>
                  <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                </a>
                <a href="#">
                <div class="user-img"> <img src="<?php echo BASE_URL; ?>plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Arijit Sinh</h5>
                  <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="<?php echo BASE_URL; ?>plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Pavan kumar</h5>
                  <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                </a>  -->
              </div>
            </li>
            <li>
          <?php echo $this->Html->link("<strong>See all Tickets</strong> <i class='fa fa-angle-right'></i>",
            array('controller'=>'tickets','action'=>'index','prefix' => 'tickets'),['class'=>"text-center",'escape'=> false]
            );  ?>

        <!--<a class="text-center" href="javascript:void(0);"> <strong>See all Tickets</strong> <i class="fa fa-angle-right"></i> </a>-->


            </li>
          </ul>
          <!-- /.dropdown-messages -->
        </li>

        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="actvtshrtbthst"></span><span class="actvtspnthst"></span></div>
          </a>
          <ul class="dropdown-menu mailbox animated bounceInDown">
            <li> <div class="drop-title ntfcount">
                  All Notifications
                  </div>
            </li>
            <li>
              <div class="message-center notificationswrapper">
            
              </div>
            </li>
            <li>
            <!-- <a class="text-center" href="javascript:void(0);"> <strong>See all Tickets</strong> <i class="fa fa-angle-right"></i> </a> -->
                 <?php echo $this->Html->link("<strong>See all Activities</strong> <i class='fa fa-angle-right'></i>",
                array('controller'=>'users','action'=>'activities','prefix' => 'admin'),['class'=>"text-center",'escape'=> false]
                );  ?>
            </li>
          </ul>
          <!-- /.dropdown-messages -->
        </li>

        <!-- /.dropdown -->
        <!-- <li class="dropdown">
        


        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="actvtshrtbthst"></span><span class="actvtspnthst "></span></div>
          </a>
          <ul class="dropdown-menu dropdown-tasks animated slideInUp ">
          <li class="notificationswrapper"></li>
            <li> <a href="#">
              <div>
                <p> <strong>In Progress 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                </div>
              </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">

              <div>
                <p> <strong>Closed</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                </div>
              </div>

              </a> </li>
            
            <li class="divider"></li>
            <li> <a href="#">
              <div>
                <p> <strong>Open</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                </div>
              </div>
              </a> </li>

            <li class="divider"></li>
            <li> <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a> </li>
          </ul>
          /.dropdown-tasks
        </li> -->
        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
        <?php
        $data=$this->General->getuseralldata();
       
        $image= (count($data) > 0) ?  ($this->General->remote_file_exists(BASE_URL."img/employee_image/".$data[0]['image_url']))  ? $data[0]['image_url'] : "no_image.png" : "no_image.png" ;
        if(!empty($image)){
        echo $this->Html->image(BASE_URL."img/employee_image/".$image ,array("title" => 'Profile Pic',"alt" => 'Profile Pic',"class"=>"img-circle","width"=>"36"));
        }else{
        echo $this->Html->image(BASE_URL."images/shared/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"class"=>"img-circle","width"=>"36"));
        }
        ?>
        <input type="hidden" name="" class="emporiginagency" value="<?php echo (count($data) > 0) ?  $data[0]['agency_id'] : "";?>">
        <input type="hidden" name="" class="empcode" value="<?php echo (count($data) > 0) ?  $data[0]['employee_code'] : "";?>">
         <b class="hidden-xs">   <?php
         $session = $this->request->session();
           $userSession = $session->read("SESSION_ADMIN");
         echo $userSession[1];?></b> </a>
          <ul class="dropdown-menu dropdown-user animated flipInY">
            <li>

          <?php
          
            echo $this->Html->link("<i class='ti-wallet'></i> Performance",
            array('controller'=>'evaluations','action'=>'myperformance','prefix' => 'evaluations'),
            array('escape'=>false)
          );
          ?>
            </li>
            <!--
            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
              <li><a href="#"><i class="ti-settings"></i> Inbox</a></li>

            <li role="separator" class="divider"></li>
                -->
            <li>
        <?php
       echo $this->Html->link("<i class='ti-settings'></i> Accounts",
          array('controller'=>'users','action'=>'profile','prefix'=> 'admin'),
          array('escape'=>false,'title'=>'My Account','id'=>'profile')
          );
       ?>



            </li>
           
            <?php $user= $this->General->getsession();
                  $current_login= $user[0];
                  $result = $this->General->getsessiondata();
                  if($result){
                      $subusers = array_unique($result);
                       $multiauth = $this->General->multAuth($result);
                      ?>
                       <li role="separator" class="divider"></li>
                        <li>
                        <?php
                          echo $this->Html->link( "Destroy All Sessions",
                          array('controller'=>'agencies','action'=>'destroyallsessions','prefix'=> 'agencies','id' =>$multiauth[0]->id),
                          array('escape'=>false,'title'=>'Switch To: '.$multiauth[0]->first_name,'class' => 'm-l-20' )
                          );
                          ?>
                        <!-- <a href=""><h6>Destroy all sessions</h6></a> -->
                        </li>
                        <?php if($current_login==$subusers[0]){ ?>
                       <li><b>
                         <?php
                          echo $this->Html->link( $multiauth[0]->first_name,
                          array('controller'=>'agencies','action'=>'switchlogin','prefix'=> 'agencies','id' =>$multiauth[0]->id),
                          array('escape'=>false,'title'=>'Switch To: '.$multiauth[0]->first_name,'class' => 'm-l-20' )
                          );
                          ?>
                       <!-- <a href=""><?php echo  $multiauth[0]->first_name;?></a> -->
                          </b>
                       </li>
                       <?php }else {?>
                         <li>
                         <!-- <a href=""><?php echo  $multiauth[0]->first_name;?></a> -->
                         <?php
                          echo $this->Html->link( $multiauth[0]->first_name,
                          array('controller'=>'agencies','action'=>'switchlogin','prefix'=> 'agencies','id' =>$multiauth[0]->id),
                          array('escape'=>false,'title'=>'Switch To: '.$multiauth[0]->first_name,'class' => 'm-l-20' )
                          );
                          ?>
                         </li>
                        <?php }
                       foreach($multiauth[1] as $v){ ?>
                         <?php
                          echo $this->Html->link("<i class='fa fa-ban deact'></i>",
                          array('controller'=>'agencies','action'=>'userlogout','prefix'=> 'agencies','id' =>$v->id),
                          array('escape'=>false,'title'=>'Logout')
                          );
                          ?>
                        <?php if($current_login == $v->id){ ?>
                       <li>
                         <b>
                          <?php
                            echo $this->Html->link( $v->first_name,
                            array('controller'=>'agencies','action'=>'switchlogin','prefix'=> 'agencies','id' =>$v->id),
                            array('escape'=>false,'title'=>'Switch To: '.$v->first_name,'class' => 'm-l-20' )
                            );
                            ?>
                        </b>
                      </li>
                       <?php }else {?>
                        <li>
                        <?php
                          echo $this->Html->link( $v->first_name,
                          array('controller'=>'agencies','action'=>'switchlogin','prefix'=> 'agencies','id' =>$v->id),
                          array('escape'=>false,'title'=>'Switch To: '.$v->first_name,'class' => 'm-l-20' )
                          );
                          ?>
                       <!--  <a href="" class="m-l-20"><?php echo  $v->first_name;?></a>  -->
                        </li>
                        <?php 
                        }?>
                <?php  }
                } ?>

            <li role="separator" class="divider"></li>
            <li>
              <?php
      echo $this->Html->link("<i class='fa fa-power-off'></i> Logout",
        array('controller'=>'users','action'=>'logout','prefix'=> 'admin' ),
        array('escape'=>false,'title'=>'Logout')
        );
      ?>

      </li>
      <li>

      </li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- .Megamenu -->
        <li class="mega-dropdown">
          <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Alerts</span> <i class="icon-options-vertical"></i></a>
          <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">
            <li class="col-sm-3">
              <ul class="nostatusupdate">
              <!--Data is Appeneded Through Ajax see common.js webworkers-->
              </ul>
            </li>
            <li class="col-sm-3">
              <ul class="nominaltickets">
                <!--Data is Appeneded Through Ajax see common.js webworkers-->
              </ul>
            </li>
            <li class="col-sm-3">
              <ul class="onleavusers">
                <!--Data is Appeneded Through Ajax see common.js webworkers-->
              </ul>
            </li>
            <li class="col-sm-3">
            <!--
              <ul>
                <li class="dropdown-header">Charts</li>
                <li> <a href="flot.html">Flot Charts</a> </li>
                <li><a href="morris-chart.html">Morris Chart</a></li>
                <li><a href="chart-js.html">Chart-js</a></li>
                <li><a href="peity-chart.html">Peity Charts</a></li>                                     <li><a href="knob-chart.html">Knob Charts</a></li>
                <li><a href="sparkline-chart.html">Sparkline charts</a></li>
                <li><a href="extra-charts.html">Extra Charts</a></li>
              </ul>
              -->
            </li>
            <li class="col-sm-12 m-t-40 demo-box">
               <div class="row">
               <!--
                  <div class="col-sm-2"><div class="white-box text-center bg-purple"><a href="../eliteadmin-inverse/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 1</a></div></div>
                  <div class="col-sm-2"><div class="white-box text-center bg-success"><a href="../eliteadmin/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 2</a></div></div>
                  <div class="col-sm-2"><div class="white-box text-center bg-info"><a href="../eliteadmin-ecommerce/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 3</a></div></div>
                  <div class="col-sm-2"><div class="white-box text-center bg-inverse"><a href="../eliteadmin-horizontal-navbar/index3.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 4</a></div></div>
                  <div class="col-sm-2"><div class="white-box text-center bg-warning"><a href="../eliteadmin-iconbar/index4.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 5</a></div></div>
                  <div class="col-sm-2"><div class="white-box text-center bg-danger"><a href="https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820" target="_blank" class="text-white"><i class="linea-icon linea-ecommerce fa-fw" data-icon="d"></i><br/>Buy Now</a></div></div>
                  -->
               </div>
            </li>
          </ul>
        </li>
        <!-- /.Megamenu -->
        <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
        <!-- /.dropdown -->
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>
  <!-- Left navbar-header -->
  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
      <ul class="nav menusdata" id="side-menu">
        <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          
        </li> -->
       


      <!--   <li> <a href="index.html" class="waves-effect active"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> HRM <span class="fa arrow"></span> </span></a>
         <ul class="nav nav-second-level">
            <?php
          $data=$this->General->getUsersession();
            if($this->General->menu_permissions('employees','employeelist',$data[0]) == "1"){ ?>
          <li>
          <?php
          echo $this->Html->link("Employees",
          array('controller'=>'employees','action'=>'employeelist','prefix' => 'employees')
          );
          ?></li>
        <?php } ?>


          <li>
          <?php echo $this->Html->link("Add Attendance",
          array('controller'=>'attendances','action'=>'add','prefix' => 'attendances')
          );
          ?></li>

        <?php if($this->General->menu_permissions('appraisals','appraisalslist',$data[0]) == "1"){ ?>
          <li>
          <?php
          echo $this->Html->link("Appraisals",
          array('controller'=>'appraisals','action'=>'appraisalslist','prefix' => 'appraisals')
          );
          ?></li>
        <?php } ?>
        <?php if($this->General->menu_permissions('employees','employeelist',$data[0]) == "1"){ ?>
          <li>
          <?php
          echo $this->Html->link("HRM Settings",
          array('controller'=>'employees','action'=>'settings','prefix' => 'employees')
          );
          ?></li>
        <?php } ?>
        <?php if($this->General->menu_permissions('staticpages','dashboardeditor',$data[0]) == "1"){ ?>
          <li>
          <?php
            echo $this->Html->link("Dashboard Editor",
            array('controller'=>'static_pages','action'=>'dashboardeditor','prefix' => 'static_pages')
          );
          ?></li>
        <?php } ?>
        <?php if($this->General->menu_permissions('evaluations','evaluationslist',$data[0]) == "1"){ ?>
          <li>
          <?php
            echo $this->Html->link("Performance Manager",
            array('controller'=>'evaluations','action'=>'evaluationslist','prefix' => 'evaluations')
          );
          ?></li>
        <?php } ?>
        <?php if($this->General->menu_permissions('evaluations','monthlyperformance',$data[0]) == "1"){ ?>
          <li>
          <?php
            echo $this->Html->link("Monthly Performance",
            array('controller'=>'evaluations','action'=>'monthlyperformance','prefix' => 'evaluations')
          );
          ?></li>
        <?php } ?>


          </ul>
        </li> -->
<!-- <li> <a href="#" class="waves-effect"><i data-icon="7" class="linea-icon linea-basic fa-fw text-danger"></i> <span class="hide-menu text-danger">Tools<span class="fa arrow"></span></span></a>
  <ul class="nav nav-second-level two-li">
  <li><?php echo $this->Html->link("Tickets",
        array('controller'=>'tickets','action'=>'index','prefix' => 'tickets')
        );
        ?></li>
  </ul>
</li> -->
        <!-- <li><a href="inbox.html" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Jobs <span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
          <li><?php echo $this->Html->link("Billings", array('controller'=>'billings','action'=>'billingslist','prefix' => 'billings'));
        ?></li>
        <?php if($this->General->menu_permissions('projects','projectslist',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Project Management",
        array('controller'=>'projects','action'=>'projectslist','prefix' => 'projects')
        );
        ?></li>
      <?php } ?>

      <?php if($this->General->menu_permissions('projects','processaudit',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Process Audit",
        array('controller'=>'projects','action'=>'processaudit','prefix' => 'projects')
        );
        ?></li>
      <?php } ?>
      <li><?php echo $this->Html->link("Process Report",
        array('controller'=>'projects','action'=>'processreport','prefix' => 'projects')
        );
        ?>
      </li>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">CRM<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level two-li">
           <?php if($this->General->menu_permissions('testimonials','testimonialslist',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Testimonials",
        array('controller'=>'testimonials','action'=>'testimonialslist','prefix' => 'testimonials')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('leads','leadslist',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Lead Management",
        array('controller'=>'leads','action'=>'leadslist','prefix' => 'leads')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('generics','admin_setting',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Settings",
        array('controller'=>'generics','action'=>'admin_setting','prefix' => 'generics')
        );  ?>
        </li>
      <?php } ?>


          <?php if($this->General->menu_permissions('sales','profiledetail',$data[0]) == "1"){ ?>
      <li><?php echo $this->Html->link("Profile Details",
        array('controller'=>'sales','action'=>'profiledetail','prefix' => 'sales')
        );  ?>
        </li>
      <?php } ?>

          </ul>
        </li>
        <li> <a href="forms.html" class="waves-effect"><i data-icon="&#xe00b;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Operations<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
        <?php if($this->General->menu_permissions('hardwares','hardwareslist',$data[0]) == "1"){ ?>
        <li>
        <?php
        echo $this->Html->link("Assets Management",
        array('controller'=>'hardwares','action'=>'hardwareslist','prefix' => 'hardwares')
        );
        ?>
        </li>
      <?php } ?>
      <?php if($this->General->menu_permissions('employees','admin_seatreport',$data[0]) == "1"){ ?>
        <li>
        <?php
        echo $this->Html->link("Seat Report",
        array('controller'=>'employees','action'=>'seatreport','prefix' => 'employees')
        );
        ?>
        </li>
      <?php } ?>
      <?php if($this->General->menu_permissions('projects','closedproject',$data[0]) == "1"){ ?>
        <li>
        <?php
        echo $this->Html->link("Closed Project",
            array('controller'=>'projects','action'=>'closedproject','prefix' => 'projects')
        );
        ?>
        </li>
      <?php } ?>
      <?php if($this->General->menu_permissions('credentials','credentialslist',$data[0]) == "1"){ ?>
        <li>
        <?php
        echo $this->Html->link("Credentials Management",
        array('controller'=>'credentials','action'=>'credentialslist','prefix' => 'credentials')
        );
        ?>
        </li>
      <?php } ?>
          </ul>
        </li>
        <li> <a href="tables.html" class="waves-effect"><i data-icon="O" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Admin<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
            <?php if($this->General->menu_permissions('contacts','contactslist',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Contacts", array('controller'=>'contacts','action'=>'contactslist','prefix' => 'contacts')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('payments','paymentslist',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Payments", array('controller'=>'payments','action'=>'paymentslist','prefix' => 'payments')
        );
        ?></li>
      <?php } ?>

      <?php if($this->General->menu_permissions('permissions','setting',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Settings",
        array('controller'=>'permissions','action'=>'setting','prefix' => 'permissions')
        );
        ?></li>
      <?php } ?>

      <?php if($this->General->menu_permissions('permissions','setPermissions',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Allot Permissions",
        array('controller'=>'permissions','action'=>'setPermissions','prefix' => 'permissions')
        );
        ?></li>
      <?php } ?>


      <?php if($this->General->menu_permissions('permissions','admin_list',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Permission Management",
        array('controller'=>'permissions','action'=>'admin_list','prefix' => 'permissions')
        );
        ?></li>
      <?php } ?>

      <?php if($this->General->menu_permissions('sales','saleslist',$data[0]) == "1"){ ?>
      <li><?php echo $this->Html->link("Profile Management",
        array('controller'=>'sales','action'=>'saleslist','prefix' => 'sales')
        );  ?>
        </li>
      <?php } ?>
      <?php if($this->General->menu_permissions('paymentmethods','payment_method',$data[0]) == "1"){ ?>
      <li><?php echo $this->Html->link("Payment Method",
        array('controller'=>'payment_methods','action'=>'payment_method','prefix' => 'payment_methods')
        );  ?>
        </li>
      <?php  } ?>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
<?php if($this->General->menu_permissions('attendances','report',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Attendance Report",
        array('controller'=>'attendances','action'=>'report','prefix' => 'attendances')
        );
        ?></li>
      <?php } ?>
        <?php if($this->General->menu_permissions('users','report',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Company Snapshot",
        array('controller'=>'users','action'=>'report','prefix' => 'admin')
        );
        ?></li>

      <?php } ?>
      <?php if($this->General->menu_permissions('leads','admin_leadreport',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Lead Report",
        array('controller'=>'leads','action'=>'admin_leadreport','prefix' => 'leads')
        );
        ?></li>
      <?php } ?>
        <?php if($this->General->menu_permissions('projects','projectreport',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Project Report",
        array('controller'=>'projects','action'=>'projectreport','prefix' => 'projects')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('tickets','report',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Ticket Report",
        array('controller'=>'tickets','action'=>'report','prefix' => 'tickets')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('report','index',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Other Reports", array('controller'=>'reports','action'=>'index','prefix' => 'reports')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('tickets','schedule',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Ticket Schedule",
        array('controller'=>'tickets','action'=>'schedule','prefix' => 'tickets')
        );
        ?></li>
      <?php } ?>
      <?php if($this->General->menu_permissions('evaluations','performancereport',$data[0]) == "1"){ ?>
        <li><?php echo $this->Html->link("Performance Report",
        array('controller'=>'evaluations','action'=>'performancereport','prefix' => 'evaluations')
        );
        ?></li>
      <?php } ?>


          </ul>
        </li> -->

<li class="pull-right"> <?php
       echo $this->Html->link("Tickets",
          array('controller'=>'tickets','action'=>'index','prefix'=> 'tickets'),
          array('class'=>'text-danger'));
       ?>
</li>

 




        <!--
        <li> <a href="widgets.html" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Widgets</span></a> </li>
        <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="F" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Multi level<span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="javascript:void(0)">Second Level Item</a> </li>
            <li> <a href="javascript:void(0)">Second Level Item</a> </li>
            <li> <a href="javascript:void(0)" class="waves-effect">Third Level <span class="fa arrow"></span></a>
              <ul class="nav nav-third-level">
                <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                <li> <a href="javascript:void(0)">Third Level Item</a> </li>
                <li> <a href="javascript:void(0)">Third Level Item</a> </li>
              </ul>
            </li>
          </ul>
        </li>
        -->

</ul>
    </div>
  </div>
  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
           <h4 class="page-title"><?php if(isset($title_for_layout)){
                                              echo $title_for_layout;
                                          }else{
                                              echo "Dashboard";
                                          }
          ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
         <!--
          <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>-->
        <div class="col-lg-9">
        <?php echo $this->Flash->render(); ?>
        </div>
          <ol class="breadcrumb">
            <li><a href="#">Welcome</a></li>
            <li class="active">Doers.online</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->

      <!-- /.row -->

      <!--row -->

      <!-- /.row -->
      <!--row -->

      <!-- /.row -->
      <!-- .right-sidebar -->
        <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
          <div class="r-panel-body">
           <!-- <ul>
              <li><b>Layout Options</b></li>

              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
            </ul>
           <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul> -->
            <ul class="m-t-20 chatonline">
              <li><b>All Projects</b></li>
              <?php
              $data= $this->General->myprojects();
              if(count($data['total_projects'])){
              foreach($data['total_projects'] as $project) {
              $url ="/projects/projectdetails/".$project['id']."";?>
              <li><a href="<?php echo $this->Url->build($url, true);?>">
              <!--<img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> -->
              <?php
                echo $this->Html->image(BASE_URL."images/shared/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"class"=>"img-circle"));
              ;?>
             <span><?php echo $project['project_name'];
                       if($project['onalert']){ ?>
                  <small class="text-danger"><?php  echo "On Alert";?></small>
                       <?php  } else { ;?>
                   <small class="text-success"><?php  echo "Running Smooth";?></small>
                        <?php }?>

             </span>
             </a>
              </li>
              <?php
                  }
               }?>
              <!--<li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
              -->
            </ul>
          </div>
        </div>
      </div>

    <?php $content_for_layout; ?>
    <?php echo $this->fetch('content'); ?>
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center">2017 © All Rights Reserved @ Meander Software Private Limited

<div class="social pull-right"><a href="https://www.facebook.com/doers.are.online/" class="btn  btn-facebook" data-toggle="tooltip" title="" data-original-title="Facebook" target="_blank"> <i aria-hidden="true" class="fa fa-facebook"></i> </a></div>

    </footer>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<?php 
  
    echo  $this->Html->script(array('jquery-3','migrate','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax','common','listing','project'));
?>
<script src="<?php echo BASE_URL; ?>js/client.js"></script>
<script src="<?php echo BASE_URL; ?>js/socket.io.js"></script>
<!-- <script src="http://localhost:8080/nodemodules/socket.io.client/socket.io-file-client.js"></script>  -->
<script src="<?php echo BASE_URL; ?>js/common.js"></script>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->

<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<?php

 if( strtolower($this->request->params['controller']) == 'projects' && $this->request->params['action'] == 'projectdetails') {
      echo  $this->Html->script('plugins/bower_components/html5-editor/wysihtml5-0.3.0');
      echo  $this->Html->script('plugins/bower_components/html5-editor/bootstrap-wysihtml5');
      echo  $this->Html->script('jquery.slimscroll');
      echo $this->Html->css(array('wysiwyg-color'));
  }
 

  ;?>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->


<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>

<!-- Sparkline chart JavaScript -->
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
  --><script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <!-- <script src="/socket.io/socket.io.js"></script> -->

 
<script type="text/javascript">
  
   $(document).ready(function() {
      $.toast({
        heading: 'Problems?',
        text: 'In case of any problem related to doers, please consult to your agency admin or reach us by clicking on this <a href="https://join.skype.com/FAuawfVBmQZK" target="_blank">link</a>.',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'info',
        hideAfter: 3500, 
        
        stack: 6
      });
      

    });
</script>

 
<!--Style Switcher -->
<!-- <script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script> -->
</body>
</html>

