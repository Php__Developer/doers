<!DOCTYPE html>  
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo BASE_URL; ?>plugins/images/afterlogin_logo.png">
    <title>Doers are online for all your needs</title>
    <link href="<?php echo BASE_URL; ?>js/admin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>js/admin/css/animate.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>js/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>js/admin/css/colors/blue.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
  

    <input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
    <script>
    var base_url = "<?php echo $this->Url->build('/', true); ?>";
    </script>
<style>
.navbar-header {
    position: fixed;
}
</style>
</head>


<body>
 <!-- Preloader -->
   <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <?php if(isset($error)){ 
    ?>
 <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" style="display:block;"> <i class="ti-user"></i> <?php echo $error ;?> <a href="#" class="closed">&times;</a> </div>
 <?php } ;?>

<div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-top alerttop2" style="display: none;">
<center><h4 class="error_text"></h4></center>
<a class="closed" href="#">×</a>
</div>

<script>

function validateForm(){
       var inputval=$('.indata').val();
        if(inputval ==""){
         $('.alerttop2').find(".error_text").html('');
           $('.alerttop2').find(".error_text").append('Please fill your contact details');
           $('.alerttop2').show();
          return false;
        }else{
          swal({   
            title: "",   
            text: " Thanks for contacting us, we will get back to you soon.",   
            type: "success",   
          });
      return false;
        }
  } 
  </script>
<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                  <?php
                        echo $this->Html->link('<b><img class="logo p-l-20" src="'.BASE_URL.'plugins/images/logo2.png" alt="home" /></b><span class="hidden-xs"><strong></strong></span>',
                        array('controller'=>'users','action'=>'goto'),
                        array('class'=>'btn default btn-outline','title'=>'Doers are online for all your needs','escape' => false)
                         );
                        ?>   
               <!--  <a class="logo" href="{{URL::route('index')}}"><b><img src="<?php echo BASE_URL; ?>plugins/images/doers_logo.png" alt="home" /></b><span class="hidden-xs"><strong></strong></span></a> -->


                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><!--<a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a> --></li>
                    <li>
                        <form role="search" class="app-search hidden-xs" onsubmit="return validateForm();">
                      <input type="text" placeholder="Submit your Contact details" class="form-control indata"> <a href=""> <i class="glyphicon glyphicon-lock locksubmit"></i> <!-- fa fa-search --></a> </form>
                     </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa icon-people "></i>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                       <div class="white-box">
                         
                           

                            <?php
                              echo $this->Form->create('User', [ 'url'=>'/','onsubmit' => '',"class"=>"form-horizontal form-material", 'id'=>'loginform','type' => 'post','prefix'=>'admin']);
                            ?>
                            <h3 class="box-title m-b-20">Sign In</h3>
                            <div class="form-group ">
                            <div class="col-xs-12">
                             <?php echo $this->Form->input("email",array("class"=>"form-control email_id","label"=>false,"div"=>false,
                                  "placeholder"=>"Email","required"=>"required"));
                              ?>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="col-xs-12">
                             <?php echo $this->Form->input("password",array("class"=>"form-control password","label"=>false,"div"=>false," placeholder"=>"Password","required"=>"required")); ?>
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-12">
                          <!--   <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox" name="remember">
                            <label for="checkbox-signup"> Remember</label>
                            </div> -->
                           <!--  <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> --> </div>
                            </div>
                            <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                            </div>
                           <!--  <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                            </div>
                            </div> -->
                            <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                         <!--    <p>Don't have an account?
                             <?php
                        echo $this->Html->link('SignUp',
                        array('controller'=>'agencies','action'=>'register','prefix' => 'agencies'),
                        array('class'=>'text-primary m-l-5','title'=>'','target' => '_blank','escape' => false)
                         );
                        ?> </p> -->
                            </div>
                            </div>
                           <?php echo $this->Form->end();?>






          </div>
                          
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa  fa-building-o"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <div class="white-box">
                              <?php
                        echo $this->Form->create('User', [ 'url'=>'/goto','onsubmit' => '',"class"=>"form-horizontal form-material", 'id'=>'loginform','type' => 'post','prefix'=>'admin']);

                       ?>
                                
                                  <h3 class="box-title m-b-20"></h3>
                                  <div class="form-group ">
                                    <div class="col-xs-12">
                                      <?php echo $this->Form->input("agency",array("class"=>"form-control","label"=>false,"div"=>false,
                                  "placeholder"=>"Agency Name","required"=>"required"));
                              ?>
                                    </div>
                                  </div>
                                  <div class="form-group text-center m-t-20">
                                    <div class="col-xs-12">
                                      <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Go</button>
                                    </div>
                                  </div>
                                    <p>Don't have an  account?
                             <?php
                        echo $this->Html->link('SignUp',
                        array('controller'=>'agencies','action'=>'register','prefix' => 'agencies'),
                        array('class'=>'text-primary m-l-5','title'=>'','target' => '_blank','escape' => false)
                         );
                        ?> </p>
                                  <div class="form-group m-b-0">
                                    <div class="col-sm-12 text-center">
                                     <!-- <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p> -->
                                    </div>
                                  </div>
                                <?php echo $this->Form->end();?>
                           </div>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> support<b class="hidden-xs"></b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="javascript:void(0)"><i class="icon-phone p-r-10"></i>ContactUs: 011-45578685</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>

               


                    <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class=""></i></a></li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
            </nav>
        <!-- Left navbar-header -->
      
        <!-- Left navbar-header end -->
        <!-- Page Content -->
                <div id="page-wrapper">
                  <div class="container-fluid">



                      <?php $content_for_layout; ?>
                      <?php echo $this->fetch('content'); ?>






                     <!-- /#page-wrapper -->
                   </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 © All Rights Reserved @ Meander Software Private Limited

<div class="social pull-right"><a href="https://www.facebook.com/doers.are.online/" class="btn  btn-facebook" data-toggle="tooltip" title="" data-original-title="Facebook" target="_blank"> <i aria-hidden="true" class="fa fa-facebook"></i> </a></div> </footer>

        </div>
        <!-- /#page-wrapper -->
    </div>
<?php 
    echo  $this->Html->script(array('jquery-3','migrate'));
?>
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->

<!--Counter js -->
<!--Morris JavaScript -->


<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script> -->
<!-- Custom Theme JavaScript -->


<script src="<?php echo BASE_URL; ?>js/admin/js/custom.min.js"></script>


<!-- <script src="<?php echo BASE_URL; ?>js/widget.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js'"></script> -->
<script src="<?php echo BASE_URL; ?>js/cbpFWTabs.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js">
  
</script>
    <!--Style Switcher -->
 

<script type="text/javascript">
      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();

$(document).ready(function(){
  $(".locksubmit").on("click", function(event){
     event.preventDefault();
     var inputval=$('.indata').val();
        if(inputval ==""){
         $('.alerttop2').find(".error_text").html('');
           $('.alerttop2').find(".error_text").append('Please fill your contact details');
           $('.alerttop2').show();
          return false;
        }else{
          swal({   
            title: "",   
            text: " Thanks for contacting us, we will get back to you soon.",   
            type: "success",   
          });
      return false;
        }

   });

});

</script>
<!--  <script src="{{url('plugins/bower_components/calendar/jquery-ui.min.js')}}"></script>
<script src="{{url('plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{url('plugins/bower_components/calendar/dist/jquery.fullcalendar.js')}}"></script> -->


  </body>

</html>
 
