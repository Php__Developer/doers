<?php
header ("Expires: Mon, 28 Oct 2008 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-type: application/vnd.ms-excel");
header ("Content-Disposition: attachment; filename=\"$filename.xls" );
header ("Content-Description: Generated Report" );
?>
<table border="1">
	<tr> 
		<td colspan='5' align='center'><b>Testimonial(s) Report<b></td> 
	</tr>
	<tr>		
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
	</tr>
	<tr>		
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
		<td class="tableTd">&nbsp;</td>
	</tr>  		
	<tr>
	<th class="tableTd">Author</th>
		<th class="tableTd">Description</th>
		<th class="tableTd">Keyword</th>
		<th class="tableTd">Type</th>
	</tr> 
	<?php //printing data
	foreach($crData as $val) { 
	$bgclr  = $val['status']  ? '#ffffff' : '#ff6544'; //if status is deactivated then red.
	?>
		<tr>
		<td bgcolor="<?php echo $bgclr;?>" align="right"><?php echo $val['author'] ?></td>
		<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo nl2br($val['description']) ?></td>
				<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo $val['keyword'] ?></td>	
				<td bgcolor="<?php echo $bgclr;?>"  align="right"><?php echo $val['type'] ?></td>	
		</tr>
	<?php
	}//end for
	?>
	</tr>		
</table>