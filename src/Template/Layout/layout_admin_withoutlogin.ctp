<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title_for_layout;?></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<?php print $this->Html->charset('UTF-8') ?>
<link rel="stylesheet" type="text/css" href="<?php  WEBROOT_DIR; ?>/css/screen.css">
<?php echo $this->Html->script(array('jquery-1.4.1.min','custom_jquery','jquery.pngFix.pack','common','listing')); ?>
<?php echo $this->Html->css(array('screen')); ?>
<script src="<?php  WEBROOT_DIR; ?>/js/jquery_latest.js"></script> 
<script src="<?php  WEBROOT_DIR; ?>/js/custom_jquery.js"></script>
<script src="<?php  WEBROOT_DIR; ?>/js/jquery.pngFix.pack.js"></script>
<script src="<?php  WEBROOT_DIR; ?>/js/common.js"></script>
<script src="<?php  WEBROOT_DIR; ?>/js/listing.js"></script>
<link rel="shortcut icon" href="<?php echo WEBROOT_DIR; ?>/img/favicon.ico">
<script>
var base_url = "<?php echo BASE_URL; ?>";
</script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
</head>
<?php $user = ($session->read("SESSION_ADMIN") !== null )? $session->read("SESSION_ADMIN") : '' ; ?>
<body id="login-bg"> 

<!-- Start: login-holder -->
<div id="login-holder">
	<!-- start logo -->
	<div id="logo-login">
		<?php echo $this->Flash->render(); ?>
		<?php 	echo $this->Html->link(
			    $this->Html->image("/webroot/images/erp_logo.png", ["alt" => "Brownies"]),
			   ['controller' => 'users', 'action' => 'login' , 'prefix' => 'admin'],
			    ['escape' => false]
			);

		;?>



		

		
	</div>
	<!-- end logo -->
		<h4 class="mailer" >If anyone facing any issue using this system, please contact your branch head</h4>
	<div class="clear"></div>

	 <?php echo $this->fetch('content'); ?>

</div>
<!-- End: login-holder -->
</body>
</html>
<?php // $this->element("sql_dump"); ?>