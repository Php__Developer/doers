<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="expires" content="0"> <!-- says that the cache expires 'now' -->
<meta http-equiv="pragma" content="no-cache"> <!-- says not to use cached stuff, if there is any -->
<meta content="IE=7" http-equiv="X-UA-Compatible" /> 
<meta content="e-magazine" name="keywords" />
<meta content="e-magazine" name="description" />
<meta content="English" name="language" />
<meta content="1 week" name="revisit-after" />
<meta content="global" name="distribution" />
<meta content="index, follow" name="robots" />
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<?php
  $session = $this->request->session();
        //$this->set('session',$session);

 $user = $session->read("SESSION_ADMIN");
//pr($user); die;

 print $this->Html->charset('UTF-8') ?>
<script>
var base_url = "<?php echo $this->Url->build('/', true); ?>";

//console.log(base_url); 
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"));
           $this->Html->css(array("jquery-ui/jquery-ui.min","jquery.datetimepicker.min","screen","jsDatePick_ltr.min","jquery_ui_datepicker","pro_drop_1","ticker-style","rating_simple"))
; ?>
<?php //echo $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple')) //ui.core
  echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) //ui.core
?>
<!--<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/fancybox/jquery.fancybox-1.3.4.pack.js"></script> -->
<script type="text/javascript"> 
    $(function () {
        $('#js-news').ticker();
  //  $('.ticker-title').append("<span>Hello</span>");
    });
</script>

<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<!--[if !IE 7]-->

<!--  styled select box script version 1 -->
<?php echo $this->Html->script(['jquery.selectbox-0.5']); ?>

<!--[endif]-->
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions','tinysort'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions','tinysort')); ?>
<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL."/css/ie.css"?>" />
<![endif]-->
</head>
<style>
.ajaxloaderdiv{
  height: 100%;
  width:100%;
  position: absolute;
  background-color: white; /* for demonstration */
  opacity:.8;
  top:0;
  display:none;
 
	}
.ajaxloader_img{
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  margin: auto; /* presto! */
}
</style>
<?php



  ?>
<body> 

<!-- start content-outer -->
<div >

<!-- start content -->
<div id="content">


 <?php


  //echo $content_for_layout; ?>




</div>
<!--  end content -->
</div>
<!--  end content-outer -->

 

    
<!-- start footer -->         

<!-- end footer -->
 <script type="text/javascript">
	$(document).ready(function(){
	$('#ui-datepicker-div').css('display','none');
		});
</script>
<div class="ajaxloaderdiv"><?php echo $this->Html->image('ajax_loader_gray.gif', array('alt' => 'CakePHP','class'=>'ajaxloader_img'));?> </div>

</body>
</html>