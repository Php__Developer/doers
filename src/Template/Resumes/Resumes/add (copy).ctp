<?php echo $this->Html->css(array("reports.css")); ?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
		 });
	});
	</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $this->Form->create('Resume',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
	<?php $this->Flash->render(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
		<!-- start id-form -->
		<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top"> Name:</th>
			<td>
				<?php  echo $this->Form->input("name",array("class"=>"inp-form","label"=>false,"div"=>false));?>
				<?php if(isset($errors['name'])){
							echo $this->General->errorHtml($errors['name']);
						} ;?>
			</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Contact:</th>
			<td>
			<?php echo $this->Form->input("contact",array("class"=>"inp-form","label"=>false,"div"=>false));?>
			<?php if(isset($errors['contact'])){
							echo $this->General->errorHtml($errors['contact']);
					} ;?>	
			</td>
		</tr>
		<tr>
			<th valign="top">Address:</th>
			<td><?php
				 echo $this->Form->input("address",array("type"=>"textarea","class"=>"form-textarea","label"=>false,"div"=>false));
				 //echo $this->Form->hidden("id");
            ?></td>
		</tr>
		<tr>
			<th valign="top">Technology:</th>
			<td><?php
				 echo $this->Form->input("technology",array("type"=>"textarea","class"=>"form-textarea","label"=>false,"div"=>false));
				 
            ?></td>
		</tr>
		<tr>
		<th valign="top">Experience:</th>
      <td> 
     <?php	$options = array();
			for($i=0;$i<=15;$i++){
					if($i==0){
						$options[$i] = '< 1 year';
					}elseif($i==1){
						$options[$i] = $i.' Year';
					}else{
						$options[$i] = $i.' Years';
					}
			}
				 echo $this->Form->input("experience",array("type"=>"select","class"=>"ourselect","options"=>$options,"label"=>false,"div"=>false));
            ?></td>
           <td> </td>
		</tr>
		<tr>
		<th valign="top">Others:</th>
      <td>
     <?php
				 echo $this->Form->input("others",array("type"=>"textarea","class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
           <td> </td>
		</tr>
		<tr>
		<th valign="top">Date Of Interview:</th>
      <td>
     <?php
				 echo $this->Form->input("created",array("type"=>"text","class"=>"inp-form","label"=>false,"div"=>false,"id"=>"startdate"));
            ?></td>	 
           <td> </td>
		</tr>
				<tr>
		<th valign="top">Offer:</th>
      <td>
     <?php
				 echo $this->Form->input("offer",array("type"=>"textarea","class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>	 
           <td> </td>
		</tr>
	
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
		<?php echo $this->Form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			  echo $this->Form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/Resumes/list'")); 
			?>
		</td>
		<td></td>
	</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Resume Management</h5>
           Resume Management will keep track of resumes information .
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $this->Html->link("Go To Listing",
            array('controller'=>'Resumes','action'=>'list')
            );	
            ?>
            </li> 
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->