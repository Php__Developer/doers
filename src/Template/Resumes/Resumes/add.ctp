<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <script src="<?php echo BASE_URL; ?>js/common.js"></script>

<script type="text/javascript">
$(function() {
    $("#bidding_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
    $("#nextfollow_date").datepicker({
        dateFormat: 'yy-mm-dd',
    });
   

});
</script>
<?php echo $this->Html->css(array('oribe')); ?>
	<!--  start content-table-inner -->
	 <div class="col-sm-6">
            <div class="white-box">


<?php echo $this->Form->create('Resume',array('url'=> ['action' => 'add'], 'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	
	<?php echo $this->Flash->render(); ?>

		<div class="form-group">
			<label for="exampleInputpwd1">Name*</label>
			<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php echo $this->form->input("name",array("type"=>"text","class"=>"form-control","label"=>false,"div"=>false)); ?>
			</div>
			<?php if(isset($errors['name'])){
				 echo $this->General->errorHtml($errors['name']);
			} ;?>
		</div>

		<div class="form-group">
			<label for="exampleInputpwd1">Contact*</label>
			<div class="input-group">
				<div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php echo $this->form->input("contact",array("type"=>"text","class"=>"form-control","label"=>false,"div"=>false)); ?>
			</div>
			<?php if(isset($errors['contact'])){
				 echo $this->General->errorHtml($errors['contact']);
			} ;?>
		</div>

		<div class="form-group"> 
               <label for="inputName" class="control-label">Address:</label>
                 <?php
		 echo $this->form->input("address",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
		?>
 		<?php if(isset($errors['address'])){
				 echo $this->General->errorHtml($errors['address']);
			} ;?>
      </div>


      <div class="form-group"> 
               <label for="inputName" class="control-label">Technologies:</label>
                 <?php
		 echo $this->form->input("technology",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
		?>
 		<?php if(isset($errors['technology'])){
				 echo $this->General->errorHtml($errors['technology']);
			} ;?>
      </div>


      <div class="form-group">
                       <label for="exampleInputpwd1">Experience:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
               <?php $options = array();
			for($i=0;$i<=15;$i++){
					if($i==0){
						$options[$i] = '< 1 year';
					}elseif($i==1){
						$options[$i] = $i.' Year';
					}else{
						$options[$i] = $i.' Years';
					}
			}
				echo $this->form->input("source",array("type"=>"select",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
            ?>
		</div>
		</div>

		 <div class="form-group"> 
               <label for="inputName" class="control-label">Others:</label>
                 <?php
		 echo $this->form->input("others",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
		?>
 		<?php if(isset($errors['others'])){
				 echo $this->General->errorHtml($errors['others']);
			} ;?>
      </div>

        <div class="form-group">
              <label for="exampleInputpwd1">Date Of Interview:*</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
				<?php
						echo $this->form->input("created",array("type"=>"text","class"=>"form-control","id"=>"bidding_date","label"=>false,"div"=>false));
		            ?>
           </div>
            <?php if(isset($errors['created'])){
							echo $this->General->errorHtml($errors['created']);
				} ;?>
       </div>

       <div class="form-group"> 
               <label for="inputName" class="control-label">Offer:</label>
                 <?php
		 echo $this->form->input("offer",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false));
		?>
 		<?php if(isset($errors['offer'])){
				 echo $this->General->errorHtml($errors['offer']);
			} ;?>
      </div>





<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'leads','action'=>'add','prefix' => 'leads'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/leads/add', true)));?>

 </div>
 </div>
</div>



<div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Resumes Management</h4>
          This section is used by User only to Manage senstive leads.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'resumes','action'=>'list'), array('style'=>'color:red;'));?>
            </li> 
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     <!-- /.right-sidebar -->
</body>
</html>


