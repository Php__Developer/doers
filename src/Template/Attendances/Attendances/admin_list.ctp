<?php echo $javascript->link(array('jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js')); ?>
<?php echo $html->css(array("jquery.fancybox-1.3.4.css")); ?>
<?php
$newUrl = "list".$urlString;
$urlArray = array(
	'field' 	=> $search1,
	'value' 	=> $search2
);
$paginator->options(array('url'=>$urlArray));
?>
<?php echo $form->create('Attendance',array('action'=>$newUrl,'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
			<table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
		<tr>
			<td width="14%">
				<b>Search by:</b>
				<?php
				$fieldsArray = array(
				''				  => 'Select',
				'User.username'  => 'Username'
				);
				echo $form->select("Attendance.fieldName",$fieldsArray,$search1,array("id"=>"searchBy","label"=>false,"style"=>"width:200px","class"=>"styledselect","empty"=>false),false); ?>
			</td>
			<td width="20%">
				<b>Search value:</b><br/>
				<?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "Attendance.status"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $form->input("Attendance.value1",array("id"=>"search_input","class"=>"top-search-inp","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
				?>
			</td>
			<td width="40%"><br/>
		  		<?php
				echo $form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
				echo $form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/attendances/list'"));				
				?>
			</td>
		</tr>
	</table>
		<br/>
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
				<th class="table-header-check"><a id="toggle-all" ></a> </th>
				<th class="table-header-repeat line-left minwidth-1"  width="21%"><?php echo $paginator->sort('Username', 'User.username');?></th>
				<th class="table-header-repeat line-left minwidth-1"  width="71%" colspan="31"><?php echo $paginator->sort('Days','Attendance.leave_reason');?></th>
				</tr>
				
				<?php if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $result):
			
			if(!$result['Attendance']['status']%2)$class = "alternate-row"; else $class = "";  ?>
				<tr class="<?php echo $class; ?>">
					<td><input  type="checkbox" name="IDs[]" value="<?php echo $result['Attendance']['id'];?>"/></td>
					<td><?php echo $result['User']['username']; ?></td>
			<?php 
					for($a=1;$a<=31;$a++)
					{?>
			<td><a  href="<?php echo BASE_URL.'admin/attendances/sendattendance/'.$result['Attendance']['id'];?>" class="addLinks fancybox"><?php echo $a;?></a></td>
		
			<?php }?>
					</tr>
				
				<?php $i++ ;
				endforeach; ?>
				<?php } ?> 
		<tr>
			<td colspan="33" class="no_records_found">No records found</td>
		</tr>
				</table>
				<!--  end product-table................................... --> 
			</div>
			<!--  end content-table  -->
	
		
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
			<td>
				<?php echo $paginator->prev('� Previous', array('class' => 'page-left'), null, array('class' => 'page-left')); ?>
				<div id="page-info"><?php echo $this->Paginator->counter(array('format' => ' Page<strong> %page%</strong> / %pages%',"id"=>"page-info")); ?></div>
				<?php echo $paginator->next('Next �', array('class' => 'page-right'), null, array('class' => 'page-right')); ?>
			</td>
			</tr>
			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>

	</td>
	<td>
	<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Leads Management</h5>
          This section is used by Admin and PM only to Manage attendences of employees.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Add attendence of yours",
            array('controller'=>'attendances','action'=>'add')
            );	
            ?>
            </li> 
  					
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->

	</td>

</table>
 
<div class="clear"></div>
 

</div>
<?php echo $form->end(); ?>
<!--  end content-table-inner  -->
