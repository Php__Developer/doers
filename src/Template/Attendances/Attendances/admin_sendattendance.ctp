  <?php echo $html->css(array("screen")); ?>
<?php echo $javascript->link(array('jquery-1.4.1.min','jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js'));?>
<script type="text/javascript" language="javascript">
		var succ = "<?php echo (!empty($success)?"1":"0"); ?>";
		if(succ == "1"){
			parent.location.reload(true); 
		}
</script>
  <!--  start content-table-inner -->
	<div id="content-table-inner" style="padding:6px 0px 0px 20px;">
	<h2>Send Attendance</h2>
	<?php echo $form->create('Attendance',array('action'=>'sendattendance','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="fancyboxform">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form"  style="font-size:12px">
		<tr>
			<th valign="top">Status:</th>
			<td><?php
				 echo nl2br($crData['Attendance']['status']);
            ?></td>
			<td></td>
		</tr>	
		<tr>
		<th valign="top">Time In:</th>
		<td><?php
				echo $form->input("time_in",array("class"=>"style-selected days","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		
		<tr>
		<th valign="top">Time Out:</th>
		<td><?php
				echo $form->input("time_out",array("class"=>"style-selected days","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
    <tr>
   
		
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $form->submit('Save',array('class'=>"form-submit",'div'=>false,'onclick'=>"return confirm('Are you sure timing is fillup correct?');"))."&nbsp;&nbsp;&nbsp;"; 
			
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->