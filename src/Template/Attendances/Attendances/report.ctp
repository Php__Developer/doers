<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#datetoshow" ).datepicker({
				dateFormat: 'dd-mm-yy'
			
			});
	
		});
		
		function submitmyform(){
			$('#adminreport').submit();
		}

		function verifyAttendance(id){
			//console.log(id);
			$('.successmessage').html('');
			$('.successmessage').show();
			href = base_url+"attendances/report"; 
			if($('#s'+id).is(':checked')) {
				var verify = 1;
			}else{
				var verify = 0;	
			}
			var date = $("#datetoshow").val();
			
			$.post(href,{'user_id':id,'date':date,'verified':verify},function(data){
						if(JSON.parse(data).status =='success'){
							if(JSON.parse(data).verified ==1){
								var veri = 'verified';
							} else{
								var veri = 'un-verified';
							}
							$('.successmessage').append('<div id="message-green" ><table cellspacing="0" cellpadding="0" border="0" width="100%"><tbody><tr><td class="green-left" style="height:30px;">Attendance '+veri+' Successfully!</td><td class="green-right"><a class="close-green"></a></td></tr></tbody></table></div>')
						}
						 $('.successmessage').delay(2000).fadeOut('slow'); 
			});	
		}
</script>
<style type="text/css">
	.successmessage_wrapper{
		height: 40px;
	}
</style>
<!--  start content-table-inner -->
	
	<?php echo $this->Form->create('Attendance',array('url' => ['action' => 'report'],'id'=>'adminreport','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<div class="white-box">
           
		<div class="col-md-2">
			<span><b>Select Date :</b></span>
			<?php
				//	$date = (!empty($date)? date("d/m/Y",strtotime($date)):date('d/m/Y') ); 
	echo $this->Form->input('datetoshow', array("type"=>'text',"class"=>"form-control","label"=>false,"div"=>false,"id"=>"datetoshow",'readonly'=>"readonly","value"=>$date,"onchange" =>"javascript :submitmyform()")); 
				?>
			<div class="addLinks">
				<?php echo $this->Html->link("View User Attendance",
					array('controller'=>'attendances','action'=>'userattendance'),
					array('class'=>'view_report popup_window','title'=>'User Attendance Report')
				);	
				?>
		</div>
		</div>
	<?php echo $this->Form->end(); ?>
	<!--  start table-content  -->
	
		
			<?php echo $this->Flash->render(); ?>
			
			<div class="white-box">
           <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
              <thead>
                <tr>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Name</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Date</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Time In</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Time Out</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">Hours</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Task</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Ticket Closed</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">HR Notes</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Verification</button></th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Status</button></th>
                      <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Action</button></th>
                </tr>
              </thead>
              <tbody>
					
				<?php if(count($report)>0){  
			$i = 1; //pr($report); die;
			foreach($report as $row): 
		
			//pr($row); 
			?>
				<tr>
					<td><?php  echo $row['first_name'].' '. $row['last_name']; ?></td>
					<td><?php echo $row['date']; ?></td>
					<td><?php echo date_create($row['date'].' '.$row['time_in'])->format('h:i A'); ?></td>
					<td><?php echo date_create($row['date'].' '.$row['time_out'])->format('h:i A'); ?></td>
					<td><?php echo $row['hours'].(($row['hours'] != "-")?' Hrs':''); ?></td>
					<td word-wrap: break-word;"><?php echo  nl2br($row['notes']); ?></td>
					<td><?php $tikCount = 0;
						foreach($closeToday as $closeTicket){
						($row['id'] == $closeTicket['to_id']) ? $tikCount++ : '' ; 
						} 
					if($tikCount != '0'){
                          $oldDate = $date;
						$date_1st=str_replace("/","-",$oldDate);
						$arr = explode('-', $date_1st);
						$dateformated = $arr[2].'-'.$arr[1].'-'.$arr[0];
						echo $this->Html->link($tikCount,
							array("controller" =>"tickets","action"=>"closereport",'prefix'=>'tickets','id' => $row['id'],'date' => $dateformated),
							array('class'=>'info-tooltip','style'=>'color:blue','title'=>'View tickets',"target"=>"_blank")
						);
					}else{
						echo $tikCount;
					}
					?>					
					</td>
					<td><?php 
					echo $row['hrnotes']; ?></td>
					<td>
					<?php if($row['verified']=="1"){
								$checked = "checked" ;
								$disabled = "";
					 }elseif($row['verified']=="0"){
								$checked = "" ;
								$disabled = "";
					}else{
								$checked = "" ;		
								$disabled = "disabled";
					}
					 ?>
					<?php
					 echo $this->Form->input("verified",array('type'=>'checkbox','id'=>'s'.$row['id'],"label"=>false,"class"=>'verified',"div"=>false,'hiddenField'=>true,"checked"=>$checked,"disabled"=>$disabled,"onclick"=>"javascript: verifyAttendance('".$row['id']."');"));
					// echo "<label for ='verified'>Verify </label><br/>";
					?>
					</td>
					<td><?php echo $row['status']; ?></td>
					<td>
						<?php
						if($row['hours'] == "-") {
						
							echo $this->Html->link("Add",
								array('controller'=>'attendances','action'=>'addall','id' => $row['id'],'date' =>date('Y-m-d'),'mode' => 'add'),
								array('class'=>'info-tooltip','title'=>'Add','target'=>'_blank')
							);
						} else {

							echo $this->Html->link("Edit",
								array('controller'=>'attendances','action'=>'addall','id' => $row['id'],'date' =>$row['date'],'mode' => 'edit'),
								array('class'=>'info-tooltip','title'=>'Edit','target'=>'_blank')
							);
						}
						?>
					</td>
				</tr>
				
				<?php $i++ ;
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="10" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
					      </tbody>
            </table>
          </div>

		<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<!--Style Switcher -->
