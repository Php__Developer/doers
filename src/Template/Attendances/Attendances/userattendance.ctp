<?php echo  $this->Html->css(array("screen","jquery_ui_datepicker")); ?>
<?php  $this->Html->script(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker')); 
echo  $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datepicker_latest'));	
?>

<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#datetoshow" ).datepicker({
				dateFormat: 'dd/mm/yy',
				//minDate: "01/01/2013", 
				maxDate :0
			});
		$( "#datefromshow" ).datepicker({
				dateFormat: 'dd/mm/yy',
				maxDate :0
			});
			$('body div#ui-datepicker-div').css({'display':'none'});
		});
		function validateForm(){
		 if($(".ourselect").val()==""){
			$("#showerror").html("<div style='color:red;font-size:12px;'> Please Select an user</div>");
			return false;
		 }
		}
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $this->Form->create('Attendance',array('url' => ['action' => 'userattendance'],'id'=>'adminreport','method'=>'POST','onsubmit' => 'return validateForm();',"class"=>"login")); ?>
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<div id="showerror"></div>
	<div id="topdiv">
		<div class="topdivset">
			<span><b>Select User :</b></span>
			<?php
				$options = $this->General->getuser_name();
				echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"ourselect",'options'=>$options,"label"=>false,"div"=>false));
				?>
		</div>
		<div class="topdivset">
			<span><b>To Date :</b></span>
			<?php
					$todate = (!empty($todate)? date("d/m/Y",strtotime($todate)):date('01/01/2013') ); 
					echo $this->Form->input('datetoshow', array("type"=>'text',"class"=>"inp-form","label"=>false,"div"=>false,"id"=>"datetoshow",'readonly'=>true,"value"=>$todate,"style"=>"height:36px;width:199px;")); 
				?>
		</div>
		<div class="topdivset">
			<span><b>From Date :</b></span>
			<?php
					$fromdate = (!empty($fromdate)? date("d/m/Y",strtotime($fromdate)):date('d/m/Y') ); 
					echo $this->Form->input('datefromshow', array("type"=>'text',"class"=>"inp-form","label"=>false,"div"=>false,"id"=>"datefromshow",'readonly'=>true,"value"=>$fromdate,"style"=>"height:36px;width:199px;")); 
				?>
		</div>
		<div class="topdivset">
		<?php
			echo $this->Form->submit('Save',array('class'=>"form-submit",'div'=>false));
			//

 echo $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".$this->Url->build('/', true)."attendances/userattendance'"));		

			//
		//	echo $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/attendances/userattendance'"));			?>
		</div>
		<div style="clear:both"></div>
	</div>
	<?php echo $this->Form->end(); ?>
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $this->Flash->render(); ?>
			<center><span style="font-weight:bold;"> User Attendance Sheet</span></center>
				<table style="margin-top:15px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
					<tr>
						<th class="userattend" width="15%">Date</th>
						<th class="userattend" width="10%">Time In</th>
						<th class="userattend" width="10%">Time Out</th>
						<th class="userattend" width="10%">Hours</th>
						<th class="userattend" width="15%">Task</th>
						<th class="userattend" width="15%">HR Notes</th>
						<th class="userattend" width="10%">Varification</th>
						<th class="userattend" width="15%">Status</th>
					</tr>
				<?php if(isset($report) && count($report)>0){
				foreach($report as $userReport): ?>
				<tr>
					<td><?php echo date("F d,Y",strtotime($userReport['date'])); ?></td>
					<td><?php echo $userReport['time_in']->format('H:i:s'); ?></td>
					<td><?php echo $userReport['time_out']->format('H:i:s'); ?></td>
					<td><?php echo $userReport['hours']; ?></td>
					<td><?php echo $userReport['notes']; ?></td>
					<td><?php echo $userReport['hrnotes']; ?></td>
					<td>
						<?php //echo $userReport['id']; 
						if($userReport['verified']==0) {
								echo "Not Verified" ;
							} else {
								echo "Verified" ; }
						?>
					</td>
					<td><?php echo $userReport['status']; ?></td>
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="clear"></div>
</div>
<!--  end content-table-inner  -->