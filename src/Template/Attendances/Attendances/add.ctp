<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<?php echo $this->Html->css(array('jquery-ui','jquery_ui_datepicker','popup')); ?>
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <!-- <script src="<?php echo BASE_URL; ?>js/datetimepicker_full.js"></script> -->
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <!-- <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script> -->
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
  <!-- <script src="<?php echo BASE_URL; ?>js/common.js"></script> -->
  <script type="text/javascript">
 $(document).ready(function() {
  //alert("ok");
  //$( "#startdate" ).removeClass('hasDatepicker');
/*$( ".attendancedate" ).datetimepicker({
        ampm: true,
        dateFormat: "dd-mm-yy",
        minDate:  "-7d",
     });*/ 
     var startDateTextBox = $('#range_example_1_start');
var endDateTextBox = $('#range_example_1_end');

startDateTextBox.datetimepicker({ 
  timeFormat: 'HH:mm',
  
  dateFormat: "dd-mm-yy",
  minDate:  "-1d",
  onClose: function(dateText, inst) {
    if (endDateTextBox.val() != '') {
      var testStartDate = startDateTextBox.datetimepicker('getDate');
      var testEndDate = endDateTextBox.datetimepicker('getDate');
      if (testStartDate > testEndDate)
        endDateTextBox.datetimepicker('setDate', testStartDate);
    }
    else {
      endDateTextBox.val(dateText);
    }
  },
  onSelect: function (selectedDateTime){
    endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
     var testStartDate = startDateTextBox.datetimepicker('getDate');
     var testEndDate = endDateTextBox.datetimepicker('getDate');
    hrsmnts(testStartDate, testEndDate);
  }
});
endDateTextBox.datetimepicker({ 
  timeFormat: 'HH:mm',
  dateFormat: "dd-mm-yy",
  onClose: function(dateText, inst) {
    if (startDateTextBox.val() != '') {
      var testStartDate = startDateTextBox.datetimepicker('getDate');
      var testEndDate = endDateTextBox.datetimepicker('getDate');
      if (testStartDate > testEndDate)
        startDateTextBox.datetimepicker('setDate', testEndDate);
    }
    else {
      startDateTextBox.val(dateText);
    }
  },
  onSelect: function (selectedDateTime){
    startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
    var testStartDate = startDateTextBox.datetimepicker('getDate');
    var testEndDate = endDateTextBox.datetimepicker('getDate');
      hrsmnts(testStartDate, testEndDate);
  }
});
    //hourminformat();

 function hrsmnts(startdate, enddate){
  // get total seconds between the times
  var delta = Math.abs(enddate - startdate) / 1000;

  // calculate (and subtract) whole days
  var days = Math.floor(delta / 86400);
  delta -= days * 86400;

  // calculate (and subtract) whole hours
  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;

  // calculate (and subtract) whole minutes
  var minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;
  var min = (minutes < 10) ? '0'+minutes : minutes;
  console.log(hours+':' +min);
  $('.workinghours').val(hours+':' +min);
  // what's left is seconds
  var seconds = delta % 60;  // in theory the modulus is not required
 }


  });
        

      </script>
<div class="row">
        <div class="col-md-12">
          <div class="white-box">
  <?php   if(($active_ticket + $close_ticket) < 6){?>
  <h3 class="box-title m-b-0">You have less than six active and closed tickets.Please add your tickets first.</h3>
  <!--<div class="addLinks" style='margin-left:-300px;'>-->
    <?php echo $this->Html->link("Add Ticket",
          array('controller'=>'tickets','action'=>'index','prefix' => 'tickets'),
          array('class'=>'text-muted m-b-30 font-13','alt'=>'ticket','style'=>'margin-left:490px;')
          );   } else { ?>
            <!--<form class="form-horizontal"> -->
              <?php echo $this->Html->link("View Attendance",
          array('controller'=>'attendances','action'=>'userview'),
          array('class'=>'text-muted m-b-30 font-13 popup_window','title'=>'Attendance Report')
        );  
        ?>
        <h3 class="box-title m-b-0">Add Attendence</h3>
             <?php echo $this->Flash->render(); ?>
                 <div class="form-group">
                     <label class="col-md-12"><span class="help"> <?php echo $this->Flash->render(); ?></span></label>
                      <div class="col-sm-12">
                      </div>
                </div>
          <?php echo $this->Form->create('Attendance',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"form-horizontal")); ?>
              <div class="form-group">
                <label class="col-md-12">Select <span class="help">Date(Time-In)</span></label>
                <div class="col-md-12">
                  <!--<input type="text" class="form-control" value="George deo...">-->
                  <?php  echo $this->Form->input("time_in",array("class"=>"form-control attendancedate","label"=>false,"div"=>false,"id"=>"range_example_1_start","readonly"=>true, "value"=>date('d-m-Y H:i')));;?>
                 
                </div>
                 <?php if(isset($errors['time_out'])){
                     echo $this->General->errorHtml($errors['add_date']);
                  } ;?>
              </div>
              <div class="form-group">
                <label class="col-md-12">Select <span class="help">Date(Time-Out)</span></label>
                <div class="col-md-12">
                  <!--<input type="text" class="form-control" value="George deo...">-->
                  <?php  echo $this->Form->input("time_out",array("class"=>"form-control attendancedate","label"=>false,"div"=>false,"id"=>"range_example_1_end","readonly"=>true, "value"=>date('d-m-Y H:i') ));;?>
                 
                </div>
                 <?php if(isset($errors['add_date'])){
                     echo $this->General->errorHtml($errors['add_date']);
                  } ;?>
              </div>
              <!--<div class="form-group">
                <label class="col-md-12" for="example-email">Email <span class="help"> e.g. "example@gmail.com"</span></label>
                <div class="col-md-12">
                  <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">
                </div>-->
                 <!-- <div class="form-group">
                     <label class="col-md-12"><span class="help">Time In</span></label>
                      <div class="col-sm-12">
                     <span class="time-in"></span>
                      </div>
                     <?php if(isset($errors['Inhour']) ||  isset($errors['Inmin'])  || isset($errors['Inmeridian'])){
                            echo $this->General->errorHtml($errors['add_date']);
                      } ;?>   
                </div>
              <div class="form-group">
                <label class="col-md-12"><span class="help">Time Out</span></label>
                <div class="col-md-12">
                   <span class="time-out"></span>
                </div>
                <?php if(isset($errors['Outhour']) ||  isset($errors['Outmin'])  || isset($errors['Outmeridian'])){
                        echo $this->General->errorHtml($errors['add_date']);
                        } ;?>
              </div> -->
              <div class="form-group">
                <label class="col-md-12" for="example-email"><span class="help">Work Hours</span></label>
                <div class="col-md-12">
                  <!--<input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">-->
                  <?php
                  echo $this->Form->input("hours",array("label"=>false,"div"=>false,"class"=>"form-control workinghours"));
                  ?>
                  <?php if(isset($errors['hours'])){
                  echo $this->General->errorHtml($errors['hours']);
                  } ;?>
                </div>
                </div>
              <div class="form-group">
                <label class="col-md-12">Task Description</label>
                <div class="col-md-12">
                  <!--<textarea class="form-control" rows="5"></textarea>-->
                  <?php
                  echo $this->Form->input("notes",array("type"=>"textarea","label"=>false,"div"=>false,"id"=>"textarea","class"=>"form-control","required"=>"required"));
                  // 'style'=>'width:520px;height:234px' echo //'<font color=maroon> Here you should update Today Task, Next Task or Any Alerts.</font>';
                  ?>
                   <?php if(isset($errors['notes'])){
                echo $this->General->errorHtml($errors['notes']);
                } ;?>
                </div>
               
              </div>
              <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-9">
                  <input type="submit" class="btn btn-info waves-effect waves-light m-t-10" value="Submit">
                </div>
              </div>
            </form>
          </div>

          <?php } ?>
        </div>
      </div>
