<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->script(array('allajax')); ?>
<script type="text/javascript">
	$(document).ready(function(){
		 $( "#startdate" ).datepicker({
	  maxDate: "0D",
	  //dateFormat: "dd-mm-YY"
	    dateFormat: "dd-mm-yy",
	  });	

		/*
		new JsDatePick({
		   useMode:2,
		   target:"startdate",
		   dateFormat:"%d/%m/%Y",
		   yearsRange:[2012,2020],
		   //selectedDate:,
		   limitToToday:true
	   }); */
           $('#postData').click(function(){
           // $('#postData').hide();
            });
  });
  
  <!----------------------Time Difference------------------------------------> 
 function timediffer(h1,h2,days,m1,m2,am1,am2)
{

var h1 = $("#AttendanceTimeInHour").val();
var h2 = $("#AttendanceTimeOutHour").val();
var am1=1;
var am2=2;
if($("#AttendanceTimeInMeridian").val()=='am'){
 am1=1;
}else{
am1=2;
}
if($("#AttendanceTimeOutMeridian").val()=='am'){
 am2=1;
}else{
am2=2;
}
var m1 = $("#AttendanceTimeInMin").val();
var m2 = $("#AttendanceTimeOutMin").val();
var days= $("#startdate").val();
var answer = "why";
var mdiff = 1;
var hdiff = 2;
pdays = parseInt(days,10);
ph1 = parseInt(h1,10);
ph2 = parseInt(h2,10);
pm1 = parseInt(m1,10);
pm2 = parseInt(m2,10);
if(am1 == 2 & ph1 < 12) ph1 = ph1 + 12;
if(am2 == 2 & ph2 < 12) ph2 = ph2 + 12;
//if(am1 == 1 & ph1 == 12) ph1 = 24;   // 
//if(am2 == 1 & ph2 == 12) ph2 = 24;
//if(am1 == 2 & am2 == 1 & ph2 < 24) ph2 = ph2 + 24;
//if(am1 == am2 & ph1 > ph2) ph2 = ph2 + 24;
if(pm2 < pm1){
pm2 = pm2 + 60;
ph2 = ph2 - 1;
}
mdiff = pm2 - pm1;
hdiff = (ph2 - ph1);
if(hdiff == 0) answer ='0 hours'+" "+ mdiff + ' minutes';
else if(hdiff == 1) answer = '1 hour and ' + mdiff + ' minutes'
else answer = hdiff + ' hours and ' + mdiff + ' minutes'

if(hdiff<0 || mdiff<0){
$('#error1').show();
$('#error1').text('Out Time should be greater than In Time.');
}
else if(mdiff<10){
$('#error1').hide();
$('#AttendanceHours').val(hdiff+":"+"0"+mdiff);
}
else if(hdiff>0 || mdiff>0){
$('#error1').hide();
$('#AttendanceHours').val(hdiff+":"+mdiff);
}

}
</script>
	<!--  start content-table-inner -->
	<div class="col-sm-12">
            <div class="white-box">
	<?php
	isset($resultData) ? $modal = $resultData: $modal = "StaticPages";
	 echo $this->Form->create($modal,array('url' => ['action' => 'addall'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php echo $this->Flash->render(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
	<tr>
			<th valign="top">User Name:</th>
			<td valign="top"><?php
				$options=$this->General->getuser_name();
				echo $this->Form->input("user_id",array( "type"=>'select',"class"=>"form-control",'options'=>$options,"label"=>false,"div"=>false,'default' => isset($selecteduser) ? $selecteduser : "" ));
				


            ?></td>			
	</tr>
	<tr>
			<th align="left" >Date:</th><td >
			<?php 
				//(!empty($result['date'])) ? $date = date_create($result['date'])->format('') : $date = date('d/m/');
					 if(isset($result['date'])){
					 $create = date_create($result['date']);
					 $date = $create->format('d/m/Y');
					 } else {
					 $date = date('d/m/Y');
					 }

					 if($mode =='edit'){
					 	$classname ='onedit';
					 } else{
					 	$classname ='onadd';
					 }
				 echo $this->Form->input("add_date",array("class"=>"form-control ","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=> $date));

            if(isset($errors['add_date'])){
							echo $this->General->errorHtml($errors['add_date']);
				} ;?>
				<input type="hidden" value="<?php echo ($mode == 'edit')?  $result['time_in']->format('Y-m-d H:i:s'): '';?>" class="time_in_hidden">
				<input type="hidden" value="<?php echo ($mode == 'edit')?  $result['time_out']->format('Y-m-d H:i:s'): '';?>" class="time_out_hidden">
				<input type="hidden" value="<?php echo $mode ;?>" class="mode">


				</td>
			
	</tr>
		<tr>
			<th align="left" >Time in</th>
			<td class = "time-in <?php echo $classname ;?>">
				<?php echo $this->Form->input("time_in",array("label"=>false,"div"=>false,'value'=>($mode == 'edit')?  $result['time_in']: ''));?>
				
			</td>
			 <?php if(isset($errors['Inhour']) ||  isset($errors['Inmin'])  || isset($errors['Inmeridian'])){
							echo $this->General->errorHtml($errors['add_date']);
				} ;?>
			</tr>
			<tr>
				<th align="left" >Time out</th>
				<td class = "time-out">
				<?php
					 echo $this->Form->input("time_out",array("label"=>false,"div"=>false,'value'=>($mode == 'edit')?  $result['time_out']: '' ));
				?></td>
				<?php if(isset($errors['Outhour']) ||  isset($errors['Outmin'])  || isset($errors['Outmeridian'])){
							echo $this->General->errorHtml($errors['add_date']);
				} ;?>
			</tr>
			<tr>
				<th align="left" >Work Hours:</th><td>

				<?php
					 echo $this->Form->input("hours",array("label"=>false,"div"=>false,"size"=>"2","onfocus"=>'timediffer();','value' => (isset($result['hours']))? $result['hours']: ""));
				?>
				 <?php if(isset($errors['hours'])){
							echo $this->General->errorHtml($errors['hours']);
				} ;?>		


				</td>
			</tr>
			<tr>

                                <!-- <tr>
				<th align="left" >Credit:</th><td>

				<?php
					 echo $this->Form->input("credit",array("label"=>false,"div"=>false,"size"=>"2"));
				?></td>
			</tr>
                        <tr>
				<th align="left" >Credit Remark:</th><td>

				<?php
				 echo $this->Form->input("credit_remark",array("label"=>false,"div"=>false,'style'=>'width:520px;height:100px'));
					 
				?></td>
			</tr>-->


              			<th align="left" >Task Description</th><td>

				<?php
					 echo $this->Form->input("notes",array("label"=>false,"div"=>false,'style'=>'width:520px;height:234px','value' => (isset($result['notes']))? $result['notes']: ""));
					 echo '<font color=maroon> Here you should update Today Task, Next Task or Any Alerts.</font>';
					 echo $this->Form->hidden("id",['value' => isset($result['id']) ? $result['id']: ""]);
					?>
					 <?php if(isset($errors['notes'])){
							echo $this->General->errorHtml($errors['notes']);
					} ;?>
				</td>
			</tr>
		
			<tr>
				<th valign="top">Status:</th>
				<td valign="top">
				<?php
				echo $this->Form->input("status",array( "type"=>'select',"class"=>"form-control",'options'=>array(	'halfday'  => 'Half Day',	'short leave'  => 'Short Leave' ,'leave'  => 'Leave' ,'present' 	 => 'Present' ),"label"=>false,"div"=>false));
				?>


				</td>
			</tr>
			
			<tr>
				<th valign="top">Verification:</th>
				<td valign="top"><?php
				echo $this->Form->input("verified",array( "type"=>'select',"class"=>"form-control",'options'=>array('1'=>'Done','0'=>'Not Done'),"label"=>false,"div"=>false));
				?></td>
			</tr>
			
			<tr>
				<th align="left" >HR Description</th>
				<td>
				<?php
				echo $this->Form->input("hrnotes",array("label"=>false,"div"=>false,'style'=>'width:520px;height:234px','value' => (isset($result['hrnotes']))? $result['hrnotes']: ""));
				echo '<div style="float:left;"><font color=maroon> Here HR or ADMIN should update issue related to any attendance entry (Like unpaid, compensatory, holiday etc.)</font></div>';
				?></td>
			</tr>
			<tr>
			<th>&nbsp;</th>
			<td valign="top">
 <button type="submit" class="btn btn-success">Save</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'attendances','action'=>'add','prefix' => 'attendances'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/attendances/add', true)));?>
</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	</div></div>
	<!--  end content-table-inner  -->