
 <!-- .row -->
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="css/oribe.css" rel="stylesheet">



<style>
.switchery.switchery-small {
    float: right;
    margin-left: 147px;
    margin-top: 12px;
}
.list-group-item-warning {
    padding: 11px;
}
</style>
<div class="row el-element-overlay m-b-40">


        <div class="col-md-12 ">
          <div class="col-md-6 ">
<div class="btn-group m-r-10">
                <button aria-expanded="true" data-toggle="dropdown" class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" type="button">Sort By <span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu animated flipInX drp_list">
                    <li><a href="#" class="1">Name</a></li>  
                    <li><a href="#" class="2">Type</a></li>
                    <li><a href="#" class="3">Created</a></li>
                    <li><a href="#" class="4">modified</a></li>
           
                </ul>
              </div>  </div>

 <div class="col-md-6">
                 <?php 
            echo $this->Html->link('<i class="fa  fa-plus"></i>',
            array('controller'=>'payment_methods','action'=>'payment_method'),
            ['escape' => false,"class"=>"btn btn-info btn-circle pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Add Payment Method"]
            );
            ?>
                </div>
       </div>
<br>
<br>
<br>

<script src="moment.js"></script>
<script src="moment-timezone-with-data.js"></script>
<div class="ajax">

  <div class="parent_data">
  <?php  if(count($resultData)>0){
				  $snl = 1;
				  foreach($resultData as $p):
				  	//pr($p);
				  	if(!$p['status']%2)$class = "nav nav-tabs list-group-item-info actioning redish"; else $class = "";  ?>

        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
          <div class="white-box">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs firstLISt" role="tablist">
              <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
              <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
            </ul>
            <!-- Tab panes  substring($row->parent->category_name,35);  -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane ihome active" >
                 <ul class="list-group ">
			<li class="list-group-item list-group-item-danger 1"><b>Name :</b> 
			<?php $str = strip_tags($p['name']); 
			echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
			<li class="list-group-item list-group-item-success 2"><b>Payment Method :</b> 
			<?php echo $p['type'];?></li>
			<li class="list-group-item list-group-item-info 3"><b>Created :</b> 
			<?php echo date(DATE_FORMAT, strtotime($p['created'])); ?></li>
			<li class="list-group-item list-group-item-warning 4"><b>Modified :</b> 
			<?php echo date(DATE_FORMAT, strtotime($p['modified'])); ?></li>
			
		   
			<?php 
			if(isset($p['modified'])){
			$modd=$p['modified']->format("Ymd");
			}else{
			$modd="";
			}
			?>
			<?php 
			if(isset($p['created'])){
			$created=$p['created']->format("Ymd");
			}else{
			$created="";
			}
			?>

  <input type="hidden" class="testicons" value = "<?php echo $this->General->ENC($p->id);?>" >

            <ul class="<?php echo $class;?>" role="tablist">
              <li><?php
                         echo $this->Html->link(
		                  '<i class="fa fa-cog"></i>',
		                  array('controller'=>'payment_methods','action'=>'edit','id'=>$p['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
		                  );
                    ?></li>
						   <li>
                         <?php
                           echo $this->Html->link(
		                  '<i class="ti-close"></i>',
		                  array('controller'=>'payment_methods','action'=>'delete','id'=>$p['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
		                  );
		                  ?>
                        </li>
                       
	                </ul>
	            <input id="last_update" class="hidden created" value="<?php echo $created;?>">

	                 <input id="last_update" class="hidden modd" value="<?php echo $modd;?>">
	            </ul>

            </div>
              <div role="tabpanel" class="tab-pane iprofile">
                <div class="col-md-6">
                  <h3>Lets check profile</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane imessages">
                <div class="col-md-6">
                  <h3>Come on you have a lot message</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane isettings">
                <div class="col-md-6">
                  <h3>Just do Settings</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            </div>
        </div>
         <?php $snl++ ;
				endforeach; ?>
				<?php } else { ?>

				<?php
				}
			?>
         </div>
        <!-- /.usercard-->
      </div>
<!-- /.row -->
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="js/jquery.slimscroll.js"></script>
<!-- Magnific popup JavaScript -->
 <?php 
    echo  $this->Html->script(array('paymentmethods','moment'));
?>
<script type="text/javascript">
function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}

      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>


