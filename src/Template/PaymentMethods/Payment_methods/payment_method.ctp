<div class="col-sm-6">
   <div class="white-box">

<?php echo $this->Form->create('PaymentMethod',array('url' => ['action' => 'payment_method'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>

	<!-- start id-form -->

   
	<h2>Provide Detail</h2>
		 <div class="form-group"> 
   <label for="inputName" class="control-label">Name</label>
			 <span style="color:red;"> &nbsp;*</span>
			 <div style="color:red;" id="error1"></div>
			<div class="error-message">
			     <?php


  echo $this->Form->input("name",array("class"=>"form-control","id"=>"inputName","label"=>false,"div"=>false));
                 ?>
                   <?php if(isset($errors['name'])){
							echo $this->General->errorHtml($errors['name']);
							} ;?>
</div>
  </div>               
<div class="form-group">
   <label for="inputName" class="control-label">Payment Method</label>    
	 <span style="color:red;"> &nbsp;*</span>
		<div id="error1" style="color:red;"></div>
		   <div class="error-message">

			 <?php  $options=array(
			          '' =>'---Select--',
			          'Paypal'    =>  'Paypal',
                      'Self'    => 'Self'
                      );
			          echo $this->Form->input("type",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));
              ?>
                <?php if(isset($errors['type'])){
							echo $this->General->errorHtml($errors['type']);
							} ;?>
		
 </div>
</div>			
	
<div class="form-group">

			<?php echo $this->Form->button('Submit',array('class'=>"btn btn-success",'div'=>false))."&nbsp"; 
			echo $this->Form->button('Reset',array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>"location.href='".$this->Url->build('/', true)."payment_methods/paymentMethod'"));

			?>	
</div>
 </div>
</div>


 <div class="col-sm-6">
    <div class="white-box">
	  <?php $user = $session->read("SESSION_ADMIN"); ?>
	     <div class="form-group">
					<h3>Payment Method</h3>
                      <h4>This section is used by Admin.</h4>
					
			          
                         <?php
                           echo $this->Html->link("Payment Method Listing ",
                            array('controller'=>'payment_methods','action'=>'paymentMethodlist'),
            array('style'=>"color:red;")
                            );
                         ?>
                    
  				
   </div>   
 </div>
</div>