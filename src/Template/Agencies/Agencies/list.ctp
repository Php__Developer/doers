<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
    <script src="<?php echo BASE_URL; ?>js/common.js"></script>
<input id="pe" class="hidden pe" value="<?php echo $pagename;?>">


<style>
.switchery.switchery-small {
    float: right;
    margin-left: 147px;
    margin-top: 12px;
}
.list-group-item-warning {
    padding: 11px;
}
</style>
<?php echo $this->Form->create('Agencies',array('url' => ['action'=>'list'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<div class="row el-element-overlay m-b-40">
<span class="pull-right text-success"> <?= $this->Paginator->counter([
    'format' => ' {{page}} of {{pages}} Pages'
]) ?></span>

	<div class="col-md-12">
	<div class="col-md-3 searchingdata">
	<?php
				$fieldsArray = array(
			    ''	 => 'Select',
				'Agencies.agency_name'     => 'Agency',
				
				'Agencies.first_name'  => 'First Name',
        'Agencies.last_name'  => 'Last Name',
        'Agencies.username'  => 'Username'
				);
			
       echo $this->Form->input("Agencies.fieldName",array("type"=>"select",'options'=>$fieldsArray,"class"=>"form-control","label"=>false,"div"=>false,"id"=>"status")); ?>

      </div>
	<div class="col-md-4">

	<?php
				$display1   = "display:none";
				$display2   = "display:none";
				if($search1 != "Agencies.status"){
					$display1 = "display:block";
				}else{
					$display2 = "display:block";
				}
					echo $this->Form->input("Agencies.value1",array("id"=>"search_input","class"=>"form-control","style"=>"width:200px;$display1", "div"=>false, "label"=> false,"value"=>$search2));
				?>
</div>
<div class="col-md-4">
	<?php
	echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
  echo $this->Html->link("Reset",  ['controller'=>'agencies','action'=>'list','prefix' => 'agencies'],['class'=>'fcbtn btn btn-danger btn-outline btn-1b resetproject']);  ?>

<div class="btn-group m-r-10">
                <button aria-expanded="true" data-toggle="dropdown" class="btn fcbtn btn btn-warning btn-outline btn-1b dropdown-toggle waves-effect waves-light" type="button">Sort By <span class="caret"></span></button>
                <ul role="menu" class="dropdown-menu animated flipInX drp_list">
                    <li><a href="#" class="1">Agency Name</a></li>  
                    <li><a href="#" class="2">First Name</a></li>
                    <li><a href="#" class="3">Last Name</a></li>
                    <li><a href="#" class="4">User Name</a></li>
           
                </ul>
              </div>
	</ul>

	 <?php 
            echo $this->Html->link('<i class="fa  fa-plus"></i>',
            array('controller'=>'agencies','action'=>'add'),
            ['escape' => false,"class"=>"btn btn-info btn-circle pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Add New Role"]
            );
            ?>
<?php echo $this->Form->end(); ?>
</div>
<div class="col-md-1 paginateproject">
	<ul class="pagination paginate-data">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
	</div>



<br>
<br>
<br>


<div class="ajax">

  <div class="parent_data">
  <?php 


  if(count($resultData)>0){
			$i = 1;
			foreach($resultData as $p):				  
				  	if(!$p['status']%2)$class = "nav nav-tabs list-group-item-info actioning redish"; else $class = "nav nav-tabs list-group-item-info";  ?>

        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 alldata">
          <div class="white-box">
          <!-- Nav tabs -->
            <ul class="nav nav-tabs firstLISt" role="tablist">
              <li role="presentation" class="active"><a href="#ihome" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-home"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#iprofile" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-user"></i></span> </a></li>
              <li role="presentation" class="" style="display:none;"><a href="#imessages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span><i class="ti-email"></i></span></a></li>
              <li role="presentation" class="" style="display:none;"><a href="#isettings" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="true"><span><i class="ti-settings"></i></span></a></li>
            </ul>
            <!-- Tab panes  substring($row->parent->category_name,35);  -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane ihome active" >
                 <ul class="list-group ">
			<li class="list-group-item list-group-item-danger 1"><b>Agency Name :</b> 
			  <?php echo  $p['agency_name']; ?>
			</li>
			<li class="list-group-item list-group-item-success 2"><b>First Name :</b> 
		      <?php $str = strip_tags($p['first_name']); 
			echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
      <li class="list-group-item list-group-item-success 2"><b>Last Name :</b> 
          <?php $str = strip_tags($p['last_name']); 
      echo $name = substr($str,0,19).((strlen($str)>19)? "...":""); ?></li>
			<li class="list-group-item list-group-item-info 3"><b>User Name :</b> 
           <a class="popover-primary" type="button" title="" data-toggle="popover" data-placement="up" data-content="<?php echo $p['username'];?>" data-original-title="User Name" aria-describedby="popover507663">
		<?php $str = strip_tags($p['username']); 
      echo $name = substr($str,0,13).((strlen($str)>13)? "...":""); ?></a></li>


  <input type="hidden" class="testicons" value = "<?php echo $this->General->ENC($p->id);?>" >

            <ul class="<?php echo $class;?>" role="tablist">
              <li><?php
                         echo $this->Html->link(
		                  '<i class="fa fa-cog"></i>',
		                  array('controller'=>'agencies','action'=>'edit','id'=>$p['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit','target'=>'_blank']
		                  );
                    ?></li>
						   <li>
                         <?php
                           echo $this->Html->link(
		                  '<i class="ti-close"></i>',
		                  array('controller'=>'agencies','action'=>'delete','id'=>$p['id']),
		                  ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Delete']
		                  );
		                  ?>
                        </li>


                        <li><?php
                         echo $this->Html->link(
                      '<i class="fa fa-user"></i>',
                      array('controller'=>'agencies','action'=>'aslogin','id'=>$p['id']),
                      ['escape' => false,'class' => 'btn default btn-outline','title'=>'Edit',]
                      );
                    ?></li>


                       
	                </ul>
	            </ul>

            </div>
              <div role="tabpanel" class="tab-pane iprofile">
                <div class="col-md-6">
                  <h3>Lets check profile</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, arcu, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane imessages">
                <div class="col-md-6">
                  <h3>Come on you have a lot message</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane isettings">
                <div class="col-md-6">
                  <h3>Just do Settings</h3>
                  <h4>you can use it with the small code</h4>
                </div>
                <div class="col-md-5 pull-right">
                  <p>Vulputate eget, fringilla vel, aliquet nec, daf adfd vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a.</p>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
            </div>
        </div>
         <?php $i++ ;
				endforeach; ?>
				<?php } else { ?>

				<?php
				}
			?>
         </div>
        <!-- /.usercard-->
      </div>
<!-- /.row -->
<?php echo $this->Html->script(array('cbpFWTabs.js')) ?>
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

<!-- Magnific popup JavaScript -->

<script type="text/javascript">
function veryfycheck()
	{
	alert("1) Either billing for this project is already added for this week."+'\n'+
	"2) Please verify all billing till previous week for this project and then reload this page again to add billing.");
	
	}

      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>

 <?php 
    echo  $this->Html->script(array('agency'));
?>