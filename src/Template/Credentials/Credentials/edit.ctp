<?php  $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'));?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions'));
$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions')); ?>
<script src="<?php echo BASE_URL; ?>js/common.js"></script>
<script type="text/javascript">
    
	$(document).ready(function(){
		$('#type').change(function(){
		var type=$('#type').val();
			if(type=='other'){
				$('#other1').html('<input id="other" type="text" name="data[Credential][other]"/>');
				}
				else
				$('#other').remove();
			});
	})
	</script>
	<!--  start content-table-inner -->
<?php echo $this->Html->css(array('oribe')); ?>
    <!--  start content-table-inner -->
	  <div class="col-sm-6">
            <div class="white-box">

		<?php echo $this->Form->create($credential,array('method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	
	<?php echo $this->Flash->render(); ?>

 <div class="form-group">
                       <label for="exampleInputpwd1">Primary User</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
       <?php
				$options=$this->General->getuser_name();
			echo $this->Form->input("user_id",array("type"=>"select","class"=>"form-control","label"=>false,"div"=>false,"options"=>$options));
            ?>
</div></div>


  <div class="form-group">
                       <label for="exampleInputpwd1">Username</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div><?php
				echo $this->Form->input("username",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div></div>


<div class="form-group">
                       <label for="exampleInputpwd1">Password</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		       <?php
				echo $this->Form->input("password",array("type"=>"text","id"=>"crpass","class"=>"form-control","label"=>false,"div"=>false));
            ?>   </div></div> 	
                  <span class="password" onclick="$('#crpass').val($.password(12,true));">Generate Password</span>

  
         <div class="form-group">
                       <label for="exampleInputpwd1">Type</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
	<?php
			
		   $options=array( 'gmail'    => 'Gmail',
		                   'dropbox'  => 'Dropbox',
                                   'skype'    => 'Skype',
                                   'webmail'  => 'Webmail',
			           'other'=>'Other'
                      );
					  
					  
				 echo $this->Form->input("type",array("class"=>"form-control","label"=>false,"div"=>false));
           
			
             ?>
			</select></div></div>




			<?php 

            if(isset($credential)){
            //	pr($credential->type);
            	//die();
            
        ?>
			<span id='other1'><?php if($credential->type=='other')

			echo $this->Form->input("other",array("class"=>"form-control","label"=>false,"div"=>false));
            }
			?>
			  
			</span> 
			
		

 <div class="form-group">
                       <label for="exampleInputpwd1">Forwarded Email ID</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		<?php
				echo $this->Form->input("forworder_email",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div></div>
		




		 <div class="form-group">
                       <label for="exampleInputpwd1">Secondary User:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


		
         <?php
				$options = $this->General->getuser_name(); 

                   
                if(isset($credential['other_alloted_user']) && $credential['other_alloted_user'] !== ''){
                 
                    $cont_cats = explode(",",$credential['other_alloted_user']);
                }else{
                    $cont_cats = [];
                }
                // this select is not working properly so html select was added 
				 $this->Form->select("other_alloted_user",$options, ['type' => 'select','size'=>'10','multiple' => true,"class"=>"multiSelect",'default' => $cont_cats,'id'=> 'other-alloted-user']);
         	 ?>
                <select name="other_alloted_user[]" multiple size='10' class="form-control" id ="other-alloted-user" >
                <?php foreach($options as $key => $value) {;?>
                <option value="<?php echo $key;?>" <?php echo (in_array($key, $cont_cats))? 'selected': '' ;?> ><?php echo $value;?></option>
                 <?php };?>
                </select>
           
           </div></div>




	 <div class="form-group"> 
               <label for="inputName" class="control-label">Description:</label>

		<?php
				 echo $this->Form->textarea("description",array("class"=>"form-control","label"=>false,"div"=>false));
				echo $this->Form->hidden("id");
            ?></div>



    <div class="form-group">
                       <label for="exampleInputpwd1">Keyword</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
<?php
    echo $this->Form->input("keyword",array("class"=>"form-control","label"=>false,"div"=>false));
    ?></div></div>

		<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'credentials','action'=>'edit','prefix' => 'credentials','id'=>$id],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/credentials/edit', true)));?>

 </div>
 </div>
</div>
    <!-- end id-form  -->

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Credentials Management</h4>
          This section is used by Admin and PM only to Manage senstive credentials.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'credentials','action'=>'credentialslist'), array('style'=>'color:red;'));?>
               </li> 
               <li>
            <?php  echo $this->Html->link("Download Gmail : Excel",
            array('controller'=>'credentials','action'=>'exportci')
           , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Gmail : PDF",
            array('controller'=>'credentials','action'=>'cyberoam')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Dropbox : Excel",
            array('controller'=>'credentials','action'=>'exportcidropbox')
           , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Dropbox : PDF",
            array('controller'=>'credentials','action'=>'dropbox')
            , array('style'=>'color:red;'));?>
            </li>
  			<li>
            <?php  echo $this->Html->link("Download Skype : Excel",
            array('controller'=>'credentials','action'=>'exportciskype')
            , array('style'=>'color:red;'));?>
            </li>
				
  					<li>
            <?php  echo $this->Html->link("Download Skype : PDF",
            array('controller'=>'credentials','action'=>'skype')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Webmail : Excel",
            array('controller'=>'credentials','action'=>'exportciwebmail')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Webmail : PDF",
            array('controller'=>'credentials','action'=>'webmail')
            , array('style'=>'color:red;'));?>
            </li>
						
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     
    
      <!-- /.right-sidebar -->





