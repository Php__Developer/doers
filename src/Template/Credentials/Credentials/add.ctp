<?php  $this->Html->script(array('jquery-1.8.3.min','jquery.bind','common','listing','jsDatePick.min.1.3','jquery.validate','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon','stuHover','site','jquery.ticker','pGenerator.jquery','rating_simple'));?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />
<link rel="shortcut icon" href="<?php echo BASE_URL; ?>img/favicon.ico">
<?php echo $this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery-dimensions/jquery.dimensions','functions'));

$this->Html->script(array('jquery.selectbox-0.5_style_2','jquery.filestyle','custom_jquery','jquery.tooltip','jquery.dimensions','jquery.pngFix.pack','functions')); ?>
<script src="<?php echo BASE_URL; ?>js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#type').change(function(){
		var type=$('#type').val();
			if(type=='other'){
				$('#other1').html('<input id="other" type="text" name="data[Credential][other]"/>');
				}
				else
				$('#other').remove();
			});
	})

</script>
<?php echo $this->Html->css(array('oribe')); ?>
    <!--  start content-table-inner -->
	  <div class="col-sm-6">
            <div class="white-box">


<?php echo $this->Form->create('Credential',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php //$session->flash(); ?>
	
  <div class="form-group">
                       <label for="exampleInputpwd1">Primary User</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		 <?php
			     $options=$this->General->getuser_name();
				  echo $this->Form->input("user_id",array("type"=>"select","class"=>"form-control","label"=>false,"div"=>false,"options"=>$options));
            ?>
</div></div>

	
		  <div class="form-group">
                       <label for="exampleInputpwd1">Username</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		

				<?php
				 echo $this->Form->input("username",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['username'])){
							echo $this->General->errorHtml($errors['username']);
							} ;?></div>
          
			 <div class="form-group">
                       <label for="exampleInputpwd1">Password</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		
			
				<?php
				echo $this->Form->input("password",array("type"=>"text","id"=>"crpass","class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['password'])){
							echo $this->General->errorHtml($errors['password']);
							} ;?></div>
    	
           <span class="password" onclick="$('#crpass').val($.password(12,true));">Generate Password</span>
<!-- 			<a style="color:red" href="#" onclick="$('#crpass').val($.password(12,true));">Generate Password</a></td> -->



		 <div class="form-group">
                       <label for="exampleInputpwd1">Type</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		
			<?php
			
			 // Creating options for Type field
		   $options=array( 'gmail'    => 'Gmail',
					  'dropbox'     => 'Dropbox',
                      'skype'     => 'Skype',
                      'webmail'   => 'Webmail',
					  'other'=>'Other'
                      );
			echo $this->Form->input("type",array('type'=>'select',"id"=>"type",'options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
             ?></div></div>
			
            <span id='other1'></span> 
			

 <div class="form-group">
                       <label for="exampleInputpwd1">Forwarded Email ID</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
		
				<?php
				 echo $this->Form->input("forworder_email",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['forworder_email'])){
							echo $this->General->errorHtml($errors['forworder_email']);
							} ;?></div>
           
           	      	 <div class="form-group">
                       <label for="exampleInputpwd1">Secondary User:</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>


           <?php
			       $options=$this->General->getuser_name();
            ?>

 <select name="other_alloted_user[]" multiple size='10' class="form-control" id ="other-alloted-user" >
                <?php foreach($options as $key => $value) {;?>
                <option value="<?php echo $key;?>" ><?php echo $value;?></option>
                 <?php };?>
                </select>   


</div></div>

<?php 
  
    echo  $this->Html->script(array('jquery-3','migrate','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax','common','listing','project'));
?>
<script src="<?php echo BASE_URL; ?>js/client.js"></script>
<script src="<?php echo BASE_URL; ?>js/socket.io.js"></script>
<!-- <script src="http://localhost:8080/nodemodules/socket.io.client/socket.io-file-client.js"></script>  -->
<script src="<?php echo BASE_URL; ?>js/common.js"></script>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->

<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<?php

 if( strtolower($this->request->params['controller']) == 'projects' && $this->request->params['action'] == 'projectdetails') {
      echo  $this->Html->script('plugins/bower_components/html5-editor/wysihtml5-0.3.0');
      echo  $this->Html->script('plugins/bower_components/html5-editor/bootstrap-wysihtml5');
      echo  $this->Html->script('jquery.slimscroll');
      echo $this->Html->css(array('wysiwyg-color'));
  }
 

  ;?>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->


<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>

<!-- Sparkline chart JavaScript -->
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
  --><script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <!-- <script src="/socket.io/socket.io.js"></script> -->

 
<script type="text/javascript">
  
   $(document).ready(function() {
      $.toast({
        heading: 'Problems?',
        text: 'In case of any problem related to doers, please consult to your agency admin or reach us by clicking on this <a href="https://join.skype.com/FAuawfVBmQZK" target="_blank">link</a>.',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'info',
        hideAfter: 3500, 
        
        stack: 6
      });
      

    });
</script>




  <div class="form-group"> 
               <label for="inputName" class="control-label">Description:</label>
			<?php
				 echo $this->Form->textarea("description",array("class"=>"form-control","label"=>false,"div"=>false));
				 //echo $form->hidden("id");
            ?></div>




 <div class="form-group">
                       <label for="exampleInputpwd1">Keyword</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
 <?php
    echo $this->Form->input("keyword",array("class"=>"form-control","label"=>false,"div"=>false));
    ?></div></div>

<div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'credentials','action'=>'add','prefix' => 'credentials'],['class'=>"btn btn-inverse waves-effect waves-light"]);  
$this->Form->button("Reset",array('type'=>'button','class'=>"btn btn-inverse waves-effect waves-light",'div'=>false,'onclick'=>$this->Url->build('/credentials/add', true)));?>

 </div>
 </div>
</div>
    <!-- end id-form  -->

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Credentials Management</h4>
          This section is used by Admin and PM only to Manage senstive credentials.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'credentials','action'=>'credentialslist'), array('style'=>'color:red;'));?>
               </li> 
               <li>
            <?php  echo $this->Html->link("Download Gmail : Excel",
            array('controller'=>'credentials','action'=>'exportci')
           , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Gmail : PDF",
            array('controller'=>'credentials','action'=>'cyberoam')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Dropbox : Excel",
            array('controller'=>'credentials','action'=>'exportcidropbox')
           , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Dropbox : PDF",
            array('controller'=>'credentials','action'=>'dropbox')
            , array('style'=>'color:red;'));?>
            </li>
  			<li>
            <?php  echo $this->Html->link("Download Skype : Excel",
            array('controller'=>'credentials','action'=>'exportciskype')
            , array('style'=>'color:red;'));?>
            </li>
				
  					<li>
            <?php  echo $this->Html->link("Download Skype : PDF",
            array('controller'=>'credentials','action'=>'skype')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Webmail : Excel",
            array('controller'=>'credentials','action'=>'exportciwebmail')
            , array('style'=>'color:red;'));?>
            </li>
  					<li>
            <?php  echo $this->Html->link("Download Webmail : PDF",
            array('controller'=>'credentials','action'=>'webmail')
            , array('style'=>'color:red;'));?>
            </li>
						
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
     
    
      <!-- /.right-sidebar -->



</body>
</html>










 
  					
				