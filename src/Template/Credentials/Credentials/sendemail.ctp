<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">

<script>
var base_url = "<?php echo $this->Url->build('/', true); ?>";

//console.log(base_url); 
</script>

<?php
echo $this->Html->css(array("style_new")); ?>
<?php echo $this->Html->script(array('jquery-1.4.1.min','jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js'));?>
<style>


.note {
    color: tomato;
    font-size: small;
}
</style>
<script>

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


function validateForm(){
	 var email = $("#email-address").val();
	// alert(validateEmail(email));
		  if (validateEmail(email) == false) {
		    $ (".email").html("");
			    $(".email").append('Email address is not valid.');
			    $('.email').attr('style','color: #fb9678;');
			    $(".email").show();
           return false;
	      } 
     } 
</script>
<script type="text/javascript" language="javascript">
		var succ = "<?php echo (!empty($success)?"1":"0"); ?>";
		if(succ == "1"){
			parent.location.reload(true); 
		}
</script>
  <!--  start content-table-inner -->
	<div id="content-table-inner" style="padding:6px 0px 0px 20px;">
	<center><h2>Send Credentials</h2></center>

<?php echo $this->Form->create('Credential',array('url' => ['action' => 'sendemail'],'method'=>'POST',"onsubmit"=>"return validateForm();","class"=>"login")); ?>

<span class="e_l email"></span>
  <div class="col-lg-9">
        <?php echo $this->Flash->render(); ?>
        </div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="fancyboxform">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form"  style="font-size:12px">
		<tr>
			<th valign="top">Email address(s):</th>
			<td><?php
				 echo $this->Form->input("email_address",array("class"=>"form-control","label"=>false,"div"=>false,));
            ?><br/><span class="note">enter multiple email addresses seperated by comma<span></td>
			
		</tr>
	
		<tr>
			<th valign="top">Username:</th>
			<td><?php
				 echo nl2br($crData['username']);
            ?></td>
			<td></td>
		</tr>	
		<tr>
			<th valign="top">Password:</th>
			<td><?php
				 echo nl2br($crData['password']);
            ?></td>
			<td></td>
		</tr>	
		<tr>
			<th valign="top">Description:</th>
			<td><?php
				 echo nl2br($crData['description']);
            ?>
      </td>
			<td></td>
		</tr>	
		<tr>
			<th valign="top">keyword(s):</th>
			<td><?php
				 echo nl2br($crData['keyword']);
            ?>
      </td>
			<td></td>
		</tr>		
		
		<tr>
			<th valign="top">Message:</th>
			<td><?php
				 echo $this->Form->textarea("message",array("class"=>"form-textarea","label"=>false,"div"=>false,"style"=>"width:390px;height:103px"));
            ?></td>
			<td></td>
		</tr>
    <tr>
    
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $this->Form->hidden('id', array("value"=>$crData['id'])); 

			echo $this->Form->submit('Save',array('class'=>"fcbtn btn btn-success btn-outline btn-1b ",'div'=>false))."&nbsp;&nbsp;&nbsp;";

			?>



			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


<script type="text/javascript">
  
   $(document).ready(function() {
      $.toast({
        heading: 'Welcome to Doers.online',
        text: 'Use the predefined ones, or specify a custom position object.',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'info',
        hideAfter: 3500, 
        
        stack: 6
      });
      

    });
</script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
