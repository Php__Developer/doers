<script type="text/javascript">
	$(document).ready(function(){
		$('#type').change(function(){
		var type=$('#type').val();
			if(type=='other'){
				$('#other1').html('<input id="other" type="text" name="data[Credential][other]"/>');
				}
				else
				$('#other').remove();
			});
	})
	</script>
    <!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('Credential',array('action'=>'add','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Primary User:</th>
			<td><?php
			     $options  = $general->getuser();
				 echo $form->input("user_id",array("type"=>"select","class"=>"ourselect","label"=>false,"div"=>false,"options"=>$options));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Username:</th>
			<td><?php
				 echo $form->input("username",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Password:</th>
			<td><?php
				 echo $form->input("password",array("type"=>"text","id"=>"crpass","class"=>"inp-form","label"=>false,"div"=>false));
            ?>    	
            </td>
			<td><a style="color:red" href="#A" onclick="$('#crpass').val($.password(12,true));">Generate Password</a></td>
		</tr>
		<tr>
			<th valign="top">Type:</th>
			<td>	
			<?php
			
			 // Creating options for Type field
		   $options=array( 'gmail'    => 'Gmail',
					  'dropbox'     => 'Dropbox',
                      'skype'     => 'Skype',
                      'webmail'   => 'Webmail',
					  'other'=>'Other'
                      );
			echo $form->input("type",array('type'=>'select',"id"=>"type",'options'=>$options,"class"=>"ourselect","label"=>false,"div"=>false));    
             ?>
			</select>
            <span id='other1'></span> 
			</td>
		</tr>
		<tr>
			<th valign="top">Forwarded Email ID:</th>
			<td><?php
				 echo $form->input("forworder_email",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Secondary User:</th>
			<td><?php
					$options = $general->getUsersprofile(); 
				 echo $form->input("other_alloted_user",array("type"=>'select','size'=>'10','multiple' => true,'options'=>$options,"class"=>"multiSelect","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Description:</th>
			<td><?php
				 echo $form->input("description",array("class"=>"form-textarea","label"=>false,"div"=>false));
				 //echo $form->hidden("id");
            ?></td>
		</tr>
    <tr>
    <th valign="top">Keyword:</th>
    <td><?php
    echo $form->input("keyword",array("class"=>"inp-form","label"=>false,"div"=>false));
    ?></td>
    <td></td>
</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/credentials/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Credentials Management</h5>
          This section is used by Admin and PM only to Manage senstive credentials.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Go To Listing",
            array('controller'=>'credentials','action'=>'list')
            );	
            ?>
            </li> 
  					<li>
            <?php echo $html->link("Download Gmail : Excel",
            array('controller'=>'credentials','action'=>'exportci/cyberoam')
            );	
            ?>
            </li>
  					<li>
            <?php echo $html->link("Download Gmail : PDF",
            array('controller'=>'credentials','action'=>'download/cyberoam')
            );	
            ?>
            </li>
  					<li>
            <?php echo $html->link("Download Dropbox : Excel",
            array('controller'=>'credentials','action'=>'exportci/dropbox')
            );	
            ?>
            </li>
  					<li>
            <?php echo $html->link("Download Dropbox : PDF",
            array('controller'=>'credentials','action'=>'download/dropbox')
            );	
            ?>
            </li>
  			<li>
            <?php echo $html->link("Download Skype : Excel",
            array('controller'=>'credentials','action'=>'exportci/skype')
            );	
            ?>
            </li>
				
  					<li>
            <?php echo $html->link("Download Skype : PDF",
            array('controller'=>'credentials','action'=>'download/skype')
            );	
            ?>
            </li>
  					<li>
            <?php echo $html->link("Download Webmail : Excel",
            array('controller'=>'credentials','action'=>'exportci/webmail')
            );	
            ?>
            </li>
  					<li>
            <?php echo $html->link("Download Webmail : PDF",
            array('controller'=>'credentials','action'=>'download/webmail')
            );	
            ?>
            </li>
						
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->