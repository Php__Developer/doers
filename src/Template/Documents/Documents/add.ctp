<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
   <div class="row">
        <div class="row">
        <div class="col-sm-12">
         <h3 class="box-title">File Upload3</h3>
          <div class="white-box dragandrophandler" id="dragandrophandler">
            <center><label for="input-file-now-custom-2" id="inputlebel" class="text-center"> Click here or Drop a file here to upload!</label></center>
          </div>
          <input type="file" id="input-file-now-custom-2" class="brwsefile" data-height="500" />
          </div>
          </div>
          <div class="row uploadprogress m-t-20">
            <div class="white-box m-l-20 m-r-5">
              <div class="row">
                   <div class="col-sm-2">
                      Upload Status : <span class="upldprgrsprcnt">0</span>%
                  </div>
                  <div class="col-sm-8">
                  <div class="progress">
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">  </div>
                      </div>
                  </div>
                </div>
                <div class="col-sm-2">
                    <span class="donefiles">0</span> uploaded of total <span class="totalfiles">0</span> files
                </div>
              </div>
               

            </div>
              
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mail_listing">
                <div class="inbox-center">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th width="30"><div class="checkbox m-t-0 m-b-0 ">
                          </div></th>
                        <th colspan="5"> <div class="btn-group">
                          </div>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default waves-effect waves-light  dropdown-toggle reloaddocs" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-refresh"></i> </button>
                          </div></th>
                        <th class="hidden-xs" width="100"><div class="btn-group pull-right">
                            <!-- <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button> -->
                          </div></th>
                      </tr>
                    </thead>
                    <tbody class="resulssets">
                    <?php 
                    foreach($docs as $doc){ ;?>
                      <tr class="unread">
                        <td><div class="checkbox m-t-0 m-b-0">
                          </div></td>
                        <td class="hidden-xs"><!-- <i class="fa fa-star-o"></i> --></td>
                        <td class="hidden-xs" title="Uploaded By: <?php echo $doc['Creater']['first_name'].' '.$doc['Creater']['last_name'] ;?>"> <?php echo $doc['Creater']['first_name'].' '.$doc['Creater']['last_name'] ;?> </td>
                        <td class="max-texts"> <a href="#">
                        <?php if($doc['type'] == 'application/vnd.oasis.opendocument.spreadsheet'){ ;?>
                            <span class="label label-info m-r-10">Ods(Linux)</span> 
                        <?php  } else if(in_array($doc['type'],['image/jpg','image/png','image/jpeg'])){ ;?>
                                <span class="label label-success m-r-10">Image</span> 
                        <?php  } else if( $doc['type'] == 'application/pdf' ) { ;?>
                                <span class="label label-alert m-r-10">Pdf</span> 
                        <?php  } else if( $doc['type'] == 'application/msword' ) { ;?>
                                <span class="label label-info m-r-10">Ms Word</span> 
                        <?php  } ;?></a>
                        </td>
                        <td class="hidden-xs" title="Last Modifier : <?php echo $doc['Modifier']['first_name'].' '.$doc['Modifier']['last_name'] ;?>"> <?php echo $doc['Modifier']['first_name'].' '.$doc['Modifier']['last_name'] ;?> </td>
                        <td class="hidden-xs"><a href="https://doersdocs.s3.amazonaws.com/projects/<?php echo $doc['name'];?>" target ="_blank"><i class="fa fa-download"></i></a> </td>
                        <td class="text-right"><?php echo $doc['created'] ;?> </td>
                      </tr>
                    <?php } ;?>
                    </tbody>
                  </table>
                </div>
                <div class="row">
                  <div class="col-xs-7 m-t-20"> Showing 1 - 15 of 200 </div>
                  <div class="col-xs-5 m-t-20">
                    <div class="btn-group pull-right">
                      <!-- <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-left"></i></button>
                      <button type="button" class="btn btn-default waves-effect"><i class="fa fa-chevron-right"></i></button> -->
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
  <input type="hidden" name="page" value="docadd" class="currentpage">
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
<script src="<?php echo BASE_URL; ?>js/dropzone.js"></script>
<script type="text/javascript">
 
  $(document).ready(function(){
   
    
  });
</script>