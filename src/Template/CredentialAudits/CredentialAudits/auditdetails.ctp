<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">


<?php //echo $this->Html->script(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','function.js')); ?>

<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>


<style>
.change_button_size{
  height: 28px;
    width: 91px;
	margin-top: -2px;
	line-height:0;
}
</style>

		
<script type="text/javascript">
	$(document).ready(function() 
	{	
		//fancybox settings
		$(".view_template").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	550, 
				'height' :	650, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
			//fancybox settings for instructions
		$(".view_inst").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	960, 
				'height' :	459, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
		$("#toggleOrder").click(function(){
			var value = $(this).val() ;
			if(value == "Orber by Deadline"){
				$("#toggleOrder").val('Orber by Modified');
				var pathArray = window.location.pathname.split( '/' );
				if(pathArray[5]=="key" ||  pathArray[5]=="modified"){

					 var base_url = "<?php echo $this->Url->build('/', true); ?>";
					$('#ticketReport').attr('action',base_url+'tickets/index/deadline/'+pathArray[6]);
				}else{
					var base_url = "<?php echo $this->Url->build('/', true); ?>";
				$('#ticketReport').attr('action',base_url+'tickets/index/deadline');
				}
				return true;
			}else if(value == "Orber by Modified"){
				$("#toggleOrder").val('Orber by Deadline');
				var pathArray = window.location.pathname.split( '/' );
				if($("#toggleOpen").val()==1){
					var base_url = "<?php echo $this->Url->build('/', true); ?>";
					$('#ticketReport').attr('action',base_url+'tickets/');
				}else if(pathArray[5]=="key" ||  pathArray[5]=="deadline"){
					var base_url = "<?php echo $this->Url->build('/', true); ?>";

					$('#ticketReport').attr('action',base_url+'tickets/index/modified/'+pathArray[6]);
				}else{
					var base_url = "<?php echo $this->Url->build('/', true); ?>";
				$('#ticketReport').attr('action',base_url+'tickets/index/');
				}
				return true;
			}
		});
		
		 $("#toggleOpen").click(function(){
				if($('#toggleOrder').val()=="Orber by Modified"){
					var base_url = "<?php echo $this->Url->build('/', true); ?>";
					$('#ticketReport').attr('action',base_url+'tickets/index/deadline');
					$('#ticketReport').submit();
				} 
				$('#ticketReport').submit();
		
			});

});
	
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
    
	});
	
</script>
<script type="text/javascript">

    /*------------------------------Create New sales audit ----------------------------------------------*/

    function audit_report()
    {
        var auditpoint = $('#auditpoint').val();
        var profile_id = $('#profile_id').val();
        if ($.active > 0) {
        } else {
        	var base_url = "<?php echo $this->Url->build('/', true); ?>";
        	//alert(base_url);
            $('.ajaxloaderdiv').show();
            $.post(base_url + 'credential_audits/auditdetails', {'auditpoint': auditpoint, 'profile_id': profile_id}, function (data) {
                if (data == 0) {
                   $('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
                    $("#error_msg").html("Audit Point can not be Added");
                } else {
                    var temp = $('#product-table tr').eq(1).text();
                    if (temp.trim() == "No records found") {
                        $('#product-table tr').eq(1).hide();
                    }
                    document.forms["AuditReportForm"].reset();
                    $('#product-table').append(data);
                    $('.ajaxloaderdiv').hide();
                    $("#success").html(" Audit Point has been added successfully!");
                    $('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');

                    setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
                }
            });
        }
    }
    /*--------------------------function for change status --------------------------------*/
    function change_status(id, status) {
        $('.ajaxloaderdiv').show();
        if ($.active > 0) {
        } else {

        	var base_url = "<?php echo $this->Url->build('/', true); ?>";
        	//alert(base_url);
            $.post(base_url + 'credential_audits/changeStatus', {'id': id, 'status': status}, function (data) {
                if (data == 1) {
			//alert('here');
					
				   $('#close_'+id).addClass('change_button_size');
					$('#open_'+id).addClass('change_button_size');
					
                    $('.ajaxloaderdiv').hide();
		           if(status==1){
                    $('#close_' + id).hide();
                    $('#open_' + id).show();
					$('#togle_color_'+id).addClass('audit_close');
					$('#togle_color_'+id).removeClass('audit_open');
					$(".audit_close").hide();
					$(".audit_open").show();
					}else if(status==2){
					$('#close_' + id).show();
                    $('#open_' + id).hide();
					$('#togle_color_'+id).removeClass('audit_close');
					$('#togle_color_'+id).addClass('audit_open');
					$(".audit_open").hide();
					$(".audit_close").show();
					}
                    
                    $("#success").html("Audit point updated successfully!");
                    $('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');

                    $('#success').show();
                    setTimeout(function () {
                        $('#success').fadeOut('slow');
                    }, 3000); 
					 setTimeout(function () {
                       location.reload();
                    }, 300);
					 
                }
            });
        }

    }
    /* ------------------Function to delete Records from milestone-------------------------*/
	function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}
		
	function deleteRecord(id){
		//pr($id);
          
		$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			var base_url = "<?php echo $this->Url->build('/', true); ?>";
			//alert(base_url);
			$.post(base_url+'credential_audits/delete',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#togle_color_'+id).remove();
					$("#success").html("Audit Point has been deleted successfully!");
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');

					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				}
			});
		}
	}
	function viewopen(){
	$(".audit_close").hide();
	$(".audit_open").show();
	
	
	}
	function viewclose(){
	$(".audit_open").hide();
	$(".audit_close").show();
	
	}

</script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $('#checkbox').click(function(){
            if($(this).prop("checked") == true){
               $(".audit_open").show();
	           $(".audit_close").show();	   
            }
            else if($(this).prop("checked") == false){
                $(".audit_open").show();
				$(".audit_close").hide();	
				
            }
        });
    });
</script>
<?php //echo $this->Form->create('CredentialAudit',array('method'=>'POST', "class" => "longFieldsForm", "name" => "AuditReportForm")); ?>

<?php echo $this->Form->create('CredentialAudit',array('url' => ['action' => 'auditdetails'],'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>


<!--  start content-table-inner -->
<style>
    .audit_close{
          color:#00c292;
		display:none;
		
    }
    .audit_open{
        color:#03a9f3;
    }
</style>
	 <div class="col-sm-12">
            <div class="white-box">
	<?php $user = $session->read("SESSION_ADMIN"); ?>  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0"> <table border="0" width="80%" cellpadding="0" cellspacing="0">
	
        <tr valign="top">
            <td>
                <!--  start table-content  -->
                <div id="table-content">
		<?php $this->Flash->render(); ?>

                    <center><div class="font-bold text-success">Credential Management Audit Report</div></center>

                    <!--  end product-table................................... --> 
                   



		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable audit_table" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">


                        <div id="success" ></div>
                        <div id="error_msg"></div>
                        <tr>
                            <th class="text-muted" nowrap><h4>Audit Points</h4></th>
                        <th class="userattend" style="width:20%;height:20px;" nowrap></th>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $this->Form->hidden('profile_id',array('id'=>"profile_id","value"=>$audit_id,"label"=>false,"div"=>false));?>
                                <?php echo $this->Form->input('auditpoint',array('id'=>'auditpoint',"label"=>false,"div"=>false,"class"=>"form-control"));?>
                            </td>
                            <td><?php echo $this->Form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: audit_report();","class"=>"btn btn-danger")); 
                                        ?>
                            </td>
                        </tr>
                    </table>



		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable audit_list" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="product-table" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                        <th class="userattend" style="width:15%;height:20px;" nowrap><h4>Audit Points</h4></th>
                        <th class="userattend" style="width:22%;height:20px;" nowrap><h4>Resolved By</h4></th>
						 <th class="userattend" style="width:22%;height:20px;" nowrap><h4>Created By</h4></th>
                        <th class="userattend" style="width:22%;height:20px;" nowrap><h4>Modified At</h4></th>
                        <th class="userattend" style="width:35%;height:20px;" nowrap><h4>Status</h4></th>
                        </tr>
						
						<div id="showLegend" style="float:left; margin-top:-16px;"> 

		<?php
		//pr($this->request); die;
		 $classLink1=""; $classLink2=""; $classLink3=""; 

					if(isset($this->request->query['url']) ){
						 $checkurlarray = explode("/", $this->request->query['url']);  
						//pr($this->request->query['url']['url']);
						if($checkurlarray['4'] == "open")
						$classLink1 = "openlink";
						else if($checkurlarray['4'] == "close")
						$classLink2 = "openlink";
						
					}
		?>
			<span class="text-info font-bold b-b m-l-20"><?php //echo $this->Html->image(BASE_URL.'/app/webroot/images/table/legend_blue.png', array('width' => '8%','height'=>'20%'))?>
		<a href="javascript://" class="text-info font-bold b-b m-l-20" id="open" onClick="viewopen()">Open</a>
		 </span> 
			<span class="text-success font-bold b-b m-l-20" >     <?php //echo $this->Html->image(BASE_URL.'/app/webroot/images/table/legend_green1.png', array('width' => '8%','height'=>'20%'))?>
			<a href="javascript://" class="text-success font-bold b-b m-l-20" id="close" onClick="viewclose()">Close</a>
		 </span> 
		<span>
       <input type="checkbox" id="checkbox" name="data[Ticket][check_it]">
       <label id="lbl_ticket" for="check_it"  class="m-l-20 m-t-5 text-primary font-bold">Show All</label>
       </span>
		
		
	</div>
                            <?php //echo "<pre>"; print_r($audit_list);exit; 
                             if(count($audit_list)>0){
                             	//pr($audit_list);
			foreach($audit_list as $val){ 
                           $id= $val['id'];
                            if($val['status']==1)
					{
					$colorchange="audit_open";								
					}
					else{
					$colorchange="audit_close";
					}
                                        ?>
                        <tr id="togle_color_<?PHP echo $id;?>" class="<?php echo $colorchange ?>">
                            
							<td><?php echo $val['auditpoint'];?></td>
							<?php 
							 if(empty($val['resolvedby'])){ ?><td></td>
								 
							 <?php }else{ ?><td><?php 
							 foreach($userData as $userDat){
							 	//pr($userDat);
							if($val['resolvedby']==$userDat['id']){
								echo $userDat['first_name']." ".$userDat['last_name'];
							}
							
							} ?></td>
							 <?php }?>
							 <td>
							<?php
                            foreach($userData as $userDat){
							if($val['created_by']==$userDat['id']){
								echo $userDat['first_name']." ".$userDat['last_name'];
							}
							
							}
							?>
							
							</td>
                            <td><?php echo $val['modified'];?></td>
                            <td><span id="status_<?php echo $id; ?>">
                                <?php if($val['status']==1){
                                 echo $this->Form->button('Close', array('type' => 'button','id'=>'close_'.$id,'class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".$val['status'].");"));
                                  echo $this->Form->button('Open', array('type' => 'button','id'=>'open_'.$id,'class'=>'change_button_size btn btn-danger','style'=>'display:none',"onclick"=>"change_status(".$id.",".($val['status']+1).");"));
                               } else { 
                                    echo $this->Form->button('Close', array('type' => 'button','id'=>'close_'.$id,'style'=>'display:none','class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".($val['status']-1).");"));
                                    echo $this->Form->button('Open', array('type' => 'button','id'=>'open_'.$id,'class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".$val['status'].");"));
                                 } ?> 
                                <?php  echo $this->Html->link("","javascript:void(0)",
						array('class'=>'info-tooltip icon-2 delete','title'=>'Delete','id'=>'del_'.$id,'onClick'=>"if(is_delete()){deleteRecord(".$id.");}")
					);?>
                                </span></td>
                        </tr>
                             <?php }
                                } else {?>
                        <tr>
                            <td colspan="5" class="no_records_found">No records found</td>
                        </tr>
			<?php } ?>
                    </table>

                    <!--  end content-table  -->
                </div>

                <div class="clear"></div>
<?php echo $this->Form->end(); ?>
                <!--  end content-table-inner  --> </div></div>