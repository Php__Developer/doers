
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','function.js')); ?>
<script type="text/javascript">
var base_url = "<?php echo $this->Url->build('/', true); ?>";
	$(document).ready(function() {
				$("#milestonedate").datepicker({
				dateFormat: 'dd-mm-yy',
			});		
			
			alphanum();
	});


	function alphanum(){
		$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	}


/*function editRecord(id){
		spantoInput('week_'+id);
		//$('#currency_'+id).text();
		var cur_options;
		$('#currency option').each(function(){
		if($(this).val()!=$('#currency_'+id).text()){
		cur_options=cur_options+'<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
		}else{
		cur_options=cur_options+'<option value="'+$(this).val()+'" selected >'+$(this).text()+'</option>';
		}

		});
		$('#currency_'+id).replaceWith('<select class="form-control" id="currency_'+id+'">'+cur_options+'</select>');
		var options;

		$("#user_id > option").each(function() {		

		 if(this.value!=$('#user_edit_'+id).find(".user_id__").text()){
		       options=options+'<option value="'+this.value+'">'+this.text+'</option>';
            }else{
             options=options+'<option value="'+this.value+'" selected >'+this.text+'</option>';
		 }

		});


		$('#user_edit_'+id).replaceWith('<select class="form-control" id="user_edit_'+id+'">'+options+'</select>');
	spantoInput('amount_'+id);
		//spantoInput('user_'+id);
		spantoInput('milestonedate_'+id);
		$("#milestonedate_"+id).datepicker({
				dateFormat: 'dd-mm-yy',
				//minDate: 0
			});
		//spantoInput('verified_'+id);
		
		$('body div#ui-datepicker-div').css({'display':'none'});
		data_span = $('#notes_'+id).html();
		$('#notes_'+id).replaceWith('<textarea class="form-control" id="notes_'+id+'" rows="2" cols="20">'+data_span+'</textarea>');
		changeLink('edit_'+id);
		alphanum();
	}*/

	/*function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="form-control numeric" style="width:160px;height:30px;"/>');
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}*/


/*function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}

function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}*/
/*
function editData(id){
		//var a=$('#user_edit_'+id).val();
		//alert(a);
			var postdata = {	'id':id,
								'currency':$('#currency_'+id).val(),
								'amount':$('#amount_'+id).val(),
								'notes':$('#notes_'+id).val(),
								'user_id':$('#user_edit_'+id).val(),
								'milestonedate':$('#milestonedate_'+id).val(),
								
										
									};
						$('.ajaxloaderdiv').show();
		if($.active>0){
						
		}else{
			$.post(base_url+'milestones/edit',postdata,function(data){
				console.log(data);
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$("#success").html("Milestone has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					updateRecord(id);
				}else if(data==0){
							$('.ajaxloaderdiv').hide();
							$("#success").html("Milestone has been updated successfully!");
					         $('#success').show();
                              updateRecord(id);
				}
			});
		}
	}*/

/*function updateRecord(id){
		inputToSpan('week_'+id);
		inputToSpan('currency_'+id);
		inputToSpan('amount_'+id);
		inputToSpan('notes_'+id);
		$('#user_edit_'+id).replaceWith('<span id="user_edit_'+id+'">'+$('#user_edit_'+id+' option:selected').text()+'<span class="user_id__" style="display:none;">'+$('#user_edit_'+id+' option:selected').val()+'</span></span>');
		//inputToSpan('user_'+id);
		inputToSpan('milestonedate_'+id);
		changeLinkBack('edit_'+id);
	}

	function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}

	function deleteRecord(id){
					$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			$.post(base_url+'milestones/delete',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#week_'+id).closest("tr").remove();
					$("#success").html("Milestone has been deleted successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					
				}
			});
		}
	}*/
	

/////

/*	function createmilestone()
	{
		//alert("hiiii");
		var user_id=$('#user_id').val();
		
		var milestonedate =$('#milestonedate').val();
		var currency = $('#currency').val();
		var amount = $('#amount').val();
		var notes = $('#notes').val();
		var project_id = $('#project_id').val();
		var pass  =  <?php echo $this->request->params['pass'][0];	?>;
			
			
					 if(milestonedate=="")
					{
						document.getElementById("valid_date").style.display = "block";
									
					 }
					 else{
						document.getElementById("valid_date").style.display = "none";
						 }		 
					 // if(amount=="")
					  if(!amount.match(/^-?\d*(\.\d+)?$/) || amount=="")
					 {
						document.getElementById("valid_amount").style.display = "block";
						return true;
						
					 }
					 else		  
					 {
						document.getElementById("valid_amount").style.display = "none";
						
					 }
					 if(notes=="")
					 {
						document.getElementById("valid_notes").style.display = "block";			
					 }else		  
					 {
						document.getElementById("valid_notes").style.display = "none";
						
					 }
		
					
		if($.active>0){
						
			}else{
					$('.ajaxloaderdiv').show();
				$.post(base_url+'milestones/add/'+pass,{'user_id':user_id,'milestonedate':milestonedate,'currency':currency,'amount':amount,'notes':notes,'project_id':project_id},function(data){
					//alert(data);
					if(data==0){
						$("#error_msg").html("Milestone can be added");
					}else{
						document.forms["milestonelistForm"].reset();
						$('.product-table').append(data);
						$('.ajaxloaderdiv').hide();
						$("#success").html("Milestone has been added successfully!");
					}
				});
			}
		}*/

///

function is_verify(){
			if(confirm("Are you sure to verify this milestone?") == true){
			return true;
		}
		return false;
	}
	function verifyRecord(id){
						$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			$.post(base_url+'milestones/verifymilestone',{'id':id,'key':'verify'},function(data){
				if(data==1){
				$('.ajaxloaderdiv').hide();
				$('#verify_'+id).toggle();
				$('#unverify_'+id).toggle();
				$('#hide_img_'+id).toggle();
				$('#show_img_'+id).toggle();
				$("#success").html("Milestone has been veryfied successfully!");
				$('#success').show();
				setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				
				}
			});
		}
	}
	
	/*------------Function TO Unverify MileStone Records----------------------------*/
	
	function is_unverify(){
			if(confirm("Are you sure to unverify this milestone?") == true){
			return true;
		}
		return false;
	}	
	function UnverifyRecord(id){
							$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			$.post(base_url+'milestones/unverifymilestone',{'id':id,'key':'unverify'},function(data){
				if(data==1){
				$('.ajaxloaderdiv').hide();
				$('#verify_'+id).toggle();
				$('#unverify_'+id).toggle();
				$('#show_img_'+id).toggle();
				$('#hide_img_'+id).toggle();
				$("#success").html("Milestone has been unveryfied successfully!");
				$('#success').show();
				setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				
				}
			});
		}
	}


</script>



















<?php

	//die("ahere");
 echo $this->Form->create('Milestone',array('method'=>'POST', "class" => "longFieldsForm", "name" => "milestonelistForm")); ?>
<!--  start content-table-inner -->


	 <div class="col-sm-12">
            <div class="white-box">



	<?php 
	 $session = $this->request->session();
	$user = $session->read("SESSION_ADMIN"); ?>

	<!--  start table-content  -->
	<?php 
		//echo "<pre>";print_r($Error);exit;
/*		$bug=array();
		if(count($Error)>0){
		foreach($Error as $k=>$error){
		$bug[$k]=$error;
		}
		//echo "<pre>";print_r($bug);exit;
		}*/
		?>
	
		
				<center><h2 class="font-bold text-success"> Milestone
				 Project List</h2></center>
			
					<div id="success" style="color:green;font-size:13px;float:left; padding-bottom:5px;"></div>
					<div id="error_msg" style="color:red;font-size:13px;float:left; padding-bottom:5px;"></div>




<!-- 		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable product-table"  id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0"> -->
		    <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable product-table" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
				   <thead>
                <tr>
              
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn"> Project Name</button></th>
                <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">User Name</button></th>
					  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Milestone Date</th>
				  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Currency</th>
					  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Amount</th>
						  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Notes</th>
						  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Status</th>
						  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Action</th>
              
                                   </tr>
              </thead>
              <tbody class="resultset">


				<?php

            if(count($resultData)>0){
			 $project_id="";
			 foreach($resultData as $result):
			 $id= $result['id'];
			  $milestoneid = $this->General->encForUrl($this->General->ENC($result['id'])); 
			 $project_id=$result['project_id'];
		?>
				<tr>
						
					<td><span class="week_"><?php echo $result['project']['project_name'];?></span></td>
					<td><span class="user_edit_"><?php echo trim($result['user']['first_name']);?>
						<span class="user_id__" style="display:none;"><?php echo $result['user']['id'];?></span></span>
					</td>
					<td><span class="milestonedate_" ><?php echo $result['milestonedate'];?></span></td>
					<td><span class="currency_"><?php echo $result['currency'];?></span></td>
					<td><span class="amount_"><?php echo $result['amount'];?></span></td>
					<td><span class="notes_"><?php echo $result['notes'];?></span></td>
					
					<td style="text-align:center">
						<?php //echo ($result['verified'] == '1')?$this->Html->image(BASE_URL."images/table/green.png", array("alt"=>"Activated")):$this->Html->image(BASE_URL."images/table/red.png", array("alt"=>"Deactivated")); ?>
	             <?php if($verified[$id]==0){
                       ?>
                       <span  class="text-danger" id="show_img_">Deactivated</span>
                        <span  style="display:none;" class="text-success" id="hide_img_">Activated</span>
                       <?php
						}else if($verified[$id]==1) {?>
					  <span  class="text-success" id="show_img_">Activated</span>
					  <span  style="display:none;" class="text-danger" id="hide_img_">Deactivated</span>
					<?php
					 }?>

					</td>
					<td>
					<input type="hidden" name="milestoneid" class="milestoneid" value="<?php echo $milestoneid ;?>">
					<?php	$this->Html->link("","javascript:void(0)",
						array('class'=>'icon-1 info-tooltip','title'=>'Edit','id'=>'edit_','onClick'=>""));

							echo $this->html->link("<i class='fa fa-gear facontrol' ></i>","javascript:void(0)",
								array('class'=>'info-tooltip editRecord','title'=>'Edit','id'=>'edit_','onClick'=>"",'escape' => false)
							 ); 

							 echo $this->html->link("<i class='fa fa-check-square facontrol' ></i>","javascript:void(0)",
								array('class'=>'info-tooltip editData default_hidden','title'=>'Edit','id'=>'edit_','onClick'=>"",'escape' => false)
							);

						 ?>
					<?php  $this->Html->link("","javascript:void(0)",
						array('class'=>'info-tooltip icon-2 delete','title'=>'Delete','id'=>'','onClick'=>""));

						echo $this->Html->link("<i class='fa fa-times facontrol' ></i>","javascript:void(0)",
						array('class'=>'info-tooltip deleterec','title'=>'Delete','id'=>'del_','onClick'=>"",'escape' => false)
						);
					?>
					<?php if($result['verified']==1){
						$this->Html->link("","javascript:void(0)",
						array('class'=>'verifyimg','title'=>'verify','id'=>'verify_','onClick'=>"")
						);

						echo $this->Html->link("<i class='fa fa-thumbs-up facontrol' ></i>","javascript:void(0)",
						array('class'=>'info-tooltip verifyrec','title'=>'Click to Unverify','id'=>'del_', 'data-key' => 'unverify','onClick'=>"",'escape' => false)
						);

					}else if($result['verified']==0) {?>
					<?php  
							echo $this->Html->link("<i class='fa fa-thumbs-down facontrol' ></i>","javascript:void(0)",
							array('class'=>'info-tooltip verifyrec','title'=>'Click to Verify','id'=>'del_', 'data-key' => 'verify', 'onClick'=>"",'escape' => false)
						);	
					  }
					?>		
					</td>
					
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?>
		<tr>
			<td colspan="10" class="no_records_found">No records found</td>
		</tr>
			<?php } ?>
			</tbody>
				</table>			
				


<br><br>                               
	  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable"  id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
				   <thead>
                <tr>
              
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">User</button></th>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Milestone Date</th>
					  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Enter Currency</th>
				  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Enter Amount</th>
					  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Mention Notes</th>
						
              
                                   </tr>
              </thead>
              <tbody>





	












                 <tr>
						 <?php 
						 if(!empty($project_id)){
						 	$project_id=  $this->General->encForUrl($this->General->ENC($project_id) );
						 }else{
						 	$project_id="";
						 }
						 ?>
					 <?php echo $this->Form->hidden('project_id',array('id'=>'project_id','value'=>$project_id,"label"=>false,"div"=>false));?>
					<td> <?php
							$options = $this->General->getuserexceptraci();
							
					echo $this->Form->input("user_id",array("type"=>'select','options'=>$options,'class'=>'form-control add_milstone','id'=>'user_id',"label"=>false,"div"=>false));?>
					
					</td>
					
					<td> <?php echo $this->Form->input("milestonedate",array("type"=>"text","readonly"=>true,"class"=>"form-control add_milstone",'id'=>'milestonedate',"label"=>false,"div"=>false));?>	
					<div class="error-message text-danger" id="valid_date" style="display:none;">Please select date</div>
					</td>
					
					<td> <?php $options=array('USD'=> 'USD',
												'AUD'=>'AUD',
												'CAD'=>  'CAD',
												'INR'=> 'INR');
						echo $this->Form->input("currency",array('type'=>'select','options'=>$options,"class"=>"form-control add_milstone",'id'=>'currency',"label"=>false,"div"=>false));?>	
					</td>
					
					<td> <?php echo $this->Form->input("amount",array('class'=>'form-control add_milstone','id'=>'amount',"label"=>false,"div"=>false));?>	
					<div class="error-message text-danger" id="valid_amount" style="display:none;">Please enter valid amount</div>
					</td>
					
					<td> <?php echo $this->Form->input("notes",array('class'=>'form-control add_milstone','type'=>'text','id'=>'notes',"label"=>false,"div"=>false));?>
						<div class="error-message text-danger" id="valid_notes" style="display:none;">Please enter Notes</div>
					</td>
					
					<td><?php echo $this->Form->button('Create New Milestone',array('type'=>'button','div'=>false,"onclick"=>"","class"=>"btn btn-success addmilestone")); 
			?></td>
		
				</tr>
				</tbody>
			</table>
			</tbody>
			</table>
			</div>
	
</div>

</div>
<?php
 echo $this->Form->end(); ?>













<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<script src="<?php echo BASE_URL; ?>js/milestones.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_c`1``omponents/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
