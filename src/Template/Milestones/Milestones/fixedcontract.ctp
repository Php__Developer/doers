

 <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','function.js')); ?>








<script type="text/javascript">
	$(document).ready(function(){ 
	
	  //jQuery.noConflict();
	
		$('.tablesorter').tablesorter({

		headers: {
            0: {
                sorter: false
            },	
		
            1: {
                sorter: false
            },
		    2: {
                sorter: false
            }    
		}		
		});
	});
	$(".viewPaydetails").tooltip({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 $("#fancybox-overlay").css({"position":"fixed"});
				}
			});
</script>
<script type="text/javascript">
	var base_url = "<?php echo $this->Url->build('/', true); ?>";
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}	
		});
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
			// maxDate:new Date(),
			 firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 0,''];
				}
			 
		 });
	});
	
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
	}	
	
 function weekFrom()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(previous).setDate(previous.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("startdate").value=monthsum2;
		 }
		 function weekFrom1()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
		var next=new Date(endDate);
		var monthsum= new Date(new Date(next).setDate(next.getDate()-6));
			var monthsum2=monthsum.toISOString().substr(0,10);
			document.getElementById("enddate").value=monthsum2;
		 }
  function nextWeek()
  {
	var startDate=document.getElementById("startdate").value;
	var endDate=document.getElementById("enddate").value;
	var previous=new Date(startDate);
	var next=new Date(endDate);
	var monthsum= new Date(new Date(previous).setDate(previous.getDate()+6));
			//alert(monthsum);
			var monthsum2=monthsum.toISOString().substr(0,10);
			//alert(monthsum2);
			document.getElementById("startdate").value=monthsum2;
	
  }
 
</script>
<?php
   echo $this->Form->create('Milestone',array('url' => ['action' => 'fixedcontract'],'method'=>'POST', 'controller'=>'projects',   "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm();')); ?>

<!--  start content-table-inner -->
 <div class="col-sm-12">
            <div class="white-box">



	<center><h4 class="font-bold text-success">Fixed Contract Projects</h4></center></br>
	<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
	<tr>	
	</br>
	<td style="width:8%;">
		<div style="float:left;margin-top:10px;margin-left:120px;">
		Week Start :
		</div>	
	</td>
		
	<td width="1%">
	<div style="float:left;margin-left:-250px;">
	<?php 
		if($fromDate!=""){
		$st_date=date('Y/m/d',strtotime($fromDate));
		}
		else{
		if(date('N')==7){
		$st_date=date('Y/m/d',strtotime("last monday"));
		}
		else{
		$st_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		}
	}
	 echo $this->Form->input("start_date",array("class"=>"form-control","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>date('Y/m/d',strtotime($st_date))));
     ?>
	</td>
	
	</div>
		
		<td width="5%">
		<div style="float:left;margin-top:0px;">
			<?php echo $this->Form->submit("Search", array('class'=>'
btn btn-danger','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
			</div>
			</td>
		</tr>
	</table>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			
			
			  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
				   <thead>
                <tr>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">S.No</button></th>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn"> Project Name</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Platform</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Technology</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">Notes</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Currency</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Amount</button></th>
              
                                   </tr>
              </thead>
              <tbody>
				
					
				<?php  $sum=0;$i = 1;  if(count($resultData)>0){ 
				
				foreach($resultData as $fixedData): 
				$total[($i-1)]= ($fixedData['currency']=='INR')? ($fixedData['amount']/60): ($fixedData['amount']/10);
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php  echo  $fixedData['Projects']['project_name']; ?></td>
					<td><?php  echo  $fixedData['Projects']['platform']; ?></td>
					<td><?php  echo  $fixedData['Projects']['technology']; ?></td>
					<td><?php echo $fixedData['notes']; ?></td>
					<td><?php echo $fixedData['currency']; ?></td>
					<td><?php echo number_format($total[($i-1)],2);?></td>
					
					
				</tr>
				<?php  $i++;
//$sum=$sum+$total;
				
				endforeach; 
				
				?>
				<tr>
						<td colspan="6" style="text-align:center;color:red;">Total</td>
						<td style="color:red;"><?php echo number_format(array_sum($total),2);?></td>
				</tr>
				<tbody>

				<?php } else { ?> 
				<tr>
					<td colspan="8" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
		</tbody>
				</table>
				<!--
				<div style="width:20%;margin-left:50px;">
				<div style="width:10px;height:10px;background-color: #00CC00;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" > Alert(Hours required between 10 to 11) </h5></div></div></br>
				<div style="width:20%;margin-left:50px;">
				<div style="width:10px;height:10px;background-color: #FFFF66;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:280px;" > Mega Alert(Hours required between 11 to 12)</h5></div></div></br>
				<div style="width:16%;margin-left:50px;">	
					<div style="width:10px;height:10px;background-color: #0099FF;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" > Urgent(Hours required between 12 to 13) </h5></div></div></br>
				<div style="width:20%;margin-left:50px;">	
				<div style="width:10px;height:10px;background-color: #FF33CC;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:280px;" >Mega Urgent (Hours required between 13 to 14)</h5></div></div></br>
				<div style="width:20%;margin-left:50px;">	
				<div style="width:10px;height:10px;background-color: #FF0000;"></div>
					<div  style="margin-left:25px;margin-top:-10px;"><h5 style="font-size:12px;width:250px;" >Emergency (Hours required more than 14)</h5></div></div></br>
				-->
				</div>
				
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>
<script>
jQuery(document).ready(function(){
	
	jQuery('.ui-widget-content').css('border','0px');
		});

</script>
	</td>
</table>
<div class="clear"></div>
</div>

</div></div>
<!--  end content-table-inner  -->


<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
