
<script>

function changepass(){
        
        if($.active>0){
        }else{
        $.post(base_url+'admin/changepass',{'key':'yes'},function(data){
                if(data==1){
                     location.reload();
                            
                }
            });
        }
    }
function is_change(){
            if(confirm("Are you sure you want to change all ERP Passwords?") == true){
            return true;
        }
        return false;
    }
</script>

<!--  start content-table-inner -->
        <div class="col-sm-6">
            <div class="white-box">

                  <?php echo $this->Flash->render(); ?>

     <?php echo $this->Form->create('Permission',array('url' => ['action' => 'setting'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>

        <!-- start id-form -->
<div class="form-group"> 
   <label for="inputName" class="control-label">IP Address</label>
               
       <?php 
          foreach($ip_add as $ipAddredd){
            
            $ip=$ipAddredd->value;
        }             
        echo $this->Form->input("ip_address",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName" ,"value"=>$ip,"required"=>"required"));
                
            ?>
 </div>

<div class="form-group"> 
   <label for="inputName" class="control-label">Email Address</label>
            <?php
          foreach($email_add as $emailADd){
            
              $email=$emailADd->value;
        }
                 
        echo $this->Form->input("mail_add",array("type"=>"textarea","class"=>"form-control","label"=>false,"div"=>false,"id"=>"inputName","value"=>$email,"required"=>"required"));
                
            ?>
</div>
<div class="form-group"> 
   <label for="inputName" class="control-label">Notification</label>         
     
     <?php
        
          foreach($notify as $notification){
            
              $noti=$notification->value;
        }                     
          echo $this->Form->input("notification",array("type"=>"textarea","id"=>"inputName","class"=>"form-control","label"=>false,"div"=>false,"value"=>$noti,"required"=>"required"));
           ?>
</div>

<div class="form-group">
 <button type="submit" class="btn btn-success">Submit</button>
 <button type="submit" class="btn btn-inverse waves-effect waves-light">Reset</button>
 </div>
 </div>
</div>
    <!-- end id-form  -->

    

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
             <h5>Settings</h5>
           IP Address and notifications can be managed form admin section.

                 <h5>Important Links:</h5>
                <?php 
            echo $this->Html->link("Change Password For All User","javascript:if(is_change()){ changepass();}",
            array('style'=>"color:red;")
            );  
            ?>     
            
   </div>   
 </div>
</div>
     
      <div class="right-sidebar">
        <div class="slimscrollright">
          <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
          <div class="r-panel-body">
            <ul>
              <li><b>Layout Options</b></li>
              <li>
                <div class="checkbox checkbox-info">
                  <input id="checkbox1" type="checkbox" class="fxhdr">
                  <label for="checkbox1"> Fix Header </label>
                </div>
              </li>
              
            </ul>
            <ul id="themecolors" class="m-t-20">
              <li><b>With Light sidebar</b></li>
              <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
              <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
              <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
              <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
              <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
              <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
              <li><b>With Dark sidebar</b></li>
              <br/>
              <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
              <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
              <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>

              <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
              <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
              <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 chatonline">
              <li><b>Chat option</b></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img"  class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img"  class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a></li>
              <li><a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /.right-sidebar -->



</body>
</html>
