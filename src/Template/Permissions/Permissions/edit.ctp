<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
 <link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
<?php
 $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui",'allajax','migrate', 'common','listing','project'));
 ?>
 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
    <script src="<?php echo BASE_URL; ?>js/common.js"></script>
    <script src="<?php echo BASE_URL; ?>js/module_page.js"></script>

  <div class="col-sm-6">
            <div class="white-box">
  <?php echo $this->Form->create($RolesData,array('method'=>'POST','onsubmit' => '',"class"=>"login")); ?>


        <div class="form-group">
                    <label for="exampleInputuname">Controller*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputuname" placeholder="Module Name" name="module_name"> -->
                      <?php echo $this->Form->select('controller',$controllers,['empty' => 'None','class' => 'form-control c0ntroller','value' => isset($controller['controller']) ? $controller['controller'] : "" ] );?>
                    </div>
                      <?php 
                      if(isset($errors['controller'])){
                  //   pr($errors['controller']); die;
                      echo $this->General->errorHtml($errors['controller']);
                      } ;?>
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Action*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->select('action',[],['empty' => '--Select--','class' => 'form-control acti0n5','default' => $action]  );?>
                    </div>
                      <?php if(isset($errors['action'])){
                      echo $this->General->errorHtml($errors['action']);
                      } ;?>
                  </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Alias*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->input("alias",array("type"=>"text","class"=>"form-control alias",'placeholder'=> "Enter Alias", "label"=>false,"div"=>false));?>
                      <?php echo $this->Form->hidden("action",array("type"=>"text","class"=>"form-control actionval",'placeholder'=> "Enter Alias", "label"=>false,"div"=>false,'value' => (isset($action))? $action : "" ));?>
                    </div>
                      <?php if(isset($errors['alias'])){
                      echo $this->General->errorHtml($errors['alias']);
                      } ;?>
                  </div>
                    <div class="form-group">
                  <label class="col-md-12">Description*</label>
                  
                   <?php echo $this->Form->input("description",array("type"=>"textarea","class"=>"form-control description",'placeholder'=> "Description ", "label"=>false,"div"=>false));?>
                   <?php echo $this->Form->hidden("type",array("type"=>"textarea","class"=>"form-control type",'placeholder'=> "Description ", "label"=>false,"div"=>false,'value' => 'permissions'));?>
                    <?php echo $this->Form->hidden("id");?>
                   <?php if(isset($errors['description'])){
                      echo $this->General->errorHtml($errors['description']);
                      } ;?>
                    <div class="description_error"></div>
                </div>

    <div class="form-group">
   <button type="submit" class="btn btn-success">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'permissions','action'=>'edit','prefix' => 'permissions','id'=>$RolesData->id],['class'=>"btn btn-inverse waves-effect waves-light"]);  
?>

 </div>
 </div>
</div>

  <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Permissions Management</h4>
                            This section is used by Admin and PM only to Manage senstive roles.
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'permissions','action'=>'list'), array('style'=>'color:red;'));?>

              </li> 
          

                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>

      <!-- /.right-sidebar -->


</form>
</body>
</html>
    

  