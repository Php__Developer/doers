<link href="../plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/ui-timepicker_latest.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />

 <script src="<?php echo BASE_URL; ?>js/jquery-3.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.js"></script>
 <script src="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.js"></script>
 <script src="<?php echo BASE_URL; ?>js/jquery_timepicker_latest.js"></script>
  <script src="<?php echo BASE_URL; ?>js/allajax.js"></script>
 

<style type="text/css">
    .bootstrap-tagsinput span {
    font-size: 9px;
    margin: 1px;
    padding: 4px;
    line-height:25px;
    font-size:11px;
}

.ss label {
    margin-left: 20px;
}
.sub > li {
    height: 30px !important;
    padding: 0;
}
</style>
<?php echo $this->Html->script(array('my_script','allajax')) ?>
<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="../plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

<div class="col-md-12">
  <div class="form-group">
      <div class= "errormessage text-danger"></div>
        <?php echo $this->Form->create('Permission',array('url' => ['action' => 'set_permissions'],'method'=>'POST','onsubmit' => '',"class"=>"login","id"=>"setPermission")); ?>


    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
            <tr>
                    
                    <td>

                   <div class="form-group">
              <label for="exampleInputpwd1">Controller:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <div style="color:red;" id="error1"></div>
                        <?php
                         
                    if(isset($folders)){
                  
                   $name=$folders;
                       
         echo $this->Form->input('SelectController', array("type"=>'select',"class"=>"form-control",'empty' => '--Select--','options' => $name,"label"=>false,"div"=>false)); 
                    } ?>
                  </div>
                    <?php if(isset($errors['SelectController'])){
                            echo $this->General->errorHtml($errors['SelectController']);
                } ;?>
                   </div>


                     <div style="color:red;" id="error1"></div>
                
                <td id="selectActionContainer">
               <div class="form-group">
              <label for="exampleInputpwd1">Action:</label>
                   <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                    <?php

            if(isset($selected_files)){
               $selected=$selected_files;
                }else{
                 $selected="";   
                }
                   
                    echo $this->Form->input('controller', array("type"=>'select',"id"=>"setAction","class"=>"form-control",'empty' => '--Select--',''=>$selected,"label"=>false,"div"=>false));
                     
                    ?></div>
                     
                   <?php if(isset($errors['controller'])){
                            echo $this->General->errorHtml($errors['controller']);
                } ;?>
               </div>
            </tr>
            <tr class="ss">
               
                
                <td>
                  <div class="form-group">
              <label for="exampleInputpwd1">Roles:</label>
                   <div class="input-group">
                        
                    <?php 
                            
                             $resultData = $this->General->getUserRoles();

                           if(isset($resultData))  {

                                  foreach($resultData as $key=>$value){
                
                              echo $this->Form->input("role_".$key,array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"value"=>$key,"id"=>$key,"hiddenField"=>false,/*'onchange'=>"javascript:chk_id(this.id)",*/'hiddenField'=>false));
                                    echo "<label for =". $key.">".$value."</label><br/>";

                                  }
                             }    
                  ?>          
                </td>
                </div>
                
                
              <!--   <th valign="top" id="usrdata" style=""></th> -->
                <td>
                    <div id="usrdat_select" >
        <?php
                              echo "<div align='center' style='color:red'>Please Select Role to Add More Users!</div>";
                              ?>
                    </div>
                </td>
                <td>
                <div id="usrdat" >
                <?php
                echo "<div align='center' style='color:red'>List of the Users Who Have been Alloted Permission to Respective Controller & Action</div>";
                ?>
                <?php echo $this->Form->input("user_ids",array("type"=>"text",'value'=> '',"class"=>"tag51nput",'placeholder'=> "", 'data-role'=> "tagsinput","label"=>false,"div"=>false,'style'=> 'height:30px'));?>
                </div>
                </td>
            </tr>
                    
        </table>

        <?php echo $this->Form->submit("Save",array("div"=>false,"class"=>"btn btn-success","name"=>"save")); ?>


        <?php echo $this->Form->end(); ?>
         <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
</div></div>
            