<script type="text/javascript">
    $(document).ready(function () {
         $(".alloted").fancybox({
            'type': 'iframe',
            'autoDimensions': false,
            'width': 650,
            'height': 700,
            'onStart': function () {
                jQuery("#fancybox-overlay").css({"position": "fixed"});
            }
        });
     });
    </script>










<!--  start content-table-inner -->
<div id="content-table-inner">
    <?php $user = $session->read("SESSION_ADMIN"); ?>
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr valign="top">
            <td>

                <!--  start table-content  -->
                <div id="table-content">
            <?php //echo $session->flash(); ?>
                    <table cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" style="margin-left:40px;">
                        <tr>
                            <td width="14%">
                                <b>Search by:</b>
        













                            </td>
                            <td width="20%">
                                <b>Search value:</b><br/>
        















                 </td>
                            <td width="40%"><br/>
                <?php
                echo  $this->Form->button("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";
                echo  $this->Form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/Permissions/list'"));             
                ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                        <tr>
                            <th class="table-header-check" ><input  type="checkbox" id="toggle-all" > </th>
                            <th class="table-header-repeat line-left " style="margin-left:-40px;width:35%;"><?php //echo $paginator->sort('Controller Name', 'Permission.controller');?></th>
                            <th class="table-header-repeat line-left "  width="19%"><?php //echo $paginator->sort('Action Name', 'Permission.action');?></th>
                            <th class="table-header-repeat line-left "  width="31%"><?php //echo $paginator->sort('Alias', 'Permission.alias');?></th>                    
                            <!--<th class="table-header-repeat line-left" width="20%">--><?php //echo $paginator->sort('User Name', 'Permission.user_id');?><!--</th>-->
                            <th class="table-header-options line-left" width="20%"><a href="#A">Options</a></th>
                        </tr>
     







                    </table>
                    <!--  end product-table................................... --> 
                </div>
                <!--  end content-table  -->

                <!--  start actions-box ............................................... -->
                <div id="actions-box">
                    <a href="" class="action-slider"></a>
                    <div id="actions-box-slider">

                    <?php echo  $this->Form->submit("Activate",array("div"=>false,"class"=>"action-activate","name"=>"publish",'onclick' => "return atleastOneChecked('Activate selected records?','Technologies/changeStatus');")); ?>






                    <?php echo  $this->Form->submit("Deactivate",array("div"=>false,"class"=>"action-deactivate","name"=>"unpublish",'onclick' => "return atleastOneChecked('Deactivate selected records?','Technologies/changeStatus');")); ?>


                    </div>
                    <div class="clear"></div>
                </div>
                <!-- end actions-box........... -->

                <!--  start paging..................................................... -->
                <table border="0" cellpadding="0" cellspacing="0" id="paging-table">
                    <tr>
                        <td>
                <?php //echo $paginator->prev('', array('class' => 'page-left'), null, array('class' => 'page-left')); ?>
                            <div id="page-info">

                            
                                







                            </div>
                <?php //echo $paginator->next('', array('class' => 'page-right'), null, array('class' => 'page-right')); ?>
                        </td>
                    </tr>
                </table>
                <!--  end paging................ -->

                <div class="clear"></div>

            </td>
            <td>

                <!--  start related-activities -->
                <div id="related-activities">
    <?php $user = $session->read("SESSION_ADMIN"); ?>
                    <!--  start related-act-top -->
                    <div id="related-act-top">
        <?php //echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
                    </div>
                    <!-- end related-act-top -->

                    <!--  start related-act-bottom -->
                    <div id="related-act-bottom">

                        <!--  start related-act-inner -->
                        <div id="related-act-inner">                
                            <div class="left"><a href=""><?php //$this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
                            <div class="right">
                                <h5>Permission Management</h5>
                                Permission Management is used to keep track of informations about the Permission. 
                                <div class="lines-dotted-short"></div>
                                <ul class="greyarrow">
                                    <li>
            <?php 
echo $this->Html->link("Add New Permission",
            array('controller'=>'Permissions','action'=>'add')
            );  
            ?>
                                    </li> 

                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <!-- end related-act-inner -->
                        <div class="clear"></div>

                    </div>
                    <!-- end related-act-bottom -->

                </div>
                <!-- end related-activities -->

            </td>
        </tr>
    </table>

    <div class="clear"></div>


</div>
<?php echo  $this->Form->end(); ?>
<!--  end content-table-inner  -->