<div class="row">
	  <div class="col-md-12">
	     <div class="white-box">
	        <h3 class="box-title">Userwise details of Permissions </h3>
	     </div>
	  </div>
 </div>
<div class="row">
        <!-- <div class="col-md-12"> -->
        <!--   <div class="white-box"> -->
<?php
if(count($modules) > 0)	{
 foreach($modules as $module){ 
 ?>
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                <div class="panel panel-info block4">
                    <div class="panel-heading"> <?php echo strtoupper($module['Perms']['alias']) ;?> | 
                        <?php $roles = $this->General->getrolenames($module['role_id']);
                                echo "Alloted To : ".implode(',', array_values($roles));
                                $users = $this->General->getusersfromrole($module['role_id'],$module['id']);
                         ?> | <?php echo "Count : ". count($users);?> | 
                         <?php
                            echo $this->Html->link(
                            'Add More Users',
                            array('controller'=>'permissions','action'=>'including','id'=>$module['id']),
                            ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit']
                            );
                        ?> | <?php
                            echo $this->Html->link(
                            'Remove Users From List',
                            array('controller'=>'permissions','action'=>'except','id'=>$module['id']),
                            ['escape' => false,'class' => 'btn default btn-outline popup_window','title'=>'Edit']
                            );
                        ?>

                        <div class="pull-right"><a href="#" data-perform="panel-collapse" class="crossicon"><i class="ti-minus"></i></a> <!-- <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> --> </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true" style="">
                        <div class="panel-body">
                            <p><?php echo strtoupper($module['description']) ;?></p>
                            <?php 
                            if(count($users) > 0){
                            foreach($users as $user){ ?>
                            <div class="media col-sm-4">
				               <?php 
				                if(!empty($user['image_url'])){ 
				                	$image = BASE_URL."img/employee_image/".$user['image_url'] ;
				                } else{
				                	$image = BASE_URL."images/shared/no_image.png";
				                }
				                ?>
			                  <a class="pull-left" href="#"> <img class="media-object thumb-sm img-circle" src="<?php echo $image ;?>" alt=""> </a>
			                  <div class="media-body"><!--  <span class="media-meta pull-right">07:23 AM</span> -->
			                    <h4 class="text-danger m-0"><?php echo $user['first_name'] .' '.$user['last_name'] ;?> </h4>
			                    <small class="text-muted">Username: <?php echo $user['username'] ;?></small> </div>
			                </div>
			                <?php } 
			                	} ?>
                        <?php    $otheraliases = $this->General->otheraliases($module['other_aliases']);
                            if(count($otheraliases) > 1){;?>

                <table class="table table-striped">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>List of the Aliases Related with  <?php echo strtoupper($module['Perms']['alias']) ;?></th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php
                    $count = 1;
                   foreach($otheraliases as $alias){ 
                    if($alias['id'] !== $module['Perms']['id']){
                   ?>
                      <tr>
                          <th scope="row"><?php echo $count ;?></th>
                          <td><?php echo $alias['alias'] ;?></td>
                      </tr>
                      <?php 
                        $count++;
                        }
                      } ?>
                  </tbody>
              </table>

              <?php } ?>
                        </div>
                    </div>
                </div>
                <hr> 
             </div>
        <?php
        	}	
         } ?>
        <!--   </div>
        </div> -->
      </div>
      <?php echo $this->Html->script(array('jquery-3')) ?>

<script type="text/javascript">
    $(document).ready(function(){
      /*ss*/



        var panelSelector = '[data-perform="panel-collapse"]';

         //$('.crossicon').each(function () {
        /*    var $this = $(this)
                , parent = $this.closest('.panel')
                , wrapper = parent.find('.panel-wrapper')
                , collapseOpts = {
                    toggle: false
                };
            if (!wrapper.length) {
                wrapper = parent.children('.panel-heading').nextAll().wrapAll('<div/>').parent().addClass('panel-wrapper');
                collapseOpts = {};
            }
            wrapper.collapse(collapseOpts).on('hide.bs.collapse', function () {
                $this.children('i').removeClass('ti-minus').addClass('ti-plus');
            }).on('show.bs.collapse', function () {
                $this.children('i').removeClass('ti-plus').addClass('ti-minus');
            });
        });*/
      //  $(this).trigger('click');
        // });
    });
    
</script>