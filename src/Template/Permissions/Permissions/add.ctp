<?php //echo $javascript->link('ckeditor/ckeditor.js'); ?>
<script type="text/javascript">
var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datetimepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			  
		 });
	$( "#enddate" ).datetimepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
		 });
	});
	function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
		
	}
	
	
</script>		
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		<?php echo $this->Form->create('Permission',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
         <?php //$session->flash(); ?>
	    <table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Controller Name:</th>
			<td><?php
			 echo $this->Form->input("controller",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		
		<tr>
			<th valign="top">Action Name:</th>
			<td>
			<?php 
			echo $this->Form->input("action",array("class"=>"inp-form","div"=>false,"label"=> false)); ?>
				</td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Alias:</th>
			<td>
			<?php  echo $this->Form->input("alias",array("class"=>"inp-form","div"=>false,"label"=> false)); ?>
						</td>
			</tr>
			<tr>
		<tr>
					
			<th valign="top">User ID:</th>
			<td><?php
				//$options=$general->getuser(); 
				echo $this->Form->input("user_id",array( "type"=>'select','size'=>'10','multiple' => true,'options'=>"","label"=>false,"div"=>false,"class"=>"multiSelect","id"=>"team_id"));
         		?>
			</td>
			
		</tr>		
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $this->Form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $this->Form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/Permissions/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Permission Management</h5>
         Permission Management is used to keep track of names,titles, and status informations of the Permission. 
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $this->Html->link("Go To Listing",
            array('controller'=>'Permissions','url'=>'list')
            );	
            ?>
            </li> 
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->