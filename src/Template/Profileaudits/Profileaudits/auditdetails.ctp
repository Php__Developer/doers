<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">


<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3')); ?>
<style>
.change_button_size{
  height: 28px;
    width: 91px;
	margin-top: -2px;
	
}
</style>		
<script type="text/javascript">
	$(document).ready(function() 
	{	



		
		$("#toggleOrder").click(function(){
			var value = $(this).val() ;
			if(value == "Orber by Deadline"){
				$("#toggleOrder").val('Orber by Modified');
				var pathArray = window.location.pathname.split( '/' );
				if(pathArray[5]=="key" ||  pathArray[5]=="modified"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline/'+pathArray[6]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline');
				}
				return true;
			}else if(value == "Orber by Modified"){
				$("#toggleOrder").val('Orber by Deadline');
				var pathArray = window.location.pathname.split( '/' );
				if($("#toggleOpen").val()==1){
					$('#ticketReport').attr('action',base_url+'admin/tickets/');
				}else if(pathArray[5]=="key" ||  pathArray[5]=="deadline"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/modified/'+pathArray[6]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/index/');
				}
				return true;
			}
		});
		
		 $("#toggleOpen").click(function(){
				if($('#toggleOrder').val()=="Orber by Modified"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/index/deadline');
					$('#ticketReport').submit();
				} 
				$('#ticketReport').submit();
		
			});

		 $('.submit-btn').on('click',function(event){
    		event.preventDefault();
        var base_url = "<?php echo $this->Url->build('/', true); ?>";
        var auditpoint = $('#auditpoint').val();
        if(auditpoint == ''){
        	alert("Audit Point Can Not Be Empty");
        	return false;
        }
        var profile_id = $('#profile_id').val();
        if ($.active > 0) {
        } else {
            $('.ajaxloaderdiv').show();
            $.post(base_url + 'profileaudits/auditdetails', {'auditpoint': auditpoint, 'profile_id': profile_id}, function (data) {
                if (data == 0) {

                          $('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
                    $("#error_msg").html("Audit Point can not be Added");
                } else {
                    var temp = $('#product-table tr').eq(1).text();
                    if (temp.trim() == "No records found") {
                        $('#product-table tr').eq(1).hide();
                    }
                    document.forms["AuditReportForm"].reset();
                    $('#product-table').append(data);
                    $('.ajaxloaderdiv').hide();
                    $("#success").html(" Audit Point has been added successfully!");
                    $('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');


                    setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
                }
            });
        }
    });

});
	
		
	
</script>
<script type="text/javascript">
    /*------------------------------Create New sales audit ----------------------------------------------*/
    //function audit_report() {
    	//submit-btn
    	
    //}
    /*--------------------------function for change status --------------------------------*/
    function change_status(id, status) {
    	var base_url = "<?php echo $this->Url->build('/', true); ?>";
        $('.ajaxloaderdiv').show();
        if ($.active > 0) {
        } else {
            $.post(base_url + 'profileaudits/changestatus', {'id': id, 'status': status}, function (data) {
                if (data == 1) {			
				 
					 setTimeout(function () {
                       location.reload();
                    }, 300);
					 
                }
            });
        }

    }
    /* ------------------Function to delete Records from milestone-------------------------*/
	function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}
		
	function deleteRecord(id){

          
    	var base_url = "<?php echo $this->Url->build('/', true); ?>";
		$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			$.post(base_url+'profileaudits/delete',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#togle_color_'+id).remove();
					$("#success").html("Audit Point has been deleted successfully!");
					$('#success').show();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:12px;');
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				}
			});
		}
	}





	function viewopen(){
	$(".audit_close").hide();
	$(".audit_open").show();
	
	
	}
	function viewclose(){
	$(".audit_open").hide();
	$(".audit_close").show();
	
	}

</script>

<script type="text/javascript">
	$(document).ready(function(){
        $('#check_it').change(function(){
            if($(this).prop("checked") == true){
               $(".audit_open").show();
	           $(".audit_close").show();	   
            }
            else if($(this).prop("checked") == false){
                $(".audit_open").show();
				$(".audit_close").hide();	
				
            }
        });
    });
</script>




    <?php echo $this->Form->create('Profileaudits',array('url' => ['action' => 'auditdetails'],'method'=>'POST','onsubmit' => '',"class"=>"longFieldsForm","id"=>$audit_id,"name"=>"AuditReportForm")); ?>



<!--  start content-table-inner -->
<style>
    .audit_close{
    	 color:#03a9f3;
          color:#00c292;
		display:none;
		
    }
    .audit_open{
        color:#03a9f3;
    }
</style>
<div id="content-table-inner">



    `

		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
         <tr valign="top">
            <td>
                <!--  start table-content  -->
                <div id="table-content">
			<?php //echo $session->flash(); ?>

                    <center><div class="font-bold text-success">Sales Profile Audit Report</div></center>
                    <!--  end product-table................................... --> 
            


		  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable audit_table" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">

                        <div id="success"></div>
                        <div id="error_msg"></div>
                        <tr>
                            <th class="userattend" style="width:60%;height:20px;" nowrap><h4>Audit Points</h4></th>
                        <th class="userattend" style="width:20%;height:20px;" nowrap></th>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $this->Form->hidden('profile_id',array('id'=>"profile_id","value"=>"$audit_id","label"=>false,"div"=>false));?>
                                <?php echo $this->Form->input('auditpoint',array('id'=>'auditpoint',"label"=>false,"div"=>false,"class"=>"form-control"));?>
                            </td>
                            <td><?php echo $this->Form->button('Save',array('type'=>'submit','div'=>false,"class"=>"btn btn-danger submit-btn")); 
                                        ?>
                            </td>
                        </tr>
                    </table>

               
                      <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable audit_list" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="product-table" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                        <th nowrap><h4>Audit Points</h4></th>
                        <th  nowrap><h4>Resolved By</h4></th>
						 <th  nowrap><h4>Created By</h4></th>
                        <th nowrap><h4>Modified At</h4></th>
                        <th nowrap><h4>Status</h4></th>
                        </tr>
						
						<div id="showLegend" style="float:left; margin-top:-16px;"> 
	              	


        <?php 
        $classLink1=""; 
        $classLink2=""; 
        $classLink3="";
        $checkurlarray = explode("/", $this->request->request['[url]']);  

					if(isset($checkurlarray['4'])){

						if($checkurlarray['4'] == "open")

						$classLink1 = "openlink";

						else if($checkurlarray['4'] == "close")

						$classLink2 = "openlink";
						
					}


		             ?>
	<span class="text-info font-bold b-b m-l-20">	<?php //echo $this->Html->image(BASE_URL.'images/table/legend_blue.png', array('width' => '8%','height'=>'20%'))?>
		        <a href="javascript://" id="open" onClick="viewopen()" class="text-info font-bold b-b m-l-20">Open</a>
		</span> 
		
	<span class="text-success font-bold b-b m-l-20" >    <?php //echo $this->Html->image(BASE_URL.'images/table/legend_green1.png', array('width' => '8%','height'=>'20%'))?>
			    <a href="javascript://" id="close" onClick="viewclose()" class="text-success font-bold b-b m-l-20">Close</a>
		</span> 
		
				<span>
                <input type="checkbox" id="check_it" name="data[Ticket][check_it]" style="display: none;">
                <label id="" for="check_it" class="m-l-20 m-t-5 text-primary font-bold">Show All</label>
       </span>


	</div>
	<?php
  //pr($audit_list);die();
	?>
    <?php  
                 if(count($audit_list)>0){
                             

			foreach($audit_list as $val){ 

                           $id= $val['id'];
                            if($val['status']==1)
					{
					$colorchange="audit_open";								
					}
					else{
					$colorchange="audit_close";
					}
                                        ?>
                        <tr id="togle_color_<?PHP echo $id;?>" class="<?php echo $colorchange ?>">
                            
							<td><?php echo $val['auditpoint'];?></td>
                            <td><?php
                            foreach($userData as $userDat){
							if($val['created_by']==$userDat['id']){
								echo $userDat['first_name']." ".$userDat['last_name'];
							}
							
							}
							?>
							</td>
                            
							 <td>
							<?php
                            foreach($userData as $userDat){
							if($val['created_by']==$userDat['id']){
								echo $userDat['first_name']." ".$userDat['last_name'];
							}
							
							}
							?>
							
							</td>
                            <td><?php echo $val['modified'];?></td>
                            <td><span id="status_<?php echo $id; ?>">
                                <?php if($val['status']==1){
      							 echo $this->Form->button('Close', array('type' => 'button','id'=>'close_'.$id,'class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".$val['status'].");"));
                                  echo $this->Form->button('Open', array('type' => 'button','id'=>'open_'.$id,'class'=>'change_button_size btn btn-danger','style'=>'display:none',"onclick"=>"change_status(".$id.",".($val['status']+1).");"));
                               } else { 
                                     echo $this->Form->button('Close', array('type' => 'button','id'=>'close_'.$id,'style'=>'display:none','class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".($val['status']-1).");"));
                                    echo $this->Form->button('Open', array('type' => 'button','id'=>'open_'.$id,'class'=>'change_button_size btn btn-danger',"onclick"=>"change_status(".$id.",".$val['status'].");"));
                                 } ?> 

                                <?php  echo $this->Html->link("","javascript:void(0)",
						array('class'=>'info-tooltip icon-2 delete','title'=>'Delete','id'=>'del_'.$id,'onClick'=>"if(is_delete()){deleteRecord(".$id.");}")
					);?>
               </span></td>
                        </tr>
                             <?php }
                                } else {?>
                        <tr>
                            <td colspan="5" class="no_records_found">No records found</td>
                        </tr>
			<?php } ?>
                    </table>
                    <!--  end content-table  -->
                </div>
                <div class="clear"></div>
<?php echo $this->Form->end(); ?>
                <!--  end content-table-inner  --> 

                <script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
