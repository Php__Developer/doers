<script>
function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}
function deleteRecord(id){
          
		$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			$.post(base_url+'admin/Profileaudits/delete',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#togle_color_'+id).remove();
					$("#success").html("Audit Point has been deleted successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				}
			});
		}
	}
function viewopen(){
	$("#audit_close").hide();
	$("#audit_open").show();
	
	
	}
	function viewclose(){
	$("#audit_open").hide();
	$("#audit_close").show();
	
	}
	function showall(){
	$("#audit_open").show();
	$("#audit_close").show();
	}
	
	
</script>
<style>
#audit_close{
	display:none;
	
}

</style>
<?php
echo $html->css(array("screen","jquery_ui_datepicker")); ?>
<?php echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','function.js')); ?>
<?php echo $html->css(array("screen")); ?>		
<?php echo $form->create('addauditpoint',array('method'=>'POST', "class" => "longFieldsForm", "name" => "AuditReportForm")); ?>
<div id="content-table-inner">
	  <?php $user = $session->read("SESSION_ADMIN"); ?>
    `     <table border="0" width="80%" cellpadding="0" cellspacing="0">	
             <tr valign="top">
               <td>
                <div id="table-content">
			     <?php echo $session->flash(); ?>
                   <center><div style="font-weight:bold;">Sales Profile Audit Report</div></center>              
                    <table border="0" width="125%" cellpadding="0" cellspacing="0" class="audit_table">
                      <div id="success" style="color:green;font-size:13px;float:left; padding-bottom:5px;"></div>
                       <div id="error_msg" style="color:red;font-size:13px;float:left; padding-bottom:5px;"></div>
                        <tr>
                           <th class="userattend" style="width:60%;height:20px;" nowrap><h4>Audit Points</h4></th>
                           <th class="userattend" style="width:20%;height:20px;" nowrap></th>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $form->hidden('profile_id',array('id'=>"profile_id","value"=>$audit_id,"label"=>false,"div"=>false));?>
                                <?php echo $form->input('auditpoint',array('id'=>'auditpoint',"label"=>false,"div"=>false,"style"=>"width:472px;height:25px;"));?>
                            </td>
                            <td><?php echo $form->button('Save',array('type'=>'button','div'=>false,"onclick"=>"javascript: audit_report();","style"=>" height: 30px;
    width: 100px;")); 
                                        ?>
                            </td>
                        </tr>
                    </table>

                    <table border="0" width="125%" cellpadding="0" cellspacing="0" id="product-table" class="audit_list">
                        <tr>
                        <th class="userattend" style="width:25%;height:20px;" nowrap><h4>Audit Points</h4></th>
                        <th class="userattend" style="width:25%;height:20px;" nowrap><h4>Resolved By</h4></th>
                        <th class="userattend" style="width:25%;height:20px;" nowrap><h4>Modified At</h4></th>
                        <th class="userattend" style="width:35%;height:20px;" nowrap><h4>Status</h4></th>
                        </tr>			
			<div id="showLegend" style="float:left; margin-top:-22px;"> 	
		<span>	<?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_blue.png', array('width' => '8%','height'=>'20%'))?>
		<a href="javascript://" id="open" onClick="viewopen()">Open</a>
		 </span> 
		<span>   <?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_green1.png', array('width' => '8%','height'=>'20%'))?>
			<a href="javascript://" id="close" onClick="viewclose()">Close</a>
		 </span> 
		<span>
		<?php echo $html->image(BASE_URL.'/app/webroot/images/table/legend_red1.png', array('width' => '8%','height'=>'20%'))?>
		       <a href="javascript://" id="close" onClick="showall()">Show All</a>
		</span>
	</div>
           <?php 
                if(count($data)>0){
			       foreach($data as $val){ 
				                               if($val['Profileaudit']['status']==1)
					{
					$colorchange="audit_open";								
					}
					else{
					$colorchange="audit_close";
					} ?>
					 <tr id="<?php echo $colorchange; ?>"> 
                      		<td><?php echo $val['Profileaudit']['auditpoint'];?></td>
                            <td><?php echo $val[0]['name'];?></td>
                            <td><?php echo $val['Profileaudit']['modified'];?></td>
							<td><?php echo $val['Profileaudit']['status']; ?></td>
                            <td><span id="status_<?php echo $id; ?>">
                                <?php if($val['Profileaudit']['status']==1){
                                 echo $this->Form->button('OPEN', array('type' => 'button','id'=>'close_'.$id,"onclick"=>"change_status(".$id.",".$val['Profileaudit']['status'].");"));
                                
                               } else{
								    echo $this->Form->button('CLOSE', array('type' => 'button','id'=>'open_'.$id,"onclick"=>"change_status(".$id.",".$val['Profileaudit']['status'].");"));
							   }
							   ?> 
                                <?php  echo $html->link("","javascript:void(0)",
						array('class'=>'info-tooltip icon-2 delete','title'=>'Delete','id'=>'del_'.$id,'onClick'=>"if(is_delete()){deleteRecord(".$id.");}")
					);?>
				</span></td>
                          </tr>
						  </tr>
                             <?php }
                                } else {?>
                          <tr>
                        <td colspan="4" class="no_records_found">No records found</td>
                           </tr>
			<?php } ?>
                    </table>          
                </div>
                <div class="clear"></div>
<?php echo $form->end(); ?>
               