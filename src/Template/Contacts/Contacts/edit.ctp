
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/fancybox/source/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_ltr.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jsdatepick/jsDatePick_rtl.css" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>js/jquery-ui/jquery-ui.css" media="screen" />
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

<?php echo $this->Form->create($ContactsData,array('url'=>['action'=>'edit'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>



	
    <!--  start content-table-inner -->
        <div class="col-sm-6">
          <div class="white-box">
          <?php $this->Flash->render(); ?>
              <div class="form-group">
              <label for="exampleInputpwd1">Name</label>
              <div class="input-group">
              <div class="input-group-addon"><i class="ti-lock"></i></div>
              <?php
              echo $this->Form->input("name",array("class"=>"form-control","label"=>false,"div"=>false));
              ?></div>
              <?php if(isset($errors['name'])){
              echo $this->General->errorHtml($errors['name']);
              } ;?>
          </div>

          <div class="form-group">
            <label for="exampleInputpwd1">Primary Email</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div><?php
            echo $this->Form->input("primaryemail",array("class"=>"form-control","label"=>false,"div"=>false));
            //echo $form->hidden("id");
            ?>
            </div>
            <?php if(isset($errors['primaryemail'])){
            echo $this->General->errorHtml($errors['primaryemail']);
            } ;?>
          </div>

          <div class="form-group">
            <label for="exampleInputpwd1">Primary Phone</label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div><?php
            echo $this->Form->input("primaryphone",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
          </div>
          <div class="form-group">
          <label for="exampleInputpwd1 col-md-4">Client Password</label>
          <span class="password col-md-4 pull-right" onclick="$('#cpassword').val($.password(12,true));" style="cursor: pointer;">Generate Password</span>
          <div class="input-group">
          <div class="input-group-addon"><i class="ti-lock"></i></div>
          <?php
          echo $this->Form->input("cpassword",array("type"=>"text","id"=>"cpassword","class"=>"form-control","label"=>false,"div"=>false));
          ?>

          </div>
          <?php if(isset($errors['cpassword'])){
          echo $this->General->errorHtml($errors['cpassword']);
          } ;?></div>





          <div class="form-group">
          <label for="exampleInputpwd1">Skyup</label>
          <div class="input-group">
          <div class="input-group-addon"><i class="ti-lock"></i></div>



          <?php
          echo $this->Form->input("skype",array("class"=>"form-control","label"=>false,"div"=>false));
          ?></div>
          <?php if(isset($errors['skype'])){
          echo $this->General->errorHtml($errors['skype']);}?>
          </div>


   <div class="form-group">
            <label for="exampleInputpwd1">Watsapp IM </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("watsapp_im",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['watsapp_im'])){
            echo $this->General->errorHtml($errors['watsapp_im']);
            } ;?>
          </div>

          <div class="form-group">
            <label for="exampleInputpwd1">Other IM </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("other_im",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['other_im'])){
            echo $this->General->errorHtml($errors['other_im']);
            } ;?>
          </div>

          <div class="form-group">
            <label for="exampleInputpwd1">Linked In Url </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("linked_in_url",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['linked_in_url'])){
            echo $this->General->errorHtml($errors['linked_in_url']);
            } ;?>
          </div>
          
          <div class="form-group">
            <label for="exampleInputpwd1">Facebook Url </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("facebook_url",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['facebook_url'])){
            echo $this->General->errorHtml($errors['facebook_url']);
            } ;?>
          </div>

         

          <div class="form-group">
            <label for="exampleInputpwd1">Twitter Url </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("twitter_url",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['twitter_url'])){
            echo $this->General->errorHtml($errors['twitter_url']);
            } ;?>
          </div>

          <div class="form-group">
            <label for="exampleInputpwd1">Youtube Url </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("youtube_url",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['youtube_url'])){
            echo $this->General->errorHtml($errors['youtube_url']);
            } ;?>
          </div>

           <div class="form-group">
            <label for="exampleInputpwd1">Other Url </label>
            <div class="input-group">
            <div class="input-group-addon"><i class="ti-lock"></i></div>
            <?php
            echo $this->Form->input("other_url",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['other_url'])){
            echo $this->General->errorHtml($errors['other_url']);
            } ;?>
          </div>  
		




 <div class="form-group"> 
               <label for="inputName" class="control-label">Other Contact:</label>
<?php
				 echo $this->Form->input("othercontact",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>



			  <div class="form-group">
                       <label for="exampleInputpwd1">Source</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>    <?php
				 echo $this->Form->input("source",array("class"=>"form-control","label"=>false,"div"=>false));
            ?></div>
            <?php if(isset($errors['source'])){
							echo $this->General->errorHtml($errors['source']);
							} ;?></div>
           



                   <div class="form-group">
                       <label for="exampleInputpwd1">Type</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div> 


           <?php
				    $options=array( 'general'    =>  'General', 
									'lead'    =>  'Lead', 
									'recurring'    => 'Recurring',
									'silver'    => 'Silver',
									'gold'     => 'Gold',
									'platinum'     => 'Platinum',
                      );
			echo $this->Form->input("type",array('type'=>'select','options'=>$options,"class"=>"form-control","label"=>false,"div"=>false));    
            ?></div></div>

            <div class="form-group">
                <label for="exampleInputuname">Client Professional Experience:</label>
                <?php echo $this->Form->textarea("professional_experience",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['professional_experience'])){
                echo $this->General->errorHtml($errors['professional_experience']);
                } ;?>
          </div>

          <div class="form-group">
                <label for="exampleInputuname">What Effort needed to make this project ongoing?</label>
                <?php echo $this->Form->textarea("efforts_for_ongoing",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['efforts_for_ongoing'])){
                echo $this->General->errorHtml($errors['efforts_for_ongoing']);
                } ;?>
          </div>

          <div class="form-group">
                <label for="exampleInputuname">What is missing in this project to achieve its goal?</label>
                <?php echo $this->Form->textarea("missing_in_goal",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['missing_in_goal'])){
                echo $this->General->errorHtml($errors['missing_in_goal']);
                } ;?>
          </div>

          <div class="form-group">
                <label for="exampleInputuname">What other work we can get by this project?</label>
                <?php echo $this->Form->textarea("other_works",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['other_works'])){
                echo $this->General->errorHtml($errors['other_works']);
                } ;?>
          </div>
          <div class="form-group">
                <label for="exampleInputuname">What other contacts we can get by this client?</label>
                <?php echo $this->Form->textarea("other_contacts",array("class"=>"form-control","label"=>false,"div"=>false));
                echo $this->Form->hidden("id"); ?>
                <?php if(isset($errors['other_contacts'])){
                echo $this->General->errorHtml($errors['other_contacts']);
                } ;?>
          </div>


           <div class="form-group restagsinputwrapper1 __wrapper">
                  <h3 class="box-title">Add / Remove Projects</h3>
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <?php echo $this->Form->input("project_id",array("type"=>"text","class"=>"restag51nput1 responsible other_aliases",'placeholder'=> "To add Roles", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>
                </div>               
                 
                <div class="errors"></div>
          </div>



         <div class="form-group"> 
               <label for="inputName" class="control-label">Notes:</label>

<?php
				 echo $this->Form->input("notes",array("class"=>"form-control","label"=>false,"div"=>false));
				 echo $this->Form->hidden("id");
         echo $this->Form->hidden("saved_projects",array("class"=>"form-control saved_projects","label"=>false,"div"=>false,'value' => $ContactsData->project_id));
            ?></div>

          <div class="form-group">
                       <label for="exampleInputpwd1">Current Client?:</label>
                       <div class="input-group">
                       <?php
					 echo $this->Form->input("iscurrent",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"id"=>"iscurrent",'hiddenField'=>true));
					//echo "<label class='projectlbl' for ='iscurrent'>Daily Update </label><br/>";
				?>

				</div></div>
	
<div class="form-group">
   <button type="submit" class="btn btn-success submit-btn">Submit</button>
 <?php 
  echo $this->Html->link("Reset",  ['controller'=>'contacts','action'=>'edit','prefix' => 'contacts','id'=>$id],['class'=>"btn btn-inverse waves-effect waves-light"]);?>

 </div>
 </div>
</div>
    <!-- end id-form  -->

 <div class="col-sm-6">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">
   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading">Contacts Management</h4>
         Contacts Management is used to keep track of names, addresses, telephone numbers, and other information. 
                    <ul >
                 <li> <?php  echo $this->Html->link("Go To Listing",array('controller'=>'contacts','action'=>'contactslist'), array('style'=>'color:red;'));?>
               </li> 
             
						
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            
   </div>   
 </div>
</div>
 <?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','custom_jquery','common','clients_page')) ?>
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/tagnew.js"></script>
<script src="<?php echo BASE_URL; ?>js/common_form.js"></script>
   
</body>
</html>


