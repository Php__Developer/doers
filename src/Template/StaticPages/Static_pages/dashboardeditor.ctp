<!-- Bootstrap Core CSS -->
<!-- <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- animation CSS -->
<!-- <link href="css/animate.css" rel="stylesheet"> -->
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
<!-- animation CSS -->
<!-- <link href="css/animate.css" rel="stylesheet"> -->
<!-- Custom CSS -->
<!-- <link href="css/style.css" rel="stylesheet"> -->
<!-- color CSS -->
<!-- <link href="css/colors/blue.css" id="theme"  rel="stylesheet"> -->
<?php echo $this->Html->css(array('colors/blue','animate')); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="http://www.w3schools.com/lib/w3data.js"></script>

<div class="row">
        <div class="">
          <div class="white-box">
            <h3 class="box-title m-b-0">Template Management</h3>
            <p class="text-muted m-b-30">This Section is used by Admin and HR to edit dashboard.</p>
          <?php echo $this->Form->create('StaticPage',array('url' => ['action' => 'dashboardeditor'],'method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
             <?php
				 echo $this->Form->textarea("content",array("class"=>"inp-form content","label"=>false,"div"=>false,'id' => 'mymce','value' => $data['value']));
              ?>
           <?php 
			//echo $this->Form->hidden("id",['value' => $SkillsData['id']]);
			?>
			<br>
			   <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
          </div>
        </div>
<?php echo $this->Html->script(array('jquery-3',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min')); ?>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.min.js"></script>
<script>
   
    $(document).ready(function(){

        $('#mymce').summernote({
            height: 350,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        $('.inline-editor').summernote({
            airMode: true            
        });

   });
   
  window.edit = function() {
          $(".click2edit").summernote()
      }, 
  window.save = function() {
          $(".click2edit").destroy()
      }
</script>
<!--Style Switcher -->
<script src="../plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>