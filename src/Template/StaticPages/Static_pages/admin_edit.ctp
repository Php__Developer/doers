<?php echo $javascript->link('ckeditor/ckeditor.js'); ?>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('StaticPage',array('action'=>'edit','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top"> Category: </th>
			<td>
			<?php
				$options=$general->getCMSCategory();
				echo $form->input('category', array("type"=>'select',"class"=>"ourselect",'options' => $options,'default'=>'1','empty' => '--Select--',"label"=>false,"div"=>false)); 
            ?>	
			</td>
		</tr>
		<tr>
			<th valign="top">Role:</th>
			<td>	
			<div style="float:left">
			<?php
								foreach($resultData as $key=>$value) :
										$checked =false ;
									/*if(!empty($this->data['StaticPage']['role_id']) && in_array($key, $this->data['StaticPage']['role_id'])){
										$checked =true;
									}*/
									echo $form->input("StaticPage.role_id.$key",array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"value"=>$key,"id"=>$key,"hiddenField"=>false,'onchange'=>"javascript:chk_id(this.id)",'hiddenField'=>false,"checked"=>$checked));
									echo "<label for =". $key.">".$value."</label><br/>";
							endforeach;
						?>
						</div>
						<div id="usrdat" style="float:right;border: 1px solid gray; border-radius:4px; padding: 5px;  height: 125px;width: 500px;
		overflow-y: scroll;" >
						<?php  
								if(isset($this->data['StaticPage']['userData']))
								{
									echo $this->element('show_users',array('userData' => $this->data['StaticPage']['userData']));}
								else {
									echo "<div align='center' style='color:red'>Please Select Role to Get Users!</div>";
								}
							?>
				</div>
				<div style="clear:both"></div>
			</td>
		</tr>
		<tr><th>&nbsp;</th><td>&nbsp;</td></tr>
		<tr>
			<th valign="top">Title:</th>
			<td><?php
				 echo $form->input("title",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
				<tr>
			<th valign="top">Content</th>
			<td>
			<?php
				 echo $form->textarea("content",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?>
			</td>
			<script>
			CKEDITOR.replace('data[StaticPage][content]', { "width": "750" }); 
			</script></tr>
		<tr>
			<th valign="top">Is Default</th>
			<td><?php
				 echo $form->input("is_default",array('type'=>'checkbox',"label"=>false,"div"=>false,"id"=>"is_default",'hiddenField'=>true));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top"><span class="star">&nbsp;</span>Meta Keywords </th>
			<td class="noheight">
		<?php
				 echo $form->input("meta_keywords",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?>
			</td></tr>
			<tr>
			<th valign="top"><span class="star">&nbsp;</span>Meta Description </th>
			<td class="noheight">
		<?php
				 echo $form->input("meta_description",array("class"=>"inp-form","label"=>false,"div"=>false));
            ?>
			</td>			
			<!-- Fckeditor Ends here -->
			<tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			
			<?php 
			echo $this->Form->hidden("id");
			echo $this->Form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $this->Form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/static_pages/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $this->Html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php  $this->Html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Template Management</h5>
							This Section is used by Admin and Sales only to Manage templates.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Go To Listing",
            array('controller'=>'static_pages','action'=>'list')
            );	
            ?>
            </li> 
						
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->
	