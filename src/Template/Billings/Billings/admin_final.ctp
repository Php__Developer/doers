
<?php echo $this->Html->css('screen'); ?>

<!--<head>
<style>
@charset "utf-8";
/* CSS Document */


</style>
<!--<link rel="stylesheet" type="text/css" href="style.css" />-->



    	<div class="wrapper-ab">
            <div class="main-ab">
            <div class="heading-ab">Admin Reports - For HR Department</div>
            <div class="box-ab"><div class="graf-ab"><a href="">Salary Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Lead Report (Date Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Performance Report (Date Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Performance Report (User / Month Wise)</a></div></div>
			<div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Learning Center Report (Date Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Learning Center Report (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">User Ticket Report (Date Wise)</a></div></div>
			<div class="cleaner"></div>
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Finance Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a href="">Week Wise Billing Report</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Week Wise Payment Report</a></div></div>
			<div class="cleaner"></div>
            
             </div>
             <div class="main-ab">
            <div class="heading-ab">Admin Reports - Sales Deparment</div>
            <div class="box-ab"><div class="graf-ab"><a href="">Pie Chart of No. of Bids (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">No of Active Leads (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">No of Win / Closed Projects (User Wise)</a></div></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Over Lead Count along with Status (User Wise)</a></div></div>
			<div class="cleaner"></div>
            <div class="box-ab"><div class="graf-ab"><a href="">Company Wide Line Graph - Active Lead / Closed / Total Bids</a></div></div>
            
            <div class="cleaner"></div>
            </div>

    </div>
