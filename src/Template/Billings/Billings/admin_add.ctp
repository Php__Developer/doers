	<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#week_start" ).datepicker({
				dateFormat: 'dd-mm-yy',
				firstDay: 1, // Start with Monday
				minDate: 0,
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}
			});
	});
		
</script>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('Billing',array('action'=>'add','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<?php $session->flash(); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
	<!-- start id-form -->
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Project name::</th>
			<td><?php
				$options=$general->getprojects();
				 echo $form->input("project_id",array( "type"=>'select',"class"=>"styledselect_form_1",'options'=>$options,"label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Week start:</th>
			<td><?php
				 echo $form->input("week_start",array("type"=>"text","readonly"=>true,"id"=>"week_start","class"=>"inp-form","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top"> Maximum limit:</th>
		<td><?php
		echo $form->input("max_limit",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top"> Todo Hour:</th>
		<td><?php
		echo $form->input("todo_hours",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top"> Monday Hour:</th>
		<td><?php
		echo $form->input("mon_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top"> Tuesday Hour:</th>
		<td><?php
		echo $form->input("tue_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Wednesday Hour:</th>
		<td><?php
		echo $form->input("wed_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Thursday Hour:</th>
		<td><?php
		echo $form->input("thur_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Friday Hour:</th>
		<td><?php
		echo $form->input("fri_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Saturday Hour:</th>
		<td><?php
		echo $form->input("sat_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
		<th valign="top">Sunday Hour:</th>
		<td><?php
		echo $form->input("sun_hour",array("class"=>"inp-form","label"=>false,"div"=>false));
		?></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Notes:</th>
			<td><?php
				 echo $form->input("notes",array("class"=>"form-textarea","label"=>false,"div"=>false));
            ?></td>
			<td></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/billings/list'")); 
			?>
			</td>
			<td></td>
		</tr>
	</table>
	<!-- end id-form  -->

			</td>
			<td>
<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_act.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">				
				<div class="left"><a href=""><?php $html->image(BASE_URL."images/forms/icon_edit.gif", array("alt"=>"Edit",'width'=>"21",'height'=>"21"))?></a></div>
				<div class="right">
					<h5>Billings Management</h5>
          This section is used by Admin and PM only to Manage senstive billings.
          <div class="lines-dotted-short"></div>
					<ul class="greyarrow">
  					<li>
            <?php 
            echo $html->link("Go To Listing",
            array('controller'=>'billings','action'=>'list')
            );	
            ?>
            </li> 
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->
			</td>
		</tr>
	</table>
	<!-- end id-form  -->
	<div class="clear"></div>
	</div>
	<!--  end content-table-inner  -->