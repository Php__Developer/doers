<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker','popup.css')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->css(array("jquery_ui_datepicker")); ?>

<?php //echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','function.js')); ?>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>



<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function() {
			alphanum();
	});
	function alphanum(){
		$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	}
	function editRecord(id){
		spantoInput('max_'+id);
		spantoInput('todo_'+id);
		spantoInput('mon_'+id);
		spantoInput('tue_'+id);
		spantoInput('wed_'+id);
		spantoInput('thur_'+id);
		spantoInput('fri_'+id);
		spantoInput('sat_'+id);
		spantoInput('sun_'+id);
			$('body div#ui-datepicker-div').css({'display':'none'});
		data_span = $('#notes_'+id).html();
		$('#notes_'+id).replaceWith('<textarea id="notes_'+id+'" rows="2" cols="1">'+data_span+'</textarea>');
		changeLink('edit_'+id);
		alphanum();
	}
	function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="numeric form-control" style="width:43px;height:30px;"/>');
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}
	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}
	function editData(id){
		if(($('#max_'+id).val() == "") || parseFloat($('#max_'+id).val()) > parseFloat('200'))	{  
$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
			$('#error_msg').html("Enter valid max limit less than 200 ");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return false;
		}
		if(($('#todo_'+id).val() == "") || parseFloat($('#todo_'+id).val()) > parseFloat('200'))	{  
$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
			$('#error_msg').html("Enter valid to-do hours less than 200");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return false;
		}
		if(checkLimit($('#mon_'+id).val()) || checkLimit($('#tue_'+id).val()) || checkLimit($('#wed_'+id).val()) || checkLimit($('#thur_'+id).val()) || checkLimit($('#fri_'+id).val()) || checkLimit($('#sat_'+id).val()) || checkLimit($('#sun_'+id).val()))	{  
			return false;
		}
			var postdata = {	'id':id,
										'project_id' : $('#projectid_'+id).val(), 
										'max_limit':$('#max_'+id).val(),
										'todo_hours':$('#todo_'+id).val(),
										'mon_hour':$('#mon_'+id).val(),
										'tue_hour' : $('#tue_'+id).val(),
										'wed_hour':$('#wed_'+id).val(),
										'thur_hour':$('#thur_'+id).val(),
										'fri_hour':$('#fri_'+id).val(),
										'sat_hour' : $('#sat_'+id).val(),
										'sun_hour' : $('#sun_'+id).val(), 
										'notes' : $('#notes_'+id).val(),
									};
						$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
				var base_url = "<?php echo $this->Url->build('/', true); ?>";

			$.post(base_url+'billings/quickview',postdata,function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Billing has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					get_total(id);
					updateRecord(id);
				}
			});
		}
	}
	function deleteRecord(id){
					$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			var base_url = "<?php echo $this->Url->build('/', true); ?>";
			$.post(base_url+'billings/quickview',{'id':id,'key':'delete'},function(data){
				if(data==1){
					$('.ajaxloaderdiv').hide();
					$('#max_'+id).closest("tr").remove();
					$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Billing has been deleted successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					get_total(id);
				}
			});
		}
	}
/*	function verifyRecord(id){
			$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			var base_url = ;
			$.post(base_url+'billings/verifybilling',{'id':id,'key':'verify'},function(data){
				//alert(data);
				console.log(data);
				if(data==1){
				$('.ajaxloaderdiv').hide();
				$('#verify_'+id).toggle();
				$('#unverify_'+id).toggle();
				$("#success").html("Billing has been veryfied successfully!");
				$('#success').show();
				setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
				
				}
			});
		}
	} */
	$(document).ready(function () {
   //alert("Working");
   $(document).on('click','.changeverify',function(){
   	verifyornot = $(this).attr('title');
   	console.log(verifyornot);
   	if(confirm("Are you sure you want to "+verifyornot+"?") == true){
   		var id = $(this).attr('id').split('_')[1];
   		$('.verify_wrapper').removeClass('justclicked');
   		$(this).closest('.verify_wrapper').addClass('justclicked');
   		console.log("working");
   		var base_url = "<?php echo $this->Url->build('/', true); ?>";
   		 if(verifyornot == 'Verify'){
   			var v =	'verify';
   			var url = 'verifybilling';
   		} else{
   			var v =	'unverify';
   			var url = 'quickview';
   		}
   		$.post(base_url+'billings/'+url,{'id':id,'key': v },function(data){
				console.log(data);
				console.log(id);
				if(data==1){
				$('.ajaxloaderdiv').hide();
				console.log($('.justclicked').closest('td').find('.unverifyimg').attr('id'));
				$('.justclicked').closest('td').find('.verify_wrapper').html('');
				console.log(verifyornot);
				if(verifyornot == 'Verify'){
					console.log("do verify");
					$('.justclicked').closest('td').find('.verify_wrapper').append('<a href="javascript:void(0)" class="unverifyimg changeverify" title="Unverify" id="verify_'+id+'"></a>')
				}else {
					$('.justclicked').closest('td').find('.verify_wrapper').append('<a href="javascript:void(0)" class="verifyimg changeverify" title="Verify" id="verify_'+id+'"></a>')
				}
				
				//$('.justclicked').closest('td').find('.verify_wrapper').html('asdf');
			
				//$('.justclicked').closest('td').find('.verifyimg').toggle();
				$('#unverify_'+id).toggle();
				$('#success').attr('style','  background: #00c292 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
					$("#success").html("Billing has been unveryfied successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					
				}
			});
			//return true;
		}
		return false;
   })
});



	/*function UnverifyRecord(id){
		$('.ajaxloaderdiv').show();
		if($.active>0){
		}else{
			var base_url = 
			$.post(base_url+'billings/quickview',{'id':id,'key':'unverify'},function(data){
				console.log(data);
				console.log(id);
				if(data==1){
				$('.ajaxloaderdiv').hide();	
				$('#verify_'+id).toggle();
				$('#unverify_'+id).toggle();
					$("#success").html("Billing has been unveryfied successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 3000);
					
				}
			});
		}
	}*/
	
	
	function get_total(id)
	{
	
		var total  = 0;
		total  = parseFloat(total+(($('#mon_'+id).val()!="")? parseFloat($('#mon_'+id).val()):0));
		total  = parseFloat(total+(($('#tue_'+id).val()!="")? parseFloat($('#tue_'+id).val()):0));
		total  = parseFloat(total+(($('#wed_'+id).val()!="")? parseFloat($('#wed_'+id).val()):0));
		total  = parseFloat(total+(($('#thur_'+id).val()!="")? parseFloat($('#thur_'+id).val()):0));
		total  = parseFloat(total+(($('#fri_'+id).val()!="")? parseFloat($('#fri_'+id).val()):0));
		total  = parseFloat(total+(($('#sat_'+id).val()!="")? parseFloat($('#sat_'+id).val()):0));
		total  = parseFloat(total+(($('#sun_'+id).val()!="")? parseFloat($('#sun_'+id).val()):0));
		if(isNaN(total)){
			total = 0.00;
		}
		$('#tot_'+id).html(total.toFixed(2));
	}
	function updateRecord(id){
		inputToSpan('max_'+id);
		inputToSpan('todo_'+id);
		inputToSpan('mon_'+id);
		inputToSpan('tue_'+id);
		inputToSpan('wed_'+id);
		inputToSpan('thur_'+id);
		inputToSpan('fri_'+id);
		inputToSpan('sat_'+id);
		inputToSpan('sun_'+id);
		inputToSpan('notes_'+id);
		changeLinkBack('edit_'+id);
	}
	function is_delete(){
			if(confirm("Are you sure you want to delete?") == true){
			return true;
		}
		return false;
	}
	function is_verify(){
			if(confirm("Are you sure you want to verify?") == true){
			return true;
		}
		return false;
	}
	function is_unverify(){
			if(confirm("Are you sure you want to unverify?") == true){
			return true;
		}
		return false;
	}
	function checkLimit(val){
		if(parseFloat(val) > parseFloat('24')){
$('#error_msg').attr('style','  background: #fb9678 none repeat scroll 0 0;border-color: #00c292;color: #ffffff;padding:20px;');
			$('#error_msg').html("Daily billing hours cannot be greater than 24");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return true;
		}
	}
	
	
</script>
<style>
.ajaxloaderdiv{
  height: 100%;
  width:100%;
  position: absolute;
  background-color: white; /* for demonstration */
  opacity:.8;
  top:0;
  display:none;
 
	}
.ajaxloader_img{
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  margin: auto; /* presto! */
}
</style>


<!--  start content-table-inner -->
	<div class="col-sm-2>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	<!--  start table-content  -->
			<div id="table-content">
			<center><h2 class="font-bold text-success"> Billing Details </h2>
				<div id="success"></div>
				<div id="error_msg"></div>
				  <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable m-l-5" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="margin-top:10px;font-size:12px;" border="0" width="100%" cellpadding="0" cellspacing="0">
				
				 <thead>
                <tr>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Week Start</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Max</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">To-do</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn"><abbr title="Rotten Tomato Rating">M</abbr></button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">T</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">W</button></th>
                   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Th</button></th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">F</button></th>
                     <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">S</button></th>
                     <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Su</button></th>
                      <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Total</button></th>
                       <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Notes</button></th>
                 
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Options</button></th>
                </tr>
              </thead>
              <tbody>
				
					
				<?php 
				//echo"<pre>";print_r($resultData);
				if(count($resultData)>0){ 
					//pr($resultData);
		
				foreach($resultData as $paymentData):
					
				$id = $paymentData['id'];
				
				?>
				<tr>
					<td><span id="week_<?php echo $id; ?>"><?php echo date("d-m-Y",strtotime($paymentData['week_start'])); ?></span></td>
					<td><span id="max_<?php echo $id; ?>"><?php  echo $paymentData['max_limit']; ?></span></td>
					<td><span id="todo_<?php echo $id; ?>"><?php echo $paymentData['todo_hours']; ?></span></td>
					<td><span id="mon_<?php echo $id; ?>"><?php  echo $paymentData['mon_hour']; ?></span></td>
					<td><span id="tue_<?php echo $id; ?>"><?php echo $paymentData['tue_hour']; ?></span></td>
					<td><span id="wed_<?php echo $id; ?>"><?php  echo $paymentData['wed_hour']; ?></span></td>
					<td><span id="thur_<?php echo $id; ?>"><?php echo $paymentData['thur_hour']; ?></span></td>
					<td><span id="fri_<?php echo $id; ?>"><?php  echo $paymentData['fri_hour']; ?></span></td>
					<td><span id="sat_<?php echo $id; ?>"><?php echo $paymentData['sat_hour']; ?></span></td>
					<td><span id="sun_<?php echo $id; ?>"><?php  echo $paymentData['sun_hour']; ?></span></td>
					<td><span id="tot_<?php echo $id; ?>"><?php echo number_format($paymentData['mon_hour']+$paymentData['tue_hour']+$paymentData['wed_hour']+$paymentData['thur_hour']+$paymentData['fri_hour']+$paymentData['sat_hour'] +$paymentData['sun_hour'],2)  ;?></span></td>
					<td><span id="notes_<?php echo $id; ?>"><?php  echo $paymentData['notes']; ?></span></td>
					<td>
					<?php	echo $this->Html->link("","javascript:void(0)",
						array('class'=>'icon-1 info-tooltip','title'=>'Edit','id'=>'edit_'.$id,'onClick'=>"javascript : editRecord(".$id.");")
					); ?>
					<?php  echo $this->Html->link("","javascript:void(0)",
						array('class'=>'info-tooltip icon-2 delete','title'=>'Delete','id'=>'del_'.$id,'onClick'=>"if(is_delete()){deleteRecord(".$id.");}")
					);?>
					<div class="verify_wrapper">
						<?php 
					//$currentWeekMonday = date('Y-m-d',time()+( 1 - date('w'))*24*3600);

					if($currentWeekMonday != $verificationWeek[$id]){
/*
					if($verified[$id]==0){

						echo $this->Html->link("","javascript:void(0)",
						array('class'=>'verifyimg','title'=>'verify','id'=>'verify_'.$id,'onClick'=>"if(is_verify()){verifyRecord(".$id.");}")
					);
					 $this->Html->link("","javascript:void(0)",
						array('class'=>'unverifyimg','title'=>'unverify','id'=>'unverify_'.$id,'style'=>'display:none','onClick'=>"if(is_unverify()){UnverifyRecord(".$id.");}")
					);
				echo  $this->Html->link("","javascript:void(0)",
						array('class'=>'unverifyimg','title'=>'unverify','id'=>'unverify_'.$id,'style'=>'display:none')
					);

					  } else if($verified[$id]==1) {?>
					<?php  echo $this->Html->link("","javascript:void(0)",
						array('class'=>'unverifyimg','title'=>'unverify','id'=>'verify_'.$id)
					);
					echo $this->Html->link("","javascript:void(0)",
						array('class'=>'verifyimg','title'=>'verify','id'=>'verify_'.$id,'style'=>'display:none','onClick'=>"if(is_verify()){verifyRecord(".$id.");}")
					);  }*/

					if($verified[$id]==0){
					
						echo  $this->Html->link("","javascript:void(0)",
						array('class'=>'verifyimg changeverify','title'=>'Verify','id'=>'verify_'.$id )
					);
					  } else if($verified[$id]==1) {?>
						<?php  echo $this->Html->link("","javascript:void(0)",
							array('class'=>'unverifyimg changeverify','title'=>'Unverify','id'=>'verify_'.$id)
						);
				   	}
					}
					?>

					</div>
					</td>
				</tr>
				<?php 
				endforeach; ?>
				<?php } else { ?> 
				<tr>
					<td colspan="13" class="no_records_found">No records found</td>
				</tr>
		<?php } ?>
				</table>
				</center>
				<!--  end product-table --> 
			</div>
			<!--  end content-table  -->
			<div class="clear"></div>

	</td>
</table>
<div class="ajaxloaderdiv"><?php echo $this->Html->image('ajax_loader_gray.gif', array('alt' => 'CakePHP','class'=>'ajaxloader_img'));?> </div>

<div class="clear"></div>
</div>
<!--  end content-table-inner  -->


<?php 
    echo  $this->Html->script(array('jquery-3','migrate','common','listing','project'));
    $this->Html->script(array('jquery-3','migrate',"jquery-ui/jquery-ui.min",'datetimepicker/jquery.datetimepicker.min','jquery_timepicker_latest' , 'allajax.js','common','listing','project'));
?>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

<!-- 		<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
 -->

<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
