<?php echo $javascript->link(array('jquery.mousewheel-3.0.4.pack.js','jquery.fancybox-1.3.4.js')); ?>
<?php echo $html->css(array("jquery.fancybox-1.3.4.css")); ?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function() {
			alphanum();
	});
	function alphanum(){
		$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	}
	function editRecord(id){
		spantoInput('mon_'+id);
		spantoInput('tue_'+id);
		spantoInput('wed_'+id);
		spantoInput('thur_'+id);
		spantoInput('fri_'+id);
		spantoInput('sat_'+id);
		spantoInput('sun_'+id);
		changeLink('edit_'+id);
			$('body div#ui-datepicker-div').css({'display':'none'});
		alphanum();
	}
	function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="numeric" style="width:40px;height:20px;"/>');
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}
	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}
	function editData(id){
		if(checkLimit($('#mon_'+id).val()) || checkLimit($('#tue_'+id).val()) || checkLimit($('#wed_'+id).val()) || checkLimit($('#thur_'+id).val()) || checkLimit($('#fri_'+id).val()) || checkLimit($('#sat_'+id).val()) || checkLimit($('#sun_'+id).val()))	{  
			return false;
		}
			var postdata = {	'id':id,
										'project_id' : $('#projectid_'+id).val(), 
										'max_limit' : $('#max_'+id).html(), 
										'todo_hours':$('#todo_'+id).html(),
										'mon_hour':$('#mon_'+id).val(),
										'tue_hour' : $('#tue_'+id).val(),
										'wed_hour':$('#wed_'+id).val(),
										'thur_hour':$('#thur_'+id).val(),
										'fri_hour':$('#fri_'+id).val(),
										'sat_hour' : $('#sat_'+id).val(),
										'sun_hour' : $('#sun_'+id).val(), 
										'notes' : $('#notes_'+id).val(),
									};
		if($.active>0){
		}else{							
			$.post(base_url+'admin/billings/quickview',postdata,function(data){
				if(data==1){
					get_total(id);
					updateRecord(id);
					$("#success").html("Report has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 5000);
				}else if(data=='error'){
					alert("Billing entry for this week already exists!");
				}
			});
		}
	}
	function get_total(id)
	{
		var total  = 0; 
		var left = 0;
		total  = parseFloat($('#mon_'+id).val()) + parseFloat($('#tue_'+id).val()) + parseFloat($('#wed_'+id).val()) + parseFloat($('#thur_'+id).val()) +  parseFloat($('#fri_'+id).val()) + parseFloat($('#sat_'+id).val()) + parseFloat($('#sun_'+id).val()) ;
		if(isNaN(total)){
			total = 0.00;
		}
		left = parseFloat($('#todo_'+id).html()) - parseFloat(total);
		$('#tot_'+id).html(total.toFixed(2));
		$('#left_'+id).html(left.toFixed(2));
	}
	function updateRecord(id){
		inputToSpan('mon_'+id);
		inputToSpan('tue_'+id);
		inputToSpan('wed_'+id);
		inputToSpan('thur_'+id);
		inputToSpan('fri_'+id);
		inputToSpan('sat_'+id);
		inputToSpan('sun_'+id);
		changeLinkBack('edit_'+id);
	}
	function checkLimit(val){
		if(parseFloat(val) > parseFloat('24')){
			$('#error_msg').html("Daily billing hours cannot be greater than 24");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return true;
		}
	}
	//------------------------Adding Calender----------------------------------------------//
	$(document).ready(function(){
		 //calender settings
		new JsDatePick({
	   useMode:2,
	   target:"startdate",
	   dateFormat:"%d/%m/%Y",
	   yearsRange:[2013,2020],
	   limitToToday:true
	   });
	   //fancybox settings
		$(".view_report").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	960, 
				'height' :	1000, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		});
  });
  
  //------------------------Adding Calender----------------------------------------------//
	$(document).ready(function(){
		 //calender settings
		new JsDatePick({
	   useMode:2,
	   target:"enddate",
	   dateFormat:"%d/%m/%Y",
	   yearsRange:[2013,2020],
	   limitToToday:true
	   });
	   //fancybox settings
		$(".view_report").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	960, 
				'height' :	1000, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		});
  });
  
  //------------------------Adding javascript for charts---------------//
  $(function () {
        $('#container').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Measuring working hours of employees'
            },
            
            xAxis: {
                categories: ['Laxmi Kant', 'Anand', 'Amit', 'Rishi', 'Rajendra'],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'working (hours)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' Hours'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'August 2014',
                data: [107, 31, 635, 203, 100]
            }, {
                name: 'September 2014',
                data: [133, 156, 477, 408, 90]
            }, {
                name: 'October 2014',
                data: [973, 914, 405, 732, 400]
            }]
        });
    });

</script>
<html>
<head>
<style>
.contentbox{
height:400px;
width:380px;
float:left;
/*border:1px solid black;*/
text:center;
margin-left:1px;
margin-top:2px;
}
.innerdiv
{
float:left;
height:380px;
width:386px;
/*border:1px solid black;*/
margin_left:5px;
}
.reportheading
{
text-align:center;
}
</style>
</head>
<table>
<tr>


<?php echo $form->create('Billing',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform")); ?>
<th align="left" >From:</th><td>

			<?php
				 echo $form->input("add_date",array("class"=>"inp-form","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>date('d/m/Y')));
            ?></td>
			
<th align="left" >To:</th><td>

			<?php
				 echo $form->input("add_date1",array("class"=>"inp-form","label"=>false,"div"=>false,"id"=>"enddate","readonly"=>true, "value"=>date('d/m/Y')));
            ?></td>
			
<td><?php echo $form->end('Go',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm3", "id" => "mainform3",)); ?></td></tr></table>
<!--  start content-table-inner --><!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
	
	<!--  start table-content  -->
			<div id="table-content">
			<?php echo $session->flash(); ?>
		<br/>
				<!--<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<div id="success" style="color:green;font-size:13px;float:left; padding-bottom:5px;"></div>
				<div id="error_msg" style="color:red;font-size:13px;float:left; padding-bottom:5px;"></div>-->
				
				<h1><b>Admin Report</b></h1>
				</br><hr></hr></br>
				
				<div class="innerdiv"><span><h2 class="reportheading">Sales</h2></span><div class="contentbox">
				
					
		<style type="text/css">
${demo.css}
		</style>
		

<script src="../../js/highcharts.js"></script>
<script src="../../js/modules/exporting.js"></script>

<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

</div></div>
				<div class="innerdiv"><span><h2  class="reportheading">Market</h2></span><div class="contentbox"> <a class="addLinks fancybox" href="<?php echo BASE_URL."admin/billings/report2"; ?>">Market Graph</a></div></div>
				<div class="innerdiv"><span><h2  class="reportheading">Bussiness</h2></span><div class="contentbox"></div></div>
				
				<!--</table>-->
				<!--  end product-table................................... --> 
			</div>
			<!--  end content-table  -->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
			<td>
				
			</td>
			</tr>
			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>

	</td>
	<td>


	
	
	

</td>
</tr>
</table>
 
<div class="clear"></div>
 

</div>
<?php echo $form->end(); ?>
<!--  end content-table-inner  -->