<link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<!-- Menu CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.css" />
<!-- Dropzone css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<!-- morris CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo BASE_URL; ?>css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo BASE_URL; ?>css/style_new.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
     <!-- wysihtml5 CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<!-- color CSS -->
<link href="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>css/colors/blue.css" id="theme"  rel="stylesheet">
   <link href="<?php echo BASE_URL; ?>js/tpt/angular-block/dist/angular-block-ui.min.css" rel="stylesheet">
<?php echo $this->Html->css(array('jquery_ui_datepicker')); ?>

<input type="hidden" class="base" value ="<?php echo $this->Url->build('/', true);?>">
<link href="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>

<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	//-------------------------Calender-------------------------//
	$(function() {
	$( "#startdate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			  //minDate: 0,
			firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 1,''];
				}	
		});
	$( "#enddate" ).datepicker({
			  ampm: true,
			  dateFormat: 'yy/mm/dd',
			 // minDate: 0,
			// maxDate:new Date(),
			 firstDay: 1, // Start with Monday
				beforeShowDay: function(date) {
					return [date.getDay() === 0,''];
				}
			 
		 });
	});
<!------------------>
function validateForm(){
		
		if(($('#enddate').val()!="" && $('#startdate').val()=="") || ($('#enddate').val()=="" && $('#startdate').val()!="")){
			var message = "<div>Enter both start and end date</div>";
			$('#error').html(message);
			return false;
		}else if(($('#enddate').val()!="" && $('#startdate').val()!="")){
			var str_date = $('#startdate').val();
			var end_date = $('#enddate').val();
			if ((Date.parse(str_date)) > (Date.parse(end_date))) {
				var message = "<div>Start date should be less than end date</div>";
				$('#error').html(message);
				return false;
			}
		}
	}	
	
function weekFrom()
    {
		var last_date=document.getElementById("startdate").value;
		var date_arr=last_date.split("/");
		var date = new Date(date_arr[0], (date_arr[1]-1), date_arr[2]);
		date.setDate(date.getDate() - 7);
		var end = date.getFullYear()+'/'+(date.getMonth()+1)+'/'+ date.getDate() ;
		document.getElementById("startdate").value=end;
	}
  
		 function weekFrom1()
     {
		var last_date=document.getElementById("enddate").value;
		var date_arr=last_date.split("/");
		var date = new Date(date_arr[0], (date_arr[1]-1), date_arr[2]);
		date.setDate(date.getDate() - 7);
		var end = date.getFullYear()+'/'+(date.getMonth()+1)+'/'+ date.getDate() ;
		document.getElementById("enddate").value=end;
	 }
  function nextWeek()
      {
		var last_date=document.getElementById("startdate").value;
		var date_arr=last_date.split("/");
		var date = new Date(date_arr[0], (date_arr[1]-1), date_arr[2]);
		date.setDate(date.getDate() + 7);
		var end = date.getFullYear()+'/'+(date.getMonth()+1)+'/'+ date.getDate() ;
			document.getElementById("startdate").value=end;
   
		}
  </script>
<!--  start content-table-inner -->
<h1 align="center">Week wise Billing Report</h1>
<?php echo $this->Form->create('Billing',array('method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform",'onsubmit'=>'return validateForm();')); ?>
<div id="content-table-inner" >
<div class="searching_div">
<table width = "100%" cellspacing="0" cellpadding="4" border="0" align="center" class="top-search" >
	<tr>	
	</br>
	<td width="8%">
	<br>
	<b>From Date:</b>
		<div style="float:left;margin-top:10px;">
		</td>
		<td width="5%">
		<?php
		 echo $this->Form->button('<<',array('type'=>'button','id'=>'weekStart','OnClick'=>'weekFrom()','class'=>'buttonnextreport1'));?>
	</td>
	<td width="5%">
	<?php 
		if($fromDate!=""){
		$st_date=date('Y/m/d',strtotime($fromDate));
		}
		else{
		if(date('N')==7){
		$st_date=date('Y/m/d',strtotime("last monday"));
		}
		else{
		$st_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
		}
	}
	 echo $this->Form->input("start_date",array("class"=>"form-control","label"=>false,"div"=>false,"id"=>"startdate","readonly"=>true, "value"=>date('Y/m/d',strtotime($st_date))));
     ?>
	</td>
	<td width="5%">
	 <?php echo $this->Form->button('>>',array('type'=>'button','id'=>'NextWeek','OnClick'=>'nextWeek()','class'=>'buttonnextreport1'));
     ?>
	</div></td>
		<td width="8%">
		</br>
		<b>To Date:</b>
		</td>
		<td width="5%">
		<div style="float:left">
	<?php echo $this->Form->button('<<',array('type'=>'button','id'=>'weekend1','OnClick'=>'weekFrom1()','class'=>'buttonnextreport1 form-control')); ?>
	</td>
			<td width="15%">
	<?php
		if($toDate!=""){
		$todate=date('Y/m/d',strtotime($toDate));
		}else{
		if(date('N')==7){
		$todate=date('Y/m/d',strtotime("sunday"));
		}else{
		$current=date("Y-m-d");
		$date = strtotime($current);
		$date = strtotime("next sunday", $date);
		$todate=date('Y/m/d',$date);
		}
	}
	echo $this->Form->input("end_date",array("class"=>"form-control","label"=>false,"div"=>false,"id"=>"enddate","readonly"=>true, "value"=>date('Y/m/d',strtotime($todate)))); ?>
	</td>
	</div>
		<td width="5%">
		<div style="float:left;margin-top:0px;">
			<?php echo $this->Form->submit("Search", array('class'=>'btn btn-success','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
			</div>
			</td>
		</tr>
	</table>
<!--  end content-table  -->
<div style="color:red;" id="error"></div>
</div>
	<div class ="lead_usr">
	<div style="clear:both"></div>
</div>
<?php echo $this->Form->end(); ?>
<?php //pr($resultData);die();  ?>
<?php if(isset($resultData) && count($resultData)>0){?>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<div id="table-content">
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
<thead>
<tr>
<th class="heading1">Project</th>
<th class="heading1">Date</th>
<th class="heading1">Max</th>
<th class="heading1">Todo</th>
<th class="heading1">Achieve</th>
</tr>
</thead>
 <?php if(count($resultData)>0){
 
			$i = 1; 
			foreach($resultData as $result):
                 // pr($result);die();
			
			$id = $result['id'];
			?>
				<tr>
					
					<td><?php echo $result['project']['project_name']; ?>
							
					</td>
				<td><?php echo $result['week_start']; ?>
							
					</td>

	
					</td>
        <td class="wrapping"><?php echo $max_limit_sum[]=number_format((float)$result['max_limit'], 2, '.', ''); ?> </td>
		<td class="wrapping"><?php echo $todo_sum[]=number_format((float)$result['todo_hours'], 2, '.', '');?> </td> 
		<td class="wrapping"><?php echo $achive_sum[]=number_format((float)$result['mon_hour'], 2, '.', '')
		+number_format((float)$result['tue_hour'], 2, '.', '')+number_format((float)$result['wed_hour'], 2, '.', '')+
		+ number_format((float)$result['thur_hour'], 2, '.', '')+number_format((float)$result['fri_hour'], 2, '.', '')+
		number_format((float)$result['sat_hour'], 2, '.', '')+number_format((float)$result['sun_hour'], 2, '.', '');?>



					</td>
				</tr>
				<?php $i++ ;
				endforeach; ?>
		</tr>
	<?php }?>
	<tr>
		<td class="wrapping" colspan="2" style="text-align:center;color:red;">Total Billing</td>
		<td class="wrapping"><?php echo number_format(array_sum($max_limit_sum),2);?></td>
		<td class="wrapping"><?php echo number_format(array_sum($todo_sum),2);?></td>
		<td class="wrapping"><?php echo number_format(array_sum($achive_sum),2);?></td>
	</tr>
</tbody>
	<?php
		} else{ ?>	
</table>		
</div>
<!--  start table-content  -->
			<div id="table-content">
			<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="tablesorter">
				<tbody>
					<tr>
					<td colspan="13" class="no_records_found">No records found</td>
				</tr>
			</tbody>
		<?php } ?>
				</table>
	<div class="clear"></div>
</div></div>

<!--<script src="<?php echo BASE_URL; ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>-->
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo BASE_URL; ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo BASE_URL; ?>js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!--Morris JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/raphael/raphael-min.js"></script>
<!--
<script src="<?php echo BASE_URL; ?>plugins/bower_components/morrisjs/morris.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo BASE_URL; ?>js/custom.min.js"></script>
<script src="<?php echo BASE_URL; ?>js/dashboard1.js"></script>
<!-- Sparkline chart JavaScript -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
 <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
 <script src="<?php echo BASE_URL; ?>tagsinput/dist/bootstrap-tagsinput.min.js"></script>

 <script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

    <script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>


<script src="<?php echo BASE_URL; ?>plugins/bower_components/dropzone-master/dist/dropzone.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> -->
<!-- Date range Plugin JavaScript -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script> -->
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- <script src="<?php echo BASE_URL; ?>plugins/bower_components/moment/moment.js"></script> -->
<script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>


      
<!--Style Switcher -->
<script src="<?php echo BASE_URL; ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
