<?php echo $this->Html->css(array('oribe')); ?>
<script type="text/javascript">

	$(document).ready(function() {
			alphanum();
  $(".closed").on("click", function(event){
      event.preventDefault();
     $(document).find('.alerttop1').hide();
  });
	
	});
	function alphanum(){
		$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	}
	function editRecord(id){
		spantoInput('mon_'+id);
		spantoInput('tue_'+id);
		spantoInput('wed_'+id);
		spantoInput('thur_'+id);
		spantoInput('fri_'+id);
		spantoInput('sat_'+id);
		spantoInput('sun_'+id);
		changeLink('edit_'+id);
			$('body div#ui-datepicker-div').css({'display':'none'});
		alphanum();
	}
	function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="numeric" style="width:30px;height:27px;"/>');
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}
	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}
	function editData(id){
		if(checkLimit($('#mon_'+id).val()) || checkLimit($('#tue_'+id).val()) || checkLimit($('#wed_'+id).val()) || checkLimit($('#thur_'+id).val()) || checkLimit($('#fri_'+id).val()) || checkLimit($('#sat_'+id).val()) || checkLimit($('#sun_'+id).val()))	{  
			return false;
		}
			var postdata = {	        'id':id,
										'project_id' : $('#projectid_'+id).val(), 
										'max_limit' : $('#max_'+id).html(), 
										'todo_hours':$('#todo_'+id).html(),
										'mon_hour':$('#mon_'+id).val(),
										'tue_hour' : $('#tue_'+id).val(),
										'wed_hour':$('#wed_'+id).val(),
										'thur_hour':$('#thur_'+id).val(),
										'fri_hour':$('#fri_'+id).val(),
										'sat_hour' : $('#sat_'+id).val(),
										'sun_hour' : $('#sun_'+id).val(), 
										'notes' : $('#notes_'+id).val(),
									};
		if($.active>0){
		}else{							
			$.post(base_url+'billings/quickview',postdata,function(data){
				if(data==1){
					get_total(id);
					updateRecord(id);
					setTimeout(function(){
						window.parent.location.href=window.parent.location.href;
					},100); 
						$(".alerttop1").find(".success_text").html("");
						$(".alerttop1").find(".success_text").append('Billing has been updated successfully!.');
						$(".alerttop1").show();
					setTimeout(function() { $('.alerttop1').fadeOut('slow'); }, 2000);

						/*var mon = [];
		//var val=0;
		$(".monday").each(function( index,value) {
			  var val= $(this).text();
		      mon.push(val);
		});
		var aa = 0;
		for (var i = 0; i < mon.length; i++) {
		    aa += mon[i] << 0;
		}
		$('#m_sum').html(aa);

		var tue = [];
		//var val=0;
		$(".tuesday").each(function( index,value) {
			  var val= $(this).text();
		      tue.push(val);
		});
		var bb = 0;
		for (var i = 0; i < tue.length; i++) {
		    bb += tue[i] << 0;
		}
		$('#t_sum').html(bb);

		var wed = [];
		//var val=0;
		$(".wednesday").each(function( index,value) {
			  var val= $(this).text();
		      wed.push(val);
		});
		var cc = 0;
		for (var i = 0; i < wed.length; i++) {
		    cc += wed[i] << 0;
		}
		$('#w_sum').html(cc);

		var thurs = [];
		//var val=0;
		$(".thursday").each(function( index,value) {
			  var val= $(this).text();
		      thurs.push(val);
		});
		var dd = 0;
		for (var i = 0; i < thurs.length; i++) {
		    dd += thurs[i] << 0;
		}
		$('#thu_sum').html(dd);


		var fri = [];
		//var val=0;
		$(".friday").each(function( index,value) {
			  var val= $(this).text();
		      fri.push(val);
		});
		var ee = 0;
		for (var i = 0; i < fri.length; i++) {
		    ee += fri[i] << 0;
		}
		$('#fri_sum').html(ee);


		var sat = [];
		//var val=0;
		$(".saturday").each(function( index,value) {
			  var val= $(this).text();
		      sat.push(val);
		});
		var ff = 0;
		for (var i = 0; i < sat.length; i++) {
		    ff += sat[i] << 0;
		}
		$('#sat_sum').html(ff);

		var sun = [];
		//var val=0;
		$(".sunday").each(function( index,value) {
			  var val= $(this).text();
		      sun.push(val);
		});
		var gg = 0;
		for (var i = 0; i < sun.length; i++) {
		    gg += sun[i] << 0;
		}
		$('#sun_sum').html(gg);*/
				}else if(data=='error'){
					alert("Billing entry for this week already exists!");
				}
			});
		}
	}
	function get_total(id)
	{
	var max=  $('#max_'+id).html();
	var todo= $('#todo_'+id).html();
	var mon= $('#mon_'+id).val();
	var tues= $('#tue_'+id).val();
	var wed=  $('#wed_'+id).val();
	var thurs= $('#thur_'+id).val();
	var fri=  $('#fri_'+id).val();
	var sat= $('#sat_'+id).val();
	var sun=  $('#sun_'+id).val(); 
	var total=+mon + +tues + +wed + +thurs + +fri +  +sat + +sun;
	var left=todo - total;
	$('#tot_'+id).html(total);
	$('#left_'+id).html(left);
	}
	function updateRecord(id){
		inputToSpan('mon_'+id);
		inputToSpan('tue_'+id);
		inputToSpan('wed_'+id);
		inputToSpan('thur_'+id);
		inputToSpan('fri_'+id);
		inputToSpan('sat_'+id);
		inputToSpan('sun_'+id);
		changeLinkBack('edit_'+id);
	
	}
	function checkLimit(val){
		if(parseFloat(val) > parseFloat('24')){
			$('#error_msg').html("Daily billing hours cannot be greater than 24");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return true;
		}
	}
</script>





   <?php echo $this->Form->create('Billing',array('url' => ['action' => 'billingslist'],'method'=>'POST','onsubmit' => '',"class"=>"longFieldsForm" ,'id'=>'mainform')); ?>

<!--  start content-table-inner -->
     <div class="col-sm-12">
    <div class="white-box">
         <div class="form-group">
          <div class="row el-element-overlay m-b-40">

          <center><h4 class="media-heading">Billing Management</h4>
          This section is used by Admin and PM only to Manage sensitive Billings. </center>
          
            
   </div>   
 </div>
</div>
</div>


        <div class="col-sm-12">
<div class="white-box">
            <!-- row -->
            <div class="row">
             
              <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                <div class="">
                  <table class="table table-hover"  id="product-table">
                    <div id="success" style="color:green;font-size:13px;float:left; padding-bottom:5px;"></div>
				<div id="error_msg" style="color:red;font-size:13px;float:left; padding-bottom:5px;"></div>
				<?php if(count($resultData)>0){?>
				<tr>
					<th class="table-header-check">No.</font></th>
					<th class="table-header-repeat line-left"  width="21%"><?php echo $this->Paginator->sort( 'Project.project_name','Project Name');?></th>


					<th class="table-header-repeat line-left"  width="13%"><?php echo $this->Paginator->sort('Billing.week_start','Week start' );?></th>

					<th class="table-header-repeat line-left"  width="5%"><?php echo $this->Paginator->sort( 'Billing.max_limit','Max');?></th>

					<th class="table-header-repeat line-left"  width="5%" nowrap><?php echo $this->Paginator->sort('Billing.todo_hours','To-do');?></th>

				   <th class="table-header-repeat line-left" width="5%"><?php echo $this->Paginator->sort( 'Billing.mon_hour','M');?></th>



				   <th class="table-header-repeat line-left"  width="5%"><?php echo $this->Paginator->sort( 'Billing.tue_hour','T');?></th>


					<th class="table-header-repeat line-left"  width="5%"><?php echo $this->Paginator->sort('Billing.wed_hour','W');?></th>

					<th class="table-header-repeat line-left"  width="5%"><?php echo $this->Paginator->sort('Billing.thur_hour','Th');?></th>

				   <th class="table-header-repeat line-left" width="5%"><?php echo $this->Paginator->sort('Billing.fri_hour','F');?></th>

				   <th class="table-header-repeat line-left"  width="5%"><?php echo $this->Paginator->sort( 'Billing.sat_hour','S');?></th>

				   <th class="table-header-repeat line-left" width="5%"><?php echo $this->Paginator->sort( 'Billing.sun_hour','Su');?></th>

				   <th class="table-header-repeat line-left"  width="6%"><?php echo $this->Paginator->sort('','Total');?></th>

				   <th class="table-header-repeat line-left" width="6%"><?php echo $this->Paginator->sort('','Left');?></th>

				<th class="table-header-repeat line-left" width="9%"><a href="#A">Options</a></th>
				</tr>
				<?php
 
			$i = 1; 
			foreach($resultData as $result):

			
			$id = $result['id'];
			?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $result['project']['project_name']; ?>
							<?php echo $this->Form->hidden("note",array("type"=>"textarea","id" => "notes_".$id,"value" => $result['notes']));?>
					</td>
					<td><span id="week_<?php echo $id; ?>"><?php echo  date("d-m-Y",strtotime($result['week_start'])); ?></span> </td>
					<td><span id="max_<?php echo $id; ?>"><?php echo $result['max_limit']; ?></span></td>
					<td><span id="todo_<?php echo $id; ?>"><?php echo $result['todo_hours']; ?></span></td>
				    <td  class="monday"><span id="mon_<?php echo $id; ?>"><?php echo $mon_sum[]= $result['mon_hour']; ?></span></td>
					<td class="tuesday"><span id="tue_<?php echo $id; ?>"><?php echo $tue_sum[]= $result['tue_hour']; ?></span></td>
					<td class="wednesday"><span id="wed_<?php echo $id; ?>" class="wednesday"><?php echo $wed_sum[]= $result['wed_hour']; ?></span></td>
					<td class="thursday"><span id="thur_<?php echo $id; ?>" class="thursday"><?php echo $thur_sum[]= $result['thur_hour']; ?></span></td>
					<td class="friday"><span id="fri_<?php echo $id; ?>" ><?php echo $fri_sum[]= $result['fri_hour']; ?></span></td>
					<td class="saturday"><span id="sat_<?php echo $id; ?>"><?php echo $sat_sum[]= $result['sat_hour']; ?></span></td>
					<td class="sunday"><span id="sun_<?php echo $id; ?>" ><?php echo $sun_sum[]= $result['sun_hour']; ?></span></td>
					<td><span id="tot_<?php echo $id; ?>"><?php $total = $result['mon_hour']+$result['tue_hour']+$result['wed_hour']+$result['thur_hour']+$result['fri_hour']+$result['sat_hour'] +$result['sun_hour'] ; echo number_format($total, 2);?></span></td>
					<td><span id="left_<?php echo $id; ?>"> <?php $left = ($result['todo_hours'] - $total); echo number_format($left, 2); ?></span></td>
					<td class="options-width" align="center">


					<?php	echo $this->Html->link("","javascript:void(0)",
						array('class'=>'icon-1 info-tooltip','title'=>'Edit','id'=>'edit_'.$id,'onClick'=>"javascript : editRecord(".$id.");")
					); ?>
				







					</td>
				</tr>
				<?php $i++ ;
				endforeach; ?>
				<tr>
		<td class="wrapping" colspan="5" style="text-align:center;color:red;"><b>Total Billing</b></td>
		<td class="wrapping" id="m_sum" ><?php echo number_format(array_sum($mon_sum),2);?></td>
		<td class="wrapping" id="t_sum" ><?php echo number_format(array_sum($tue_sum),2);?></td>
		<td class="wrapping" id="w_sum"><?php echo number_format(array_sum($wed_sum),2);?></td>
		<td class="wrapping" id="thu_sum"><?php echo number_format(array_sum($thur_sum),2);?></td>
		<td class="wrapping" id="fri_sum"><?php echo number_format(array_sum($fri_sum),2);?></td>
		<td class="wrapping" id="sat_sum"><?php echo number_format(array_sum($sat_sum),2);?></td>
		<td class="wrapping" id="sun_sum"><?php echo number_format(array_sum($sun_sum),2);?></td>
		<td class="wrapping" colspan="3"></td>
	    </tr>
				<?php } else { ?>
		<tr>
			<td colspan="15" class="no_records_found"><center><h3 class="text-uppercase">No Record Found !</h3></center><p class="text-muted m-t-30 m-b-30"></p> </td>
		</tr>
			<?php } ?>
				</table>
                </div>
                	<?php if(count($resultData)>0){?>
                <div class="row">
                  <div class="col-xs-7 m-t-20"> Showing 
				<?= $this->Paginator->counter([
				    'format' => ' {{page}} - {{pages}} of
				             {{count}}'
				]) ?>

                   </div>
	                  <div class="col-xs-5 m-t-20  paginateproject">
	         <ul class="pagination paginate-data pull-right">
	            <li class="previousdata">

	                <?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
	            </li>
	            <li>
	                <?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
	            </li>
	        </ul>
	     </div>
                </div>
                <?php
                }?>
              </div>
            </div>
            <!-- /.row -->
          </div>
</div>
<?php echo $this->Form->end(); ?>
    <!-- end id-form  -->


    
      <!-- /.right-sidebar -->



</body>
</html>
