<html>
<body>
<?php echo $this->Html->script(array('jquery-3','jquery.bind','common','listing','jsdatepick/jsDatePick','jquery-ui/jquery-ui.min','datetimepicker/jquery.datetimepicker.min','stuHover','site','jquery-ticker/jquery.ticker','pGenerator.jquery','rating_simple','fancybox/source/jquery.fancybox')) ?>
<?php echo $this->Html->css(array("screen")); ?>
<script type="text/javascript">
	$(document).ready(function() 
	{
		//fancybox settings
		$(".view_template").fancybox({
				'type': 'iframe',
				'autoDimensions' : false,
				'width' :	550, 
				'height' :	650, 
				'onStart': function(){
     		 	 jQuery("#fancybox-overlay").css({"position":"fixed"});
   			}
		
			});
			$("#toggleOrder").click(function(){
			var value = $(this).val() ;
			if(value == "Orber by Deadline"){
				$("#toggleOrder").val('Orber by Modified');
				var pathArray = window.location.pathname.split( '/' );
				if(pathArray[5]=="key" ||  pathArray[5]=="modified"){
					if(pathArray[7] == "undefined"){
						pathArray[7]="";
					}
					$('#ticketReport').attr(	'action',base_url+'admin/tickets/report/deadline/'+pathArray[6]+'/'+pathArray[7]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/report/deadline');
				}
			}else if(value == "Orber by Modified"){
				$("#toggleOrder").val('Orber by Deadline');
				var pathArray = window.location.pathname.split( '/' );
				if($("#toggleOpen").val()==1){
					$('#ticketReport').attr('action',base_url+'admin/tickets/report');
				}else if(pathArray[5]=="key" ||  pathArray[5]=="deadline"){
					if(pathArray[7] == "undefined"){
						pathArray[7]="";
					}
					$('#ticketReport').attr('action',base_url+'admin/tickets/report/modified/'+pathArray[6]+'/'+pathArray[7]);
				}else{
				$('#ticketReport').attr('action',base_url+'admin/tickets/report/');
				}
			}
			$('#ticketReport').submit();
		});
		
		$("#toggleOpen").click(function(){
				$('#ticket_id').val('');
				if($('#toggleOrder').val()=="Orber by Modified"){
					$('#ticketReport').attr('action',base_url+'admin/tickets/report/deadline');
					$('#ticketReport').submit();
				} 
				$('#ticketReport').submit();
		
			});
	});
	function closefb()
	{
		$.fancybox.close();
		var base_url = '<?php echo BASE_URL; ?>';
		window.location=document.URL;
	}
	function submit_myform(){
		$('#ticket_id').val('');
			$('#ticketReport').attr('action',base_url+'admin/tickets/report');
		$('#ticketReport').submit();
	}
</script>
<!--  start content-table-inner -->


	<?php echo $this->Form->create('Ticket',array('url' => ['action' => 'report'],'id'=>'adminreport','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>







<a id='close-fb' style='visibility:hidden' href="javascript:void(0);" onclick='javascript:closefb();'></a> <!--This Link is used to close iframe on click -->
<div id="content-table-inn">
<div id="ticketheader" class="grey-listleft">
		<div id="ticket_hnumber1"> Ticket No.</div>
		<div id="ticket_htitle1"> Title </div>
		<div id="ticket_replier1" style="color:#FFFFFF"> Last Replier </div>
		<div id="ticket_htype1"> Type </div>
		<div id="ticket_status1" style="font-size:12px;font-weight:bold;color:#FFFFFF"> Status </div>
		<div id="ticket_ip"> IP Address </div>
		<div id="ticket_deadline2"> Deadline </div>
		<div id="ticket_hreply1" > Response </div>
		<div id="ticket_hremind1" > Reminder </div>
		<div id="ticket_hicon1" > Align </div>
		<div style="clear:both"></div>
	</div>
<div id="ticketlist">
	<?php



	 if(!empty($resultData))  { 

				foreach($resultData as $ticketData) { 


				?>
	<?php if($ticketData['status']==0) { ?>
		<div class="green-listleft" >
		<!--div class="green-listleft"-->
	<?php }
	elseif($ticketData['status']==1 && ($ticketData['deadline'] > date("Y-m-d H:i:s")) ) { ?>
		<div class="blue-listleft">
	<?php } else {?>
		<div class="red-listleft">
	<?php } ?>
	<div id="ticket_number_usr"><?php echo $ticketData['Ticket_number'];  ?> </div>
	<div id="ticket_title1">
		<?php $str = strip_tags($ticketData['title']); echo substr($str,0,25).((strlen($str)>25)?"...":""); ?>
				
	</div>
	<div id="ticket_replier2">
	<?php $str = strip_tags(($ticketData['Last_replier']['first_name']." ".$ticketData['Last_replier']['last_name'])); 
		echo substr($str,0,20).((strlen($str)>20)?"...":""); ?>
	</div>
	<div id="ticket_type1">
		<?php echo ucfirst($ticketData['Ticket']['type']);  ?>
	</div>
	<div id="ticket_status11">
					<?php if($ticketData['Ticket']['status']==0){
									$Ticketstatus = "Closed";
								}elseif($ticketData['Ticket']['status']==1){
									$Ticketstatus = "Open";
								}else{
									$Ticketstatus = "<i>In Progress</i>";
								}
					?>
					<?php  echo $Ticketstatus; ?>
	</div>
	<div id="ticket_deadline">
					<?php // echo date("d/m/Y h:i a",strtotime($ticketData['modified']));  ?>
					<?php if ($ticketData ['ipaddress'] != MY_IP) {
						$ip = "<span style='color:red'>". $ticketData ['ipaddress']  . "</span>" ;
						}else {
							$ip = $ticketData['ipaddress'];
						}
					echo $ip;
					?>
	</div>
	<div id="ticket_deadline">
		<?php echo date("d/m/Y h:i a",strtotime($ticketData['deadline']));  ?>	
	</div>
					<div id="ticket_resp2">
					<?php
					//pr($this->request->params); die;
					//pr("asdflkjahksldfhklasdhfklja".$ticketData['id']); die;
					 echo $this->Html->link("",
								array('controller'=>'tickets','action'=>'response','ticket_id' => $ticketData['id'],'act' => $this->request->params["action"]),
								array('class'=>'view_template response-1','alt'=>'response')
							   );  ?>		
					</div>
					<div id="ticket_respond1">
					<?php echo $this->Html->link("",
								array('controller'=>'tickets','action'=>'index','prefix' => 'tickets','ticket_id' => $ticketData['id'] ),
								array('class'=>'reminder-1-ticket info-tooltip','title'=>'reminder',"onclick"=>'javascript:return getConfirmation("Are you sure , You want to send Reminder?");')
							   );  ?>
					</div>

             <div id="ticket_assg11"> 
					<?php echo $ticketData['User_From']['first_name']." ".$ticketData['User_From']['last_name']; ?>
					<span> to </span><br/>
						<?php echo $ticketData['User_To']['first_name']." ".$ticketData['User_To']['last_name'] ?>  
					</div>
					<div style="clear:both;"></div>
			</div>
		<?php } } else{?>
			<br/><br/>
			<div align="center" style='height:100px;font-size:14px;font-weight:bold;'> No Tickets to display </div>
		<?php } ?>
		
	</div>
	
</div>
<?php echo $this->Form->end(); ?>