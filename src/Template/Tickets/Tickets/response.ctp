<link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>js/tpt/angular-datetimepicker/src/css/datetimepicker.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
 <link href="<?php echo BASE_URL; ?>js/tpt/editable/css/xeditable.css"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
  <link href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" id="theme"  rel="stylesheet">
     <div class="row">
        <div class="col-md-12">
          <div class="white-box">
               <?php $url =  $this->Url->build('/', true);?>
            <div class="row" ng-app="createTree" ng-controller="createCtr"  ng-init ="name='ticketresponse';base_url='<?php echo $url;?>';origin='<?php echo $id;?>';redirect='<?php echo $redirect;?>'">
            <div class="custom-preloader" ng-hide="hideloader">
                <div class="cssload-speeding-wheel"></div>
             </div>
            <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="fa fa-thumbs-up"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
            <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" ng-class="fields.errorswrapper"> <i class="ti-hand-stop"></i>&^&errors.message&^&<a href="#" class="closed" ng-click ="closetheerror('errorswrapper')">&times;</a> </div>
            <div class="alert alert-loading myadmin-alert-top alerttop" ng-class="fields.infowrapper"> <span class="succesfont">&^&info.message&^& </span></div>
            
         
              <div class="row responsesection" scrolly>
                <div class="media m-b-30 p-t-20">
                  
                  <h4 class="font-bold m-t-0" ng-mouseover="showicon('title')" ng-mouseleave="hideicon('title')" title="Title">
                      <span editable-text="ticket.title" e-form="textBtnForm"  onbeforesave="validatetitle($data)"  e-ng-disabled="disbaletitleedit">
                      &^& ticket.title &^&
                      </span>  
                      <i class="fa fa-pencil m-r-30" ng-class="show.title" ng-click="!isclickenable || textBtnForm.$show()" ng-hide="disbaletitleedit || textBtnForm.$visible" onbeforesave="validatetitle($data)"></i>
                      <span class="pull-right">
                        <a href="#" ng-click="!ispriorityenabled || togglepriority()"><i class="fa fa-bolt m-l-10 m-r-10" ng-class="{normal:'t-normal', urgent:'t-urgent', 'emergency':'t-emergency', 'medium':'t-medium'}[ticket.priority.k]"  data-toggle="tooltip"  data-placement= "top" title="&^& ticket.priority.v &^&"></i></a>
                        <a href="#" ng-click="setreminder(ticket.id)"><i class="fa fa-bell m-l-10 m-r-10" title="Set Reminder"></i></a>
                        <a href="#desc" class="focussummernote m-l-10 m-r-10" data-toggle="tooltip" data-placement="top" data-original-title="Go To Response Section"><i class="fa fa-reply"></i></a>
                         <?php   echo $this->Html->link('<i class="ti-back-left "></i>',
                          array('controller'=>'tickets','action'=>'index'),
                          ['escape' => false,"class"=>"m-l-10 m-r-10","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Go Back To Tickets List"]
                        );?>
                      </span>
                  </h4> 
                  <hr>

                   <a class="pull-left lrtxt" href="#"> <img class="media-object thumb-sm img-circle" src="&^&  (ticket.User_From.image_url !== '') ? base_url + 'img/employee_image/' + ticket.User_From.image_url :  base_url + 'images/shared/no_image.png' &^& " alt=""> </a>
                  <div class="media-body"> 
                      <span class="media-meta pull-right">
                         <p> &^& ticket.created   | date:'MMM d, y h:mm:ss a':'+0000' &^&</p>
                           <p class="media-meta ipaddrtxt pull-right">
                              &^& ticketip&^&
                          </p>
                      </span>            
                    <h4 class="text-danger m-0">&^& ticket.User_From.first_name &^& &^& ticket.User_From.last_name &^&</h4>
                    <small class="text-muted lrtxt">From: &^& ticket.User_From.username &^&</small> </div>
                    <hr>
                  <div class="row">
                   <div class="" id="profile">
                  <div class="row m-r-10">

                    <div class="col-md-3 col-xs-6 b-r"> <strong>To</strong> <br>
                      <div class="col-md-10">
                        <p class="text-muted" editable-select="ticket.to_id"  e-ng-options="option.first_name for option in users track by option.id" title="Click to Edit" onbeforesave="updateadmin($data)">
                            &^&showadmin()&^&
                        </p>
                      </div>
                    </div>

                    <div class="col-md-3 col-xs-6 b-r"> <strong>Type</strong> <br>
                      <p class="text-muted" editable-select="ticket.type" e-form="typeBtnForm" ng-click="!istypeenable || typeBtnForm.$show()" e-ng-options="option.v for option in type track by option.k" title="Click to Edit" onbeforesave="updatetype($data)">
                      &^&showtype()&^&
                      </p>
                       <p class="text-muted" editable-select="ticket.scheduletype" e-form="scheduletypeBtnForm" ng-click="!isscheduletypeenable || scheduletypeBtnForm.$show()"  e-ng-options="option.v for option in scheduletype track by option.k" title="Click to Edit" onbeforesave="updatescheduletype($data)">
                        &^&ticket.scheduletypeface&^&
                      </p>
                    </div>

                    <div class="col-md-3 col-xs-6 b-r"> <strong>Last Replier</strong> <br>
                      <p class="text-muted lrtxt">&^&ticket.Last_replier.first_name&^& &^&ticket.Last_replier.last_name&^&</p>
                    </div>
                    <div class="col-md-3 col-xs-6"> <strong>Status</strong> <br>
                      <!-- <p class="text-muted">London</p> -->
                      <a class="text-muted" editable-select="ticket.currentstatus" e-form="statusBtnForm" ng-click="!isstatusenable || statusBtnForm.$show()"  e-ng-options="option.v for option in status track by option.k" title="Click to Edit" onbeforesave="updatestatus($data)">
                        &^&showstatus()&^&
                      </a>
                    </div>
                  </div>
                   <hr>
                  <div class="row m-r-10">
                     <div class="col-md-3 col-xs-6 b-r"> <strong>Deadline</strong> <br>
                      <!-- <p class="text-muted">&^&ticket.deadline | date:'medium'&^&</p> -->
                      <a href="#" editable-text="ticket.deadline"  e-formclass="jquerydatetimepicker" onbeforesave="updatedeadline($data)" title="Click to Edit">
                      &^& showdeadline()&^&
                      </a>
                    </div>
                    <div class="col-md-3 col-xs-6 b-r"> <strong>Entity</strong> <br>
                      <p class="text-muted lrtxt">&^&ticket.entity_type.name&^&</p>
                      <p class="text-muted" editable-select="entityvaluemodel" e-form="entityvalueBtnForm" ng-click="!isentityvalueenable || entityvalueBtnForm.$show()"  e-ng-options="option.v for option in entityvalue track by option.k" title="Click to Edit" onbeforesave="updateentityalue($data)">
                        &^&selectedprojectface&^&
                      </p>
                      <!-- <p class="text-muted">&^&selectedprojectface&^&</p> -->
                    </div>
                    <div class="col-md-3 col-xs-6 b-r"> <strong>Billable Hours</strong> <br>
                      <!-- <p class="text-muted">johnathan@admin.com</p> -->
                      <a href="#" editable-text="ticket.billable_hours" onbeforesave="updateba($data)"  title="Click to Edit" e-formclass="jquerytimepicker">
                        &^&showbillablehours()&^&
                        <!-- &^&ticket.billable_hours&^& -->
                      </a>
                    </div>

                    <div class="col-md-3 col-xs-6"> <strong>Task Time</strong> <br>
                       <a href="#" editable-text="ticket.tasktime" onbeforesave="updatetasktime($data)"  title="Click to Edit" e-formclass="jquerytimepicker">
                        &^&ticket.tasktime&^&
                      </a>
                    </div>

                  </div>
                  <div attachedtags></div>
                  <hr>
                  <!-- <p class="m-t-30 white-space-pre" compiledes="ticket.description"></p> -->
                  <p class="m-t-30 white-space-pre" ng-bind-html="ticket.description"></p>
                 
                </div> 
               <div class="allresponses white-space-pre" apipeline ng-repeat="(index, appdata) in responders track by $index" id="a&^&$index&^&">
               </div>
                <div class="b-all p-20">
                   <div class="form-group" id ="desc">
                <?php
                 echo  $this->Form->textarea('description', array("type"=>'text',"class"=>"title form-control", 'id' => '',"label"=>false,"div"=>false,'placeholder' => "",'rows'=>"15",'ng-model' =>"ticket.desc", 'summer') ); 
                ?>
                 <!-- <summernote config="options" ng-model="ticket.desc"></summernote> -->
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.desc"><button type="button" class="close" ng-click ="closetheerror('desc')" aria-hidden="true">&times;</button>&^&errors.desc&^&
                </div>
                </div>
                </div>
                <div class="dynamichtml white-space-pre" tagcontent  ng-show ="hidetextdes" ng-repeat="(index, deshtml) in tichtml track by $index" id="a&^&$index&^&">
                </div>


                </div>
                <center class="m-t-20">
                  <button type="submit" class="btn btn-primary " ng-click="!issubmitenable ||new_app_submit();$event.preventDefault()"><i class="fa fa-envelope-o"></i> Respond</button>
                  <button class="btn btn-default" formaction="<?php echo $url;?>tickets/index"  ng-click="goback()" ><i class="ti-back-left "></i> Back</button>  
                </center>
                
              </div>
              <i class="fa fa-arrow-circle-up gototop" ng-show="scrolltotop" ng-class="make_gtp_vi" aria-hidden="true" ng-click="!shouldgotop || gototop()" title="Go To Top"></i>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
      </div>
     <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
      <?php echo $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/ticket_response','angular/jquery-ui/jquery-ui','angular/aria','angular/upload/angular-upload.min','tpt/angular-fullscreen/src/angular-fullscreen','tpt/ng-file-upload/dist/ng-file-upload-shim','tpt/ng-file-upload/dist/ng-file-upload','angular/ui/src/sortable','tpt/angular-block/dist/angular-block-ui.min','tpt/sweet-alert/SweetAlert','jquery-ui-datepicker','datetimepicker_full','tpt/editable/js/xeditable'));
    ?>
   <script src="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.min.js"></script>
   