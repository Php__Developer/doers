<link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
<!--  <link href="<?php echo BASE_URL; ?>css/ui_boot.css" id="theme"  rel="stylesheet"> -->
<!--  <link href="<?php echo BASE_URL; ?>js/tpt/fullscreen/release/style.css" id="theme"  rel="stylesheet"> -->
<!--  <link href="<?php echo BASE_URL; ?>js/tpt/highlight/styles/github.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>js/tpt/ng-ckeditor/ng-ckeditor.css" id="theme"  rel="stylesheet"> -->
  <!-- <link href="<?php echo BASE_URL; ?>js/editable/css/xeditable.css"  rel="stylesheet"> -->
<!-- <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
  
<!-- <link href="<?php echo BASE_URL; ?>css/aria.css" id="theme"  rel="stylesheet"> -->
 <link href="<?php echo BASE_URL; ?>js/tpt/angular-datetimepicker/src/css/datetimepicker.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
 <link href="<?php echo BASE_URL; ?>js/tpt/editable/css/xeditable.css"  rel="stylesheet">
     <div class="row">
        <!-- Left sidebar -->
        <div class="col-md-12">
          <div class="white-box">
            <!-- row -->
               <?php $url =  $this->Url->build('/', true);?>
            <div class="row" ng-app="createTree" ng-controller="createCtr"  ng-init ="name='ticketresponse';base_url='<?php echo $url;?>';origin='<?php echo $id;?>'">
              <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel">
              <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="ti-user"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
                <div> <!-- <a href="#" class="btn btn-custom btn-block waves-effect waves-light">Add New Ticket</a> -->
                      <?php  $this->Html->link("Add Ticket",
                        array('controller'=>'tickets','action'=>'add2'),
                        array('class'=>'btn btn-custom btn-block waves-effect waves-light','alt'=>'ticket')
                        ); ?>
                  <div class="list-group mail-list m-t-20">
                      <a href="#" class="list-group-item all_tickets" ng-click="get_data('all',1,'none','none','yes')" ng-class="active.all">All <span class="label label-rouded label-warning pull-right" ng-bind="count.all"></span></a> 
                      <a href="#" class="list-group-item opn_tickets" ng-click="get_data('open',0,'none','none','yes')" ng-class="active.open">Open <span class="label label-rouded label-success pull-right" ng-bind="count.opened"></span></a>
                      <a href="#" class="list-group-item clsd_tickets" ng-click="get_data('close',0,'none','none','yes')" ng-class="active.close">Closed <span class="label label-rouded label-danger pull-right" ng-bind="count.closed"></span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('inprogress',0,'none','none','yes')" ng-class="active.inprogress">In Progress<span class="label label-rouded label-info pull-right" ng-bind="count.inprogress"></span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('expire',0,'none','none','yes')" ng-class="active.expire">Expired<span class="label label-rouded label-info pull-right" ng-bind="count.expired"></span></a>
                      <!-- <a href="#" class="list-group-item">Trash <span class="label label-rouded label-default pull-right">55</span></a>  -->
                  </div>
                  <h3 class="panel-title m-t-40 m-b-0">Tags</h3>
                  <hr class="m-t-5">
                  <div class="list-group b-0 mail-list"> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-info m-r-10"></span>Medium</a>
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Urgent</a>
                    <!-- <a href="#" class="list-group-item"><span class="fa fa-circle text-purple m-r-10"></span>Private</a> -->
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Emergency</a> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>Normal</a> 
                 </div>
                </div>
              </div>
           		<div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                <div class="media m-b-30 p-t-20">
                  <h4 class="font-bold m-t-0" ng-mouseover="showicon('title')" ng-mouseleave="hideicon('title')" title="Title">
                    <span editable-text="ticket.title" e-form="textBtnForm" onbeforesave="validatetitle($data)">
                    &^& ticket.title &^&
                    </span>  
                    <i class="fa fa-pencil m-r-30" ng-class="show.title" ng-click="textBtnForm.$show()" ng-hide="textBtnForm.$visible" onbeforesave="validatetitle($data)"></i>
                    <?php   echo $this->Html->link('<i class="fa  fa-mail-reply"></i>',
                    array('controller'=>'tickets','action'=>'index2'),
                    ['escape' => false,"class"=>"pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Go Back To Tickets List"]
                    );?>

                  </h4> 
                  <hr>
                  <div class="row">
                    <div class="col-sm-4">
                        <select name="app" class="form-control" ng-options="option.first_name for option in users track by option.id"  ng-model="ticket.to_id"> 
                        </select>
                    </div>
                    <div class="col-sm-4">
                       <select name="app" class="form-control" ng-options="option.v for option in type track by option.k"  ng-model="ticket.type"> 
                        <!-- <option value="" selected="selected">Select Type</option> -->
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="app" class="form-control" ng-options="option.v for option in priority track by option.k"  ng-model="ticket.priority"> 
                        <!-- <option value="" selected="selected">Select Type</option> -->
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select name="app" class="form-control" ng-options="option.project_name for option in projectid track by option.id"  ng-model="ticket.projectid"> 
                        <!-- <option value="" selected="selected">Select Priority</option> -->
                        </select>
                    </div>
                    <div class="col-sm-4">
                      <div class="dropdown">
                      <a class="dropdown-toggle" id="dropdown2" role="button" data-toggle="dropdown" data-target="#" href="#">
                      <div class="input-group"><input type="text" class="form-control thedeadline" data-ng-model="ticket.deadline | date:'MMM d, y h:mm:ss a':'+0000'" ><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                      </div>
                      </a>
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <datetimepicker data-ng-model="ticket.deadline" data-on-set-time="onTimeSet(newDate, oldDate)" data-datetimepicker-config="{ dropdownSelector: '#dropdown2' }" />
                    
                      </ul>
                      </div>
                 
                     </div>
                     <div class="col-sm-4">
                        <select name="app" class="form-control" ng-options="option.v for option in status track by option.k"  ng-model="ticket.status"> 
                        <!-- <option value="" selected="selected">Select Type</option> -->
                        </select>
                    </div>
                  </div>
                 
                  <div class="b-all p-20">
                    <p>&^&(ticket.from_id==currentuser)? (ticket.response == null) ? 'No Response Yet!': ticket.response : ticket.description&^&</p> 
                  </div>
                
               
                <div class="b-all p-20">
                  <!-- <p class="p-b-20">click here to <a href="#">Reply</a> or <a href="#">Forward</a></p> -->
                   <div class="form-group">
                  <!-- <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea> -->
                <?php
                 echo $this->Form->textarea('description', array("type"=>'text',"class"=>"title form-control", 'id' => 'desc',"label"=>false,"div"=>false,'placeholder' => "",'rows'=>"15",'ng-model' =>"ticket.desc") ); 
                ?>
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.desc"><button type="button" class="close" ng-click ="closetheerror('desc')" aria-hidden="true">&times;</button>
                </div>
                </div>


                </div>
                <button type="submit" class="btn btn-primary " ng-click="new_app_submit();$event.preventDefault()"><i class="fa fa-envelope-o"></i> Send</button>
                <button class="btn btn-default" type="reset"><i class="fa fa-times"></i> Discard</button>
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
 <!--  <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
  <script src="<?php echo BASE_URL; ?>plugins/bower_components/blockUI/jquery.blockUI.js"></script>
  <script src="<?php echo BASE_URL; ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
  <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script> -->
       <?php 
    echo  $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/ticket_response','angular/jquery-ui/jquery-ui','angular/aria','tpt/angular-ckeditor/angular-ckeditor','angular/upload/angular-upload.min','tpt/angular-fullscreen/src/angular-fullscreen','tpt/ng-file-upload/dist/ng-file-upload-shim','tpt/ng-file-upload/dist/ng-file-upload','angular/ui/src/sortable','tpt/angular-block/dist/angular-block-ui.min','tpt/sweet-alert/SweetAlert','tpt/editable/js/xeditable','tic_index','tpt/angular-datetimepicker/src/js/moment','tpt/angular-datetimepicker/src/js/datetimepicker','tpt/angular-datetimepicker/src/js/datetimepicker.templates','tpt/editable/js/xeditable'));
    ?>
   
      <!-- <script src="<?php echo BASE_URL; ?>"></script> -->
      <!-- /.row -->