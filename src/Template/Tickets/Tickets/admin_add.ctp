<?php  echo $html->css(array('jquery_ui_datepicker','popup.css')); ?>
<?php echo $javascript->link(array('jquery-1.4.1.min','jquery-ui.min','jquery-ui-datepicker','jquery-ui-timepicker-addon')); ?>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(function() {
	//var data ='<?php echo date('F d, Y');?>';
	$( "#deadline" ).datetimepicker({
			  ampm: true,
			  dateFormat: 'MM dd, yy',
			  minDate: 0,
		 });
	//	hourminformat();
	});
	/** Added by Neelam **/
	function hourminformat(time,errorID,msg){
			if(time!= ""){
				if(!time.match(/^([0-9]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/)){
						$('#'+errorID).html(msg);
						$('#'+errorID).attr('style','display:block;color:red;font-size:13px;');
						setTimeout(function() { $('#'+errorID).fadeOut('slow'); }, 2000);
						$('#'+time).val('');
						return true;
				}	
			}
	}
	// not null validation function
	function notEmpty(val,errorID,msg){
		if(val == ""){
			$('#'+errorID).html(msg);
			$('#'+errorID).attr('style','display:block;color:red;font-size:13px;');
			setTimeout(function() { $('#'+errorID).fadeOut('slow'); }, 5000);
			return true;
		}
	}
	function post_data()
	{
		var to_id = $('#toID').val();
		if(notEmpty(to_id,'error_to','Select User.'))
		{ return false; }
		var title = $('.title').val();
		if(notEmpty(title,'error_title','Enter title.'))
		{ return false; }
		var projectid = $('#project').val();
		var tasktime = $('#tasktime').val();
		if(hourminformat(tasktime,'error_time','Enter valid time.'))
		{ return false; }
		var billable_hours = $('#billable_hours').val();
		if(hourminformat(billable_hours,'error_bill','Enter valid time.'))
		{ return false; }
		var desc = $('#desc').val();
		if(notEmpty(desc,'error_desc','Enter description.'))
		{ return false; }
		var deadline = $('#deadline').val();
		if(notEmpty(deadline,'error_dead','Choose deadline.'))
		{ return false; }
		var type = $('#type').val();
		var priority = $('#priority').val();
                $('#postData').hide();
		if($.active>0){
		}else{
			href = base_url+'admin/tickets/add';
			$.post(href,{'to_id':to_id,'title':title,'desc':desc,'deadline':deadline,'type':type,'priority':priority,'tasktime':tasktime,'projectid':projectid,'billable_hours':billable_hours},function(data){
			if(data==1)
			{
                            //alert("hihihihiihihii");
                            var vaccume="";
                                $('.title').val(vaccume);
                                $('#desc').val(vaccume);
                                $('#postData').show();
				window.parent.$('#close-fb').click();
                               
			}
			});
		}
	} 
	$(window).ready(function(){
	$('#ui-datepicker-div').attr('style','display:none');
	});
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner" align='center' style="padding:10px;font-family:arial;">
	<h3>Add Ticket</h3>
	<?php echo $form->create('Ticket',array('action'=>'add','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
			<!-- start id-form -->
			<div id="error_msg" style="display:none;"></div>
			<table border="0" cellpadding="0" cellspacing="7"  id="id-form" width="60%">
					<tr style="padding-top:5px;">
						<td valign="top"><b>To:<b></td>
						<td><?php
								$options=$general->getuser(); 
							
								echo $form->input('to_id', array("type"=>'select',"class"=>"ourselect",'id'=>'toID','options' => $options,'empty' => '--Select--',"selected"=>"$current_user","label"=>false,"div"=>false)); 
							?>
							<div id="error_to" style="display:none;"></div>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Title:</b></td>
						<td><?php
								echo $form->input('title', array("type"=>'text',"class"=>"title","label"=>false,'size'=>'27',"div"=>false)); 
							?>
							<div id="error_title" style="display:none;"></div>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Type:</b></td>
						<td><?php
							    $options=array('task'    =>  'Task', 
												'issue'    => 'Issue',
												'bug'    => 'Bug',
												'lead'    => 'Lead',
												'request' => 'Request',
								  );
						echo $form->input("type",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"type","label"=>false,"div"=>false));    
						?></td>
					</tr>
				
					<tr>
						<td valign="top"><b>Priority:</b></td>
						<td><?php
							    $options=array('normal'    =>  'Normal : Should be replied within 24 Hours', 
														'medium'    => 'Medium : Should be replied within 12 Hours',
														'urgent'    => 'Urgent : Should be replied within 02 Hours',
														'emergency' => 'Emergency : Should be replied within 01 Hour',
								  );
						echo $form->input("priority",array('type'=>'select','options'=>$options,"class"=>"ourselect","id"=>"priority","label"=>false,"div"=>false));    
						?></td>
					</tr>
					<tr>
						<td valign="top"><b>Deadline:</b></td>
						<td><?php
								echo $form->input('deadline', array("type"=>'text',"id"=>"deadline","label"=>false,"div"=>false,'readonly'=>true,'size'=>'27')); 
							?>
							<div id="error_dead" style="display:none;"></div>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Project:</b></td>
						<td>
							<?php
								$session_info = $this->Session->read("SESSION_ADMIN");
								$userID = $session_info[0];
								$options = $general->getProjectsofuser($userID);
								echo $form->input('project',array("type"=>"select","id"=>"project","class"=>"ourselect","options"=>$options,"div"=>false,"label"=>false)); 
							?>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Time:</b></td>
						<td>
							<?php echo $form->input('tasktime',array("type"=>"text","id"=>"tasktime","div"=>false,"label"=>false,"size"=>'27',"value"=>"1:00")); ?>
							<span style="font-size:12px;color:red;"> hh:mm </span>
						<div id="error_time" style="display:none;"></div>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Billable Hours:</b></td>
						<td><?php
							echo $form->input('billable_hours',array("type"=>"text","id"=>"billable_hours","div"=>false,"label"=>false,"size"=>'27',"value"=>"00:00")); ?>
							<span style="font-size:12px;color:red;"> hh:mm </span>
						<div id="error_bill" style="display:none;"></div>
						</td>
					</tr>
					<tr>
						<td valign="top"><b>Description:</b></td>
						<td><?php
								echo $form->textarea('description', array("type"=>'text',"id"=>"desc","label"=>false,"div"=>false,'cols'=>'40','rows'=>'4')); 
							?>
							<div id="error_desc" style="display:none;"></div>
						</td>
					</tr>
					
				
					<!--tr>
						<td valign="top"><b>Response:</b></td>
						<td><?php
								echo $form->textarea('response', array("type"=>'text',"id"=>"response","label"=>false,"div"=>false,'cols'=>'40','rows'=>'4')); 
							?>
						</td>
					</tr-->
					
					<!--tr>
						<td valign="top"><b>Status:</b></td>
						<td><?php
								$options = array(
									'1'=>'Open',
									'2'=>'In Progress',
									'0'=>'Closed'
								);
								echo $form->input('status', array("type"=>'select',"class"=>"ourselect","id"=>"status","options"=>$options,"label"=>false,"div"=>false)); 
							?>
						</td>
					</tr-->
					
					<tr>
						<td>
							&nbsp;
						</td>
						<td><?php echo $form->input('Save',array('type'=>'button','label'=>false,'class'=>'form-submit','id'=>'postData','div'=>false,'onclick'=>'javascript:post_data();')); ?></td>
					</tr>
			</table>
	</div>