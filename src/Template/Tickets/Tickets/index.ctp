<link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>js/tpt/highlight/styles/github.css" id="theme"  rel="stylesheet">
  <link href="<?php echo BASE_URL; ?>js/tpt/editable/css/xeditable.css"  rel="stylesheet">
  <link href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" id="theme"  rel="stylesheet">

<link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />

  <?php $url =  $this->Url->build('/', true);
        echo $this->Html->css(array('jquery_ui_datepicker','popup.css'));?>
     <div class="row" id="popupContainer"  ng-app="createTree" ng-controller="createCtr as ctrl" ng-init ="name='tickindex';base_url='<?php echo $url;?>'; currentuser='<?php echo $session_info[0] ;?>'" block-ui="main" class="block-ui-main">
     <div class="custom-preloader" ng-hide="hideloader">
          <div class="cssload-speeding-wheel"></div>
      </div>
		        <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="fa fa-thumbs-up"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
            <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" ng-class="fields.errorswrapper"> <i class="ti-hand-stop"></i>&^&errors.message&^&<a href="#" class="closed" ng-click ="closetheerror('errorswrapper')">&times;</a> </div>
            <div class="alert alert-loading myadmin-alert-top alerttop" ng-class="fields.infowrapper"> <span class="succesfont">&^&info.message&^& </span></div>
        <!-- Left sidebar -->
        <div class="col-md-12">
          <div class="white-box">
            <!-- row -->
            <div class="row">
              <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel" >
                <div> 
                     <!--  <button class="btn btn-custom btn-block waves-effect waves-light" ng-click="addticketpopup()">Add Ticket</button> -->
                      <?php echo $this->Html->link("Add Ticket",
                        array('controller'=>'tickets','action'=>'add'),
                        array('class'=>'btn btn-custom btn-block waves-effect waves-light','alt'=>'ticket')
                        ); ?>
                  <div class="list-group mail-list m-t-20">
                      <a href="#" class="list-group-item opn_tickets" ng-click="get_data('open',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.open">Open <span class="label label-rouded label-open pull-right" ng-bind="count.opened">0</span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('inprogress',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.inprogress">In Progress<span class="label label-inprogress label-info pull-right" ng-bind="count.inprogress">0</span></a>

                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('scheduled',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.scheduled">Scheduled<span class="label label-scheduled label-info pull-right" ng-bind="count.scheduled">0</span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('mandatory',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.mandatory">Mandatory<span class="label label-mandatory label-info pull-right" ng-bind="count.mandatory">0</span></a>

                      <a href="#" class="list-group-item clsd_tickets" ng-click="get_data('close',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.close">Closed <span class="label label-rouded label-closed pull-right" ng-bind="count.closed">0</span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('expire',0,'none','none','yes','none',0,'none','none','selected')" ng-class="active.expire">Expired<span class="label label-danger label-info pull-right" ng-bind="count.expired">0</span></a>
                      <a href="#" class="list-group-item all_tickets" ng-click="get_data('all',1,'none','none','yes','none',0,'none','none','selected')" ng-class="active.all">All <span class="label label-rouded label-default pull-right" ng-bind="count.all">0</span></a> 
                      <!-- <a href="#" class="list-group-item">Trash <span class="label label-rouded label-default pull-right">55</span></a>  -->
                  </div>
                  <h3 class="panel-title m-t-40 m-b-0">Tags</h3>
                  <hr class="m-t-5">
                  <div class="list-group b-0 mail-list"> 
                    <a href="#" class="list-group-item" ng-repeat="(index, x) in tagsarr track by $index" ng-click="getdatabytag(x.Value)"><span ng-class="{1:'text-success', 2:'text-info', 3:'text-danger', 4:'text-warning'}[x.Value]" class="fa fa-circle m-r-10"></span>&^&x.label&^&</a>
                   <!--  <a href="#" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Urgent</a>
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Emergency</a> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>Normal</a>  -->
                 </div>
                </div>
              </div>
              <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                <div class="inbox-center">
                
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <!-- <th width="30" ><div class="checkbox m-t-0 m-b-0 ">
                          </div></th> -->
                        <th colspan="2" class="one_th"> <div class="btn-group ">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light m-r-5" data-toggle="dropdown" aria-expanded="false"> Sort <b class="caret"></b> </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#fakelink" ng-click="get_data('selected','selected','created','none','selected','selected','selected','selected','selected','selected')" >Created Date</a></li>
                              <li><a href="#fakelink" ng-click="get_data('selected','selected','modified','none','selected','selected','selected','selected','selected','selected')" >Modified Date</a></li>
                              <li><a href="#fakelink" ng-click="get_data('selected','selected','deadline','none','selected','selected','selected','selected','selected','selected')">Deadline</a></li>
                             <!--  <li class="tic_id">
                                    <a href="#" editable-text="user.name" onbeforesave="updateUser($data)">
                                      &^& ticket.id || 'TicketID' &^&
                                    </a>
                              </li> -->
                             <!--  <li class="divider"></li> -->
                             <!--  <li><a href="#fakelink">Separated link</a></li> -->
                            </ul>
                          </div>
                          <!-- <div class="btn-group">
                            <button type="button" class="btn btn-default waves-effect waves-light  dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ng-click="get_data('selected','selected','selected','none','selected','selected','selected')"> <i class="fa fa-refresh"></i> </button>
                          </div> --></th>
                                <th colspan="5" class="p-t-0 p-b-0">
                               <!--  <div class="col-sm-3">
                                  <select class="form-control">
                                    <option value="">Male</option>
                                    <option value="">Female</option>
                                  </select>
                                  <span class="help-block"> Select your gender. </span> 
                                 </div>
                                  -->
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 m-t-20 m-r-10">
                                    <select class="m-r-10 bs-select-hidden" data-style="btn-primary btn-outline" title="Search By">
                                      <option data-tokens="to_name">Ticket To</option>
                                      <option data-tokens="from_name">Ticket from_name</option>
                                      <option data-tokens="title">Ticket Title</option>
                                    </select>
                                    <div class="btn-group bootstrap-select p-r-20 ticket_to">
                                      <button type="button" class="btn dropdown-toggle btn-primary btn-outline" data-toggle="dropdown" title="&^&selectedsearch.v&^&" aria-expanded="false"><span class="filter-option pull-left">&^&selectedsearch.v&^&</span>&nbsp;<span class="caret"></span></button>
                                      <div class="dropdown-menu open" style="max-height: 138.6px; overflow: hidden; min-height: 0px;" title="Search By">
                                        <ul class="dropdown-menu inner" role="menu" style="max-height: 123.6px; overflow-y: auto; min-height: 0px;">
                                        <li ng-click="searchselectchng(x)" ng-repeat="(index, x) in searchdata track by $index"  data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="ketchup mustard"><span class="text">&^&x.v&^&</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>
                                        
                                        </ul></div>
                                    </div>
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 m-t-20" ng-class="altersearchboxclass" ng-hide="hidetextinput">
                                     <div class="form-group">
                                          <input class="form-control srchbox" id="exampleInputuname" placeholder="Enter Search Keyword" type="text" ng-model="search.searchval">
                                      </div>  
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 m-t-20" ng-show="showdateinput">
                                    <div class="form-group">
                                        <div class="input-group">
                                         <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                          <input class="form-control srchbox" id="date_timepicker_start" placeholder="Start Date" type="text" ng-model="search.startdate" jquerydatepicker>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 m-t-20" ng-show="showdateinput">
                                    <div class="form-group">
                                        <div class="input-group">
                                         <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                          <input class="form-control srchbox" id="date_timepicker_end" placeholder="End Date" type="text" ng-model="search.enddate" jquerydatepicker> 
                                        </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 m-t-20" ng-class="altersearchboxclass" ng-show="showpriority">
                                     <div class="form-group">
                                         <select name="app" class="form-control" ng-options="option.v for option in searchdd track by option.k"  ng-model="search.priority"> 
                                         </select>
                                      </div>  
                                  </div>
                                <?php
                                echo $this->Form->button("Search", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon srchbox m-t-20','id'=>'search','ng-click'=>"searchresults()"))."&nbsp;&nbsp;&nbsp;";
                                echo $this->Form->button("Reset", array('class'=>'fcbtn btn btn-success btn-outline btn-1b searchfaicon srchbox m-t-20','id'=>'search','ng-click'=>"get_data('selected','selected','none','none','selected','selected','selected','none','none','none')"))."&nbsp;&nbsp;&nbsp;";
                                  ?>


                         </th>
                         <th class="filter_img">
                           <!-- <span class="m-b-10 prioritysorticons">
                             <a href="#" ng-click="get_data('selected','selected','normal','none','selected','selected','selected','selected','selected')" class="m-r-5 m-l-5">
                                <i class="fa fa-bolt t-normal" data-toggle="tooltip"  data-placement= "top" title="Filter By priority(Normal)"></i>
                              </a>
                              <a href="#" ng-click="get_data('selected','selected','medium','none','selected','selected','selected','selected','selected')" class="m-r-5 m-l-5">
                                <i class="fa fa-bolt 't-medium" data-toggle="tooltip"  data-placement= "top" title="Filter By priority(Medium)"></i>
                              </a>
                              <a href="#" ng-click="get_data('selected','selected','urgent','none','selected','selected','selected','selected','selected')" class="m-r-5 m-l-5">
                                <i class="fa fa-bolt t-urgent" data-toggle="tooltip"  data-placement= "top" title="Filter By priority(Urgent)"></i>
                              </a>
                              <a href="#" ng-click="get_data('selected','selected','emergency','none','selected','selected','selected','selected','selected')" class="m-r-5 m-l-5">
                                <i class="fa fa-bolt t-emergency" data-toggle="tooltip"  data-placement= "top" title="Filter By priority(Emergency)"></i>
                              </a>
                              

                           </span> -->
                         </th>
                        <th class="hidden-xs two_th" width="100" colspan="2"><div class="btn-group pull-right">
                            <button type="button" class="btn btn-default waves-effect" ng-class="prebtnclass" ng-click ="!tiggerprev || get_data('selected','selected','selected','none','no','pre','selected','selected','selected','selected')"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default waves-effect" ng-class="nxtbtnclass" ng-click ="!tiggernxt || get_data('selected','selected','selected','none','no','nxt','selected','selected','selected','selected')"><i class="fa fa-chevron-right"></i></button>
                          </div></th>
                      </tr>
                    </thead>
                    <tbody  ng-model="t_data" class="alltickets">
                       <tr class="" ng-repeat="(index, x) in t_data track by $index" ng-class="{true:'',false:'unread'}[x.Last_replier.id==userID]" >
                            <td>
                                <i ng-class="{true:'ti-angle-double-up',false:'ti-angle-double-down'}[x.from_id==userID]" data-toggle="tooltip"  data-placement= "top" title="&^& x.idp &^&"></i>
                            </td>
                           <td class="hidden-xs" title="Click To Edit">
                                <a href="<?php echo $url;?>tickets/response/&^&x.id&^&"><i class="fa fa-pencil" title="Edit"></i></a>
                           </td>
                           <td class="hidden-xs" ng-bind="x.User_To.first_name" title="To: &^& x.User_To.first_name &^&"></td>
                           <td class="max-texts"> 
                          </td>
                          <td class="hidden-xs" data-toggle="tooltip" data-placement="top">
                              <a href="#" />
                                 <span class="label m-r-10" title="Normal" ng-class="{'To-Do':'label-inprogress', 'Scheduled':'label-info', 'Mandatory':'label-mandatory label-info'}[x.type]" ng-bind="x.type" ></span>
                              <span ng-class="{'1':'font-open', '2':'font-inprogess', '0' :'font-close'}[x.status]" ng-click="showAlert($event,$index)" > &^& x.title | limitTo:25 &^& </span>
                              <!-- <a class="mytooltip" href="javascript:void(0)"> 
                                <span ng-class="{'1':'font-open', '2':'font-inprogess', '0' :'font-close'}[x.status]"> &^& x.title | limitTo:25 &^& </span>
                                <span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">&^& x.title &^& <br> &^& x.description &^&</span></span></span>
                              </a> -->
                          </td>
                           <td class="hidden-xs" ng-bind="x.User_From.first_name " title="Last Replied By : &^& x.Last_replier.first_name &^&"></td>
                          <td class="hidden-xs">
                              <!-- <i class="fa fa-bell" title="Set Reminder" ng-click="setreminder(x.id)"></i> -->
                              <a href="#" ng-click="!ispriorityenabled || togglepriority($index)">
                                <i class="fa fa-bolt" ng-class="{normal:'t-normal', urgent:'t-urgent', 'emergency':'t-emergency', 'medium':'t-medium'}[x.priority]"  data-toggle="tooltip"  data-placement= "top" title="&^& x.priority &^&"></i>
                              </a>
                              

                          </td>
                          <td class="text-right"  title="Deadline" colspan="2">
                          	<span ng-class="{true:'font-expired',false:'none'}[x.isexpired=='yes']" uib-tooltip-html="'Created On :<br>'+ x.created+'<br>'+'Modified On :<br>'+ x.modified+'<br>'+'Deadline Till :<br>'+ x.deadline | date:'medium':'+0000' +'<br>'"  tooltip-placement="top"> &^& x.created  | date:'medium':'+0000' &^&</span>

                          </td>
                       </tr>
                    </tbody>
                  </table>
                </div>
                <div class="row default_hidden" ng-class="norecordsmsg">
                  <div class="col-md-12">
                    <div class="white-box">
                      <h3>Result</h3>
                      No Records Found! 
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-7 m-t-20">   &^& ((totalrecords >  0 ) ? 'Showing '+ currentrecodsfrom + ' - ' + currentrecodstill + ' of ' + totalrecords : "" )&^&  </div>
                  <div class="col-xs-5 m-t-20">
                    <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default waves-effect" ng-class="prebtnclass" ng-click ="!tiggerprev || get_data('selected','selected','selected','none','no','pre','selected','selected','selected','selected')"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default waves-effect" ng-class="nxtbtnclass" ng-click ="!tiggernxt || get_data('selected','selected','selected','none','no','nxt','selected','selected','selected','selected')"><i class="fa fa-chevron-right"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
             
      </div>
      <style type="text/css">
        .footer{
          bottom: unset;
        }
      </style>
     <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
     <!-- <script src="<?php echo BASE_URL; ?>tpt/sweet-alert2/dist/sweetalert2.min.js"></script> -->
        <?php 
      echo  $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/create','angular/jquery-ui/jquery-ui','angular/aria','tpt/angular-ckeditor/angular-ckeditor','angular/upload/angular-upload.min','tpt/angular-fullscreen/src/angular-fullscreen','tpt/ng-file-upload/dist/ng-file-upload-shim','tpt/ng-file-upload/dist/ng-file-upload','angular/ui/src/sortable','tpt/angular-block/dist/angular-block-ui.min','tpt/sweet-alert/SweetAlert','tpt/angular-datetimepicker/src/js/moment','tpt/angular-datetimepicker/src/js/datetimepicker','tpt/angular-datetimepicker/src/js/datetimepicker.templates','jquery-ui.min','jquery-ui-datepicker','datetimepicker_full'));
    ?>
    <script src="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js"></script>  
     
     
     
      