<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){ 
		$('.tablesorter').tablesorter({
				
		$(".viewPaydetails").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	650, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
</script>
<!--  start content-table-inner -->
	<div class="white-box">
	 <table class="tablesaw table-bordered table-hover table tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="stack" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
              <thead>
                <tr>
                <?php $lastmonth1 = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));   
						  $lastMonth = $this->General->getFirstLastDates(date("m",$lastmonth1),date('Y')) ; 
						  //pr($lastMonth);
						  $curMonth = $this->General->getFirstLastDates(date('m'),date('Y')) ; //pr($curMonth); die;?>
   <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Sno</button></th>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Employee Name</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">No. of Open Ticket</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">No. of Expired Ticket</button></th>

  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Last Deadline</button></th>

  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Free from</button></th>

  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">ROI <?php echo date("F",$lastmonth1); ?></button></th>

  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">ROI <?php echo date("F"); ?></button></th>

                                </tr>
              </thead>
              <tbody>
			<?php  $sl=1;
				  foreach($resultData as $userData):  $class="";
//pr($resultData);
				    ?>
				<tr class="<?php echo $class; ?>">
				         <td><?php echo $sl; ?></td>
					<td><?php echo $userData ['first_name']." ".$userData ['last_name']; ?> </td>
					<!--Count total open tickets and expired tickets per user -->
					<?php 

 //$openTicket = 0 ; $openExpired = 0;  foreach ($resultTicket as $ticket) {
//pr($ticket);

					//		if(($ticket['Tickets']['to_id']==$userData['User//s']['id'] ) && ($ticket['Tickets']['deadline'] < //date("Y-m-d H:i:s"))){
						//		$openExpired++;
								//pr($openExpired);
						//	}else if($ticket['Tickets']['to_id']==$userData[//'Users']['id']){
							//	$openTicket++;
						//	}
					//	}
					

					$openTicket = 0 ; $openExpired = 0;  foreach ($resultTicket as $ticket) {
//pr($ticket);

							if(($ticket['to_id']==$userData['id'] ) && ($ticket['deadline'] < date("Y-m-d H:i:s"))){
								$openExpired++;
								//pr($openExpired);
							}else if($ticket['to_id']==$userData['id']){
								$openTicket++;
							}
						}
					?>
<?php //echo $openTicket+$openExpired; ?> 
					<td><?php echo $openTicket+$openExpired; ?> </td>
				    <td><?php  echo  $openExpired ; ?></td>
					<!--arranging all ticket deadlines in an array per user -->
					<?php $max_deadline = array(); 
					foreach($resultTicket as $key=>$deadline)  {
						if($deadline['to_id']==$userData['id']){
						       $max_deadline[$key]= $deadline['deadline'] ;
							 }	
					} 
					?>
					<td> 
					<?php !empty($max_deadline) ? $max =max($max_deadline): $max="";   // get max deadline(last deadline) for tickets
						if(!empty($max)) { 
							echo date("d/m/Y h:i a",strtotime($max)); 
						} else { 
							echo ""; 
						} 
					?>
					</td>
					<td>
					<?php 
						$today_date_only = date("Y-m-d");
						echo ($userData['freefrom']>$today_date_only) ? date("d/m/Y",strtotime($userData['freefrom'])) :  date("d/m/Y");
					?>
					</td>
					<!--Calculate specified months ROI for a responsible user -->
					<?php  $preMonthAmount = array(); $currentMonthAmount = array();
					foreach ($paymentData as $k=>$v) { 
							if(($v['Payments']['responsible_id']==$userData['Users']['id'])){
							//	echo "1st--".$lastMonth['firstdate']; echo "last--".$lastMonth['lastdate']; 
								if(($v['Payments']['payment_date'] >= $lastMonth['firstdate']) && ($v['Payments']['payment_date'] <= $lastMonth['lastdate'])){
									//echo "in<br/>";
								$preMonthAmount[$k] = ($v['Payments']['currency']=='inr') ?  $v['Payments']['amount']/50 : $v['Payments']['amount'];
									//echo "<br/>";
								}elseif(($v['Payments']['payment_date'] >= $curMonth['firstdate']) && ($v['Payments']['payment_date'] <= $curMonth['lastdate'])){
									$currentMonthAmount[$k] = ($v['Payments']['currency']=='inr') ?  $v['Payments']['amount']/50 : $v['Payments']['amount'];
									
								}
							}
						}
						//pr($preMonthAmount); pr($currentMonthAmount);
					?>
					<td><?php $amount1 = (array_sum($preMonthAmount)/10) ; 
						if($amount1 < 120) {
						$styleColor = "lessAmount"; } else{
						$styleColor = '';
						} 
						echo $this->Html->link($amount1,
								array('controller'=>'payments','action'=>'admin_paymentdetails','prefix' => 'payments','id' => $userData['id'],'last'=>'last'),
								array('class'=>'info-tooltip viewPaydetails '.$styleColor,'title'=>'Edit','target'=>'_blank')
							);
					?></td>
					<td><?php $amount2 = (array_sum($currentMonthAmount)/10) ; 
						if($amount2 < 120) {
						$styleColor = "lessAmount"; } else{
						$styleColor = '';
						} 
						echo $this->Html->link($amount2,
								array('controller'=>'payments','action'=>'admin_paymentdetails','prefix' => 'payments','id' => $userData['id'],'cur'=>'cur'),
								array('class'=>'info-tooltip viewPaydetails '.$styleColor,'title'=>'Edit','target'=>'_blank')
							);
					?></td>
				</tr>
				<?php
				  $sl++;
				 endforeach;    ?>
				
					      </tbody>
    
		   </table>
			
			
			<!--  start paging..................................................... -->

			<!--  end paging................ -->
			
		</div>
		<!-- /#wrapper -->
<!-- jQuery -->
<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery peity -->
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<!--Style Switcher -->
