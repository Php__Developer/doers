 
 <link href="<?php echo BASE_URL; ?>js/tpt/angular-datetimepicker/src/css/datetimepicker.css" id="theme"  rel="stylesheet">
  <link href="<?php echo BASE_URL; ?>js/datetimepicker/jquery.datetimepicker.css" id="theme"  rel="stylesheet">
 <link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <!-- <link href="<?php echo BASE_URL; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
 <link href="<?php echo BASE_URL; ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

<!-- animation CSS -->
<?php $url =  $this->Url->build('/', true);?>
<?php echo $this->Html->script(array('jquery-3')); ?>
    
     
     <div class="row">
      <input type="hidden" name="" class="emporigin" value="<?php echo $current_user;?>">
        <div class="col-md-12">
          <div class="white-box">
          <?php $url =  $this->Url->build('/', true);?>
            <div class="row" ng-app="createTree" ng-controller="createCtr" ng-init ="name='ticket';base_url='<?php echo $url;?>';currentuser='<?php echo $current_user;?>'; is_notified = '<?php echo $is_notified ;?>'; currentuseridenc='<?php echo $this->General->ENC($current_user);?>';tag='<?php echo $tag;?>'; project_id='<?php echo $project_id;?>';redirect='<?php echo $redirect;?>';selectedentity_type='<?php echo $selectedentity_type;?>'; selectedentity_id='<?php echo $selectedentity_id;?>'; selectedtask_type='<?php echo $selectedtask_type;?>'; ">
             <div class="custom-preloader" ng-hide="hideloader">
                <div class="cssload-speeding-wheel"></div>
             </div>
             <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success1 myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="fa fa-thumbs-up"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
            <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" ng-class="fields.errorswrapper"> <i class="ti-hand-stop"></i>&^&errors.message&^&<a href="#" class="closed" ng-click ="closetheerror('errorswrapper')">&times;</a> </div>
            <div class="alert alert-loading myadmin-alert-top alerttop" ng-class="fields.infowrapper"> <span class="succesfont">&^&info.message&^& </span></div>
            
            <!-- <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel"> -->
          <!--  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="fa fa-thumbs-up"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
           <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" ng-class="fields.errorswrapper"> <i class="ti-hand-stop"></i>&^&errors.message&^&<a href="#" class="closed" ng-click ="closetheerror('errorswrapper')">&times;</a> </div>
         -->
            <!--   <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing"> -->
            <div class="row">
            
            <!-- <div class="col-md-12"><a href="#" ng-click="cancel('no_delete')" class="pull-right" data-toggle="tooltip" data-placement="top" data-original-title="Go Back To Tickets List" > <i class="fa  fa-times"></i></a></div> -->
                <h3 class="box-title text-center"><span class="text-center">Add New Ticket</span>
                   <?php    $this->Html->link('<i class="fa fa-arrow-left"></i>',
                    array('controller'=>'tickets','action'=>'index'),
                    ['escape' => false,"class"=>"pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Go Back To Tickets List" ]
                    );?>
                </h3>
                <?php echo $this->Form->create('Ticket',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"",'ng-submit' => "new_app_submit()")); ?>
                <!-- Left Section Start -->
                     <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                           <div class="input-group" title="To:">
                              <div class="input-group-addon"><i class="fa fa-user"></i></div>
                              <select name="app" class="form-control" ng-options="option.first_name for option in users track by option.id"  ng-model="app.to_id"></select>
                                <div class="error_to"></div>
                                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="to_id"><button type="button" class="close" ng-click ="closetheerror('to_id')" aria-hidden="true">&times;</button>&^&errors.to_id&^&</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" title="Type">
                                  <div class="input-group-addon"><i class="fa fa-bars"></i></div>
                                <select name="app" class="form-control" ng-options="option.v for option in type track by option.k"  ng-model="app.type" ng-change="checktype()"> 
                                </select>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.priority"><button type="button" class="close" ng-click ="closetheerror('priority')" aria-hidden="true">&times;</button>&^&errors.priority&^&
                            </div>

                        </div>
                        <div class="row" ng-show="showshceduled">
                          <div class="col-md-12">
                            <div class="form-group" >
                            <div class="input-group" title="Select Time">
                                  <div class="input-group-addon"><i class=" ti-reload"></i></div>
                                <select name="app" class="form-control" ng-options="option.v for option in scheduletype track by option.k"  ng-model="app.scheduletype"> 
                                </select>
                            </div>
                            </div>
                        </div>
                          <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.scheduletype"><button type="button" class="close" ng-click ="closetheerror('scheduletype')" aria-hidden="true">&times;</button>&^&errors.scheduletype&^&
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group" title="Priority">
                                  <div class="input-group-addon"><i class="fa fa-list-alt"></i></div>
                                <select name="app" class="form-control" ng-options="option.v for option in priority track by option.k"  ng-model="app.priority"> 
                                </select>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.priority"><button type="button" class="close" ng-click ="closetheerror('priority')" aria-hidden="true">&times;</button>&^&errors.priority&^&
                                </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group" title="Deadline">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <?php echo $this->Form->input('deadline', array("type"=>'text',"class"=>"title form-control","label"=>false,"div"=>false,'ng-model' =>'app.deadline','jquerydatetimepicker')); ?>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.deadline"><button type="button" class="close" ng-click ="closetheerror('deadline')" aria-hidden="true">&times;</button>&^&errors.deadline&^&
                            </div>
                        </div>


                        <div class="form-group">
                             <div class="input-group" title="Entity">
                              <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                            <select name="app" class="form-control" ng-options="option.name for option in projectid track by option.id"  ng-model="app.projectid"  ng-change = "entitychange()"> 
                            </select>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.projectid"><button type="button" class="close" ng-click ="closetheerror('projectid')" aria-hidden="true">&times;</button>&^&errors.projectid&^&
                            </div>
                        </div>
                        
                        <div class="form-group default_hidden" ng-class="entity.showhide">
                             <div class="input-group" title="&^&entity.title&^&">
                              <span class="input-group-addon"><i class="fa fa-anchor"></i></span>
                            <select name="app" class="form-control" ng-options="option.name for option in entity.value track by option.id"  ng-model="app.entity_value"> 
                            </select>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.projectid"><button type="button" class="close" ng-click ="closetheerror('projectid')" aria-hidden="true">&times;</button>&^&errors.projectid&^&
                            </div>
                        </div> 

                         <div class="form-group tagsformgroup" ng-class="tag.showhide">
                          <div class="input-group inpwrapper" title="&^&entity.title&^&">
                              <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                            
                            <?php echo $this->Form->input("tags",array("type"=>"text","class"=>"leadtag51nput other_aliases agency form-control",'placeholder'=> "Please enter tag", "label"=>false,"div"=>false ,'ng-model' => 'tag.id','tagsinput'));?>
                            
                          </div>
                          <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.tags"><button type="button" class="close" ng-click ="closetheerror('tags')" aria-hidden="true">&times;</button>&^&errors.projectid&^&
                          </div>
                           
                        </div> 

                        <div class="form-group">
                            <div class="input-group" title="Task Time">
                                  <span class="input-group-addon"><i class="fa fa-clock-o "></i></span>
                                <?php echo $this->Form->input('tasktime', array("type"=>'text',"class"=>"title form-control tasktime","label"=>false,"div"=>false,'placeholder' => "Task Time: Default will be 1:00",'id' => 'tasktime',"ng-init"=> "app.tasktime='1:00'",'ng-model' =>'app.tasktime')); ?>
                            </div>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.tasktime"><button type="button" class="close" ng-click ="closetheerror('tasktime')" aria-hidden="true">&times;</button>&^&errors.tasktime&^&
                                </div>
                        </div>

                        <div class="form-group">
                         <div class="input-group" title="Billable Hours">
                            <span class="input-group-addon"><i class="ti-alarm-clock"></i></span>
                            <?php
                            echo $this->Form->input('billable_hours', array("type"=>'text',"class"=>"title form-control billable_hours","label"=>false,"div"=>false,'placeholder' => "Billable Hours: Default will be 1:00",'id' => 'billable_hours',"ng-init"=> "app.billable_hours='1:00'",'ng-model' =>'app.billable_hours')); 
                            echo $this->Form->hidden('page', array("type"=>'text',"class"=>"page_ form-control","label"=>false,"div"=>false,'placeholder' => "",'value' => 'new_ticket')); 
                            ?>
                            
                        </div>
                        <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.billable_hours"><button type="button" class="close" ng-click ="closetheerror('billable_hours')" aria-hidden="true">&times;</button>&^&errors.billable_hours&^&
                            </div>
                        </div>

                </div>
                <!-- Left Section End -->

                <!-- Right Section Start -->
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                        <div class="input-group" title="Title">
                            <span class="input-group-addon"><i class="fa fa-tasks"></i></span>
                             <?php echo $this->Form->input('title', array("type"=>'text',"class"=>"title form-control","label"=>false,"div"=>false,'placeholder' => "Title:",'ng-model' =>'app.title')); ?>
                            
                         </div>
                         <div class="alert alert-danger default_hidden" ng-class="fields.title"><button type="button" class="close" ng-click ="closetheerror('title')" aria-hidden="true">&times;</button>&^&errors.title&^&
                            </div>
                          <div class="error_title"></div>
                         </div>
                          <div class="form-group" ng-show="hidetextdess">
                            <?php echo $this->Form->textarea('description', array("type"=>'text',"class"=>"title form-control", 'id' => 'desc',"label"=>false,"div"=>false,'placeholder' => "",'rows'=>"14",'ng-model' =>'app.desc','ng-class' => 'hidetextdess', 'summer')); ?>
                            <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.desc"><button type="button" class="close" ng-click ="closetheerror('desc')" aria-hidden="true">&times;</button>&^&errors.desc&^&
                            </div>
                        </div>
                          <div class="dynamichtml white-space-pre" tagcontent  ng-show ="hidetextdes" ng-repeat="(index, deshtml) in tichtml track by $index" id="a&^&$index&^&">
                          </div>

                       
                </div>
                <!-- Right Section End -->
                <hr>
                <div class="col-md-12">
                     <?php  $this->Form->input('<i class="fa fa-envelope-o"></i> Send',array('type'=>'button','class'=>'btn btn-primary','id'=>'','onclick'=>'new_app_submit()','label' => false,'div'=>false, 'ng-click'=> "new_app_submit()")); ?>
                    <?php  $this->Form->input('<i class="fa fa-times"></i> Discard',array('type'=>'button','class'=>'btn btn-default','id'=>'postData','onclick'=>'','label' => false,'div'=>false)); ?>
                    <center>
                      <button type="submit" class="btn btn-primary" ng-click="new_app_submit();$event.preventDefault()" pd><i class="fa fa-envelope-o"></i> Add</button>
                       <button class="btn btn-default"  ng-click="goback()" pd><i class="ti-back-left "></i> Back</button>  
                    </center>
                    
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
        </div>
        </div>
      </div>
     <?php echo
        $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/add','angular/jquery-ui/jquery-ui','angular/aria','tpt/angular-ckeditor/angular-ckeditor','angular/upload/angular-upload.min','tpt/angular-fullscreen/src/angular-fullscreen','tpt/ng-file-upload/dist/ng-file-upload-shim','tpt/ng-file-upload/dist/ng-file-upload','angular/ui/src/sortable','tpt/angular-block/dist/angular-block-ui.min','tpt/sweet-alert/SweetAlert','jquery-ui-datepicker','datetimepicker_full'));
    ?>
       <script src="<?php echo BASE_URL; ?>plugins/bower_components/summernote/dist/summernote.min.js"></script>