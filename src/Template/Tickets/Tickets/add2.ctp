 <link href="<?php echo BASE_URL; ?>js/tpt/angular-datetimepicker/src/css/datetimepicker.css" id="theme"  rel="stylesheet">
<!-- animation CSS -->

     <div class="row">
        <div class="col-md-12">
          <div class="white-box">
          <?php $url =  $this->Url->build('/', true);?>
            <div class="row" ng-app="createTree" ng-controller="createCtr" ng-init ="name='ticket';base_url='<?php echo $url;?>';currentuser='<?php echo $current_user;?>'">
           <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel">
           <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="ti-user"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div>
                <div> <!-- <a href="#" class="btn btn-custom btn-block waves-effect waves-light" ng-click="new_app_submit();$event.preventDefault()">Add New Ticket</a> -->
                      <?php  $this->Html->link("Add Ticket",
                        array('controller'=>'tickets','action'=>'add2'),
                        array('class'=>'btn btn-custom btn-block waves-effect waves-light','alt'=>'ticket' )
                        ); ?>
                  <div class="list-group mail-list m-t-20">
                      <a href="#" class="list-group-item all_tickets" ng-click="get_data('all',1,'none','none','yes')" ng-class="active.all">All <span class="label label-rouded label-warning pull-right" ng-bind="count.all"></span></a> 
                      <a href="#" class="list-group-item opn_tickets" ng-click="get_data('open',0,'none','none','yes')" ng-class="active.open">Open <span class="label label-rouded label-success pull-right" ng-bind="count.opened"></span></a>
                      <a href="#" class="list-group-item clsd_tickets" ng-click="get_data('close',0,'none','none','yes')" ng-class="active.close">Closed <span class="label label-rouded label-danger pull-right" ng-bind="count.closed"></span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('inprogress',0,'none','none','yes')" ng-class="active.inprogress">In Progress<span class="label label-rouded label-info pull-right" ng-bind="count.inprogress"></span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('expire',0,'none','none','yes')" ng-class="active.expire">Expired<span class="label label-rouded label-info pull-right" ng-bind="count.expired"></span></a>
                      <!-- <a href="#" class="list-group-item">Trash <span class="label label-rouded label-default pull-right">55</span></a>  -->
                  </div>
                  <h3 class="panel-title m-t-40 m-b-0">Labels</h3>
                  <hr class="m-t-5">
                  <div class="list-group b-0 mail-list"> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-info m-r-10"></span>Medium</a>
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Urgent</a>
                    <!-- <a href="#" class="list-group-item"><span class="fa fa-circle text-purple m-r-10"></span>Private</a> -->
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Emergency</a> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>Normal</a> 
                 </div>
                </div>
              </div>
              <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">
                <h3 class="box-title">Add New Ticket 
                   <?php   echo $this->Html->link('<i class="fa  fa-mail-reply"></i>',
                    array('controller'=>'tickets','action'=>'index2'),
                    ['escape' => false,"class"=>"pull-right","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Go Back To Tickets List"]
                    );?>
                </h3>
                <div class="form-group">
                <?php echo $this->Form->create('Ticket',array('url' => ['action' => 'add'],'method'=>'POST','onsubmit' => '',"class"=>"",'ng-submit' => "new_app_submit()")); ?>
                 <!--  <input class="form-control" placeholder="To:"> -->
                 <?php 
                 $options =  $this->General->getuser_name();
                  $this->Form->select('to_id',$options,['empty' => 'To:','class' => 'form-control','value' => '','id' => 'toID' ,'ng-model' =>'app.to_id' ,'prevententer'] )
                 ;?>
                 <select name="app" class="form-control" ng-options="option.first_name for option in users track by option.id"  ng-model="app.to_id"> 
               <!--  <option value="" selected="selected">Select User</option> -->
                </select>
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="to_id"><button type="button" class="close" ng-click ="closetheerror('to_id')" aria-hidden="true">&times;</button>&^&errors.to_id&^&
                </div>
                 <div class="error_to"></div>
                </div>
                <div class="form-group">
                 <!--  <input class="form-control" placeholder="Title:"> -->
                 <?php
                 echo $this->Form->input('title', array("type"=>'text',"class"=>"title form-control","label"=>false,"div"=>false,'placeholder' => "Title:",'ng-model' =>'app.title')); 
                ?>
                <div class="alert alert-danger default_hidden" ng-class="fields.title"><button type="button" class="close" ng-click ="closetheerror('title')" aria-hidden="true">&times;</button>&^&errors.title&^&
                </div>
                <div class="error_title"></div>
                </div>
                  <div class="form-group">
                 <!--  <input class="form-control" placeholder="To:"> -->
                 <select name="app" class="form-control" ng-options="option.v for option in type track by option.k"  ng-model="app.type"> 
                <!-- <option value="" selected="selected">Select Type</option> -->
                </select>
                 <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.type"><button type="button" class="close" ng-click ="closetheerror('type')" aria-hidden="true">&times;</button>&^&errors.type&^&
                </div>
                </div>
                    <div class="form-group">
                 <!--  <input class="form-control" placeholder="To:"> -->
                 <select name="app" class="form-control" ng-options="option.v for option in priority track by option.k"  ng-model="app.priority"> 
                <!-- <option value="" selected="selected">Select Priority</option> -->
                </select>
                  <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.priority"><button type="button" class="close" ng-click ="closetheerror('priority')" aria-hidden="true">&times;</button>&^&errors.priority&^&
                </div>
                </div>
                 <div class="form-group">
                <div class="dropdown">
                <a class="dropdown-toggle" id="dropdown2" role="button" data-toggle="dropdown" data-target="#" href="#">
                <div class="input-group"><input type="text" class="form-control" data-ng-model="app.deadline | date:'medium'" ><span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                </a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <datetimepicker data-ng-model="app.deadline" data-datetimepicker-config="{ dropdownSelector: '#dropdown2' }" />
                </ul>
                </div>
                <!-- <input type="text" class="form-control" data-ng-model="data.dateDropDownInput"> -->
               <!--  <datetimepicker data-ng-model="app.deadline"></datetimepicker> -->
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.deadline"><button type="button" class="close" ng-click ="closetheerror('deadline')" aria-hidden="true">&times;</button>&^&errors.deadline&^&
                </div>
                </div>
                   <div class="form-group">
                 
                  <select name="app" class="form-control" ng-options="option.project_name for option in projectid track by option.id"  ng-model="app.projectid"> 
                <!-- <option value="" selected="selected">Select Priority</option> -->
                </select>
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.projectid"><button type="button" class="close" ng-click ="closetheerror('projectid')" aria-hidden="true">&times;</button>&^&errors.projectid&^&
                </div>
               
                </div>
                 <div class="form-group">
                 <!--  <input class="form-control" placeholder="Title:"> -->
                 <?php
                 echo $this->Form->input('tasktime', array("type"=>'text',"class"=>"title form-control tasktime","label"=>false,"div"=>false,'placeholder' => "Task Time: Default will be 1:00",'id' => 'tasktime',"ng-init"=> "app.tasktime='1:00'",'ng-model' =>'app.tasktime')); 
                ?>
                  <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.tasktime"><button type="button" class="close" ng-click ="closetheerror('tasktime')" aria-hidden="true">&times;</button>&^&errors.tasktime&^&
                </div>
                </div>
                <div class="form-group">
                 <!--  <input class="form-control" placeholder="Title:"> -->
                 <?php
                 echo $this->Form->input('billable_hours', array("type"=>'text',"class"=>"title form-control billable_hours","label"=>false,"div"=>false,'placeholder' => "Billable Hours: Default will be 1:00",'id' => 'billable_hours',"ng-init"=> "app.billable_hours='1:00'",'ng-model' =>'app.billable_hours')); 
                 echo $this->Form->hidden('page', array("type"=>'text',"class"=>"page_ form-control","label"=>false,"div"=>false,'placeholder' => "",'value' => 'new_ticket')); 
                ?>
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.billable_hours"><button type="button" class="close" ng-click ="closetheerror('billable_hours')" aria-hidden="true">&times;</button>&^&errors.billable_hours&^&
                </div>
                </div>
                <div class="form-group">
                  <!-- <textarea class="textarea_editor form-control" rows="15" placeholder="Enter text ..."></textarea> -->
                <?php
                 echo $this->Form->textarea('description', array("type"=>'text',"class"=>"title form-control", 'id' => 'desc',"label"=>false,"div"=>false,'placeholder' => "",'rows'=>"15",'ng-model' =>'app.desc')); 
                ?>
                <div class="alert alert-danger alert-dismissable default_hidden" ng-class="fields.desc"><button type="button" class="close" ng-click ="closetheerror('desc')" aria-hidden="true">&times;</button>&^&errors.desc&^&
                </div>

                </div>
               <!--  <h4><i class="ti-link"></i> Attachment</h4>
                <form action="#" class="dropzone">
                  <div class="fallback">
                    <input name="file" type="file" multiple />
                  </div>
                </form> -->
                <hr>
                <?php  $this->Form->input('<i class="fa fa-envelope-o"></i> Send',array('type'=>'button','class'=>'btn btn-primary','id'=>'','onclick'=>'','label' => false,'div'=>false, 'ng-click'=> "new_app_submit()")); ?>
                <?php  $this->Form->input('<i class="fa fa-times"></i> Discard',array('type'=>'button','class'=>'btn btn-default','id'=>'postData','onclick'=>'','label' => false,'div'=>false)); ?>
                <button type="submit" class="btn btn-primary " ng-click="new_app_submit();$event.preventDefault()"><i class="fa fa-envelope-o"></i> Send</button>
                <button class="btn btn-default" type="reset"><i class="fa fa-times"></i> Discard</button>
              </div>
            </div>
          </div>
        </div>
      </div>
     <?php 
      echo  $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/add','angular/jquery-ui/jquery-ui','angular/aria','tpt/angular-ckeditor/angular-ckeditor','angular/upload/angular-upload.min','tpt/angular-fullscreen/src/angular-fullscreen','tpt/ng-file-upload/dist/ng-file-upload-shim','tpt/ng-file-upload/dist/ng-file-upload','angular/ui/src/sortable','tpt/angular-block/dist/angular-block-ui.min','tpt/sweet-alert/SweetAlert','tpt/angular-datetimepicker/src/js/moment','tpt/angular-datetimepicker/src/js/datetimepicker','tpt/angular-datetimepicker/src/js/datetimepicker.templates'));
    ?>