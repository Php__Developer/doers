<!-- <script type="text/javascript">
	
		 $(document).ready(function(){ 
		 $(".bdayclass").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	650, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
			 $(".financialreport").fancybox({
					'type': 'iframe',
					'autoDimensions' : false,
					'width' :	950, 
					'height' :	700, 
					'onStart': function(){
					 jQuery("#fancybox-overlay").css({"position":"fixed"});
				}
			});
	}); 
	function closefb()
	{
		$.fancybox.close();
		window.location=document.URL;
	}
	</script> -->
 
<!--  start related-activities -->
	<div id="related-activities">
		
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
	
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
				<div class="right">
					 <h4 class="media-heading">Employees Management</h4>
          This section is used by Admin and HR only to manage all employees information.
<span id="animationSandbox" style="display: block;">


		  <?php
			if(!empty($employee) && $this->request->params['action'] == "edit"){
				$userId = $employee['id'];
			    $profileImage = $employee['image_url'];
				if(!empty($profileImage)){
					echo $this->Html->image(BASE_URL."img/employee_image/".$profileImage ,array("title" => 'Profile Pic',"alt" => 'Profile Pic',"id"=>"profileImg"));
				}else{
					echo $this->Html->image(BASE_URL."img/employee_image/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"id"=>"profileImg"));
				}
			}elseif(!empty($employee)){
				echo $this->Html->image(BASE_URL."img/employee_image/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"id"=>"profileImg"));
			}elseif($this->request->params['action'] == "add"){
			
          echo $this->Html->image(BASE_URL."img/employee_image/no_image.png",array("title" => 'Profile Pic',"alt" => 'Profile Pic',"id"=>"profileImg"));
        }?>
        </span>
      
          <div class="lines-dotted-short"></div>
		  <?php if(!empty($employee) && $this->request->params['action'] == "edit"){ ?>
				<form id="form1" action="" method="post" enctype="multipart/form-data">
				<div>
					<span id="spanButtonPlaceHolder"></span>
					<input id="btnCancel" type="button" value="Cancel All Uploads" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;display:none" />
				</div>

				</form>
		  <?php }else if($this->request->params['action'] == "add"){
?>
	<form id="form1" action="" method="post" enctype="multipart/form-data">
				<div>
					<span id="spanButtonPlaceHolder"></span>
					<input id="btnCancel" type="button" value="Cancel All Uploads" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;display:none" />
				</div>

				</form>
<?php


		  } ?>
<ul class="greyarrow">

<li>
<script type="text/javascript">
    function edit(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
           //console.log(reader);
            reader.onload = function(e) {
                $('#profileImg').attr('src', e.target.result)
                 .width('200')
                 .height('185');
            }
            reader.readAsDataURL(input.files[0]);
            // /  console.log(input.files[0]);
        }
    }

</script>

 </li>
 <?php if($this->request->params['action'] == "edit" || $this->request->params['action'] == "add"){ ?>
 <li>
<input type="file" name="upload" class="btn btn-block btn-primary" onchange="edit(this)" />
 </li>
 <?php } ;?>

   <div class="sttabs tabs-style-linebox erw">
                    <nav class="lis">
                     <h4 class="media-heading"></h4>
                    <ul >
             <li>

            <?php
			if(!empty($data)){
				echo $this->Html->link("Back to Listing", array('controller'=>'employees','action'=>'employeelist'),
				array('style'=>'color:red'));
			}else{
				echo $this->Html->link("Add New Employee", array('controller'=>'employees','action'=>'add'),
				array('style'=>'color:red'));
			}
            ?>
            </li> 
			<?php if($this->General->menu_permissions('employees','admin_birthday',$user[0]) == ADMIN_ID){ ?>
			<li>
			<?php echo $this->Html->link("View Birthdays", 
				array('controller'=>'employees','action'=>'birthday'),
				array('class'=>'bdayclass popup_window',"title"=>'Happy Birthdays','style'=>'color:red')); ?>
            </li>
			<?php } ?>
			<?php if($this->General->menu_permissions('employees','admin_birthday',$user[0]) == ADMIN_ID){ ?>
			<li>
			<?php echo $this->Html->link("View Appraisals", 
				array('controller'=>'appraisals','action'=>'userappraisallist','prefix' => 'appraisals'),
				array('class'=>'popup_window','style'=>'color:red'));
				  ?>
            </li>
			<?php } ?>
			<?php if($this->General->menu_permissions('employees','admin_birthday',$user[0]) == ADMIN_ID){ ?>
			<li>
			<?php echo $this->Html->link("Employee Financial Report", 
				array('controller'=>'employees','action'=>'financereport'),
				array('class'=>'financialreport popup_window',"title"=>"Employee Financial Report",'style'=>'color:red')
				);  ?>
            </li>
			<?php } ?>
                 </ul>
                    </nav>
               </div><!-- /tabs -->
            

       
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->