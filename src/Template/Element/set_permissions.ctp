<script type="text/javascript">
	
	$(window).load(function(){
		$('#setAction').change(function(){
			var act_id = $(this).val();
			if(act_id != ""){
				href=base_url+'permissions/fetchUserBasedOnAction/';
				$.post(href,{actID:act_id},function(data){
					$('#usrdat').html(data);
				});
			}else{
				$('#usrdat').html("<div align='center' style='color:red'>Please Select Role to Get Users!</div>");
				
			}
		});
		
		$('#PermissionSelectController').change(function(){
			href=base_url+'permissions/getactions/';
			var controllerName= $(this).val();
			$.post(href,{controllerID:controllerName},function(data){
				$('#setAction').html(data);
		});
		});
		
	});
</script>
<div id="content">

	<?php echo $form->create('Permission',array('action'=>'index','method'=>'POST', "name" => "setPermission","id" => "setPermission")); ?>

	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
					<th valign="top" >Controller:</th>
					<td>
						<?php
							$option ="";
						foreach($result as $res) :
							$option['cont'][$res['Permission']['controller']] =  $res['Permission']['controller'];
						  
						endforeach;
						
							$options[] = array_unique($option['cont']);
							
							echo $form->input('SelectController', array("type"=>'select',"class"=>"ourselect",'empty' => '--Select--','options' => $options,"label"=>false,"div"=>false)); 
						
						?>
					</td>
				<th valign="top" style="margin-left:116px;float:left;"> Action:</th>
				<td id="selectActionContainer">
					<?php
					/*	$option ="";
					foreach($result as $rs) :
							
						$option['cont'][$rs['Permission']['id']] =  $rs['Permission']['alias'];
					  
					endforeach;
						*/
					echo $form->input('controller', array("type"=>'select',"id"=>"setAction","class"=>"ourselect",'empty' => '--Select--',"label"=>false,"div"=>false));
					?>
				</td>
			</tr>
			<tr class="ss">
				<th  valign="top" >Roles:</th>
				
				<td>
					<?php 
							
							foreach($resultData as $key=>$value) :
									echo $form->input("role_".$key,array('type'=>'checkbox','class'=> 'roles',"label"=>false,"div"=>false,"value"=>$key,"id"=>$key,"hiddenField"=>false,'onchange'=>"javascript:chk_id(this.id)",'hiddenField'=>false));
									echo "<label for =". $key.">".$value."</label><br/>";
							endforeach;
					?>			
				</td>
				
				
				<th valign="top" id="usrdata" style=""></th>
				<td>
					<div id="usrdat" style="border: 1px solid gray; border-radius:4px; padding: 5px;  height: 200px;width: 500px;
		overflow-y: scroll;" >
						<?php  
								if(!empty($userData)) :
									echo $this->element('show_users'); 
								else :
									echo "<div align='center' style='color:red'>Please Select Role to Get Users!</div>";
								endif;
							?>
					</div>
				</td>
			</tr>
					
		</table>
		<?php echo $form->submit("Save",array("div"=>false,"class"=>"form-submit","name"=>"save")); ?>
		<?php echo $form->end(); ?>
</div>
			