<script type="text/javascript">
		var swfu;

		window.onload = function() {
			var id = '<?php echo ((!empty($data) && $data['mode']=="edit")?$data['id']:''); ?>';
			var settings = {
				flash_url : base_url+"js/swfupload/swfupload.swf",
				upload_url: base_url+'employees/uploadPic/'+id,
				post_params: {"PHPSESSID" : "<?php echo session_id(); ?>"},
				file_types : "*.jpeg;*.jpg;*.png;*.gif;*.doc;*.docx;*.ppt;*.txt;*.xls;*.xlsx;*.pdf",
				file_queue_limit : 0,
				file_post_name: 'uploadfile',
				debug: false,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel1"
				},
				// Button settings
				
				button_image_url : base_url+'js/swfupload/wdp_buttons_upload_114x29.png',
				button_width : 114,
				button_height : 29,
				button_placeholder_id: "spanDocPlaceHolder",
				button_text_left_padding: 12,
				button_text_top_padding: 3,
				
				// The event handler functions are defined in handlers.js
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : function(file, serverData, responseReceived){location.href=base_url+"/admin/employees/add/"+id; },
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
	     };
	</script>

<!--  start related-activities -->
	<div id="related-activities">
	<?php $user = $session->read("SESSION_ADMIN"); ?>
		<!--  start related-act-top -->
		<div id="related-act-top">
		<?php echo $html->image(BASE_URL."images/forms/header_related_doc.gif", array("alt"=>"Edit",'width'=>"271", 'height'=>"43")); ?>
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
				<div class="right">
					<h5>Documents Management</h5>
          All corresponding documents will be listed here
          <div class="lines-dotted-short"></div>
		  <?php if(!empty($data) && $data['mode'] == "edit"){ ?>
				<form id="form1" action="" method="post" enctype="multipart/form-data">
				<div>
					<span id="spanDocPlaceHolder"></span>
					<input id="btnCancel1" type="button" value="Cancel All Uploads" onclick="swfu.cancelQueue();" disabled="disabled" style="margin-left: 2px; font-size: 8pt; height: 29px;display:none" />
				</div>

				</form>
		  <?php } ?>
			<ul class="greyarrow">
  				<li>
					<?php echo $html->link("Download Search Result : Excel", array('controller'=>'employees','action'=>'exportci','onclick'=>'return false;')
            );	
            ?>
            </li>
						
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->