<div class="row">
<div class="alert alert-loading myadmin-alert-top alerttop  successmessageloader"> <span class="succesfont"></span></div>
<!-- <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop alert-dismissable successmessage" >  </div> -->
<div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop successmessage"> 

</div>
        <div class="col-md-6">
          <div class="white-box">
            <h3 class="box-title m-b-0">Add New Alias</h3>
            <p class="text-muted m-b-30 font-13"> Please Enter Details Below </p>
            <div class="row mainrow">
              <div class="col-sm-12 col-xs-12">
              <?php echo $this->Form->create('Module',array('url' => ['action' => 'addmodule'],'method'=>'POST','onsubmit' => '',"class"=>"")); ?>
              
                  <div class="form-group">
                    <label for="exampleInputuname">Controller*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputuname" placeholder="Module Name" name="module_name"> -->
                      <?php echo $this->Form->select('controller',$controllers,['empty' => 'None','class' => 'form-control c0ntroller','default' => isset($controller) ? $controller : "" ] );?>
                    </div>
                      <?php 
//                      pr($errors); die;
                      if(isset($errors['controller'])){
                  //   pr($errors['controller']); die;
                      echo $this->General->errorHtml($errors['controller']);
                      } ;?>
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Action*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->select('action',[],['empty' => '--Select--','class' => 'form-control acti0n5','id' => ''] );?>
                    </div>
                      <?php if(isset($errors['action'])){
                      echo $this->General->errorHtml($errors['action']);
                      } ;?>
                  </div>

                    <div class="form-group">
                    <label for="exampleInputEmail1">Alias*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->input("alias",array("type"=>"text","class"=>"form-control alias",'placeholder'=> "Enter Alias", "label"=>false,"div"=>false));?>
                    </div>
                      <?php if(isset($errors['alias'])){
                      echo $this->General->errorHtml($errors['alias']);
                      } ;?>
                  </div>
                  <div class="form-group">
                       <label for="exampleInputpwd1">Notification For This Alias*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('is_notified',['No'=> 'No','Yes'=>'Yes'],['class' => 'form-control is_notified'] );?>
                      </div>
                      <span class="help-block">Should Notifications to be sent for this module</span>
                      <?php if(isset($errors['is_notified'])){
                      echo $this->General->errorHtml($errors['is_notified']);
                      } ;?>
                      <div class="is_displayed_error"></div>
                    </div>
                    <div class="form-group">
                  <label class="col-md-12">Description*</label>
                  
                   <?php echo $this->Form->input("description",array("type"=>"textarea","class"=>"form-control description",'placeholder'=> "Description ", "label"=>false,"div"=>false));?>
                   <?php echo $this->Form->hidden("type",array("type"=>"textarea","class"=>"form-control type",'placeholder'=> "Description ", "label"=>false,"div"=>false,'value' => 'permissions'));?>

                   <?php if(isset($errors['description'])){
                      echo $this->General->errorHtml($errors['description']);
                      } ;?>
                    <div class="description_error"></div>
                </div>
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                  <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>
       <div class="col-md-6 ">
        <div class="white-box ">
        <h3 class="box-title m-b-0">Untracked Permissions</h3>
        <div class="row errors">
              <!-- <div class="controller_error"></div>
              <div class="action_error"></div> -->
              <div class="alias_error"></div>
            </div>
             <div class="row">
             <div class="col-md-6 m-b-20">
                <button class="btn btn-block btn-outline btn-rounded btn-info addall pull-right default_hidden">Add All</button>
              </div>
             <div class="resultsets">
               
             </div>
            </div>

            <div class="row">
               <div class="col-md-4">
                <button class="btn btn-block btn-outline btn-success untracked_perms">Untracked Permissions</button>
              </div>
            </div>
            <!-- <h3 class="box-title m-b-0">Swipe Table</h3>
            <p class="text-muted m-b-20">The Column Swipe Table allows the user to select which columns they want to be visible.</p>
            <div class="tablesaw-bar mode-swipe tablesaw-all-cols-visible"><div class="tablesaw-advance"><a href="#" class="tablesaw-nav-btn btn btn-micro left disabled" title="Previous Column"></a><a href="#" class="tablesaw-nav-btn btn btn-micro right disabled" title="Next Column"></a></div></div>
            <table class="tablesaw table-bordered table-hover table tablesaw-swipe" data-tablesaw-mode="swipe" id="table-1937" style="">
              <thead>
                <tr>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist">Movie Title</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="">Rank</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="">Year</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class=""><abbr title="Rotten Tomato Rating">Rating</abbr></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="">Gross</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Avatar</a></td>
                  <td class="">1</td>
                  <td class="">2009</td>
                  <td class="">83%</td>
                  <td class="">$2.7B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Titanic</a></td>
                  <td class="">2</td>
                  <td class="">1997</td>
                  <td class="">88%</td>
                  <td class="">$2.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">The Avengers</a></td>
                  <td class="">3</td>
                  <td class="">2012</td>
                  <td class="">92%</td>
                  <td class="">$1.5B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Harry Potter and the Deathly Hallows—Part 2</a></td>
                  <td class="">4</td>
                  <td class="">2011</td>
                  <td class="">96%</td>
                  <td class="">$1.3B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Frozen</a></td>
                  <td class="">5</td>
                  <td class="">2013</td>
                  <td class="">89%</td>
                  <td class="">$1.2B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Iron Man 3</a></td>
                  <td class="">6</td>
                  <td class="">2013</td>
                  <td class="">78%</td>
                  <td class="">$1.2B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Transformers: Dark of the Moon</a></td>
                  <td class="">7</td>
                  <td class="">2011</td>
                  <td class="">36%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">The Lord of the Rings: The Return of the King</a></td>
                  <td class="">8</td>
                  <td class="">2003</td>
                  <td class="">95%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Skyfall</a></td>
                  <td class="">9</td>
                  <td class="">2012</td>
                  <td class="">92%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Transformers: Age of Extinction</a></td>
                  <td class="">10</td>
                  <td class="">2014</td>
                  <td class="">18%</td>
                  <td class="">$1.0B</td>
                </tr>
              </tbody>
            </table> -->
          </div>
        </div>
        
      </div>
      <?php  echo  $this->Html->script(array('jquery-3','plugins/bower_components/multiselect/js/jquery.multi-select','module_page'));?>