<link href="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
  
  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop errormsg"> 

  </div>
  <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop successmessage"> 

</div>
<div class="row">
        <div class="col-md-6">
          <div class="white-box">
            <h3>Module Management</h3>
            In This Panel Super Admin Can Manage The Modules Of Current Agency. If you disbale any Module all the Submodules will be disabled & all the actions to </div>
        </div>
        <div class="col-md-6">
          <div class="white-box">
            <h3>Other Controls</h3>
            <?php 
			echo $this->Html->link('<i class="fa  fa-plus"></i>',
			array('controller'=>'modules','action'=>'addmodule'),
			['escape' => false,"class"=>"btn btn-info btn-circle m-r-20","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Aliases"]
			);

			echo $this->Html->link('<i class="fa  fa-plus"></i>',
			array('controller'=>'modules','action'=>'addsubmodule'),
			['escape' => false,"class"=>"btn btn-info btn-circle m-r-20","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to add Module"]
			);

		

			/*echo $this->Html->link('<i class="fa  fa-pencil"></i>',
			array('controller'=>'modules','action'=>'editmodules'),
			['escape' => false,"class"=>"btn btn-info btn-circle m-r-20","data-toggle"=>"tooltip", "data-color"=> "#99d683","data-placement"=>"top" ,"data-original-title"=>"Click to add Sub-Modules"]
			);*/

			?>
           </div>
        </div>
  </div>
 <div class="row">
 <?php 
 if(isset($resultData)){
 foreach($resultData as $data) { 
 	$module_is_enabled = ($data['is_enabled'] == 'Yes') ? 'checked': '';
 	?>
 	 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 module_wrapper0_">
          <div class="panel panel-default">
            <div class="panel-heading"><?php echo $data['display_name'] ;?>
	            <div class="panel-action moduleChekb0x_wrapper">
	            	<input type="checkbox" <?php echo $module_is_enabled;?> class="js-switch pull-right moduleChekb0x"  data-color="#99d683" />
	            	<input type="hidden" value = "<?php echo $this->General->ENC($data->id);?>" class="m0e0n0u__" />
			      <?php 	echo $this->Html->link('<i class="fa  fa-pencil"></i>',
					array('controller'=>'modules','action'=>'editsubmodules','origin' =>  $this->General->encForUrl($this->General->ENC($data->id))  ),
					['escape' => false,"class"=>"btn btn-success btn-circle edit1con","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to Edit Modules"]
					);?>
					 <?php 	echo $this->Html->link('<i class="fa  fa-times"></i>',
					array('controller'=>'modules','action'=>'deletemodule','origin' =>  $this->General->encForUrl($this->General->ENC($data->id))  ),
					['escape' => false,"class"=>"btn btn-danger btn-circle delete1con","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to Edit Modules"]
					);?>
	             </div>
            </div>
            <div class="panel-wrapper collapse in modulesscroll">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th class="text-center">#</th>
                    <th>Sub-module</th>
                    <th>Enable/Disbale</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  	$child_modules = $this->General->child_modules($data->id);
                  	 if(count($child_modules)){ 
                  	 	$counter = 1;
                  	 	foreach ($child_modules  as $module){
                  	 		$sub_module_is_enabled = ($module['is_enabled'] == 'Yes') ? 'checked': '';

                  	 	?>
                  <tr>
                    <td align="center"><?php echo $counter ;?></td>
                    <td><?php echo $module['display_name'];?></td>
                    <td>
                    <input type="checkbox" <?php echo $sub_module_is_enabled ;?> class="js-switch submoduleChekb0x"  data-color="#f96262" data-size="small" />
                      <?php 	echo $this->Html->link('<i class="fa  fa-pencil"></i>',
					array('controller'=>'modules','action'=>'editsubmodules','origin' =>  $this->General->encForUrl($this->General->ENC($module->id))  ),
					['escape' => false,"class"=>"btn btn-success btn-circle subedit1con","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to Edit Modules"]
					);?>
					 <?php 	echo $this->Html->link('<i class="fa  fa-times"></i>',
					array('controller'=>'modules','action'=>'deletesubmodule','origin' =>  $this->General->encForUrl($this->General->ENC($module->id))  ),
					['escape' => false,"class"=>"btn btn-danger btn-circle subdelete1con","data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Click to Edit Modules"]
					);?>
					<input type="hidden" value = "<?php echo $this->General->ENC($module->id);?>" class="subm0e0n0u__" />
                    <input type="hidden" value = "submodule" class="from__" />
                    </td>
                    
                  </tr>
                  <?php 
                  		$counter++;
                  		}
                  }?>
                  <!--<tr>
                    <td align="center">2</td>
                    <td>Jacob</td>
                    <td>Thornton</td>
                  </tr>
                  <tr>
                    <td align="center">3</td>
                    <td>Steave</td>
                    <td>Jobs</td>
                  </tr> -->
                </tbody>
              </table>
            </div>
          </div>
        </div>
       <?php 
       		}
       }?>
 </div>
 <?php 
    echo  $this->Html->script(array('jquery-3','module_page'));
?>
<script src="<?php echo BASE_URL; ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>

