<div class="row">
        <div class="col-md-6">
          <div class="white-box">
            <h3 class="box-title m-b-0">Add New Module</h3>
            <p class="text-muted m-b-30 font-13"> Please Enter Details Below </p>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
              <?php echo $this->Form->create($modules,array('url' => ['action' => 'editmodules'],'method'=>'POST','onsubmit' => '',"class"=>"")); ?>
                  <div class="form-group">
                    <label for="exampleInputuname">Module Name</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputuname" placeholder="Module Name" name="module_name"> -->
                      <?php echo $this->Form->input("module_name",array("type"=>"text",'id'=>"exampleInputuname" ,"class"=>"form-control",'placeholder'=> "Module Name", "label"=>false,"div"=>false));?>
                      <?php echo $this->Form->hidden("id",array("type"=>"text",'id'=>"exampleInputuname" ,"class"=>"form-control origin",'placeholder'=> "Module Name", "label"=>false,"div"=>false));?>
                    </div>
                      <?php if(isset($errors['module_name'])){
                      echo $this->General->errorHtml($errors['module_name']);
                      } ;?>
                    
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Display Name</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->input("display_name",array("type"=>"text",'id'=>"exampleInputEmail1" ,"class"=>"form-control",'placeholder'=> "Module Name", "label"=>false,"div"=>false));?>
                    </div>
                      <?php if(isset($errors['display_name'])){
                      echo $this->General->errorHtml($errors['display_name']);
                      } ;?>
                  </div>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Enable/Disbale</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <!--   <select class="form-control" name="is_enabled">
                        <option value="Yes">Enable</option>
                        <option value="No">Disbale</option>
                      </select> -->
                      <?php echo $this->Form->select('is_enabled',['No'=> 'Disbale','Yes'=>'Enable'],['empty' => '--Select--','class' => 'form-control'] );?>
                      </div>
                      <span class="help-block">Enable OR disbale this Module for current Agency</span>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Display Or Not</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <!--   <select class="form-control" name="is_displayed">
                        <option value="No">Do Not Display</option>
                        <option value="Yes">Display The Module</option>
                      </select> -->
                      <?php echo $this->Form->select('is_displayed',['No'=> 'Do Not Display','Yes'=>'Display The Module'],['empty' => '--Select--','class' => 'form-control'] );?>
                      </div>
                      <span class="help-block">Enable OR disbale this Module for current Agency</span>
                    </div>
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                  <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                </form>
              </div>
            </div>
          </div>
        </div>
       <!-- <div class="col-md-6">
          <div class="white-box">
            <h3 class="box-title m-b-0">Sample Forms with Right icon</h3>
            <p class="text-muted m-b-30 font-13"> Bootstrap Elements </p>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <form>
                  <div class="form-group">
                    <label for="exampleInputuname">User Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="exampleInputuname" placeholder="Username">
                      <div class="input-group-addon"><i class="ti-user"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <div class="input-group">
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      <div class="input-group-addon"><i class="ti-email"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputpwd1">Password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" id="exampleInputpwd1" placeholder="Enter pwd">
                      <div class="input-group-addon"><i class="ti-lock"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputpwd2">Confirm Password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" id="exampleInputpwd2" placeholder="Enter pwd">
                      <div class="input-group-addon"><i class="ti-lock"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="checkbox checkbox-success">
                      <input id="checkbox2" type="checkbox">
                      <label for="checkbox2"> Remember me </label>
                    </div>
                  </div>
                  <div class="text-right">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div> -->
        
      </div>