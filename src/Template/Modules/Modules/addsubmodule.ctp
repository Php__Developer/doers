<link href="<?php echo BASE_URL; ?>plugins/bower_components/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" />
<div class="row">
<div class="alert alert-loading myadmin-alert-top alerttop  successmessageloader"> <span class="succesfont"></span></div>
<link href="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
<!-- <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop alert-dismissable successmessage" >  </div> -->
<div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-success myadmin-alert-top alerttop successmessage"> 

</div>
    <div class="alert alert-danger alert-dismissable default_hidden errormsg">
    <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         Lorem ipsum dolor sit amet, consectetur adipisicing elit. -->
    </div>
 <!-- <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. </div> -->
        <div class="col-md-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Add New Module/Sub-Module</h3>
            <p class="text-muted m-b-30 font-13"> Please Enter Details Below </p>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
              <?php echo $this->Form->create('Module',array('url' => ['action' => 'addsubmodule'],'method'=>'POST','onsubmit' => '',"class"=>"")); ?>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Agencies</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('agency_id',$agencies,['class' => 'form-control agency_id' , 'multiple' => true] );?>
                      </div>
                      <span class="help-block">Please select Agency under which you want to implement this Module/Sub-module</span>
                      <?php if(isset($errors['agency_id'])){
                      echo $this->General->errorHtml($errors['agency_id']);
                      } ;?>
                      <div class="agency_id_error"></div>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Roles*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('role_id',$roles,['class' => 'form-control role_id','id' => '', 'multiple' => true] );?>
                      </div>
                      <span class="help-block">Please select Roles that can access this Module/Sub-module</span>
                      <?php if(isset($errors['role_id'])){
                      echo $this->General->errorHtml($errors['role_id']);
                      } ;?>
                      <div class="role_id_error"></div>
                    </div>
                  <div class="form-group">
                    <label for="exampleInputuname">Name*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="ti-wallet"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputuname" placeholder="Sub-Module Name" name="sub_module"> -->
                      <?php echo $this->Form->input("module_name",array("type"=>"text","class"=>"form-control module_name",'placeholder'=> "Enter Name", "label"=>false,"div"=>false));?>
                      <?php echo $this->Form->hidden("mode",array("type"=>"text","class"=>"form-control mode",'placeholder'=> "Sub-Module Name", 'value'=> $mode, "label"=>false,"div"=>false));?>
                      <?php echo $this->Form->hidden("type",array("type"=>"text","class"=>"form-control thetype",'placeholder'=> "Sub-Module Name", 'value'=> 'module', "label"=>false,"div"=>false));?>
                    </div>
                      <?php if(isset($errors['module_name'])){
                      echo $this->General->errorHtml($errors['module_name']);
                      } ;?>
                    <div class="module_name_error"></div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Display Name*</label>
                    <div class="input-group">
                      <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                      <!-- <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Display Name" name="display_name"> -->
                      <?php echo $this->Form->input("display_name",array("type"=>"text","class"=>"form-control display_name",'placeholder'=> "Enter Display Name", "label"=>false,"div"=>false));?>
                    </div>
                      <?php if(isset($errors['display_name'])){
                      echo $this->General->errorHtml($errors['display_name']);
                      } ;?>
                      <div class="display_name_error"></div>
                  </div>
                      <div class="form-group">
                       <label for="exampleInputpwd1">Parent Module</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                        <?php echo $this->Form->select('parent_module',$modules,['empty' => 'None','class' => 'form-control parentm0dule'] );?>
                      </div>
                      <span class="help-block">Please select Module under which you want to display that Sub-module</span>
                      <?php if(isset($errors['parent_module'])){
                      echo $this->General->errorHtml($errors['parent_module']);
                      } ;?>
                      <div class="parent_module_error"></div>
                    </div>
                    <div class="subm0dulefields default_hidden">
                        <div class="form-group">
                           <label for="exampleInputpwd1">Default Alias*</label>
                           <div class="input-group">
                            <div class="input-group-addon"><i class="ti-lock"></i></div>
                          <?php echo $this->Form->select('default_alias',$aliases,['empty' => '--Select--','class' => 'form-control default_alias','value' => [$selectedcontroller] ] );?>
                          </div>
                          <span class="help-block">Please select Controller to which that module is related</span>
                           <?php if(isset($errors['controller'])){
                          echo $this->General->errorHtml($errors['controller']);
                          } ;?>
                          <div class="default_alias_error"></div>
                        </div>
                        <div class="form-group">
                           <label for="exampleInputpwd1">Related Aliases</label>
                        <div class="input-group m-b-30"> <span class="input-group-addon"><i class="ti-lock"></i></span>
                        <!-- <input type="text" value=" "  placeholder="add tags" class="tag51nput" data-role="tagsinput" name="related_actions"> -->
                        <?php echo $this->Form->input("other_aliases",array("type"=>"text","class"=>"tag51nput other_aliases",'placeholder'=> "Type in box below to add more aliases", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>

                        </div>
                        <span class="help-block">Please Type in the Following box </span>
                        <div class="other_aliases_error"></div>
                        </div>
                       <div class="form-group">
                        <label for="exampleInputEmail1">Add More Aliases(if not added)</label>
                        <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
                          <input class="typeaheadcontro__ form-control" type="text" placeholder="Actions" name="actiontypebox">
                        </div>
                      </div> 

                    </div>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Enable/Disbale*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                       <!--  <select class="form-control" name="is_enabled">
                        <option value="Yes">Enable</option>
                        <option value="No">Disbale</option>
                      </select> -->
                      <?php echo $this->Form->select('is_enabled',['No'=> 'Disbale','Yes'=>'Enable'],['empty' => '--Select--','class' => 'form-control is_enabled'] );?>
                      </div>
                      <span class="help-block">Enable OR disbale this Module for current Agency</span>
                    <?php if(isset($errors['is_enabled'])){
                      echo $this->General->errorHtml($errors['is_enabled']);
                      } ;?>
                      <div class="is_enabled_error"></div>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputpwd1">Display Or Not*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('is_displayed',['No'=> 'Do Not Display','Yes'=>'Display The Module'],['empty' => '--Select--','class' => 'form-control is_displayed'] );?>
                      </div>
                      <span class="help-block">Enable OR disbale this Module for current Agency</span>
                      <?php if(isset($errors['is_displayed'])){
                      echo $this->General->errorHtml($errors['is_displayed']);
                      } ;?>
                      <div class="is_displayed_error"></div>
                    </div>
                     <div class="form-group">
                       <label for="exampleInputpwd1">Core Module Or Not*</label>
                       <div class="input-group">
                        <div class="input-group-addon"><i class="ti-lock"></i></div>
                      <?php echo $this->Form->select('is_displayed',['No'=> 'No','Yes'=>'Yes'],['class' => 'form-control is_core'] );?>
                      </div>
                      <span class="help-block">Core Modules will be visible to all agencies By default</span>
                      <?php if(isset($errors['is_displayed'])){
                      echo $this->General->errorHtml($errors['is_displayed']);
                      } ;?>
                      <div class="is_displayed_error"></div>
                    </div>
                  <div class="form-group">
                  <label class="col-md-12">Description*</label>
                  
                   <?php echo $this->Form->input("description",array("type"=>"textarea","class"=>"form-control desc",'placeholder'=> "Description ", "label"=>false,"div"=>false));?>
                 <?php echo $this->Form->hidden("type",array("type"=>"textarea","class"=>"form-control type",'placeholder'=> "Description ", "label"=>false,"div"=>false,'value' => 'modules'));?>
                   <?php if(isset($errors['module_name'])){
                      echo $this->General->errorHtml($errors['module_name']);
                      } ;?>
                    <div class="description_error"></div>
                </div>
                  <div class="form-group"></div>
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10 m0dulesubmit">Submit</button>
                  <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                </form>
                <div class="c0ntr0ll__"><?php echo implode(',',$controllers);?></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 ">
        <div class="white-box ">
        <h3 class="box-title m-b-0">Unused Aliases</h3>
        <div class="row errors">
              <!-- <div class="controller_error"></div>
              <div class="action_error"></div> -->
              <div class="alias_error"></div>
            </div>
             <div class="row">
             <!-- <div class="col-md-4 m-b-20">
                <button class="btn btn-block btn-outline btn-rounded btn-info addall pull-right">Add All</button>
              </div> -->
             <div class="resultsets">
               
             </div>
            </div>

            <div class="row">
               <div class="col-md-4">
                <button class="btn btn-block btn-outline btn-success untracked_perms">Untracked Permissions</button>
              </div>
            </div>
            <!-- <h3 class="box-title m-b-0">Swipe Table</h3>
            <p class="text-muted m-b-20">The Column Swipe Table allows the user to select which columns they want to be visible.</p>
            <div class="tablesaw-bar mode-swipe tablesaw-all-cols-visible"><div class="tablesaw-advance"><a href="#" class="tablesaw-nav-btn btn btn-micro left disabled" title="Previous Column"></a><a href="#" class="tablesaw-nav-btn btn btn-micro right disabled" title="Next Column"></a></div></div>
            <table class="tablesaw table-bordered table-hover table tablesaw-swipe" data-tablesaw-mode="swipe" id="table-1937" style="">
              <thead>
                <tr>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist">Movie Title</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="">Rank</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="">Year</th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="1" class=""><abbr title="Rotten Tomato Rating">Rating</abbr></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="">Gross</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Avatar</a></td>
                  <td class="">1</td>
                  <td class="">2009</td>
                  <td class="">83%</td>
                  <td class="">$2.7B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Titanic</a></td>
                  <td class="">2</td>
                  <td class="">1997</td>
                  <td class="">88%</td>
                  <td class="">$2.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">The Avengers</a></td>
                  <td class="">3</td>
                  <td class="">2012</td>
                  <td class="">92%</td>
                  <td class="">$1.5B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Harry Potter and the Deathly Hallows—Part 2</a></td>
                  <td class="">4</td>
                  <td class="">2011</td>
                  <td class="">96%</td>
                  <td class="">$1.3B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Frozen</a></td>
                  <td class="">5</td>
                  <td class="">2013</td>
                  <td class="">89%</td>
                  <td class="">$1.2B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Iron Man 3</a></td>
                  <td class="">6</td>
                  <td class="">2013</td>
                  <td class="">78%</td>
                  <td class="">$1.2B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Transformers: Dark of the Moon</a></td>
                  <td class="">7</td>
                  <td class="">2011</td>
                  <td class="">36%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">The Lord of the Rings: The Return of the King</a></td>
                  <td class="">8</td>
                  <td class="">2003</td>
                  <td class="">95%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Skyfall</a></td>
                  <td class="">9</td>
                  <td class="">2012</td>
                  <td class="">92%</td>
                  <td class="">$1.1B</td>
                </tr>
                <tr>
                  <td class="title tablesaw-cell-persist"><a href="javascript:void(0)">Transformers: Age of Extinction</a></td>
                  <td class="">10</td>
                  <td class="">2014</td>
                  <td class="">18%</td>
                  <td class="">$1.0B</td>
                </tr>
              </tbody>
            </table> -->
          </div>
        </div>

       <!-- <div class="col-md-6">
          <div class="white-box">
            <h3 class="box-title m-b-0">Sample Forms with Right icon</h3>
            <p class="text-muted m-b-30 font-13"> Bootstrap Elements </p>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <form>
                  <div class="form-group">
                    <label for="exampleInputuname">User Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control" id="exampleInputuname" placeholder="Username">
                      <div class="input-group-addon"><i class="ti-user"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <div class="input-group">
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      <div class="input-group-addon"><i class="ti-email"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputpwd1">Password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" id="exampleInputpwd1" placeholder="Enter pwd">
                      <div class="input-group-addon"><i class="ti-lock"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputpwd2">Confirm Password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" id="exampleInputpwd2" placeholder="Enter pwd">
                      <div class="input-group-addon"><i class="ti-lock"></i></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="checkbox checkbox-success">
                      <input id="checkbox2" type="checkbox">
                      <label for="checkbox2"> Remember me </label>
                    </div>
                  </div>
                  <div class="text-right">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                    <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div> -->
        
      </div>

      <?php  echo  $this->Html->script(array('jquery-3','plugins/bower_components/multiselect/js/jquery.multi-select','module_page'));?>
       <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
      <script src="<?php echo BASE_URL; ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
      <script src="<?php echo BASE_URL; ?>bootstrap/dist/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?php echo BASE_URL; ?>plugins/bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
