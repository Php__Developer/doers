<div class="form-group">
     <label for="exampleInputpwd1">Controller*</label>
     <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
    <?php echo $this->Form->select('controller',$controllers,['empty' => '--Select--','class' => 'form-control c0ntrol3r','value' => [$selectedcontroller] ] );?>
    </div>
    <span class="help-block">Please select Controller to which that module is related</span>
     <?php if(isset($errors['controller'])){
    echo $this->General->errorHtml($errors['controller']);
    } ;?>
    <div class="controller_error"></div>
  </div>
   <div class="form-group">
     <label for="exampleInputpwd1">Action*</label>
     <div class="input-group">
      <div class="input-group-addon"><i class="ti-lock"></i></div>
     <!--  <select class="form-control acti0ns" name="action">
      <option value="">--Select--</option>
    </select> -->
    <?php echo $this->Form->select('action',[],['empty' => '--Select--','class' => 'form-control acti0ns','value' => [$selectedaction ]  ] );?>
    <input type="hidden" value="<?php echo $selectedaction;?>" name="selectedaction"  class="selectedaction" >
    </div>
    <span class="help-block">Please select the action of the controller</span>
      <?php if(isset($errors['action'])){
    echo $this->General->errorHtml($errors['action']);
    } ;?>
    <div class="action_error"></div>
  </div>
  <div class="form-group">
     <label for="exampleInputpwd1">Related Actions</label>
  <div class="input-group m-b-30"> <span class="input-group-addon"><i class="ti-lock"></i></span>
  <!-- <input type="text" value=" "  placeholder="add tags" class="tag51nput" data-role="tagsinput" name="related_actions"> -->
  <?php echo $this->Form->input("related_actions",array("type"=>"text","class"=>"tag51nput",'placeholder'=> "add tags", 'data-role'=> "tagsinput","label"=>false,"div"=>false));?>

  </div>
  <span class="help-block">Please Type in the Following box </span>
  <div class="related_actions_error"></div>
  </div>
 <div class="form-group">
  <label for="exampleInputEmail1">Add More Actions(if not added)</label>
  <div class="input-group">
     <div class="input-group-addon"><i class="fa fa-reorder"></i></div>
    <input class="typeaheadcontro__ form-control" type="text" placeholder="Actions" name="actiontypebox">
  </div>
</div> 