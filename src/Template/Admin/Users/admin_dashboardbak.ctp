<h1>
	<font size="+2"><marquee behavior="alternate" bgcolor="yellow" scrollamount="3"><span style="color:#ff0000;">Alerts</span>: Sales Meeting Daily | Engineering Meeting Daily | Update Daily Work on VOOAP</marquee></font></h1>
<p>
	<span style="font-size:12px;"><span style="color: rgb(255, 0, 0);"><u><strong>Notifications:</strong></u></span></span></p>
<ul style="">
	<li>
		<u style="color: rgb(255, 0, 0);"><strong>__________________________________________________________________________________</strong></u></li>
	<li>
		&nbsp;</li>
	<li>
		<span style="font-size:12px;">Annual Function on 5th January 2013. It is Mandatory for everyone either trainee or employee of VLL Software.&nbsp;</span><br />
		<h4 class="ml20 pr20 normal left mtb5" style="margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 13px; vertical-align: baseline; background-color: transparent; font-weight: normal; color: rgb(45, 45, 45); font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 23.566667556762695px;">
			<em><strong><span style="font-size:12px;">Venue: </span><span style="color:#ff0000;">Lemon fusion &nbsp;</span><span style="font-size:12px;">SCO 443 &amp; 444,&nbsp;<strong style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;">Sector 35</strong>&nbsp;C, Chandigarh. For any query call Gurnam 07837284703 or email at hr@VLLsoftware.com</span></strong></em></h4>
	</li>
	<li>
		<p>
			<span style="color:#ff0000;"><u><strong>__________________________________________________________________________________</strong></u></span></p>
	</li>
	<li>
		<span style="font-size:12px;">Monthly Perfomace will be filled till 8th January 2013 by respective seniors.</span></li>
	<li>
		<u style="color: rgb(255, 0, 0);"><strong>__________________________________________________________________________________</strong></u></li>
	<li>
		<span style="font-size:12px;">Mastersheets could be reviewed anytime by either <font color="#008000">Process OR Engineering persons.</font></span></li>
	<li>
		<u style="color: rgb(255, 0, 0);"><strong>__________________________________________________________________________________</strong></u></li>
	<li>
		<span style="font-size:12px;">VOOAP NEXT Release: <strong>Engineering &amp; Sales Email Formats</strong> will be updated soon, you can copy&amp;paste whenever applicable</span></li>
	<li>
		<u style="color: rgb(255, 0, 0);"><strong>__________________________________________________________________________________</strong></u></li>
	<li>
		<span style="font-size:12px;">Update Your Mail Signatures like following (Change based on Client or Subject):</span></li>
</ul>
<p>
	<span style="font-size:12px;"><span style="font-family: arial;">--&nbsp;</span><br style="font-family: arial; font-size: small;" />
	<br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<span style="font-family: arial; text-align: -webkit-auto; color: rgb(0, 0, 0);">Thanks,</span><br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<span style="font-family: arial; text-align: -webkit-auto; color: rgb(0, 0, 0);">Regards,</span><br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<b style="text-align: -webkit-auto; font-family: arial; font-size: small; color: rgb(0, 0, 0);">&lt;Full Name&gt;</b></span></p>
<p>
	<span style="font-size:12px;"><b style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);">&lt;Designation&gt;</b><br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<span style="font-family: arial; text-align: -webkit-auto; color: rgb(0, 0, 0);">VLL Software (P) Ltd.</span><br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<span style="font-family: arial; text-align: -webkit-auto; color: rgb(0, 0, 0);">Empowering Innovation With Excellence</span><br style="font-family: arial; font-size: small;" />
	<a href="http://VLLsoftware.com/" style="color: rgb(17, 85, 204); font-family: arial; font-size: small;" target="_blank">http://VLLsoftware.com</a><br style="font-family: arial; font-size: small; text-align: -webkit-auto; color: rgb(0, 0, 0);" />
	<span style="font-family: arial; text-align: -webkit-auto; color: rgb(0, 0, 0);">Phone: +91-172-654-1211</span></span></p>
<p>
	<u style="color: rgb(255, 0, 0);"><strong>__________________________________________________________________________________</strong></u></p>
