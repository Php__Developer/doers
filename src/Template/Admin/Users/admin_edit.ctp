<?php 
#_________________________________________________________________________#
    /**                                                           
    * @Date: 18-aug-2015                                          
    * @Purpose: To edit payment method.                                             
    **/
?>
<script type="text/javascript">
	$(document).ready(function() { 
			//calender
			$( "#payment_date" ).datepicker({
				dateFormat: 'dd-mm-yy'
			});
		$('body div#ui-datepicker-div').css({'display':'none'});
	});
</script>
<!--  start content-table-inner -->
	<div id="content-table-inner">
	<?php echo $form->create('PaymentMethod',array('action'=>'edit','method'=>'POST','onsubmit' => '',"class"=>"login",'enctype'=>"multipart/form-data")); ?>
	<?php $session->flash(); ?>
	<?php $session->flash(); ?>
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form">
		<tr>
			<th valign="top">Name:</th>
			<td>
			<?php
				 echo $form->input("name",array("type"=>"text","class"=>"inp-form","label"=>false,"div"=>false));
            ?>
			<?php
				 echo $form->input("id",array("type"=>"hidden","class"=>"inp-form","label"=>false,"div"=>false));
			?>
			</td>
			<td>
			<?php
				 echo $form->input("modified",array("type"=>"hidden","class"=>"inp-form","label"=>false,"div"=>false));
            ?>
			</td>
		</tr>
        <tr>
          <th valign="top">Payment Method:</th>
			<td><?php    $options=array(
			          ''     => '--Select--',
					  'paypal'    =>  'Paypal', 
                      'self'    => 'Self'    
                      );
             echo $form->input("type",array('type'=>'select','options'=>$options,"class"=>"styledselect_form_1","label"=>false,"div"=>false));    
             ?>
            </td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
			<?php 
			  $abc = $this->data['PaymentMethod']['id'];
			echo $form->submit('Save',array('class'=>"form-submit",'div'=>false))."&nbsp;&nbsp;&nbsp;"; 
			echo $form->button('Cancel',array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/payment_methods/edit/$abc'")); 
			?>
			</td>
		    <td></td>
		</tr>
	</table>
	