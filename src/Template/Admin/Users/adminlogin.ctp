<!-- 
<a class="sticon ti-home pull-right" href="<?php echo $this->Url->build('/', true); ?>"> -->
<a class="sticon ti-home pull-right" href="http://doers.online/"> 

<span></span>
</a>
      <?php
	echo $this->Form->create('User', [ 'url'=>'/adminlogin','onsubmit' => '',"class"=>"form-horizontal form-material", 'id'=>'loginform','type' => 'post','prefix'=>'admin']);

 ?>
 <a href="javascript:void(0)" class="text-center db"><img src="<?php echo BASE_URL; ?>plugins/images/beforelogin_logo.png" alt="Home" class="logo"/ title="Doers are online for all your needs"><br/>
<h3><?php echo isset($agency) ?  $agency : "" ?></h3>

 <div class="form-group">
 <?php echo $this->Flash->render(); ?><br/><br/>   
       <div class="col-xs-12">
            <?php echo $this->Form->input("username",array("class"=>"form-control","label"=>false,"div"=>false,
            "placeholder"=>"Username","required"=>"required"));
        ?>
          </div>
         
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            
            <?php echo $this->Form->input("password",array("class"=>"form-control","label"=>false,"div"=>false," placeholder"=>"Password","required"=>"required")); ?>

          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Remember me </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
           <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
<?php //echo $this->Form->submit('Log In',array('class'=>"btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light",'div'=>false)); ?>
          </div>
          </div>

        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><!--<a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> --> </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p><!--Don't have an account?-->
           <b><?php //echo  $this->Html->link("Sign Up",
            //['controller' => 'users', 'action' => 'register' , 'prefix' //=> 'admin'],
           // ['class'=>'text-primary m-l-5','title'=>'Sign Up']); ?>
          </b></p>
 </div>
        </div>
     <?php echo $this->Form->end();?>

      <?php
  echo $this->Form->create('User', [ 'url'=>'/forgot_password','onsubmit' => '',"class"=>"form-horizontal", 'id'=>'recoverform','type' => 'post','prefix'=>'admin']);
  ?>
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">



 <div class="col-xs-12">
          <?php echo $this->Form->input("user_name",array("class"=>"form-control","label"=>false,"div"=>false,"placeholder"=>"Email","required"=>"required"));
             ?>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
              
        </div>

<?php echo $this->Form->end();?>