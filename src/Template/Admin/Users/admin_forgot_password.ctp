<!-- start loginbox -->
	<?php echo $form->create('User',array('action'=>'forgot_password','method'=>'POST','onsubmit' => '',"class"=>"login")); ?>
	<div id="loginbox">
 <h2 class="heading">Forgot Password</h2>
	<!--  start login-inner -->
	<div id="login-inner">
	<?php echo $session->flash(); ?>
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Email</th>
			<td><?php echo $form->input("user_name",array("class"=>"login-inp","label"=>false,"div"=>false));
             ?></td>
		</tr>
	
		<tr>
			<th></th>
			<td valign="top">&nbsp;</td>
		</tr>
		<tr>
			<th></th>
			<td><?php echo $form->submit('',array('class'=>"submit-login",'div'=>false)); ?>
			
			</td>
		</tr>
		<tr>
			<th></th>
			<td valign="top">&nbsp;</td>
		</tr>
		<tr>
			<th></th>
			<td>
				<?php echo $html->link("Back to Login",
						array('controller'=>'users','action'=>'login'),
						array('class'=>' forget_pass','title'=>'Login')
				   	); ?>
			</td>
		</tr>
		</table>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
 </div>
 <!--  end loginbox -->
<?php echo $form->end();?>