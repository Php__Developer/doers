<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function(){
		$('#allUsrStatus').hide();
		$( "#deadline" ).datepicker({
			  dateFormat: 'MM dd, yy',
			  minDate: 0
		 });
		});
	function show_div(){
		$('#allUsrStatus').fadeIn('slow');
		$('#UsrStatus').attr('onClick','hide_div();');
	}
	function hide_div(){
	$('#allUsrStatus').fadeOut('slow');
		$('#UsrStatus').attr('onClick','show_div();');
	}
	function validateForm()
	{
		var data = $('#deadline').val();
		if(data ==""){
			alert("Please Select date.");
			return false;
		}
		else{
			href=base_url+'users/addCurrentStatus/';
				$.post(href,{current_status:data},function(data){
					if(data != 0){
						var message = "<div align='center' style='color:green; font-size:12px;padding-bottom: 1em; padding-right: 55em;'>Your Status has been updated successfully</div>";
						$('#status_inst').find('i').html(data);
						$('#status').attr('style','border:1px solid black;');	
					}
					else{
						var message = "<div align='center' style='font-size:12px;color:red;'>Sorry, Your Status has not been updated!</div>";
					}
					$('#result_msg_div').html(message);
					$('#result_msg_div').show();
					setTimeout(function() { $('#result_msg_div').fadeOut('slow'); }, 1000);
				});
		}
	
	}
	function clearExpiredval(id){
	var html_value=$('#'+id).val();
			$('#'+id).attr('style','border:2px solid red;');	
	}
	function clearDefaultval(id){
	var html_value=$('#'+id).val();
			$('#'+id).attr('style','border:1px solid black');
	}
</script>
          <div style="font-size:12px;color:blue;">
	       <?php  

				if($noticeficationCout!=0){
					echo "you have performance comment  ".$noticeficationCout;
				}
		    ?>
	</div>
	
	<table border="0" cellpadding="0" cellspacing="0"  id="id-form" class="hellStatus" width="100%">
	<div class="mystatsdiv" >
	
		<tr style="border-top:1px solid #C0C0C0;border-right:1px solid #C0C0C0;border-left:1px solid #C0C0C0; padding:5px;padding-bottom:0px;">
			<td width="94%">
			<div id='result_msg_div' style='padding-right:58px;'></div>
				<?php 
					$today_date_only = date("Y-m-d");
					$resultdata['freefrom'];
					$freefrom = ($resultdata['freefrom']>$today_date_only) ? date("M d,Y",strtotime($resultdata['freefrom'])) :  date("M d,Y");
					echo "<span style='color:#92B22C;font-weight:bold;padding-right:1em;'>I will be free from : </span>";
					echo $form->input("freefrom",array("type"=>"text",'class'=>"inp-form","id"=>"deadline",'div'=>false,"label"=>false,"readonly"=>true,"value"=>$freefrom));
				?>
				<div style="padding-right:40em;float:right;">
				<?php
					echo $form->input("Update",array('class'=>"form-submit",'div'=>false,'onclick'=>'return validateForm();',"label"=>false)); 
				?>
				</div>
			</td>
				<td valign="bottom">
				
			</td>
		</tr>
		<tr style="border-bottom:1px solid #C0C0C0;border-right:1px solid #C0C0C0;border-left:1px solid #C0C0C0;padding:5px;padding-top:0px;">
			<td colspan="2">
			<div id="status_inst">
				<?php echo "Note : It will help management to schedule your time properly." ?>
			</div>
			</td>
		</tr>
		
	</div>
		<tr>
		<td colspan="2">
		<a href="javascript:void(0)" style="color:red;font-weight:bold;" onclick='show_div();' id="UsrStatus" >Status Alert(s) [+ /-] : <font color="blue"> Click to view resource wise alert(s)</font></a> 
		<div id ="allUsrStatus" style="margin-top:5px;">
			<div class="noStatuslbl" style="float:left;padding-left:0px;"><font color = "black">No Status Update from last 24 hours: </font><br/>
			<?php   $previousDay = date("Y-m-d H:i:s ",time()-(24*3600)); 
				$i = 0; 
				$c1 = count($userData); 
				$a =$b=1;
				if($c1>0) {
					foreach($userData as $users){
						if($users['User']['status_modified'] < $previousDay) { ?>
							<?php if($i <= ceil($c1/2)) {
									if($a==1){
										echo "<div style='float:left;width:50%'>";
									}
									echo "<span style='font-size:12px;'>".$users['User']['first_name'].' '.$users['User']['last_name']."</span><br/>";
									$a++;
							}else{
								if($b==1){
									echo "</div><div style='float:right;width:50%'>";
								}
								echo "<span style='font-size:12px;'>".$users['User']['first_name'].' '.$users['User']['last_name']."</span><br/>";
								$b++;
							} $i++;
						}
					} 
					echo '<div style="clear:both"></div></div>';
				}else{
					echo "<span style='font-size:14px;color:#000000;'> No Users!</span><br/>";
				}  ?>
			</div>
			<div class="noticlbl">
			<font color = "black"> Nominal Tickets, Seems No Work: </font> <br/>
				<?php $j = 0; $a =$b=1; $c2 = count($allNominalTicketUser);
				if($c2>0) {
				foreach($allNominalTicketUser as $key=>$val){
					 if($j <= ceil($c2/2)) {
						if($a==1){
								echo "<div style='float:left;width:50%'>";
							}
						echo "<span style='font-size:12px;'>".$val."</span><br/>"; 
						$a++;
					}else{
						if($b==1){
							echo "</div><div style='float:right;width:50%'>";
						}
						echo "<span style='font-size:12px;'>".$val."</span><br/>"; 
						$b++;
					}  $j++;
				}
				echo '<div style="clear:both"></div></div>';
				} else{
					echo "<span style='font-size:14px;color:#000000;'> No Users! </span><br/>";
				}?>
			
			
			</div>
			<div class="vooaplbldiv">
			<font color = "black">On Leave (Date <?php echo date("d-m-Y",strtotime($attendDate)); ?>): </font><br/>
				<?php $k = 0;  $a =$b=1; $c3 = count($onLeaveUsers); 
				if($c3>0) {
				foreach($onLeaveUsers as $key=>$val){
						if($k <= ceil($c3/2)) {
						if($a==1){
									echo "<div style='float:left;width:50%'>";
								}
							echo "<span style='font-size:12px;'>".$val."</span><br/>";
							$a++;
						}  else{
						if($b==1){
								echo "</div><div style='float:right;width:50%'>";
							}
						echo "<span style='font-size:12px;'>".$val."</span><br/>";
						$b++;
					} $k++; 
				} 
					echo '<div style="clear:both"></div></div>';
				} else{
					echo "<span style='font-size:14px;color:#000000;'> No Users !</span><br/>";
				}	?>
			
			</div>
			<div style="clear:both;"></div>
		</div>
		</td>
		</tr>
	</table>
		<?php

			echo $form->end();
			echo $resultdata['StaticPage']['content']; ?>

