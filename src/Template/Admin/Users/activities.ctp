
<link href="<?php echo BASE_URL; ?>css/jquery-ui.css" id="theme"  rel="stylesheet">
 <!-- <link href="<?php echo BASE_URL; ?>css/ui_boot.css" id="theme"  rel="stylesheet"> -->
 <!-- <link href="<?php echo BASE_URL; ?>js/tpt/fullscreen/release/style.css" id="theme"  rel="stylesheet"> -->
 <link href="<?php echo BASE_URL; ?>js/tpt/highlight/styles/github.css" id="theme"  rel="stylesheet">
 <!-- <link href="<?php echo BASE_URL; ?>js/tpt/ng-ckeditor/ng-ckeditor.css" id="theme"  rel="stylesheet"> -->
  <link href="<?php echo BASE_URL; ?>js/tpt/editable/css/xeditable.css"  rel="stylesheet">
 <!--  <link href="<?php echo BASE_URL; ?>js/tpt/angular-block-ui-master/dist/angular-block-ui.min.css"  rel="stylesheet"> -->
  <!-- <link data-require="angular-block-ui@*" data-semver="0.1.1" rel="stylesheet" href="https://cdn.rawgit.com/McNull/angular-block-ui/v0.1.1/dist/angular-block-ui.min.css" /> -->
<!-- <link href="<?php echo BASE_URL; ?>css/aria.css" id="theme"  rel="stylesheet">
 -->
  <?php $url =  $this->Url->build('/', true);?>
     <div class="row"  ng-app="createTree" ng-controller="createCtr" ng-init ="name='tickindex';base_url='<?php echo $url;?>'; currentuser='<?php echo $session_info[0] ;?>'" block-ui="main" class="block-ui-main">
		<!-- <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-primary myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <i class="ti-user"></i>&^&success.message&^&<a href="#" class="closed" ng-click ="closetheerror('successwrapper')">&times;</a> </div> -->
		<div class="alert alert-loading myadmin-alert-top alerttop" ng-class="fields.successwrapper"> <span class="succesfont">&^&success.message&^& </span></div>
    <div class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert-danger myadmin-alert-top alerttop" ng-class="fields.errorswrapper"> <i class="fa fa-bolt"></i>&^&errors.message&^&<a href="#" class="closed" ng-click ="closetheerror('errorswrapper')">&times;</a> </div>
        <!-- Left sidebar -->
        <div class="col-md-12">
          <div class="white-box">
            <!-- row -->
           
            <div class="row">
              <div class="col-lg-2 col-md-3  col-sm-12 col-xs-12 inbox-panel" >
                <div> <!-- <a href="#" class="btn btn-custom btn-block waves-effect waves-light">Add New Ticket</a> -->
                    
                  <div class="list-group mail-list m-t-20">
                      <a href="#" class="list-group-item all_tickets" ng-click="get_data('all',1,'none','none','yes')" ng-class="active.all">All Activities<span class="label label-rouded label-default pull-right" ng-bind="count.all"></span></a> 
                      <a href="#" class="list-group-item opn_tickets" ng-click="get_data('open',0,'none','none','yes')" ng-class="active.open">Tickets Activities <span class="label label-rouded label-open pull-right" ng-bind="count.opened"></span></a>
                      <a href="#" class="list-group-item clsd_tickets" ng-click="get_data('close',0,'none','none','yes')" ng-class="active.close">Project Activities <span class="label label-rouded label-closed pull-right" ng-bind="count.closed"></span></a>
                      <!-- 
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('inprogress',0,'none','none','yes')" ng-class="active.inprogress">In Progress<span class="label label-inprogress label-info pull-right" ng-bind="count.inprogress">0</span></a>
                      <a href="#" class="list-group-item nprgress_tickets" ng-click="get_data('expire',0,'none','none','yes')" ng-class="active.expire">Expired<span class="label label-danger label-info pull-right" ng-bind="count.expired">0</span></a> -->
                      <!-- <a href="#" class="list-group-item">Trash <span class="label label-rouded label-default pull-right">55</span></a>  -->
                  </div>
                  <h3 class="panel-title m-t-40 m-b-0">Tags</h3>
                  <hr class="m-t-5">
                  <div class="list-group b-0 mail-list"> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-info m-r-10"></span>Medium</a>
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-warning m-r-10"></span>Urgent</a>
                    <!-- <a href="#" class="list-group-item"><span class="fa fa-circle text-purple m-r-10"></span>Private</a> -->
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-danger m-r-10"></span>Emergency</a> 
                    <a href="#" class="list-group-item"><span class="fa fa-circle text-success m-r-10"></span>Normal</a> 
                 </div>
                </div>
              </div>
              <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12 mail_listing">
                <div class="inbox-center">
                
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th width="30"><div class="checkbox m-t-0 m-b-0 ">
                            <!-- <input id="checkbox0" type="checkbox" class="checkbox-toggle" value="check all">
                            <label for="checkbox0"></label> -->
                          </div></th>
                        <th colspan="5"> <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle waves-effect waves-light m-r-5" data-toggle="dropdown" aria-expanded="false"> Sort <b class="caret"></b> </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#fakelink" ng-click="get_data('selected','selected','modified','none','selected')" >Modified Date</a></li>
                              <li><a href="#fakelink" ng-click="get_data('selected','selected','deadline','none','selected')">Deadline</a></li>
                              <li class="tic_id"><!-- <a href="#fakelink" ng-click="get_data('selected',0,'ticketno')">Ticket No.</a> -->
                                    <!-- <a href="#" id="inline-firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Ticket ID" data-title="Enter Ticket ID">Ticket ID</a> -->
                                    <a href="#" editable-text="user.name" onbeforesave="updateUser($data)">
                                      &^& ticket.id || 'TicketID' &^&
                                    </a>
                              </li>
                             <!--  <li class="divider"></li> -->
                             <!--  <li><a href="#fakelink">Separated link</a></li> -->
                            </ul>
                          </div>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default waves-effect waves-light  dropdown-toggle" data-toggle="dropdown" aria-expanded="false" ng-click="get_data('selected','selected','selected','none','selected')"> <i class="fa fa-refresh"></i> </button>
                          </div></th>
                        <th class="hidden-xs" width="100" colspan="2"><div class="btn-group pull-right">
                            <button type="button" class="btn btn-default waves-effect" ng-class="prebtnclass" ng-click ="!tiggerprev || get_data('selected','selected','selected','none','no','pre')"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default waves-effect" ng-class="nxtbtnclass" ng-click ="!tiggernxt || get_data('selected','selected','selected','none','no','nxt')"><i class="fa fa-chevron-right"></i></button>
                          </div></th>
                      </tr>
                    </thead>
                    <tbody  ng-model="t_data" class="alltickets">
                    <!-- 
                     <tr class="unread">
                        <td><div class="checkbox m-t-0 m-b-0">
                            <input  type="checkbox">
                            <label for="checkbox0"></label>
                          </div></td>
                        <td class="hidden-xs"><i class="fa fa-star text-warning"></i></td>
                        <td class="hidden-xs">Genelia Roshan</td>
                        <td class="max-texts"><a href="inbox-detail.html">Lorem ipsum perspiciatis unde omnis iste natus error sit voluptatem</a></td>
                        <td class="hidden-xs"><i class="fa fa-paperclip"></i></td>
                        <td class="text-right"> May 13 </td>
                      </tr> -->
                      <!-- "data-toggle"=>"tooltip", "data-placement"=>"top" ,"data-original-title"=>"Go Back To Tickets List" -->
                       <tr class="unread" ng-repeat="(index, x) in t_data track by $index">
                            <td >
                            <i ng-class="{true:'ti-angle-double-up',false:'ti-angle-double-down'}[x.from==userID]" data-toggle="tooltip"  data-placement= "top" title="&^& x._id &^&"></i>
                            </td>
                           <!-- <td class="hidden-xs" title="Click To Edit"><i class="fa fa-pencil" title="Edit" ng-click="ticketresponse(x.id)"></i></td> -->
                           <td class="hidden-xs" ng-bind="x.to_name" title="To: &^& x.to_name &^&"></td>
                           <td class="max-texts"> 
                          </td>
                          <td class="hidden-xs" title="&^& x.title &^&">
                              <a href="#" />
                          <span class="label m-r-10" title="Normal" ng-class="{information:'label-success', alert:'label-warning', 'emergency':'label-danger', 'medium':'label-info'}[x.type]" ng-bind="x.type" ></span>
                              <span ng-class="{'1':'font-open', '2':'font-inprogess', '3' :'font-close'}[x.entity]"> &^& x.logtext | limitTo:25 &^& </span>
                          </td>
                           <td class="hidden-xs" ng-bind="x.from_name" title="From: &^& x.from_name &^&"></td>
                          <td class="hidden-xs"><!--< i class="fa fa-bell" title="Set Reminder" ng-click="setreminder(x._id)"></i> --></td>
                          <td class="text-right"  title="Activity Done At" >
                          	<span ng-class="{true:'font-expired',false:'none'}[x.isexpired=='yes']" > &^& x.created_at  | date:'medium':'+0000' &^&</span>

                          </td>
                       </tr>
             
                    </tbody>
                  </table>

                </div>
                <div class="row default_hidden" ng-class="norecordsmsg">
                  <div class="col-md-12">
                    <div class="white-box">
                      <h3>Result</h3>
                      No Records Found! 
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-7 m-t-20">   &^& ((totalrecords >  0 ) ? 'Showing '+ currentrecodsfrom + ' - ' + currentrecodstill + ' of ' + totalrecords : "" )&^&  </div>
                  <div class="col-xs-5 m-t-20">
                    <div class="btn-group pull-right">
                      <button type="button" class="btn btn-default waves-effect" ng-class="prebtnclass" ng-click ="!tiggerprev || get_data('selected','selected','selected','none','no','pre')"><i class="fa fa-chevron-left"></i></button>
                      <button type="button" class="btn btn-default waves-effect" ng-class="nxtbtnclass" ng-click ="!tiggernxt || get_data('selected','selected','selected','none','no','nxt')"><i class="fa fa-chevron-right"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
     <script src="<?php echo BASE_URL; ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
   <!--    <script src="<?php echo BASE_URL; ?>plugins/bower_components/blockUI/jquery.blockUI.js"></script>  -->
       <?php 
    echo  $this->Html->script(array('jquery-3','angular/src/angular/angular','angular/src/angular/angular-animate','angular/src/angular/angular-sanitize','angular/src/ui_boot','angular/ang_mat','angular/activities','angular/jquery-ui/jquery-ui','angular/aria','angular/ui/src/sortable','tpt/sweet-alert/SweetAlert','tpt/editable/js/xeditable','tic_index'));
    ?>
     <!-- <script data-require="angular-block-ui@*" data-semver="0.1.1" src="https://cdn.rawgit.com/McNull/angular-block-ui/v0.1.1/dist/angular-block-ui.min.js"></script> -->
      <!-- <script src="<?php echo BASE_URL; ?>"></script> -->
      <!-- /.row -->