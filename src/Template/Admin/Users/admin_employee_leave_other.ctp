<?php
//echo "<pre>";
//print_r($Emp_record['0']);


?>
<style type="text/css">
    
    .comment_box{
        
        
        width: 150px !important;
        height: 40px !important;
    }
</style>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	$(document).ready(function() {
			alphanum();
	});
	function alphanum(){
		$('.numeric').keyup(function () {
				if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
				   this.value = this.value.replace(/[^0-9\.]/g, '');
				}
			});
	}
	function editRecord(id){
		spantoInput('january_'+id);
		spantoInput('february_'+id);
		spantoInput('march_'+id);
		spantoInput('april_'+id);
		spantoInput('may_'+id);
		spantoInput('june_'+id);
		spantoInput('july_'+id);
                spantoInput('august_'+id);
                spantoInput('september_'+id);
                spantoInput('october_'+id);
                spantoInput('november_'+id);
                spantoInput('december_'+id);
                 spantoInput('comment_'+id);
                 $('#'+'comment_'+id).removeAttr("class");
                   $('#'+'comment_'+id).attr("class",'comment_box');
		changeLink('edit_'+id);
			$('body div#ui-datepicker-div').css({'display':'none'});
		alphanum();
	}
	function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="numeric" style="width:40px;height:20px;"/>');
                
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}
	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}
	function editData(id){
		if(checkLimit($('#mon_'+id).val()) || checkLimit($('#tue_'+id).val()) || checkLimit($('#wed_'+id).val()) || checkLimit($('#thur_'+id).val()) || checkLimit($('#fri_'+id).val()) || checkLimit($('#sat_'+id).val()) || checkLimit($('#sun_'+id).val()))	{  
			return false;
		}
			var postdata = {	'id':id,
										'project_id' : $('#projectid_'+id).val(), 
										'january_leave' : $('#january_'+id).val(), 
										'february_leave':$('#february_'+id).val(),
										'march_leave':$('#march_'+id).val(),
										'april_leave' : $('#april_'+id).val(),
										'may_leave':$('#may_'+id).val(),
										'june_leave':$('#june_'+id).val(),
										'july_leave':$('#july_'+id).val(),
										'august_leave' : $('#august_'+id).val(),
										'september_leave' : $('#september_'+id).val(), 
                                                                                'october_leave' : $('#october_'+id).val(), 
                                                                                'november_leave' : $('#november_'+id).val(), 
                                                                                'december_leave' : $('#december_'+id).val(), 
										'notes' : $('#comment_'+id).val(),
									};
		if($.active>0){
		}else{							
			$.post(base_url+'admin/users/update_leave',postdata,function(data){
				if(data==1){
					get_total(id);
					updateRecord(id);
					$("#success").html("Billing has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 5000);
				}else if(data=='error'){
					alert("Billing entry for this week already exists!");
				}
			});
		}
	}
	function get_total(id)
	{
		var total  = 0; 
		var left = 0;
		total  = parseFloat($('#mon_'+id).val()) + parseFloat($('#tue_'+id).val()) + parseFloat($('#wed_'+id).val()) + parseFloat($('#thur_'+id).val()) +  parseFloat($('#fri_'+id).val()) + parseFloat($('#sat_'+id).val()) + parseFloat($('#sun_'+id).val()) ;
		if(isNaN(total)){
			total = 0.00;
		}
		left = parseFloat($('#todo_'+id).html()) - parseFloat(total);
		$('#tot_'+id).html(total.toFixed(2));
		$('#left_'+id).html(left.toFixed(2));
	}
	function updateRecord(id){
		inputToSpan('january_'+id);
		inputToSpan('february_'+id);
		inputToSpan('march_'+id);
		inputToSpan('april_'+id);
		inputToSpan('may_'+id);
		inputToSpan('june_'+id);
		inputToSpan('july_'+id);
                inputToSpan('august_'+id);
                inputToSpan('september_'+id);
                inputToSpan('october_'+id);
                inputToSpan('november_'+id);
                inputToSpan('december_'+id);
                inputToSpan('comment_'+id);
                
                
		changeLinkBack('edit_'+id);
	}
	function checkLimit(val){
		if(parseFloat(val) > parseFloat('24')){
			$('#error_msg').html("Daily billing hours cannot be greater than 24");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return true;
		}
	}
</script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/colortip-1.0-jquery.css"/>


<style>
    .salary_table{
        
        border-color:#999999; 
        width: 1000px;
        
    }
.salary_table td{

border: 1px solid #D2D2D2;
    padding: 10px 5px 10px 10px;
font-size:12px;
}
.salary_table th{

padding:3px;
border-color:#999999; 
font-size:12px;

}
</style>


<h3><?php echo "Leave Calculation"."&nbsp;&nbsp;&nbsp;".date('F Y');  ?></h3>
<table border="1" cellpadding="4" class="salary_table">

<thead>
<tr>
    <th style="width:200px">Name</th> 
<?php
for ($x=2; $x<12+2; $x++) {
?>

<th><?php  echo date('F', mktime(0,0,0,$x,0,0));   ?></th>


<?php  }   ?>
<th style="width:200px">Comment</th><th style="width:200px">Total Leave</th><th style="width:200px">Options</th>
</tr>
 </thead> 
<?php
$grand_total=array();



$cmp=0;

foreach($Emp_record as $emp_detail){
 $leave=array();

 
 $user=$Emp_record[$cmp]['User']['id']; 
 $user1=$Emp_record[$cmp+1]['User']['id']; 
 $cmp++;
 if($user!=$user1)
{

?>

<tr><td style="width:200px"><?php 


echo $emp_detail['User']['first_name']." ".$emp_detail['User']['last_name'];   ?></td>

    <td><span id="january_<?php echo $user; ?>"><?php 
    
    $leave[]=$emp_detail['Leave_other']['0']['january'];
    if(!empty($emp_detail['Leave_other']['0']['january'])){echo $emp_detail['Leave_other']['0']['january']   ;}else{ echo "0";}  ?></td>
    <td><span id="february_<?php echo $user; ?>"><?php 
     $leave[]=$emp_detail['Leave_other']['0']['february'];
    if(!empty($emp_detail['Leave_other']['0']['february'])){echo $emp_detail['Leave_other']['0']['february']   ;}else{ echo "0";}
   
    
    
    ?></td>
    <td><span id="march_<?php echo $user; ?>"><?php 
     $leave[]=$emp_detail['Leave_other']['0']['march'];
      if(!empty($emp_detail['Leave_other']['0']['march'])){echo $emp_detail['Leave_other']['0']['march']   ;}else{ echo "0";}
    
      ?></td>
    <td><span id="april_<?php echo $user; ?>"><?php 
     $leave[]=$emp_detail['Leave_other']['0']['april'];
     if(!empty($emp_detail['Leave_other']['0']['april'])){echo $emp_detail['Leave_other']['0']['april']   ;}else{ echo "0";}
    
     ?></td>
     <td><span id="may_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['may'];
      if(!empty($emp_detail['Leave_other']['0']['may'])){echo $emp_detail['Leave_other']['0']['may']   ;}else{ echo "0";}  ?></td>
     <td><span id="june_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['june'];
      if(!empty($emp_detail['Leave_other']['0']['june'])){echo $emp_detail['Leave_other']['0']['june']   ;}else{ echo "0";}  ?></td>
     <td><span id="july_<?php echo $user; ?>"><?php 
     
      $leave[]=$emp_detail['Leave_other']['0']['july'];
      if(!empty($emp_detail['Leave_other']['0']['july'])){echo $emp_detail['Leave_other']['0']['july']   ;}else{ echo "0";}  ?></td>
     <td><span id="august_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['august'];
      if(!empty($emp_detail['Leave_other']['0']['august'])){echo $emp_detail['Leave_other']['0']['august']   ;}else{ echo "0";} ?></td>
     <td><span id="september_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['october'];
      if(!empty($emp_detail['Leave_other']['0']['october'])){echo $emp_detail['Leave_other']['0']['september']   ;}else{ echo "0";} ?></td>
     <td><span id="october_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['october'];
      if(!empty($emp_detail['Leave_other']['0']['october'])){echo $emp_detail['Leave_other']['0']['october']   ;}else{ echo "0";}  ?></td>
     <td><span id="november_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['november'];
      if(!empty($emp_detail['Leave_other']['0']['november'])){echo $emp_detail['Leave_other']['0']['november']   ;}else{ echo "0";}  ?></td>
     <td><span id="december_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['december'];
      if(!empty($emp_detail['Leave_other']['0']['december'])){echo $emp_detail['Leave_other']['0']['december']   ;}else{ echo "0";} ?></td>
     <td width="100"><span id="comment_<?php echo $user; ?>"><?php 
      $leave[]=$emp_detail['Leave_other']['0']['comment'];
      if(!empty($emp_detail['Leave_other']['0']['comment'])){echo $emp_detail['Leave_other']['0']['comment']   ;}else{ echo "";}?></td>

             
<td><?php 

echo array_sum($leave);
 ?></td>
<td><?php 
echo "<div style='float:right'>";

echo $html->link("","javascript:void(0)",array('class'=>'icon-1 info-tooltip','title'=>'Edit','id'=>'edit_'.$user,'onClick'=>"javascript : editRecord(".$user.");"));

echo "</div>";
  ?></td>
</tr>

<?php  } } ?>

</table>


<script type="text/javascript" src="<?php echo BASE_URL; ?>js/colortip-1.0-jquery.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/script_tooltip.js"></script>


