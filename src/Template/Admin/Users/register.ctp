

 <?php echo $this->Flash->render(); ?>
<section id="wrapper" class="">
  <div class="login-box">
    <div class="white-box">
<?php
      echo $this->Form->create('User', [ 'url'=>'/register','onsubmit' => '',"class"=>"form-horizontal form-material", 'id'=>'loginform','type' => 'post','prefix'=>'admin']);

      ?>
        

        <h3 class="box-title m-b-20">Sign Up</h3>

 <div class="form-group ">
          <div class="col-xs-12">
           <?php echo $this->Form->input("agency_name",array("class"=>"form-control","label"=>false,"div"=>false,
            "placeholder"=>"Agency Name", "required"=>"required"));
            ?>
            
           <?php if(isset($errors['agency_name'])){
                                    echo $this->General->errorHtml($errors['agency_name']);
                                } ;?>
          </div>
        </div>


        <div class="form-group ">
          <div class="col-xs-12">
          <?php echo $this->Form->input("first_name",array("class"=>"form-control","label"=>false,"div"=>false,
            "placeholder"=>"First Name", "required"=>"required"));
        ?>
       
           <?php if(isset($errors['first_name'])){
                                    echo $this->General->errorHtml($errors['first_name']);
                                } ;?>
        </div>
        </div>


        <div class="form-group ">
          <div class="col-xs-12">
             <?php echo $this->Form->input("last_name",array("class"=>"form-control","label"=>false,"div"=>false,
            "placeholder"=>"Last Name", "required"=>"required" ));
        ?>
           <?php if(isset($errors['last_name'])){
                                    echo $this->General->errorHtml($errors['last_name']);
                                } ;?>

          </div>
        </div>

        
          <div class="form-group ">
          <div class="col-xs-12">
           <?php echo $this->Form->input("username",array("class"=>"form-control","label"=>false,"div"=>false,
            "placeholder"=>"Email", "required"=>"required" ));
        ?>
                <?php if(isset($errors['username'])){
                                    echo $this->General->errorHtml($errors['username']);
                                } ;?>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
          <?php echo $this->Form->input("password",array("class"=>"form-control","label"=>false,"div"=>false," placeholder"=>"Password", "required"=>"required")); ?>
           <?php if(isset($errors['password'])){
                                    echo $this->General->errorHtml($errors['password']);
                                } ;?>
            
          </div>
        </div>

<!--         <div class="form-group">
          <div class="col-xs-12">
           <?php echo $this->Form->input("password",array("class"=>"form-control","label"=>false,"div"=>false," placeholder"=>"Confirm Password",'id'=>'confirm_password')); ?>
         <?php if(isset($errors['confirm_password'])){
                                    echo $this->General->errorHtml($errors['confirm_password']);
                                } ;?>
          </div>
        </div>
 -->
      <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox" name="is_agree" required="required">
              <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
            </div>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" id="signup" name="submit" value="submit">Sign Up</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Already have an account?<b><?php echo  $this->Html->link("Sign In",
            ['controller' => 'users', 'action' => 'login' , 'prefix' => 'admin'],
            ['class'=>'text-primary m-l-5','title'=>'Sign In']); ?></b></a></p>
          </div>
        </div>
        <?php echo $this->Form->end();?>
    
    </div>
  </div>









</section>



