   <style>
     .dashboar_wrapper{
      min-height: 480px;
     }
   </style>

   <div class="row">
   <input type="hidden" name="orgin" class="emporigin" value="<?php echo $userinfo['employee_code'] ?>" >
   <input type="hidden" name="page" class="currentlocation" value="<?php echo $page ?>" >
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="white-box" >
            <div class="row row-in">
              <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic" ></i>
                    <h5 class="text-muted vb">Open Tickets</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-danger"><?php $data=$this->General->mytickets();
                    echo count($data['open']);?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
                    <h5 class="text-muted vb">In Progress Tickets</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-megna"><?php $data=$this->General->mytickets();
                    echo count($data['inprogress']);?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6 row-in-br">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
                    <h5 class="text-muted vb">Closed Tickets</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-primary"><?php  $data=$this->General->mytickets();
                    echo count($data['closed']);?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6  b-0">
                <div class="col-in row">
                  <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                    <h5 class="text-muted vb">Total PROJECTS</h5>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <h3 class="counter text-right m-t-15 text-success"><?php echo $total_projects;?></h3>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--row -->
         <div class="row">
        <div class="col-md-7 col-lg-9 col-sm-12 col-xs-12">
          <div class="white-box dashboar_wrapper">
           <h3 class="box-title">DashBoard</h3>
            <ul class="list-inline text-right">
              <li>
                <!--<h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>iPhone</h5>-->
              </li>
              <li>
               <!-- <h5><i class="fa fa-circle m-r-5" style="color: #fb9678;"></i>iPad</h5>-->
              </li>
              <li>
                <!--<h5><i class="fa fa-circle m-r-5" style="color: #9675ce;"></i>iPod</h5>-->
              </li>
            </ul>
            <!--<div id="morris-area-chart" style="height: 340px;"></div>-->
            <div id="" style=""><?php  echo $dashcontent['value']; ?>
               </div>
          </div>
        </div>
        <div class="col-md-5 col-lg-3 col-sm-6 col-xs-12">
          <div class="bg-theme-dark m-b-15">
            <div class="row weather p-20">
              <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 m-t-40">
                <h3>&nbsp;</h3>
                <h1><?php echo $onalert_projects;?><sup></sup></h1>
                <p class="text-white">Project(s) On Alert</p>
              </div>
              <div class="col-md-6 col-xs-6 col-lg-6 col-sm-6 text-right"> 
              <?php if($onalert_projects > 0){ ?>
              		  <i class="fa fa-exclamation"></i>
              <?php }else { ;?>
              		<i class="fa fa-check "></i>
              <?php } ;?>
              <!--<i class="wi wi-day-cloudy-high"></i>-->
            

              <br/>
                <br/>
                <b class="text-white">Projects Status</b>
                <p class="w-title-sub"><?php echo date('F d');?></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5 col-lg-3 col-sm-6 col-xs-12">
          <div class="bg-theme m-b-15">
            <div id="myCarouse2" class="carousel vcarousel slide p-20">
              <!-- Carousel items -->
              <?php 
              $options = $this->General->dynamic_notification();
                $notes=array();
                foreach($options as $val){ 
                $notice=$val['value'];}
                $notes=explode(',',$notice);
              ;?>
              <div class="carousel-inner ">
              <?php
              for($i = 0; $i < count($notes); $i++){
               if($i == 0){?>
                <div class="active item">
                <?php }else {?>
                <div class="item">
                <?php }?>
                  <h3 class="text-white"><?php echo $notes[$i];?></h3>
                  <div class="twi-user"><!--<img src="../plugins/images/users/hritik.jpg" alt="user" class="img-circle img-responsive pull-left">-->
                    <h4 class="text-white m-b-0">Regards</h4>
                    <p class="text-white">Team Doers</p>
                  </div>
                </div>
                <?php }?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php  echo  $this->Html->script(array('jquery-3','dashboard.js'));?>
