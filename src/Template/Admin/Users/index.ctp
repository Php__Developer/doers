
 <!-- start of part one -->
  <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title"></h4>
        </div>
        </div>

      <div class="row">
        <div class="col-lg-6 col-sm-12 col-xs-12">
          <div class="collapse m-t-15" id="pgr1" aria-expanded="true">
            <pre class="line-numbers language-javascript m-t-0"><code><b>Use below code & put in column</b><br/>
                  &lt;div class="white-box"&gt;
                      &lt;h3 class="box-title"&gt;NEW CLIENTS&lt;/h3&gt;
                      &lt;ul class="list-inline two-part"&gt;
                      &lt;li&gt;&lt;i class="icon-people text-info"&gt;&lt;/i&gt;&lt;/li&gt;
                      &lt;li class="text-right"&gt;&lt;span class="counter"&gt;23&lt;/span&gt;&lt;/li&gt;
                      &lt;/ul&gt;
                  &lt;/div&gt;</code></pre>
          </div>
          <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">NEW CLIENTS</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-people text-info"></i></li>
                  <li class="text-right"><span class="counter">23</span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">NEW Projects</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-folder text-purple"></i></li>
                  <li class="text-right"><span class="counter">169</span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Open Projects</h3>
                <ul class="list-inline two-part">
                  <li><i class="icon-folder-alt text-danger"></i></li>
                  <li class="text-right"><span class="counter">311</span></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="white-box">
                <h3 class="box-title">Total Tickets</h3>
                <ul class="list-inline two-part">
                  <li><i class="ti-wallet text-success"></i></li>
                  <li class="text-right"><span class="">2.4k</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-sm-12 col-xs-12">
          <div class="collapse m-t-15" id="pgr2" aria-expanded="true">
            <pre class="line-numbers language-javascript m-t-0"><code><b>Use below code & put in column</b><br/>
        &lt;div class="carousel-inner"&gt;
            &lt;div class="active item"&gt;
              &lt;div class="overlaybg"&gt;&lt;img src="<?php echo BASE_URL; ?>plugins/images/news/slide1.jpg"/&gt;&lt;/div&gt;
              &lt;div class="news-content"&gt;&lt;span class="label label-danger label-rounded"&gt;Primary&lt;/span&gt;
                &lt;h2&gt;..content here...&lt;/h2&gt;
                &lt;a href="#"&gt;Read More&lt;/a&gt;&lt;/div&gt;
            &lt;/div&gt;
            &lt;div class="item"&gt;
              &lt;div class="overlaybg"&gt;&lt;img src="<?php echo BASE_URL; ?>plugins/images/news/slide1.jpg"/&gt;&lt;/div&gt;
              &lt;div class="news-content"&gt;&lt;span class="label label-primary label-rounded"&gt;Primary&lt;/span&gt;
                &lt;h2&gt;...content here...&lt;/h2&gt;
                &lt;a href="#"&gt;Read More&lt;/a&gt;&lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;</code></pre>
          </div>
          <div class="news-slide m-b-15">
            <div class="vcarousel slide">
              <!-- Carousel items -->
              <div class="carousel-inner">
                <div class="active item">
                  <div class="overlaybg"><img src="<?php echo BASE_URL; ?>plugins/images/news/1.png"/></div>
                  <div class="news-content"><!-- <span class="label label-danger label-rounded">Primary</span> -->
                    <h2>NO BID. NO GIG. No Fuss Nothing!! Payments on delivery Only</h2>
                    <!-- <a href="#">Read More</a> --></div>
                </div>
                <div class="item">
                  <div class="overlaybg"><img src="<?php echo BASE_URL; ?>plugins/images/news/2.png"/></div>
                  <div class="news-content"><!-- <span class="label label-primary label-rounded">Primary</span> -->
                    <h2>No Dummy Freelancers. Legal Agencies Only.</h2>
                    <!-- <a href="#">Read More</a> --></div>
                </div>
                <div class="item">
                  <div class="overlaybg"><img src="<?php echo BASE_URL; ?>plugins/images/news/3.png"/></div>
                  <div class="news-content"><!-- <span class="label label-success label-rounded">Primary</span> -->
                    <h2> Track Your Project health, Get Instant Notifications of any new activity! .</h2>
                    <!-- <a href="#">Read More</a> --></div>
                </div>
                  <div class="item">
                  <div class="overlaybg"><img src="<?php echo BASE_URL; ?>plugins/images/news/4.png"/></div>
                  <div class="news-content"><!-- <span class="label label-success label-rounded">Primary</span> -->
                    <h2> Know Your Project to avoid further complexities. Get Free Consultancy from DOERS now! .</h2>
                    <!-- <a href="#">Read More</a> --></div>
                </div>
                   <div class="item">
                  <div class="overlaybg"><img src="<?php echo BASE_URL; ?>plugins/images/news/5.png"/></div>
                  <div class="news-content"><!-- <span class="label label-success label-rounded">Primary</span> -->
                    <h2>  One point stand for all Start Up. Post your Idea and view it to become a Reality!</h2>
                    <!-- <a href="#">Read More</a> --></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> 


    <!-- end of part one -->
    <!-- start part two -->
       <!-- .row -->
      <div class="row el-element-overlay">
        <!-- /.usercard -->
       <!-- <div class="col-md-12"><h4>Scroll up effect <br/><small>You can use scroll down effect <code>el-overlay scrl-up</code></small></h4><hr></div> -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/1.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                             
                              <li>
                        <?php
                       $this->Html->link("<i class='icon-link'></i>",
                        array('controller'=>'Users','action'=>'webdevelopment'),
                        array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                         );
                        ?> 
                           <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                          </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Web Development</h3>
               <!--        <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/2.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              
                              <li>
                              <?php
                               $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'mobiledevelopment'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Mobile App Development</h3>
                  <!--     <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/3.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              <li>
                              <a href="http://drawtopic.in"  class="btn default btn-outline"><i class='icon-link'></i></a>
                         <?php
                             $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'training'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Training</h3>
                     <!--  <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/4.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              
                              <li>
                              <?php
                               $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'placement'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                               <a href="#"  class="btn default btn-outline"><i class='icon-link'></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Placement</h3>
            <!--           <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
      </div>
      <!--  next 4 -->
      <div class="row el-element-overlay">
        <!-- /.usercard -->

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/5.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              
                              <li>
                              <?php
                              $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'cloudhandling'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Server and Cloud Handling</h3>
                      <!-- <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/6.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              
                              <li>
                              <?php
                               $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'digitalmarketing'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Digital Marketing</h3>
            <!--           <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/7.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                              
                              <li><?php
                             $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'contentwriting'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">Content Writing  </h3>
                 <!--      <small></small><br/>  -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
        <!-- /.usercard -->
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
          <div class="white-box">
              <div class="el-card-item">
                  <div class="el-card-avatar el-overlay-1">
                      <img src="<?php echo BASE_URL; ?>plugins/images/users/8.png"/>
                      <div class="el-overlay scrl-up">
                          <ul class="el-info">
                             
                              <li>
                              
                              <?php
                                $this->Html->link("<i class='icon-link'></i>",
                              array('controller'=>'users','action'=>'financialservices'),
                              array('class'=>'btn default btn-outline','title'=>'Details','escape' => false)
                               );
                              ?>
                              <a class="btn default btn-outline" href="#" ><i class="icon-link"></i></a>
                              </li>
                          </ul>
                      </div>
                  </div>
                  <div class="el-card-content">
                      <h3 class="box-title">IT SUPPORT & Services</h3>
                     <!--  <small></small> -->
                  </div>
              </div>
          </div>
        </div>
        <!-- /.usercard-->
      </div>
      <!--   next 4 end        -->
<!-- end part two -->
 <!-- start part three -->
    <!-- Tabstyle start -->
     
              <!--  <h3 class="box-title m-b-0 m-t-40"></h3>
                <code></code>
                -->
                <section class="m-t-40">
                  <div class="sttabs tabs-style-linemove">
                    <nav>
                      <ul>
                        <li><a href="#section-linemove-1" class="sticon ti-home"><span>Open Source</span></a></li>
                        <li><a href="#section-linemove-2" class="sticon ti-gift"><span>Mobility</span></a></li>
                        <li><a href="#section-linemove-3" class="sticon ti-trash"><span>Business Intelligence</span></a></li>
                        <li><a href="#section-linemove-4" class="sticon ti-upload"><span>Process Consulting</span></a></li>
                        <li><a href="#section-linemove-5" class="sticon ti-settings"><span>Cloud Solutions</span></a></li>
                      </ul>
                    </nav>
                    <div class="content-wrap text-center">
                      <section id="section-linemove-1"><h2>We use open source, as server side language that lets you create both static and dynamic websites. It powers some of the biggest websites on the Internet and is responsible for more than 35% of generated web traffic. </h2></section>
                      <section id="section-linemove-2"><h2>Mobile Apps development has been a revolutionary advancement in the world of software development, since they provided an alternative to the desktop and web based software.</h2></section>
                      <section id="section-linemove-3"><h2>Modeling, integrating and harmonizing the data across different systems and defining the core data that is a value driver for different users across the organization is no easy feat.</h2></section>
                      <section id="section-linemove-4"><h2>As per research 63% of software projects exceed their budget estimates, with the top four reasons all relating to product usability: Incomplete understanding on Project requirements Frequent requests for changes by users, overlooked tasks, and insufficient user analysis communication and understanding.</h2></section>
                      <section id="section-linemove-5"><h2>While Amazon is best known as an online store where you can literally find everything, it has also become the 800 pound gorilla in the cloud services market. Amazon Web Services (AWS) have brought about a revolution in cloud computing, lowering the barrier to entry for companies who want to launch a product or a service online.</h2></section>
                    </div><!-- /content -->
                  </div><!-- /tabs -->
                </section>
         
 <!-- end part three -->
    <!-- start part fourth -->
       <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">Next Generation Techniques</div>
            <div class="panel-wrapper collapse in">
              <div class="panel-body">
              <p><ul><li>Big Data, processing of large data sets in a distributed computing environment.</li>
              <li>Native iOS apps for iPad and iPhone.</li>
<li>Android Apps development on mobile and tablets.</li>
<li>Windows & Blackberry mobile apps.</li>
<li>PhoneGap and other Jquery based Mobile Framework development.</li>
<li>Keen eye on performance of developed apps on marketplace.</li>
<li>Mobile testing on major popular devices and environment.</li>



              </ul></p>
               <!--  <a class="btn btn-custom m-t-10 ">Get Code</a> -->
                <div class="m-t-15 collapseblebox dn">
                  <div class="well"> <code> &lt;div class="panel panel-default"&gt;<br/>
                    &nbsp; &nbsp;&lt;div class="panel-heading"&gt;Default Panel&lt;/div&gt; <br/>
                    <br/>
                    &lt;div class="panel-wrapper collapse in"&gt;<br/>
                    &nbsp;&nbsp; &lt;div class="panel-body"&gt;<br/>
                    ... ... ...<br/>
                    &nbsp;&nbsp; &lt;/div&gt;<br/>
                    &lt;/div&gt;<br/>
                    <br/>
                    &lt;div class="panel-footer"&gt; Panel Footer &lt;/div&gt;<br/>
                    &lt;/div&gt; </code> </div>
                </div>
              </div>
              <div class="panel-footer">  </div>
            </div>
          </div>
        </div>
         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">Database Driven Web Applications</div>
            <div class="panel-wrapper collapse in">
              <div class="panel-body">
                <p><ul><li>Mechanism for encrypted and secure data via ACL and DBA.</li>
<li>Smartflow via XML & JSON to send authentic reports to all platforms.</li>
<li>Low Cost with High Return on Investment architecture designs.
<li>Detail Reporting and File Format Conversion.</li>
<li>Detail reporting with filtering and file format conversion capabilities.</li>
<li>Automatic deviation alert reporting from set standards in policies.</li></ul></p>
            <!--     <a class="btn btn-custom m-t-10 ">Get Code</a> -->
                <div class="m-t-15 collapseblebox dn">
                  <div class="well"> <code> &lt;div class="panel panel-default"&gt;<br/>
                    &nbsp; &nbsp;&lt;div class="panel-heading"&gt;Default Panel&lt;/div&gt; <br/>
                    <br/>
                    &lt;div class="panel-wrapper collapse in"&gt;<br/>
                    &nbsp;&nbsp; &lt;div class="panel-body"&gt;<br/>
                    ... ... ...<br/>
                    &nbsp;&nbsp; &lt;/div&gt;<br/>
                    &lt;/div&gt;<br/>
                    <br/>
                    &lt;div class="panel-footer"&gt; Panel Footer &lt;/div&gt;<br/>
                    &lt;/div&gt; </code> </div>
                </div>
              </div>
              <div class="panel-footer"> </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-heading">Mobility on All Platforms</div>
            <div class="panel-wrapper collapse in">
              <div class="panel-body">
                <p><ul><li>Responsive designs compatible to all screens through Bootstrap Framework.</li>
<li>Native iOS apps for iPad and iPhone.</li>
<li>Android Apps development on mobile and tablets.</li>
<li>Windows & Blackberry mobile apps.</li>
<li>PhoneGap and other Jquery based Mobile Framework development.</li>
<li>Keen eye on performance of developed apps on marketplace.</li>
<li>Mobile testing on major popular devices and environment.</li></ul></p>
<!--                 <a class="btn btn-custom m-t-10 ">Get Code</a> --> <!-- just rmove the collapseble class if we add this class then it shows the  -->
                <div class="m-t-15 collapseblebox dn">
                  <div class="well"> <code> &lt;div class="panel panel-default"&gt;<br/>
                    &nbsp; &nbsp;&lt;div class="panel-heading"&gt;Default Panel&lt;/div&gt; <br/>
                    <br/>
                    &lt;div class="panel-wrapper collapse in"&gt;<br/>
                    &nbsp;&nbsp; &lt;div class="panel-body"&gt;<br/>
                    ... ... ...<br/>
                    &nbsp;&nbsp; &lt;/div&gt;<br/>
                    &lt;/div&gt;<br/>
                    <br/>
                    &lt;div class="panel-footer"&gt; Panel Footer &lt;/div&gt;<br/>
                    &lt;/div&gt; </code> </div>
                </div>
              </div>
              <div class="panel-footer"> </div>
            </div>
          </div>
        </div>
      </div>
    <!-- end part fourth -->














<!-- <div align="center">
<h1> Virtual Office On All Platforms </h1>
<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FVLLsoftware&amp;width=292&amp;height=590&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;border_color&amp;header=true&amp;appId=211031582287023" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:292px; height:590px;" allowTransparency="true"></iframe>
</div>
<div class="m1 h2">
<div class="fr w2 x1">
<p class="c3 b p3">Our Key benefits are:</p>
<p class="x2 c3 p4">USA Based Contracts</p>
<p class="x2 c3 p4">ISO 9001:2000 Processes</p>
<p class="x2 c3 p4">Highly Competent Resources</p>
<p class="x2 c3 p4">Seamless Communication</p>
<p class="x2 c3 p4">Security & IP Protections</p>
<p class="x2 c3 p4">Customize Pricing Models</p>
<div align="center" class="x3"><img src="<?php echo BASE_URL."images/request-quote.jpg"?>" width="181" height="32" vspace="5"><br><br>
<br>
<p align="right" style="padding-right:20px;"><a href=" http://www.facebook.com/VLLsoftware" target="_blank"><img src="<?php echo BASE_URL."images/fb.jpg"?>" width="15" height="15" hspace="4"></a>
<a href=" https://twitter.com/VLLSoftware" target="_blank"><img src="<?php echo BASE_URL."images/twitt.jpg"?>" width="15" height="15" hspace="4"></a>
<a href="http://www.youtube.com/VLLsol" target="_blank"><img src="<?php echo BASE_URL."images/youtube.png"?>" width="15" height="15" hspace="4"></a>
<a href="http://www.linkedin.com/company/VLL-software-pvt-ltd" target="_blank"><img src="<?php echo BASE_URL."images/linkedin.png"?>" width="15" height="15" hspace="4"></a>
</p>
</div>
<p align="right"></p>
<div class="v1">
<img src="<?php echo BASE_URL."images//arw3.gif"?>" width="44" height="139" align="right"><img src="<?php echo BASE_URL."images/arw2.gif"?>" width="33" height="139" align="left">
<p><img src="<?php echo BASE_URL."images/port.gif"?>" width="113" height="129" align="right" vspace="5"><br>
<br>
<b>Project:</b><br>
NBLS<br>
<br>
<p class="f2 c4 b"><a href="#">Visit Website:</a></p><br><br>
<p class="c b c5 f2"><a href="<?php echo BASE_URL."static_pages/view/portfolio"?>">See our complete portfolio</a></p><br>
<br>
<br>

</p><br><br>


</div>
</div>

<div class="fl">
<div class="w10 x18 h4">
<p class="c3 b f5 p28"><i><b class="f7">Empowering Innovation</b></i> with Excellence...</p>
</div>
<DIV ID="countrytabs">
<div class="fl w11 x19 li5 p29 c f2"><A HREF="#" REL="country1">Our Strong Domain Experience in<br>
<b class="f8">REAL ESTATE INDUSTRIES</b></A></div>
<div class="fl w11 x20 li5 c f2 p29"><A HREF="#" REL="country2">Domain Knowldege in<br>
<b class="f8">SOCIAL NETWORKING</b></A></div><div class="fl w11 x21 li5 c f2 p29"><A HREF="#" REL="country3">Domain Knowldege in<br>
<b class="f8">HEALTH CARE</b></A></div>

<div class="v5 w10" ID="country1">
<p class="v6 c3 f6 p21">Real Estate</p>
<p class="c3 p22 v6 f3">realestate</p>
<p class="p23 f3">We don't just build real estate web sites,</p>
<p class="c12 p24 f3 b">We deliver a complete marketing solution</p>
<p class="x17 p25 b c6">Enterprise level real estate portal</p>
<p class="x17 p25 b c6">A choice of high end template designs</p>
<p class="x17 p25 b c6">Fully featured back and Content Management System</p>
<p class="x17 p25 b c6">Enterprise level real estate portal</p>
<p class="x17 p25 b c6">A choice of high end template designs</p>
<p class="x17 p25 b c6">Fully featured back and Content Management System</p>
<p><br>
</p>
<p class="p26"><a href="<?php echo BASE_URL."static_pages/view/portfolio"?>"><Img src="<?php echo BASE_URL."images/view.jpg"?>" width="162" height="60"></a></p>
</div>
<div class="v5 w10" ID="country2">
<p class="v7 c3 f6 p21">Social Networking</p>
<p class="c3 p22 v7 f3">socialnetworking</p>
<p class="p23 f3">Give your website a voice! </p>
<p class="c12 p24 f3 b">We develop Social Networking web portals. </p>
<p class="x17 p25 b c6">Invite, meet and make new friends.</p>
<p class="x17 p25 b c6">Personalize voices & RSS feeds.</p>
<p class="x17 p25 b c6">Create multiple channels & Share with friends.</p>
<p class="x17 p25 b c6">Slideshows, video players and music players.</p>
<p><br><br>
<br>
</p>
<p class="p26"><a href="<?php echo BASE_URL."static_pages/view/portfolio"?>"><Img src="<?php echo BASE_URL."images/view.jpg"?>" width="162" height="60"></a></p>
</div>
<div class="v5 w10" ID="country3">
<p class="v8 c3 f6 p21">Health Care</p>
<p class="c3 p22 v8 f3">healthcare</p>
<p class="p23 f3">Transforming healthcare with better information  </p>
<p class="c12 p24 f3 b">integration for better results/decisions: </p>
<p class="x17 p25 b c6">Engineering web applications to simplify complex medical workflows</p>
<p class="x17 p25 b c6">Connecting benefit administration, care management and constituent engagement</p>
<p class="x17 p25 b c6">Enabling payers managing the unit cost as well as funding cost of care</p>
<p><br><br><br>
<br>
</p>
<p class="p26"><a href="<?php echo BASE_URL."static_pages/view/portfolio"?>"><Img src="<?php echo BASE_URL."images/view.jpg"?>" width="162" height="60"></a></p>
</div>
</DIV>
</div>


</div>
<div class="m1 cb">
<div class="fl w3 v3">
<p class="v2 p5 b c6">Our Technologies</p>
<p class="p6 b">Microsoft Technology</p>
<p align="center"><img src="<?php echo BASE_URL."images/microsoft-technology.gif"?>" width="285" height="62"></p>
<p class="p6 b">Open Source</p>
<p align="center"><img src="<?php echo BASE_URL."images/pen-source.jpg"?>" width="284" height="102"></p>
<p class="p6 b">Mobile Technologies</p>
<p align="center"><img src="<?php echo BASE_URL."images/mobile.jpg"?>" width="284" height="70"></p>
<!--<p class="x4 li1 p7 b"><a href="#">Read More</a></p>
</div>
<div class="fl w3">
<p class="v2 p5 b c7">Testimonials</p>-->
<?php /* $testimonial=$general->gettestimonial();
foreach($testimonial as $data){
*/ ?>

<?php //} ?>
<!--<div class="fl w5">
<p class="x5 li1 p9"><a href="#">Real Estate</a></p>
<p class="x5 li1 p9"><a href="#">Real Estate</a></p>

</div>
<div class="fl">
<p class="x5 li1 p9"><a href="#">Real Estate</a></p>
<p class="x5 li1 p9"><a href="#">Real Estate</a></p>

</div>
<p><br>
<br>
<br>
<br>
<br>
</p>
<p class="x4 li1 p27 b cb"><a href="#">Read More</a></p>-->

<!--<p class="x4 li1 p7 b"><a href="<?php echo BASE_URL.'testimonials/details'?>">Read All Testimonials</a></p>
</div>
</div>
<div class="bo2">
<div class="fl w4 v3">
<p class="v2 p5 b c6">Recent News</p>-->
   <?php /*$get=$general->getNews(); 
         foreach($get as $news) { ?>
	        <p class="p10 x6"><b class="c6"><?php echo $news['News']['title']; ?></b><br/>
		     <?php echo substr(strip_tags($news['News']['description']),0,85)."...";?>
	        </p>
    <?php } */ ?>
<!--<p class="x4 li1 p7 b"><a href="<?php echo BASE_URL.'news/details'?>">Read All News</a></p>

</div>
</div>-->

