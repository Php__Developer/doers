
 <!-- start of part one -->
 <link href="{{url('plugins/bower_components/calendar/dist/fullcalendar.css')}}" rel="stylesheet" />

       <!-- row -->
        <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Course Details</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"><!-- <a href="https://themeforest.net/item/elite-admin-responsive-dashboard-web-app-kit-/16750820" target="_blank" class="btn btn-danger pull-right m-l-20 btn-rounded btn-outline hidden-xs hidden-sm waves-effect waves-light">Buy Now</a>-->
                        <ol class="breadcrumb">
                            <li><a href="#">University</a></li>
                            <li class="active">Course Details</li>
                        </ol>
                    </div>
                </div>
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <h3 class="box-title">Drag and drop your event</h3>
            <div class="m-t-20">
              <div class="calendar-event" data-class="bg-primary">My Event One <a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>
              <div class="calendar-event"  data-class="bg-success">My Event Two <a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>
              <div class="calendar-event"  data-class="bg-warning">My Event Three <a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>
              <div class="calendar-event"  data-class="bg-custom">My Event Four <a href="javascript:void(0);" class="remove-calendar-event"><i class="ti-close"></i></a></div>
              <input type="text" placeholder="Add Event and hit enter" class="form-control add-event m-t-20">
            </div>
          </div>
        </div>
        <!-- <div class="col-md-12">
          <div class="white-box">
            <div id="calendar"></div>
          </div>
        </div> -->
      </div>
      <!-- /.row -->
<!-- end of part one -->

<!-- start part two -->
   <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-12 m-b-20">
                                    <img class="img-responsive" src="<?php echo BASE_URL; ?>plugins/images/heading-bg/slide2.jpg" alt="course-image">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Course Name</strong>
                                    <br>
                                    <p class="text-muted">Johnathan Deo</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Course Price</strong>
                                    <br>
                                    <p class="text-muted">$200.00</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Professor Name</strong>
                                    <br>
                                    <p class="text-muted">Steve Gection</p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Difficulty</strong>
                                    <br>
                                    <p class="text-muted">Advanced</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries </p>
                                    <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                        <ul class="common-list">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> List group item heading</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Donec id elit non mi porta gravida</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                    </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <ul class="common-list">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> List group item heading</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Donec id elit non mi porta gravida</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                    </ul>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <ul class="common-list">
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> List group item heading</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Donec id elit non mi porta gravida</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Maecenas sed diam eget</a></li>
                                    </ul>
                                    </div>
                            </div> 
                            <hr>
                              <div class="row pricing-plan">
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-l">
                                            <div class="pricing-header">
                                                <h4 class="text-center">Silver</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>24</h2>
                                                <p class="uppercase">per month</p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i> 3 Members</div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Single Device</div>
                                                <div class="price-row"><i class="icon-drawar"></i> 50GB Storage</div>
                                                <div class="price-row"><i class="icon-refresh"></i> Monthly Backups</div>
                                                <div class="price-row">
                                                    <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box b-l">
                                        <div class="pricing-body">
                                            <div class="pricing-header">
                                                <h4 class="text-center">Gold</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>34</h2>
                                                <p class="uppercase">per month</p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i> 5 Members</div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Single Device</div>
                                                <div class="price-row"><i class="icon-drawar"></i> 80GB Storage</div>
                                                <div class="price-row"><i class="icon-refresh"></i> Monthly Backups</div>
                                                <div class="price-row">
                                                    <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box featured-plan">
                                        <div class="pricing-body">
                                            <div class="pricing-header">
                                                <h4 class="price-lable text-white bg-warning"> Popular</h4>
                                                <h4 class="text-center">Platinum</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>45</h2>
                                                <p class="uppercase">per month</p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i> 10 Members</div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Single Device</div>
                                                <div class="price-row"><i class="icon-drawar"></i> 120GB Storage</div>
                                                <div class="price-row"><i class="icon-refresh"></i> Monthly Backups</div>
                                                <div class="price-row">
                                                    <button class="btn btn-lg btn-info waves-effect waves-light m-t-20">Sign up</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                    <div class="pricing-box">
                                        <div class="pricing-body b-r">
                                            <div class="pricing-header">
                                                <h4 class="text-center">Dimond</h4>
                                                <h2 class="text-center"><span class="price-sign">$</span>54</h2>
                                                <p class="uppercase">per month</p>
                                            </div>
                                            <div class="price-table-content">
                                                <div class="price-row"><i class="icon-user"></i> 15 Members</div>
                                                <div class="price-row"><i class="icon-screen-smartphone"></i> Single Device</div>
                                                <div class="price-row"><i class="icon-drawar"></i> 1TB Storage</div>
                                                <div class="price-row"><i class="icon-refresh"></i> Monthly Backups</div>
                                                <div class="price-row">
                                                    <button class="btn btn-success waves-effect waves-light m-t-20">Sign up</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><hr>
                            <div class="row text-center">
                                <button class="btn btn-danger btn-lg">Join Course</button>
                            </div>     
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
                                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

