<html>
<link href="../plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">

<!--  start content-table-inner -->
	
	<!--  start table-content  -->
		
			<?php echo $this->Flash->render(); ?>
		
<div class="white-box">

		<div class="col-md-1 pull-right">
	<ul class="pagination paginate-data">
		<li class="previousdata">
			<?php  echo $this->Paginator->prev('<span class="fa fa-angle-left" ></span>', ['escape' => false] );?>
		</li>
		<li>
			<?php echo $this->Paginator->next('<span class="fa fa-angle-right" ></span>', ['escape' => false] ); ?>
		</li></ul>
	</div>
     

           <table class="tablesaw table-bordered table-hover table  tablesaw-sortable" data-tablesaw-mode="stack" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-8990" style="">
              <thead>
                <tr>
                 <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head" data-sortable-numeric="false"><button class="tablesaw-sortable-btn">Current Status</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Updated On</button></th>
                  <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric=""><button class="tablesaw-sortable-btn">Notify</button></th>
                                </tr>
              </thead>
              <tbody>
			<?php foreach($resultData as $userData): $class="";
			//if(!$userData['status']%2)$class = "alternate-row"; else $class = "";  ?>
				<tr class="<?php echo $class; ?>">
					<!--td><?php echo $userData ['first_name']." ".$userData ['last_name']; ?></td-->
					<td>
						<div class="statusmsg">
						<?php echo $userData ['current_status']; ?>
						</div>
							<span class="usr_ip_1">
						   <?php if ($userData ['ipaddress'] != MY_IP) {
							 $ip = "<span class='pull-right text-danger'>". $userData ['ipaddress']  . "</span>" ;
							 }else {
							  $ip = $userData ['ipaddress'];
							 }
							echo $ip;
							?> 
						  </span>
						  <br/>
						<span id="usr_name_1" class="pull-right text-success">
							<?php echo $userData ['first_name']." ".$userData ['last_name']; ?> 
						</span> 
					</td>
					<?php 

					$userData['expTime'] = $this->General->expiredTime($userData['status_modified']);
					if($userData['expTime']['hours'] >=2) {  ?>
							<td class="text-danger"><?php echo date("d/m/Y h:ia",strtotime($userData ['status_modified']));  ?></td>
					<?php } else { ?>
							<td class="text-success"><?php echo date("d/m/Y h:ia",strtotime($userData ['status_modified']));  ?></td>
					<?php	} ?>
					<td class="options-width" align="center">
						<?php
				  echo $this->Html->link(
                      '<i class="ti-email"></i>',
                      array('controller'=>'users','action'=>'report','id'=>$userData['id']),
                      ['escape' => false,'class' => 'btn default btn-outline deleteprojectdata','title'=>'Email','onclick'=>'return confirm("Are you sure you want to remind for update?")']
                      );?>
					</td>
				</tr>
				<?php endforeach; ?>
				
					      </tbody>
    
		   </table>
			
			
			<!--  start paging..................................................... -->

			<!--  end paging................ -->
			
		</div>

		<script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw.js"></script>
<script src="../plugins/bower_components/tablesaw-master/dist/tablesaw-init.js"></script>
<!--Style Switcher -->
