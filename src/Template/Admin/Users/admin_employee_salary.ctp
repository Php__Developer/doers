<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/colortip-1.0-jquery.css"/>

<style>
    .salary_table{
        
        border-color:#999999; 
        width: 1000px;
        
    }
.salary_table td{

border: 1px solid #D2D2D2;
    padding: 10px 5px 10px 10px;
font-size:12px;
}
.salary_table th{

padding:3px;
border-color:#999999; 
font-size:12px;

}
</style>
<style type="text/css">
    
    .comment_box{
        
        
        width: 150px !important;
        height: 40px !important;
    }
</style>
<script type="text/javascript">
	var base_url = '<?php echo BASE_URL; ?>';
	
	
	function editRecord(id){
		
                 spantoInput('comment_'+id);
                 $('#'+'comment_'+id).removeAttr("class");
                   $('#'+'comment_'+id).attr("class",'comment_box');
		changeLink('edit_'+id);
			$('body div#ui-datepicker-div').css({'display':'none'});
		alphanum();
	}
	function spantoInput(spanID){
		var data_span = $('#'+spanID).html();
		$('#'+spanID).replaceWith('<input type="text" id="'+spanID+'" value="'+data_span+'" class="numeric" style="width:40px;height:20px;"/>');
                
	}
	function inputToSpan(inputID){
		var data_input = $('#'+inputID).val();
		$('#'+inputID).replaceWith('<span id="'+inputID+'">'+data_input+'</span>');
	}
	function changeLink(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-5');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editData('+id+')');
		$('#'+linkID).attr('title','Edit');
	}
	function changeLinkBack(linkID){
		$('#'+linkID).removeClass();
		$('#'+linkID).addClass('icon-1');
		var id = linkID.replace('edit_','');
		$('#'+linkID).attr('href','javascript:void(0)');
		$('#'+linkID).attr('onClick','editRecord('+id+')');
		$('#'+linkID).attr('title','Save');
	}
	function editData(id){
		if(checkLimit($('#mon_'+id).val()) || checkLimit($('#tue_'+id).val()) || checkLimit($('#wed_'+id).val()) || checkLimit($('#thur_'+id).val()) || checkLimit($('#fri_'+id).val()) || checkLimit($('#sat_'+id).val()) || checkLimit($('#sun_'+id).val()))	{  
			return false;
		}
			var postdata = {	'id':id,
                                                'month':'<?php echo $month_act;     ?>',
										
										'notes' : $('#comment_'+id).val(),
									};
		if($.active>0){
		}else{							
			$.post(base_url+'admin/users/update_salary_comment',postdata,function(data){
				if(data==1){
					get_total(id);
					updateRecord(id);
					$("#success").html("Billing has been updated successfully!");
					$('#success').show();
					setTimeout(function() { $('#success').fadeOut('slow'); }, 5000);
				}else if(data=='error'){
					alert("Billing entry for this week already exists!");
				}
			});
		}
	}
	function get_total(id)
	{
		var total  = 0; 
		var left = 0;
		total  = parseFloat($('#mon_'+id).val()) + parseFloat($('#tue_'+id).val()) + parseFloat($('#wed_'+id).val()) + parseFloat($('#thur_'+id).val()) +  parseFloat($('#fri_'+id).val()) + parseFloat($('#sat_'+id).val()) + parseFloat($('#sun_'+id).val()) ;
		if(isNaN(total)){
			total = 0.00;
		}
		left = parseFloat($('#todo_'+id).html()) - parseFloat(total);
		$('#tot_'+id).html(total.toFixed(2));
		$('#left_'+id).html(left.toFixed(2));
	}
	function updateRecord(id){
		
                inputToSpan('comment_'+id);
                
                
		changeLinkBack('edit_'+id);
	}
	function checkLimit(val){
		if(parseFloat(val) > parseFloat('24')){
			$('#error_msg').html("Daily billing hours cannot be greater than 24");
			$('#error_msg').show();
			setTimeout(function() { $('#error_msg').fadeOut('slow'); }, 5000);
			return true;
		}
	}
</script>
<?php


 
if(date('m')==$month_act)
{ 
   $day_count=date('j'); 
}
else{
    
    $day_count=cal_days_in_month(CAL_GREGORIAN,$month_act,$year_act);
}
?> 
<?php

$urlString = "";
$newUrl = "employee_salary".$urlString;

?>

<div style="height:100px;padding: 10px;">
<?php echo $form->create('User',array('action'=>$newUrl,'method'=>'POST', "class" => "longFieldsForm", "name" => "listForm", "id" => "mainform","onsubmit"=>"return validateForm();")); ?>
                               <div style="margin-bottom: 10px;">
							  <div style="float:left;">
				Month:
					<?php  App::import('Component', 'common');
						$this->Common= new CommonComponent;
						$options = $this->Common->getMonthsArray();
						echo $form->input("month2",array("type"=>"select","class"=>"top-search-inp","options"=>$options,"label"=>false,"div"=>false,"style"=>"width:100px;","id"=>"month2"));
					?>
					</div>
					
						<div style="float:left;margin-right:39px">	<b>Year :</b></div>
				<div style="float:left;">
					<?php 
						  $curYear 	= "2013";
						  $array 	= array();
						  $array[''] = "-Select-";
						  while($curYear<='2025'){
						  $array[$curYear] = $curYear;
						  $curYear++;
						  }
						  echo $form->input("year",array("type"=>"select","class"=>"top-search-inp","options"=>$array,"label"=>false,"div"=>false,"style"=>"width:100px;"));
					?>
				</div>
					
					
					
					
					
					</div>
<?php echo $form->submit("Search", array('class'=>'form-search','id'=>'search','onclick'=>'setSubmitMode(this.id)'))."&nbsp;&nbsp;&nbsp;";  ?>
                                                                                                                                                   
<?php	echo $form->button("Reset",array('type'=>'button','class'=>"form-reset",'div'=>false,'onclick'=>"location.href='".BASE_URL."admin/users/employee_salary'")); ?>
</div>
<h3 style="float:right">
<?php
$user=$this->Session->read("SESSION_ADMIN");
     
        $role_array=explode(',', $user[2]);
     $d=count($role_array);
    
    
         
         echo $this->Html->link('Download',array('controller'=>'users','action'=>'admin_download'), array('target'=>'_blank'));
         
     
?>
</h3>

<h3><?php echo "Salary Calculation"."&nbsp;&nbsp;&nbsp;".date('F', mktime(0,0,0,$month_act+1,0,0))." ".$year_act;  ?></h3>
<table border="1" cellpadding="4" class="salary_table">

<thead>
<tr>
    <th style="width:200px">Name</th> <th style="width:200px">Current Salary</th>
<?php
for($i=0;$i<$day_count; $i++){
?>

<th><?php  echo ($i+1);   ?></th>


<?php  }   ?>
<th style="width:200px">Comment</th>

<th style="width:200px">Total Salary</th>
</tr>
 </thead> 
<?php
$grand_total=array();



$cmp=0;
foreach($Emp_record as $emp_detail){
 

 
 $user=$Emp_record[$cmp]['User']['id']; 
 $user1=$Emp_record[$cmp+1]['User']['id']; 
 $cmp++;
 if($user!=$user1)
{

?>

<tr><td style="width:200px"><?php echo $emp_detail['User']['first_name']." ".$emp_detail['User']['last_name'];   ?></td>
<td><?php echo $emp_detail['Appraisal']['appraised_amount']; 
  $amount= $emp_detail['Appraisal']['appraised_amount'];
  $day_amount=$emp_detail['Appraisal']['appraised_amount']/30;
  ?></td>

<?php 

$salary=array();
$salary2=array();
$c=0;

foreach($emp_detail['Attendance'] as $attendence ) { 

$c++;

   
$dt=$c."".date('M', mktime(0,0,0,$month_act+1,0,0)).$year_act;

$ad=date('D', strtotime($dt));

if($ad=="Sun"){
$c++;
echo "<td>";
$actual_salary2=1*$day_amount;
$salary2[]=$actual_salary2;
 
 ?>
<a title='<?php echo $p."*".round($day_amount,2)."=".round($actual_salary2,2)    ?>' style='color:red !important;'><?php  echo "Sun";   ?></a>
<?php

echo "</td>";



}



?>

<td>

<?php

if($attendence['hours']>5){$p=1;}else{$p=0.5;}
$actual_salary=$p*$day_amount;
$salary[]=$actual_salary;
 ?>
 <a title='<?php echo $p."*".round($day_amount,2)."=".round($actual_salary,2)    ?>'><?php  echo $p;   ?></a>
  

</td>
<?php






} ?>
 
<?php    
$ext_td=$day_count-$c;

for($k=0;$k<$ext_td;$k++){
    ?>


<td><?php echo "0"; ?></td>



<?php   }   ?>





<td>
   <span id="comment_<?php echo $user; ?>"><?php 
      
      if(!empty($emp_detail['Leave']['comment'])){echo $emp_detail['Leave']['comment']   ;}else{ echo "";}?>
       
       
   </span>
       <?php
                       $user1=$this->Session->read("SESSION_ADMIN");
                          $role_array=explode(',', $user1[2]);
     $d=count($role_array);
    
     if($d>2 ||$user1[2]==3){
echo "<div style='float:right'>";

echo $html->link("","javascript:void(0)",array('class'=>'icon-1 info-tooltip','style'=>'float:right', 'title'=>'Edit','id'=>'edit_'.$user,'onClick'=>"javascript : editRecord(".$user.");"));

echo "</div>";
}
  ?>
</td>

<td><?php 


$grand_total[]=round(array_sum($salary)+array_sum($salary2),2);
 

 echo round(array_sum($salary)+array_sum($salary2),2);  ?></td>
</tr>

<?php  } } ?>
<tr><td colspan="<?php echo $i+3; ?>"><center>Grand Total</center></td><td><?php echo round(array_sum($grand_total),2);  ?></td></tr>
</table>


<script type="text/javascript" src="<?php echo BASE_URL; ?>js/colortip-1.0-jquery.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>js/script_tooltip.js"></script>


