<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class GeneralHelper extends Helper
{	
	function set(){
		return "";
	}
	function errorHtml($field){
		/*
		$divstart = '<div class="validation errors">';
		$divend=  '</div>';
		$span = '';
		foreach($field as $key => $message){
			$span .= "<span class='validation_error'>{$message}</span>";
		}
*/
		
	  	$span = '';
	  	foreach($field as $key => $message){
	  		//pr($key);
			$span .= '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$message.'</div>';
		}
		//die;

		/*$divstart = '<span class="help-block with-errors">';
	  $divend=  '</span>';
	  	$span = '';
	  	foreach($field as $key => $message){
			$span .= "<ul class='list-unstyled'><li>{$message}</ul></span>";
		}*/
		
          return $span;

	}

	function checkverifystatus($ProjectsID,$currentWeekMonday){
		$users = TableRegistry::get('Billings');
			$query = $users->find('all',[
					'conditions' => [ 'verified'=>'0',
										'project_id'=>$ProjectsID,
										'week_start <=' => date_create($currentWeekMonday)->format('Y-m-d H:i:s')
					]
				]);
		 $result = isset($query)? $query->toArray():array();		
		 return $result;
	}

	function getusersfromrole($role_id,$moduleid){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		$users = TableRegistry::get('Users');
		$exp = explode(',', $role_id);

		$modules = TableRegistry::get('Modules');
		$modulesData = $modules->find('all',[
			'conditions' => ['id' => $moduleid]
			])->first();
		$including = explode(',', $modulesData->including_user_id);
		$excluding = $modulesData->except_user_id;
		$ids =[];
		if(count($including) > 0){
				$ids = 	array_merge($ids,$including);
					
		} else{
				$ids =[];	
		}
		if(count($exp) > 0){
				foreach($exp as $role){
					$result= $users->find("list", [
					'keyField' => 'id',
					'valueField' => 'username',
					'conditions' => ['status'=>'1','agency_id' => $userSession[3],'FIND_IN_SET(\''. $role .'\',role_id)'],
					'fields' => ['id','username']
		  		  ]);
		  		   $result = isset($result)? $result->toArray():array();	
		//  		   pr($result);
		  		   $idss = array_merge($ids,array_keys($result));
		  		   foreach($idss as $id){
		  		   	if(!in_array($id, $ids) && !in_array($id, explode(',', $excluding)) ){
		  		   		array_push($ids, $id);
		  		   	}
		  		   }
				}
				//pr($ids);

				if(count($ids) > 0){
					$query = $users->find('all',[
							'conditions' => ['id IN' => array_unique($ids),'agency_id' => $userSession[3]]				
						]);
				}

		}
		 $result = isset($query)? $query->toArray():array();		
		 return $result; 
		
	}

    function getEmailTemplateFields() {

	$fieldsArray = array();

	$fieldsArray[""]   		= "--- Select ---";

	$fieldsArray["EmailTemplate.name"] 	= "Name";

	$fieldsArray["EmailTemplate.subject"] 	= "Subject";

	return $fieldsArray;

    }

	

	 //get list of users

	function getuser() {
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");

      $users = TableRegistry::get('Users');
         $result= $users->find("list", [
			'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['status'=>'1','agency_id' => $userSession[3],"!FIND_IN_SET('1',role_id)"],
			'fields' => ['id','username'],
			'order'=>['username' =>  'ASC']
  		  ]);
         $result = isset($result)? $result -> toArray():array();
         $finalresult = array('' => '--Select--') + $result;
         return $finalresult;
    } 

	

	 //get list of emails for users (by roles optional) 

	 function getemail_ID($role = null) {

				App::import("Model","User");

				(!empty($role)) ? $str = "and FIND_IN_SET($role,role_id)" : $str = "";

				$cond = "status = '1' ".$str;

				 $this->User= new User;

				 $result= $this->User->find('list', array(

				 'conditions' => $cond,

				 'fields' => array('id','username'),

				 'order'=>'id  ASC'

				));

			 return $result;

    }  

	

	

	function getCMSCategory(){

		$options = array(

		'1'=>'Admin',

		'2'=>'Human Resource',

		'3'=>'Sales',

		'4'=>'Engineering',

		'5'=>'Finance',

		'6'=>'Operations'

		);

		return $options;

	}

    // get Category List as a dropdown in Add Product in admin
/*
    function addCategoryList($id=null) {

	$catArray = array('0' => 'No Parent');

	    App::import("Model","Category");

	    $this->Category = new Category();

	

	    $conditions = array('parent_id' => '0');

	    $category =  $this->Category->find('all', array('fields'=>array('Category.id','Category.title'),'conditions' => $conditions,"order" => 'Category.title asc'));

	    foreach($category as $cat){ 

		$catArray[$cat['Category']['id']] = $cat['Category']['title'];

	    }

	    return $catArray;

    }



    // get Category List as a dropdown in Add Product in admin

    function getCategoryList() {

	

	    $catArray = array();	    

	    App::import("Model","Category");

	    $this->Category=  new Category();

	    $category =  $this->Category->find('all', 

		array(

			'fields'   =>array('Category.id','Category.title'),

			'conditions'=> array('status' => '1','is_deleted'=>'0','parent_id' => '0'),

			'order'     => 'Category.title ASC'

		)

	    );

	    foreach($category as $cat){ 

		$catArray[$cat['Category']['id']] = ucwords($cat['Category']['title']);

	    }

	    return $catArray;

    }

    */

    // get countries List as a dropdown in admin

    function getcountries() {

	

	    $cntArray = array();

	    $cntArray [""]  = "--- Select ---";

	    App::import("Model","Country");

	    $this->Country= new Country();

	    $country=  $this->Country->find('all', array('fields'=>array('Country.id','Country.country_name')));

	    

	    foreach($country as $cat){ 

		$cntArray[$cat['Country']['id']] = $cat['Country']['country_name'];

	    }	    

	    return $cntArray;

    }

    

    // get static pages fields as a dropdown in searching in admin @neema

    function getPagesFields() {

	$fieldsArray = array();

	$fieldsArray[""]   		= "--- Select ---";

	$fieldsArray["StaticPage.title"] 	= "Title";

	$fieldsArray["StaticPage.content"] 	= "Content";

	$fieldsArray["StaticPage.slug"] 	= "Slug";

	$fieldsArray["StaticPage.created"] 	= "Created On";

	return $fieldsArray;

    }

    

    

    // get Customer List as a dropdown in Add/Edit Dog in admin

    function getUserList($uid=null) {

	    $supArray = array(''=>'---- Select Customer ----');

	    App::import("Model","User");

	    $this->User= new User();

	    if($uid!='') {

		$conditions = array("User.type in ('C') and User.status='1' and User.id=".$uid);

	    }else{

		$conditions = array("User.type in ('C') and User.status='1'");		

	    }



	    $supplier =  $this->User->find('all', array('fields'=>array('User.id','User.first_name','User.last_name'),'conditions' => $conditions,'order'=>'User.first_name'));

	    foreach($supplier as $cat){

		$supArray[$cat['User']['id']] = ucwords($cat['User']['first_name']." ".$cat['User']['last_name']);

	    }

	    return $supArray;

    }

 

           

    //	This function is called to get links of header and footer

    function getfooterlink($pos=null) {

	    App::import("Model","StaticPage");

	    $this->StaticPage= new StaticPage();

	    $link= $this->StaticPage->find('all', array(

	       'conditions' => array('StaticPage.status' => '1','StaticPage.id'=>array('1','3','4','5','6','2')),

	       'fields' => array('StaticPage.id', 'StaticPage.title'),

		'order'=>"field(id,'1','3','4','5','6','2')"

	       )

	    ); 

	return $link;

    }



    //	This function is called to other links of header and footer

    function getotherfooterlink($pos=null) {

	    App::import("Model","StaticPage");

	    $this->StaticPage= new StaticPage();

	    $link= $this->StaticPage->find('all', array(

	       'conditions' => array('StaticPage.status' => '1','StaticPage.id NOT'=>array('1','3','4','5','6','2')),

	       'fields' => array('StaticPage.id', 'StaticPage.title'),

		'order'=>"title ASC"

	       )

	    ); 

	return $link;

    }



    //	This function is called to sort any associated field

    function getsortlink($name = null, $sort = null, $passesArgs = array(),$urlArray = array()) {

	App::import('Helper','Html');

	$html = new HtmlHelper();

	return $html->link($name, array_merge(array(

		'controller' => $this->params['controller'],

		'action' => $this->params['action'],

		'page' => (!empty($passesArgs['page'])?$passesArgs['page']:"1"),

		'sort' => $sort,

		'direction' => (empty($passesArgs['direction']) || $passesArgs['direction'] == 'asc')?'desc' : 'asc',

		'limit' => (!empty($passesArgs['limit'])?$passesArgs['limit']:RECORDS_PER_PAGE)

	),$urlArray));

    }



    //	This function is called to get links of logged in users.

    function getLinkPermission() {

	   App::import("Model","User");
	    $this->User= new User();
        $id=$_SESSION['SESSION_ADMIN']['id'];
        $type=$_SESSION['SESSION_ADMIN']['type'];

	    

	    $this->User->expects( array('AdminPrevilege'));

	    $data = $this->User->find('first',

		array(

			'conditions'=>array('User.id'=>$id, 'type'=>$type),

			'fields' => array('User.id',

						'User.first_name',

						'User.last_name',

						'AdminPrevilege.*'

						)

		)

	   );

			

	return $data;

    }



  /**

    * @Date: 18-Dec-2010

    * @Method : paymentMethods

    * @Purpose: Function to show weights in dropdown menu.

    * @Param: none

    * @Return: none

    **/

	

    function paymentMethods(){

	$data = array(

		"" => " Select ",

		"check" => "Check",

		"credit card" => "Credit Card",

		"cash" => "Cash",

	);

	return $data;

    }





	//to display all static pages

   function pages() {

			App::import("Model","StaticPage");

			$this->StaticPage = new StaticPage();

             $name = array(

				  'conditions' => array(

						'StaticPage.status' =>'1'

				   ),

				   'fields' => array('slug','title')

				   );

				   //print_r($city_name);

			$page = $this->StaticPage->find('list', $name);

			

			return $page;

	}









	  //Retrieve category for news

    function getCategory(){

		App::import('Model','NewsCategory');

		$this->NewsCategory= new NewsCategory;

		$result= $this->NewsCategory->find('list',array(

		'conditions'=>array('status'=>'1'),

		'fields'=>array('id','cat'),

		'order'=>'cat ASC'

		));

		$result= array(''=>'---Please Select---') + $result;

		return $result;

	}

	//display Question type in drop down

	function getQues(){

		$ques=array(

		''=>'--Select--',

		'Our Company'=>'Our Company',

		'Product Enquiry'=> 'Product Enquiry',

		'Our Services'=>'Our Services',

		'Brands'=>'Brands'

		);

		return $ques;

	}

	

	  //Retrieve user roles

    function getUserRoles(){

    	  $roles = TableRegistry::get('Roles');
		  $data  = 	$roles->find("list" , [
					'keyField' => 'id',
					'valueField' => 'role',
		          'conditions' => ['status' => 1,'role !='=> 'Super Admin'],
		          'fields' => ['id','role'],
		          'order'=>['role'=>'ASC']
		       ]);
		  	 $result = isset($data)? $data->toArray():array();
		return $result;

	}

	

		//get clients names from contacts table

	

	function getclients($id = null){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		 $Contacts = TableRegistry::get('Contacts');
		$cond = (empty($id)) ?  [" iscurrent" => 1, 'agency_id' => $userSession[3]  ] : [" iscurrent" => 1 , 'agency_id' => $userSession[3] ,'OR' => ['id' => $id, 'agency_id' => $userSession[3]]];
		$result= $Contacts->find('all');
		$result = $Contacts->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'name',
			'conditions' => $cond,
			'fields' => ['id','name'],
			'order' => 'name ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
 		//pr($res);
		$finalresult = array('' => '--Select--') + $res;
		return $finalresult;

	}

  
	// get permissions for menus 

   function menu_permissions($controller,$action,$userID) {
		   $permissions = TableRegistry::get('Permissions');
       $data  = $permissions->find("all" , [
		          'conditions' => ['controller' => $controller, 'action' => $action],
		          'fields' => ['user_id']
		       ])->first();
            $permissionsResult = isset($data)? $data->toArray():array();
       $usersdata = TableRegistry::get('Users');
			$user= $usersdata->find("all", [
			'conditions' => ['id'=>$userID]
			])->first();
			//pr($permissionsResult); die;
		  if((count($permissionsResult) > 0) && (count($user) > 0) ){
		   $users = explode(",",$permissionsResult['user_id']);
		if (in_array($userID,$users) ||  in_array(8, explode(",",$user->role_id)) ) {
			return true;
		      } else{ 
			return false;
		     }
		  }
		  return true;
	}

	/**********************************************************************/
	/* @page : modules permissions for admin
	/* @who :Maninder
	/* @why : Super admin needs to controll menus and sub menus permissions
	/* @date: 12-01-2017
	**********************************************************************/
	function modules_permissions($module,$type){
		$modules = TableRegistry::get('Modules');
		$module = $modules->find('all',[
			'conditions' => ['module_name' => $module,'is_enabled' =>'Yes']
			])->first();
		$userinfo = getuseralldata();
		if( (count($module) &&  isset($module)) || $userinfo->role_id == 8 ){ 
			// if the module for that agency is enabled OR logged in user is super admin
			return true; // allowed
		} else {
			return false;
		}

	}
	/**********************************************************************/
	/* @page : new_layout_admin
	/* @who :Maninder
	/* @why : all menus will be iterated through this functions
	/* @date: 12-01-2017
	**********************************************************************/
	function get_menus(){
			$modules = TableRegistry::get('Modules');
			$module = $modules->find('all',[
			'conditions' => ['parent_module' => 0,'is_enabled' =>'Yes','is_displayed' => 'Yes']
			])->all();
			return $module; 
	}

	function getUsersession(){
           $session = $this->request->session();
           $this->set('session',$session);
           $userSession = $session->read("SESSION_ADMIN");
        return $userSession;
    }

		function getuseralldata(){
           $session = $this->request->session();
           $userSession = $session->read("SESSION_ADMIN");
           $user_id=$userSession[0];
           $users = TableRegistry::get('Users');
           $result= $users->find("all", [
            'conditions' => ['id'=>$user_id],
  		  ]);
         $result = isset($result)? $result -> toArray():array();

            return $result;
    }





function getcurrentuserinformation(){
  $this->set("title_for_layout", "ERP Dashboard");
	$session = $this->request->session();
      $this->set('session',$session);
      $userSession = $session->read("SESSION_ADMIN");
		$condition = "id = $userSession[0]";
    $users = TableRegistry::get('Users');
    $data  =  $users->find("all" , [
        'conditions' => ['id' => $userSession[0]],
        'fields' => ['freefrom','current_status','status_modified'],
        ]);
    $result = isset($data)? $data->toArray():array();
     $freefrom = $result[0]['freefrom']; 
    $diff = abs(time()-strtotime($result[0]['status_modified']));
                 if ($diff < 0)$diff = 0;
                 $dl = floor($diff/60/60/24);
                 $hl = floor(($diff - $dl*60*60*24)/60/60);
                 $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);
                 $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));
               // OUTPUT
			   $hl = ($dl *24)+$hl;
      $diff = array('hours'=>$hl, 'minutes'=>$ml, 'seconds'=>$sl);
      $current_status = $result[0]['current_status'];
	$conn = ConnectionManager::get('default');	
		if((HOLIDAY == '0') && (date('l') !="Sunday")){
			//get all users
			$credentials = TableRegistry::get('Users');
				$data = $credentials->find("all")
				->where([
				'AND'=>[['status'=>1],["!FIND_IN_SET('1',role_id)"] ],
				])->order(['first_name' => 'ASC']);
			$userData = isset($data)? $data->toArray():array();

			(date('l') =="Monday") ? $last_date = date("Y-m-d",time()-(2*24*3600)) : $last_date = date("Y-m-d",time()-(24*3600));   //condition for monday and other days
$lastDayAttendance1 = $conn
    ->newQuery()
    ->select(['user_id','status'])
    ->from('attendances')
    ->where( ['date'=>$last_date,'agency_id' => $userSession[3]] )
    ->execute()
    ->fetchAll('assoc');
$AttenceArray=array();
foreach($lastDayAttendance1 as $key=>$v){
	$AttenceArray[$v['user_id']]=  $v['status'];
   }
       $onLeaveUser=array();
         if(count($userData) > 0 ){
            foreach($userData as $usr){
          if (in_array($usr['id'],array_keys($AttenceArray))) {
            if($lastDayAttendance[$usr['id']] == "leave"){
              $onLeaveUser[$usr['id']] = $usr['first_name'].' '.$usr['last_name'];
            } else{
            }
          }else{
            $onLeaveUser[$usr['id']] = $usr['first_name'].' '.$usr['last_name'];
          }
          } 
        } else {
          $onLeaveUser =[];
        }
        $today = date("Y-m-d H:i:s");
        $query ="SELECT * FROM (Select count(`id`) as tot , MIN(`to_id`), MIN(`title`) ,MIN(`deadline`),MIN(`status`) FROM  tickets group by `tickets`.`to_id`) `total` WHERE `total`.`tot` < 5 AND `agency_id` = $userSession[3]"; // working
        $openTicketUsers = $conn->execute($query)->fetchAll('assoc');
                $allNominalTicketUser=array();
                foreach($openTicketUsers as $ticket){ 
          $index[] = $ticket['MIN(`to_id`)'];
        }
               foreach($userData as $user){
               foreach($tic as $t){
                if($user['id']==$t){
                   $allNominalTicketUser[$user['id']] = $user['first_name'].' '.$user['last_name'];
                }
               }
           }

              } else{
        $last_date = date("Y-m-d",time()-(24*3600) );
        $onLeaveUser = [];
        $userData = $conn
            ->newQuery()
            ->select(['id','first_name','last_name','status_modified'])
            ->from('users')
            ->where( ['status' => 1, '!FIND_IN_SET (1,role_id)','agency_id' => $userSession[3] ] )
            ->order(['first_name' => 'asc'])
            ->execute()
            ->fetchAll('assoc');
    }
      $sessionID = $userSession[0];
      $rolesAry = explode(",",$userSession[2]);
      $lastmonth = date("m",mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")));
     $evalData = $conn
    ->newQuery()
    ->select('*')
    ->from('evaluations')
   ->where( ['user_id'=>$sessionID,'month'=>$lastmonth,'year'=>date('Y'),'user_comment'=>NULL,'senior_comment !='=>' ','OR'=>[
        ['status'=>'1'],['status'=>'2']
        ]] )
    ->execute()
    ->fetchAll('assoc');
      $noticeficationCout= count($evalData);
      die;
	return array("date"=>$date,'attendDate'=>$last_date,'onLeaveUsers'=>$onLeaveUser,'userData'=>$userData,'allNominalTicketUser'=>$allNominalTicketUser,'resultdata'=>$data, );

}






	//get project list from projects table
/*Maninder*/
/*30-Dec-2016*/
/*For Rendering Tickets Data*/


function myprojects(){
	/*Projects Data*/
	$session = $this->request->session();
	$this->set('session',$session);
	$userSession = $session->read("SESSION_ADMIN");
	$pp = TableRegistry::get('Projects');
	$total_projects = $pp->find() 
	->select([ 'Projects.id', 'Projects.project_name','Projects.dailyupdate','Projects.dropbox','Projects.vooap','Projects.onalert','Projects.processnotes','Projects.created','Projects.modified','Projects.finishdate','Projects.nextrelease','Projects.totalamount','Engineer_User.first_name','Engineer_User.last_name','Last_Updatedby.first_name','Last_Updatedby.last_name'])
	->where([ 
	'OR' => [ ['pm_id' => $userSession[0] ],['engineeringuser' =>
	$userSession[0] ],['salesbackenduser' =>$userSession[0] ],['salesfrontuser' => $userSession[0] ], 'FIND_IN_SET(\''. $userSession[0] .'\',Projects.team_id)'],
	['Projects.status' => 'current'] ])
	->order(['Projects.modified' => 'DESC'])->contain(['Engineer_User','Last_Updatedby'])->all();
	/*Projects Data*/
	return ['total_projects' => $total_projects];
}



function mytickets(){
	   /*Tickets Data*/
	 $session = $this->request->session();
	$this->set('session',$session);
	$userSession = $session->read("SESSION_ADMIN");
      $tt = TableRegistry::get('Tickets');
 	 //$result = $this->Tickets->find()
      //pr($userSession); die;
      $cond =	[
			'OR' => [['Tickets.from_id' => $userSession[0]], ['Tickets.to_id' => $userSession[0]]],
			'Tickets.status IN' => [0,1,2]
					];
	$oderby = ['Tickets.deadline' => 'asc'];
    $tickets = $tt->find()
	->select(['Tickets.id','Tickets.priority', 'Tickets.to_id','Tickets.from_id','Tickets.title','Tickets.deadline','Tickets.status','Tickets.type','Tickets.modified','User_To.id','User_To.image_url','User_To.first_name','User_To.last_name','User_From.id','User_From.first_name','User_From.image_url','User_From.last_name','Replier.image_url','Replier.first_name','Replier.last_name'])
	->where($cond)
	->order($oderby)->contain(['User_To','User_From','Replier'])->all()->toArray();
	//pr($tickets); die;
	$open =[];
	$inprogress =[];
	$closed =[];
	if(count($tickets) >0 ){
		foreach($tickets as $ticket){
			if($ticket['status'] == 1){
				$open[] = $ticket['id'];	
			}
			if($ticket['status'] == 2){
				$inprogress[] = $ticket['id'];	
			}
			if($ticket['status'] == 0){
				$closed[] = $ticket['id'];	
			}
		}
	}
	
	$this->set('open',$open);
	$this->set('inprogress',$inprogress);
	$this->set('closed',$closed);
return ['open' => $open,'inprogress'=>$inprogress,'closed'=> $closed,'tickets' => $tickets];
      /*Tickets Data*/
}

	//get project list from projects table




//by anamika
	function getProjects(){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
        $Projects = TableRegistry::get('Projects');
		 $result = $Projects->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'project_name',
			'conditions' => ['status'=>'current','agency_id' => $userSession[3]],
			'fields' => ['id','project_name'],
			'order' => 'project_name ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
		$finalresult = array('' => '--Select--') + $res;
		return $finalresult;
	}


   /**

    * @Date: 07-March-2013

    *@Method : getInstructionLists

    *@Purpose: Get an array of links for leads listing

  **/



  function getInstructionLists(){



    return $instLinkArray = array(

									'58' => 	'Client Management',

									'87' => 	'Client Sample Terms & Condition',

									'83' => 	'Client Types',

									'72' =>		'Current Sales Plan',

									'56' =>		'Engagement Model',

									'54' => 	'Freelancing Sites Policy',

									'86' =>		'General Freelancing Sites',

									'53' =>		'Lead Management',

									'89' =>		'Payment Reminder(s)',

									'62' =>		'Project Categories',

									'88' =>		'Sales Credentials',

									'39' =>		'Sales Important URL',

									'64' =>		'Sales Reminder Format(s)',

									'55' =>		'Sales Tips',

									//'76' =>		'Sales Traits'
									'179' =>		'Sales Traits'

								);

  }

  

  //get all users by roles from users table

	function getUsersByRole($role = null){
		$User = TableRegistry::get('Users');
		$session = $this->request->session();
		$userSession = $session->read("SESSION_ADMIN");
		 (!empty($role)) ? $str = "and FIND_IN_SET($role,role_id)" : $str = "";
		 $cond = ['status' => 1 ,'agency_id' => $userSession[3] ,"FIND_IN_SET(5,role_id)"];
          $result= $User->find("list", [
			'keyField' => 'id',
			'valueField' => function ($row) {
            					return $row['first_name'] . ' ' . $row['last_name'];
       		 				}
    	])
         ->where([$cond])
         ->order(['first_name' =>  'ASC']);
		$res = isset($result)? $result->toArray():array();
		$finalresult = array('' => '--Select--') + $res;
		return $finalresult;
	}


function getUsersprofile($role = null) {
	//pr("hii");
	$session = $this->request->session();
	$userSession = $session->read("SESSION_ADMIN");

     $User = TableRegistry::get('Users');
        // pr($User);
   (!empty($role)) ? $str = "and FIND_IN_SET($role,role_id)" : $str = "";
		//pr($str);die;
    $cond = "status = '1' ".$str;
 	$data= $User->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            return $row['first_name'] . ' ' . $row['last_name'];
        }
    ])
		->where(['FIND_IN_SET(\''. $role.'\',role_id)','agency_id' => $userSession[3], 'status' => '1'])         
         ->order(['first_name' =>  'ASC']);
		  	 $result = isset($data)? $data->toArray():array();
         return $result;
}

function getAllUsersprofiles() {
	$session = $this->request->session();
	$userSession = $session->read("SESSION_ADMIN");
    $User = TableRegistry::get('Users');
 	$data= $User->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            return $row['first_name'] . ' ' . $row['last_name'];
	        }
	    ])
		->where(['agency_id' => $userSession[3], 'status' => '1'])         
        ->order(['first_name' =>  'ASC']);
		$result = isset($data)? $data->toArray():array();
        return $result;
}



	

		 //get list of users

	function getuserexceptme($id) {
				$session = $this->request->session();
				$userSession = $session->read("SESSION_ADMIN");
			  $users = TableRegistry::get('Users');
		      $data= $users->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            return $row['first_name'] . ' ' . $row['last_name'];
        }
    ])
        /* ->where(['status' => 1,'id !='=>$id,'agency_id' => $userSession[3]])  changes by diwaker sir said my name is not comming when i add emplopyee  */
        ->where(['status' => 1,'agency_id' => $userSession[3]])
         ->order(['first_name' =>  'ASC']);
		  	 $results = isset($data)? $data->toArray():array();
		  	  if(isset($this->request->data["controller"]) && $this->request->data["controller"]=="projects"){
			$result = ['' => 'Management Cell'] + $result;
		 }else{
			$result = ['' => '--Select--']+$results;
		 }
		return $result;


    }



// that is used on the http://doers.online/msoftware/projects/processreport on this page becoz current user not set as a responsible 
	function getuserexceptme_processreport($id) {
			  	$session = $this->request->session();
				$userSession = $session->read("SESSION_ADMIN");

			  $users = TableRegistry::get('Users');
		      $data= $users->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            return $row['first_name'] . ' ' . $row['last_name'];
       			 }
   			 ])

         ->where(['status' => 1,'agency_id' => $userSession[3]])
         ->order(['first_name' =>  'ASC']);
		    // pr($data);
		  	 $results = isset($data)? $data->toArray():array();
		 	//pr($results); die;
		  	  if(isset($this->request->data["controller"]) && $this->request->data["controller"]=="projects"){
			$result = ['' => 'Management Cell'] + $result;
			//pr($result);
		 }else{
			$result = ['' => '--Select--']+$results;
			//pr($result);
		 }
		return $result;
    }






//

function getuserexceptmedit($id) {
			  $session = $this->request->session();
				$userSession = $session->read("SESSION_ADMIN");

			  $users = TableRegistry::get('Users');
		      $data= $users->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
            	return $row['first_name'] . ' ' . $row['last_name'];
        		}        	
    		])
         ->where(['status' => 1,'id'=>$id,'agency_id' => $userSession[3]])
         ->order(['first_name' =>  'ASC']);
		    // pr($data);
		  	 $results = isset($data)? $data->toArray():array();
		 	//pr($results); die;
		  	  if(isset($this->request->data["controller"]) && $this->request->data["controller"]=="projects"){
			$result = ['' => 'Management Cell'] + $result;
			//pr($result);
		 }else{
			$result = $results;
			//pr($result);
		 }
		return $result;




/*

        App::import("Model","User");

         $this->User= new User;

         $result= $this->User->find('list', array(

			 //'conditions' => array('status'=>'1','id !='=>$id),

			 'conditions' => array('status'=>'1'),

			 'fields' => array('id','user_name'),

			 'order'=>'user_name  ASC'

		));

		 if($this->params["controller"]=="projects"){

			$result = array('' => 'Management Cell') + $result;

		 }else{

			$result = array('' => '--Select--') + $result;

		 }

         return $result;
         */

    }















    //
	

	function getFirstLastDates($m = null,$y = null){

		$month= (!empty($m)) ? $m : date('m'); 

		$year= (!empty($y)) ? $y : date('Y'); 

		$lastday = cal_days_in_month(CAL_GREGORIAN, $month, $year); 

		$result =array();

		$result['firstdate'] = $year."-".$month."-01";

		$result['lastdate'] = date($year."-".$month.'-'.$lastday);// this function will give you the number of days in given month(that will be last date of the month) 

		return $result;

	}

	

	//get project list from projects table on the basis of user IDs 

 function getProjectsofuser($userid = null){

	$projects = TableRegistry::get('Projects');



		 $condition =  "`status`='current' AND (`pm_id` = $userid OR `engineeringuser` = $userid OR `salesfrontuser` =$userid OR `salesbackenduser` = $userid OR FIND_IN_SET($userid,team_id))";
//pr($condition);
    
		     $result= $projects->find("list" , [
			'keyField' => 'id',
			'valueField' => 'project_name',
			'conditions' =>[
							'status' => 'current',
			        		'OR' => [['pm_id' => $userid], ['engineeringuser' => $userid], ['salesfrontuser' => $userid], ['salesbackenduser' => $userid],'FIND_IN_SET(\''. $userid .'\',team_id)']
			        		],
			 'fields' => ['id','project_name'],
			'order' => ['project_name' => 'ASC']
			 ]);
			
		$result = array('0' => 'Other') + $result->toArray();
		//pr($result); die;
		 //$result = isset($result)? $result -> toArray():array();
		return $result;


	}


	

	//get all skills list

	function getSkills(){

		 App::import("Model","Skill");

		 $this->Skill = new Skill;

         $result= $this->Skill->find('list', array(

         'fields' => array('id','skills'),

         'order'=>'skills  ASC'

    ));

         $result = array('' => '--Select--') + $result;

         return $result;



	}

	

	//changes sql timestamp format to compare

	function expiredTime($modified_date){

                $diff = abs(time()-strtotime($modified_date));

                 if ($diff < 0)$diff = 0;

                 $dl = floor($diff/60/60/24);

                 $hl = floor(($diff - $dl*60*60*24)/60/60);

                 $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);

                 $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));

               // OUTPUT

			   $hl = ($dl *24)+$hl;

               $return = array('hours'=>$hl, 'minutes'=>$ml, 'seconds'=>$sl);
               //pr($return); die;
               return $return;

	}

	function getuserexceptraci(){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
         $users = TableRegistry::get('Users');
		 $data= $users->find("list", [
					'keyField' => 'id',
					 'valueField' => function ($row) {
		            return trim($row['first_name']);
		        }
		    ])
         ->where(['status'=>'1','agency_id' => $userSession[3]])
         ->order(['first_name' =>  'ASC']);

 		$result = isset($data)? $data->toArray():array();
		return $result;

	}


	 //get designations from setting table

	function getalldesignations($agency_id) {
			$setting = TableRegistry::get('Settings');
		  $data = $setting->find("all", [
          'conditions' => ['Settings.key'=>'designations','agency_id' => $agency_id],
          'fields' => ['key','value','agency_id']
      		  ])->first();
		  	$result = isset($data)? $data->toArray():array();
		$resultAry = explode(",",$result['value']); 
		$finalData = array();
		foreach($resultAry as $k=>$v){
			$val = trim($v," ");
			$finalData[$val] = $val;
		}
        $finalData = array('' => '--Select--') + $finalData;
     //   pr($finalData);die();

         return $finalData;

    }

	//get salary and designation info for specific id from appraisal tables

	function getInfoFromAppraisal($id = null) {

     $roles = TableRegistry::get('Appraisals');
		  $data  = 	$roles->find("all" , [
					//'keyField' => 'new_designaiton',
					//'valueField' => 'appraised_amount',
		          'conditions' => ['user_id' => $id,'status'=>1],
		          'fields' => ['appraised_amount','new_designaiton'],
		          'order' => ['effective_from'=>'desc'],
		       ])->first();
		  	 $result = isset($data)? $data->toArray():array();
         return $result;


    }
	 //get list of users ecxept logged in user

	function getuserexceptloggedin($id) {


 $roles = TableRegistry::get('Users');
         //pr($roles);
		  $data  = 	$roles->find("list" , [
					'keyField' => 'id',
					'valueField' => 'first_name','last_name',
		          'conditions' => ['status'=>'1'],
		          'fields' => ['id','first_name','last_name'],
		         
		       ]);
		// pr($data->toArray());
		  	 $result = isset($data)? $data->toArray():array();
		  	// pr($result);
		
         return $result;
}






    //
	

	// get Client Session info

	function getClientSession(){

		$clientSession = $_SESSION["SESSION_CLIENT"]; 

		return $clientSession;

	}

	/*function getusername($id){

		$Users = TableRegistry::get('Users');

         $this->User= new User;

         $result= $this->User->find('first', array(

			 'conditions' => array('id'=>$id),

			 'fields' => array('user_name')

		));

		 return $result;

	}*/

function getusername($id){

   $users = TableRegistry::get('Users');
  // pr($users);
		 $data = $users->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['id'=>$id],
			'fields' => ['id','username']
			
		 ]);
		 //pr($data);
		 
 		$result = isset($data)? $data->toArray():array();
 		//pr($result);

		return $result;

	}

 


function getupaymentname($id = null){
   $Test = TableRegistry::get('PaymentMethods');
 // pr($id);
		 $data = $Test->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'name',
			'fields' => ['id','name']
			
		 ]);
		 //pr($data);
		 
 		$result = isset($data)? $data->toArray():array();
 		//pr($result); die;

		return $result;

	}



	function getCredentialsforUser($idStr){
			//PR($idStr);
		   $credentials = TableRegistry::get('Credentials');
		  $data  = 	$credentials->find("all" , [
		          //'conditions' => ['id IN' => implode(',',$idStr)],
		           'conditions' => ['id IN' => $idStr],
		          'fields' => ['id','type','username']
		       ]);//->all();		//pr($data);die;
		   $result = isset($data)? $data->toArray():array();
		   //pr($result); die;
		return $result;
		
		 //$credentials->updateAll($update,['user_id IN' => $this->request->data['IDs']]);



		/*


		 App::import("Model","Credential");

         $this->Credential= new Credential;

		 $result= $this->Credential->find('all', array(

			 'conditions' => "Credential.id in ($idStr)",

			 'fields' => array('Credential.id','Credential.type','Credential.username')

		));

		return $result;

		*/

	}
	

	//get profile names from sale_profile table

	function getprofile(){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		 $SaleProfiles = TableRegistry::get('SaleProfiles');
		 $result = $SaleProfiles->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['status'=>1,'agency_id' => $userSession[3]],
			'fields' => ['id','username'],
			'order' => 'username ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
		$finalresult = array('' => '--Select--') + $res;
		return $finalresult;
	}
	

function getprofileps($id = null){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");
		 $SaleProfiles = TableRegistry::get('SaleProfiles');
		 $result = $SaleProfiles->find('list',[
		 	'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['agency_id'=> $userSession[3]],
			'fields' => ['id','username'],
			'order' => 'username ASC'
		 ]);
 		$res = isset($result)? $result->toArray():array();
		$finalresult = $res;
		return $finalresult;
	}
	

		function dynamic_notification(){
			$setting = TableRegistry::get('Settings');
		  $data = $setting->find("all", [
          'conditions' => ['Settings.key' => 'notification'],
          'fields' => ['value']
      		  ]);
		  	$notice = isset($data)? $data->toArray():array();
				return $notice;

			}
			
				//get profiles detail  from sale_profiles table on the basis of user IDs 
function getProfilesForLead($userID = null){
		$session = $this->request->session();
		$this->set('session',$session);
		$userSession = $session->read("SESSION_ADMIN");

		$SaleProfiles = TableRegistry::get('SaleProfiles');
		$res = $SaleProfiles->find('list',[
		'keyField' => 'id',
		'valueField' => 'alloteduserid',
		'fields'=>['id', 'alloteduserid'],
		'conditions'=>['status'=>1,'agency_id' => $userSession[3]]
		]);

		$result = isset($res)? $res->toArray():array();
			$ids = array();
		$finalid = array();
		$id_array = array();
		foreach($result as $key=>$val){
    		if(in_array($userID,explode(",",$val))){
		        $id_array[]=$key;
		  }else{
		  	   $session = $this->request->session();
				$userSession = $session->read("SESSION_ADMIN");
				$role = explode(",",$userSession['2']);
			  if(in_array("1", $role)){
                     $id_array[]=$key;
                    }
		          }


		       }
          	if(count($id_array) > 0){
          		$finalresult = $SaleProfiles->find('list', [
					'keyField' => 'id',
					'valueField' => 'username',
					'fields'=>[ 'id','username'],
					'conditions'=>['id IN'=>$id_array ,'agency_id' => $userSession[3]]
					]);
          	}
                
               // pr($finalresult->toArray()); die;
					$result = isset($finalresult)? $finalresult->toArray(): array();

							$finalresult = array('' => '--Select--') + $result;
							return $finalresult;	
	   	   
	       }


function getWeekRange(){

    return array(
	
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'today'=>'Today',	
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }
   function selectforbidreport(){

    return array(
	
	'today'=>'Today',
	'yesterday'=>'Yesterday',
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }




  function getMonthsArray(){

    return array(
	""=>"-Select-",
	"01" => "January",
	"02" => "February",
	"03" => "March",
	"04" => "April",
	"05" => "May",
	"06" => "June",
	"07" => "July",
	"08" => "August",
	"09" => "September",
	"10" => "October",
	"11" => "November",
	"12" => "December"
		);
  }

   /**
    * @Date: 22-Sep-2014
    *@Method : getMonthRange
    *@Purpose: Get an array of Months
  **/

  function getMonthRange(){

    return array(
	
	'month'=>'Month',
	'year'=>'Year',
	'today'=>'Today',	
	'week'=>'Week',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }


  /**
    * @Date: 22-Sep-2014
    *@Method : getDateRange
    *@Purpose: Get an array of Months
  **/

  function getDateRange(){

    return array(
	
	'today'=>'Today',
	'week'=>'Week',
	'month'=>'Month',
	'year'=>'Year',
	'custom'=>'Custom',
	"select"=>"-Select-"
		);
  }

  function getuser_name() {
		$session = $this->request->session();
		$this->set('session',$session);
		$user = $session->read("SESSION_ADMIN");

       $users = TableRegistry::get('Users');
         $result= $users->find("list", [
			'keyField' => 'id',
			 'valueField' => function ($row) {
           					 return $row['first_name'] . ' ' . $row['last_name'];
       						 }
   		 ])
        ->where(['status'=>'1','agency_id' => $user[3]])
         ->order(['first_name' =>  'ASC']);
         $result = isset($result)? $result -> toArray():array();
         $finalresult = array('' => '--Select--') + $result;
         return $finalresult;
    } 

      function getuser_reports() {
		$session = $this->request->session();
		$this->set('session',$session);
		$user = $session->read("SESSION_ADMIN");
		$users = TableRegistry::get('Users');
		$result= $users->find("list", [
		'keyField' => 'id',
		 'valueField' => function ($row) {
				            return $row['first_name'] . ' ' . $row['last_name'];
				        }
		  ])
	     ->where(['status'=>'1','agency_id' => $user[3],"!FIND_IN_SET('1',role_id)"])
	     ->order(['first_name' =>  'ASC']);

     $result = isset($result)? $result -> toArray():array();
     $TakingArray=array();
         foreach($result as $v){
         	$TakingArray[$v]= $v;
         	
         }

         $finalresult = array('' => '--Select--') + $TakingArray;

         return $finalresult;

    }

   /**
    * @Date: 11-jan-2017
    *@Method : child_modules
    *@Purpose: get child modules of a parent module
    * @who : Maninder
  **/

	function child_modules($parent_id){
		//pr($parent_id); die;
			$modules =  $users = TableRegistry::get('Modules');
			$module = $modules->find('all',[
				'conditions' => ['parent_module' => $parent_id]
				])->all();
			//pr($module); die;
			return $module;
		}
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function strtohex($x)
  {
    $s='';
    foreach (str_split($x) as $c) $s.=sprintf("%02X",ord($c));
    return($s);
  }
 	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function ENC($string){
   $iv =   IVIM;
   $pass = PASSIM;
    $method = 'aes-128-cbc';    // method for encryption
    $enc = openssl_encrypt($string, $method, $this->strtohex ($pass), false ,$this->strtohex ($iv));
    return $enc;
  }
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function DCR($string){
    $iv =  IVIM;  //  getting defined constants
    $pass = PASSIM; //  getting defined constants
    $this->strtohex($iv);
    $this->strtohex($pass);
    $method = 'aes-128-cbc';    // method for encryption
    $dcr =  openssl_decrypt( $string , $method , $this->strtohex ($pass), false , $this->strtohex ($iv) );
    return $dcr;
  }
/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl Encryptions for url eligible encryptions
	/* @date: 12-01-2017
	**********************************************************************/
  function encForUrl($enc){
  $encoded = str_replace('/', 'Hxkuy9jdji877jkgha692ahuiyadyu76iuyai6', $enc);
  $encoded = str_replace('+', 'iy878ned7ehhsd87e6fd2140jdamnhgHHDysMM', $encoded);
  $encoded = str_replace('|', 'MMDU897bhhdtHHdtslsatxtas', $encoded);
  return $encoded;
 }
	/**********************************************************************/
	/* @page : Internal
	/* @who : Maninder
	/* @why : Open ssl for url eligible decryptions
	/* @date: 12-01-2017
	**********************************************************************/
	function dcrForUrl($dcr){
	  $decrypt = str_replace('iy878ned7ehhsd87e6fd2140jdamnhgHHDysMM', '+', $dcr);
	  $decrypt = str_replace('Hxkuy9jdji877jkgha692ahuiyadyu76iuyai6', '/', $decrypt);
	  $decrypt = str_replace('MMDU897bhhdtHHdtslsatxtas', '|', $decrypt);
	  return $decrypt;
	}
	

	/**********************************************************************/
	/* @page : New layout Admin
	/* @who : Maninder
	/* @why : for rendering menus in layout
	/* @date: 12-01-2017
	**********************************************************************/
	function child_modules_fl($parent_id){
		//pr($parent_id); die;
			$modules =  $users = TableRegistry::get('Modules');
			$userinfo = $this->getuseralldata();
			//pr($userinfo); die;
			$modules = $modules->find('all',[
				'conditions' => ['parent_module' => $parent_id, 'is_enabled' => 'Yes' ,'is_displayed' => 'Yes' ]
				])->contain(['Perms'])->all();
			pr($modules); die;
			$filtered_modules = [];
			$roles = explode(',', $userinfo[0]['role_id']) ; // current user's roles
			$useragency = $userinfo[0]['agency_id'];
			if(count($roles) > 0 ){ // if there are any roles alloted to currentuser
				foreach($modules as $module){ // iterating all modules of current agency
					$modulesroles = explode(',', $module->role_id); // creating array of roles to which current module is alloted
					pr($modulesroles); die;
					$agencies = explode(',', $module->agency_id);
					$is_once = 'yes'; // to make sure that item is pushed to final array once only
					foreach($roles as $role){ // iterating users roles
						if(in_array($role, $modulesroles) && $is_once == 'yes'  && in_array($useragency, $agencies)){ // if user has role to which that module is alloted
							$filtered_modules[] = $module; // push whole module to final array
							$is_once = 'no'; // stopping to push same module again
						}
					}
				}	
			}
	}
	function getaliasnames($ids){
			$exp = explode(',', $ids);
			$permissions =  TableRegistry::get('Permissions');
			$modules = $permissions->find('list',[
					'keyField' => 'alias',
					'valueField' => 'alias',
					'fields'=>[ 'alias','alias'],
				'conditions' => ['id IN' => $exp ]
				])->toArray();
			return array_keys($modules);
		}



	function getsessiondata(){
           $session = $this->request->session();
           $this->set('session',$session);
           $adminlogin = $session->read("adminlogin");
        return $adminlogin;
    }



function getuserid($id){
             $usersdata = TableRegistry::get('Users');
			$user= $usersdata->find("all", [
			'conditions' => ['id'=>$id]
			])->first();
			  return $user;

    }

	function getsession(){
           $session = $this->request->session();
           $this->set('session',$session);
           $sess = $session->read("SESSION_ADMIN");
        return $sess;
    }


function multAuth($subusers)
{
  $authArray=[];
    $usersdata = TableRegistry::get('Users');
    $admin= $usersdata->find("all", [
			'conditions' => ['id'=>$subusers[0]]
			])->first();
array_shift($subusers);
  $usersdata = TableRegistry::get('Users');
    $users= $usersdata->find("all", [
			'conditions' => ['id IN'=>$subusers]
			])->all();
  $authArray[] = $admin;
  $authArray[] = $users;
  //pr($authArray); die;
  return $authArray;


}

function getrolenames($role_id){
	  $roles = TableRegistry::get('Roles');
		  $data  = 	$roles->find("list" , [
					'keyField' => 'id',
					'valueField' => 'role',
		            'conditions' => [/*'status' => 1,*/ 'id IN' => explode(',', $role_id)],
		            'fields' => ['id','role'],
		            'order'=>['role'=>'ASC']
		       ]);
		// pr($data->toArray()); die;

		 $result = isset($data)? $data->toArray():array();
		return $result;
}
function otheraliases($ids){
	$modules = TableRegistry::get('Permissions');
	$exp = explode(',', $ids);
	/*pr($exp); die;*/
	$result=[];
	if(count($exp) > 0){
	 $modulesdata = $modules->find("all", [
			'conditions' => ['id IN'=>$exp]
			])->all();
	  $result = isset($modulesdata)? $modulesdata->toArray():array();
	}
	return $result;
}

function getclientbyid($id){
	$modules = TableRegistry::get('Contacts');
	//$exp = explode(',', $ids);
	/*pr($exp); die;*/
	$result=[];
	//if(count($exp) > 0){
	 $modulesdata = $modules->find("all", [
			'conditions' => ['id'=>$id],
			'fields' => ['id','name']
			])->first();

	  $result = isset($modulesdata)? $modulesdata->toArray(): array();
	//}
	return $result;
}


function remote_file_exists($url)
        {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if( $httpCode == 200 ){return true;}
        }

 function get_active_users($ids){

      $users = TableRegistry::get('Users');
         $result= $users->find("all", [
			'keyField' => 'id',
			'valueField' => 'username',
			'conditions' => ['status'=>'1','id IN' => $ids],
			'fields' => ['id','username'],
			'order'=>['username' =>  'ASC']
  		  ]);
         $result = isset($result)? $result->toArray():array();
         $finalresult = $result;
         return $finalresult;
    } 

   
  // Getting Count of Audit Points of Given profile ID  
  function profileaudit_count($profile_id){
  	//echo $profile_id; die;
  	 $Profileaudit = TableRegistry::get('Profileaudits');
 	$pro = $Profileaudit->find('all',['fields'=>['status','profile_id','resolvedby'],
            'conditions' => ['profile_id' => $profile_id ]
          ])->count();

 	return $pro;

 }
 

} //last

